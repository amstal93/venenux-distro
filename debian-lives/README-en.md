# VenenuX how to build a live easyle

Building a live OS image for daily usage on older or mid modern machine computers 
PAra español latino [README.md](README.md) tome en consideracion que no hablamos majo españa.

It is a ready-to-use operating system that includes powered packages from VenenuX
for remote management as the most critical component to assist users.

## Introduction and Downloads

Running what is in this repository **will create a disk or ISO with an operating system,
ready to use, YES, READY TO USE, DOES NOT REQUIRE TO INSTALL but can be installed**, 
filed produced can be "recorded on a blank CD/DVD" or "can be recorded on a USBdrive" and used on the PC,
without deleting your installed operating system, but with the option to replace it if you want.

** All this is not necessary, download it ready to use from our website
web: http://vegnuli.sourceforge.net/** Where it says "VenenuX OS"!

[![Download VeGNUli ISO](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/vegnuli/files/VenenuX-1.0/)

## Quick guide to this repository

VenenuX has two flavours, server and desktop.

VenenuX is based on Debian, but function oriented and performance focused.
Its employ the MATE desktop for modern machines and LXDE for low performance computers.

The VenenuX versions are always the same at the Debian releases, the main difference 
is more support focused to usage and functionality, pointing in performance, 
also we try to give moree support for older releases that of course still worked!

## VenenuX desktop

This system was created using the VenenuX packages and the Live-build debian system,

With this ISO, can be recorded both in USB or CD/DVD and simply botted in the computer,
What is the process that ignorant mortals know? obviously yes.

Currently this repository use branches to perfrom al the flavours and releases, if 
you wants a VenenuX version go to web download, if you wish to build those resources 
you must know about git usage and branches.

## FAQ: frecuently answers and questions

#### **P: How can i use it?**

With this ISO, it is recorded both in USB as on CD/DVD and simply booted into a computer, 
fgor this easyle use Balena Echer Electron program that is same in any OS.
One finished to burn o dumped it booted into the computer and used without installation (but with option too)

#### **P: Can i used as main OS system?**

Yes, it has a large number of programs, it even has a complete development environment, 

# About live build and VenenuX

It is recommended to visit the project http://gitlab.com/venenux/venenux-distro for information, the base 
packages, why they are included and why not. VenenuX is an operating system derived from Debian GNU/Linux 
focused on multimedia, not only professional, but also for the user who wants to see everything.

#### Quick guide to live-build VenenuX

live build creates a structure with which you can alter the result of the live cd, here shown, 
below the specific documentation of this and how to make your own iso:

```
├── lb-scriptpropio        <--------- script creado con la invocacion a lb config
└── config
    ├── archives           <--------- xxxx.list.yyyy sources.list de repositorios ajenos
    ├── bootloaders
    │   └── isolinux       <--------- contenido COMPLETO base de el cd/img resultante, incluyendi menu.cfg
    ├── hooks
    ├── includes.binary    <--------- archivos extra que terminaran incluidos en el cd o imagen, en raiz
    ├── includes.chroot
    │   ├── etc
    │   │   ├── lightdm    <--------- queremos meter lxdm pero systemd no nos deja, versiones 0.x usan lxdm/slim
    │   │   ├── live       <--------- live user setups scripts
    │   │   └── skel       <--------- skeleton /home directory, plantilla de como home se preconfigura
    │   └── usr
    │       ├── bin       <--------- colocar aqui cualquier programa precompilado o directo
    │       ├── lib       <--------- colocar aqui cualquier precompilada precompilado o directo
    │       ├── local
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    ├── includes.installer <--------- installer preseed.cfg file and logo/theme update for the graphical installer
    │   └── usr
    │       └── share       <--------- colocar aqui cualquier oro archiovo e ira a share en el live
    │           └── graphics  <--------- logo/theme update for the graphical installer
    ├── package-lists      <--------- listado de nombre of packages to be installed, uno por linea
    └── packages.chroot    <--------- otros packages to be installed manualmente o por archivo deb
```

# See also:

* [CONTRIBUTING.md](CONTRIBUTING.md) : Reglas de conducta (inglesh)
* [CONTRIBUTING.es.md](CONTRIBUTING.es.md) : Reglas de conducta (español)
* [README.packages.md](README.packages.md) : package list inclusion.

