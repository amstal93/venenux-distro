This is a list of the menu-item numbers we have currently assigned.
If you need a number, don't just grab one, ask on debian-boot@lists.debian.org
first.

Please do not allocate numbers with too high a granularity. If you have two
packages, A and B, and A depends on B, then main-menu will always put A
after B. There is no need to assign different numbers to the two; one
number will do and will be more flexible, and help avoid BASIC line number
disease. In other words, having a bunch of menu items with the same number
is a feature, not a bug. Use different menu items only to break ties and do
gross positioning in the menu.

(At some point in the future, all numbers 0-100 below should be multiplied
by 10 to give us more space.)

0 -> 10 Reserved

10 -> 30 Preparation and installer setup
10 localechooser
11 load-floppy
12 kbd-chooser
13 iso-scan
   cdrom-detect
14 file-preseed
16 load-iso
   load-cdrom
17 ethdetect
   ppp-udeb
   s390-netdevice (s390)
18 netcfg
   netcfg-static
21 network-preseed
   network-console
23 choose-mirror
   download-installer
25 lowmem

30 -> 40 Disk detection & etc
35 disk-detect (!s390)
37 s390-dasd (s390)

40 -> 50 Disk preparation
42 partman (as default partitioner)
43 partitioner (m68k, mips, s390)
44 lvmcfg
44 partman (mips)
45 partconf (m68k, mips, s390)
45 mdconfig
47 evms
49 partman (as non-default partitioner [s390])
50 autopartkit

60 -> 70 Base install
62 tzsetup
   clock-setup
63 user-setup
65 base-installer

70 -> 80 Post-base-install
70 apt-setup
   pkgsel
73 palo-installer (hppa)
   grub-installer (i386)
   yaboot-installer (powerpc/powermac_newworld, powerpc/chrp*)
   quik-installer (powerpc/powermac_oldworld)
   zipl-installer (s390)
   arcboot-installer (mips)
   delo-installer (mipsel)
   silo-installer (sparc)
   vmelilo-installer (m68k/vme*)
   glantank-installer (arm/iop32x)
75 lilo-installer (i386)
76 elilo-installer (ia64, i386)
77 nobootloader (all)
78 finish-install

80 -> 90 Reserved

900 -> \infty Neverdefault
930 cdebconf-priority
940 cdrom-checker
940 baseconfig-udeb
950 di-utils-shell
