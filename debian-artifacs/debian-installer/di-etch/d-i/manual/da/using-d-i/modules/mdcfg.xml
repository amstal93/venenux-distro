<!-- retain these comments for translator revision tracking -->
<!-- original version: 22841 untranslated -->

   <sect3 id="mdcfg">
   <title>Configuring Multidisk Device (Software RAID)</title>
<para>

If you have more than one harddrive<footnote><para>

To be honest, you can construct MD device even from partitions
residing on single physical drive, but that won't bring you anything
useful.

</para></footnote> in your computer, you can use
<command>mdcfg</command> to setup your drives for increased
performance and/or better reliability of your data. The result is
called <firstterm>Multidisk Device</firstterm> (or after its most
famous variant <firstterm>software RAID</firstterm>).

</para><para>

MD is basically a bunch of partitions located on different disks and
combined together to form a <emphasis>logical</emphasis> device. This
device can then be used like an ordinary partition (i.e. in
<command>partman</command> you can format it, assign a mountpoint,
etc.).

</para><para>

The benefit you gain depends on a type of a MD device you are
creating. Currently supported are:

<variablelist>
<varlistentry>

<term>RAID0</term><listitem><para>

Is mainly aimed at performance.  RAID0 splits all incomming data into
<firstterm>stripes</firstterm> and distributes them equally over each
disk in the array. This can increase the speed of read/write
operations, but when one of the disks fails, you will loose
<emphasis>everything</emphasis> (part of the information is still on
the healthy disk(s), the other part <emphasis>was</emphasis> on the
failed disk).

</para><para>

The typical use for RAID0 is a partition for video editing.

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID1</term><listitem><para>

Is suitable for setups where reliability is the first concern.  It
consists of several (usualy two) equaly sized partitions where every
partition contains exactly the same data. This essentialy means three
things.  First, if one of your disks fails, you still have the data
mirrored on the remaining disks. Second, you can use only a fraction
of the available capacity (more precisely, it is the size of the
smallest partition in the RAID). Third, file reads are load balanced among
the disks, which can improve performance on a server, such as a file
server, that tends to be loaded with more disk reads than writes.

</para><para>

Optionally you can have a spare disk in the array which will take the
place of the failed disk in the case of failure.

</para></listitem>
</varlistentry>
</variablelist>

To sum it up:

<informaltable>
<tgroup cols="5">
<thead>
<row>
  <entry>Type</entry>
  <entry>Minimum Devices</entry>
  <entry>Spare Device</entry>
  <entry>Survives disk failure?</entry>
  <entry>Available Space</entry>
</row>
</thead>

<tbody>
<row>
  <entry>RAID0</entry>
  <entry>2</entry>
  <entry>no</entry>
  <entry>no</entry>
  <entry>Size of the smallest partition multiplied by number of devices in RAID</entry>
</row>

<row>
  <entry>RAID1</entry>
  <entry>2</entry>
  <entry>optional</entry>
  <entry>yes</entry>
  <entry>Size of the smallest partition in RAID</entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

If you want to know the whole truth about Software RAID, have a look
at <ulink url="&url-software-raid-howto;">Software RAID HOWTO</ulink>.

</para><para>

To create a MD device, you need to have the desired partitions it
should consist of marked for use in a RAID.  (This is done in
<command>partman</command> in the <guimenu>Partition
settings</guimenu> menu where you should select <menuchoice>
<guimenu>Use as:</guimenu> <guimenuitem>Use the partition as a RAID
device</guimenuitem> </menuchoice>.)

</para><warning><para>

Support for MD is a relatively new addition to the installer.
You may experience problems for some RAID levels and in combination
with some bootloaders if you try to use MD for the root
(<filename>/</filename>) filesystem. For experienced users, it may be
possible to work around some of these problems by executing some
configuration or installation steps manually from a shell.

</para></warning><para>

Next, you should choose <guimenuitem>Configure software
RAID</guimenuitem> from the main <command>partman</command> menu.
On the first screen of <command>mdcfg</command> simply select
<guimenuitem>Create MD device</guimenuitem>. You will be presented with
a list of supported types of MD devices, from which you should choose
one (e.g. RAID1). What follows depends on the type of MD you selected.
</para>

<itemizedlist>
<listitem><para>

RAID0 is simple &mdash; you will be issued with the list of available
RAID partitions and your only task is to select the partitions which
will form the MD.

</para></listitem>
<listitem><para>

RAID1 is a bit more tricky. First, you will be asked to enter the
number of active devices and the number of spare devices which will
form the MD. Next, you need to select from the list of available RAID
partitions those that will be active and then those that will be
spare. The count of selected partitions must be equal to the number
provided few seconds ago. Don't worry. If you make a mistake and
select different number of partitions, the &d-i; won't let you
continue until you correct the issue.

</para></listitem>
</itemizedlist>

<para>

It is perfectly possible to have several types of MD at once. For
example if you have three 200 GB hard drives dedicated to MD, each
containing two 100 GB partitions, you can combine first partitions on
all three disk into the RAID0 (fast 300 GB video editing partition)
and use the other three partitions (2 active and 1 spare) for RAID1
(quite reliable 100 GB partition for <filename>/home</filename>).

</para><para>

After you setup MD devices to your liking, you can
<guimenuitem>Finish</guimenuitem> <command>mdcfg</command> to return
back to the <command>partman</command> to create filesystems on your
new MD devices and assign them the usual attributes like mountpoints.

</para>
   </sect3>
