<!-- retain these comments for translator revision tracking -->
<!-- original version: 18773 untranslated -->

 <sect1 id="linux-upgrade">
 <title>Installing &debian; from a Unix/Linux System</title>

<para>

This section explains how to install &debian; from an existing
Unix or Linux system, without using the ncurses-based, menu-driven
installer as explained in the rest of the manual. This "cross-install"
HOWTO has been requested by users switching to &debian; from
Redhat, Mandrake, and SUSE. In this section some familiarity with
entering *nix commands and navigating the file system is assumed. In
this section, <prompt>$</prompt> symbolizes a command to be entered in
the user's current system, while <prompt>#</prompt> refers to a
command entered in the Debian chroot.

</para><para>

Once you've got the new Debian system configured to your preference,
you can migrate your existing user data (if any) to it, and keep on
rolling. This is therefore a "zero downtime" &debian;
install. It's also a clever way for dealing with hardware that
otherwise doesn't play friendly with various boot or installation
media.

</para>

  <sect2>
  <title>Getting Started</title>
<para>

With your current *nix partitioning tools, repartition the hard
drive as needed, creating at least one filesystem plus swap. You
need at least 150MB of space available for a console only install,
or at least 300MB if you plan to install X.

</para><para>

To create file systems on your partitions. For example, to create an 
ext3 file system on partition <filename>/dev/hda6</filename> (that's  
our example root partition):

<informalexample><screen>

 $ mke2fs -j /dev/hda6

</screen></informalexample>

To create an ext2 file system instead, omit <userinput>-j</userinput>.

</para><para>

Initialize and activate swap (substitute the partition number for 
your intended Debian swap partition):

<informalexample><screen>

 $ mkswap /dev/hda5
 $ sync; sync; sync
 $ swapon /dev/hda5

</screen></informalexample>

</para><para>

Mount one partition as <filename>/mnt/debinst</filename> (the
installation point, to be the root (<filename>/</filename>) filesystem
on your new system). The mount point name is strictly arbitrary, it is
referenced later below.

<informalexample><screen>

 $ mkdir /mnt/debinst
 $ mount /dev/hda6 /mnt/debinst

</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Install <command>debootstrap</command></title>
<para>

The tool that the Debian installer uses, which is recognized as the
official way to install a Debian base system, is
<command>debootstrap</command>. It uses <command>wget</command>, but
otherwise depends only on <classname>/bin/sh</classname>. Install
<command>wget</command> if it isn't already on your current system,
then download and install <command>debootstrap</command>.

</para><para>

If you have an rpm-based system, you can use alien to convert the 
.deb into .rpm, or download an rpm-ized version at 
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink>

</para><para>

Or, you can use the following procedure to install it
manually. Make a work folder for extracting the .deb into:

<informalexample><screen>

 $ mkdir work
 $ cd work

</screen></informalexample>
</para><para>

The <command>debootstrap</command> binary is located in the Debian
archive (be sure to select the proper file for your
architecture). Download the <command>debootstrap</command> .deb from
the <ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/">
pool</ulink>, copy the package to the work folder, and extract the
binary files from it. You will need to have root privileges to install
the binaries.

<informalexample><screen>

 $ ar -xf debootstrap_0.X.X_arch.deb
 $ cd /
 $ zcat &#60; /full-path-to-work/work/data.tar.gz | tar xv

</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Run <command>debootstrap</command></title>
<para>

<command>debootstrap</command> can download the needed files directly
from the archive when you run it. You can substitute any Debian
archive mirror for <userinput>http.us.debian.org/debian</userinput> in
the command example below, preferably a mirror close to you
network-wise. Mirrors are listed at 
<ulink url="http://www.debian.org/misc/README.mirrors"></ulink>.

</para><para>

If you have a &releasename; &debian; CD mounted at
<filename>/cdrom</filename>, you could substitute a file URL instead
of the http URL: <userinput>file:/cdrom/debian/</userinput>

</para><para>

Substitute one of the following for <replaceable>ARCH</replaceable> 
in the <command>debootstrap</command> command: 

<userinput>alpha</userinput>, 
<userinput>arm</userinput>,
<userinput>hppa</userinput>, 
<userinput>i386</userinput>, 
<userinput>ia64</userinput>, 
<userinput>m68k</userinput>,
<userinput>mips</userinput>, 
<userinput>mipsel</userinput>, 
<userinput>powerpc</userinput>, 
<userinput>s390</userinput>, or
<userinput>sparc</userinput>.

<informalexample><screen>

 $ /usr/sbin/debootstrap --arch ARCH sarge \
     /mnt/debinst http://http.us.debian.org/debian

</screen></informalexample>

</para>

  </sect2>

  <sect2>
  <title>Configure The Base System</title>

<para>

Now you've got a real Debian system, though rather lean, on disk.
<command>Chroot</command> into it:

<informalexample><screen>

 $ chroot /mnt/debinst /bin/bash

</screen></informalexample>
</para>

   <sect3>
   <title>Mount Partitions</title>
<para>

You need to create <filename>/etc/fstab</filename>.

<informalexample><screen>

 # editor /etc/fstab

</screen></informalexample>

</para><para>

Here is a sample you can modify to suit:

<informalexample><screen>

# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump pass
/dev/XXX         /             ext2    defaults                 0    0
/dev/XXX         /boot         ext2    ro,nosuid,nodev          0    2

/dev/XXX         none          swap    sw                       0    0
proc             /proc         proc    defaults                 0    0

/dev/fd0         /mnt/floppy   auto    noauto,rw,sync,user,exec 0    0
/dev/cdrom       /mnt/cdrom    iso9660 noauto,ro,user,exec      0    0

/dev/XXX         /tmp          ext2    rw,nosuid,nodev          0    2
/dev/XXX         /var          ext2    rw,nosuid,nodev          0    2
/dev/XXX         /usr          ext2    rw,nodev                 0    2
/dev/XXX         /home         ext2    rw,nosuid,nodev          0    2

</screen></informalexample>

</para><para>

Use <userinput>mount -a</userinput> to mount all the file systems you
have specified in your <filename>/etc/fstab</filename>, or to mount
file systems individually use:

<informalexample><screen>

 # mount /path  # e.g.:  mount /usr

</screen></informalexample>

</para><para>

You can mount the proc file system multiple times and to arbitrary
locations, though /proc is customary. If you didn't use
<userinput>mount -a</userinput>, be sure to mount proc before
continuing:

<informalexample><screen>

 # mount -t proc proc /proc

</screen></informalexample>

</para>

   </sect3>

   <sect3>
   <title>Configure Keyboard</title>

<para>

To configure your keyboard:

<informalexample><screen>

 # dpkg-reconfigure console-data

</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Configure Networking</title>
<para>

To configure networking, edit
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>, and
<filename>/etc/hostname</filename>.

<informalexample><screen>

 # editor /etc/network/interfaces 

</screen></informalexample>

</para><para>

Here are some simple examples from
<filename>/usr/share/doc/ifupdown/examples</filename>:

<informalexample><screen>
######################################################################
# /etc/network/interfaces -- configuration file for ifup(8), ifdown(8)
# See the interfaces(5) manpage for information on what options are 
# available.
######################################################################

# We always want the loopback interface.
#
auto lo
iface lo inet loopback

# To use dhcp:
#
# auto eth0
# iface eth0 inet dhcp

# An example static IP setup: (broadcast and gateway are optional)
#
# auto eth0
# iface eth0 inet static
#     address 192.168.0.42
#     network 192.168.0.0
#     netmask 255.255.255.0
#     broadcast 192.168.0.255
#     gateway 192.168.0.1
</screen></informalexample>

</para><para>

Enter your nameserver(s) and search directives in
<filename>/etc/resolv.conf</filename>:

<informalexample><screen>

 # editor /etc/resolv.conf

</screen></informalexample>

</para><para>

A simple <filename>/etc/resolv.conf</filename>:

<informalexample><screen>

# search hqdom.local\000
# nameserver 10.1.1.36
# nameserver 192.168.9.100

</screen></informalexample>

</para><para>

Enter your system's host name (2 to 63 characters):

<informalexample><screen>

 # echo DebianHostName &#62; /etc/hostname

</screen></informalexample>

</para><para>

If you have multiple network cards, you should arrange the names of
driver modules in the <filename>/etc/modules</filename> file into the
desired order. Then during boot, each card will be associated with the
interface name (eth0, eth1, etc.) that you expect.

</para>
   </sect3>

   <sect3>
   <title>Configure Timezone, Users, and APT</title>

<para>

Set your timezone, add a normal user, and choose your <command>apt</command>
sources by running

<informalexample><screen>

 # /usr/sbin/base-config new

</screen></informalexample>
</para>
   </sect3>

   <sect3>
   <title>Configure Locales</title>
<para>

To configure your locale settings to use a language other than
English, install the locales support package and configure it:

<informalexample><screen>

 # apt-get install locales
 # dpkg-reconfigure locales

</screen></informalexample>

NOTE: Apt must be configured before, ie. during the base-config phase.
Before using locales with character sets other than ASCII or latin1,
please consult the appropriate localisation HOWTO.

</para>
   </sect3>
  </sect2>

  <sect2>
  <title>Install a Kernel</title>

<para>

If you intend to boot this system, you probably want a Linux kernel
and a boot loader. Identify available pre-packaged kernels with

<informalexample><screen>

 # apt-cache search kernel-image

</screen></informalexample>

</para><para>

Then install your choice using its package name.

<informalexample><screen>

 # apt-get install kernel-image-2.X.X-arch-etc

</screen></informalexample>

</para>
  </sect2>

  <sect2>
<title>Set up the Boot Loader</title>
<para>

To make your &debian; system bootable, set up your boot loader to load
the installed kernel with your new root partition. Note that debootstrap
does not install a boot loader, though you can use apt-get inside your
Debian chroot to do so.

</para><para arch="x86">

Check <userinput>info grub</userinput> or <userinput>man
lilo.conf</userinput> for instructions on setting up the
bootloader.  If you are keeping the system you used to install Debian, just
add an entry for the Debian install to your existing grub
<filename>menu.lst</filename> or <filename>lilo.conf</filename>.  For
<filename>lilo.conf</filename>, you could also copy it to the new system and
edit it there. After you are done editing, call lilo (remember it will use
<filename>lilo.conf</filename> relative to the system you call it from).

</para><para arch="x86">

Here is a basic <filename>/etc/lilo.conf</filename> as an example:

<informalexample><screen>

boot=/dev/hda6
root=/dev/hda6
install=/boot/boot-menu.b
delay=20
lba32
image=/vmlinuz
label=Debian

</screen></informalexample>

</para><para arch="powerpc">

Check <userinput>man yaboot.conf</userinput> for instructions on
setting up the bootloader.  If you are keeping the system you used to
install Debian, just add an entry for the Debian install to your
existing <filename>yaboot.conf</filename>.  You could also copy it to
the new system and
edit it there. After you are done editing, call ybin (remember it will
use <filename>yaboot.conf</filename> relative to the system you call it from).

</para><para arch="powerpc">

Here is a basic <filename>/etc/yaboot.conf</filename> as an example:

<informalexample><screen>

boot=/dev/hda2
device=hd:
partition=6
root=/dev/hda6
magicboot=/usr/lib/yaboot/ofboot
timeout=50
image=/vmlinux
label=Debian

</screen></informalexample>

On some machines, you may need to use <userinput>ide0:</userinput> 
instead of <userinput>hd:</userinput>. 

</para>
  </sect2>
 </sect1>
