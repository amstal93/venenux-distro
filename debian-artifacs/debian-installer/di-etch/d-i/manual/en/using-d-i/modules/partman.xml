<!-- retain these comments for translator revision tracking -->
<!-- $Id: partman.xml 45238 2007-02-18 19:31:55Z fjp $ -->

   <sect3 id="partman">
   <title>Partitioning Your Disks</title>

<para>

Now it is time to partition your disks. If you are uncomfortable with
partitioning, or just want to know more details, see <xref
linkend="partitioning"/>.

</para>
<warning arch="sparc"><para>
<!-- BTS: #384653 -->

If a hard disk has previously used under Solaris, the partitioner may not
detect the size of the drive correctly. Creating a new partition table
does not fix this issue. What does help, is to <quote>zero</quote> the
first few sectors of the drive:

<informalexample><screen>
# dd if=/dev/zero of=/dev/hd<replaceable>X</replaceable> bs=512 count=2; sync
</screen></informalexample>

Note that this will make any existing data on that disk inaccessible.

</para></warning>
<para>

First you will be given the opportunity to automatically partition
either an entire drive, or available free space on a drive. This is
also called <quote>guided</quote> partitioning. If you do not want to
autopartition, choose <guimenuitem>Manual</guimenuitem> from the menu.

</para><para>

If you choose guided partitioning, you may have three options: to create
partitions directly on the hard disk (classic method), or to create them
using Logical Volume Management (LVM), or to create them using encrypted
LVM<footnote>

<para>
The installer will encrypt the LVM volume group using a 256 bit AES key and
makes use of the kernel's <quote>dm-crypt</quote> support.
</para>

</footnote>.

</para>
<note><para>

The option to use (encrypted) LVM may not be available on all architectures.

</para></note>
<para>

When using LVM or encrypted LVM, the installer will create most
partitions inside one big partition; the advantage of this method is that
partitions inside this big partition can be resized relatively easily later.
In the case of encrypted LVM the big partition will not be readable without
knowing a special key phrase, thus providing extra security of your
(personal) data.

</para><para>

When using encrypted LVM, the installer will also automatically erase the
disk by writing random data to it. This further improves security (as it
makes it impossible to tell which parts of the disk are in use and also
makes sure that any traces of previous installations are erased), but may
take some time depending on the size of your disk.

</para>
<note><para>

If you choose guided partitioning using LVM or encrypted LVM, some changes
in the partition table will need to be written to the selected disk while
LVM is being set up. These changes effectively erase all data that is
currently on the selected hard disk and you will not be able to undo them
later. However, the installer will ask you to confirm these changes before
they are written to disk.

</para></note>
<para>

If you choose guided partitioning (either classic or using (encrypted)
LVM) for a whole disk, you will first be asked to select the disk you want
to use. Check that all your disks are listed and, if you have several disks,
make sure you select the correct one. The order they are listed in may
differ from what you are used to. The size of the disks may help to identify
them.

</para><para>

Any data on the disk you select will eventually be lost, but you will always
be asked to confirm any changes before they are written to the disk.
If you have selected the classic method of partitioning, you will be able to
undo any changes right until the end; when using (encrypted) LVM this is not
possible.

</para><para>

Next, you will be able to choose from the schemes listed in the table below.
All schemes have their pros and cons,
some of which are discussed in <xref linkend="partitioning"/>. If you are
unsure, choose the first one. Bear in mind that guided partitioning
needs a certain minimal amount of free space to operate with. If you don't
give it at least about 1GB of space (depends on chosen scheme), guided
partitioning will fail.

</para>

<informaltable>
<tgroup cols="3">
<thead>
<row>
  <entry>Partitioning scheme</entry>
  <entry>Minimum space</entry>
  <entry>Created partitions</entry>
</row>
</thead>

<tbody>
<row>
  <entry>All files in one partition</entry>
  <entry>600MB</entry>
  <entry><filename>/</filename>, swap</entry>
</row><row>
  <entry>Separate /home partition</entry>
  <entry>500MB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>, swap
  </entry>
</row><row>
  <entry>Separate /home, /usr, /var and /tmp partitions</entry>
  <entry>1GB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>,
    <filename>/usr</filename>, <filename>/var</filename>,
    <filename>/tmp</filename>, swap
  </entry>
</row>

</tbody></tgroup></informaltable>

<para>

If you choose guided partitioning using (encrypted) LVM, the installer will also create
a separate <filename>/boot</filename> partition. The other partitions,
including the swap partition, will be created inside the LVM partition.

</para><para arch="ia64">

If you choose guided partitioning for your IA-64 system, there
will be an additional partition, formatted as a FAT16 bootable filesystem,
for the EFI boot loader.
There is also an additional menu item in the formatting menu to manually
set up a partition as an EFI boot partition.

</para><para arch="alpha">

If you choose guided partitioning for your Alpha system, an
additional, unformatted partition will be allocated at the beginning of
your disk to reserve this space for the aboot boot loader.

</para><para>

After selecting a scheme, the next screen will show your new partition
table, including information on whether and how partitions will be
formatted and where they will be mounted.

</para><para>

The list of partitions might look like this:

<informalexample><screen>
  IDE1 master (hda) - 6.4 GB WDC AC36400L
        #1 primary   16.4 MB  B f ext2       /boot
        #2 primary  551.0 MB      swap       swap
        #3 primary    5.8 GB      ntfs
           pri/log    8.2 MB      FREE SPACE

  IDE1 slave (hdb) - 80.0 GB ST380021A
        #1 primary   15.9 MB      ext3
        #2 primary  996.0 MB      fat16
        #3 primary    3.9 GB      xfs        /home
        #5 logical    6.0 GB    f ext3       /
        #6 logical    1.0 GB    f ext3       /var
        #7 logical  498.8 MB      ext3
        #8 logical  551.5 MB      swap       swap
        #9 logical   65.8 GB      ext2
</screen></informalexample>

This example shows two IDE harddrives divided into several partitions;
the first disk has some free space. Each partition line consists of the
partition number, its type, size, optional flags, file system, and
mountpoint (if any). Note: this particular setup cannot be created using
guided partitioning but it does show possible variation that can be achieved
using manual partitioning.

</para><para>

This concludes the guided partitioning. If you are satisfied with the
generated partition table, you can choose <guimenuitem>Finish
partitioning and write changes to disk</guimenuitem> from the menu to
implement the new partition table (as described at the end of this
section). If you are not happy, you can choose to <guimenuitem>Undo
changes to partitions</guimenuitem> and run guided partitioning again, or
modify the proposed changes as described below for manual partitioning.

</para><para>

A similar screen to the one shown just above will be displayed if you
choose manual partitioning except that your existing partition table will
be shown and without the mount points. How to manually setup your partition
table and the usage of partitions by your new Debian system will be covered
in the remainder of this section.

</para><para>

If you select a pristine disk which has neither partitions nor free
space on it, you will be asked if a new partition table should be
created (this is needed so you can create new partitions). After this,
a new line entitled <quote>FREE SPACE</quote> should appear in the table
under the selected disk.

</para><para>

If you select some free space, you will have the opportunity to create a
new partition. You will have to answer a quick series of questions about
its size, type (primary or logical), and location (beginning or end of
the free space). After this, you will be presented with a detailed
overview of your new partition. The main setting is <guimenuitem>Use
as:</guimenuitem>, which determines if the partition will have a file
system on it, or be used for swap, software RAID, LVM, an encrypted
file system, or not be used at all. Other settings include
mountpoint, mount options, and bootable flag; which settings are shown
depends on how the partition is to be used. If you don't like the
preselected defaults, feel free to change them to your liking. E.g. by
selecting the option <guimenuitem>Use as:</guimenuitem>, you can
choose a different filesystem for this partition, including options
to use the partition for swap, software RAID, LVM, or not
use it at all. Another nice feature is the ability to copy data from
an existing partition onto this one.
When you are satisfied with your new partition, select
<guimenuitem>Done setting up the partition</guimenuitem> and you will
return to <command>partman</command>'s main screen.

</para><para>

If you decide you want to change something about your partition,
simply select the partition, which will bring you to the partition
configuration menu. This is the same screen as is used when creating
a new partition, so you can change the same settings. One thing
that may not be very obvious at a first glance is that you can
resize the partition by selecting the item displaying the size of the
partition. Filesystems known to work are at least fat16, fat32, ext2,
ext3 and swap. This menu also allows you to delete a partition.

</para><para>

Be sure to create at least two partitions: one for the
<emphasis>root</emphasis> filesystem (which must be mounted as
<filename>/</filename>) and one for <emphasis>swap</emphasis>. If you
forget to mount the root filesystem, <command>partman</command> won't
let you continue until you correct this issue.

</para><para arch="ia64">

If you forget to select and format an EFI boot partition,
<command>partman</command> will detect this and will not let you continue
until you allocate one.

</para><para>

Capabilities of <command>partman</command> can be extended with installer
modules, but are dependent on your system's architecture. So if you can't
see all promised goodies, check if you have loaded all required modules
(e.g. <filename>partman-ext3</filename>, <filename>partman-xfs</filename>,
or <filename>partman-lvm</filename>).

</para><para>

After you are satisfied with partitioning, select <guimenuitem>Finish
partitioning and write changes to disk</guimenuitem> from the partitioning
menu. You will be presented with a summary of changes made to the disks
and asked to confirm that the filesystems should be created as requested.

</para>
   </sect3>
