<!-- retain these comments for translator revision tracking -->
<!-- $Id: localechooser.xml 33725 2006-01-03 19:39:07Z fjp $ -->


   <sect3 id="localechooser">
   <title>Selecting Localization Options</title>

<para>

In most cases the first questions you will be asked concern the selection
of localization options to be used both for the installation and for the
installed system. The localization options consist of language, country
and locales.

</para><para>

The language you choose will be used for the rest of the installation
process, provided a translation of the different dialogs is available.
If no valid translation is available for the selected language, the
installer will default to English.

</para><para>

The selected country will be used later in the installation process to
pick the default timezone and a Debian mirror appropriate for your
geographic location. Language and country together will be used to set
the default locale for your system and to help select your keyboard.

</para><para>

You will first be asked to select your preferred language. The language
names are listed in both English (left side) and in the language itself
(right side); the names on the right side are also shown in the proper
script for the language. The list is sorted on the English names.
At the top of the list is an extra option that allows you to select the
<quote>C</quote> locale instead of a language. Choosing the <quote>C</quote>
locale will result in the installation proceding in English; the installed
system will have no localization support as the <classname>locales</classname>
package will not be installed.

</para><para>

If you selected a language that is recognized as an official language for
more than one country<footnote>

<para>

In technical terms: where multiple locales exist for that language with
differing country codes.

</para>

</footnote>, you will next be asked to select a country.
If you choose <guimenuitem>Other</guimenuitem> at the bottom of the list,
you will be presented with a list of all countries, grouped by continent.
If the language has only one country associated with it, that country
will be selected automatically.

</para><para>

A default locale will be selected based on the selected language and country.
If you are installing at medium or low priority, you will have the option
of selecting a different default locale and of selecting additional locales to
be generated for the installed system.

</para>
   </sect3>
