<!-- retain these comments for translator revision tracking -->
<!-- original version: 43558 -->
<!-- revised by Herbert P Fortes Neto (hpfn) 2006.05.02 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.12.27 -->

  <sect2 condition="supports-bootp" id="tftp-bootp">
  <title>Configurando um servidor BOOTP</title>
<para>

Existem dois servidores BOOTP disponíveis para o GNU/Linux. O primeiro é o
CMU <command>bootpd</command>. O outro é, na verdade, um servidor DHCP: ISC
<command>dhcpd</command>. No &debian; eles estão disponíveis nos pacotes
<classname>bootp</classname> e <classname>dhcp3-server</classname>
respectivamente.

</para><para>

Para usar o CMU <command>bootpd</command> você deverá primeiro
descomentar (ou adicionar) a linha relevante em
<filename>/etc/inetd.conf</filename>.
No &debian;, você poderá executar <userinput>update-inetd --enable
bootps</userinput> então o comando <userinput>/etc/init.d/inetd
reload</userinput> para fazer isto. Apenas para o caso de seu servidor BOOTP
não estar <quote>rodando</quote> Debian, a linha em questão deveria se parecer
com:

<informalexample><screen>
bootps  dgram  udp  wait  root  /usr/sbin/bootpd  bootpd -i -t 120
</screen></informalexample>

Agora, você deverá criar um arquivo <filename>/etc/bootptab</filename>.
Este terá a mesma quantidade de formato críptico e familiar como o
bom e antigo <filename>printcap</filename> do BSD,
<filename>termcap</filename>, e
<filename>disktab</filename>. Veja a página de manual do
<filename>bootptab</filename> para mais informações. Para o
CMU <command>bootpd</command> você precisará conhecer o endereço de
hardware (MAC) do cliente. Aqui está um exemplo de arquivo
<filename>/etc/bootptab</filename>:

<informalexample><screen>
client:\
  hd=/tftpboot:\
  bf=tftpboot.img:\
  ip=192.168.1.90:\
  sm=255.255.255.0:\
  sa=192.168.1.1:\
  ha=0123456789AB:
</screen></informalexample>

Você pelo menos precisará mudar a opção <quote>ha</quote>, que especifica o
endereço de hardware do cliente. A opção <quote>bf</quote> especifica o arquivo
que o cliente deverá baixar via TFTP; veja 
<xref linkend="tftp-images"/> para mais detalhes.

<phrase arch="mips">
Nas máquinas SGI você poderá apenas apertar enter no monitor de comandos
e digitar <userinput>printenv</userinput>. O valor da variável
<userinput>eaddr</userinput> é o endereço MAC da máquina.
</phrase>

</para><para>

Em contraste, a configuração de um BOOTP com o ISC <command>dhcpd</command>
é realmente fácil, por causa que ele trata clientes BOOTP de uma
forma especial como clientes DHCP. Algumas arquiteturas requerem uma
configuração complexa para a inicialização dos clientes via
BOOTP. Caso a sua seja uma destas, leia a seção <xref linkend="dhcpd"/>.
Neste caso, você será provavelmente capaz de adicionar a diretiva
<userinput>allow bootp</userinput> no bloco de configuração de sub-rede
de seu cliente e reiniciar o <command>dhcpd</command> com o comando
<userinput>/etc/init.d/dhcpd3-server restart</userinput>.

</para>
  </sect2>
