<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45291 -->

<appendix id="installation-howto">
<title>Guide de l'installation</title>
<para>

Ce document d�crit l'installation de &debian; &releasename; pour
&arch-title; (<quote>&architecture;</quote>) avec le nouvel installateur 
Debian.
Il reproduit le processus d'installation et donne des informations valables
pour la plupart des installations. Quand des informations suppl�mentaires sont
n�cessaires, nous renvoyons aux explications plus d�taill�es qui se trouvent
dans d'autres parties de ce document.

</para>
<sect1 id="howto-preliminaries">
<title>Pr�liminaires</title>
<para>

<phrase condition="unofficial-build">
L'installateur Debian est encore en phase b�ta.
</phrase>
Si vous rencontrez des erreurs lors de l'installation, veuillez vous
r�f�rer � <xref linkend="submit-bug" /> pour savoir comment les signaler.
Si ce document ne r�pond pas � vos questions, posez-les sur la liste de 
diffusion debian-boot (&email-debian-boot-list;) ou demandez sur 
IRC (#debian-boot, r�seau OFTC).

</para>
 </sect1>

 <sect1 id="howto-getting-images">
<title>D�marrer l'installateur</title>
<para>

<phrase condition="unofficial-build">
Des informations sur les images sur c�d�rom se trouvent sur la <ulink url="&url-d-i;">
page de l'installateur Debian</ulink>.
</phrase>
L'�quipe debian-cd fournit des images sur c�d�rom contenant l'installateur sur la 
<ulink url="&url-debian-cd;">page Debian CD</ulink>.
Pour savoir o� trouver des c�d�roms, lisez <xref linkend="official-cdrom" />.

</para><para>

Certaines m�thodes d'installation demandent des images diff�rentes des
images sur c�d�rom.
<phrase condition="unofficial-build">
La <ulink url="&url-d-i;">page de l'installateur</ulink> offre des liens vers 
ces images.
</phrase>
La <xref linkend="where-files" /> explique comment trouver des images sur les
miroirs Debian.

</para>
<para>

Les sections suivantes donnent des pr�cisions sur les images n�cessaires
pour chaque type d'installation.

</para>
  <sect2 id="howto-getting-images-cdrom">
<title>C�d�rom</title>
<para>

Pour installer &releasename; avec l'installateur, il existe deux sortes 
d'image, appel�es images <quote>netinst</quote>. Ces images s'amorcent � partir du c�d�rom
et installent les paquets en les r�cup�rant sur le r�seau, d'o� le nom,
<quote>netinst</quote>. La diff�rence entre ces images est que, sur l'image
compl�te, sont inclus les paquets du syst�me de base, tandis que sur l'image
<quote>business card</quote> ces paquets sont r�cup�r�s sur le web. Si vous pr�f�rez,
vous pouvez obtenir une image compl�te qui n'a pas besoin du r�seau pour
l'installation. Seul le premier c�d�rom de cet ensemble est n�cessaire.

</para><para>

T�l�chargez l'image que vous pr�f�rez et gravez-la sur un c�d�rom.
<phrase arch="x86">Pour amorcer � partir du c�d�rom, vous devrez sans doute
changer la configuration du BIOS&nbsp;; c'est expliqu� dans la
<xref linkend="bios-setup" />.</phrase>
<phrase arch="powerpc">
Pour amorcer un PowerMac � partir du c�d�rom, appuyez sur la touche 
<keycap>c</keycap> lors du d�marrage. Voyez la
<xref linkend="boot-cd" /> pour d'autres fa�ons de faire.
</phrase>

</para>
  </sect2>
  
  <sect2 condition="supports-floppy-boot" id="howto-getting-images-floppy">
<title>Disquettes</title>
<para>

Si vous ne pouvez pas amorcer le syst�me � partir d'un c�d�rom, vous pouvez
t�l�charger des images sur disquette. Vous avez besoin de
<filename>floppy/boot.img</filename>, <filename>floppy/root.img</filename> et
peut-�tre d'une ou de plusieurs disquettes de pilotes.

</para><para>

La disquette d'amor�age est celle qui contient <filename>boot.img</filename>.
Lors de l'amor�age, cette disquette demandera l'insertion de la seconde
disquette, celle qui contient <filename>root.img</filename>.

</para><para>

Si vous pr�voyez d'utiliser le r�seau pour faire l'installation, vous aurez
besoin d'une disquette qui contient <filename>floppy/net-drivers-1.img</filename>.
Pour des cartes r�seau moins communes, la gestion des cartes PCMCIA ou la gestion du r�seau
par USB, vous aurez besoin d'une deuxi�me disquette,
<filename>floppy/net-drivers-2.img</filename>. 

</para><para>

Si vous poss�dez un c�d�rom que vous ne pouvez utiliser pour l'amor�age,
amorcez � partir des disquettes et prenez 
<filename>floppy/cd-drivers.img</filename> sur une disquette de pilotes pour
terminer l'installation avec votre c�d�rom.

</para><para>

Les disquettes font partie des supports les moins fiables. Attendez-vous �
trouver beaucoup de disquettes d�fectueuses 
(lisez la <xref linkend="unreliable-floppies" />). Chaque fichier 
<filename>.img</filename> t�l�charg� doit �tre plac� sur une seule disquette.
Vous pouvez utiliser la commande dd pour �crire sur /dev/fd0, ou tout
autre moyen, voyez la <xref linkend="create-floppy" />.
Puisque vous avez plusieurs disquettes, c'est une bonne id�e de les
�tiqueter.

</para>
  </sect2>
  
  <sect2 condition="bootable-usb" id="howto-getting-images-usb">
<title>Cl�s USB</title>
<para>

On peut aussi amorcer � partir d'un support USB amovible. Par exemple, une cl� 
USB, facilement transportable, peut servir � installer Debian.

</para><para>

Pour pr�parer la cl� USB, la m�thode la plus simple est de t�l�charger le fichier
<filename>hd-media/boot.img.gz</filename>, et d'extraire avec gunzip l'image
de 256&nbsp;Mo. Transf�rez ensuite cette image sur la cl�, dont la taille doit 
�videmment �tre au moins �gale � 256&nbsp;Mo. Bien s�r, cela d�truira toutes les 
donn�es pr�sentes sur la cl�. Puis montez la cl�, avec un syst�me de fichiers 
FAT. T�l�chargez maintenant une image de type <quote>netinst</quote> et copiez ce fichier, 
dont le nom doit se terminer par <literal>.iso</literal>, sur la cl�.

</para><para>

Il y a des moyens plus souples de mettre en oeuvre une cl� USB et
l'installateur Debian peut fonctionner avec des cl�s qui poss�dent moins de
m�moire. Pour des pr�cisions, voyez la <xref linkend="boot-usb-files" />.

</para><para arch="x86">

Certains BIOS peuvent lancer directement des p�riph�riques USB, d'autres
non. Il vous faudra configurer le BIOS et permettre l'amor�age d'un
<quote>disque amovible</quote> ou d'un <quote>USB-ZIP</quote>. Pour des pr�cisions,
voyez la <xref linkend="usb-boot" />.

</para><para arch="powerpc">

Pour amorcer les syst�mes Macintosh sur des p�riph�riques USB, il faut
manipuler le microprogramme Open Firmware. Pour des indications, voyez
<xref linkend="usb-boot" />.

</para>
  </sect2>
  
  <sect2 id="howto-getting-images-netboot">
<title>Amorcer sur le r�seau</title>
<para>
	
Il est aussi possible de lancer l'installateur Debian � partir du r�seau. 
Les diff�rentes m�thodes pour cet amor�age r�seau d�pendent de l'architecture 
et de la configuration. Les fichiers dans <filename>netboot/</filename> servent
� l'amor�age de l'installateur.

</para><para arch="x86">

La m�thode la plus facile est l'amor�age de type PXE. Extrayez les fichiers
de <filename>netboot/pxeboot.tar.gz</filename> dans le r�pertoire
<filename>/var/lib/tftpboot</filename> ou dans le r�pertoire que demande
le serveur tftp. Et configurez le serveur DHCP pour qu'il passe le
nom <filename>/pxelinux.0</filename> aux clients. Avec un peu de chance,
tout marchera bien.
Pour des instructions pr�cises, voyez la <xref linkend="install-tftp" />.

</para>
  </sect2>
  
  <sect2 id="howto-getting-images-hard-disk">
<title>Amorcer depuis un disque dur</title>
<para>


Il est possible de faire une installation sans utiliser de support
amovible, par exemple avec un disque dur poss�dant d�j� un autre syst�me
d'exploitation. T�l�chargez <filename>hd-media/initrd.gz</filename>,
<filename>hd-media/vmlinuz</filename> et une image CD dans le r�pertoire
racine du disque. Assurez-vous que le nom de l'image CD se termine par
<literal>.iso</literal>. Maintenant, il suffit d'amorcer Linux avec initrd.
<phrase arch="x86">
La <xref linkend="boot-initrd" /> explique comment faire.
</phrase>

</para>
  </sect2>
</sect1>
 
<sect1 id="howto-installation">
<title>Installation</title>
<para>

Apr�s le d�marrage de l'installateur, l'�cran initial est affich�.
Appuyez sur la touche &enterkey; pour amorcer, ou bien lisez les instructions 
sur les autres m�thodes d'amor�age et sur les param�tres (voyez la
<xref linkend="boot-parms" />).

</para>
<para>

On vous demandera ensuite de choisir une langue. D�placez-vous avec les fl�ches
et appuyez sur la touche &enterkey; quand vous avez choisi. On vous demandera
un pays, � choisir parmi les pays o� cette langue est parl�e. Si votre pays
n'est pas dans la premi�re liste, une liste contenant tous les pays est
disponible.

</para>
<para>

On vous demandera de confirmer la carte clavier. Choisissez la carte par
d�faut, sauf si vous en connaissez une meilleure.

</para>
<para>

Patientez maintenant, tandis que l'installateur d�tecte le mat�riel et
charge ses composants.
</para>
<para>

Ensuite l'installateur recherche le mat�riel r�seau et configure le r�seau
avec DHCP. Si vous n'�tes pas sur un r�seau ou si vous n'utilisez pas DHCP,
vous aurez la possibilit� de configurer vous-m�me le r�seau.

</para>
<para>

Il est temps maintenant de partitionner les disques. Vous aurez d'abord la
possibilit� de partitionner automatiquement soit un disque entier soit l'espace
libre d'un disque (partitionnement assist�). C'est la m�thode recommand� pour les d�butants
ou pour les gens press�s. Si vous ne voulez pas du partitionnement automatique, choisissez
<guimenuitem>Manuel</guimenuitem> dans le menu.
</para>

<para arch="x86">

Si vous voulez pr�server une partition DOS ou Windows, faites attention en
utilisant le partitionnement automatique. Si vous utilisez le partitionnement
automatique, vous pouvez redimensionner une
partition FAT ou NTFS avec l'installateur pour faire de la place � 
Debian&nbsp;: choisissez simplement la partition et indiquez la taille
voulue.
</para>

<para>

L'�cran suivant montre la table des partitions, avec les syst�mes de fichiers
et les points de montage. Choisissez une partition que vous voulez modifier
ou supprimer. Si vous aviez choisi le partitionnement automatique, il vous 
suffira de s�lectionner
<guimenuitem>Terminer le partitionnement et appliquer les changements</guimenuitem> dans le
menu pour accepter ce qui a �t� fait. N'oubliez pas qu'il vous faut au moins 
une partition d'�change et une partition racine mont�e sur
<filename>/</filename>. D'autres informations sur le partitionnement se
trouvent dans l'<xref linkend="partitioning"/>.

</para>
<para>

L'installateur formate les partitions et installe le syst�me de base, 
ce qui peut prendre du temps. Puis le noyau est install�.

</para>

<para>
Ensuite l'horloge et le fuseau horaire sont d�finis. L'installateur essaiera
de choisir automatiquement les bons r�glages et ne posera une question qu'en cas de
doute. Puis les comptes des utilisateurs sont cr��s. Par d�faut vous devez fournir
un mot de passe pour le compte du superutilisateur (<quote>root</quote>) et vous
devez cr�er un compte d'utilisateur ordinaire.
</para>

<para>

Le syst�me de base install� est un syst�me fonctionnel mais limit�. Pour le rendre
plus efficace, vous pouvez maintenant installer d'autres paquets en choisissant
des t�ches. Il faut aussi configurer <classname>apt</classname> et indiquer d'o� les
paquets seront t�l�charg�s.
La t�che <quote>Standard system</quote> est s�lectionn�e par d�faut et doit �tre
install�e. Si vous voulez un environnement de bureau graphique, s�lectionnez
la t�che <quote>Desktop environment</quote>. Consultez <xref linkend="pkgsel"/>
pour des informations suppl�mentaires sur cette �tape.

</para>  

<para>

La derni�re �tape est l'installation d'un programme d'amor�age. Si
l'installateur a d�tect� d'autres syst�mes d'exploitation sur la machine,
il les ajoutera au menu du programme d'amor�age.
<phrase arch="x86">GRUB est par d�faut install� sur le secteur principal 
d'amor�age du premier disque dur, ce qui est une bonne id�e. Mais vous avez la 
possibilit� d'annuler ce choix et de l'installer ailleurs.
</phrase>

</para>
<para>

L'installateur annonce maintenant que l'installation est termin�e. Retirez le 
c�d�rom ou le support que vous avez utilis� et appuyez sur la touche &enterkey; pour 
r�amorcer la machine.
Vous devriez pouvoir maintenant vous connecter au nouveau syst�me. Cette �tape est
expliqu�e dans le <xref linkend="boot-new" />.

</para>
<para>

Si vous avez besoin d'autres informations sur ce processus d'installation,
voyez le <xref linkend="d-i-intro" />.

</para>
 </sect1>
 
<sect1 id="howto-installation-report">
<title>Envoyez-nous un rapport d'installation</title>
<para>

Si l'installateur Debian a install� correctement le syst�me, veuillez prendre
le temps de nous envoyer un rapport. 
Vous pouvez simplement installer le paquet reportbug 
(<command>aptitude install reportbug</command>), le configurer comme
nous l'avons expliqu� dans <xref linkend="mail-outgoing"/>, et ex�cuter la commande
<command>reportbug installation-reports</command>.

</para><para>

Si vous n'avez pas pu terminer l'installation, vous avez sans doute trouv� un bogue 
dans l'installateur.
Il est n�cessaire que nous le connaissions pour am�liorer l'installateur.
Veuillez prendre le temps de nous le signaler. Vous pouvez utiliser le mod�le
pr�c�dent. Si l'installateur a compl�tement �chou�, voyez la
<xref linkend="problem-report" />.

</para>
 </sect1>

 <sect1 id="howto-installation-finally">
<title>Et ils eurent&hellip;</title>
<para>

Nous esp�rons que votre syst�me Debian vous plaira et qu'il sera utile.
Vous pouvez maintenant lire le <xref linkend="post-install" />.

</para>
 </sect1>
</appendix>
