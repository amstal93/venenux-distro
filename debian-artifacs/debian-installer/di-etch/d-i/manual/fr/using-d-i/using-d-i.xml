<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44026 -->

<chapter id="d-i-intro"><title>Comment utiliser l'installateur Debian ?</title>

 <sect1><title>Comment fonctionne l'installateur ?</title>
<para>

L'installateur Debian comprend plusieurs composants qui ont chacun une t�che 
pr�cise � accomplir. Chaque composant pose � l'utilisateur les questions
n�cessaires � l'ex�cution de sa t�che. Chaque question poss�de une priorit� et 
cette priorit� est d�clar�e au d�marrage de l'installateur.
</para>
<para>
Pour une installation standard, seules les questions essentielles (priorit�
<emphasis>high</emphasis>) sont pos�es. Ainsi l'installation est grandement
automatis�e, avec peu d'interventions de l'utilisateur. Les composants sont
choisis et ex�cut�s automatiquement selon le mat�riel et selon le type 
d'installation demand�e. L'installateur utilise des valeurs par d�faut pour les
questions qui ne sont pas pos�es.
</para>

<para>
Quand survient un probl�me, l'utilisateur voit un message d'erreur et le menu 
de l'installateur peut s'afficher pour permettre le choix d'une autre action. 
Quand il n'y a pas de probl�me, l'utilisateur ne voit jamais le menu de 
l'installateur. Il r�pond simplement aux questions qui lui sont pos�es par
chaque composant. Les messages concernant des erreurs s�rieuses ont une 
priorit� <emphasis>critical</emphasis> pour que l'utilisateur les re�oive toujours.
</para>
<para>
On peut modifier les valeurs par d�faut utilis�es par l'installateur en passant
des param�tres d'amor�age au d�marrage de l'installateur. Si, par exemple, vous
pr�f�rez configurer vous-m�me le r�seau &mdash; DHCP est utilis� quand c'est 
possible &mdash;, vous pouvez ajouter le param�tre d'amor�age
<userinput>netcfg/disable_dhcp=true</userinput>.
Les param�tres disponibles sont donn�s dans la <xref linkend="installer-args"/>. 

</para><para>

Certains utilisateurs pr�f�rent une interface dot�e d'un menu&nbsp;; ils
veulent contr�ler chaque �tape et ne pas laisser l'installateur agir
automatiquement. Pour utiliser l'installateur dans ce mode manuel, il faut
ajouter le param�tre de d�marrage <userinput>priority=medium</userinput>.

</para>
<para>
Si, quand ils sont install�s, vous devez passer des param�tres aux modules du 
noyau, vous devrez ex�cuter l'installateur en mode <emphasis>expert</emphasis>.
Vous pouvez le faire en utilisant la commande <command>expert</command> au
d�marrage de l'installateur, ou en ajoutant le param�tre d'amor�age
<userinput>priority=low</userinput>.
Le mode expert permet de contr�ler compl�tement l'installateur.
</para> 

<para>

L'affichage de l'installateur se fait en mode caract�re, il ne poss�de pas
d'interface graphique. La souris ne fonctionne pas dans cet environnement.
Voici les touches qui servent � naviguer dans les diff�rents menus.
La fl�che <keycap>droite</keycap> ou la touche <keycap>Tab</keycap> 
servent � avancer dans les s�lections ou les boutons affich�s, et la fl�che
<keycap>gauche</keycap> ou la touche 
<keycombo> <keycap>Shift</keycap> <keycap>Tab</keycap></keycombo>, � reculer.
Les fl�ches <keycap>haut</keycap> et <keycap>bas</keycap> s�lectionnent
des �l�ments dans une liste d�roulante, et d�roulent aussi la liste. De plus,
pour les longues listes, taper une lettre d�roulera la liste jusqu'� une
section qui poss�dent des �l�ments commen�ant par cette lettre&nbsp;; vous 
pouvez utiliser les touches <keycap>Pg-Up</keycap> et <keycap>Pg-Down</keycap>
pour parcourir la liste selon les sections. La touche <keycap>Espace</keycap>
s�lectionne les �l�ments de type checkbox. Activez les choix avec
&enterkey;.

</para><para arch="s390">

Les S/390 ne connaissent pas les consoles virtuelles. Vous pouvez ouvrir une 
seconde et une troisi�me session ssh pour visualiser les journaux 
d�crits ci-dessous.

</para><para>

Le journal de l'installation et les messages d'erreur sont redirig�s vers la quatri�me console.
Vous pouvez y acc�der en pressant 
<keycombo><keycap>Left Alt</keycap><keycap>F4</keycap></keycombo> 
(il faut garder la touche <keycap>Alt</keycap> enfonc�e pendant que vous appuyez 
sur la touche de fonction <keycap>F4</keycap>). Revenez sur le processus
d'installation avec 
<keycombo><keycap>Left Alt</keycap><keycap>F1</keycap></keycombo>.

</para><para>

Ces messages se retrouvent aussi dans
<filename>/var/log/syslog</filename>. Apr�s l'installation, ce
journal est copi� dans <filename>/var/log/installer/syslog</filename>
sur votre nouveau syst�me. D'autres messages d'installation se trouvent,
pendant l'installation, dans le fichier
<filename>/var/log/</filename>, et, quand la
machine a d�marr� le nouveau syst�me, ces messages se trouvent dans
<filename>/var/log/installer/</filename>.

</para>
 </sect1>


  <sect1 id="modules-list"><title>Introduction aux composants</title>
<para>

Voici une liste des composants de l'installateur, avec une br�ve description
des buts de chacun d'eux. Des pr�cisions sur leur utilisation se trouvent
dans <xref linkend="module-details"/>.

</para>

<variablelist>
<varlistentry>

<term>Le menu principal</term><listitem><para>

Affiche la liste des composants pendant les op�rations de l'installateur et
lance le composant qui a �t� choisi. Les questions du menu principal ont la
priorit� <emphasis>medium</emphasis> (moyenne), et si la priorit� que vous avez choisie est 
<emphasis>high</emphasis> (haute) ou <emphasis>critical</emphasis> (critique)
(<emphasis>high</emphasis>, par d�faut), vous ne verrez pas le menu. Quand une erreur se 
produit, qui demande votre intervention, la priorit� de la question peut �tre
abaiss�e temporairement pour vous permettre de r�soudre le probl�me, et dans 
ce cas, le menu appara�t.
</para>
<para>
Depuis le composant en cours, vous pouvez revenir au menu principal en 
s�lectionnant plusieurs fois le bouton <quote>Retour</quote>.
</para></listitem>
</varlistentry>

<varlistentry>

<term>localechooser</term><listitem><para>

Ce programme permet � l'utilisateur de choisir des param�tres r�gionaux pour l'installation
et le syst�me � installer : la langue, le pays et les <emphasis>locales</emphasis>.
L'installateur affiche ses messages dans la langue choisie ; si la traduction
des messages dans cette langue n'est pas compl�te, certains messages
seront affich�s en anglais.

</para></listitem>
</varlistentry>
<varlistentry>

<term>kbd-chooser</term><listitem><para>

Ce programme affiche une liste des cartes clavier. 

</para></listitem>
</varlistentry>
<varlistentry>

<term>hw-detect</term><listitem><para>

Ce programme d�tecte automatiquement la plupart des �l�ments mat�riels du syst�me, les
cartes r�seau, les disques, les cartes PCMCIA.
</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-detect</term><listitem><para>

Ce programm recherche et monte les c�d�roms pour l'installation de Debian.

</para></listitem>
</varlistentry>
<varlistentry>

<term>netcfg</term><listitem><para>

Ce programme configure la connexion r�seau pour la communication sur internet.

</para></listitem>
</varlistentry>
<varlistentry>

<term>iso-scan</term><listitem><para>

Ce programme recherche les fichiers ISO, qui se trouvent sur un c�d�rom ou sur un
disque dur.

</para></listitem>
</varlistentry>
<varlistentry>

<term>choose-mirror</term><listitem><para>

Ce programme affiche une liste des miroirs de l'archive Debian. L'utilisateur peut
choisir la source des paquets � installer. 

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-checker</term><listitem><para>

Ce programme v�rifie l'int�grit� des c�d�roms. Ainsi l'utilisateur peut s'assurer que
le c�d�rom d'installation n'est pas corrompu.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lowmem</term><listitem><para>

Lowmem essaie de d�tecter les syst�mes qui ont peu de m�moire et s'arrange
pour supprimer les programmes non indispensables de l'installateur, au prix de
certaines fonctionnalit�s.

</para></listitem>
</varlistentry>
<varlistentry>

<term>anna</term><listitem><para>

<emphasis>Anna's Not Nearly APT</emphasis>. C'est un programme qui installe
les paquets qui ont �t� r�cup�r�s sur le miroir choisi.

</para></listitem>
</varlistentry>
<varlistentry>
<term>partman</term><listitem><para>

Ce programme permet de partitionner les disques du syst�me, de cr�er les
syst�mes de fichiers sur les partitions et de monter ces partitions.
Il poss�de d'autres fonctionnalit�s int�ressantes comme son mode
automatique ou la gestion des volumes logiques (LVM).
C'est le partitionneur principal de Debian.

</para></listitem>
</varlistentry>
<varlistentry>

<term>autopartkit</term><listitem><para>

Ce programme partitionne automatiquement un disque entier suivant des
valeurs pr�alablement donn�es par l'utilisateur.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partitioner</term><listitem><para>

Ce programme permet de partitionner les disques du syst�me.
Un partitionneur appropri� � l'architecture de votre syst�me est choisi.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partconf</term><listitem><para>

Ce programme affiche la liste des partitions et cr�e les syst�mes de fichiers sur les
partitions choisies selon les instructions de l'utilisateur.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lvmcfg</term><listitem><para>
Ce programme aide l'utilisateur dans sa configuration du
<firstterm>gestionnaire de volumes logiques</firstterm>
<emphasis>(LVM, Logical Volume Manager)</emphasis>.

</para></listitem>
</varlistentry>
<varlistentry>

<term>mdcfg</term><listitem><para>

Ce programme permet la configuration d'un <firstterm>RAID</firstterm> logiciel
<emphasis>(Redundant Array of Inexpensive Disks)</emphasis>. Ce RAID logiciel est
sup�rieur � celui des contr�leurs IDE qu'on trouve sur les cartes m�re r�centes.

</para></listitem>
</varlistentry>
 <varlistentry>

<term>tzsetup</term><listitem><para>

Ce programme permet de choisir le fuseau horaire � partir du lieu indiqu� pr�c�demment.

</para></listitem>
</varlistentry>
<varlistentry>

<term>clock-setup</term><listitem><para>

Ce programme contr�le le r�glage de l'horloge (UTC ou non).

</para></listitem>
</varlistentry>
<varlistentry>

<term>user-setup</term><listitem><para>

Ce programme permet la cr�ation du mot de passe pour le superutilisateur
(<emphasis>root</emphasis>) et l'ajout d'un utilisateur ordinaire.

</para></listitem>
</varlistentry>
<varlistentry>

<term>base-installer</term><listitem><para>

Ce programme installe l'ensemble des programmes de base qui permettront
le fonctionnement de l'ordinateur sous Linux apr�s le red�marrage.

</para></listitem>
</varlistentry>
 <varlistentry>

<term>apt-setup</term><listitem><para>

Ce programme configure apt � partir d'informations tir�es du
support d'installation.

</para></listitem>
</varlistentry>
<varlistentry>

<term>pkgsel</term><listitem><para>

Ce programme utilise le logiciel <classname>tasksel</classname> pour s�lectionner et installer
des logiciels suppl�mentaires.

</para></listitem>
</varlistentry>
<varlistentry>

<term>os-prober</term><listitem><para>

Ce programme d�tecte les syst�mes d'exploitation pr�sents sur le syst�me et
passe cette information au programme bootloader-installer&nbsp;; celui-ci 
vous donne
la possibilit� d'ajouter ces syst�mes au menu des programmes d'amor�age.
Ainsi vous pourrez choisir, au moment du d�marrage, quel syst�me lancer.

</para></listitem>
</varlistentry>
<varlistentry>

<term>bootloader-installer</term><listitem><para>

Ce programme installe un programme d'amor�age sur le disque choisi&nbsp;;
c'est n�cessaire pour d�marrer Linux sans lecteur de disquette ou sans c�d�rom.
Beaucoup de programmes d'amor�age permettent de choisir le syst�me 
d'exploitation que l'on veut lancer au d�marrage de la machine.

</para></listitem>
</varlistentry>
<varlistentry>

<term>shell</term><listitem><para>

Ce programme permet d'ex�cuter un interpr�teur de commandes, � partir du menu 
ou dans la deuxi�me console.

</para></listitem>
</varlistentry>
<varlistentry>

<term>save-logs</term><listitem><para>

Ce programme permet d'enregistrer des informations sur une disquette,
un r�seau, un disque dur, etc. quand quelque chose se passe mal&nbsp;; 
ainsi on peut par la suite envoyer des 
informations pr�cises aux d�veloppeurs Debian.

</para></listitem>
</varlistentry>

</variablelist>

 </sect1>

&using-d-i-components.xml;

</chapter>

