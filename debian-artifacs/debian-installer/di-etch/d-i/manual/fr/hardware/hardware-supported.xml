<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43943 -->

 <sect1 id="hardware-supported">
<title>Mat�riel reconnu</title>

<para>
En ce qui concerne le mat�riel, Debian n'a pas plus d'exigences que le noyau 
Linux et les outils GNU. Par cons�quent, toute architecture ou plateforme, sur
laquelle le noyau Linux, la libc, le compilateur gcc, etc. ont �t� 
port�s, et pour laquelle un portage de Debian existe, peuvent faire fonctionner
Debian. Reportez-vous aux pages sur les portages 
<ulink url="&url-ports;"></ulink> pour plus de pr�cisions concernant les
syst�mes d'architecture &arch-title; qui ont �t� test�s avec Debian.
</para>

<para>
Plut�t que d'essayer de d�crire les diff�rentes configurations
mat�rielles accept�es par &arch-title;, cette
section contient des informations g�n�rales et des pointeurs
vers des informations compl�mentaires.
</para>

  <sect2><title>Architectures reconnues</title>

<para>
Debian &release; fonctionne sur douze architectures
principales et sur de nombreuses variantes de celles-ci,
appel�es <quote>saveurs</quote>.
</para>

<para>
<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>Architecture</entry><entry>�tiquette Debian</entry>
  <entry>Sous-Architecture</entry><entry>Saveur</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Intel x86-based</entry>
  <entry>i386</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry>AMD64 &amp; Intel EM64T</entry>
  <entry>amd64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry>DEC Alpha</entry>
  <entry>alpha</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry morerows="3">ARM et StrongARM</entry>
  <entry morerows="3">arm</entry>
  <entry>Netwinder et CATS</entry>
  <entry>netwinder</entry>
</row><row>
  <entry>Intel IOP32x</entry>
  <entry>iop32x</entry>
</row><row>
  <entry>Intel IXP4xx</entry>
  <entry>ixp4xx</entry>
</row><row>
  <entry>RiscPC</entry>
  <entry>rpc</entry>
</row>

<row>
  <entry morerows="1">HP PA-RISC</entry>
  <entry morerows="1">hppa</entry>
  <entry>PA-RISC 1.1</entry>
  <entry>32</entry>
</row><row>
  <entry>PA-RISC 2.0</entry>
  <entry>64</entry>
</row>

<row>
  <entry>Intel IA-64</entry>
  <entry>ia64</entry>
  <entry></entry>
  <entry></entry>
</row>

<row>
  <entry morerows="3">MIPS (grand boutien)</entry>
  <entry morerows="3">mips</entry>
  <entry>SGI IP22 (Indy/Indigo 2)</entry>
  <entry>r4k-ip22</entry>
</row><row>
  <entry>SGI IP32 (O2)</entry>
  <entry>r5k-ip32</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
  <entry morerows="4">MIPS (petit boutien)</entry>
  <entry morerows="4">mipsel</entry>
  <entry>Cobalt</entry>
  <entry>cobalt</entry>
</row><row>
  <entry morerows="1">DECstation</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Broadcom BCM91250A (SWARM)</entry>
  <entry>sb1-bcm91250a</entry>
</row><row>
  <entry>Broadcom BCM91480B (BigSur)</entry>
  <entry>sb1a-bcm91480b</entry>
</row>

<row>
  <entry morerows="5">Motorola 680x0</entry>
  <entry morerows="5">m68k</entry>
  <entry>Atari</entry>
  <entry>atari</entry>
</row><row>
  <entry>Amiga</entry>
  <entry>amiga</entry>
</row><row>
  <entry>68k Macintosh</entry>
  <entry>mac</entry>
</row><row>
  <entry morerows="2">VME</entry>
  <entry>bvme6000</entry>
</row><row>
  <entry>mvme147</entry>
</row><row>
  <entry>mvme16x</entry>
</row>

<row>
  <entry morerows="2">IBM/Motorola PowerPC</entry>
  <entry morerows="2">powerpc</entry>
  <entry>CHRP</entry>
  <entry>chrp</entry>
</row><row>
  <entry>PowerMac</entry>
  <entry>pmac</entry>
</row><row>
  <entry>PReP</entry>
  <entry>prep</entry>
</row>

<row>
  <entry morerows="2">Sun SPARC</entry>
  <entry morerows="2">sparc</entry>
  <entry>sun4m</entry>
  <entry>sparc32</entry>
</row><row>
  <entry>sun4u</entry>
  <entry morerows="1">sparc64</entry>
</row><row>
  <entry>sun4v</entry>
</row>

<row>
  <entry morerows="1">IBM S/390</entry>
  <entry morerows="1">s390</entry>
  <entry>IPL avec VM-reader et DASD</entry>
  <entry>generic</entry>
</row><row>
  <entry>IPL avec bande</entry>
  <entry>bande</entry>
</row>

</tbody></tgroup></informaltable>

</para>

<para>
Ce document d�crit l'installation pour l'architecture <emphasis>&arch-title;</emphasis>.
Des versions pour les autres architectures disponibles existent sur les pages
<ulink url="http://www.debian.org/ports/">Debian-Ports</ulink>.
</para>

<para condition="new-arch">
Voici la premi�re version officielle de &debian; pour l'architecture 
&arch-title;. Nous pensons que le portage a fait ses preuves et qu'il peut 
�tre rendu public. Cependant, comme il n'a pas encore �t� soumis � la 
<quote>vraie vie</quote> (ni test� par d'autres utilisateurs) comme l'ont �t� 
d'autres architectures, vous pourriez rencontrer quelques bogues. Utilisez 
notre <ulink url="&url-bts;">syst�me de suivi des bogues</ulink> pour 
signaler les probl�mes&nbsp;; n'oubliez pas de mentionner que le bogue s'est 
produit sur une plateforme &arch-title;. Et pr�venez aussi la 
<ulink url="&url-list-subscribe;">liste de diffusion debian-&arch-listname;</ulink>.
</para>

  </sect2>

<!-- supported cpu docs -->
&supported-alpha.xml;
&supported-amd64.xml;
&supported-arm.xml;
&supported-hppa.xml;
&supported-i386.xml;
&supported-ia64.xml;  <!-- FIXME: currently missing -->
&supported-m68k.xml;
&supported-mips.xml;
&supported-mipsel.xml;
&supported-powerpc.xml;
&supported-s390.xml;
&supported-sparc.xml;

  <sect2 id="gfx" arch="not-s390">
<title>Gestion des cartes graphiques</title>

<para arch="x86">
Vous devriez utiliser une interface graphique compatible
VGA pour le terminal de console. Pratiquement toutes les
cartes graphiques modernes sont compatibles VGA. Les
anciens standards comme CGA, MDA ou HGA devraient �galement
fonctionner, pour autant que vous ne vouliez pas utiliser
X11. Il faut remarquer que X11 n'est pas utilis� durant le
processus d'installation d�crit dans ce document.
</para>

<para>
Debian reconna�t les interfaces graphiques dans la mesure o� elles sont
reconnues par le syst�me X11 de X.Org. La plupart des
cartes AGP, PCI et PCIe fonctionnent sous X.Org. Des pr�cisions sur les bus 
graphiques accept�s, les cartes, les moniteurs et les souris sont disponibles sur 
<ulink url="&url-xorg;"></ulink>. Debian &release; est fourni avec
la version &x11ver; de X.Org.
</para>

<para arch="mips">
<!-- FIXME: mention explicit graphics chips and not system names -->
Le syst�me X window de X.Org n'est disponible pour le moment que sur SGI Indy et O2. 
Les cartes Broadcom BCM91250A et BCM91480B poss�dent des supports PCI 3.3v et acceptent 
l'�mulation VGA ou le <quote>framebuffer Linux</quote> pour certaines cartes
graphiques. Voyez la <ulink url="&url-bcm91250a-hardware;">page</ulink> pour
la carte BCM91250A.
</para><para arch="mipsel">
Le syst�me X window de X.Org est disponible sur certains mod�les DECstation.
Les cartes Broadcom BCM91250A et BCM91480B poss�dent des supports PCI 3.3v et acceptent
l'�mulation VGA ou le <quote>framebuffer Linux</quote> pour certaines cartes
graphiques. Voyez la <ulink url="&url-bcm91250a-hardware;">page</ulink> pour
les  cartes Broadcom.
</para>

<para arch="sparc">

La plupart des options graphiques communes des machines bas�es sur Sparc sont
g�r�es. Les pilotes de X.org sont disponibles pour sunbw2, suncg14, suncg3, suncg6,
sunleo et suntcx framebuffers, cartes Creator3D et Elite3D (sunffb driver), 
cartes ATI PGX24/PGX64 (ati driver), et cartes PermediaII (glint driver).
Pour utiliser une carte Elite3D avec X.org vous devez aussi installer le paquet
<classname>afbinit</classname> et lire la documentation pour savoir comment activer
la carte.
</para>
<para arch="sparc">

Il n'est pas exceptionnel qu'une machine Sparc poss�de deux cartes graphiques
en configuration normale. Il se peut alors que le noyau Linux ne dirige pas la sortie
vid�o vers la carte utilis�e par le firmware. L'absence de sortie vid�o sur la
console graphique peut �tre confondue avec un arr�t (habituellement le dernier message
vu sur la console est <quote>Booting Linux...</quote>). Une solution possible est
d'enlever l'une des cartes vid�o. Une autre solution est de d�sactiver l'une des
cartes avec un param�tre pass� au noyau. Quand la sortie vid�o n'est pas
n�cessaire ou souhait�e, on peut utiliser une console s�rie. Sur certains syst�mes, on peut
activer automatiquement la console s�rie en d�connectant le clavier
avant d'amorcer le syst�me.
</para>
  </sect2>

  <sect2 arch="x86" id="laptops"><title>Portables</title>

<para>
Les ordinateurs portables sont aussi support�s. Les
portables sont souvent sp�cialis�s ou contiennent du
mat�riel propri�taire. Pour v�rifier que votre portable fonctionne bien avec 
GNU/Linux, consultez les
<ulink url="&url-x86-laptop;">pages sur les portables sous Linux</ulink>.
</para>
  </sect2>

  <sect2 condition="defaults-smp">
<title>Processeurs multiples</title>

<para>
Cette architecture accepte les syst�mes � plusieurs processeurs (<quote>symmetric multiprocessing</quote> ou SMP). L'image standard du noyau Debian &release; a �t� compil� avec SMP. Cela
ne devrait pas emp�cher l'installation, puisque le noyau SMP devrait d�marrer 
sur les syst�mes non-SMP (le noyau sera simplement un peu plus gros).
</para>

<para>
Pour optimiser le noyau pour un seul processeur, vous devrez
remplacer le noyau Debian standard. Vous trouverez une
discussion sur la fa�on de faire dans la <xref linkend="kernel-baking"/>. 
Aujourd'hui (version &kernelversion; du noyau) la fa�on de d�sactiver SMP est 
de ne pas choisir <quote>&smp-config-option;</quote> dans la section
<quote>&smp-config-section;</quote> quand on configure le noyau.
</para>
</sect2>

<sect2 condition="smp-alternatives">
<title>Processeurs multiples</title>
<para>
Cette architecture accepte les syst�mes � plusieurs processeurs 
(<quote>symmetric multiprocessing</quote> ou SMP). L'image
standard du noyau Debian &release; a �t� compil� avec l'option
<firstterm>SMP-alternatives</firstterm>. Le noyau d�tectera si votre syst�me poss�de plusieurs
processeurs et d�sactivera automatiquement la gestion SMP sur les syst�mes avec un seul
processeur.
</para>

<para arch="i386">
Pour l'architecture &arch-title;, la variante 486 de l'image Debian du noyau
n'est pas compil� avec l'option SMP.
</para>
</sect2>

<sect2 condition="supports-smp">
 <title>Processeurs multiples</title>
<para>
Cette architecture accepte les syst�mes � plusieurs processeurs 
(<quote>symmetric multiprocessing</quote> ou SMP). 
Cependant, l'image standard du noyau Debian &release; ne reconna�t pas 
le SMP. Cela ne devrait pas emp�cher l'installation, puisque le noyau non-SMP 
devrait d�marrer sur les syst�mes SMP, le noyau utilisera simplement le 
premier CPU.
    </para>
<para>
Afin de tirer profit de plusieurs processeurs, vous devrez remplacer le 
noyau standard Debian. Vous trouverez une
discussion sur la fa�on de faire dans la <xref linkend="kernel-baking"/>. 
Aujourd'hui (version &kernelversion; du noyau) la fa�on d'activer SMP est de 
choisir <quote>&smp-config-option;</quote> dans la section
<quote>&smp-config-section;</quote> quand on configure le noyau.
</para>
  </sect2>
<sect2 condition="supports-smp-sometimes">
 <title>Processeurs multiples</title>
<para>
Cette architecture accepte les syst�mes � plusieurs processeurs 
(<quote>symmetric multiprocessing</quote> ou SMP). 
Une image standard du noyau Debian &release; a �t� compil�e avec SMP.
Selon votre support d'installation, ce noyau peut ou non �tre install� par 
d�faut. Cela ne devrait pas emp�cher l'installation, puisque le noyau non-SMP 
devrait d�marrer
sur les syst�mes SMP&nbsp;; le noyau utilisera simplement le premier 
processeur.
</para><para>
Afin de tirer profit de plusieurs processeurs, vous devrez v�rifier que le
noyau install� accepte le SMP ou bien vous en installerez un.

</para><para>

Vous pouvez aussi compiler vous-m�me un noyau avec SMP. Vous trouverez une
discussion sur la fa�on de faire dans la <xref linkend="kernel-baking"/>.
Aujourd'hui (version &kernelversion; du noyau) la fa�on d'activer SMP est de
choisir <quote>&smp-config-option;</quote> dans la section
<quote>&smp-config-section;</quote> quand on configure le noyau.
</para>
</sect2>
 </sect1>
