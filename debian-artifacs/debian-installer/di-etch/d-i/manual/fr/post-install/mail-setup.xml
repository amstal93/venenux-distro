<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43774 -->

 <sect1 id="mail-setup">
 <title>Configurer le courrier �lectronique</title>
<para>

Le courrier �lectronique tient aujourd'hui une grande place dans la vie des
gens. Comme il est important que le syst�me de courrier, avec ses nombreuses options,
soit bien configur�, nous aborderons dans cette section ses principaux �l�ments.

</para><para>

Un syst�me de courrier est compos� de trois �l�ments. Il y a d'abord
l'<firstterm>agent utilisateur</firstterm>, <emphasis>Mail User Agent, (MUA)</emphasis>
qui est le programme avec lequel l'utilisateur lit et �crit son courrier.
Ensuite il y a l'<firstterm>agent de transport</firstterm>,
<emphasis>Mail Transfer Agent, (MTA)</emphasis>, programme qui transporte les courriers
d'un ordinateur � l'autre. Enfin il y a l'<firstterm>agent de distribution</firstterm>,
<emphasis>Mail Delivery Agent (MDA)</emphasis>,
programme qui distribue le courrier dans les bo�tes des utilisateurs.

</para><para>

Ces trois fonctions peuvent �tre effectu�es par des programmes distincts,
ou bien seulement par un ou deux programmes. Il est possible aussi que
diff�rents programmes accomplissent ces t�ches pour diff�rents types de courrier.

</para><para>

Sur Linux et les syst�mes Unix, <command>mutt</command> est un lecteur tr�s appr�ci�.
Comme les programmes traditionnels de Linux, il est en mode texte. Il est souvent
associ� � <command>exim</command> ou <command>sendmail</command> comme MTA et � 
<command>procmail</command> comme MDA.

</para><para>

Avec la popularit� croissante des environnements de bureau graphique,
des programmes comme <command>evolution</command> de GNOME,
<command>kmail</command> de KDE ou <command>thunderbird</command> de Mozilla
(disponible dans Debian sous l'appellation <command>icedove</command><footnote>

<para>
<command>thunderbird</command> a �t� modifi� en <command>icedove</command> pour
des raisons l�gales. 
</para>

</footnote>) deviennent aussi populaires. Ils combinent les trois fonctions,
MUA, MTA et MDA, mais ils peuvent &mdash; et le sont souvent &mdash; �tre utilis�s
avec les programmes traditionnels de Linux.

</para>

  <sect2 id="mail-default">
  <title>Configuration par d�faut</title>
<para>

M�me si vous comptez utiliser un programme graphique, il est important d'installer
et de configurer un ensemble traditionnel MTA/MDA. En effet, certains utilitaires du syst�me
<footnote>

<para>
Par exemple, <command>cron</command>, <command>quota</command>,
<command>logcheck</command>, <command>aide</command>, &hellip;
</para>

</footnote> peuvent envoyer des messages importants sous forme de courriels �
l'administrateur du syst�me.

</para><para>

Pour cette raison, les paquets <classname>exim4</classname> et
<classname>mutt</classname> sont install�s d'office (� moins que vous
n'ayez d�s�lectionn� la t�che <quote>standard</quote> pendant l'installation).
L'agent de transport du courrier <command>exim4</command>, combinant les fonctions MTA MDA,
est un programme relativement petit mais tr�s pratique. Par d�faut, il est configur�
pour n'envoyer des courriels que sur le syst�me local. Les courriels adress�s �
l'administrateur (le compte root) sont envoy�s � l'utilisateur cr�� pendant
l'installation <footnote>
<para>

Le renvoi de courriel pour root vers le compte utilisateur est configur� dans
le fichier <filename>/etc/aliases</filename>.. S'il n'existe pas de compte utilisateur,
le courriel sera bien s�r envoy� au compte root.

</para>

</footnote>.

</para><para>

Quand des courriels syst�me sont envoy�s, ils sont ajout�s dans le fichier
<filename>/var/mail/<replaceable>nom_utilisateur</replaceable></filename>.
Les courriels peuvent �tre lus avec <command>mutt</command>.

</para>
  </sect2>

  <sect2 id="mail-outgoing">
  <title>Envoyer des courriels vers l'ext�rieur</title>
<para>

Comme il a �t� dit pr�c�demment, le syst�me Debian install� ne g�re l'envoi de courriel qu'en
local et ne permet pas d'envoyer des messages vers l'ext�rieur ni d'en recevoir
de l'ext�rieur.

</para><para>

Si vous d�sirez que <classname>exim4</classname> g�re l'envoi de messages
vers l'ext�rieur, lisez la section qui suit, elle vous pr�sentera les
options de configuration disponibles. N'oubliez pas de tester si
l'envoi et la r�ception de courrier sont corrects.

</para><para>

Si vous avez l'intention d'utiliser un programme graphique avec le serveur
de courrier de votre fournisseur d'acc�s � internet ou de votre entreprise,
vous n'avez pas vraiment besoin de configurer <classname>exim4</classname>.
Indiquez juste � votre programme de courrier les bons serveurs � utiliser pour
envoyer et recevoir les messages.

</para><para>

Dans ce cas, vous aurez aussi � configurer certains programmes pour envoyer
correctement des courriels. Par exemple, <command>reportbug</command>, un programme
qui permet d'envoyer des rapports de bogues pour les paquets Debian,
s'attend � trouver <classname>exim4</classname>.

</para><para>

Pour indiquer � <command>reportbug</command> d'utiliser un serveur de courrier
externe, lancez la commande <command>reportbug --configure</command>
et r�pondez <quote>no</quote> � la question de savoir si un MTA est disponible.
On vous demandera le serveur � utiliser pour envoyer les rapports de bogues.

</para>
  </sect2>

  <sect2 id="config-mta">
  <title>Configuration de Exim4</title>
<para>

Si vous d�sirez que votre syst�me g�re le courrier vers l'ext�rieur, vous devez
reconfigurer la paquet <classname>exim4</classname> <footnote>

<para>
Vous pouvez bien s�r supprimer <classname>exim4</classname> et le remplacer par
un autre MTA/MDA.
</para>

</footnote>:

<informalexample><screen>
# dpkg-reconfigure exim4-config
</screen></informalexample>

</para><para>

Apr�s avoir saisi cette commande (en tant que superutilisateur),
il vous sera demand� si vous voulez diviser la configuration en petits fichiers.
En cas de doute, choisissez l'option par d�faut.

</para><para>

Plusieurs sc�narios communs vous sont propos�s. Choisissez celui qui vous para�t
le plus conforme � vos besoins.

</para>

<variablelist>
<varlistentry>
<term>site internet</term>
<listitem><para>

Votre syst�me est connect� � un r�seau et le courrier est exp�di� et re�u
directement avec SMTP. Dans les �crans suivants, on vous posera quelques
questions �l�mentaires comme le nom de votre machine pour le courrier, les
domaines dont vous acceptez ou relayez les courriels.

</para></listitem>
</varlistentry>

<varlistentry>
<term>courrier envoy� par une machine relais (<emphasis>smarthost</emphasis>)</term>
<listitem><para>

Dans ce sc�nario, le courrier sortant est envoy� � une autre machine, appel�
le <emphasis>smarthost</emphasis> qui exp�die le courrier � sa destination.
Cette machine relais garde les
courriels qui vous sont adress�s et vous permet de ne pas �tre constamment
connect�. Vous devez donc r�cup�rer vos courriels sur cette machine avec des
programmes comme fetchmail.

</para><para>

Le plus souvent, le � smarthost � est le serveur de votre fournisseur d'acc�s �
internet. C'est l'option adapt�e � un syst�me connect� par
le r�seau t�l�phonique. Le serveur peut �tre aussi celui d'une entreprise, ou bien
m�me un autre syst�me sur votre r�seau.

</para></listitem>
</varlistentry>

<varlistentry>
<term>courrier envoy� par une machine relais ; pas de courrier local</term>
<listitem><para>


Cette option est presque la m�me que la pr�c�dente
sauf que le syst�me ne g�re pas le courrier local. Les messages du syst�me,
par exemple pour l'administrateur, sont toujours g�r�s.

</para></listitem>
</varlistentry>

<varlistentry>
<term>distribution locale seulement</term>
<listitem><para>

C'est l'option par d�faut.

</para></listitem>
</varlistentry>

<varlistentry>
<term>pas de configuration pour le moment</term>
<listitem><para>
Choisissez cette option si vous �tes absolument certain de savoir ce que vous
faites. Le syst�me de courrier ne sera pas configur�. Tant qu'il ne le sera
pas, vous ne pourrez ni envoyer ni recevoir des courriels. Les messages
importants venant des utilitaires du syst�me ne vous parviendront pas.

</para></listitem>
</varlistentry>
</variablelist>

<para>

Si aucun de ces sc�narios ne s'accorde � vos besoins, ou si vous voulez un
r�glage tr�s fin, vous devrez modifier les fichiers de configuration qui se
trouvent dans le r�pertoire <filename>/etc/exim4</filename>. D'autres
informations sur le programme <command>exim4</command> se trouvent dans
<filename>/usr/share/doc/exim4</filename>. Le fichier <filename>README.Debian.gz</filename>
contient d'autres informations sur la configuration de <classname>exim4</classname>. Il
signale aussi d'autres sources d'informations. 

</para><para>

Il faut noter qu'envoyer des messages directement sur internet quand on ne
poss�de pas de nom de domaine officiel peut provoquer le rejet des messages,
� cause des mesures antispam prises par les serveurs de courriers.
Il est pr�f�rable d'utiliser le serveur de son fournisseur d'acc�s � internet.
Si vous le voulez malgr� tout, vous pouvez utiliser une autre adresse que celle
cr��e par d�faut. Avec <classname>exim4</classname>, c'est possible en ajoutant
une entr�e dans <filename>/etc/email-addresses</filename>.

</para>
  </sect2>
 </sect1>
