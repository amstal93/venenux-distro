<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45438 -->

<chapter id="boot-new">
 <title>D�marrer votre nouveau syst�me Debian</title>

 <sect1 id="base-boot"><title>Le moment de v�rit�</title>
<para>

Voici ce que les ing�nieurs en �lectronique appellent le
<emphasis>test de la fum�e</emphasis>&nbsp;: d�marrer un syst�me pour la 
premi�re fois.
</para>
<para arch="x86">
Apr�s une installation standard, le premier �cran que vous verrez au d�marrage
du syst�me est le menu du programme d'amor�age <classname>grub</classname>
ou celui de <classname>lilo</classname>. Le premier choix est votre nouveau
syst�me Debian. Si d'autres syst�mes (comme Windows) ont �t� d�tect�s, ils seront affich�s
au dessous.
</para>
<para>
Si le syst�me ne d�marre pas correctement, ne paniquez pas. Si l'installation s'est d�roul�e
normalement, il est tr�s probable que seul un probl�me mineur emp�che le syst�me d'amorcer
Debian. Dans la plupart des cas, il ne sera pas n�cessaire de refaire une installation pour
corriger de tels probl�mes. On peut commencer par utiliser le mode de secours
int�gr� � l'installateur (voyez <xref linkend="rescue"/>).
</para>

<para>
Si vous d�couvrez Debian et Linux, vous aurez besoin de l'aide d'utilisateurs
exp�riment�s.
<phrase arch="x86">Les canaux IRC #debian ou #debian-boot sur le r�seau OFTC apportent
une aide directe. Vous pouvez aussi contacter la
<ulink url="&url-list-subscribe;">liste de diffusion debian-user</ulink>.</phrase>
<phrase arch="not-x86">Pour des architectures comme &arch-title;, la meilleure id�e
est de s'adresser � la <ulink url="&url-list-subscribe;">liste debian-&arch-listname;</ulink>.
</phrase>
Vous pouvez aussi envoyer un rapport d'installation, voyez <xref linkend="submit-bug"/>.
Assurez-vous de bien d�crire votre probl�me et d'inclure tous les messages qui
sont affich�s. Cela peut aider � diagnostiquer le probl�me.
</para>
<para arch="x86">
Veuillez envoyer un rapport d'installation si des syst�mes d'exploitation install�s sur
la machine n'ont pas �t� d�tect�s ou ont �t� mal d�tect�s.
</para>

  <sect2 arch="m68k"><title>Amorcer une BVME 6000</title>
<para>

Si vous faites une installation sans disque sur une machine BVM ou Motorola 
VMEbus&nbsp;: une fois que le syst�me a lanc� le programme 
/<command>tftplilo</command> depuis le serveur TFTP, � l'invite 
<prompt>LILO Boot:</prompt> entrez l'une des options suivantes&nbsp;:

<itemizedlist>
<listitem><para>

<userinput>b6000</userinput> suivi par &enterkey;
pour amorcer une BVME4000/6000

</para></listitem><listitem><para>

<userinput>b162</userinput> suivi par &enterkey;
pour amorcer une MVME162

</para></listitem><listitem><para>

<userinput>b167</userinput> suivi par &enterkey;
pour amorcer une MVME166/167

</para></listitem>
</itemizedlist>

</para>

   </sect2>

  <sect2 arch="m68k"><title>Amorcer un Macintosh</title>

<para>

Allez dans le r�pertoire qui contient les fichiers d'installation et
d�marrez le programme d'amor�age <command>Penguin</command>, en pressant
la touche <keycap>command</keycap>. Allez sur la bo�te de dialogue
<userinput>Settings</userinput> 
(<keycombo><keycap>command</keycap> <keycap>T</keycap> </keycombo>), et 
identifiez la ligne d'options du noyau. Elle devrait ressembler � 
<userinput>root=/dev/ram ramdisk_size=15000</userinput>.

</para><para>

Vous devez la remplacer par 
<userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>.
Remplacez le <replaceable>yyyy</replaceable> par le nom de la partition Linux 
sur laquelle vous avez install� le syst�me. Par exemple,
<filename>/dev/sda1</filename>, vous l'aviez not� pr�c�demment.
Le param�tre <userinput>fbcon=font:VGA8x8</userinput>
(ou <userinput>video=font:VGA8x8</userinput> pour les noyaux de version
inf�rieure � 2.6) est particuli�rement recommand�
aux utilisateurs qui poss�dent un �cran minuscule.
Vous pouvez changer ce param�tre � tout moment.

</para><para>

Si vous ne souhaitez pas automatiquement lancer GNU/Linux � chaque
d�marrage, d�s�lectionnez l'option <userinput>Auto Boot</userinput>. 
Enregistrez vos param�tres dans le fichier <filename>Prefs</filename> avec 
l'option <userinput>Save Settings As Default</userinput>.

</para><para>

S�lectionnez maintenant <userinput>Boot Now</userinput> 
(<keycombo><keycap>command</keycap> <keycap>B</keycap> </keycombo>) pour 
d�marrer votre syst�me GNU/Linux, fra�chement install�, � la place du
syst�me d'installation sur le disque virtuel.

</para><para>

Debian devrait d�marrer, et vous devriez voir d�filer les m�mes messages que 
lorsque vous avez d�marr� le syst�me d'installation, suivis de nouveaux 
messages.
</para>
   </sect2>

<sect2 arch="powerpc"><title>OldWorld PowerMacs</title>
<para>

Si la machine ne d�marre pas quand l'installation est termin�e, et s'arr�te
avec l'invite <prompt>boot:</prompt>, saisissez 
<userinput>Linux</userinput> suivi de &enterkey;. La configuration par d�faut 
dans le fichier <filename>quik.conf</filename> est nomm�e Linux. Les noms
d�finis dans ce fichier seront affich�s si vous pressez la touche
<keycap>Tab</keycap> � l'invite <prompt>boot:</prompt>. Vous pouvez essayer
de r�amorcer l'installateur et de modifier le fichier 
<filename>/target/etc/quik.conf</filename> qui a �t� cr�� � l'�tape
<guimenuitem>Installer Quik sur un disque dur</guimenuitem>.
Des renseignements sur <command>quik</command> sont disponibles dans
<ulink url="&url-powerpc-quik-faq;"></ulink>.
</para>
<para>
Pour d�marrer de nouveau sous MacOS sans r�initialiser la <quote>nvram</quote>,
saisissez <userinput>bye</userinput> � l'invite de l'Open Firmware (en 
supposant que MacOS n'a pas �t� enlev� de la machine). Pour obtenir une invite
Open Firmware, appuyez simultan�ment sur les touches
<keycombo><keycap>command</keycap> <keycap>option</keycap> <keycap>o</keycap>
<keycap>f</keycap> </keycombo> pendant le d�marrage � froid de la machine.
Si vous devez r�initialiser les changements de l'Open Firmware en nvram,
appuyez simultan�ment sur les touches
<keycombo><keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap><keycap>r</keycap> </keycombo>
lors d'un d�marrage � froid de la machine.
</para>
<para>
Si vous amorcez le syst�me install� avec <command>BootX</command>, choisissez
simplement le noyau dans le dossier <filename>Linux Kernels</filename>,
ne choisissez pas l'option ramdisk et ajoutez un p�riph�rique racine pour
votre installation, par exemple <userinput>/dev/hda8</userinput>.
</para>

</sect2>

  <sect2 arch="powerpc"><title>NewWorld PowerMacs</title>

<para>
Sur les machines de type G4 et iBook, vous pouvez maintenir la touche
<keycap>option</keycap> enfonc�e pour obtenir un �cran graphique, avec un
bouton pour chaque syst�me d'exploitation d�marrable&nbsp;; pour &debian;
il s'agit d'un bouton avec une ic�ne de petit pingouin.
</para>
<para>
Si vous avez conserv� MacOS et qu'� un moment ou � un autre la variable
<envar>boot-device</envar> de l'Open Firmware a �t� modifi�e, vous devrez
restaurer sa configuration. Pour cela, maintenez enfonc�es les touches
<keycombo><keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap><keycap>r</keycap> </keycombo>
lors du d�marrage � froid de la machine.
</para>
<para>
Les noms d�finis dans <filename>yaboot.conf</filename> seront
affich�s si vous appuyez sur la touche <keycap>Tab</keycap> � l'invite
<prompt>boot:</prompt>.
</para>
<para>
La r�initialisation de l'Open Firmware sur les mat�riels G3 et G4 les fera
d�marrer par d�faut sur &debian; (� condition d'avoir effectu� un
partitionnement correct et d'avoir plac� la partition Apple_Bootstrap en
premier). Si &debian; se trouve sur un disque SCSI et que MacOS se trouve sur
un disque IDE, il se pourrait que cela ne fonctionne pas et que vous deviez
lancer l'OpenFirmware et d�clarer la variable <envar>boot-device</envar>
manuellement&nbsp;; en principe, <command>ybin</command> le fait
automatiquement.
</para>
<para>
Apr�s avoir lanc� &debian; pour la premi�re fois, vous pourrez ajouter autant
d'options que vous le souhaitez (par exemple, les options pour amorcer 
d'autres syst�mes)
dans le fichier <filename>/etc/yaboot.conf</filename> et d�marrer
<command>ybin</command> pour mettre � jour votre partition de d�marrage avec
la nouvelle configuration. Lisez le
<ulink url="&url-powerpc-yaboot-faq;">HOWTO sur yaboot</ulink>.
</para>
</sect2>

 </sect1>

&mount-encrypted.xml;

 <sect1 id="login">
 <title>Se connecter</title>

<para>

Quand le syst�me a �t� amorc�, vous vous retrouvez face � l'invite de 
connexion (login). Connectez-vous en utilisant le nom d'utilisateur et le mot 
de passe que vous avez choisis pendant le processus d'installation. 
Vous pouvez maintenant utiliser votre syst�me.

</para><para>

Si vous �tes un nouvel utilisateur, vous voudrez peut-�tre parcourir la 
documentation qui est d�j� install�e sur votre syst�me. Il existe plusieurs 
syst�mes de documentation&nbsp;; un travail est en cours afin de les int�grer 
dans un tout coh�rent. Vous trouverez ci-dessous quelques points de d�part.

</para><para>

La documentation qui accompagne les programmes que vous avez install�s
se trouve dans le r�pertoire <filename>/usr/share/doc/</filename>, dans un
sous-r�pertoire qui porte le nom du programme, et plus pr�cis�ment le nom du
paquet qui contient le programme. 
Il arrive qu'une documentation soit mise dans un paquet s�par� qui n'est pas install�
par d�faut. Par exemple, de la documentation concernant le gestionnaire de paquets
<command>apt</command> se trouve dans les paquets <classname>apt-doc</classname> et
<classname>apt-howto</classname>.

</para><para>
 
De plus, il y a quelques r�pertoires sp�ciaux dans le r�pertoire 
<filename>/usr/share/doc/</filename>. Les HOWTO Linux sont install�s au 
format <emphasis>.gz</emphasis> (compress�) dans le r�pertoire
      <filename>/usr/share/doc/HOWTO/en-txt/</filename>.
Une fois <classname>dhelp</classname> install�, vous pouvez consulter avec votre
navigateur le fichier 
<filename>/usr/share/doc/HTML/index.html</filename>
qui contient un index sur la documentation install�e.

</para><para>

Pour consulter facilement ces documents, on peut utiliser un navigateur
en mode texte&nbsp;:


<informalexample><screen>
$ cd /usr/share/doc/
$ w3c .
</screen></informalexample>

Le point apr�s la commande <command>w3c</command> demande d'afficher le contenu
du r�pertoire courant.

</para><para>

Avec un environnement graphique, vous pouvez lancer son navigateur depuis le menu
application et mettre <userinput>/usr/share/doc/</userinput> comme adresse.
</para> 

<para>
Vous pouvez aussi saisir <userinput>info
<replaceable>command</replaceable></userinput> ou <userinput>man
<replaceable>command</replaceable></userinput> pour obtenir des informations 
sur la plupart des commandes disponibles depuis l'interpr�teur de commandes. 
En ex�cutant <userinput>help</userinput>, vous afficherez l'aide sur les 
commandes de l'interpr�teur de commandes. Et si vous tapez une commande 
suivie par <userinput>--help</userinput>, un court r�sum� sur l'usage de 
cette commande sera affich�. Si le r�sultat d'une commande d�file au-del� du 
haut de l'�cran, tapez <userinput>|&nbsp;more</userinput> apr�s la commande de 
fa�on � provoquer une pause entre chaque �cran. Pour voir une liste de toutes 
les commandes disponibles qui commencent par une lettre donn�e, tapez cette 
lettre, suivie de deux fois la touche de tabulation.

</para>

 </sect1>
</chapter>
