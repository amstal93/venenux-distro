<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 36639 -->

  <sect2 arch="arm" id="boot-tftp"><title>Amorcer � partir de TFTP</title>

&boot-installer-intro-net.xml;

  <sect3 arch="arm"><title>Amorcer � partir de TFTP sur une Netwinder</title>

<para>

Les Netwinders poss�dent deux interfaces r�seau&nbsp;: une carte 10&nbsp;Mbps 
compatible NE2000 (g�n�ralement d�finie comme <filename>eth0</filename>) et la 
carte Tulip en 100&nbsp;Mbps. Charger l'image par TFTP avec la carte en 100&nbsp;Mbps
peut poser des probl�mes. Il est recommand� d'utiliser la carte � 10&nbsp;Mbps
(celle marqu�e <literal>10 Base-T</literal>).

</para>
<note><para>

Vous aurez besoin de NeTTrom version&nbsp;2.2.1 ou sup�rieure pour amorcer le 
syst�me d'installation. Il est recommand� d'utiliser NeTTrom&nbsp;2.3.3.
Malheureusement des probl�mes de licence emp�chent le chargement des fichiers du
firmware. Si la situation venait � changer, les nouvelles images seraient
<ulink url="http//www.netwinder.org/">ici</ulink>.

</para></note>

<para>

Quand vous amorcez votre Netwinder, vous devez interrompre le processus d'amor�age
pendant la phase de compte � rebours. Cela permet de configurer certains �l�ments
n�cessaires. Tout d'abord, chargez les param�tres par d�faut&nbsp;:

<informalexample><screen>
    NeTTrom command-&gt; load-defaults
</screen></informalexample>

Ensuite vous devez configurer le r�seau, m�me avec une adresse fixe :

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 flash
    NeTTrom command-&gt; setenv eth0_ip 192.168.0.10/24
</screen></informalexample>

o� 24 est le nombre de bits du masque r�seau, ou bien sans adresse&nbsp;:

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 dhcp
</screen></informalexample>

Il se peut que vous ayez aussi � configurer les r�glages de
<userinput>route1</userinput> si le serveur TFTP n'est pas sur le sous-r�seau local.

Ensuite vous devez indiquer le serveur TFTP et l'endroit o� se trouve l'image.
Vous pouvez alors enregistrer ces param�tres en flash.

<informalexample><screen>
    NeTTrom command-&gt; setenv kerntftpserver 192.168.0.1
    NeTTrom command-&gt; setenv kerntftpfile boot.img
    NeTTrom command-&gt; save-all
</screen></informalexample>

Puis vous devez indiquer au firmware que l'image TFTP doit �tre amorc�e&nbsp;:

<informalexample><screen>
    NeTTrom command-&gt; setenv kernconfig tftp
    NeTTrom command-&gt; setenv rootdev /dev/ram
</screen></informalexample>

Si vous voulez installer NetWinder avec la console s�rie, vous devez utiliser
la commande qui suit.

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram console=ttyS0,115200
</screen></informalexample>

Pour les installations avec clavier et moniteur, vous devez ex�cutez&nbsp;:

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram
</screen></informalexample>

Utilisez la commande <command>printenv</command> pour r�afficher vos 
r�glages. 
Une fois les v�rifications faites, vous pouvez charger l'image&nbsp;:

<informalexample><screen>
    NeTTrom command-&gt; boot
</screen></informalexample>

En cas de probl�mes, il existe un 
<ulink url="http://www.netwinder.org/howto/Firmware-HOWTO.html">HOWTO pr�cis</ulink>.

</para>
  </sect3>

  <sect3 arch="arm"><title>Amorcer � partir de TFTP sur CATS</title>

<para>

Sur les machines CATS, utilisez <command>boot de0:</command>
        ou quelque chose de similaire � l'invite de Cyclone.

</para>
   </sect3>
  </sect2>



  <sect2 arch="arm"><title>Amorcer depuis un c�d�rom</title>

&boot-installer-intro-cd.xml;

<para>
Pour amorcer � partir d'un c�d�rom depuis l'invite sur une console
Cyclone, utilisez la commande
<command>boot cd0:cats.bin</command>
</para>
  </sect2>
  <sect2 arch="arm" id="boot-firmware"><title>Amorcer depuis un Firmware</title>

&boot-installer-intro-firmware.xml;

   <sect3 arch="arm" id="boot-firmware-nslu2"><title>Amorcer un NSLU2</title>
<para>

Il y a trois fa�ons de mettre le firmware en flash.

</para>

    <sect4 arch="arm"><title>Utiliser l'interface web du NSLU2</title>
<para>

Allez dans la section administration et choisissez l'�l�ment <literal>Upgrade</literal>.
Vous pouvez chercher l'image que vous venez de t�l�charger. Pressez le bouton
<literal>Start Upgrade</literal>, confirmez, attendez quelques minutes puis
confirmez � nouveau. Le syst�me s'amorcera directement sur l'installateur.

</para>
    </sect4>

    <sect4 arch="arm"><title>Par le r�seau avec Linux/Unix</title>
<para>

Vous pouvez utiliser <command>upslug2</command> sur une machine Linux ou Unix
pour mettre � jour la machine par le r�seau. Ce logiciel est un paquet Debian.

D'abord mettez votre NSLU2 en mode upgrade :

<orderedlist>
<listitem><para>

D�connectez les disques des ports USB.

</para></listitem>
<listitem><para>

Eteignez le NSLU2

</para></listitem>
<listitem><para>

Gardez enfonc� le bouton reset (accessible par un petit trou � l'arri�re,
juste au dessus du bouton power).

</para></listitem>
<listitem><para>

Pressez et relachez le bouton power pour lancer le NSLU2.

</para></listitem>
<listitem><para>


Attendez dix secondes en scrutant la LED � ready/status �. Apr�s dix secondes
elle passera au rouge. Relachez imm�diatement le bouton reset.

</para></listitem>
<listitem><para>

La LED � ready/status � passera alternativement du vert au rouge
(il y a une seconde de d�lai avant le premier vert). Le NSLU2 est maintenant
en mode mise � jour.

</para></listitem>
</orderedlist>

Voyez les <ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">pages sur NSLU2-Linux
</ulink> si vous avez des probl�mes.

Une fois en mode mise � jour, vous pouvez mettre en m�moire la nouvelle image.

<informalexample><screen>
sudo upslug2 -i di-nslu2.bin
</screen></informalexample>

Remarquez que ce logiciel montre aussi l'adresse MAC du NSLU2, ce qui peut �tre
pratique pour configurer le serveur DHCP. Apr�s que l'image a �t� charg�e et v�rifi�e,
le syst�me red�marre automatiquement. N'oubliez pas de rebrancher votre disque USB,
sinon l'installateur ne pourra pas le trouver

</para>
    </sect4>

    <sect4 arch="arm"><title>Par le r�seau avec Windows</title>
<para>

Il existe un <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">outil
</ulink> pour Windows qui met � jour le firmware par le r�seau.

</para>
    </sect4>
   </sect3>
  </sect2>