<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44560 -->

 <sect1 id="boot-troubleshooting">
 <title>Dysfonctionnements pendant la proc�dure d'installation</title>
<para>
</para>

  <sect2 arch="not-s390" id="unreliable-cd">
  <title>Fiabilit� des lecteurs de c�d�roms</title>
<para>

Parfois, particuli�rement avec d'anciens lecteurs, l'installateur ne
peut s'amorcer � partir d'un c�d�rom. Il arrive aussi, m�me apr�s un amor�age
r�ussi, que l'installateur ne reconnaisse pas le lecteur ou bien signale des erreurs
de lecture pendant l'installation.
</para>
<para>
Plusieurs causes sont possibles. Voici une liste de probl�mes connus, accompagn�s
de suggestions sur la mani�re de les traiter.
</para>
<para>
On peut tout d'abord essayer deux choses tr�s simples&nbsp;:

<itemizedlist>
<listitem><para>
Si le c�d�rom ne s'amorce pas, il faut v�rifier qu'il est correctement ins�r� et
qu'il n'est pas sale.
</para></listitem>
<listitem><para>
Si l'installateur ne reconna�t pas le c�d�rom, on peut lancer une deuxi�me fois 
l'option <menuchoice> <guimenuitem>D�tecter et monter un CD-ROM</guimenuitem></menuchoice>.
Certains probl�mes de DMA avec les anciens lecteurs sont r�solus de cette fa�on.
</para></listitem>
</itemizedlist>

</para>
<para>
Si rien ne fonctionne, essayez les suggestions qui suivent. La plupart, mais pas toutes,
concernent aussi bien les CD-ROM que les DVD, mais nous utilisons le terme c�d�rom pour
simplifier.
</para>

<para>
Si vous ne pouvez faire l'installation � partir d'un c�d�rom, vous pouvez essayer l'une
des autres m�thodes disponibles.
</para>

  <sect3>
  <title>Probl�mes communs</title>

<itemizedlist>

  <listitem><para>
Certains lecteurs anciens ne peuvent lire des disques grav�s � grande vitesse sur
les lecteurs modernes.
</para></listitem>
<listitem><para>
Quand le syst�me s'amorce correctement � partir d'un c�d�rom, cela ne veut pas
n�cessairement dire que Linux reconnaisse le contr�leur auquel est connect�
le lecteur de c�d�roms.
</para></listitem>
<listitem><para>
Certains lecteurs anciens ne fonctionnent pas correctement si le DMA (<emphasis>direct memory
access</emphasis>) est activ�.
</para></listitem>
</itemizedlist>
</sect3>

<sect3>
  <title>Comment r�soudre ces probl�mes ?</title>
<para>
Si le c�d�rom ne peut amorcer l'installateur, voici une liste de suggestions&nbsp;:

<itemizedlist>
  <listitem><para>

V�rifiez que le BIOS est r�ellement capable de g�rer l'amor�age � partir d'un c�d�rom,
certains anciens syst�mes ne le permettent pas. V�rifiez aussi que le lecteur
reconna�t le support que vous utilisez.
</para></listitem>
<listitem><para>
Si vous avez t�l�charg� une image ISO, v�rifiez que la somme MD5 de cette image
correspond � celle associ�e dans le fichier <filename>MD5SUMS</filename>. Ce fichier se trouve
normalement l� o� vous avez t�l�charg� l'image.

<informalexample><screen>
$ md5sum <replaceable>debian-testing-i386-netinst.iso</replaceable>
a20391b12f7ff22ef705cee4059c6b92  <replaceable>debian-testing-i386-netinst.iso</replaceable>
</screen></informalexample>

V�rifiez ensuite que la somme MD5 du c�d�rom grav� correspond aussi. La commande
suivante devrait fonctionner. Elle utilise la taille de l'image pour lire le nombre d'octets
sur le c�d�rom.
 
<informalexample><screen>
$ dd if=/dev/cdrom | \
> head -c `stat --format=%s <replaceable>debian-testing-i386-netinst.iso</replaceable>` | \
> md5sum
a20391b12f7ff22ef705cee4059c6b92  -
262668+0 records in
262668+0 records out
134486016 bytes (134 MB) copied, 97.474 seconds, 1.4 MB/s
</screen></informalexample>

</para></listitem>
</itemizedlist>
</para>

<para>
Quand le lecteur de c�d�roms n'est pas reconnu m�me apr�s un amor�age r�ussi, il suffit souvent
de recommencer pour r�soudre le probl�me. Si vous poss�dez plusieurs lecteurs,
mettez le c�d�rom dans l'autre lecteur. Si rien ne fonctionne ou si des erreurs de lecture
sont signal�es, essayez les suggestions list�es ci-dessous. Une connaissance �l�mentaire
de Linux est n�cessaire. Pour ex�cuter ces commandes, il faut passer sur la deuxi�me
console virtuelle (VT2) et lancer un interpr�teur de commandes.

<itemizedlist>
  <listitem><para>

Passez sur VT4 et consultez le fichier <filename>/var/log/syslog</filename> (avec l'�diteur
<command>nano</command>) en recherchant des messages d'erreur sp�cifiques. V�rifiez
ensuite la sortie de <command>dmesg</command>.
</para></listitem>
<listitem><para>
Si votre lecteur a �t� reconnu, v�rifiez la sortie de <command>dmesg</command>.
Vous devriez voir des lignes de ce type (elles ne se suivent pas n�cessairement)&nbsp;:

<informalexample><screen>
Probing IDE interface ide1...
hdc: TOSHIBA DVD-ROM SD-R6112, ATAPI CD/DVD-ROM drive
ide1 at 0x170-0x177,0x376 on irq 15
hdc: ATAPI 24X DVD-ROM DVD-R CD-R/RW drive, 2048kB Cache, UDMA(33)
Uniform CD-ROM driver Revision: 3.20
</screen></informalexample>

Si vous ne voyez rien de tel, il est probable que le contr�leur du lecteur
n'a pas �t� reconnu ou qu'il n'est pas du tout g�r�. Si vous connaissez le pilote n�cessaire
� ce contr�leur, vous pouvez le charger vous-m�me avec <command>modprobe</command>.
</para></listitem>
<listitem><para>
V�rifiez que le lecteur de c�d�rom est bien list� dans le r�pertoire <filename>/dev/</filename>.
Pour l'exemple ci-dessus, ce serait <filename>/dev/hdc</filename>.
Il doit exister aussi un fichier <filename>/dev/cdroms/cdrom0</filename>.
</para></listitem>
<listitem><para>
V�rifiez avec la commande <command>mount</command> que le c�d�rom est d�j� mont�.
Si non, montez-le vous-m�me&nbsp;:

<informalexample><screen>
$ mount /dev/<replaceable>hdc</replaceable> /cdrom
</screen></informalexample>

V�rifiez si cette commande a provoqu� des messages d'erreur.

</para></listitem>
<listitem><para>
V�rifiez si le DMA est activ�&nbsp;:

<informalexample><screen>
$ cd /proc/<replaceable>ide</replaceable>/<replaceable>hdc</replaceable>
$ grep using_dma settings
using_dma      1       0       1       rw
</screen></informalexample>

Un <quote>1</quote> dans la premi�re colonne apr�s <literal>using_dma</literal>
signifie qu'il est activ�. Dans ce cas, d�sactivez-le&nbsp;:

<informalexample><screen>
$ echo -n "using_dma:0" >settings
</screen></informalexample>

Assurez-vous que vous �tes bien dans le r�pertoire du p�riph�rique qui
correspond au lecteur de c�d�roms.
</para></listitem>
<listitem><para>
Si vous rencontrez des probl�mes pendant l'installation, v�rifiez l'int�grit� du c�d�rom
avec l'option qui se trouve en fin du menu principal de l'installateur.
Cette option peut aussi servir de test quand le c�d�rom est lu correctement.
</para></listitem>
</itemizedlist>
</para>
</sect3>
</sect2>  
  

  <sect2 condition="supports-floppy-boot" id="unreliable-floppies">
  <title>Fiabilit� des disquettes</title>

<para>

Le plus gros probl�me pour les gens qui utilisent des disquettes pour installer Debian 
est celui de leur fiabilit�.

</para><para>

La disquette d'amor�age est celle qui pose les probl�mes les plus d�licats
car elle est lue directement par le mat�riel, avant que Linux ne se charge. 
Souvent, le mat�riel ne lit pas aussi s�rement qu'un pilote Linux de lecteur de 
disquettes et peut s'arr�ter sans message d'erreur quand il lit
des donn�es incorrectes. Il peut aussi y avoir des erreurs avec les 
disquettes de pilotes et la plupart d'entre elles se signalent par des 
messages sur des erreurs d'entr�e/sortie (<emphasis>I/O error</emphasis>).

</para><para>

Si vous �tes bloqu� pendant l'installation avec une disquette, la premi�re 
chose � faire est de r��crire l'image sur une <emphasis>nouvelle</emphasis> disquette.
Reformater 
simplement l'ancienne disquette peut �tre insuffisant, m�me si la disquette est
reformat�e et �crite sans erreur. Il est parfois utile d'�crire les disquettes
sur un autre syst�me.

</para><para>

Un utilisateur a signal� qu'il avait d� r��crire les
images sur la disquette <emphasis>trois</emphasis> fois avant qu'elle
fonctionne&nbsp;; ensuite, tout s'est bien d�roul� avec cette
troisi�me disquette.

</para>
<para>
Normalement, il n'est pas n�cessaire de t�l�charger � nouveau l'image&nbsp;;
mais, en cas de probl�mes, il est toujours utile de v�rifier que les images ont �t�
correctement t�l�charg�es en v�rifiant les sommes MD5.

</para><para>

D'autres utilisateurs ont signal� qu'en red�marrant plusieurs fois avec la 
m�me disquette, on arrive parfois � faire amorcer le syst�me. Tout cela est 
d� � des d�faillances mat�rielles ou � des d�faillances du microprogramme
de contr�le des lecteurs de disquettes.

</para>
  </sect2>

  <sect2><title>Configuration d'amor�age</title>

<para>

Si le noyau se bloque pendant la phase d'amor�age, s'il ne reconna�t pas vos p�riph�riques
ou bien si les disques ne sont pas identifi�s correctement, la premi�re chose � faire 
est de v�rifier les param�tres d'amor�age, ainsi qu'il a �t� discut� 
dans la <xref linkend="boot-parms"/>.

</para>

<para>

Les probl�mes sont souvent r�solus en retirant p�riph�riques et 
extensions et en essayant de r�amorcer. <phrase arch="x86">Modems internes, 
cartes son et p�riph�riques Plug'n Play sont souvent la cause des probl�mes.</phrase>

</para>
<para>

Si votre machine poss�de beaucoup de m�moire, plus de 512&nbsp;Mo, et si 
l'installateur se bloque lors de l'amor�age du noyau, vous pouvez donner un
param�tre d'amor�age pour limiter le total de la m�moire que le noyau devra 
prendre en compte, par exemple <userinput>mem=512m</userinput>.

</para>
  </sect2>

  <sect2 arch="x86" id="i386-boot-problems">
  <title>Probl�mes communs pour &arch-title;</title>
<para>
Certains probl�mes d'installation peuvent �tre r�solus ou �vit�s en
passant des param�tres d'amor�age � l'installateur.
</para>
<para>
Certains syst�mes ont un lecteur de disquettes avec
�&nbsp;DCL invers�s&nbsp;�. Si vous obtenez des erreurs lors de la
lecture de disquettes et si vous savez que les disquettes sont
correctes, essayez le param�tre <userinput>floppy=thinkpad</userinput>.
</para>
<para>
Sur certains syst�mes, tels que les IBM PS/1 ou ValuePoint (qui ont des
pilotes de disques ST-506), le disque IDE peut ne pas �tre reconnu. Encore
une fois, essayez d'abord sans param�tre suppl�mentaire pour voir si
votre disque IDE est reconnu correctement. Si ce n'est pas le cas, d�terminez
la g�om�trie du disque (cylindres, t�tes et secteurs) et utilisez le
param�tre
<userinput>hd=<replaceable>cylindres</replaceable>,<replaceable>t�tes</replaceable>,<replaceable>secteurs</replaceable></userinput>.
</para>
<para>
Si vous avez une tr�s vieille machine et si le noyau g�le apr�s avoir
affich� <computeroutput>Checking 'hlt' instruction...</computeroutput>,
vous devez passer le param�tre d'amor�age <userinput>no-hlt</userinput>
qui d�sactive ce test.
</para>
<para>
Si votre �cran commence � afficher une image bizarre lors des d�marrages
du noyau, par exemple, un �cran enti�rement blanc, noir ou un �cran
contenant un m�lange de couleurs, alors tr�s certainement la carte
video du syst�me ne parvient pas � passer en mode
<emphasis>framebuffer</emphasis> correctement. Dans ce cas, essayez de
passer les param�tres d'amor�age <userinput>fb=false video=vga16:off</userinput>
pour d�sactiver la console <emphasis>framebuffer</emphasis>. Seules quelques
langues seront utilisables pour l'installation, � cause des caract�ristiques
limit�es de la console. R�f�rez-vous � la <xref linkend="boot-parms"/> pour plus
de pr�cisions.
</para>

<sect3>
<title>Le syst�me bloque lors de la configuration des services PCMCIA</title>
<para>
Quelques ordinateurs portables fabriqu�s par Dell s'arr�tent fr�quemment de
fonctionner lorsque la d�tection des p�riph�riques essaie d'acc�der � certaines
adresses mat�rielles. D'autres portables connaissent aussi des probl�mes
semblables. Si vous rencontrez un tel probl�me et si vous n'avez pas besoin de
services PCMCIA pendant l'installation, vous pouvez d�sactiver cette option avec le
param�tre <userinput>hw-detect/start_pcmcia=false</userinput>. Vous pourrez
configurer les services PCMCIA apr�s l'installation et �viter le probl�me des adresses des
ressources.
  </para>
<para>
Vous pouvez aussi d�marrer l'installateur en mode expert.
Vous pourrez ainsi indiquer les options pour les adresses des
ressources de votre mat�riel. Par exemple, si votre ordinateur est un des portables
Dell mentionn�s plus haut, vous pouvez indiquer
<userinput>exclude port 0x800-0x8ff</userinput>. Il existe une liste de
ces options communes dans la section sur les ressources syst�me du
<ulink
url="http://pcmcia-cs.sourceforge.net/ftp/doc/PCMCIA-HOWTO-1.html#ss1.12">HOWTO
PCMCIA</ulink>. Notez qu'il faut supprimer les virgules quand vous
passez ces valeurs � l'installateur.
</para>
</sect3>

<sect3>
<title>Le syst�me se bloque lors du chargement des modules USB</title>
<para>
Le noyau essaye normalement d'installer les modules USB et les pilotes
de claviers USB afin de permettre l'installation de quelques claviers USB
non standard. Cependant, sur certains syst�mes USB, le pilote provoque un
blocage lors du d�marrage.
Il est possible d'�viter ces probl�mes en d�sactivant le contr�leur USB
dans la configuration BIOS de votre carte m�re. Il est �galement possible
d'utiliser le param�tre <userinput>debian-installer/probe/usb=false</userinput> � l'invite de
d�marrage, ce qui emp�chera le chargement de ces modules.
</para>
</sect3>
</sect2>

 <sect2 arch="sparc" id="sparc-boot-problems">
  <title>Probl�mes communs sur &arch-title;</title>
<para>

Certains probl�mes communs lors de l'installation m�ritent une mention.
</para>
   <sect3>
   <title>Sortie video mal dirig�e</title>
<para>

Il est tr�s courant qu'une machine poss�de deux cartes vid�o, par exemple une carte
ATI et une carte Sun Creator 3D. D�s lors, apr�s le d�marrage du syst�me, il se peut
que la sortie vid�o soit mal dirig�e. Si c'est le cas, l'affichage est&nbsp;:

<informalexample><screen>
Remapping the kernel... done
Booting Linux...
</screen></informalexample>

Pour contourner ce probl�me, vous pouvez soit retirer l'une des cartes soit
d�sactiver celle qui n'est pas utilis�e pendant la phase d'amor�age de l'OpenProm
avec un param�tre du noyau. Par exemple, pour d�sactiver une carte ATI, amorcez
l'installateur avec le param�tre <userinput>video=atyfb:off</userinput>.
</para><para>

Notez que vous aurez � ajouter ce param�tre au fichier de configuration de silo
(modifiez <filename>/target/etc/silo.conf</filename> avant le  red�marrage).
Si vous installez X11, vous devez modifier aussi le pilote vid�o dans
<filename>/etc/X11/xorg.conf</filename>.
</para>
</sect3>

   <sect3>
   <title>Amor�age ou installation impossibles � partir d'un c�d�rom</title>
<para>

Il est connu que certains syst�mes Sparc s'amorcent difficilement par
c�d�rom et, m�me si l'amor�age r�ussit, l'installation �choue parfois inexplicablement.
La plupart des probl�mes ont �t� signal�s sur des syst�mes SunBlade.
</para>
<para>
Pour de tels syst�mes, nous recommandons d'amorcer l'installateur par le r�seau.
</para>
</sect3>
</sect2>


  <sect2 id="kernel-msgs">
  <title>Comprendre les messages du noyau au d�marrage</title>

<para>

Durant la s�quence de d�marrage, vous pouvez voir beaucoup de messages du 
genre 
<computeroutput>can't find <replaceable>something</replaceable></computeroutput>, 
ou <computeroutput><replaceable>something</replaceable> not present</computeroutput>, 
<computeroutput>can't initialize <replaceable>something</replaceable></computeroutput> ou m�me
<computeroutput>this driver release depends on <replaceable>something</replaceable> </computeroutput>. La plupart de ces messages sont sans cons�quence. 
Vous les voyez parce que le noyau du syst�me d'installation est con�u pour 
tourner sur des ordinateurs avec des p�riph�riques tr�s diff�rents. �videmment,
aucun ordinateur ne poss�de tous les p�riph�riques possibles,
et le syst�me d'exploitation recherche un p�riph�rique que vous ne poss�dez pas. Vous pourriez 
�galement voir le syst�me s'arr�ter pendant un moment. Cela arrive quand il 
attend la r�ponse d'un p�riph�rique qui n'est pas pr�sent sur votre syst�me. 
Si vous trouvez le temps de d�marrage du syst�me exag�r�ment long, vous 
pouvez cr�er un noyau personnalis� (voyez&nbsp;: <xref linkend="kernel-baking"/>).

</para>
  </sect2>


  <sect2 id="problem-report">
  <title>Signaler les probl�mes</title>
<para>

Si vous parvenez � la fin de la phase d'amor�age mais que vous ne parveniez 
pas � terminer l'installation, le choix <guimenuitem>Sauvegarder les journaux de d�bogage</guimenuitem>
du menu peut vous aider. 
Vous pouvez copier sur une disquette les journaux contenant les erreurs syst�me
et des informations sur la configuration ou vous pouvez les consulter avec un navigateur web.
Toutes ces informations peuvent donner des indications sur ce qui s'est mal pass� et 
sur la mani�re de le corriger. Si vous envoyez un rapport de bogue, vous
pouvez attacher cette information au rapport.

</para><para>

Pendant l'installation, d'autres messages int�ressants se trouvent dans le 
fichier <filename>/target/var/log/debian-installer/</filename>, et, quand
l'ordinateur a d�marr� le nouveau syst�me install�, ces messages se trouvent
dans le fichier <filename>/var/log/installer/</filename>.

</para>
  </sect2>

  <sect2 id="submit-bug">
  <title>Faire un rapport de bogue</title>
<para>

Si vous avez toujours des ennuis, faites un rapport de bogue. 
Nous vous demandons aussi d'envoyer des rapports sur les installations 
r�ussies, car nous cherchons des informations sur toutes les configurations
mat�rielles possibles.

</para>
<para>
Notez que votre rapport d'installation sera publi� dans le
syst�me de suivi des bogues, <emphasis>Bug Tracking System (BTS)</emphasis>,
de Debian et envoy� sur une
liste de diffusion publique. Votre adresse de courrier sera aussi rendue publique.
</para>

<para>

Si vous utilisez d�j� un syst�me Debian, vous pouvez installer les paquets
<classname>installation-report</classname> et <classname>reportbug</classname> 
(<command>aptitude install installation-report reportbug</command>).
Puis ex�cutez la commande <command>reportbug installation-reports</command>.

</para>
<para>

Vous pouvez aussi utiliser le mod�le suivant. Envoyez-le comme un 
rapport de bogue pour le paquet <classname>installation-reports</classname>,
� l'adresse <email>submit@bugs.debian.org</email>. 
Note du traducteur&nbsp;: veillez � utiliser la langue anglaise dans la
mesure du possible. Les contributeurs francophones du projet peuvent bien entendu
assister les d�veloppeurs et traduire certains rapports d'installation,
mais cette op�ration complexe risque d'aboutir � un r�sultat moins efficace.



<informalexample><screen>
Package: installation-reports

Boot method: &lt;Comment avez-vous amorc� l'installateur ? c�d�rom, disquette ? r�seau ?&gt;
Image version: &lt;URL compl�te de l'image t�l�charg�e&gt;
Date: &lt;Date et heure de l'installation&gt;

Machine: &lt;Description de la machine (p. ex. IBM Thinkpad R32)&gt;
Processor:
Memory:
Partitions: &lt;df -Tl ; mais nous pr�f�rons la table des partitions&gt;

R�sultat de lspci -nn et lspci -vnn :

Base System Installation Checklist:
[O] = OK, [E] = Error (please elaborate below), [ ] = didn't try it

Initial boot:           [ ]
Detect network card:    [ ]
Configure network:      [ ]
Detect CD:              [ ]
Load installer modules: [ ]
Detect hard drives:     [ ]
Partition hard drives:  [ ]
Install base system:    [ ]
Clock/timezone setup:   [ ]
User/password setup:    [ ]
Install tasks:          [ ]
Install boot loader:    [ ]
Overall install:        [ ]

Comments/Problems:

&lt;D�crivez l'installation, les pens�es, commentaires ou id�es de propositions
que vous avez eus pendant cette installation.&gt;
</screen></informalexample>

Dans le rapport de bogue, d�crivez le probl�me en incluant les derniers 
messages visibles du noyau dans le cas d'un plantage du noyau. D�crivez les 
�tapes que vous avez effectu�es pour arriver l�.

</para>
</sect2>
</sect1>
