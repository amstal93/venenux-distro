<!-- retain these comments for translator revision tracking -->
<!-- original version: 44026 -->

   <sect3 id="mdcfg">
   <title>Configuració de dispositius de discs múltiples (RAID de programari)</title>
<para>

Si teniu més d'un disc dur<footnote><para>

Per ser honests, podeu construir dispositius de discs múltiples fins
i tot a partir de particions en un mateix dispositiu físic, però això no us
portarà a res útil.

</para></footnote> en el vostre ordinador, podeu utilitzar el
<command>mdcfg</command> per configurar els vostres discs per obtenir
un millor rendiment i/o més seguretat en les vostres dades. El
resultat s'anomena <firstterm>dispositiu de discs múltiples</firstterm> (MD)
(o la seva variant més coneguda, <firstterm>RAID de programari</firstterm>).

</para><para>

Els MD són bàsicament un grapat de particions localitzades a diversos discs i
que es combinen juntes per formar un dispositiu <emphasis>lògic</emphasis>.
Aquest dispositiu es pot utilitzar llavors com una partició ordinària (p.ex.
en el <command>partman</command> la podeu formatar, assignar-li un punt de
muntatge, etc.).

</para><para>

Els beneficis que això aporta depèn del tipus de dispositiu MD que creeu.
Actualment se suporten:

<variablelist>
<varlistentry>

<term>RAID0</term><listitem><para>

S'enfoca principalment al rendiment. El RAID0 reparteix totes les dades
entrants en <firstterm>bandes</firstterm> i les distribueix de la mateixa
manera sobre cada disc de la matriu. Això pot augmentar la velocitat de
les operacions de lectura/escriptura, però quan un dels discs falla, ho
perdreu <emphasis>tot</emphasis> (part de la informació encara és al disc/s
sa, l'altra part <emphasis>era</emphasis> al disc que ha fallat).

</para><para>

L'ús típic del RAID0 és una partició per edició de vídeo.

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID1</term><listitem><para>

És adequat per les configuracions on la fiabilitat és la primera preocupació.
Consisteix en diverses (usualment dues) particions de la mateixa mida en les
quals es guarden les mateixes dades. Això significa essencialment tres coses.
La primera, que si un dels discs falla, encara teniu les dades repetides en
els discs restants. La segona, que només podeu utilitzar una fracció de la
capacitat disponible (més precisament, la mida de la partició més petita del
RAID). La tercera, que les lectures als fitxers s'equilibren entre els discs,
cosa que pot augmentar el rendiment en un servidor, com un servidor de fitxers,
que tendeix a carregar-se amb més lectures que no pas escriptures.

</para><para>

Opcionalment podeu tenir un disc de recanvi en la matriu que agafaria el lloc
del disc espatllat en cas de fallida.

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID5</term><listitem><para>

És un bon compromís entre velocitat, rendiment i redundància de les dades.
El RAID5 reparteix totes les dades entrants en bandes i les distribueix
equitativament entre tots els discs excepte un (similar al RAID0). A
diferència del RAID0, el RAID5 també calcula informació de
<firstterm>paritat</firstterm>, que s'escriu en el disc restant. El disc
de paritat no és estàtic (això seria l'anomenat RAID4), sinó que canvia
periòdicament, de manera que la informació de paritat es distribueixi
equitativament entre tots els discs. Quan un dels discs falla, la part
d'informació que falta es pot calcular a partir de les dades restants i
de la seva paritat. El RAID5 ha de consistir en almenys tres particions
actives. Opcionalment podeu tenir un disc de recanvi en la matriu, el
qual agafaria el lloc del disc espatllat en cas de fallida.

</para><para>

Com podeu veure, el RAID5 té un grau de rendiment similar al RAID1 però
aconsegueix menys redundància. Per altra banda, pot resultar una mica
més lent en les operacions d'escriptura que no pas el RAID0 degut al
càlcul de la informació de paritat.

</para></listitem>
</varlistentry>
</variablelist>

Per fer un sumari:

<informaltable>
<tgroup cols="5">
<thead>
<row>
  <entry>Tipus</entry>
  <entry>Dispositius mínims</entry>
  <entry>Dispositiu de recanvi</entry>
  <entry>Sobreviu a una fallida de disc?</entry>
  <entry>Espai disponible</entry>
</row>
</thead>

<tbody>
<row>
  <entry>RAID0</entry>
  <entry>2</entry>
  <entry>no</entry>
  <entry>no</entry>
  <entry>Mida de la partició més petita multiplicada pel nombre de
         dispositius en el RAID</entry>
</row>

<row>
  <entry>RAID1</entry>
  <entry>2</entry>
  <entry>opcional</entry>
  <entry>sí</entry>
  <entry>Mida de la partició més petita en el RAID</entry>
</row>

<row>
  <entry>RAID5</entry>
  <entry>3</entry>
  <entry>opcional</entry>
  <entry>sí</entry>
  <entry>Mida de la partició més petita multiplicada pel (nombre de
         dispositius en el RAID menys ú)
  </entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

Si voleu conèixer més sobre el RAID de programari, doneu un cop d'ull al
<ulink url="&url-software-raid-howto;">COM ES FA del RAID de programari</ulink>.

</para><para>

Per crear un dispositiu MD, necessiteu tenir les particions de què
desitgeu que consti marcades per a ser usades en un RAID. (Això és fa
en el menú <guimenu>Paràmetres de la partició</guimenu> del
<command>partman</command> on hauríeu de seleccionar <menuchoice>
<guimenu>Utilitza-ho com:</guimenu> <guimenuitem>volum físic per a
RAID</guimenuitem> </menuchoice>).

</para><warning><para>

El suport per MD és una addició relativament nova a l'instal·lador.
Podeu experimentar problemes per alguns nivells de RAID i en
combinació amb alguns carregadors de l'arrencada si intenteu
utilitzar MD pel sistema de fitxers arrel (<filename>/</filename>).
Pels usuaris experimentats, pot ser possible solucionar temporalment
alguns d'aquests problemes executant manualment alguns passos de la
configuració o de la instal·lació des d'un intèrpret d'ordres.

</para></warning><para>

A continuació, hauríeu d'elegir <guimenuitem>Configura el RAID de
programari</guimenuitem> del menú principal del <command>partman</command>.
(El menú tan sols apareixerà després de marcar al menús una partició per
utilitzar-la com <guimenuitem>volum físic per RAID</guimenuitem>.)
A la primera pantalla del <command>mdcfg</command> seleccioneu simplement
<guimenuitem>Crea dispositius MD</guimenuitem>. Se us presentarà una
llista dels tipus de dispositius MD suportats, dels quals n'haureu
d'escollir un (p.ex. RAID1). El que segueixi dependrà del tipus de MD que
seleccioneu.

</para>

<itemizedlist>
<listitem><para>

El RAID0 és senzill &mdash; se us proporcionarà una llista de les
particions RAID disponibles i la vostra única tasca serà seleccionar
la partició que formarà el MD.

</para></listitem>
<listitem><para>

El RAID1 és una mica més delicat. Primer, se us demanarà que introduïu
el nombre de dispositius actius i el nombre de dispositius de recanvi que
formaran el MD. A continuació, necessitareu seleccionar d'una llista de
les particions RAID disponibles aquelles que seran actives i aquelles que
serviran de recanvi. El compte de particions seleccionades ha de ser igual
al nombre proporcionat uns segons abans. No us preocupeu. Si feu un error i
seleccioneu un nombre diferent de particions, el &d-i; no us permetrà
continuar fins que corregiu la qüestió.

</para></listitem>
<listitem><para>

El RAID5 té un procediment de configuració similar al RAID1 amb l'excepció
que necessitareu utilitzar almenys <emphasis>tres</emphasis> particions
actives.

</para></listitem>
</itemizedlist>

<para>

És perfectament possible tenen diversos tipus de MD a la vegada. Per
exemple, si teniu tres discs durs de 200 GiB dedicats a MD, i cadascun
conté dues particions de 100 GiB, podeu combinar les primeres particions
dels tres discs en un RAID0 (partició ràpida de 300 GiB per edició de vídeo)
i utilitzar les altres tres particions (2 actives i 1 de recanvi) per
un RAID1 (una partició bastant fiable de 100 GiB pel
<filename>/home</filename>).

</para><para>

Després que hàgiu configurat les dispositius MD al vostre gust, podeu
seleccionar <guimenuitem>Finalitza</guimenuitem> el <command>mdcfg</command>
per retornar al <command>partman</command> per crear els sistemes de fitxers
en els nous dispositius MD i assignar-los els atributs usuals com els punts
de muntatge.

</para>
   </sect3>
