<!-- retain these comments for translator revision tracking -->
<!-- original version: 44002 untranslated -->

  <sect2 arch="x86"><title>Booting from a CD-ROM</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!--
CD #1 of official Debian CD-ROM sets for &arch-title; will present a
<prompt>boot:</prompt> prompt on most hardware. Press
<keycap>F4</keycap> to see the list of kernel options available
from which to boot. Just type your chosen flavor name (idepci,
vanilla, compact, bf24) at the <prompt>boot:</prompt> prompt
followed by &enterkey;.

</para><para>

If your hardware doesn't support booting of multiple images, put one
of the other CDs in the drive. It appears that most SCSI CD-ROM drives
do not support <command>isolinux</command> multiple image booting, so users
with SCSI CD-ROMs should try either CD2 (vanilla) or CD3 (compact),
or CD5 (bf2.4).

</para><para>

CD's 2 through 5 will each boot a
different ``flavor'' depending on which CD-ROM is
inserted. See <xref linkend="kernel-choice"/> for a discussion of the
different flavors.  Here's how the flavors are laid out on the
different CD-ROMs:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

Allows a selection of kernel images to boot from (the idepci flavor is
the default if no selection is made).

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Boots the <quote>vanilla</quote> flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Boots the <quote>compact</quote> flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Boots the <quote>idepci</quote> flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Boots the <quote>bf2.4</quote> flavor.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

  </sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
     until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Booting from a DOS partition</title>

&boot-installer-intro-hd.xml;

<para>

Boot into DOS (not Windows) without any drivers being loaded.  To do
this, you have to press <keycap>F8</keycap> at exactly the right
moment (and optionally select the <quote>safe mode command prompt only</quote>
option).  Enter the subdirectory for the flavor you chose, e.g.,

<informalexample><screen>
cd c:\install
</screen></informalexample>.

Next, execute <command>install.bat</command>.
The kernel will load and launch the installer system.

</para><para>

Please note, there is currently a loadlin problem (#142421) which
precludes <filename>install.bat</filename> from being used with the
bf2.4 flavor. The symptom of the problem is an
<computeroutput>invalid compressed format</computeroutput> error.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <title>Booting from Linux Using <command>LILO</command> or
  <command>GRUB</command></title>

<para>
To boot the installer from hard disk, you must first download
and place the needed files as described in <xref linkend="boot-drive-files"/>.
</para>

<para>
If you intend to use the hard drive only for booting and then
download everything over the network, you should download the
<filename>netboot/debian-installer/&architecture;/initrd.gz</filename> file and its
corresponding kernel
<filename>netboot/debian-installer/&architecture;/linux</filename>. This will allow you
to repartition the hard disk from which you boot the installer, although you
should do so with care.
</para>

<para>
Alternatively, if you intend to keep an existing partition on the hard
drive unchanged during the install, you can download the
<filename>hd-media/initrd.gz</filename> file and its kernel, as well as
copy a CD iso to the drive (make sure the file is named ending in
<literal>.iso</literal>). The installer can then boot from the drive
and install from the CD image, without needing the network.
</para>

<para>
For <command>LILO</command>, you will need to configure two
essential things in <filename>/etc/lilo.conf</filename>:
<itemizedlist>
<listitem><para>

to load the <filename>initrd.gz</filename> installer at boot time;

</para></listitem>
<listitem><para>

have the <filename>vmlinuz</filename> kernel use a RAM disk as
its root partition.

</para></listitem>
</itemizedlist>

Here is a <filename>/etc/lilo.conf</filename> example:

</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

For more details, refer to the
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> and
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry> man pages. Now run
<userinput>lilo</userinput> and reboot.

</para><para>

The procedure for <command>GRUB</command> is quite similar. Locate your
<filename>menu.lst</filename> in the <filename>/boot/grub/</filename>
directory (sometimes in the <filename>/boot/boot/grub/</filename>),
add the following lines:

<informalexample><screen>
title  New Install
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

and reboot.

</para><para>

Note that the value of the <userinput>ramdisk_size</userinput> may need to be
adjusted for the size of the initrd image.
From here on, there should be no difference between <command>GRUB</command>
or <command>LILO</command>.

</para>
  </sect2>

  <sect2 arch="x86" condition="bootable-usb" id="usb-boot">
  <title>Booting from USB Memory Stick</title>
<para>

Let's assume you have prepared everything from <xref
linkend="boot-dev-select"/> and <xref linkend="boot-usb-files"/>.  Now
just plug your USB stick into some free USB connector and reboot the
computer.  The system should boot up, and you should be presented with
the <prompt>boot:</prompt> prompt.  Here you can enter optional boot
arguments, or just hit &enterkey;.

</para>
  </sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <title>Booting from Floppies</title>
<para>

You will have already downloaded the floppy images you needed and
created floppies from the images in <xref linkend="create-floppy"/>.
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see
<xref linkend="rescue-replace-kernel"/>. -->

</para><para>

To boot from the installer boot floppy, place it in the primary floppy
drive, shut down the system as you normally would, then turn it back
on.

</para><para>

For installing from an LS-120 drive (ATAPI version) with a set of
floppies, you need to specify the virtual location for the floppy
device.  This is done with the <emphasis>root=</emphasis> boot
argument, giving the device that the ide-floppy driver maps the device
to. For example, if your LS-120 drive is connected as the first IDE
device (master) on the second cable, you enter
<userinput>install root=/dev/hdc</userinput> at the boot prompt.

</para><para>

Note that on some machines, <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Delete</keycap></keycombo> does not
properly reset the machine, so a <quote>hard</quote> reboot is recommended.  If
you are installing from an existing operating system (e.g., from a DOS
box) you don't have a choice. Otherwise, please do a hard reboot when
booting.

</para><para>

The floppy disk will be accessed, and you should then see a screen
that introduces the boot floppy and ends with the <prompt>boot:</prompt>
prompt.

</para><para>

Once you press &enterkey;, you should see the message
<computeroutput>Loading...</computeroutput>, followed by
<computeroutput>Uncompressing Linux...</computeroutput>, and
then a screenfull or so of information about the hardware in your
system.  More information on this phase of the boot process can be
found below in <xref linkend="kernel-msgs"/>.

</para><para>

After booting from the boot floppy, the root floppy is
requested. Insert the root floppy and press &enterkey;, and the
contents are loaded into memory. The installer program
<command>debian-installer</command> is automatically launched.

</para>
  </sect2>

  <sect2 arch="x86" id="boot-tftp"><title>Booting with TFTP</title>

&boot-installer-intro-net.xml;

<para>

There are various ways to do a TFTP boot on i386.

</para>

   <sect3><title>NIC or Motherboard that support PXE</title>
<para>

It could be that your Network Interface Card or Motherboard provides
PXE boot functionality.
This is a <trademark class="trade">Intel</trademark> re-implementation
of TFTP boot. If so, you may be able to configure your BIOS to boot from the
network.

</para>
   </sect3>

   <sect3><title>NIC with Network BootROM</title>
<para>

It could be that your Network Interface Card provides
TFTP boot functionality.

</para><para condition="FIXME">

Let us (<email>&email-debian-boot-list;</email>) know how did you manage it.
Please refer to this document.

</para>
   </sect3>

   <sect3><title>Etherboot</title>
<para>

The <ulink url="http://www.etherboot.org">etherboot project</ulink>
provides bootdiskettes and even bootroms that do a TFTPboot.

</para>
   </sect3>
  </sect2>

  <sect2 arch="x86"><title>The Boot Prompt</title>
<para>

When the installer boots, you should be presented with a friendly graphical
screen showing the Debian logo and the boot prompt:

<informalexample><screen>
Press F1 for help, or ENTER to boot:
</screen></informalexample>

At the boot prompt
you can either just press &enterkey; to boot the installer with
default options or enter a specific boot method and, optionally, boot
parameters.

</para><para>

Information on available boot methods and on boot parameters which might
be useful can be found by pressing <keycap>F2</keycap> through
<keycap>F8</keycap>. If you add any parameters to
the boot command line, be sure to type the boot method (the default is
<userinput>install</userinput>) and a space before the first parameter (e.g.,
<userinput>install fb=false</userinput>).

<note><para>

If you are installing the system via a remote management device that
provides a text interface to the VGA console, you may not be able to
see the initial graphical splash screen upon booting the installer;
you may even not see the boot prompt. Examples of these devices include
the text console of Compaq's <quote>integrated Lights Out</quote> (iLO)
and HP's <quote>Integrated Remote Assistant</quote> (IRA).
You can blindly press F1<footnote>

<para>

In some cases these devices will require special escape sequences to
enact this keypress, for example the IRA uses <keycombo> <keycap>Ctrl</keycap>
<keycap>F</keycap> </keycombo>,&nbsp;<keycap>1</keycap>.

</para>

</footnote> to bypass this screen and view the help text. Once you are
past the splash screen and at the help text your keystrokes will be echoed
at the prompt as expected. To prevent the installer from using the
framebuffer for the rest of the installation, you will also want to add
<userinput>fb=false</userinput> to the boot prompt,
as described in the help text.

</para></note>
</para>

  </sect2>
