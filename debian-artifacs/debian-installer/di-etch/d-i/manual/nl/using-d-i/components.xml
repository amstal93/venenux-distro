<!-- original version: 43965 -->

 <sect1 id="module-details">
 <title>Individuele modules gebruiken</title>
<para>

In deze sectie beschrijven we elke module van het installatiesysteem
in detail. De modules zijn gegroepeerd in stadia die herkenbaar
zouden moeten zijn voor gebruikers. Ze worden gepresenteerd in de
volgorde waarin ze tijdens de installatie verschijnen. Merk op dat
mogelijk niet alle modules voor elke installatie worden gebruikt; welke
modules daadwerkelijk worden gebruikt is afhankelijk van de door u
gekozen installatiemethode en van uw apparatuur.

</para>

  <sect2 id="di-setup">
  <title>Instelling van het installatiesysteem en hardware configuratie</title>
<para>

Laten we er vanuit gaan dat uw systeem met het Debian installatiesysteem is
opgestart en dat u het eerste scherm voor u heeft. Op dat moment is de
functionaliteit van &d-i; nog vrij beperkt. Het weet nog niet veel over uw
apparatuur, welke taal u prefereert en zelfs de taken die het moet uitvoeren.
Maak u geen zorgen. &d-i; is slim genoeg om uw apparatuur te scannen, de nog
ontbrekende modules te localiseren en zichzelf op te waarderen tot een
volwaardig installatiesysteem.

U zult echter &d-i; nog altijd moeten helpen met enige informatie die het niet
automatisch kan bepalen (zoals de selectie van de taal en toetsenbordindeling
die u verkiest of van de gewenste spiegelserver).

</para><para>

U zult merken dat &d-i; verschillende keren tijdens dit stadium een
<firstterm>hardwareherkenning</firstterm> uitvoert. De eerste keer is dit
specifiek gericht op de apparatuur (bijvoorbeeld uw CD-speler of netwerkkaart)
die nodig is voor het laden van de benodigde modules van het installatiesysteem.
Omdat bij deze eerste keer mogelijk nog niet alle stuurprogramma's beschikbaar
zijn, moet de hardwareherkenning later in het proces worden herhaald.

</para>

&module-lowmem.xml;
&module-localechooser.xml;
&module-kbd-chooser.xml;
&module-s390-netdevice.xml;
&module-ddetect.xml;
&module-cdrom-detect.xml;
&module-iso-scan.xml;
&module-anna.xml;
&module-netcfg.xml;
&module-choose-mirror.xml;

  </sect2>

  <sect2 id="di-partition">
  <title>Schijfindeling en selectie van aanhechtpunten</title>
<para>

Op dit punt, nadat voor de laatste keer hardwareherkenning heeft plaatsgevonden,
heeft &d-i; zijn volledige sterkte bereikt: aangepast aan de behoeften van de
gebruiker en klaar voor het echte werk.

Zoals de titel van deze sectie aangeeft, is de voornaamste taak van de volgende
modules het indelen van uw harde schijven, het creëren van bestandssystemen en
het toekennen van aanhechtpunten. Optioneel kunnen nauw gerelateerde zaken als
LVM of RAID worden geconfigureerd.

</para>

&module-s390-dasd.xml;
&module-partman.xml;
&module-autopartkit.xml;
&module-partitioner.xml;
&module-partconf.xml;
&module-mdcfg.xml;
&module-partman-lvm.xml;
&module-partman-crypto.xml;
  </sect2>

  <sect2 id="di-system-setup">
  <title>Algemene instellingen van het systeem</title>
<para>

Na de schijfindeling stelt het installatiesysteem nog enkele vragen die
gebruikt zullen worden voor enkele algemene instellingen van het systeem
dat op het punt staat geïnstalleerd te worden.

</para>

&module-tzsetup.xml;
&module-clock-setup.xml;
&module-user-setup.xml;
  </sect2>

  <sect2 id="di-install-base">
  <title>Installatie van het Basissysteem</title>
<para>

Hoewel dit stadium het minst problematisch is, beslaat het wel een significant
deel van de tijd van de installatie omdat hier het volledige basissysteem wordt
opgehaald, geverifieerd en uitgepakt. Als u een langzame computer of
netwerkverbinding heeft, kan dit enige tijd in beslag nemen.

</para>

&module-base-installer.xml;
  </sect2>


  <sect2 id="di-install-software">
  <title>Installing Additional Software</title>
<para>

Nadat het basissysteem is geïnstalleerd, beschikt u over een bruikbaar systeem,
maar met beperkte functionaliteit. De meeste gebruikers zullen aanvullende
programmatuur op het systeem willen installeren om het aan te passen aan hun
behoeften. Het installatiesysteem maakt dit mogelijk. Deze stap kan, zeker
indien u een langzame computer of netwerkverbinding heeft, nog meer tijd in
beslag nemen dan de installatie van het basissysteem

</para>

&module-apt-setup.xml;
&module-pkgsel.xml;
  </sect2>

  <sect2 id="di-make-bootable">
  <title>Zorgen dat uw systeem kan worden opgestart</title>
<para condition="supports-nfsroot">

Als u een werkstation zonder schijfeenheden installeert, is opstarten vanaf
een locale schijfeenheid uiteraard geen optie en zal deze stap worden
overgeslagen. <phrase arch="sparc">Desgewenst kunt u OpenBoot instellen om
standaard vanaf het netwerk op te starten; zie <xref
linkend="boot-dev-select-sun"/>.</phrase>

</para>

&module-os-prober.xml;
&module-alpha-aboot-installer.xml;
&module-hppa-palo-installer.xml;
&module-x86-grub-installer.xml;
&module-x86-lilo-installer.xml;
&module-ia64-elilo-installer.xml;
&module-mips-arcboot-installer.xml;
&module-mipsel-colo-installer.xml;
&module-mipsel-delo-installer.xml;
&module-powerpc-yaboot-installer.xml;
&module-powerpc-quik-installer.xml;
&module-s390-zipl-installer.xml;
&module-sparc-silo-installer.xml;
&module-nobootloader.xml;
  </sect2>

  <sect2 id="di-finish">
  <title>De installatie afronden</title>
<para>

Dit zijn de laatste acties die moeten worden uitgevoerd voordat de
computer kan worden opgestart met uw nieuwe systeem. Het bestaat
voornamelijk uit het afwerken van losse eindjes van &d-i;.

</para>

&module-finish-install.xml;
  </sect2>

  <sect2 id="di-miscellaneous">
  <title>Diverse modules</title>
<para>

De modules in deze sectie zijn normaalgesproken niet betrokken in het
installatieproces, maar zijn beschikbaar op de achtergrond voor gebruik
als er iets mis gaat.

</para>

&module-save-logs.xml;
&module-cdrom-checker.xml;
&module-shell.xml;
&module-network-console.xml;
  </sect2>
 </sect1>
