<!-- original version: 40671 -->

<chapter id="boot-new">
 <title>De computer opstarten met uw nieuwe Debian systeem</title>

 <sect1 id="base-boot"><title>Het moment van de waarheid</title>
<para>

De eerste keer dat u uw computer opnieuw opstart met uw nieuwe Debian
systeem is een soort <quote>vuurdoop</quote>.

</para><para>

Als uw nieuwe Debian systeem niet goed opstart, probeer dan op te starten
met uw originele installatie opstartmedium of gebruik, als u deze heeft,
een 'Custom boot floppy' en start uw systeem opnieuw op. Waarschijnlijk
zal u enkele opstartargumenten moeten opgeven, zoals
<userinput>root=<replaceable>root</replaceable></userinput>, waarbij
<replaceable>root</replaceable> staat voor uw root-partitie (bijvoorbeeld
<filename>/dev/hda1</filename> of <filename>/dev/sda1</filename>).
U kunt echter ook de instructies in <xref linkend="rescue"/> volgen voor
het gebruik van de herstelmodus van het installatiesysteem.

<!-- FJP: Is opstartargumenten consistent met andere plaatsen? //-->
<!-- FJP: Custom boot floppy moet nader worden uitgewerkt in manual //-->

</para>

  <sect2 arch="m68k"><title>Een BVME 6000 opstarten</title>

<para>

Als u zojuist een installatie zonder schijfeenheden heeft uitgevoerd
op een BVM of Motorola VMEbus machine: geef dan, nadat het systeem het
programma <command>tftplilo</command> vanaf de TFTP-server heeft geladen,
één van de volgende commando's bij de <prompt>LILO Boot:</prompt> prompt:

<itemizedlist>
<listitem><para>

<userinput>b6000</userinput> gevolgd door &enterkey;
om een BVME 4000/6000 op te starten

</para></listitem><listitem><para>

<userinput>b162</userinput> gevolgd door &enterkey;
om een MVME162 op te starten

</para></listitem><listitem><para>

<userinput>b167</userinput> gevolgd door &enterkey;
om een MVME166/167 op te starten

</para></listitem>
</itemizedlist>

</para>
  </sect2>

  <sect2 arch="m68k"><title>Een Macintosh opstarten</title>

<para>

Ga naar de map die de installatiebestanden bevat en start de
<command>Penguin</command> opstartlader op, waarbij u de
<keycap>command</keycap>-toets ingedrukt houdt. Ga vervolgens naar de
<userinput>Settings</userinput>-dialoog
(<keycombo> <keycap>command</keycap> <keycap>T</keycap> </keycombo>), en zoek
de invoerregel voor opstartparameters voor de kernel, die er ongeveer als volgt
zou moeten uitzien: <userinput>root=/dev/ram video=font:VGA8x16</userinput>.

</para><para>

U dient deze regel aan te passen naar
<userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>.
Vervang daarbij <replaceable>yyyy</replaceable> met de Linux-naam van de
partitie waarop u het systeem heeft geïnstalleerd (bijvoorbeeld
<filename>/dev/sda1</filename>), zoals u eerder heeft genoteerd.
De <userinput>video=font:VGA8x8</userinput> wordt vooral aangeraden voor
gebruikers met kleinere schermen. De kernel zou een mooier (6x11)
lettertype kiezen, maar de console driver voor dat lettertype kan het
systeem doen vastlopen, dus 8x16 of 8x8 kiezen is in dit stadium
veiliger. U kunt dit op elk moment wijzigen.

</para><para>

Als u GNU/Linux niet automatisch wilt laten opstarten bij elke systeemstart,
zorg er dan voor dat de optie <userinput>Auto Boot</userinput> niet
geselecteerd is. Sla uw instellingen op in het bestand
<filename>Prefs</filename> met de optie <userinput>Save Settings As
Default</userinput>.

</para><para>

Kies nu <userinput>Boot Now</userinput> (<keycombo>
<keycap>command</keycap> <keycap>B</keycap> </keycombo>) om het nieuw
geïnstalleerde GNU/Linux-systeem op te starten in plaats van het
RAMdisk-gebaseerde installatiesysteem.

</para><para>

Debian zou nu moeten opstarten, en u zou de zelfde meldingen moeten zien
als bij het opstarten van het installatiesysteem, gevolgd door een aantal
nieuwe berichten.

</para>
  </sect2>

  <sect2 arch="powerpc"><title>OldWorld PowerMacs</title>

<para>

Als de machine niet goed opstart na afronding van de installatie en stopt met
een <prompt>boot:</prompt> prompt, probeer dan <userinput>Linux</userinput>
gevolgd door &enterkey; in te geven. (De standaard opstartconfiguratie in
<filename>quik.conf</filename> is Linux genaamd.) De namen die in
<filename>quik.conf</filename> zijn gedefinieerd, worden getoond als u bij de
<prompt>boot:</prompt> prompt de <keycap>Tab</keycap>-toets indrukt. U kunt
ook proberen opnieuw het installatieprogramma op te starten en vervolgens het
bestand <filename>/target/etc/quik.conf</filename> te wijzigen dat daar is
weggeschreven door de stap <guimenuitem>Quik op een harde schijf
installeren</guimenuitem>. Informatie over het werken met
<command>quik</command> is beschikbaar op
<ulink url="&url-powerpc-quik-faq;"></ulink>.
<!-- FJP: Hoe is "Install Quick on a Hard Disk" vertaald? //-->

</para><para>

Geef, om MacOS op te starten zonder dat de nvram wordt ingesteld naar
de standaardwaarden, bij de OpenFirmware-prompt het commando
<userinput>bye</userinput> (ervan uitgaande dat MacOS niet van de machine
is verwijderd). Houd, om een OpenFirmware-prompt te verkrijgen, de toetsen
<keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>o</keycap>
<keycap>f</keycap> </keycombo> ingedrukt terwijl u de machine inschakelt.
Houd, als u de de wijzigingen in de OpenFirmware nvram wilt herstellen,
de toetsen <keycombo> <keycap>command</keycap> <keycap>option</keycap>
<keycap>p</keycap> <keycap>r</keycap> </keycombo> ingedrukt terwijl u
de machine inschakelt.

</para><para>

Als u <command>BootX</command> gebruikt om het geïnstalleerde systeem op
te starten, kunt u gewoon de gewenste kernel selecteren in de map
<filename>Linux Kernels</filename>, de optie ramdisk deselecteren en het
root-apparaat (bijvoorbeeld <userinput>/dev/hda8</userinput>) toevoegen
dat overeenkomt met uw installatie.

</para>
  </sect2>

  <sect2 arch="powerpc"><title>NewWorld PowerMacs</title>

<para>

Op G4- en iBook-systemen, kunt u de toets <keycap>option</keycap> ingedrukt
houden. U krijgt dan een grafisch scherm met een knop voor elk besturingssysteem
dat kan worden opgestart: &debian; zal een knop met daarop een klein icoon van
een penguin zijn.

</para><para>

Als u MacOS heeft behouden en als dat op enig moment de OpenFirmware-variabele
<envar>boot-device</envar> zou wijzigen, dan dient u OpenFirmware te herstellen
naar zijn standaard configuratie. Hiervoor houdt u de toetsen <keycombo>
<keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap>
<keycap>r</keycap> </keycombo> ingedrukt terwijl u de machine inschakelt.

</para><para>

De namen die zijn gedefinieerd in <filename>yaboot.conf</filename> zullen
worden getoond als u bij de <prompt>boot:</prompt> prompt de toets
<keycap>Tab</keycap> indrukt.

</para><para>

Het herstellen van OpenFirmware op G3- of G4-systemen zal standaard resulteren
in het opstarten van &debian; (als u de schijf juist heeft ingedeeld en de
Apple_Bootstrap partitie als eerste heeft geplaatst). Als u &debian; op een
SCSI harde schijf en MacOS op een IDE harde schijf heeft, werkt dit mogelijk
niet en zal u in OpenFirmware de variabele <envar>boot-device</envar> moeten
instellen. Normaal gesproken doet <command>ybin</command> dit automatisch.

</para><para>

Nadat u &debian; voor de eerste keer heeft opgestart, kunt u aanvullende
opties die u wenst (zoals voor 'dual boot') toevoegen aan
<filename>/etc/yaboot.conf</filename> en <command>ybin</command> starten om uw
opstartpartitie bij te werken met de gewijzigde configuratie. Aanvullende
informatie is beschikbaar op
<ulink url="&url-powerpc-yaboot-faq;">yaboot HOWTO</ulink>.

</para>
   </sect2>
 </sect1>

&mount-encrypted.xml;

 <sect1 id="login">
 <title>Aanloggen</title>

<para>

Nadat het systeem is opgestart, wordt de aanlogprompt getoond.
Log aan met uw persoonlijke gebruikersaccount door de gebruikersnaam en
het wachtwoord die u tijdens de installatie heeft geselecteerd, in te
geven. Uw systeem is nu klaar voor gebruik.

</para><para>

Als u een nieuwe gebruiker bent, adviseren wij om, terwijl u begint uw systeem
te gebruiken, ook de documentatie te verkennen die al is geïnstalleerd tijdens
het installatieproces.

</para><para>

De documentatie bij programma's die u heeft geïnstalleerd, kunt u vinden in
submappen onder <filename>/usr/share/doc/</filename>; deze submappen hebben
de naam van de geïnstalleerde pakketten. De APT User's Guide bijvoorbeeld voor
het gebruik van <command>apt</command> om andere programma's op uw systeem te
installeren, kunt u vinden in
<filename>/usr/share/doc/apt/guide.html/index.html</filename>.
<!-- FJP: Mappen hebben niet de naam van het programma, maar van het pakket! //-->
<!-- FJP: Is dit wel zo'n geschikt voorbeeld? //-->

</para><para>

Daarnaast zijn er enkele bijzondere mappen onder
<filename>/usr/share/doc/</filename>. Linux HOWTO handleidingen worden
in <emphasis>.gz</emphasis>-formaat geïnstalleerd in
<filename>/usr/share/doc/HOWTO/en-txt/</filename>. Na installatie van
<command>dhelp</command> vindt u in
<filename>/usr/share/doc/HTML/index.html</filename> een inhoudsopgave
van documentatie die met een browser kan worden bekeken.

</para><para>

Een eenvoudige manier om deze documenten te bekijken, is met
<userinput>cd /usr/share/doc/</userinput>, gevolgd door
<userinput>lynx .</userinput> (de punt staat voor de huidige map).
<!-- FJP: wordt lynx nog steeds standaard geïnstalleerd? Nee! //-->

</para><para>

U kunt ook <userinput>info <replaceable>command</replaceable></userinput> of
<userinput>man <replaceable>command</replaceable></userinput> gebruiken om
documentatie te bekijken over de meeste opdrachten die vanaf de opdrachtregel
gegeven kunnen worden. Ook als u een opdracht ingeeft gevolgd door
<userinput>--help</userinput>, krijgt u over het algemeen een korte samenvatting
over het gebruik van de betreffende opdracht. Als de uitvoer van een
opdracht niet op één scherm past, probeer dan om <userinput> | more</userinput>
achter de opdracht in te geven; hierdoor zal de uitvoer pauzeren voordat deze
voorbij de bovenkant van het scherm schuift. U kunt een overzicht krijgen van
alle opdrachten die met (een) bepaalde letter(s) beginnen door direct achter de
letter(s) tweemaal op <keycap>tab</keycap> te drukken.
<!-- FJP: origineel: tab is <keycap> //-->

</para><para>

Voor een meer volledige introductie van Debian en GNU/Linux verwijzen wij naar
<filename>/usr/share/doc/debian-guide/html/noframes/index.html</filename>.

</para>

 </sect1>
</chapter>
