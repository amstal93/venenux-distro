<!-- Start of file howto/installation-howto.xml -->
<!-- $Id: installation-howto.xml 45291 2007-02-20 14:06:48Z fjp $ -->

<appendix id="installation-howto">
<title
>Cài đặt thế nào</title>

<para
>Tài liệu này diễn tả cách cài đặt &debian; &releasename; dành cho &arch-title; (kiến trúc <quote
>&architecture;</quote
>) bằng &d-i; mới. Nó là sự giải thích nhanh của tiến trình cài đặt mà nên chứa tất cả thông tin cần thiết để cài đặt trong phần lớn trường hợp. Khi thông tin thêm có thể là hữu ích, chúng tôi sẽ liên kết đến sự giải thích chi tiết hơn trong <link linkend="debian_installation_guide"
>Hướng dẫn cài đặt &debian;</link
>. </para>

 <sect1 id="howto-preliminaries">
 <title
>Chuẩn bị</title>
<para
><phrase condition="unofficial-build"
>Trình cài đặt Debian vẫn còn trong tình trạng thử nghiệm B.</phrase
> Nếu bạn gặp lỗi trong khi cài đặt, xem <xref linkend="submit-bug"/> để tìm thông tin về cách thông báo lỗi. Nếu bạn cần biết gì không nằm trong phạm vị của tài liệu này, xin hãy hỏi câu hoặc trong hộp thư chung « debian-boot » (&email-debian-boot-list;) hoặc trên IRC (kênh #debian-boot trên mạng OFTC). </para>
 </sect1>

 <sect1 id="howto-getting-images">
 <title
>Khởi động trình cài đặt</title>
<para
><phrase condition="unofficial-build"
> Để tìm một số liên kết nhanh đến ảnh đĩa CD, xem <ulink url="&url-d-i;"
>trang chủ &d-i;</ulink
>. </phrase
> Nhóm debian-cd cung cấp nhiều bản xây dựng của ảnh đĩa CD bằng &d-i; trên <ulink url="&url-debian-cd;"
>trang đĩa CD Debian</ulink
>. Để tìm thông tin thêm về nơi cần lấy đĩa CD, xem <xref linkend="official-cdrom"/>. </para
><para
>Một số phương pháp cài đặt riêng cần thiết ảnh kiểu khác với ảnh đĩa CD. <phrase condition="unofficial-build"
> <ulink url="&url-d-i;"
>Trang chủ &d-i;</ulink
> chứa liên kết đến ảnh kiểu khác. </phrase
> <xref linkend="where-files"/> diễn tả cách tìm ảnh trên máy nhân bản Debian. </para
><para
>Những tiết đoạn phụ dưới đây cung cấp chi tiết về ảnh nào bạn nên lấy để thực hiện mỗi phương pháp cài đặt. </para>

  <sect2 id="howto-getting-images-cdrom">
  <title
>CD-ROM</title>

<para
>Có hai ảnh đĩa CD kiểu « netinst » (cài đặt qua mạng) có thể được dùng để cài đặt &releasename; bằng &d-i;. Hai ảnh này được định để khởi động từ đĩa CD và cài đặt các gói thêm qua mạng. Sự khác giữa hai ảnh này là ảnh netinst đầy chứa các gói cơ bản, còn bạn cần phải tải chúng xuống Mạng khi dùng ảnh thẻ kinh doanh. Hoặc bạn có thể lấy một đĩa CD kích cỡ đầy đủ mà không cần mạng để cài đặt. Bạn cần có chỉ đĩa CD thứ nhất của bộ này. </para
><para
>Hãy tải về kiểu nào bạn thích, và chép ra nó vào một đĩa CD. <phrase arch="x86"
>Để khởi động đĩa CD này, bạn có thể cần phải thay đổi cấu hình BIOS, như được diễn tả trong <xref linkend="bios-setup"/>.</phrase
> <phrase arch="powerpc"
> Để khởi động máy kiểu PowerMac từ đĩa CD, hãy bấm phím <keycap
>c</keycap
> trong khi khởi động. Xem <xref linkend="boot-cd"/> để tìm phương pháp khác khởi động từ đĩa CD. </phrase
> </para>
  </sect2>

  <sect2 condition="supports-floppy-boot" id="howto-getting-images-floppy">
  <title
>Đĩa mềm</title>
<para
>Nếu bạn không có khả năng khởi động từ đĩa CD, bạn vẫn có thể tải về ảnh đĩa mềm để cài đặt Debian. Bạn cần có hai tập tin <filename
>floppy/boot.img</filename
>, <filename
>floppy/root.img</filename
> và có lẽ một hay nhiều đĩa trình điều khiển. </para
><para
>Đĩa mềm khởi động là đĩa mang nhãn <filename
>boot.img</filename
>. Đĩa mềm này, khi được khởi động, sẽ nhắc bạn nạp đĩa mềm thứ hai &mdash; đĩa mang nhãn <filename
>root.img</filename
>. </para
><para
>Nếu bạn định cài đặt qua mạng, bình thường bạn cần có tập tin <filename
>floppy/net-drivers-1.img</filename
>. Cho mang kiểu PCMCIA hoặc USB, và một số thẻ mạng ít thường hơn, bạn cũng cần có một đĩa mềm điều khiển thứ hai, <filename
>floppy/net-drivers-2.img</filename
>. </para
><para
>Nếu bạn có đĩa CD, nhưng không thể khởi động được từ nó, hãy khởi động từ đĩa mềm thay thế, rồi sử dụng tập tin <filename
>floppy/cd-drivers.img</filename
> trên đĩa mềm trình điều khiển, để cài đặt xong bằng đĩa CD. </para
><para
>Đĩa mềm là một của những vật chứa ít tin cậy nhất, vậy bạn hãy chuẩn bị quản lý nhiều đĩa sai (xem <xref linkend="unreliable-floppies"/>). Mỗi tập tin <filename
>.img</filename
> (ảnh) được tải về thì chiếm một đĩa mềm riêng; bạn có thể sử dụng lệnh « dd » để ghi nó vào « /dev/fd0 », hoặc phương pháp khác (xem <xref linkend="create-floppy"/> để tìm chi tiết). Vì bạn sẽ cần phải quản lý nhiều đĩa mềm, khuyên bạn nhãn mỗi đĩa riêng. </para>
  </sect2>

  <sect2 condition="bootable-usb" id="howto-getting-images-usb">
  <title
>Thanh bộ nhớ USB</title>
<para
>Cũng có thể cài đặt từ thiết bị lưu trữ USB rời. Thí dụ, một dây khoá USB có thể làm vật chứa cài đặt Debian hữu ích mà bạn có thể mang suốt. </para
><para
>Phương pháp dễ nhất chuẩn bị thanh bộ nhớ USB là tai về kho <filename
>hd-media/boot.img.gz</filename
>, rồi chạy chương trình « gunzip » để giải nén ảnh 256 MB từ tập tin đó. Hãy ghi ảnh này một cách trực tiếp vào thanh bộ nhớ, mà cần phải có kích cỡ ít nhất 256 MB. Tất nhiên tiến trình này sẽ hủy hoàn toàn dữ liệu nào tồn tại trên thanh bộ nhớ đó. Sau đó, hãy gắn kết thanh bộ nhớ, mà lúc bây giờ chứa hệ thống tập tin kiểu FAT. Bước kế tiếp, tải về một ảnh đĩa CD cài đặt qua mạng (netinst) Debian, rồi sao chép tập tin này vào thanh bộ nhớ; bất cứ tên tập tin nào là thích hợp miễn là nó kết thúc bằng <literal
>.iso</literal
>. </para
><para
>Có một số phương pháp khác, dẻo hơn, để thiết lập thanh bộ nhớ để dùng &d-i;, cũng có thể làm cho nó hoạt động được với thanh bộ nhớ nhỏ hơn. Để tìm chi tiết, xem <xref linkend="boot-usb-files"/>. </para
><para arch="x86"
>Một số BIOS riêng có khả năng khởi động trực tiếp vật chứa USB, còn một số điều không có. Bạn có thể cần phải cấu hình BIOS để khởi động từ <quote
>ổ đĩa rời</quote
>, ngay cả <quote
>USB-ZIP</quote
>, để làm cho nó khởi động được từ thiết bị USB. Để tìm mẹo có ích và chi tiết, xem  <xref linkend="usb-boot"/>. </para
><para arch="powerpc"
>Tiến trình khởi động hệ thống kiểu Macintosh từ thiết bị lưu trữ USB đòi hỏi phải tự sử dụng Open Firmware (phần vững mở). Để tìm chỉ dẫn, xem <xref linkend="usb-boot"/>. </para>
  </sect2>

  <sect2 id="howto-getting-images-netboot">
  <title
>Khởi động từ mạng</title>
<para
>Cũng có thể khởi động &d-i; một cách hoàn toàn từ mạng. Mỗi phương pháp khởi động từ mạng phụ thuộc vào kiến trúc và thiết lập khởi động mạng riêng của bạn. Những tập tin nằm trong thư mục <filename
>netboot/</filename
> có thể được dùng để khởi động &d-i; từ mạng. </para
><para arch="x86"
>Phương pháp thiết lập dễ nhất rất có thể là tiến trình khởi động từ mạng kiểu PXE. Hãy giải nến (gunzip và bỏ .tar) tập tin <filename
>netboot/pxeboot.tar.gz</filename
> vào thư mục <filename
>/var/lib/tftpboot</filename
> hoặc nơi nào thích hợp với trình phục vụ tftp của bạn. Thiết lập trình phục vụ DHCP để gởi tên tập tin <filename
>/pxelinux.0</filename
> qua cho trình khách, và nếu có may, mọi điều nên đơn giản hoạt động được. Để tìm chỉ dẫn chi tiết, xem <xref linkend="install-tftp"/>. </para>
  </sect2>

  <sect2 id="howto-getting-images-hard-disk">
  <title
>Khởi động từ đĩa cứng</title>
<para
>Có thể khởi động trình cài đặt khi không dùng vật chứa rời nào, chỉ dùng một phần cứng đã có, mà có thể chứa hệ điều hành khác. Hãy tải về hai tập tin <filename
>hd-media/initrd.gz</filename
>, <filename
>hd-media/vmlinuz</filename
>, và một ảnh đĩa CD Debian vào thư mục cấp đầu của đĩa cứng đó. Chắc là ảnh đĩa CD có tên tập tin kết thúc bằng <literal
>.iso</literal
>. Sau đó, đơn giản cần khởi động Linux bằng initrd. <phrase arch="x86"
> <xref linkend="boot-initrd"/> diễn tả một cách làm như thế. </phrase
> </para>
  </sect2>
 </sect1>

 <sect1 id="howto-installation">
<title
>Cài đặt</title>
<para
>Một khi trình cài đặt khởi chạy, bạn sẽ xem màn hình ban đầu. Hãy bấm &enterkey; để khởi động, hoặc đọc các chỉ dẫn về những phương pháp khởi động và tham số khác (xem <xref linkend="boot-parms"/>).  </para
><para
>Sau một thời gian, bạn sẽ được nhắc chọn ngôn ngữ của mình. Hãy sử dụng phím mũi tên để chọn ngôn ngữ, rồi bấm &enterkey; để tiếp tục. Sau đó, bạn sẽ được nhắc chọn quốc gia, trong danh sách gồm quốc gia nơi nói ngôn ngữ bạn. Nếu chỗ bạn không nằm trong danh sách ngắn, có sẵn một danh sách mọi quốc gia trên khắp thế giới. </para
><para
>Có lẽ bạn sẽ được nhắc xác nhận bố trí bàn phím của mình. Hãy chọn bố trí bàn phím thích hợp, hoặc chọn điều mặc định nếu bạn chưa chắc. </para
><para
>Sau đó, bạn có thể nghỉ trong khi trình cài đặt Debian phát hiện một số phần cứng của bạn, và tải phần còn lại của nó từ đĩa CD, đĩa mềm, USB v.v. </para
><para
>Tiếp theo, trình cài đặt sẽ thử phát hiện phần cứng mạng của bạn, để thiết lập thao tác chạy mạng bằng DHCP. Nếu bạn chưa lên mạng, hoặc không có khả năng DHCP, bạn sẽ có dịp tự cấu hình mạng. </para
><para
>Đây là giai đoạn phân vùng đĩa. Trước tiên, bạn sẽ có dịp phân vùng tự động hoặc một đĩa hoàn toàn, hoặc sức chứa còn rảnh có sẵn trên một đĩa (khả năng phân vùng đã hướng dẫn). Tùy chọn này được khuyến khích cho người dùng mới hoặc người nào vội vàng. Nếu bạn không muốn tự động phân vùng, hãy chọn <guimenuitem
>Bằng tay</guimenuitem
> trong trình đơn. </para
><para arch="x86"
>Nếu bạn có một phân vùng thêm kiểu DOS hay Windows mà bạn muốn bảo tồn, hãy rất cẩn thận tự động khởi động. Nếu bạn chọn tự phân vùng, có thể sử dụng trình cài đặt để thay đổi kích cỡ của phân vùng FAT hay NTFS tồn tại để tạo đủ chỗ cho bản cài đặt Debian: đơn giản hãy chọn phân vùng đó rồi ghi rõ kích cỡ mới cho nó. </para
><para
>Trên màn hình kế tiếp, bạn sẽ xem bảng phân vùng, cách sẽ định dạng phân vùng, và nơi sẽ gắn kết chúng. Hãy chọn phân vùng cần sửa đổi hoặc xóa bỏ. Nếu bạn đã phân vùng tự động, bạn nên có khả năng chọn <guimenuitem
>Phân vùng xong và ghi các thay đổi vào đĩa</guimenuitem
> trong trình đơn, để sử dụng cấu hình được thiết lập. Ghi nhớ : cần phải gán ít nhất một phân vùng dành cho chỗ trao đổi (swap space), cũng gắn kết một phân vùng đến <filename
>/</filename
>. <xref linkend="partitioning"/> có thông tin thêm về cách tạo phân vùng. </para
><para
>Lúc này, &d-i; định dạng các phân vùng của bạn, rồi bắt đầu cài đặt hệ thống cơ bản, mà có thể hơi lâu. Sau đó, hạt nhân sẽ được cài đặt. </para
><para
>Bước cuối cùng là cài đặt một bộ tải khởi động (boot loader). Nếu trình cài đặt phát hiện hệ điều hành khác nằm trên máy tính của bạn, nó sẽ thêm mỗi HĐH vào trình đơn khởi động, cũng cho bạn biết như thế. <phrase arch="x86"
>Mặc định là GRUB sẽ được cài đặt vào mục ghi khởi động chủ của đĩa cứng thứ nhất, mà thường là sự chọn tốt. Bạn sẽ có dịp bỏ qua sự chọn đó và cài đặt GRUB vào nơi khác. </phrase
> </para
><para
>&d-i; lúc bây giờ sẽ báo bạn biết khi tiến trình cài đặt đó mới chạy xong. Hãy gỡ bỏ đĩa CD-ROM hay vật chứa khởi động khác, rồi bấm &enterkey; để khởi động lại máy tính. Nó nên khởi động vào giai đoạn kế tiếp của tiến trình cài đặt, được diễn tả trong <xref linkend="boot-new"/>. </para
><para
>Nếu bạn muốn tìm thông tin thêm về tiến trình cài đặt, xem <xref linkend="d-i-intro"/>. </para>
 </sect1>

 <sect1 id="howto-installation-report">
 <title
>Gởi báo cáo cài đặt cho chúng tôi</title>
<para
>Nếu bạn đã cài đặt thành công bằng &d-i;, xin hãy mất thời gian để cung cấp một báo cáo. Phưng pháp báo cáo dễ nhất là cài đặt gói « reportbug » (dùng lệnh <command
>aptitude install reportbug</command
>), cấu hình <classname
>reportbug</classname
> như được giải thích trong <xref linkend="mail-outgoing"/>, rồi chạy lệnh <command
>reportbug installation-reports</command
>. </para
><para
>Nếu bạn chưa cài đặt xong, rất có thể là bạn đã gặp lỗi trong trình cài đặt Debian. Để cải tiến phần mềm cài đặt, chúng tôi cần phải biết lỗi này: bạn báo cáo nhé. Bạn có thể thông báo lỗi trong báo cáo cài đặt; nếu tiến trình cài đặt thất bại hoàn toàn, xem <xref linkend="problem-report"/>. </para>
 </sect1>

 <sect1 id="howto-installation-finally">
 <title
>Vậy cuối cùng&hellip;</title>
<para
>Chúng tôi hy vọng tiến trình cài đặt Debian chạy được cho bạn, cũng là bạn tìm thấy Debian là hữu hiệu. Đề nghị bạn đọc <xref linkend="post-install"/>. </para>
 </sect1>
</appendix>
<!--   End of file howto/installation-howto.xml -->
