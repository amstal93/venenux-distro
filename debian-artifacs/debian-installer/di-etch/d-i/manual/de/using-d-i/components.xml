<!-- retain these comments for translator revision tracking -->
<!-- original version: 43965 -->

 <sect1 id="module-details">
 <title>Die einzelnen Komponenten</title>
<para>

In diesem Kapitel beschreiben wir detailliert jede Komponente des
Installers. Die Komponenten sind in (für Benutzer sinnvolle) Gruppen
gegliedert. Sie werden in der Reihenfolge vorgestellt, in der sie auch
während der Installation vorkommen. Bedenken Sie, dass nicht alle Module
bei jeder Installation benutzt werden; welche Komponenten jeweils benutzt
werden, hängt von der Installationsmethode und von Ihrer Hardware ab.

</para>

  <sect2 id="di-setup">
  <title>Den Debian-Installer einrichten und Konfiguration der Hardware</title>
<para>

Nehmen wir an, dass der &d-i; gebootet hat und Sie sein erstes
Bild sehen. In diesem Moment sind die Fähigkeiten des Debian-Installers noch sehr
begrenzt. Er weiß noch nicht viel über Ihre Hardware, Ihre bevorzugte Sprache
oder die Aufgabe, die er erledigen soll. Machen Sie sich deswegen keine Sorgen.
Da der &d-i; sehr clever ist, kann er automatisch Ihre Hardware erkennen,
seine restlichen noch nicht geladenen Komponenten finden und sich selbst zu
einem leistungsfähigen Installationssystem machen.

Allerdings benötigt er immer noch Ihre Hilfe, um einige Informationen zu bekommen,
die er nicht automatisch erkennen kann (wie die bevorzugte Sprache, die
Tastaturbelegung oder den gewünschten Netzwerk-Spiegelserver).

</para><para>

Sie werden feststellen, dass der &d-i; mehrmals während dieses Schrittes
eine <firstterm>Hardware-Erkennung</firstterm> durchführt. Das erste Mal geht
es hauptsächlich um die Hardware, die benötigt wird, um die Installer-Komponenten
zu laden (z.B. Ihr CD-ROM-Laufwerk oder Ihre Netzwerkkarte).
Weil allerdings im ersten Durchlauf möglicherweise noch nicht alle
Treiber zur Verfügung stehen, muss die Hardware-Erkennung eventuell später noch
einmal wiederholt werden.

</para>

&module-lowmem.xml;
&module-localechooser.xml;
&module-kbd-chooser.xml;
&module-s390-netdevice.xml;
&module-ddetect.xml;
&module-cdrom-detect.xml;
&module-iso-scan.xml;
&module-anna.xml;
&module-netcfg.xml;
&module-choose-mirror.xml;

  </sect2>

  <sect2 id="di-partition">
  <title>Partitionierung und Auswahl der Einhängepunkte im Dateisystem</title>
<para>

Jetzt, nachdem die Hardware-Erkennung ein letztes Mal durchgeführt wurde,
sollte der &d-i; seine volle Stärke erreicht haben, optimal abgestimmt auf
die Bedürfnisse des Benutzers und bereit, mit der richtigen Arbeit zu beginnen.

Wie der Titel dieses Abschnitts vermuten lässt, liegt die Hauptaufgabe der
jetzt folgenden Komponenten im Partitionieren Ihrer Laufwerke, Erstellen von
Dateisystemen, Festlegen der Einhängepunkte sowie damit eng verwandter Themen
wie der Konfiguration von LVM- oder RAID-Laufwerken.

</para>

&module-s390-dasd.xml;
&module-partman.xml;
&module-autopartkit.xml;
&module-partitioner.xml;
&module-partconf.xml;
&module-mdcfg.xml;
&module-partman-lvm.xml;
&module-partman-crypto.xml;
  </sect2>

  <sect2 id="di-system-setup">
  <title>Einrichten des Systems</title>
<para>

Nach der Partitionierung stellt der Installer ein paar weitere Fragen, die
für das Einrichten des zu installierenden Systems benötigt werden.
	
</para>

&module-tzsetup.xml;
&module-clock-setup.xml;
&module-user-setup.xml;
</sect2>

  <sect2 id="di-install-base">
  <title>Installation des Basissystems</title>
<para>

Obwohl dieser Schritt der am wenigsten problematische ist, benötigt er einen
erheblichen Teil der Zeit, da hier das komplette Basissystem heruntergeladen,
überprüft und entpackt wird. Wenn Sie einen langsamen Rechner oder eine langsame
Netzwerkverbindung haben, kann das schon einige Zeit dauern.

</para>

&module-base-installer.xml;
  </sect2>
  


  <sect2 id="di-install-software">
  <title>Installation zusätzlicher Software</title>
<para>

Nachdem das Basissystem installiert ist, haben Sie ein benutzbares aber noch
eingeschränktes System. Die meisten Benutzer werden zusätzliche Software auf
dem System installieren wollen, um es an ihre Anforderungen anzupassen, und
der Installer ermöglicht dies auch. Dieser Schritt kann sogar länger als die
Installation des Basissystems dauern, wenn Sie einen langsamen Rechner oder
langsames Netzwerk haben.

</para>
&module-apt-setup.xml;
&module-pkgsel.xml;
  </sect2>
	
  <sect2 id="di-make-bootable">
  <title>Ihr System bootfähig machen</title>

<para condition="supports-nfsroot">

Wenn Sie gerade eine Workstation ohne Festplatte installieren, ist das
Booten von Festplatte natürlich keine sinnvolle Sache; deswegen wird
der Schritt in diesem Fall übersprungen.
<phrase arch="sparc">Sie sollten vielleicht OpenBoot so einstellen, dass
standardmäßig per Netzwerk gebootet wird; siehe dazu
<xref linkend="boot-dev-select-sun"/>.</phrase>

</para>

&module-os-prober.xml;
&module-alpha-aboot-installer.xml;
&module-hppa-palo-installer.xml;
&module-x86-grub-installer.xml;
&module-x86-lilo-installer.xml;
&module-ia64-elilo-installer.xml;
&module-mips-arcboot-installer.xml;
&module-mipsel-colo-installer.xml;
&module-mipsel-delo-installer.xml;
&module-powerpc-yaboot-installer.xml;
&module-powerpc-quik-installer.xml;
&module-s390-zipl-installer.xml;
&module-sparc-silo-installer.xml;
&module-nobootloader.xml;
  </sect2>

  <sect2 id="di-finish">
  <title>Die Installation beenden</title>
<para>

Dies sind die letzten Dinge, die noch erledigt werden müssen, bevor Sie
Ihr neues System booten können. Hauptsächlich geht es darum, nach der Installation
ein wenig aufzuräumen.

</para>

&module-finish-install.xml;
  </sect2>

  <sect2 id="di-miscellaneous">
  <title>Verschiedenes</title>
<para>

Die Komponenten, die wir hier auflisten, sind normalerweise nicht am
Installationsprozess beteiligt, warten aber im Hintergrund, um den
Benutzer zu unterstützen, falls etwas schief läuft.

</para>

&module-save-logs.xml;
&module-cdrom-checker.xml;
&module-shell.xml;
&module-network-console.xml;
  </sect2>
 </sect1>
