<!-- retain these comments for translator revision tracking -->
<!-- original version: 44410 -->

 <sect1 id="linux-upgrade">
 <title>&debian; von einem anderen Unix/Linux-System aus installieren</title>

<para>

Dieses Kapitel beschreibt, wie man &debian; von einem vorhandenen
Unix- oder Linux-System aus installiert, ohne den
menügeführten Installer zu verwenden, der im Rest dieses Handbuchs
beschrieben wird. Dieses <quote>Einmal-quer-durch-die-Installation</quote>-HowTo
wurde erbeten von Leuten, die von Red Hat, Mandrake oder Suse zu
&debian; wechseln. In diesem Kapitel gehen wir davon aus, dass Sie
ein wenig Erfahrung mit *nix-Befehlen haben sowie mit der Navigation
durch das Dateisystem. In diesem Abschnitt symbolisiert ein <prompt>$</prompt>
einen Befehl, der im Homeverzeichnis des Benutzers
ausgeführt wird, während <prompt>#</prompt> bedeutet, dass das
Kommando im Debian-chroot ausgeführt wird.

</para><para>

Sobald Sie das neue Debian-System nach Ihren Wünschen konfiguriert haben,
können Sie Ihre evtl. vorhandenen eigenen Dateien hinüber verschieben und
loslegen. Deswegen wird dies auch die
<quote>zero-downtime</quote>-&debian;-Installation (Installation ohne eine
Zeitspanne, während der das System nicht verfügbar ist) genannt. Es ist ebenso
ein geschickter Weg, mit Hardwarekomponenten zurechtzukommen, die sich sonst
nicht gut mit verschiedenen Boot- oder Installationsmedien vertragen.

</para>

  <sect2>
  <title>Es geht los!</title>
<para>

Benutzen Sie die *nix-Partitionierungsprogramme des vorhandenen Systems, um
Ihre Festplatte nach Bedarf neu zu partitionieren; erstellen Sie zumindest
ein neues Dateisystem plus Swap. Sie benötigen ungefähr 350MB Speicher für
eine Nur-Konsolen-Installation oder ca. 1GB, wenn Sie vorhaben, X zu
installieren (und noch mehr, wenn Sie Desktop-Umgebungen wie GNOME oder KDE
installieren möchten).

</para><para>

Dateisysteme auf Ihren Partitionen erzeugen: um ein Dateisystem auf einer
Partition zu erstellen, zum Beispiel ein ext3-Dateisystem auf
<filename>/dev/hda6</filename> (dies soll in diesem Beispiel unsere
root-Partition sein):

<informalexample><screen>
# mke2fs -j /dev/<replaceable>hda6</replaceable>
</screen></informalexample>

Um stattdessen ein ext2-Dateisystem zu erzeugen, lassen Sie das
<userinput>-j</userinput> weg.

</para><para>

Initialisieren und aktivieren Sie den Swap (passen Sie die Partitionsnummer
für Ihre geplante Debian-Swap-Partition an):

<informalexample><screen>
# mkswap /dev/<replaceable>hda5</replaceable>
# sync; sync; sync
# swapon /dev/<replaceable>hda5</replaceable>
</screen></informalexample>

Hängen Sie eine Partition (wahrscheinlich die neue) als <filename>/mnt/debinst</filename> ins
Dateisystem ein (das Installationsverzeichnis; es wird das
root-(<filename>/</filename>)-Dateisystem des neuen Debian). Der
Einhängepunkt (<filename>/mnt/debinst</filename>) ist rein willkürlich
gewählt; es wird später noch öfter darauf verwiesen.

<informalexample><screen>
# mkdir /mnt/debinst
# mount /dev/<replaceable>hda6</replaceable> /mnt/debinst
</screen></informalexample>

</para>
 <note><para>

Falls Sie Teile des Dateisystems (z.B. /usr) auf andere Partitionen speichern
und eingebinden möchten, müssen Sie diese Verzeichnisse manuell erstellen
und einbinden, bevor Sie mit dem nächsten Schritt weitermachen.

 </para></note>
  </sect2>

  <sect2>
  <title><command>debootstrap</command> installieren</title>
<para>

Das Programm, das auch der Debian-Installer benutzt und das als offizielle
Methode angesehen wird, ein Debian-Basissystem zu installieren, ist
<command>debootstrap</command>. Es nutzt <command>wget</command> und
<command>ar</command>, aber ansonsten hängt es nur von
<classname>/bin/sh</classname> und grundsätzlichen
Unix-/Linuxwerkzeugen<footnote>

<para>

Dazu gehören die GNU Core Utilities und Kommandos wie
<command>sed</command>, <command>grep</command>, <command>tar</command> und
<command>gzip</command>.

</para>

</footnote> ab. Installieren Sie <command>wget</command>
und <command>ar</command>, falls sie noch nicht auf Ihrem laufenden System
vorhanden sind; dann laden Sie <command>debootstrap</command> herunter und
installieren es ebenfalls.

</para>

<!-- The files linked to here are from 2004 and thus currently not usable
<para>

Wenn Sie ein rpm-basiertes System haben, können Sie alien verwenden,
um das .deb in ein .rpm-Paket umzuwandeln oder Sie laden sich eine
rpm-Version von
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink>
herunter.

</para>
-->

<para>

Oder Sie nutzen die folgende Prozedur, um es manuell zu installieren.
Erstellen Sie ein Arbeitsverzeichnis, in das Sie die .deb-Pakete entpacken.

<informalexample><screen>
# mkdir work
# cd work
</screen></informalexample>

Das <command>debootstrap</command>-Binary finden Sie im Debian-Archiv
(achten Sie darauf, die richtige Datei für Ihre Architektur zu verwenden).
Laden Sie das <command>debootstrap</command>.deb aus dem
<ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/">
Pool</ulink> herunter, kopieren Sie es in das Arbeitsverzeichnis work und
extrahieren Sie daraus die Dateien. Sie benötigen root-Rechte, um die
Binär-Dateien zu installieren.

<informalexample><screen>
# ar -xf debootstrap_0.X.X_all.deb
# cd /
# zcat /full-path-to-work/work/data.tar.gz | tar xv
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Starten Sie <command>debootstrap</command></title>
<para>

<command>debootstrap</command> kann die benötigten Dateien direkt vom
Archiv herunterladen, wenn Sie es starten. Sie können in dem folgenden Befehl jeden
Debian-Archivspiegel-Server statt <userinput>&archive-mirror;/debian</userinput>
einsetzen, vorzugsweise einen Spiegel in Ihrer Nähe. Eine Liste aller Spiegelserver
finden Sie auf <ulink url="http://www.debian.org/misc/README.mirrors"></ulink>.

</para><para>

Wenn Sie eine &debian;-&releasename-cap;-CD eingelegt und in <filename>/cdrom</filename>
eingebunden haben, können Sie statt der http-URL auch eine file-URL angeben:
<userinput>file:/cdrom/debian/</userinput>.

</para><para>

Setzen Sie in dem folgenden <command>debootstrap</command>-Befehl für
<replaceable>ARCH</replaceable> eine der folgenden Architekturbezeichnungen
ein:

<userinput>alpha</userinput>, 
<userinput>amd64</userinput>,
<userinput>arm</userinput>,
<userinput>hppa</userinput>, 
<userinput>i386</userinput>, 
<userinput>ia64</userinput>, 
<userinput>m68k</userinput>,
<userinput>mips</userinput>, 
<userinput>mipsel</userinput>, 
<userinput>powerpc</userinput>, 
<userinput>s390</userinput> oder
<userinput>sparc</userinput>.

<informalexample><screen>
# /usr/sbin/debootstrap --arch ARCH &releasename; \
    /mnt/debinst http://ftp.us.debian.org/debian
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Das Basissystem konfigurieren</title>
<para>

Sie haben jetzt ein echtes, aber noch etwas mageres Debian-System auf
der Festplatte. Wechseln Sie mit <command>chroot</command> hinein:

<informalexample><screen>
# LANG=C chroot /mnt/debinst /bin/bash
</screen></informalexample>

Danach müssen Sie unter Umständen die Definition der Terminal-Variable
anpassen, so dass sie mit dem Debian-Basissystem kompatibel ist, zum Beispiel:

<informalexample><screen>
# export TERM=<replaceable>xterm-color</replaceable>
</screen></informalexample>

</para>

   <sect3>
   <title>Partitionen einbinden</title>
<para>

Sie müssen die Datei <filename>/etc/fstab</filename> erzeugen.

<informalexample><screen>
# editor /etc/fstab
</screen></informalexample>

Hier ein Beispiel, das Sie sich anpassen können:

<informalexample><screen>
# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump  pass
/dev/XXX         /             ext3    defaults                 0     1
/dev/XXX         /boot         ext3    ro,nosuid,nodev          0     2

/dev/XXX         none          swap    sw                       0     0
proc             /proc         proc    defaults                 0     0

/dev/fd0         /media/floppy auto    noauto,rw,sync,user,exec 0     0
/dev/cdrom       /media/cdrom  iso9660 noauto,ro,user,exec      0     0

/dev/XXX         /tmp          ext3    rw,nosuid,nodev          0     2
/dev/XXX         /var          ext3    rw,nosuid,nodev          0     2
/dev/XXX         /usr          ext3    rw,nodev                 0     2
/dev/XXX         /home         ext3    rw,nosuid,nodev          0     2
</screen></informalexample>

Nutzen Sie den Befehl <userinput>mount -a</userinput>, um alle Dateisysteme,
die Sie in <filename>/etc/fstab</filename> angegeben haben, einzubinden; um
die Dateisysteme einzeln einzubinden, benutzen Sie:

<informalexample><screen>
# mount /path   # z.B.: mount /usr
</screen></informalexample>

Aktuelle Debian-Systeme haben Einhängepunkte (Mountpoints) für
Wechseldatenträger in <filename>/media</filename>, behalten aber aus
Kompatibilitätsgründen auch symbolische Links in <filename>/</filename>.
Erstellen Sie diese nach Bedarf, zum Beispiel mit:

<informalexample><screen>
# cd /media
# mkdir cdrom0
# ln -s cdrom0 cdrom
# cd /
# ln -s media/cdrom
</screen></informalexample>

Sie können das proc-Dateisystem mehrfach einbinden und in frei wählbare
Verzeichnisse, obwohl <filename>/proc</filename> dafür üblich ist. Wenn Sie nicht
<userinput>mount -a</userinput> verwendet haben, stellen Sie sicher, dass
proc eingebunden ist, bevor Sie weitermachen:

<informalexample><screen>
# mount -t proc proc /proc
</screen></informalexample>

</para><para>

Der Befehl <userinput>ls /proc</userinput> sollte jetzt ein nicht-leeres
Verzeichnis zeigen. Falls dies fehlschlägt, können Sie vielleicht proc
außerhalb der chroot-Umgebung einbinden:

<informalexample><screen>
# mount -t proc proc /mnt/debinst/proc
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Die Zeitzone setzen</title>
<para>

Eine Option in der Datei <filename>/etc/default/rcS</filename> legt fest, ob
das System die Hardware-CMOS-Uhr als UTC-Zeit oder als lokale Zeit
interpretiert. Mit dem folgenden Befehl können Sie diese Option setzen sowie
Ihre Zeitzone festlegen:

<informalexample><screen>
# editor /etc/default/rcS
# tzconfig
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Das Netzwerk konfigurieren</title>
<para>

Um Ihr Netzwerk einzurichten, müssen Sie die Dateien
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>,
<filename>/etc/hostname</filename> und
<filename>/etc/hosts</filename> anpassen.

<informalexample><screen>
# editor /etc/network/interfaces 
</screen></informalexample>

Hier sind ein paar einfache Beispiele aus
<filename>/usr/share/doc/ifupdown/examples</filename>:

<informalexample><screen>
######################################################################
# /etc/network/interfaces -- configuration file for ifup(8), ifdown(8)
# See the interfaces(5) manpage for information on what options are 
# available.
######################################################################

# We always want the loopback interface (die Loopback-Schnittstelle wird
# immer benötigt).
auto lo
iface lo inet loopback

# To use dhcp (wenn Sie DHCP benutzen möchten):
#
# auto eth0
# iface eth0 inet dhcp

# An example static IP setup: (broadcast and gateway are optional)
# (ein Beispiel für eine statische IP-Einstellung / broadcast und gateway
# sind hierbei optional):
# auto eth0
# iface eth0 inet static
#     address 192.168.0.42
#     network 192.168.0.0
#     netmask 255.255.255.0
#     broadcast 192.168.0.255
#     gateway 192.168.0.1
</screen></informalexample>

Tragen Sie die Adresse Ihres/Ihrer Nameserver(s) sowie
Suchregeln in <filename>/etc/resolv.conf</filename> ein:

<informalexample><screen>
# editor /etc/resolv.conf
</screen></informalexample>

Eine einfache beispielhafte <filename>/etc/resolv.conf</filename>:

<informalexample><screen>
search hqdom.local
nameserver 10.1.1.36
nameserver 192.168.9.100
</screen></informalexample>

Geben Sie den Hostnamen Ihres Systems ein (zwischen 2 und 63 Stellen lang):

<informalexample><screen>
# echo DebianHostName &gt; /etc/hostname
</screen></informalexample>

Und hier ein Beispiel für <filename>/etc/hosts</filename> mit
IPv6-Unterstützung:

<informalexample><screen>
127.0.0.1 localhost DebianHostName

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
</screen></informalexample>

Wenn Sie mehrere Netzwerkkarten haben, sollten Sie die Namen der
Treibermodule in <filename>/etc/modules</filename> in die richtige
Reihenfolge bringen. Während des Bootens wird dann jede Karte die
Schnittstellenbezeichnung (eth0, eth1, etc.) bekommen, die Sie erwarten.

</para>
   </sect3>

   <sect3>
   <title>Apt konfigurieren</title>

<para>

Debootstrap hat eine grundsätzliche <filename>/etc/apt/sources.list</filename>
erstellt, die es erlaubt, zusätzliche Pakete zu installieren. Allerdings
möchten Sie vielleicht einige Quellen hinzufügen, z.B. für Quellpakete oder
für Sicherheits-Updates:

<informalexample><screen>
deb-src http://ftp.us.debian.org/debian etch main

deb http://security.debian.org/ etch/updates main
deb-src http://security.debian.org/ etch/updates main
</screen></informalexample>

Denken Sie daran, <userinput>aptitude update</userinput> auszuführen, nachdem
Sie Änderungen in der sources.list-Datei gemacht haben.

</para>
   </sect3>

   <sect3>
   <title>Die lokalen Einstellungen (locales) konfigurieren</title>
<para>

Um Ihre lokalen Einstellungen anzupassen, wenn Sie nicht Englisch
verwenden möchten, installieren Sie das Paket <classname>locales</classname>
und konfigurieren es. Es wird die Verwendung von UTF-8-Locales empfohlen.

<informalexample><screen>
# aptitude install locales
# dpkg-reconfigure locales
</screen></informalexample>

Um Ihre Tastatur zu konfigurieren (falls nötig):
 
<informalexample><screen>
# aptitude install console-data
# dpkg-reconfigure console-data
</screen></informalexample>

</para><para>

Beachten Sie, dass die Tastatureinstellung nicht verändert werden kann, während
Sie im chroot sind; die Konfiguration wird jedoch beim nächsten Neustart aktiv.

</para>
   </sect3>
  </sect2>

  <sect2>
  <title>Einen Kernel installieren</title>
<para>

Wenn Sie vorhaben, dieses System zu booten, möchten Sie wahrscheinlich
einen Linux-Kernel und einen Bootloader. Sie finden verfügbare, bereits
fertig paketierte Kernel mit dem Befehl

<informalexample><screen>
# apt-cache search linux-image
</screen></informalexample>

</para><para>

Wenn Sie vorhaben, einen fertig paketierten Kernel zu verwenden, sollten Sie
vielleicht vorher die Konfigurationsdatei
<filename>/etc/kernel-img.conf</filename> erstellen. Hier eine Beispieldatei:

<informalexample><screen>
# Kernel image management overrides
# See kernel-img.conf(5) for details
do_symlinks = yes
relative_links = yes
do_bootloader = yes
do_bootfloppy = no
do_initrd = yes
link_in_boot = no
</screen></informalexample>

</para><para>

Detailierte Informationen über diese Datei und die verschiedenen Optionen
finden Sie in ihrer Handbuchseite (Manpage), die nach der Installation des
Paketes <classname>kernel-package</classname> verfügbar ist. Wir empfehlen
zu überprüfen, ob die eingetragenen Werte für Ihr System passend sind.

</para><para>

Um einen Kernel Ihrer Wahl zu installieren, benutzen Sie seinen Paketnamen:

<informalexample><screen>
# aptitude install linux-image-<replaceable>&kernelversion;-arch-etc</replaceable>
</screen></informalexample>

Wenn Sie die Datei <filename>/etc/kernel-img.conf</filename> vor der
Installation eines Debian-Kernels nicht erstellt haben, werden Ihnen eventuell
einige Fragen gestellt, die sich darum drehen.

 </para>
  </sect2>

  <sect2>
<title>Den Bootloader einrichten</title>
<para>

Um Ihr &debian;-System bootfähig zu machen, richten Sie Ihren Bootloader ein,
so dass er den installierten Kernel mit Ihrer neuen root-Partition startet.
Bedenken Sie, dass <command>debootstrap</command> keinen Bootloader installiert,
allerdings können Sie aptitude in Ihrer Debian-chroot-Umgebung benutzen, um
dies zu erledigen.

</para><para arch="x86">

Lesen Sie <userinput>info grub</userinput> oder <userinput>man
lilo.conf</userinput>, um Informationen über die Einrichtung des Bootloaders
zu bekommen. Wenn Sie das Betriebssystem, das Sie zur Installation von Debian
benutzt haben, behalten möchten, müssen Sie lediglich einen Eintrag zur
vorhandenen grub-<filename>menu.lst</filename> oder zu
<filename>lilo.conf</filename> hinzufügen. Die Datei
<filename>lilo.conf</filename> können Sie auch auf Ihr neues Debian-System
kopieren und dort bearbeiten. Rufen Sie danach <command>lilo</command> auf
(bedenken Sie: lilo nutzt die <filename>lilo.conf</filename> relativ zum
System, von dem aus Sie es aufrufen).

</para><para arch="x86">

Die Installation und Einrichtung von <classname>grub</classname> ist so
einfach wie:

<informalexample><screen>
# aptitude install grub
# grub-install /dev/<replaceable>hda</replaceable>
# update-grub
</screen></informalexample>

Der zweite Befehl installiert <command>grub</command> (in diesem Fall in den
Master Boot Record (MBR) von <literal>hda</literal>). Der letzte Befehl erzeugt
eine schöne funktionierende Konfigurationsdatei
<filename>/boot/grub/menu.lst</filename>.

</para><para arch="x86">

Hier ein grundsätzliches Beispiel einer <filename>/etc/lilo.conf</filename>:

<informalexample><screen>
boot=/dev/<replaceable>hda6</replaceable>
root=/dev/<replaceable>hda6</replaceable>
install=menu
delay=20
lba32
image=/vmlinuz
label=Debian
</screen></informalexample>

</para><para arch="x86">

Abhängig davon, welchen Bootloader Sie ausgewählt haben, können Sie jetzt
einige zusätzliche Änderungen in <filename>/etc/kernel-img.conf</filename>
machen.

</para><para arch="x86">

Für <classname>grub</classname> als Bootloader sollten Sie die Option
<literal>do_bootloader</literal> auf <quote>no</quote> setzen. Um Ihre
<filename>/boot/grub/menu.lst</filename> bei Installation oder Löschung
von Debian-Kernels automatisch aktualisieren zu lassen, fügen Sie der Datei
die folgenden Zeilen hinzu:

<informalexample><screen>
postinst_hook = update-grub
postrm_hook   = update-grub
</screen></informalexample>

Bei Verwendung von <classname>lilo</classname> als Bootloader muss
<literal>do_bootloader</literal> auf <quote>yes</quote> gesetzt sein.

</para><para arch="powerpc">

Lesen Sie <userinput>man yaboot.conf</userinput>, um Informationen über
die Einrichtung des Bootloaders zu bekommen. Wenn Sie das Betriebssystem, das Sie
zur Installation von Debian benutzt haben, behalten möchten, müssen Sie
lediglich einen Eintrag für das Debian-System zur vorhandenen
<filename>yaboot.conf</filename> hinzufügen.
Sie können auch die Datei <filename>yaboot.conf</filename> auf Ihr
neues Debian-System kopieren und dort bearbeiten. Rufen Sie danach <command>ybin</command> auf
(bedenken Sie: ybin nutzt die <filename>yaboot.conf</filename> relativ zum
System, von dem aus Sie es aufrufen).

</para><para arch="powerpc">

Hier ein grundsätzliches Beispiel einer <filename>/etc/yaboot.conf</filename>:

<informalexample><screen>
boot=/dev/hda2
device=hd:
partition=6
root=/dev/hda6
magicboot=/usr/lib/yaboot/ofboot
timeout=50
image=/vmlinux
label=Debian
</screen></informalexample>

Auf einigen Maschinen müssen Sie unter Umständen <userinput>ide0:</userinput>
benutzen statt <userinput>hd:</userinput>.

</para>
  </sect2>

  <sect2>
<title>Zum Schluss</title>
<para>

Wie bereits vorher erwähnt, wird das installierte System nur sehr
minimalistisch ausgestattet sein. Wenn Sie möchten, dass das System ein
bisschen ausgereifter wird, gibt es eine einfache Methode, alle Pakete mit
<quote>Standard</quote>-Priorität zu installieren:

<informalexample><screen>
# tasksel install standard
</screen></informalexample>

Sie können natürlich auch einfach <command>aptitude</command> benutzen,
um individuell Pakete zu installieren.

</para><para>

Nach der Installation liegen viele heruntergeladene Pakete in dem
Verzeichnis <filename>/var/cache/apt/archives/</filename>. Sie können
etliches an Festplattenspeicher wieder freigeben, indem Sie folgendes
ausführen:

<informalexample><screen>
# aptitude clean
</screen></informalexample>

</para>
  </sect2>
 </sect1>
