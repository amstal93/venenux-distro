<!-- retain these comments for translator revision tracking -->
<!-- original version: 43939 -->

 <sect1 id="needed-info">
 <title>Benötigte Informationen</title>

  <sect2>
  <title>Dokumentation</title>

   <sect3>
   <title>Installationshandbuch</title>

<para condition="for_cd">

Das Dokument, das Sie gerade lesen, in reinem ASCII-, HTML- oder PDF-Format:

</para>

<itemizedlist condition="for_cd">

&list-install-manual-files;

</itemizedlist>

<para condition="for_wdo">

Das Dokument, das Sie gerade lesen; es ist die offizielle Version des
Installationshandbuchs für die &releasename-cap;-Veröffentlichung von Debian;
es ist in <ulink url="&url-release-area;/installmanual">verschiedenen Formaten
und Übersetzungen</ulink> verfügbar.

</para>

<para condition="for_alioth">

Das Dokument, das Sie gerade lesen; es ist eine Entwicklerversion des
Installationshandbuchs für die nächste Veröffentlichung von Debian (dem
Nachfolger von &releasename-cap;); es ist in
<ulink url="&url-d-i-alioth-manual;">verschiedenen Formaten und
Übersetzungen</ulink> verfügbar.

</para>

</sect3>


   <sect3><title>Hardware-Dokumentation</title>
<para>

Enthält oft nützliche Informationen zum Konfigurieren oder Verwenden Ihrer Hardware.

</para>

<!-- We need the arch dependence for the whole list to ensure proper xml
     as long as not architectures have a paragraph -->
<itemizedlist arch="x86;m68k;alpha;sparc;mips;mipsel">
<listitem arch="x86"><para> 

<ulink url="&url-hardware-howto;">Linux-Hardware-Kompatibilitäts-HowTo</ulink>

</para></listitem>

<listitem arch="m68k"><para>

<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>

</para></listitem>

<listitem arch="alpha"><para>

<ulink url="&url-alpha-faq;">Linux/Alpha FAQ</ulink>

</para></listitem>

<listitem arch="sparc"><para>

<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors F.A.Q.</ulink>

</para></listitem>

<listitem arch="mips;mipsel"><para>

<ulink url="&url-linux-mips;">Linux/Mips-Website</ulink>

</para></listitem>

</itemizedlist>
   </sect3>


   <sect3 arch="s390">
   <title>&arch-title; &ndash; Hardware-Referenzen</title>
<para>

Installationsanweisungen und Gerätetreiber (DASD, XPRAM, Console,
tape, z90 crypto, chandev, Netzwerk) für Linux auf &arch-title; mit Kernel 2.4

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://oss.software.ibm.com/developerworks/opensource/linux390/docu/l390dd08.pdf">Device Drivers and Installation Commands</ulink>

</para></listitem>
</itemizedlist>

<para>

IBM-Redbook, das beschreibt, wie man Linux mit z/VM auf zSeries
und &arch-title;-Hardware verwendet.

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg244987.pdf">
Linux for &arch-title;</ulink>

</para></listitem>
</itemizedlist>

<para>

IBM-Redbook, das die verfügbaren Linux-Distributionen für den Mainframe
auflistet. Es beinhaltet zwar kein Kapitel über Debian, aber das Konzept
der Basisinstallation ist für jede &arch-title;-Distribution gleich.

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg246264.pdf">
Linux for IBM eServer zSeries and &arch-title;: Distributions</ulink>

</para></listitem>
</itemizedlist>
   </sect3>

  </sect2>

  <sect2 id="fsohi">
  <title>Quellen für Hardwareinformationen finden</title>
<para>

In vielen Fällen ist der Installer in der Lage, Ihre Hardware automatisch
zu erkennen. Um jedoch vorbereitet zu sein, empfehlen wir, sich vor der
Installation mit der Hardware vertraut zu machen.

</para><para>

Hardwareinformationen können von folgenden Quellen bezogen werden:

</para>

<itemizedlist>
<listitem><para>

Die Handbücher, die mit jedem Hardwareteil mitgeliefert werden.

</para></listitem>
<listitem><para>

Das BIOS-Setup Ihres Computers. Sie gelangen in das BIOS-Setup, indem
Sie eine Taste/Tastenkombination drücken, während der Computer startet. Sehen
Sie in Ihrem Handbuch nach, um die passende Kombination herauszufinden. Oftmals ist es die
<keycap>Entfernen</keycap>-Taste.

</para></listitem>
<listitem><para>

Die Verpackung Ihrer Hardware.

</para></listitem>

<listitem arch="x86"><para>

Das System-Fenster in der Windows-Systemsteuerung.

</para></listitem>
<listitem><para>

Systembefehle oder Werkzeuge in einem anderen Betriebssystem, inklusive
in Dateimanagern angezeigte Informationen. Diese Quelle ist im Speziellen
nützlich, um Informationen über RAM- oder Festplattenspeicher zu erhalten.

</para></listitem>
<listitem><para>

Ihr Systemadministrator oder Internetprovider. Sie können Ihnen
die zur Einrichtung notwendigen Einstellungen von Netzwerk und
E-Mail verraten.

</para></listitem>
</itemizedlist>

<para>

<table>
<title>Zur Installation notwendige Hardware-Informationen</title>
<tgroup cols="2">
<thead>
<row>
  <entry>Hardware</entry><entry>Informationen, die Sie benötigen könnten</entry>
</row>
</thead>

<tbody>
<row arch="not-s390">
  <entry morerows="5">Festplatten</entry>
  <entry>Wie viele Sie haben.</entry>
</row>
<row arch="not-s390"><entry>Deren Reihenfolge im System.</entry></row>
<!-- "not-m68k;not-s390" would really turn out to be everything... -->
<row arch="alpha;arm;hppa;x86;ia64;mips;mipsel;powerpc;sparc">
  <entry>Ob IDE oder SCSI (die meisten Rechner haben IDE).</entry>
</row>
<row arch="m68k">
  <entry>Ob IDE oder SCSI (die meisten m68k-Computer haben SCSI).</entry>
</row>
<row arch="not-s390"><entry>Verfügbarer freier Plattenplatz.</entry></row>
<row arch="not-s390"><entry>Partitionen.</entry></row>
<row arch="not-s390">
  <entry>Partitionen, auf denen andere Betriebssysteme installiert
    sind.</entry>
</row>
<row arch="not-s390">
  <entry morerows="5">Bildschirm</entry>
  <entry>Modell und Hersteller.</entry>
</row>
<row arch="not-s390"><entry>Unterstützte Auflösungen.</entry></row>
<row arch="not-s390"><entry>Horizontale Bildwiederholrate.</entry></row>
<row arch="not-s390"><entry>Vertikale Bildwiederholrate.</entry></row>
<row arch="not-s390">
  <entry>Unterstützte Farbtiefe (Anzahl der Farben).</entry>
</row>
<row arch="not-s390"><entry>Bildschirmgröße.</entry></row>

<row arch="not-s390">
  <entry morerows="3">Maus</entry>
  <entry>Typ: seriell, PS/2 oder USB.</entry>
</row>
<row arch="not-s390"><entry>Anschluss.</entry></row>
<row arch="not-s390"><entry>Hersteller.</entry></row>
<row arch="not-s390"><entry>Anzahl der Tasten.</entry></row>

<row arch="not-s390">
  <entry morerows="1">Netzwerk</entry>
  <entry>Modell und Hersteller.</entry>
</row>
<row arch="not-s390"><entry>Typ des Adapters/der Karte.</entry></row>

<row arch="not-s390">
  <entry morerows="1">Drucker</entry>
  <entry>Modell und Hersteller.</entry>
</row>
<row arch="not-s390"><entry>Unterstützte Druckauflösungen.</entry></row>

<row arch="not-s390">
  <entry morerows="2">Grafikkarte</entry>
  <entry>Modell und Hersteller.</entry>
</row>
<row arch="not-s390"><entry>Verfügbarer Video-Speicher.</entry></row>
<row arch="not-s390">
  <entry>Unterstützte Auflösungen und Farbtiefen (vergleichen Sie
    diese Angaben mit denen Ihres Bildschirms).</entry>
</row>

<row arch="s390">
  <entry morerows="1">DASD</entry>
  <entry>Gerätenummer(n).</entry>
</row>
<row arch="s390"><entry>Verfügbarer freier Plattenplatz.</entry></row>

<row arch="s390">
  <entry morerows="2">Netzwerk</entry>
  <entry>Typ des Adapters/der Karte.</entry>
</row>
<row arch="s390"><entry>Gerätenummern.</entry></row>
<row arch="s390"><entry>Relative Adapternummer bei OSA-Karten.</entry></row>

</tbody></tgroup></table>

</para>
  </sect2>

  <sect2>
  <title>Hardware-Kompatibilität</title>

<para>

Viele Markenprodukte funktionieren problemlos unter Linux. Zudem verbessert
sich die Hardwareunterstützung für Linux täglich. Linux unterstützt jedoch
nicht so viele verschiedene Typen von Hardware wie manch anderes Betriebssystem.

</para><para arch="x86">

Im Besonderen kann Linux normalerweise keine Hardware betreiben, die
eine laufende Windows-Version benötigt.

</para><para arch="x86">

Einige Windows-spezifische Hardware kann jedoch mit etwas Aufwand
unter Linux lauffähig gemacht werden. Außerdem sind Treiber für
Windows-spezifische Hardware meist für einen speziellen Linux-Kernel
angepasst. Aus diesem Grund können sie schnell überholt sein.

</para><para arch="x86">

So genannte Win-Modems sind der gängigste Typ dieser Hardware.
Drucker und andere Geräte können jedoch ebenfalls Windows-spezifisch
sein.

</para><para>

Sie können die Hardware-Kompatibilität wie folgt überprüfen:

<itemizedlist>
<listitem><para>

Suchen Sie auf der Website des Herstellers nach (neuen) Treibern.

</para></listitem>
<listitem><para>

Suchen Sie auf Webseiten oder in Handbüchern nach Informationen über Emulationen.
Produkte weniger bekannter Marken können manchmal die Treiber oder Einstellungen
von besser bekannten verwenden.

</para></listitem>
<listitem><para>

Durchsuchen Sie Hardware-Kompatibilitätslisten für Linux auf Webseiten für
Ihre Architektur.

</para></listitem>
<listitem><para>

Suchen Sie im Internet nach Erfahrungsberichten anderer Benutzer.

</para></listitem>
</itemizedlist>

</para>
  </sect2>

  <sect2>
  <title>Netzwerkeinstellungen</title>

<para>

Wenn Ihr Computer rund um die Uhr mit einem Netzwerk verbunden ist
(z.B. Ethernet oder eine gleichwertige Verbindung &ndash; keine
PPP-(Wähl-)Verbindung), sollten Sie beim Systemadministrator des Netzwerks
die folgenden Informationen erfragen:

<itemizedlist>
<listitem><para>

Ihren Hostnamen (den Name Ihres Rechners im Netzwerk; möglicherweise
können Sie selbst einen auswählen).

</para></listitem>
<listitem><para>

Ihren Domainnamen.

</para></listitem>
<listitem><para>

Die IP-Adresse Ihres Computers.

</para></listitem>
<listitem><para>

Die Netzmaske zur Verwendung in Ihrem Netzwerk.

</para></listitem>
<listitem><para>

Die IP-Adresse eines Standard-Gateway-Systems, zu dem Sie routen sollen,
<emphasis>falls Ihr Netzwerk einen Gateway hat</emphasis>.

</para></listitem>
<listitem><para>

Den Rechner in Ihrem Netzwerk, den Sie als DNS-(Domain Name Service-)Server
verwenden sollen.

</para></listitem>
</itemizedlist>

</para><para>

Andererseits, wenn Ihr Administrator Ihnen sagt, dass ein DHCP-Server
verfügbar und empfohlen ist, benötigen Sie all diese Informationen
nicht, da der DHCP-Server diese während des Installationsprozesses
direkt an Ihren Computer weitergibt.

</para><para>

Wenn Sie ein kabelloses Funk-Netzwerk (WLAN) verwenden, sollten Sie auch das 
Folgende erfragen:

<itemizedlist>
<listitem><para>

Die ESSID Ihres kabellosen Netzwerks.

</para></listitem>
<listitem><para>

Den WEP-Sicherheitsschlüssel (falls notwendig).

</para></listitem>
</itemizedlist>

</para>
  </sect2>

 </sect1>
