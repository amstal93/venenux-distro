<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 39644 -->
<!-- revisado por ender, 31 dic 2005 -->
<!-- traducido por jfs, 30 dic 2005 -->

 <sect1 id="plip" arch="x86">
 <title>Instalar &debian; a trav�s de una l�nea IP paralela (PLIP)</title>

<para>

Esta secci�n explica c�mo se puede instalar &debian; en un ordenador que
carezca de tarjeta de red y que s�lo tiene un ordenador actuando como pasarela
conectado a trav�s de un cable de <quote>m�dem nulo</quote> (tambi�n llamado cable
<quote>null-modem</quote> o <quote>null-printer</quote>). El sistema pasarela
deber�a estar conectado a una red por la que se pueda acceder a una r�plica de
Debian (por ejemplo, en Internet).

</para><para>

En el ejemplo de este ap�ndice se configura una conexi�n PLIP con una
pasarela que est� conectada a Internet a trav�s de una conexi�n de 
acceso telef�nico (ppp0). Se utilizar�n las direcciones IP 192.168.0.1
y 192.168.0.2 para las interfaces PLIP en el sistema a instalar (objetivo) y en
el sistema origen, respectivamente (estas direcciones IP deber�an
estar libres en su espacio de direcciones de red).

</para><para>

La conexi�n PLIP que se configura durante la instalaci�n tambi�n estar�
disponible despu�s del reinicio del sistema en el propio sistema instalado
(v�ase <xref linkend="boot-new"/>).

</para><para>

Antes de empezar deber� comprobar que la configuraci�n de BIOS
para el puerto paralelo (direcci�n base de E/S (<quote>IO address</quote>) e IRQ)
de ambos sistemas, origen y objetivo. Los valores m�s habituales son <literal>io=0x378</literal> e
<literal>irq=7</literal>.

</para>

  <sect2>
  <title>Requisitos</title>

<itemizedlist>
<listitem><para>

Un sistema destino, llamado <emphasis>objetivo</emphasis>, donde se va a instalar
Debian.

</para></listitem>
<listitem><para>

Los medios para la instalaci�n del sistema; v�ase <xref linkend="installation-media"/>.

</para></listitem>
<listitem><para>

Otro sistema conectado a Internet que actuar� como pasarela y llamaremos aqu�
<emphasis>sistema origen</emphasis> o simplemente <emphasis>origen</emphasis>.

</para></listitem>
<listitem><para>

Un cable <quote>m�dem nulo</quote> DB-25. Para m�s informaci�n sobre
este cable e instrucciones de c�mo hacer uno, puede consultar el documento
<ulink url="&url-plip-install-howto;">PLIP-Install-HOWTO</ulink>.

</para></listitem>
</itemizedlist>

  </sect2>

  <sect2>
  <title>Configurar el sistema origen</title>
<para>

El programa mostrado a continuaci�n es un ejemplo sencillo que configura
el ordenador fuente como una pasarela a Internet utilizando ppp0.

<informalexample><screen>
#!/bin/sh

# Eliminamos los m�dulos que est�n ejecut�ndose en el n�cleo para evitar
# conflictos y para reconfigurarlos manualmente.
modprobe -r lp parport_pc
modprobe parport_pc io=<replaceable>0x378</replaceable> irq=<replaceable>7</replaceable>
modprobe plip

# Configura la interfaz plip (plip0 en este caso, pruebe en caso de duda "dmesg | grep plip" ).
ifconfig <replaceable>plip0 192.168.0.2</replaceable> pointopoint <replaceable>192.168.0.1</replaceable> netmask 255.255.255.255 up

# Configurar la pasarela.
modprobe iptable_nat
iptables -t nat -A POSTROUTING -o <replaceable>ppp0</replaceable> -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Instalar el objetivo</title>
<para>

Arranque con el medio de instalaci�n. Tiene que ejecutar la
instalaci�n en modo experto, utilizando <userinput>expert</userinput>
en el indicador de sistema del arranque.
Tambi�n debe indicar los par�metros que necesite para los m�dulos
del n�cleo en el indicador de sistema del arranque. Por ejemplo,
deber� introducir lo que se muestra a continuaci�n si necesita
arrancar el instalador y fijar los valores necesarios a las
opciones <quote>io</quote> e <quote>irq</quote> en el m�dulo
parport_pc:

<informalexample><screen>
expert parport_pc.io=<replaceable>0x378</replaceable> parport_pc.irq=<replaceable>7</replaceable>
</screen></informalexample>

Abajo se dan las respuestas que deber�a dar durante las distintas fases de la
instalaci�n.

</para>

<orderedlist>
<listitem><para>

<guimenuitem>Cargar componentes del instalador desde CD</guimenuitem>

</para><para>

Seleccione la opci�n <userinput>plip-modules</userinput> de la lista, esto har�
que el sistema de instalaci�n pueda utilizar los controladores PLIP.

</para></listitem>
<listitem><para>

<guimenuitem>Detecci�n del hardware de red</guimenuitem>

</para>

 <itemizedlist>
 <listitem><para>

Si el objetivo <emphasis>tiene</emphasis> una tarjeta de red se mostrar� una
lista de los m�dulos con controladores para las tarjetas detectadas.  Si quiere
hacer que &d-i; utilice plip en lugar de �stas tendr� que deseleccionar todos
los m�dulos de controladores listados. Obviamente, si el objetivo no tiene una
tarjeta de red el instalador no mostrar� esta lista.

 </para></listitem>
 <listitem><para>

El instalador le preguntar� si quiere seleccionar un m�dulo de
controlador de red de la lista dado que antes no se ha seleccionado ni
detectado una tarjeta de red.  Seleccione el m�dulo
<userinput>plip</userinput>.

 </para></listitem>

 </itemizedlist>

</listitem>
<listitem><para>

<guimenuitem>Configurar la red</guimenuitem>

 <itemizedlist>
 <listitem><para>

�Desea configurar autom�ticamente la red con DHCP? No

 </para></listitem>
 <listitem><para>

Direcci�n IP: <userinput><replaceable>192.168.0.1</replaceable></userinput>

 </para></listitem>
 <listitem><para>

Direcci�n punto a punto:
<userinput><replaceable>192.168.0.2</replaceable></userinput>

 </para></listitem>
 <listitem><para>

Direcciones de servidores de nombres: puede introducir la misma direcci�n que la 
que utiliza la fuente (cons�ltela en <filename>/etc/resolv.conf</filename>)

 </para></listitem>
 </itemizedlist>

</para></listitem>
</orderedlist>

  </sect2>
 </sect1>
