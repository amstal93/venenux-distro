<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44436 -->
<!-- revisado jfs, 21 noviembre 2004 -->


 <sect1 id="linuxdevices"><title>Dispositivos en Linux</title>
<para>

Linux tiene varios ficheros especiales en <filename>/dev</filename>.
Estos ficheros se llaman ficheros de dispositivo pero no se comportan
como los ficheros habituales. Los tipos m�s com�nes de ficheros de
dispositivos son los de dispositivos de bloques o de caracer.  Estos
ficheros son una interfaz al controlador de dispositivo (parte del
n�cleo de Linux) que a su vez le permite el acceso al hardware. Un
tipo de archivo de dispositivo menos frecuente son las
<firstterm>pipe</firstterm> (N. del T., a veces traducido como
�tuber�a�).  En la tabla mostrada a continuaci�n se lista los ficheros
de dispositivo m�s importantes.

</para><para>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>fd0</filename></entry>
  <entry>Primera unidad de disquetes</entry>
</row><row>
  <entry><filename>fd1</filename></entry>
  <entry>Segunda unidad de disquetes</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>hda</filename></entry>
  <entry>Disco duro IDE / CD-ROM en el primer puerto IDE (Maestro)</entry>
</row><row>
  <entry><filename>hdb</filename></entry>
  <entry>Disco duro IDE / CD-ROM en el primer puerto IDE (Esclavo)</entry>
</row><row>
  <entry><filename>hdc</filename></entry>
  <entry>Disco duro IDE / CD-ROM en el segundo puerto IDE (Maestro)</entry>
</row><row>
  <entry><filename>hdd</filename></entry>
  <entry>Disco duro IDE / CD-ROM en el segundo puerto IDE (Esclavo)</entry>
</row><row>
  <entry><filename>hda1</filename></entry>
  <entry>Primera partici�n del primer disco duro IDE</entry>
</row><row>
  <entry><filename>hdd15</filename></entry>
  <entry>Decimoquinta partici�n del cuarto disco duro IDE</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>sda</filename></entry>
  <entry>Disco duro SCSI con el menor ID SCSI (por ejemplo 0)</entry>
</row><row>
  <entry><filename>sdb</filename></entry>
  <entry>Disco duro SCSI con el siguiente ID SCSI (por ejemplo 1)</entry>
</row><row>
  <entry><filename>sdc</filename></entry>
  <entry>Disco duro SCSI con el siguiente ID SCSI (por ejemplo 2)</entry>
</row><row>
  <entry><filename>sda1</filename></entry>
  <entry>Primera partici�n del primer disco duro SCSI</entry>
</row><row>
  <entry><filename>sdd10</filename></entry>
  <entry>D�cima partici�n del cuarto disco duro SCSI</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>sr0</filename></entry>
  <entry>CD-ROM SCSI con el ID SCSI m�s bajo</entry>
</row><row>
  <entry><filename>sr1</filename></entry>
  <entry>CD-ROM SCSI con el siguiente ID SCSI</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>ttyS0</filename></entry>
  <entry>Puerto serie 0, COM1 en MS-DOS</entry>
</row><row>
  <entry><filename>ttyS1</filename></entry>
  <entry>Puerto serie 1, COM2 en MS-DOS</entry>
</row><row>
  <entry><filename>psaux</filename></entry>
  <entry>Rat�n de tipo PS/2</entry>
</row><row>
  <entry><filename>gpmdata</filename></entry>
  <entry>Pseudo-dispositivo, repite los datos generados por el demonio GPM (rat�n)</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>cdrom</filename></entry>
  <entry>Enlace simb�lico a la unidad de CD-ROM</entry>
</row><row>
  <entry><filename>mouse</filename></entry>
  <entry>Enlace simb�lico al fichero de dispositivo del rat�n</entry>
</row>
</tbody></tgroup></informaltable>

<informaltable><tgroup cols="2"><tbody>
<row>
  <entry><filename>null</filename></entry>
  <entry>Todo lo que se escriba en este dispositivo desaparecer�</entry>
</row><row>
  <entry><filename>zero</filename></entry>
  <entry>Se pueden leer continuamente ceros desde este dispositivo</entry>
</row>
</tbody></tgroup></informaltable>

</para>

  <sect2 arch="not-s390" id="device-mouse">
    <title>Configurar su rat�n</title>

<para>

Puede usar su rat�n tanto en consola de Linux (con gpm) como en el
entorno de ventanas X. Generalmente para lograr esto s�lo tiene
que instalar <filename>gpm</filename> y el servidor de X. Ambos
deber�an configurarse para utilizar como dispositivo de rat�n
<filename>/dev/input/mice</filename>. El protocolo de rat�n a
utilizar se llama <userinput>exps2</userinput> en gpm, y
<userinput>ExplorerPS/2</userinput> en X. Los archivos de configuraci�n son
<filename>/etc/gpm.conf</filename> y <filename>/etc/X11/xorg.conf</filename>
respectivamente.

</para><para>

Para que su rat�n funcione debe cargar algunos m�dulos del n�cleo. 
El rat�n se detecta de forma autom�tica en la mayor�a de los casos, pero
esto no siempre sucede en el caso de ratones serie antiguos o ratones de bus 
<footnote>

<para>
Los ratones serie tienen generalmente un conector de 9 pines con forma de letra D.
Los ratones de bus tienen un conector de 8 pines redondos, que no debe confundirse
con el conector de 6 pines redondo de los ratones PS/2 o el conector redondo de
cuatro pines de los ratones ADB.
</para>
</footnote>, que son muy raros salvo en el caso de utilizar ordenadores antiguros.
A continuaci�n se resumen los m�dulos del n�cleo necesarios para los distintos tipos
de rat�n:

<informaltable><tgroup cols="2"><thead>
<row>
  <entry>Modulo</entry>
  <entry>Descripci�n</entry>
</row>
</thead><tbody>
<row>
  <entry>psmouse</entry>
  <entry>rat�n PS/2 mice (deber�a detectarse autom�ticamente)</entry>
</row>
<row>
  <entry>usbhid</entry>
  <entry>rat�n USB (deber�a detectarse autom�ticamente)</entry>
</row>
<row>
  <entry>sermouse</entry>
  <entry>Para la mayor�a de los ratones serie</entry>
</row>
<row>
  <entry>logibm</entry>
  <entry>Rat�n de bus conectado a una tarjeta adaptadora de Logitech</entry>
</row>
<row>
  <entry>inport</entry>
  <entry>Rat�n de bus conectado a una tarjeta de ATI or InPort de Microsoft</entry>
</row>
</tbody></tgroup></informaltable>

Debe usar la orden <command>modconf</command> (en el paquete del mismo
nombre) para cargar un m�dulo para la controladora del rat�n y mirar
en la categor�a <userinput>kernel/drivers/input/mouse</userinput>.

</para><para arch="powerpc">
<!-- FJP 20070122: No estoy seguro si esto es v�lido -->

Los n�cleos modernos le ofrecen la capacidad de emular un rat�n de
tres botones aunque su rat�n solo tenga uno. Simplemente a�ada las
siguientes l�neas al fichero <filename>/etc/sysctl.conf</filename>.

<informalexample><screen>
# Emulaci�n de un rat�n de 3 botones
# Activar emulaci�n
/dev/mac_hid/mouse_button_emulation = 1
# Hacer que el bot�n del medio funcione al pulsar la tecla F11
/dev/mac_hid/mouse_button2_keycode = 87
# Hacer que el bot�n derecho funcione al pulsar la tecla F12
/dev/mac_hid/mouse_button3_keycode = 88
# Para usar teclas diferentes, utilice showkey para saber el c�digo de �stas.
</screen></informalexample>

</para>
  </sect2>
 </sect1>

 <sect1 id="tasksel-size-list">
 <title>Espacio en disco requerido para las tareas</title>
<para>

Una instalaci�n est�ndar para i386, incluyendo todos los paquetes
est�ndar y el n�cleo 2.6 utilizado por omisi�n, ocupa
&std-system-size; MB de espacio en disco.  Una instalaci�n m�nima base sin seleccionar la
tarea <quote>Sistema est�ndar</quote> ocupar� &base-system-size; MB.

</para>
<important><para>

En ambos casos es importante tener en cuenta que este es el espacio
<emphasis>despu�s</emphasis> de haber terminado la instalaci�n y
de que se hayan borrado todos los ficheros temporales. Tampoco
tiene en cuenta la cantidad utilizada por el propio sistema
de ficheros, por ejemplo por los ficheros de �journal�. Esto
significa que hace falta bastante m�s disco 
<emphasis>durante</emphasis> la instalaci�n y durante el uso habitual
del sistema.
        
</para></important>
<para>

La siguiente tabla lista los tama�os indicados por aptitude
para las tareas listadas en tasksel. Tenga en cuenta que algunas tareas tienen
componentes comunes, de modo que el tama�o total instalado para
dos tareas juntas podr�a ser inferior al total obtenido al sumar
sus tama�os individualmente.

</para><para>

Tenga en cuenta que tendr� que a�adir los tama�os que se indican en la tabla al
tama�o de la instalaci�n est�ndar para poder determinar el tama�o de sus particiones.
La mayor�a del espacio en disco que se indica en <quote>Tama�o instalado</quote>
acabar� utiliz�ndose de <filename>/usr</filename> y en <filename>/lib</filename>. Por otro lado, el tama�o
que se indica en <quote>Tama�o de descarga</quote> ser� necesario (temporalmente)
en <filename>/var</filename>.
</para><para>

<informaltable><tgroup cols="4">
<thead>
<row>
  <entry>Tarea</entry>
  <entry>Tama�o instalado (MB)</entry>
  <entry>Tama�o de descarga (MB)</entry>
  <entry>Espacio necesario para instalar (MB)</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Entorno de escritorio</entry>
  <entry>&task-desktop-inst;</entry>
  <entry>&task-desktop-dl;</entry>
  <entry>&task-desktop-tot;</entry>
</row>

<row>
  <entry>Port�til<footnote>
        
         <para>
Hay un solape considerable entre la tarea �Port�til� y la tarea �Entorno de escritorio�. La tarea �Port�til� s�lo utilizar� algunos MB adicionales de espacio en disco si instala ambas.
         </para>
</footnote></entry>
  <entry>&task-laptop-inst;</entry>
  <entry>&task-laptop-dl;</entry>
  <entry>&task-laptop-tot;</entry>
</row>

<row>
  <entry>Servidor Web</entry>
  <entry>&task-web-inst;</entry>
  <entry>&task-web-dl;</entry>
  <entry>&task-web-tot;</entry>
</row>

<row>
  <entry>Servidor de impresoras</entry>
  <entry>&task-print-inst;</entry>
  <entry>&task-print-dl;</entry>
  <entry>&task-print-tot;</entry>
</row>

<row>
  <entry>Servidor de DNS</entry>
  <entry>&task-dns-inst;</entry>
  <entry>&task-dns-dl;</entry>
  <entry>&task-dns-tot;</entry>
</row>

<row>
  <entry>Servidor de ficheros</entry>
  <entry>&task-file-inst;</entry>
  <entry>&task-file-dl;</entry>
  <entry>&task-file-tot;</entry>
</row>

<row>
  <entry>Servidor de correo</entry>
  <entry>&task-mail-inst;</entry>
  <entry>&task-mail-dl;</entry>
  <entry>&task-mail-tot;</entry>
</row>

<row>
  <entry>Base de datos SQL</entry>
  <entry>&task-sql-inst;</entry>
  <entry>&task-sql-dl;</entry>
  <entry>&task-sql-tot;</entry>
</row>

</tbody>
</tgroup></informaltable>

<note><para>

La tarea <emphasis>Entorno de escritorio</emphasis> instalar�
el entorno de escritorio de GNOME.

</para></note>

</para><para>

Puede que <command>tasksel</command> instale autom�ticamente una
<firstterm>tarea de localizaci�n</firstterm> si est� realizando la instalaci�n
en un idioma que no sea el ingl�s, siempre y cuando haya una disponible para su
idioma. Los requisitos de espacio var�an por idioma pero deber�a, en este caso,
tener en cuenta que podr�a necesitar 350 MB en total para la descarga e instalaci�n
de esta tarea.

</para>
 </sect1>
