<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44410 -->
<!-- revisado jfs, 21 noviembre 2004 -->
<!-- revisado rudy, 24 feb. 2005 -->
<!-- actualizado jfs, 23 marzo 2006 -->
<!-- actualizado jfs, enero 2007 -->

 <sect1 id="linux-upgrade">
 <title>Instalar &debian; desde un sistema Unix/Linux</title>

<para>

Esta secci�n explica como instalar &debian; desde un sistema Unix o
Linux existente, sin usar el instalador basado en men�s,
como se explica en el resto de este manual.  Los usuarios que se
cambian a &debian; desde Red Hat, Mandrake y SuSE han solicitado este
C�MO de <quote>instalaci�n cruzada</quote>. En esta secci�n se asume alguna
familiaridad con la introducci�n de �rdenes en *nix y navegaci�n en el
sistema de ficheros. En esta secci�n <prompt>$</prompt> simboliza una
orden a introducirse en el sistema actual del usuario, mientras que
<prompt>#</prompt> se refiere a una orden introducida en la jaula
(<quote>chroot</quote>, N. del t.) de Debian.

</para><para>

Una vez que tenga el nuevo sistema Debian configurado a su preferencia,
puede migrar los datos existentes de sus usuarios (si fuese el caso) a
�ste y continuar funcionando. Esto es por tanto una instalaci�n
<quote>sin parada</quote> de &debian;. Es tambi�n una forma inteligente de
tratar con hardware que no puede utilizarse de forma sencilla con
los distintos mecanismos de instalaci�n o arranque disponibles.

</para>

  <sect2>
  <title>Primeros pasos</title>
<para>

Reparticione el disco duro como lo necesita
con las herramientas de particionado de *nix que disponga,
debe crear por lo menos un sistema de
ficheros m�s una partici�n de intercambio. Necesita
por lo menos 350 MB de espacio disponible para una instalaci�n de
consola o por lo menos 1 GB si va a instalar el entorno X (m�s si
quiere instalar entornos de escritorio como GNOME o KDE).

</para><para>

A continuaci�n, deber� crear sistemas de ficheros en sus particiones. Por ejemplo,
para crear un sistema de ficheros ext3 en la partici�n
<filename>/dev/hda6</filename> (es nuestra partici�n ra�z de ejemplo):

<informalexample><screen>
# mke2fs -j /dev/<replaceable>hda6</replaceable>
</screen></informalexample>

En cambio para crear un sistema de ficheros ext2, omita <userinput>-j</userinput>.

</para><para>

Inicialice y active la partici�n de intercambio (sustituya el n�mero de
partici�n por la partici�n de intercambio de Debian que vaya a utilizar):

<informalexample><screen>
# mkswap /dev/<replaceable>hda5</replaceable>
# sync; sync; sync
# swapon /dev/<replaceable>hda5</replaceable>
</screen></informalexample>

Monte una partici�n como <filename>/mnt/debinst</filename> (el punto
de instalaci�n, que ser� el sistema de ficheros ra�z
(<filename>/</filename>) en su nuevo sistema). El nombre del punto de
montaje es estrictamente arbitrario, pero se utilizar� este mismo
nombre m�s adelante.

<informalexample><screen>
# mkdir /mnt/debinst
# mount /dev/<replaceable>hda6</replaceable> /mnt/debinst
</screen></informalexample>

</para>

<note><para>

Deber� crear y montar manualmente los directorios que sean necesarios antes de
continuar con la siguiente parte del proceso si desea montar partes del sistema
de ficheros (como por ejemplo <quote>/usr</quote>)
en particiones distintas.

</para></note>
  </sect2>

  <sect2>
  <title>Instalar <command>debootstrap</command></title>
<para>

<command>debootstrap</command> es la herramienta que usa el instalador
de Debian, es tambi�n la forma oficial de instalar un sistema base
Debian.  �ste usa <command>wget</command> y <command>ar</command>,
pero, salvo esto, s�lo
depende de <classname>/bin/sh</classname> y algunas herramientas b�sicas
de Unix/Linux<footnote>

<para>
Esto incluye las utilidades principales de GNU y �rdenes como
<command>sed</command>,
<command>grep</command>, <command>tar</command> y <command>gzip</command>.

</para>

</footnote>. Si a�n no lo ha hecho,
instale <command>wget</command> y <command>ar</command>
en su sistema actual, y luego
descargue e instale <command>debootstrap</command>.

</para>

<!-- The files linked to here are from 2004 and thus currently not usable
<para>

Si tiene un sistema basado en rpm, puede usar alien para
convertir el .deb en .rpm, o descargar una versi�n adaptada a rpm de
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink>

</para>
-->

<para>

O bien, puede usar el siguiente procedimiento para instalarlo manualmente.
Cree un directorio temporal <quote>trabajo</quote> para extraer el .deb en �l:

<informalexample><screen>
# mkdir trabajo
# cd trabajo
</screen></informalexample>

El binario de <command>debootstrap</command> se encuentra en el archivo de
Debian (aseg�rese de elegir el fichero adecuado para su arquitectura).
Descargue el .deb de <command>debootstrap</command> desde el almac�n
de paquetes en
<ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/"></ulink>,
copie el paquete al directorio <quote>trabajo</quote> y extraiga los
ficheros de �ste. Necesitar� privilegios de superusuario para
instalar los ficheros.

<informalexample><screen>
# ar -x debootstrap_0.X.X_all.deb
# cd /
# zcat /ruta-completa-hasta-trabajo/trabajo/data.tar.gz | tar xv
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Ejecutar <command>debootstrap</command></title>
<para>

<command>debootstrap</command> descargar� los ficheros necesarios
directamente desde el archivo cuando lo ejecute. Puede sustituir
<userinput>&archive-mirror;/debian</userinput> por cualquier servidor de r�plica
del archivo Debian, en la orden de ejemplo que se muestra a continuaci�n.
Es mejor que utilice un servidor de r�plica cercano (es decir, en una red pr�xima).
Puede encontrar una lista completa de los servidores de r�plica en:
<ulink url="http://www.debian.org/misc/README.mirrors"></ulink>.

</para><para>

Puede utilizar una direcci�n de fichero si tiene un CD de la versi�n
&releasename; de &debian; montado en <filename>/cdrom</filename>. Para
ello utilice, en lugar de la direcci�n http,
<userinput>file:/cdrom/debian/</userinput>.

</para><para>

Sustituya <replaceable>ARQ</replaceable> por alguno de los valores
mostrados a continuaci�n en la orden <command>debootstrap</command> en
funci�n de su arquitectura:

<userinput>alpha</userinput>,
<userinput>amd64</userinput>,
<userinput>arm</userinput>,
<userinput>hppa</userinput>,
<userinput>i386</userinput>,
<userinput>ia64</userinput>,
<userinput>m68k</userinput>,
<userinput>mips</userinput>,
<userinput>mipsel</userinput>,
<userinput>powerpc</userinput>,
<userinput>s390</userinput> o
<userinput>sparc</userinput>.

<informalexample><screen>
# /usr/sbin/debootstrap --arch ARQ &releasename; \
     /mnt/debinst http://ftp.us.debian.org/debian
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Configurar el sistema base</title>
<para>

Ahora tiene instalado un sistema Debian, aunque algo limitado, en
su disco. Ejecute la orden <command>chroot</command> en �l:

<informalexample><screen>
# LANG=C chroot /mnt/debinst /bin/bash
</screen></informalexample>

Despu�s de haaber hecho esto puede que necesite establecer una
definici�n de terminal compatible con el sistema base de Debian. Por
ejemplo:

<informalexample><screen>
# export TERM=<replaceable>xterm-color</replaceable>
</screen></informalexample>

</para>

   <sect3>
   <title>Montar particiones</title>
<para>

Necesita crear <filename>/etc/fstab</filename>.

<informalexample><screen>
# editor /etc/fstab
</screen></informalexample>

Este es un ejemplo que puede modificar a sus necesidades:

<informalexample><screen>
# /etc/fstab: informaci�n est�tica de sistema de ficheros.
#
# file system    mount point   type    options                  dump pass
/dev/XXX         /             ext3    defaults                 0    1
/dev/XXX         /boot         ext3    ro,nosuid,nodev          0    2

/dev/XXX         none          swap    sw                       0    0
proc             /proc         proc    defaults                 0    0

/dev/fd0         /media/floppy auto    noauto,rw,sync,user,exec 0    0
/dev/cdrom       /media/cdrom  iso9660 noauto,ro,user,exec      0    0

/dev/XXX         /tmp          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /var          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /usr          ext3    rw,nodev                 0    2
/dev/XXX         /home         ext3    rw,nosuid,nodev          0    2
</screen></informalexample>

Utilice <userinput>mount -a</userinput> para montar todos los sistemas de
ficheros que ha especificado en <filename>/etc/fstab</filename> o
m�ntelos individualmente usando:

<informalexample><screen>
# mount /ruta  # por ej.:  mount /usr
</screen></informalexample>

Los sistemas Debian actuales tienen puntos de montaje para medios
removibles bajo <filename>/media</filename>, pero mantienen enlaces
simb�licos por compatibilidad en 
<filename>/</filename>. Cree esto si los necesita, como por ejemplo:

<informalexample><screen>
# cd /media
# mkdir cdrom0
# ln -s cdrom0 cdrom
# cd /
# ln -s media/cdrom
</screen></informalexample>

Puede montar el sistema de ficheros <quote>proc</quote> tantas veces como quiera y
en cualquier ubicaci�n, aunque la ubicaci�n habitual es <filename>/proc</filename>.
Aseg�rese de montar proc antes de continuar si no usa <userinput>mount
-a</userinput>:


<informalexample><screen>
# mount -t proc proc /proc
</screen></informalexample>

</para><para>

Si ejecuta la orden <userinput>ls /proc</userinput> deber�a ver que el directorio
no est� vac�o. Si esto falla, puede intentar montar <quote>proc</quote> fuera
del entorno chroot:

<informalexample><screen>
# mount -t proc proc /mnt/debinst/proc
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Configurar la zona horaria</title>
 <para>
 
Una opci�n en el archivo <filename>/etc/default/rcS</filename>
determina si el sistema interpreta el reloj de hardware como UTC o
como hora local. La siguiente orden le permite configurar esto y
elegir su zona horaria.
 
 <informalexample><screen>
# editor /etc/default/rcS
# tzconfig
 </screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Configurar la red</title>
<para>

Para configurar la red, edite
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>,
<filename>/etc/hostname</filename> y
<filename>/etc/hosts</filename>.

<informalexample><screen>
# editor /etc/network/interfaces
</screen></informalexample>

Aqu� hay algunos ejemplos sencillos que podr� encontrar en
<filename>/usr/share/doc/ifupdown/examples</filename>:

<informalexample><screen>
######################################################################
# /etc/network/interfaces -- fichero de configuraci�n para ifup(8), ifdown(8)
# Lea la p�gina de manual de interfaces(5) para informaci�n sobre las
# opciones disponibles.
######################################################################

# Siempre necesitamos la interfaz loopback.
#
auto lo
iface lo inet loopback

# Para usar dhcp:
#
# auto eth0
# iface eth0 inet dhcp

# Un ejemplo de configuraci�n de IP est�tica: (el broadcast y pasarela son
# opcionales)
#
# auto eth0
# iface eth0 inet static
#     address 192.168.0.42
#     network 192.168.0.0
#     netmask 255.255.255.0
#     broadcast 192.168.0.255
#     gateway 192.168.0.1
</screen></informalexample>

Introduzca su servidor o servidores de nombres as� como las
directivas de b�squeda en
<filename>/etc/resolv.conf</filename>:

<informalexample><screen>
# editor /etc/resolv.conf
</screen></informalexample>

Un <filename>/etc/resolv.conf</filename> sencillo de ejemplo ser�a:

<informalexample><screen>
search hqdom.local
nameserver 10.1.1.36
nameserver 192.168.9.100
</screen></informalexample>

Escriba el nombre de m�quina de su sistema (de 2 a 63 caracteres):

<informalexample><screen>
# echo MaquinaDebian &gt; /etc/hostname
</screen></informalexample>

Y una <filename>/etc/hosts</filename> b�sico con soporte IPv6 ser�a:

<informalexample><screen>
127.0.0.1 localhost MaquinaDebian

# Las siguientes l�neas son recomendables en equipos que pueden
# utilizar IPv6
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
</screen></informalexample>

Si tiene m�ltiples tarjetas de red, debe a�adir los nombres de los
m�dulos de controlador en el fichero <filename>/etc/modules</filename>
en el orden que desee. Luego durante el arranque, cada tarjeta ser�
asociada con el nombre de la interfaz (eth0, eth1, etc.) que haya indicado.

</para>
   </sect3>

   <sect3>
   <title>Configurar Apt</title>
 <para>
 
Debootstrap habr� creado un 
<filename>/etc/apt/sources.list</filename> muy b�sico que le permite
instalar paquetes adicioanles. Seguramente querr�, sin embargo, tener
algunas fuentes adicionales para poder acceder a paquetes fuente y a
actualizaciones de seguridad:

<informalexample><screen>
deb-src http://ftp.us.debian.org/debian etch main

deb http://security.debian.org/ etch/updates main
deb-src http://security.debian.org/ etch/updates main
</screen></informalexample>

Aseg�rese de ejecutar <userinput>aptitude update</userinput> despu�s
de hacer cambios a la lista de fuentes.

</para>
   </sect3>

   <sect3>
   <title>Configure Locales and Keyboard</title>
<para>


Instale el paquete de soporte de localizaci�n
<classname>locales</classname> y config�relo para poder definir los
valores de localizaci�n para usar un idioma diferente al
ingl�s. Actualmente se recomienda utilizar locales UTF-8.

<informalexample><screen>
# aptitude install locales
# dpkg-reconfigure locales
</screen></informalexample>

Para configurar su teclado (si lo necesita):
 
<informalexample><screen>
# aptitude install console-data
# dpkg-reconfigure console-data
</screen></informalexample>

</para><para>

Tenga en cuenta que no puede configurar el teclado mientras est� dentro del
chroot, pero se configurar� en el siguiente rearranque.

</para>
   </sect3>
  </sect2>

  <sect2>
  <title>Instalar un n�cleo</title>
<para>

Si intenta arrancar este sistema, probablemente querr� un n�cleo Linux
y un gestor de arranque. Identifique los n�cleos previamente empaquetados
disponibles con:

<informalexample><screen>
# apt-cache search linux-image
</screen></informalexample>

Luego instale su elecci�n usando el nombre del paquete.

</para><para>

Si va a utilizar un n�cleo pre-empaquetado puede querer crear su
propio fichero de configuraci�n
<filename>/etc/kernel-img.conf</filename> antes de hacerlo. Aqu� hay
un fichero de ejemplo:

<informalexample><screen>
# Kernel image management overrides
# See kernel-img.conf(5) for details
do_symlinks = yes
relative_links = yes
do_bootloader = yes
do_bootfloppy = no
do_initrd = yes
link_in_boot = no
</screen></informalexample>

</para><para>

Consulte su p�gina de manual si quiere informaci�n detallada de este
fichero y de las distintas opciones. La p�gina de manual estar�
disponible una vez instale el paquete
<classname>kernel-package</classname>. Le recomendamos que revise si
los valores son correctos para su sistema.

</para><para>

Despu�s instale el paquete del n�cleo que elija utilizando el nombre
del paquete.

<informalexample><screen>
# aptitude install linux-image-<replaceable>&kernelversion;-arch-etc</replaceable>
</screen></informalexample>

Se le har�n algunas preguntas referidas a
<filename>/etc/kernel-img.conf</filename> durante la instalaci�n si no
cre� ese fichero antes de instalar un n�cleo pre-empaquetado.


</para>
  </sect2>

  <sect2>
<title>Configurar el gestor de arranque</title>
<para>

Para poder arrancar su sistema &debian; deber� configurar su gestor de
arranque para cargar el n�cleo instalado en su nueva partici�n
ra�z. Tenga en cuenta que <command>debootstrap</command> no instala un
gestor de arranque, pero puede usar <command>aptitude</command> dentro
de su jaula Debian para hacer esto.

</para><para arch="x86">

Use <userinput>info grub</userinput> o <userinput>man
lilo.conf</userinput> para leer las instrucciones sobre c�mo
configurar el gestor de arranque. Si desea mantener el sistema que
utiliz� para instalar Debian, simplemente a�ada una entrada para la
instalaci�n de Debian en el fichero <filename>menu.lst</filename> o
<filename>lilo.conf</filename> actual. Tambi�n podr�a copiarlo al
nuevo sistema y editarlo all�. Ejecute <command>lilo</command> una vez termine de editar
el fichero (recuerde que �ste usar� el <filename>lilo.conf</filename>
relativo al sistema desde el cual lo ejecute).

</para><para arch="x86">

La instalaci�n y configuraci�n de <classname>grub</classname> es tan
sencilla como hacer:

<informalexample><screen>
# aptitude install grub
# grub-install /dev/<replaceable>hda</replaceable>
# update-grub
</screen></informalexample>

La segunda orden instalar� <command>grub</command> (en este caso en el
MBR de <literal>hda</literal>). La �ltima orden crear� un
<filename>/boot/grub/menu.lst</filename> funcional.

</para><para arch="x86">

Aqu� tiene un ejemplo b�sico de /etc/lilo.conf:

<informalexample><screen>
boot=/dev/<replaceable>hda6</replaceable>
root=/dev/<replaceable>hda6</replaceable>
install=menu
delay=20
lba32
image=/vmlinuz
label=Debian
</screen></informalexample>

</para><para arch="x86">

Puede hacer algunos cambios a
<filename>/etc/kernel-img.conf</filename> dependiendo del cargador de
arranque que haya instalado.

</para><para arch="x86">

Deber�a fijar la opci�n <literal>do_bootloader</literal> a
<quote>no</quote> en el caso de que tenga el cargador de arranque
<classname>grub</classname>. Para actualizar autom�ticamente su
<filename>/boot/grub/menu.lst</filename> cuando se instalen o eliminen
n�cleos de Debian debe a�adir las siguientes l�neas:

<informalexample><screen>
postinst_hook = update-grub
postrm_hook   = update-grub
</screen></informalexample>

El valor de la opci�n <literal>do_bootloader</literal> debe estar
fijado a <quote>yes</quote> en el caso del cargador de arranque
<classname>lilo</classname>.

</para><para arch="powerpc">

Puede consultar <userinput>man yaboot.conf</userinput> para obtener
m�s informaci�n sobre la configuraci�n del gestor de arranque.  Si
desea mantener el sistema que utiliz� para instalar Debian,
simplemente a�ada una entrada para la instalaci�n de Debian al
<filename>yaboot.conf</filename> existente.  Tambi�n podr� copiarlo al
nuevo sistema y editarlo all�. Ejecute ybin despu�s de que finalice la
edici�n (recuerde que �ste usar� el <filename>yaboot.conf</filename>
relativo al sistema desde el cual lo ejecute).

</para><para arch="powerpc">

Aqu� tiene un ejemplo b�sico de <filename>/etc/yaboot.conf</filename>:

<informalexample><screen>
boot=/dev/hda2
device=hd:
partition=6
root=/dev/hda6
magicboot=/usr/lib/yaboot/ofboot
timeout=50
image=/vmlinux
label=Debian
</screen></informalexample>

Puede que tenga que utilizar <userinput>ide0:</userinput>
en algunas m�quinas
en lugar de <userinput>hd:</userinput>.

</para>
  </sect2>

  <sect2>
<title>Toques finales</title>
<para>

El sistema instalado, como se ha mencionado previamente, ser� muy
b�sico. Si quiere que su sistema sea un poco m�s maduro puede hacerlo f�cilmente
instalando todos los paquetes de prioridad <quote>standard</quote>:

<informalexample><screen>
# tasksel install standard
</screen></informalexample>

Por supuesto, tambi�n puede utilizar <command>aptitude</command> para
instalar individualmente todos los paquetes.

</para><para>

Despue de la instalaci�n habr� muchos paquetes descargados en
<filename>/var/cache/apt/archives/</filename>. Puede liberar algo de
espacio de disco ejecutando:

<informalexample><screen>
# aptitude clean
</screen></informalexample>

</para>
  </sect2>

 </sect1>
