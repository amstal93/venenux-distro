<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45239 -->
<!-- revisado jfs, 23 feb. 2005 -->

 <sect1 id="boot-parms"><title>Par�metros de arranque</title>
<para>

Los <quote>par�metros de arranque</quote> son los par�metros del n�cleo
de Linux que generalmente se utilizan para asegurar la
correcta gesti�n de los perif�ricos.
En la mayor�a de los casos el n�cleo puede auto-detectar toda la
informaci�n necesaria sobre sus perif�ricos pero deber� ayudar un poco a su n�cleo
en algunos casos.

</para><para>

Si �sta es la primera vez que arranca su sistema lo recomendable
es utilizar los par�metros de arranque predeterminados.
Es decir, no defina ning�n par�metro adicional.
Observe si su sistema arranca correctamente de esta manera, probablemente
ser� lo haga. Si no lo hace, podr� reiniciar m�s adelante
despu�s de buscar qu� par�metros espec�ficos necesita utilizar para
informar a su sistema del hardware del que dispone.

</para><para>

Puede encontrar informaci�n sobre muchos de los par�metros de arranque en el
<ulink url="http://www.tldp.org/HOWTO/BootPrompt-HOWTO.html">C�MO
de BootPrompt</ulink>, que incluye tambi�n consejos �tiles para hardware
poco com�n.  Esta secci�n solamente contiene un resumen de los par�metros m�s
importantes. Podr� consultar tambi�n algunas precauciones comunes
m�s adelante en la <xref linkend="boot-troubleshooting"/>.

</para><para>

El n�cleo deber� emitir el siguiente mensaje en una de las primeras etapas
del proceso de arranque:

<informalexample><screen>
Memory:<replaceable>avail</replaceable>k/<replaceable>total</replaceable>k available
</screen></informalexample>

El valor <replaceable>total</replaceable> debe corresponder a la cantidad de RAM
disponible, en Kilobytes. Si no corresponde al tama�o real de la RAM que tiene
instalada en su sistema, debe usar el par�metro
<userinput>mem=<replaceable>ram</replaceable></userinput>,
definiendo la cantidad de memoria en <replaceable>ram</replaceable> y
utilizando el sufijo <quote>k</quote> para indicar Kilobytes, o <quote>m</quote> para indicar Megabytes.
Por ejemplo, para indicar que su sistema tiene 64 MB de RAM puede utilizar
tanto <userinput>mem=65536k</userinput> como
<userinput>mem=64m</userinput>.

</para><para condition="supports-serial-console">

Si arranca desde una consola serie el n�cleo habitualmente la detectar�
autom�ticamente<phrase arch="mipsel"> (aunque no en las DECstations)</phrase>.
Si desea arrancar por consola serie un ordenador que ya tiene instalados
una tarjeta de v�deo (framebuffer) y un teclado, deber� indic�rselo al
n�cleo con el par�metro
<userinput>console=<replaceable>dispositivo</replaceable></userinput>,
donde <replaceable>dispositivo</replaceable> es su dispositivo serie,
y ser� generalmente parecido a <filename>ttyS0</filename>.

</para><para arch="sparc">

Los dispositivos serie de &arch-title; son <filename>ttya</filename> o
<filename>ttyb</filename>.
Tambi�n puede definir en <quote>OpenPROM</quote> las variables
<envar>input-device</envar> y
<envar>output-device</envar> a <filename>ttya</filename>.

</para>


  <sect2 id="installer-args"><title>Par�metros del instalador de Debian</title>
<para>

El sistema de instalaci�n reconoce algunos par�metros adicionales que <footnote>

<para>
Con los n�cleos actuales (kernel 2.6.9 o versiones m�s recientes)
puede utilizar treinta y dos opciones de l�nea de �rdenes y treinta y dos
opciones de entorno. Si excede estos n�meros el n�cleo abortar�.

</para>

</footnote> pueden serle �tiles.

</para><para>

Algunos par�metros tienen una <quote>forma abreviada</quote> que
permite evitar las limitaciones de las opciones de l�nea de �rdenes en
el n�cleo y hace m�s f�cil la introducci�n de par�metros. Las formas
abreviadas de los par�metros se mostrar�n entre par�ntesis tras la
forma (normal) extendida. Los ejemplos en este manual generalmente
utilizan la forma simplificada.

</para>

<variablelist>
<varlistentry>
<term>debconf/priority (priority)</term>
<listitem><para>

El valor de este par�metro define la prioridad de los mensajes que se
mostrar�n durante la instalaci�n. No se mostrar� ning�n mensaje de
menor prioridad a la aqu� definida.

</para><para>

La instalaci�n utiliza <userinput>priority=high</userinput>
como valor predeterminado.  Esto significa que se mostrar�n los
mensajes de prioridades alta y cr�tica, pero no as� los mensajes con
prioridades media y baja. El instalador, sin embargo, ajustar� esta
prioridad si se produce alg�n error.

</para><para>

Si utiliza como par�metro de arranque
<userinput>priority=medium</userinput>, se le mostrar� el men�
del instalador y tendr� un mayor control sobre la instalaci�n.  Si usa
<userinput>priority=low</userinput>, se mostrar�n todos los
mensajes (esto es equivalente al m�todo de arranque
<emphasis>experto</emphasis>). Si utiliza
<userinput>priority=critical</userinput>, el sistema de
instalaci�n mostrar� solamente los mensajes cr�ticos e intentar� hacer
lo correcto sin formular muchas preguntas.

</para></listitem>
</varlistentry>

<varlistentry>
<term>DEBIAN_FRONTEND</term>
<listitem><para>

Este par�metro de arranque controla el tipo de interfaz de usuario que
utilizar� el instalador. A continuaci�n se muestran los posibles
valores que puede tomar este par�metro:

<itemizedlist>
<listitem>
<para><userinput>DEBIAN_FRONTEND=noninteractive</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=text</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=newt</userinput></para>
</listitem>
</itemizedlist>

La interfaz predeterminada es
<userinput>DEBIAN_FRONTEND=newt</userinput>.  Si va a realizar una
instalaci�n a trav�s de una consola serie puede que sea mejor utilizar
<userinput>DEBIAN_FRONTEND=text</userinput>.  Esta opci�n ahora
mismo no es muy �til ya que, por regla general, s�lo dispondr� de la
interfaz <userinput>newt</userinput> en el medio de instalaci�n
predeterminado. El instalador gr�fico utiliza la interfaz
<userinput>gtk</userinput> en las arquitecturas en las que est�
soportado.

</para></listitem>
</varlistentry>


<varlistentry>
<term>BOOT_DEBUG</term>
<listitem><para>

Si establece este par�metro a dos conseguir� que el proceso de arranque del
instalador genere registros m�s detallados. Si lo establece a tres obtendr� un
int�rprete de l�nea de �rdenes en puntos estrat�gico del proceso de arranque
(salga del int�rprete para continuar con el proceso de arranque).

<variablelist>
<varlistentry>
<term><userinput>BOOT_DEBUG=0</userinput></term>
<listitem><para>Este es el valor predeterminado.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=1</userinput></term>
<listitem><para>M�s detallado de lo habitual.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=2</userinput></term>
<listitem><para>Incluye mucha informaci�n de depuraci�n.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=3</userinput></term>
<listitem><para>

Se ejecutan int�rpretes de �rdenes en diversos puntos en el proceso de
arranque para permitir una depuraci�n detallada. Salga del int�rprete
de �rdenes para proseguir con el arranque.

</para></listitem>
</varlistentry>
</variablelist>

</para></listitem>
</varlistentry>


<varlistentry>
<term>INSTALL_MEDIA_DEV</term>
<listitem><para>

Mediante este par�metro se indica la ruta al dispositivo desde donde cargar
el instalador. Por ejemplo,
<userinput>INSTALL_MEDIA_DEV=/dev/floppy/0</userinput>

</para><para>

El disquete de arranque generalmente analiza todos las unidades
de disquete en busca del disquete ra�z. Puede modificar este comportamiento a
trav�s de este par�metro para que busque s�lo en un dispositivo concreto.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/framebuffer (fb)</term>
<listitem><para>

En algunas arquitecturas se utiliza el framebuffer del n�cleo para
ofrecer la opci�n a realizar la instalaci�n en un n�mero diverso de idiomas.
Si el uso de este dispositivo origina problemas en su
sistema puede deshabilitar esta caracter�stica con
el par�metro <userinput>fb=false</userinput>.
Algunos s�ntomas de este problema son mensajes que traten sobre <quote>bterm</quote> o <quote>bogl</quote>,
la aparici�n de una pantalla en blanco o que el sistema se quede congelado
algunos minutos despu�s de iniciarse la instalaci�n.

</para><para arch="x86">

Tambi�n puede utilizar el argumento <userinput>video=vga16:off</userinput>
para deshabilitar el uso del n�cleo framebuffer. Los problemas antes indicados se han
observado en equipos Dell Inspiron con tarjetas Mobile Radeon.

</para><para arch="m68k">

Los problemas antes indicados se han observado en sistemas Amiga 1200 y SE/30.

</para><para arch="hppa">

Se han observado este tipo de problemas en hppa.

</para><note arch="sparc"><para>

El soporte de frameb�fer est� <emphasis>deshabilitado por
omisi�n</emphasis> para &arch-title; debido a los problemas de
pantalla que se producen en algunos sistemas. Esto puede dar lugar a
una presentaci�n visual poco agradable en aquellos sistemas que no
ofrezcan un soporte adecuado del frameb�fer, como es el caso de
aquellos sistemas con tarjetas gr�ficas ATI. Si tiene problemas
visuales con el instalador puede intentar arrancar con el par�metro
<userinput>debian-installer/framebuffer=true</userinput> o
la versi�n reducida <userinput>fb=true</userinput>.

</para></note></listitem>
</varlistentry>

<varlistentry arch="not-s390">
<term>debian-installer/theme (theme)</term>
<listitem><para>

Un tema determina c�mo se muestra la interfaz de usuario del
instalador (colores, iconos, etc.). Los temas disponibles dependen de
la interfaz. Actualmente las interfaces newt y gtk s�lo tienen un tema
denominado <quote>dark</quote> (oscuro, n. del t.) que se ha dise�ado
para aquellas personas con problemas visuales. Puede fijar este tema
arrancando el instalador con 
<userinput>theme=<replaceable>dark</replaceable></userinput>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/probe/usb</term>
<listitem><para>

Puede evitar que se comprueben los dispositivos USB en el arranque
definiendo este valor a <userinput>false</userinput> en caso de
que esta comprobaci�n de lugar a problemas.

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

Por omisi�n, el &d-i; configura autom�ticamente la conexi�n de red a trav�s de
DHCP. No podr� revisar ni cambiar los valores obtenidos de esta forma,
si la prueba tiene �xito. Sin embargo, podr� realizar la configuraci�n
de forma manual en el caso de que falle la solicitud DHCP.

</para><para>

Puede usar el par�metro
<userinput>netcfg/disable_dhcp=true</userinput> para que no se lleve a
cabo la configuraci�n de red usando DHCP, entonces tendr� que introducir
la informaci�n manualmente. Esto puede ser �til si tiene un
servidor DHCP en su red local pero no quiere utilizarlo porque, por
ejemplo, no proporciona valores correctos.


</para></listitem>
</varlistentry>

<varlistentry>
<term>hw-detect/start_pcmcia</term>
<listitem><para>

Si quiere evitar que se ejecuten los servicios PCMCIA deber� definir
este par�metro a <userinput>false</userinput>.  Algunos ordenadores
port�tiles son conocidos por tener problemas en estos casos, y puede
ser recomendable deshabilitarlo.

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/url (url)</term>
<listitem><para>

Este valor especifica la ruta de un fichero de preconfiguraci�n que
se descargar� y utilizar� para automatizar la instalaci�n.
Vea <xref linkend="automatic-install"/>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>cdrom-detect/eject</term>
<listitem><para>

Antes de reiniciar, &d-i; expulsa autom�ticamente el medio �ptico utilizado
durante la instalaci�n por omisi�n. Esto puede no ser necesario si el sistema
no arranca autom�ticamente del CD. En algunos casos puede ser incluso indeseable,
por ejemplo, si la unidad �ptica no puede volver a insertar el medio por s�
mismo y el usuario no est� all� para hacerlo manualmente. Muchas unidades
especiales no son capaces de recargar medios de forma autom�tica, entre �stas
se encuentran las denominadas <quote>slot loading</quote>, <quote>+slim-line</quote>,
y estilo <quote>caddy</quote>.

</para><para>

F�jelo al valor <userinput>false</userinput> para deshabilitar la expulsi�n
autom�tica, pero debe ser consciente de que tiene que asegurar que el sistema
no arranca autom�ticamente de la unidad �ptica una vez se haya realizado
la instalaci�n inicial y el sistema se reinicie.
</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/file (file)</term>
<listitem><para>

Este valor especifica la url de un fichero de preconfiguraci�n que
se cargar� en la instalaci�n autom�tica.
Consulte <xref linkend="automatic-install"/>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/interactive</term>
<listitem><para>

Se se fija a <userinput>true</userinput> (verdadero), se mostrar�n las
preguntas aunque ya hayan sido preconfiguradas. Esta opci�n puede ser
�til para probar o depurar un fichero de preconfiguraci�n. Tenga 
en cuenta que este valor no tiene ning�n efecto sobre los par�metros
que se introducen como par�metros de arranque, pero puede utilizar
una sintaxis especial para �stos. Para m�s informaci�n consulte
<xref linkend="preseed-seenflag"/>.
         
</para></listitem>
</varlistentry>

<varlistentry>
<term>auto-install/enable (auto)</term>
<listitem><para>

Demora las preguntas que se realizan normalmente antes de la
preconfiguraci�n hasta despu�s de configurar la red.
Consulte <xref linkend="preseed-auto"/> para m�s informaci�n de c�mo
utilizar esto para realizar instalaciones automatizadas.

</para></listitem>
</varlistentry>

<varlistentry>
<term>cdrom-detect/eject</term>
<listitem><para>

Antes de reiniciar el sistema, &d-i; expulsa autom�ticamente el medio
�ptico utilizado durante la instalaci�n por omisi�n. Esto puede no ser
necesario si el sistema no arranca de forma autom�tica del CD. En
algunos casos puede incluso no ser deseable, por ejemplo, si la unidad
�ptica no puede volver a insertar el medio y el usuario no est� all�
para hacerlo manualmente. Muchas unidades de distinto tipo (�slot
loading�, �slim-line� y �caddty style�) no pueden recargar el medio
autom�ticamente.

</para><para>

Fije este valor a <userinput>false</userinput> para deshabilitar el
expulsado autom�tico, pero tenga en cuenta que debe asegurarse que el
sistema no arranca de forma autom�tica de la unidad �ptica tras la
instalaci�n inicial.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/allow_unauthenticated</term>
<listitem><para>

Por omisi�n, el instalador obliga a que los repositorios est�n autenticados con
una clave gpg conocida. Si se fija esta opci�n a <userinput>true</userinput>,
se deshabilitar� esta autenciaci�n.  <emphasis role="bold">Aviso: inseguro, no
recomendado.</emphasis>
            
</para></listitem>
</varlistentry>

<varlistentry arch="alpha;m68k;mips;mipsel">
<term>ramdisk_size</term>
<listitem><para>

Este par�metro deber�a siempre estar fijado al valor correcto
cuando es necesario. Debe fijarlo s�lo si ve errores durante el
arranque que indiquen que el disco de ram no se carg� por completo. El
valor se debe dar en kB.

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>mouse/left</term>
<listitem><para>

Los usuarios pueden cambiar el rat�n para que opere para zurdos
fijando este par�metro a <userinput>true</userinput> en la interfaz
gtk (instalador gr�fico).

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>directfb/hw-accel</term>
<listitem><para>

La aceleraci�n hardware en directfb est� deshabilitada por omisi�n en
la interfaz gtk (instalador gr�fico. Debe configurar este par�metro a
<userinput>true</userinput> si desea activarla cuando arranca el
instalador.

</para></listitem>
</varlistentry>


<varlistentry>
<term>rescue/enable</term>
<listitem><para>

Fije este valor a  <userinput>true</userinput> si desea entrar en el modo de rescate
en lugar de hacer una instalaci�n normal. Si desea m�s informaci�n consulte
<xref linkend="rescue"/>.

</para></listitem>
</varlistentry>

</variablelist>


   <sect3 id="preseed-args">
   <title>Utilizar par�metros de arranque para responder a preguntas</title>
<para>

Se puede fijar un valor durante el indicador de arranque para
cualquier pregunta que se realiza durante la instalaci�n, con algunas
excepciones. Realmente esto es s�lo �til para algunos casos
concretos. Puede encontrar instrucciones generales que describen como
hacer esto en <xref linkend="preseed-bootparms"/>. A continuaci�n se
listan algunos ejemplos.

</para>

<variablelist>

<varlistentry>
<term>debian-installer/locale (locale)</term>
<listitem><para>

Puede utilizarse para fijar tanto el idioma como el pa�s para la
instalaci�n. S�lo funcionar� si la localizaci�n est� soportada en
Debian. Por ejemplo, puede utilizar
<userinput>locale=de_CH</userinput> para seleccionar alem�n como 
idioma y Suiza como pa�s.

</para></listitem>
</varlistentry>

<varlistentry>
<term>anna/choose_modules (modules)</term>
<listitem><para>

Puede utilizarse para cargar autom�ticamente componentes del
instalador que no se cargan por omisi�n.
Algunos ejemplos de componentes opcionales que pueden ser �tiles son
<classname>openssh-client-udeb</classname> (para poder utilizar el
programa <command>scp</command> durante la instalaci�n))<phrase
arch="not-s390"> y <classname>ppp-udeb</classname> (que permite la
configuraci�n PPPoE)</phrase>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

Puede fijar este valor a <userinput>true</userinput> si quiere
deshabilitar DHCP y forzar la configuraci�n est�tica de la red.

</para></listitem>
</varlistentry>

<varlistentry>
<term>mirror/protocol (protocol)</term>
<listitem><para>

El instalador utilizar� el protocolo HTTP para descargar ficheros de
las r�plicas de Debian y no es posible cambiar a FTP en instalaciones
que se realizan con prioridad normal. Puede forzar al instalador a
utilizar el protocolo FTP si fija este par�metro a
<userinput>ftp</userinput>. Tenga en cuenta que no podr� seleccionar
una r�plica ftp de una lista, tendr� que introducir el nombre del
sistema donde est� la r�plica manualmente.

</para></listitem>
</varlistentry>

<varlistentry>
<term>tasksel:tasksel/first (tasks)</term>
<listitem><para>

Puede utilizarse para seleccionar tareas que no est�n disponibles en
la lista interactiva de tareas, como pueda ser la tarea
<literal>kde-desktop</literal>.
Para m�s informaci�n consulte <xref linkend="pkgsel"/>.

</para></listitem>
</varlistentry>

</variablelist>

   </sect3>

   <sect3 id="module-parms">
   <title>Pasar par�metros a m�dulos del n�cleo</title>
<para>

Puede pasar par�metros a los m�dulos del n�cleo como se describe en la
documentaci�n del n�cleo si los controladores est�n compilados dentro
de �ste. Sin embargo, no es posible pasar par�metros a los m�dulos
como har�a normalmente en el caso de controladores compilados como
m�dulos, debido a que los m�dulos del n�cleo se carga de una forma un
poco distinta en la instalaci�n a como se hace en un sistema ya
instalado. Tiene que utilizar una sintaxis especial que reconoce el
instalador. Si la utiliza, el instalador se asegurar� que los
par�metros se guarden en el fichero de configuraci�n apropiado y se
utilicen cuando se carguen los m�dulos. Los par�metros que utilice
tambi�n se propagan de forma autom�tica a la configuraci�n utilizada
en el sistema instalado.

</para><para>

Tenga en cuenta que ahora es raro pasar par�metros a los m�dulos. En
la mayor parte de las situaciones el n�cleo podr� analizar el hardware
disponible en un sistema y fijar valores por omisi�n correctos de esta
forma. Existen algunas situaciones, sin embargo, en las que ser�
necesario pasar par�metros manualmente.

</para><para>

Debe utilizar la siguiente sintaxis para fijar par�metros para los m�dulos:

<informalexample><screen>
<replaceable>nombre_m�dulo</replaceable>.<replaceable>nombre_par�metro</replaceable>=<replaceable>valor</replaceable>
</screen></informalexample>

Si necesita pasar m�ltiples par�metros al mismo m�dulo o a distintos
m�dulos s�lo tiene que repetir este formato. Por ejemplo, para indicar a
una tarjeta de red 3Com antigua que utilice el conector BNC (coaxial)
y la interrupci�n (IRQ) 10 deber�a utiliza:

<informalexample><screen>
3c509.xcvr=3 3c509.irq=10
</screen></informalexample>

</para>
   </sect3>

   <sect3 id="module-blacklist">
   <title>Poner m�dulos del n�cleo en una lista negra</title>
<para>

Algunas veces es necesario poner un m�dulo en una lista negra para que
ni el n�cleo ni �udev� lo carguen autom�ticamente. Una raz�n para hacer
esto es cuando un m�dulo concreto causa problemas en su hardware. El
n�cleo tambi�n puede algunas veces listar dos controladores distintos
para el mismo dispositivo. Esto puede hacer que el dispositivo no
funcione correctamente si los controladores entran en conflicto o si
se carga el controlador err�neo primero.

</para><para>

Puede poner un m�dulo en la lista negra con la siguiente sintaxis:

<userinput><replaceable>nombre_m�dulo</replaceable>.blacklist=yes</userinput>.
Esto har� que el m�dulo se ponga en la lista negra en
<filename>/etc/modprobe.d/blacklist.local</filename> tanto durante la
instalaci�n como en el sistema instalado.

</para><para>

Tenga en cuenta que el sistema de instalaci�n puede llegar a cargar el
m�dulo por s� s�lo. Puede evitar esto ejecutando la instalaci�n en
modo experto y deseleccionando el m�dulo de la lista de m�dulos que se
muestra durante las fases de detecci�n de hardware.

</para>
   </sect3>

  </sect2>
 </sect1>

