<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43841 -->

  <sect2 arch="ia64"><title>CD-ROM からの起動</title>

&boot-installer-intro-cd.xml;

  <note>
  <title>CD の内容</title>

<para>
<!--
There are three basic variations of Debian Install CDs.
The <emphasis>Business Card</emphasis> CD has a minimal installation
that will fit on the small form factor CD media.
It requires a network connection in order to install the rest of the
base installation and make a usable system.
The <emphasis>Network Install</emphasis> CD has all of the packages
for a base install but requires a network connection to a Debian
mirror site in order to install the
extra packages one would want for a complete system .
The set of Debian CDs can install a complete system from the wide
range of packages without needing access to the network.
-->
基本的な Debian インストール CD が 3 種類あります。
<emphasis>Business Card</emphasis> CD は、小さいフォームファクタの 
CD メディアに合うような最小インストールを持っています。
それは基本インストールの残りをインストールし、
使用可能なシステムを作るためにネットワーク接続が必要です。
<emphasis>Network Install</emphasis> CD は、
基本インストールのパッケージをすべて持っていますが、
完全なシステムが欲しい場合は、特別パッケージをインストールするのに 
Debian ミラーサイトへのネットワーク接続が必要です。
Debian CD セットは、ネットワークへのアクセスを必要とせずに、
広範囲のパッケージから完全なシステムをインストールできます。

</para>
  </note>

<para>

<!--
The IA-64 architecture uses the next generation Extensible Firmware Interface
(EFI) from Intel.
Unlike the traditional x86 BIOS which knows little about the boot
device other than the partition table and Master Boot Record (MBR),
EFI can read and write files from FAT16 or FAT32 formatted disk
partitions.
This simplifies the often arcane process of starting a system.
The system boot loader and the EFI firmware that supports it have
a full filesystem to store the files necessary for booting the
machine.
This means that the system disk on an IA-64 system has an additional
disk partition dedicated to EFI instead of the simple MBR or boot
block on more conventional systems.
-->
IA-64 アーキテクチャでは、
Intel の Extensible Firmware Interface (EFI) を使用しています。
パーティションテーブルやマスターブートレコード (MBR) 以外に起動デバイスのことを
ほとんど知らない従来の x86 BIOS と異なり、
EFI は FAT16 や FAT32 でフォーマットしたディスクパーティションから、
ファイルを読み書きすることができます。
これは、しばしば不可解なシステムの起動プロセスを単純にします。
システムブートローダーやそれをサポートする EFI ファームウェアは、
機械を起動することに必要なファイルを格納するためには充分な、
ファイルシステムを持っています。
これは、IA-64 システム上のシステムディスクは、
(従来のシステムの単純な MBR やブートブロックに代わる) EFI 専用の
追加ディスクパーティションを持つことを意味します。

</para><para>

<!--
The Debian Installer CD contains a small EFI partition where the
<command>ELILO</command> bootloader, its configuration file, the installer's
kernel, and initial filesystem (initrd) are located.
The running system also contains an EFI partition where the necessary
files for booting the system reside.
These files are readable from the EFI Shell as described below.
-->
Debian インストーラ CD には、
<command>ELILO</command> ブートローダやその設定ファイル、
インストーラーのカーネルや初期ファイルシステム (initrd) が存在する、
小さな EFI パーティションを含んでいます。
稼働中のシステムは、システムを起動するのに必要なファイルが存在する、
EFI パーティションを含んでいます。
これらのファイルは下に記述したように EFI シェルから読むことができます。

</para><para>

<!--
Most of the details of how <command>ELILO</command> actually loads and
starts a system are transparent to the system installer.
However, the installer must set up an EFI partition prior to installing
the base system.  Otherwise, the installation of <command>ELILO</command>
will fail, rendering the system un-bootable.
The EFI partition is allocated and formatted in the partitioning step
of the installation prior to loading any packages on the system disk.
The partitioning task also verifies that a suitable EFI partition is
present before allowing the installation to proceed.
-->
システムインストーラに透過的な部分は、
ほとんどがどのように <command>ELILO</command> が実際にロードし、
システムを起動するかといったものの詳細です。
しかしインストーラは、基本システムをインストールするに先立ち、
EFI パーティションをセットアップする必要があります。
そうでなければ、<command>ELILO</command> のインストールが失敗し、
システムが起動不可能になります。
EFI パーティションは、システムディスクにパッケージをロードする前に、
インストーラのパーティション分割時に割当・フォーマットが行われます。
さらにパーティション分割タスクでは、インストールプロセスが先に進む前に、
適切な EFI パーティションが存在していることを確認します。

</para><para>

<!--
The EFI Boot Manager is presented as the last step of the firmware
initialization.
It displays a menu list from which the user can select
an option.
Depending on the model of system and what other software has been
loaded on the system, this menu may be different from one system
to another.
There should be at least two menu items displayed,
<command>Boot Option Maintenance Menu</command> and
<command>EFI Shell (Built-in)</command>.
Using the first option is preferred, however, if that
option is not available or the CD for some reason does not
boot with it, use the second option.
-->
ファームウェア初期化の最終ステップとして、EFI Boot Manager が起動されます。
ここでは、オプションを選択できるようにメニューリストが表示されます。 
システムの型番や、システムに他のソフトウェアが読み込まれるかによりますが、
このメニューは他のシステムとは異なっているかもしれません。
ここでは <command>Boot Option Maintenance Menu</command> と
<command>EFI Shell (Built-in)</command> の、
少なくとも 2 つのメニュー項目が表示されます。
前者の使用をお奨めしますが、これが有効でなかったり、
何らかの理由で CD から起動できない場合は、後者を使用してください。

</para>

  <warning>
  <title>重要</title>
<para>
<!--
The EFI Boot Manager will select a default boot action, typically
the first menu choice, within a pre-set number of seconds.
This is indicated by a countdown at the bottom of the screen.
Once the timer expires and the systems starts the default action,
you may have to reboot the machine in order to continue the installation.
If the default action is the EFI Shell, you can return to the Boot Manager
by running <command>exit</command> at the shell prompt.
-->
EFI ブートマネージャは、規定の秒数が経過するとデフォルトの起動 
(一般的にメニューの先頭) を行います。
これは画面の下段にカウントダウンで表示されています。
タイマーが終了すると、システムはデフォルトの動作を行います。
インストールを続けるためには、マシンを再起動しなければなりません。
デフォルトの動作が EFI シェルの場合、
シェルプロンプトで <command>exit</command> として、
ブートマネージャに戻ることができます。
</para>
  </warning>

  <sect3 arch="ia64" id="bootable-cd">
  <title>オプション 1: 起動オプションメンテナンスメニューからの起動</title>
<para>

</para>

<itemizedlist>

<listitem><para>
<!--
Insert the CD in the DVD/CD drive and reboot the machine.
The firmware will display the EFI Boot Manager page and menu after
it completes its system initialization.
-->
DVD/CD ドライブに CD を挿入し、マシンを再起動する。
システムの初期化が終わると、
ファームウェアが EFI ブートマネージャの画面やメニューを表示する。
</para></listitem>

<listitem><para>
<!--
Select <command>Boot Maintenance Menu</command> from the menu
with the arrow keys and press <command>ENTER</command>.
This will display a new menu.
-->
メニューから <command>Boot Maintenance Menu</command> を矢印キーで選択し、
<command>ENTER</command> を押す。
ここで新しいメニューを表示する。
</para></listitem>

<listitem><para>
<!--
Select <command>Boot From a File</command> from the menu
with the arrow keys and press <command>ENTER</command>.
This will display a list of devices probed by the firmware.
You should see two menu lines containing either the label
<command>Debian Inst [Acpi ...</command> or
<command>Removable Media Boot</command>.
If you examine the rest of the menu line, you will notice that
the device and controller information should be the same.
-->
メニューから <command>Boot From a File</command> を矢印キーで選択し、
<command>ENTER</command> を押す。
ここでは、ファームウェアが検出したデバイスの一覧を表示する。
その中に、<command>Debian Inst [Acpi ...</command> や
<command>Removable Media Boot</command> といったメニュー行があるはずである。
メニュー行の残りを検討すると、
デバイスやコントローラの情報が同じだと言うことに気がつくだろう。
</para></listitem>

<listitem><para>
<!--
You can choose either of the entries that refer to the CD/DVD
drive.
Select your choice with the arrow keys and press <command>ENTER</command>.
If you choose <command>Removable Media Boot</command> the machine
will immediately start the boot load sequence.
If you choose <command>Debian Inst [Acpi ...</command> instead, it
will display a directory listing of the bootable portion of the
CD, requiring you to proceed to the next (additional) step.
-->
CD/DVD ドライブを参照するエントリを選ぶことができる。
矢印キーで選択し、<command>ENTER</command> を押すこと。
<command>Removable Media Boot</command> を選択すると、
すぐにブートロードシーケンスを始めるだろう。
また <command>Debian Inst [Acpi ...</command> を選択すると、
次 (追加) のステップに移ることを要求し、
CD の起動可能部分のディレクトリ一覧を表示するだろう。
</para></listitem>

<listitem><para>
<!--
You will only need this step if you chose
<command>Debian Inst [Acpi ...</command>.
The directory listing will also show
<command>[Treat like Removable Media Boot]</command> on the next to
the last line.
Select this line with the arrow keys and press <command>ENTER</command>.
This will start the boot load sequence.
-->
このステップは、
<command>Debian Inst [Acpi ...</command> を選択した時のみ必要である。
ディレクトリ一覧には、次の行から最後の行までに 
<command>[Treat like Removable Media Boot]</command> も表示している。
この行を矢印キーで選択し、<command>ENTER</command> を押すと、
ブートロードシーケンスを開始する。
</para></listitem>

</itemizedlist>

<para>

<!--
These steps start the Debian boot loader which will display a
menu page for you to select a boot kernel and options.
Proceed to selecting the boot kernel and options.
-->
以上の手順で、起動カーネルやオプションを選択するメニューを表示する、
Debian ブートローダを起動します。
起動カーネルやオプションの選択に移行してください。

</para>
  </sect3>

  <sect3 arch="ia64" id="boot-with-efi">
  <title>オプション 2: EFI シェルからの起動</title>
<para>
<!--
If, for some reason, option 1 is not successful, reboot the machine
and when the EFI Boot Manager screen appears there should be
one option called <command>EFI Shell [Built-in]</command>.
Boot the Debian Installer CD with the following steps:
-->
何らかの理由でオプション 1 が成功しない場合、
マシンを再起動して EFI ブートマネージャが表示されていれば、
<command>EFI Shell [Built-in]</command> というオプションがあるはずです。
以下のようにして Debian インストーラ CD を起動してください。

</para>

<itemizedlist>

<listitem><para>
<!--
Insert the CD in the DVD/CD drive and reboot the machine.
The firmware will display the EFI Boot Manager page and menu after
it completes system initialization.
-->
DVD/CD ドライブに CD を挿入し、マシンを再起動する。
システムの初期化が終わると、
ファームウェアが EFI ブートマネージャの画面やメニューを表示する。
</para></listitem>

<listitem><para>
<!--
Select <command>EFI Shell</command> from the menu with the arrow keys
and press <command>ENTER</command>.
The EFI Shell will scan all of the bootable devices and display
them to the console before displaying its command prompt.
The recognized bootable partitions on devices will show a device name of
<filename>fs<replaceable>n</replaceable>:</filename>.
All other recognized partitions will be named
<filename>blk<replaceable>n</replaceable>:</filename>.
If you inserted the CD just before entering the shell, this may
take a few extra seconds as it initializes the CD drive.
-->
メニューより、矢印キーで <command>EFI Shell</command> を選択し、
<command>ENTER</command> を押す。
EFI シェルは起動可能なデバイスをすべて読むことができ、
コマンドプロンプトを表示する前にそのデバイスをコンソールに表示する。
認識したそのデバイスの起動可能パーティションは、
<filename>fs<replaceable>n</replaceable>:</filename>
というデバイス名で表し、
その他の認識したパーティションは、
<filename>blk<replaceable>n</replaceable>:</filename> と表す。
シェルが起動する前に CD を挿入した場合、
CD ドライブを初期化するのに数秒よけいにかかる可能性がある。
</para>
</listitem>

<listitem><para>
<!--
Examine the output from the shell looking for the CDROM drive.
It is most likely the <filename>fs0:</filename> device although
other devices with bootable partitions will also show up as
<filename>fs<replaceable>n</replaceable></filename>.
-->
CDROM ドライブを探すシェルからの出力を検討する。
<filename>fs0:</filename> デバイスとなる可能性が高いが、
<filename>fs<replaceable>n</replaceable></filename> という、
起動可能パーティションを持つ他のデバイスとして表される。

</para></listitem>

<listitem><para>
<!--
Enter <command>fs<replaceable>n</replaceable>:</command> and press
<command>ENTER</command> to select that
device where <replaceable>n</replaceable> is the partition number for the
CDROM.  The shell will now display the partition number as its prompt.
-->
デバイスを選ぶのに <command>fs<replaceable>n</replaceable>:</command> 
と入力し、<command>ENTER</command> を押す。
<replaceable>n</replaceable> には CDROM のパーティション番号を指定する。
シェルのプロンプトにパーティション番号が表示される。

</para></listitem>

<listitem><para>
<!--
Enter <command>elilo</command> and press <command>ENTER</command>.
This will start the boot load sequence.
-->
<command>elilo</command> と入力し、<command>ENTER</command> を押す。
これでブートローダが起動される。
</para></listitem>

</itemizedlist>

<para>

<!--
As with option 1, these steps start the Debian boot loader which will
display a menu page for you to select a boot kernel and options.
You can also enter the shorter
<command>fs<replaceable>n</replaceable>:elilo</command> command at
the shell prompt.
Proceed to selecting the boot kernel and options.
-->
オプション 1 と同様に、
以上の手順で起動カーネルやオプションを選択するメニューを表示する
Debian ブートローダを起動します。
シェルプロンプトでもっと短く、
<command>fs<replaceable>n</replaceable>:elilo</command>
と入力することもできます。
起動カーネルやオプションの選択に進んでください。

</para>

  </sect3>

  <sect3 arch="ia64" id="serial-console">
  <title>シリアルコンソールを用いたインストール</title>

<para>

<!--
You may choose to perform an install using a monitor and keyboard
or using a serial connection.  To use a monitor/keyboard setup,
select an option containing the string [VGA console].  To install
over a serial connection, choose an option containing the string
[<replaceable>BAUD</replaceable> baud serial console], where
<replaceable>BAUD</replaceable> is the speed of your serial console.
Menu items for the most typical baud rate settings on the ttyS0
device are preconfigured.
-->
インストールを行うにあたり、モニターとキーボードを使用するか、
シリアル接続を使用するかを選択できます。
モニター・キーボードセットアップを使用するなら、
[VGA console] という文字列が入っているオプションを選択してください。
シリアル接続でインストールするには、
[<replaceable>BAUD</replaceable> baud serial console]
という文字列が入っているオプションを選択してください。
ここで <replaceable>BAUD</replaceable> にはシリアルコンソールの通信速度です。
メニュー項目には、
ttyS0 デバイスの一般的なボーレート (通信速度) があらかじめ設定されています。

</para><para>

<!--
In most circumstances, you will want the installer to use the same
baud rate as your connection to the EFI console.  If you aren't
sure what this setting is, you can obtain it using the command
<command>baud</command> at the EFI shell.
-->
ほとんどの環境では、EFI コンソールに接続するボーレートと同じボーレートで、
インストーラを動かすことになります。
この設定がよくわからなければ、
EFI シェルで <command>baud</command> コマンドを使って取得できます。

</para><para>

<!--
If there is not an option available that is configured for the serial
device or baud rate you would like to use, you may override the console setting
for one of the existing menu options.  For example, to use a
57600 baud console over the ttyS1 device, enter
<command>console=ttyS1,57600n8</command> into
the <classname>Boot:</classname> text window.
-->
使用したいシリアルデバイスやボーレートがオプションで有効でなければ、
既存のメニューオプションのコンソール設定を、上書きしてもかまいません。
例えば、ttyS1 デバイスを 57600 ボーのコンソールとして使用する場合、
<classname>Boot:</classname> テキストウィンドウに 
<command>console=ttyS1,57600n8</command> と入力してください。

</para>

<note><para>
<!--
Most IA-64 boxes ship with a default console setting of 9600 baud.
This setting is rather slow, and the normal installation process
will take a significant time to draw each screen.  You should consider
either increasing the baud rate used for performing the installation,
or performing a Text Mode installation.  See the <classname>Params</classname>
help menu for instructions on starting the installer in Text Mode.
-->
ほとんどの IA-64 ボックスは、
デフォルトのコンソールを 9600 ボーに設定して出荷されています。
この設定は現在かなり遅く、通常のインストールプロセス中、
各画面を描画するのにかなり時間がかかります。
インストールするにあたり、使用するボーレートを増加するか、
テキストモードインストールを検討するべきです。
テキストモードでインストールを開始するには、
<classname>Params</classname> ヘルプメニューの説明をご覧ください。
</para></note>

<warning><para>
<!--
If you select the wrong console type, you
will be able to select the kernel and enter parameters but both
the display and your input will go dead as soon as the kernel starts,
requiring you to reboot before you can begin the installation.
-->
間違ったコンソールタイプを選択すると、
カーネルの選択やパラメータの入力はできますが、
カーネルが起動すると同時に入出力が死んでしまいます。
インストールを始める前に、再起動する必要があるでしょう。
</para></warning>
  </sect3>

  <sect3 arch="ia64" id="kernel-option-menu">
  <title>起動カーネルやオプションの選択</title>

<para>

<!--
The boot loader will display a form with a menu list and a text
window with a <classname>Boot:</classname> prompt.
The arrow keys select an item from the menu and any text typed
at the keyboard will appear in the text window.
There are also help screens which can be displayed by pressing
the appropriate function key.
The <classname>General</classname> help screen explains the menu
choices and the <classname>Params</classname> screen explains
the common command line options.
-->
ブートローダを、メニューリストを備えた形式や、
<classname>Boot:</classname> プロンプトがあるテキストウィンドウの形式で
表示します。
矢印キーでメニューからアイテムを選べますし、
キーボードで入力したテキストを、テキストウィンドウに表示できるでしょう。
また、適切なファンクションキーを押して、ヘルプ画面を表示することもできます。
<classname>General</classname> ヘルプ画面はメニューの選択肢を、
<classname>Params</classname> 画面は、
共通のコマンドラインオプションを説明します。

</para><para>

<!--
Consult the <classname>General</classname> help screen for the
description of the kernels and install modes most appropriate
for your installation.
You should also consult <xref linkend="boot-parms"/> below for any additional
parameters that you may want to set in the <classname>Boot:</classname>
text window.
The kernel version you choose selects the kernel version that will be
used for both the installation process and the installed system.
If you encounter kernel problems with the installation, you may also
have those same problems with the system you install.
The following two steps will select and start the install:
-->
カーネルの説明を <classname>General</classname> ヘルプ画面を調べて、
最も適切なモードをインストールしてください。
<xref linkend="boot-parms"/> 以下でも、<classname>Boot:</classname> 
テキストウィンドウに設定する追加パラメータについて、調べることができます。
選択したカーネルバージョンを、
インストール処理中とインストールしたシステムの両方で、使用することになります。
インストール中にカーネルの問題に遭遇した場合、
インストール後のシステムでも同じ問題に遭遇する可能性があります。
以下の手順で選択しインストールを開始してください。

</para>

<itemizedlist>

<listitem><para>
<!--
Select the kernel version and installation mode most
appropriate to your needs with the arrow keys.
-->
カーネルのバージョンと目的に対して最も適切なモードを、
矢印キーで選択する。
</para></listitem>

<listitem><para>
<!--
Enter any boot parameters by typing at the keyboard.
The text will be displayed directly in the text window.
This is where kernel parameters (such as serial console
settings) are specified.
-->
起動パラメータをキーボードから入力する。
このテキストはテキストウィンドウに直接表示される。
ここでカーネルパラメータ (シリアルコンソールの設定など) の指定を行う。
</para></listitem>

<listitem><para>
<!--
Press <command>ENTER</command>.  This will load and start the
kernel.
The kernel will display its usual initialization messages followed
by the first screen of the Debian Installer.
-->
<command>ENTER</command> を押す。
これでカーネルをロードし起動する。
カーネルは Debian インストーラの第 1 画面に通常の初期化メッセージを表示する。
</para></listitem> 

</itemizedlist>

<para>

<!--
Proceed to the next chapter to continue the installation where you will
set up the language locale, network, and disk partitions.
-->
言語ロケール、ネットワーク、ディスクパーティションをセットアップするよう、
インストールを続けるのには、次章に進んでください。

</para>
  </sect3>
  </sect2>

  <sect2 arch="ia64" id="boot-tftp"><title>TFTP での起動</title>

<para>
<!--
Booting an IA-64 system from the network is similar to a CD boot.
The only difference is how the installation kernel is loaded.
The EFI Boot Manager can load and start programs from a server on
the network.
Once the installation kernel is loaded and starts, the system install
will proceed through the same steps as the CD install with the exception
that the packages of the base install will be loaded from the network
rather than the CD drive.
-->
IA-64 システムをネットワークから起動するのは、CD から起動するのに似ています。
違いは、カーネルをどのようにロードするのかと言ったことだけです。
EFI ブートマネージャは、ネットワーク上のサーバからプログラムをロードし、
実行することができます。
いったんインストールカーネルをロードし起動してしまえば、
基本インストールを CD ドライブからでなくネットワークから読み込むことを除けば、
CD インストールと同じ手順で、システムのインストールを行うことになります。

</para>

&boot-installer-intro-net.xml;

<para>

<!--
Network booting an IA-64 system requires two architecture-specific actions.
On the boot server, DHCP and TFTP must be configured to deliver  
<command>elilo</command>.
On the client a new boot option must be defined in the EFI boot manager
to enable loading over a network.
-->
IA-64 システムでのネットワークブートは、
2 つのアーキテクチャ特有のアクションが必要です。
ブートサーバでは、elilo に伝えるために DHCP と TFTPを設定しなければなりません。
クライアントでは、新しいブートオプションを、
ネットワーク越しのロードができるよう EFI ブートマネージャに定義しなければなりません。

</para>
  <sect3 arch="ia64" id="boot-tftp-server">
  <title>サーバの設定</title>
<para>

<!--
A suitable TFTP entry for network booting an IA-64 system looks something
like this:
-->
IA-64 システムのネットワークブート用の適切な TFTP エントリは、
以下のようになります:

<informalexample><screen>
host mcmuffin {
        hardware ethernet 00:30:6e:1e:0e:83;
        fixed-address 10.0.0.21;
        filename "debian-installer/ia64/elilo.efi";
}
</screen></informalexample>

<!--
Note that the goal is to get <command>elilo.efi</command> running on
the client.
-->
<command>elilo.efi</command> をクライアントで実行することが目的なのに、
注意してください。

</para><para>

<!--
Extract the <filename>netboot.tar.gz</filename> file into the directory used
as the root for your tftp server.  Typical tftp root directories include
<filename>/var/lib/tftp</filename> and <filename>/tftpboot</filename>.
This will create a <filename>debian-installer</filename> directory
tree containing the boot files for an IA-64 system.
-->
<filename>netboot.tar.gz</filename> ファイルを、
tftp サーバのルートで使用するディレクトリに展開してください。
典型的な tftp ルートディレクトリは <filename>/var/lib/tftp</filename> や
<filename>/tftpboot</filename> を含んでいます。
これにより、IA-64 システムの起動ファイルを含む、
<filename>debian-installer</filename> ディレクトリを作成します。

</para><para>

<informalexample><screen>
# cd /var/lib/tftp
# tar xvfz /home/user/netboot.tar.gz
./
./debian-installer/
./debian-installer/ia64/
[...]
</screen></informalexample>

<!--
The <filename>netboot.tar.gz</filename> contains an
<filename>elilo.conf</filename> file that should work for most configurations.
However, should you need to make changes to this file, you can find it in the
<filename>debian-installer/ia64/</filename> directory.
-->
<filename>netboot.tar.gz</filename> には、
ほとんど設定済みの <filename>elilo.conf</filename> ファイルが格納されています。
しかし、万一このファイルを変更するなら、
<filename>debian-installer/ia64/</filename> ディレクトリにあります。

<!--
It is possible to have different config files for different clients by naming
them using the client's IP address in hex with the suffix
<filename>.conf</filename> instead of <filename>elilo.conf</filename>.
See documentation provided in the <classname>elilo</classname> package
for details.
-->
<filename>elilo.conf</filename> の代わりに、
名前にクライアントのIPアドレス (16 進) を使用して、
拡張子 <filename>.conf</filename> を持つ、
別のクライアント用の config ファイルを持つことができます。
詳細は、<classname>elilo</classname> パッケージで提供されるドキュメントを、
参照してください。

</para>
  </sect3>
  <sect3 arch="ia64" id="boot-tftp-client">
  <title>クライアントの設定</title>
<para>

<!--
To configure the client to support TFTP booting, start by booting to
EFI and entering the <guimenu>Boot Option Maintenance Menu</guimenu>.
-->
TFTP ブートをサポートするクライアントを設定するのは、
EFI を起動し、<guimenu>Boot Option Maintenance Menu</guimenu> 
へ入るところから始めます。

<itemizedlist>
<listitem><para>

<!--
Add a boot option.
-->
起動オプションを加えてください。
</para><para>

</para></listitem>
<listitem><para>

<!--
You should see one or more lines with the text
<guimenuitem>Load File [Acpi()/.../Mac()]</guimenuitem>.  If more
than one of these entries exist, choose the one containing the
MAC address of the interface from which you'll be booting.
Use the arrow keys to highlight your choice, then press enter.
-->
<guimenuitem>Load File [Acpi()/.../Mac()]</guimenuitem> 
というテキストがある行を見てください。複数行存在する場合、
起動したいインターフェースの MAC アドレスを含むものを選択してください。
矢印キーで選択肢を反転し、enter を押してください。
</para></listitem>
<listitem><para>

<!--
Name the entry <userinput>Netboot</userinput> or something similar,
save, and exit back to the boot options menu.
-->
このエントリに <userinput>Netboot</userinput> や似た名前を指定し、
保存後、ブートオプションメニューへ抜けてください。

</para></listitem>
</itemizedlist>

<!--
You should see the new boot option you just created, and selecting it
should initiate a DHCP query, leading to a TFTP load of
<filename>elilo.efi</filename> from the server.
-->
今作成した新しい起動オプションが、
サーバから <filename>elilo.efi</filename> を読み込むような TFTP を導く、
DHCP クエリを始めるようになっているか、確認した方がいいでしょう。

</para><para>

<!--
The boot loader will display its prompt after it has downloaded and
processed its configuration file.
At this point, the installation proceeds with the same steps as a
CD install.  Select a boot option as in above and when the kernel
has completed installing itself from the network, it will start the
Debian Installer.
-->
その設定ファイルをダウンロードし処理した後に、
ブートローダは自身のプロンプトを表示します。
ポイントは CD でインストールするのと同様の手順で、
インストールが行われると言うことです。
上述のように起動オプションを選択し、
ネットワークからカーネルの読み込みが完了すれば、
Debian インストーラを起動します。

</para><para>
<!--
Proceed to the next chapter to continue the installation where
you will set up the language locale, network, and disk partitions.
-->
言語ロケール、ネットワーク、ディスクパーティションをセットアップするよう、
インストールを続けるのには、次章に進んでください。

</para>
  </sect3>
  </sect2>
