<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 44560 -->

 <sect1 id="boot-troubleshooting">
 <title>インストールプロセスのトラブルシューティング</title>
<para>
</para>

  <sect2 arch="not-s390" id="unreliable-cd">
<!--
  <title>CD-ROM Reliability</title>
-->
  <title>CD-ROM の信頼性</title>
<para>

<!--
Sometimes, especially with older CD-ROM drives, the installer may fail
to boot from a CD-ROM. The installer may also &mdash; even after booting
successfully from CD-ROM &mdash; fail to recognize the CD-ROM or return
errors while reading from it during the installation.
-->
時々、特に古い CD-ROM ドライブの場合、
CD-ROM からのインストーラの起動に失敗するかもしれません。
また、インストーラは (その CD-ROM から起動しても) CD-ROM を認識しなかったり、
インストール中、 CD の読み込みでエラーを返す可能性もあります。

</para><para>

<!--
There are a many different possible causes for these problems. We can
only list some common issues and provide general suggestions on how to
deal with them. The rest is up to you.
-->
この問題の原因は様々なことが考えられます。
一般的な問題を挙げて、一般的な対処法を提供することしかできません。
後はあなた次第です。

</para><para>

<!--
There are two very simple things that you should try first.
-->
まずはじめに試すのは、以下の 2 点です。

<itemizedlist>
<listitem><para>

<!--
If the CD-ROM does not boot, check that it was inserted correctly and that
it is not dirty.
-->
CD-ROM が起動しない場合、正しく挿入されているか、
汚れていないかを確認してください。

</para></listitem>
<listitem><para>

<!--
If the installer fails to recognize a CD-ROM, try just running the option
<menuchoice> <guimenuitem>Detect and mount CD-ROM</guimenuitem> </menuchoice>
a second time. Some DMA related issues with older CD-ROM drives are known to
be resolved in this way.
-->
インストーラが CD-ROM を認識しない場合、次に 
<menuchoice> <guimenuitem>CD-ROM の検出とマウント</guimenuitem> </menuchoice>
を実行してください。
古い CD-ROM ドライブの DMA に関連する問題は、
この方法で解決することが知られています。

</para></listitem>
</itemizedlist>

</para><para>

<!--
If this does not work, then try the suggestions in the subsections below.
Most, but not all, suggestions discussed there are valid for both CD-ROM and
DVD, but we'll use the term CD-ROM for simplicity.
-->
これでも動作しない場合、以下の節にあることを試してみてください。
ほとんどの (でもすべてではない) 提案は CD-ROM と DVD の両方で有効ですが、
単純に CD-ROM という用語を用いています。

</para><para>

<!--
If you cannot get the installation working from CD-ROM, try one of the
other installation methods that are available.
-->
CD-ROM からインストールができなければ、
他のインストール方法も試してみてください。

</para>

  <sect3>
<!--
  <title>Common issues</title>
-->
  <title>共通の問題</title>

<itemizedlist>
  <listitem><para>

<!--
Some older CD-ROM drives do not support reading from discs that were burned
at high speeds using a modern CD writer.
-->
古い CD-ROM ドライブの中には、最近の CD ライタで使用するような、
高速で焼いた ディスクからの読み込みをサポートしていない物があります。

  </para></listitem>
  <listitem><para>

<!--
If your system boots correctly from the CD-ROM, it does not necessarily
mean that Linux also supports the CD-ROM (or, more correctly, the controller
that your CD-ROM drive is connected to).
-->
システムが CD-ROM から正しく起動するといって、
Linux がその CD-ROM ドライブ
(もっと正確に言うと、CD-ROM ドライブがつながっているコントローラ) 
をサポートしているとは限りません。

  </para></listitem>
  <listitem><para>

<!--
Some older CD-ROM drives do not work correctly if <quote>direct memory
access</quote> (DMA) is enabled.
-->
古い CD-ROM ドライブの中には、
<quote>ダイレクトメモリアクセス</quote> (DMA) が有効だと、
正しく動作しない物もあります。

  </para></listitem>
</itemizedlist>

  </sect3>

  <sect3>
<!--
  <title>How to investigate and maybe solve issues</title>
-->
  <title>調査および問題解決の方法</title>
<para>

<!--
If the CD-ROM fails to boot, try the suggestions listed below.
-->
CD-ROM が起動に失敗したら、以下のことを試してください。

<itemizedlist>
  <listitem><para>

<!--
Check that your BIOS actually supports booting from CD-ROM (older systems
possibly don't) and that your CD-ROM drive supports the media you are using.
-->
BIOS が CD-ROM からの起動をきちんとサポートしているか 
(古いシステムはおそらく無理)、
CD-ROM ドライブが使う予定のメディアをサポートしているかをチェックしてください。

  </para></listitem>
  <listitem><para>

<!--
If you downloaded an iso image, check that the md5sum of that image matches
the one listed for the image in the <filename>MD5SUMS</filename> file that
should be present in the same location as where you downloaded the image
from.
-->
ISO イメージをダウンロードした場合、
イメージをダウンロードしたのと同じ場所にある <filename>MD5SUMS</filename> 
に記載されている md5sum と同じかどうかチェックしてください。

<informalexample><screen>
$ md5sum <replaceable>debian-testing-i386-netinst.iso</replaceable>
a20391b12f7ff22ef705cee4059c6b92  <replaceable>debian-testing-i386-netinst.iso</replaceable>
</screen></informalexample>

<!--
Next, check that the md5sum of the burned CD-ROM matches as well. The
following command should work. It uses the size of the image to read the
correct number of bytes from the CD-ROM.
-->
次に、焼いた CD-ROM の md5sum と一致するかどうかチェックしてください。
以下のコマンドで行います。
CD-ROM から正しいバイト数を読み込むのにイメージのサイズを利用します。

<informalexample><screen>
$ dd if=/dev/cdrom | \
> head -c `stat --format=%s <replaceable>debian-testing-i386-netinst.iso</replaceable>` | \
> md5sum
a20391b12f7ff22ef705cee4059c6b92  -
262668+0 records in
262668+0 records out
134486016 bytes (134 MB) copied, 97.474 seconds, 1.4 MB/s
</screen></informalexample>

  </para></listitem>
</itemizedlist>

</para><para>

<!--
If, after the installer has been booted successfully, the CD-ROM is not
detected, sometimes simply trying again may solve the problem. If you have
more than one CD-ROM drive, try changing the CD-ROM to the other drive.
If that does not work or if the CD-ROM is recognized but there are errors
when reading from it, try the suggestions listed below. Some basic knowledge
of Linux is required for this.
To execute any of the commands, you should first switch to the second
virtual console (VT2) and activate the shell there.
-->
インストーラの起動が成功した後で、CD-ROM を検出しない場合、
単純にリトライするだけで解決することもあります。
CD-ROM ドライブが複数ある場合、他の CD-ROM ドライブに変えてみてください。
それでも動作しなかったり、CD-ROM を認識しても読み込みエラーが発生する場合は、
以下のことを試してみてください。Linux の基礎知識が少し必要です。
コマンドを実行するには、まず第 2 仮想コンソール (VT2) に切り替えて、
シェルを有効にしてください。

<itemizedlist>
  <listitem><para>

<!--
Switch to VT4 or view the contents of <filename>/var/log/syslog</filename>
(use <command>nano</command> as editor) to check for any specific error
messages. After that, also check the output of <command>dmesg</command>.
-->
エラーメッセージをチェックするのに VT4 に切り替えたり、
<filename>/var/log/syslog</filename> の内容を 
(エディタの <command>nano</command> を使用して) 表示してください。
その後、<command>dmesg</command> の出力でもチェックできます。

  </para></listitem>
  <listitem><para>

<!--
Check in the output of <command>dmesg</command> if your CD-ROM drive was
recognized. You should see something like (the lines do not necessarily
have to be consecutive):
-->
CD-ROM ドライブを認識したかを <command>dmesg</command> 
の出力でチェックしてください。以下のように見えます。
(行は連続している必要はありません)

<informalexample><screen>
Probing IDE interface ide1...
hdc: TOSHIBA DVD-ROM SD-R6112, ATAPI CD/DVD-ROM drive
ide1 at 0x170-0x177,0x376 on irq 15
hdc: ATAPI 24X DVD-ROM DVD-R CD-R/RW drive, 2048kB Cache, UDMA(33)
Uniform CD-ROM driver Revision: 3.20
</screen></informalexample>

<!--
If you don't see something like that, chances are the controller your CD-ROM
is connected to was not recognized or may be not supported at all. If you
know what driver is needed for the controller, you can try loading it manually
using <command>modprobe</command>.
-->
以上のように見えなければ、CD-ROM を接続したコントローラを認識できないか、
おそらく全くサポートされていません。
コントローラに必要なドライバが分かっていれば、
<command>modprobe</command> を用い、手で読み込むのを試せます。

  </para></listitem>
  <listitem><para>

<!--
Check that there is a device node for your CD-ROM drive under
<filename>/dev/</filename>. In the example above, this would be
<filename>/dev/hdc</filename>.
There should also be a <filename>/dev/cdroms/cdrom0</filename>.
-->
<filename>/dev/</filename> にある 
CD-ROM ドライブのデバイスノードをチェックしてください。
上の例では、<filename>/dev/hdc</filename> になっています。
<filename>/dev/cdroms/cdrom0</filename> にもあるかもしれません。

  </para></listitem>
  <listitem><para>

<!--
Use the <command>mount</command> command to check if the CD-ROM is already
mounted; if not, try mounting it manually:
-->
CD-ROM がすでにマウントされていないか、
<command>mount</command> コマンドでチェックしてください。
マウントされていなければ、手でマウントしてください。

<informalexample><screen>
$ mount /dev/<replaceable>hdc</replaceable> /cdrom
</screen></informalexample>

<!--
Check if there are any error messages after that command.
-->
上記のコマンド後に、エラーメッセージがでるかチェックしてください。

  </para></listitem>
  <listitem><para>

<!--
Check if DMA is currently enabled:
-->
DMA が有効か、以下のようにチェックしてください。

<informalexample><screen>
$ cd /proc/<replaceable>ide</replaceable>/<replaceable>hdc</replaceable>
$ grep using_dma settings
using_dma      1       0       1       rw
</screen></informalexample>

<!--
A <quote>1</quote> in the first column after <literal>using_dma</literal>
means it is enabled. If it is, try disabling it:
-->
<literal>using_dma</literal> の後、初めの列にある <quote>1</quote> は、
有効という意味です。その場合以下のように無効にしてください。

<informalexample><screen>
$ echo -n "using_dma:0" >settings
</screen></informalexample>

<!--
Make sure that you are in the directory for the device that corresponds
to your CD-ROM drive.
-->
確実に、CD-ROM ドライブに一致するデバイスのディレクトリで操作してください。

  </para></listitem>
  <listitem><para>

<!--
If there are any problems during the installation, try checking the integrity
of the CD-ROM using the option near the bottom of the installer's main menu.
This option can also be used as a general test if the CD-ROM can be read
reliably.
-->
インストール中に何か問題があれば、インストーラメインメニューの下の方にある、
CD-ROM の整合性チェックを行ってください。
CD-ROM が確実に読める場合、このオプションを一般的なテストとして使用できます。

  </para></listitem>
</itemizedlist>

</para>
  </sect3>
  </sect2>

  <sect2 condition="supports-floppy-boot" id="unreliable-floppies">
  <title>フロッピーディスクの信頼性</title>

<para>

<!--
The biggest problem for people using floppy disks to install Debian
seems to be floppy disk reliability.
-->
フロッピーディスクを用いて Debian をインストールする人がつまずく最大の問題は、
フロッピーディスクの信頼性だと思います。

</para><para>

<!--
The boot floppy is the floppy with the worst problems, because it
is read by the hardware directly, before Linux boots.  Often, the
hardware doesn't read as reliably as the Linux floppy disk driver, and
may just stop without printing an error message if it reads incorrect
data. There can also be failures in the driver floppies, most of which
indicate themselves with a flood of messages about disk I/O errors.
-->
特にブートフロッピーが最も問題になるようです。
これは Linux が起動する前に、ハードウェアから直接読み込まれるからでしょう。
ハードウェアは Linux のフロッピーディスクドライバほど
信頼性の高い方法で読み込みを行ってくれないことが多く、
正しくないデータに当たると、
エラーメッセージも表示せずに単に止まってしまいます。
ドライバフロッピーで問題が起きることもあるようで、
この場合は大抵、ディスク I/O エラーに関するメッセージが大量に表示されます。

</para><para>

<!--
If you are having the installation stall at a particular floppy, the first
thing you should do is write the image to a <emphasis>different</emphasis>
floppy and see if that solves the problem. Simply reformatting the old
floppy may not be sufficient, even if it appears that the floppy was
reformatted and written with no errors. It is sometimes useful to try
writing the floppy on a different system.
-->
インストールが特定のフロッピーで停止してしまう場合は、
まず <emphasis>別の</emphasis> フロッピーに書き直して、
問題が解決するか見てください。
古いフロッピーをフォーマットし直すだけでは充分ではありません
(そのフロッピーのフォーマットや書き込み時にエラーが出なかったとしても、です)。
フロッピーを別のシステムで書き込んでみると、
うまくいくこともあるようです。

</para><para>

<!--
Normally you should not have download a floppy image again, but if you
are experiencing problems it is always useful to verify that the images
were downloaded correctly by verifying their md5sums.
-->
通常、フロッピーイメージを再度ダウンロードする必要はありませんが、
問題が常に発生する場合は、イメージのダウンロードが正しく行われているかを、
md5sum を検証して確認するといいでしょう。

</para><para>

<!--
One user reports he had to write the images to floppy
<emphasis>three</emphasis> times before one worked, and then
everything was fine with the third floppy.
-->
あるユーザの報告によると、イメージのフロッピーへの書き込みを
<emphasis>3 回</emphasis>やり直さないと、
うまく動くようにならなかったそうです。
その 3 番目のフロッピーでは、何も問題なくいったそうです。

</para><para>

<!--
Other users have reported that simply rebooting a few times with the
same floppy in the floppy drive can lead to a successful boot. This is
all due to buggy hardware or firmware floppy drivers.
-->
また別のユーザからの報告では、
同じフロッピーをドライブに入れたまま数回再起動を繰り返すだけで、
うまく起動したのだそうです。
これはハードウェアかファームウェアのフロッピードライバの
できが悪かったためでしょう。

</para>
  </sect2>

  <sect2><title>起動設定</title>

<para>

<!--
If you have problems and the kernel hangs during the boot process,
doesn't recognize peripherals you actually have, or drives are not
recognized properly, the first thing to check is the boot parameters,
as discussed in <xref linkend="boot-parms"/>.
-->
ブートプロセスの最中にカーネルがハングしたり、
搭載されている周辺機器やドライブが正確に認識されないなどの問題が起こったら、
まず <xref linkend="boot-parms"/> の説明に従って
ブートパラメータを確認してください。

</para><para>

<!--
Often, problems can be solved by removing add-ons and peripherals, and
then trying booting again.  <phrase arch="x86">Internal modems, sound
cards, and Plug-n-Play devices can be especially problematic.</phrase>
-->
また、増設カードや周辺機器を取り外して再起動してみると、
このような問題が解決できることもよくあります。
<phrase arch="x86">内蔵モデム、サウンドカード、
Plug-n-Play デバイスなどは特に問題となりがちです。</phrase>

</para><para>

<!--
If you have a large amount of memory installed in your machine, more
than 512M, and the installer hangs when booting the kernel, you may
need to include a boot argument to limit the amount of memory the
kernel sees, such as <userinput>mem=512m</userinput>.
-->
マシンにメモリがたくさん (512M 以上) 積まれていて、
インストーラがカーネルの起動時にハングする場合は、
<userinput>mem=512m</userinput> のようなブート引き数を使って、
カーネルが扱うメモリの量を制限する必要があるかもしれません。

</para>
  </sect2>

  <sect2 arch="x86" id="i386-boot-problems">
<!--
  <title>Common &arch-title; Installation Problems</title>
-->
  <title>&arch-title; へのインストールに共通の問題</title>
<para>

<!--
There are some common installation problems that can be solved or avoided by
passing certain boot parameters to the installer.
-->
インストールの際の共通の問題がいくつかあり、
これはインストーラに渡す起動パラメータで解決したり、
確実にバイパスして回避したりできます。

</para><para>

<!--
Some systems have floppies with <quote>inverted DCLs</quote>. If you receive
errors reading from the floppy, even when you know the floppy is good,
try the parameter <userinput>floppy=thinkpad</userinput>.
-->
システムによっては ``inverted DCLs'' 
つきのフロッピードライブを装備したものがあります。
フロッピーディスク自体には問題がないはずなのに、
読み込み中にエラーが出た場合には、
<userinput>floppy=thinkpad</userinput> というパラメータを試してください。

</para><para>

<!--
On some systems, such as the IBM PS/1 or ValuePoint (which have ST-506
disk drivers), the IDE drive may not be properly recognized.  Again,
try it first without the parameters and see if the IDE drive is
recognized properly.  If not, determine your drive geometry
(cylinders, heads, and sectors), and use the parameter
<userinput>hd=<replaceable>cylinders</replaceable>,<replaceable>heads</replaceable>,<replaceable>sectors</replaceable></userinput>.
-->
IBM PS/1 や (ST-506 ディスクドライバを装備した) ValuePoint
などのシステムでは、IDE ドライブが正しく認識されないことがあります。
繰り返しになりますが、まずパラメータなしでカーネルを起動し、
IDE ドライブが正しく認識されるかどうかを見てください。
もし認識できなかったら、
ドライブのジオメトリ (シリンダ・ヘッダ・セクタ) を調べて、
<userinput>hd=<replaceable>cylinders</replaceable>,<replaceable>heads</replaceable>,<replaceable>sectors</replaceable></userinput>
というパラメータを使ってください。

</para><para>

<!--
If you have a very old machine, and the kernel hangs after saying
<computeroutput>Checking 'hlt' instruction...</computeroutput>, then
you should try the <userinput>no-hlt</userinput> boot argument, which
disables this test.
-->
非常に古いマシンにおいて、
<computeroutput>Checking 'hlt' instruction...</computeroutput>
と表示されたあとにカーネルがハングしてしまう場合は、
ブート引数 <userinput>no-hlt</userinput> を使って、
このチェックを無効にしてみるとよいでしょう。

</para><para>

<!--
If your screen begins to show a weird picture while the kernel boots,
eg. pure white, pure black or colored pixel garbage, your system may
contain a problematic video card which does not switch to the
framebuffer mode properly.  Then you can use the boot parameter
<userinput>fb=false video=vga16:off</userinput> to disable the framebuffer
console. Only a reduced set of
languages will be available during the installation due to limited
console features. See <xref linkend="boot-parms"/> for details.
-->
カーネルの起動時に画面が不思議な絵 (例: 真っ白、真っ黒、色付きピクセル屑) 
を表示し始める場合、あなたのシステムは、
フレームバッファモードに適切に変わらない問題のあるビデオカードがついているかもしれません。
その場合、フレームバッファコンソールを無効にするため、ブートパラメータに 
<userinput>fb=false video=vga16:off</userinput> を使用できます。
コンソールの機能が制限されているため、
インストール中には特定の言語しか使えません。
詳細は <xref linkend="boot-parms"/> をご覧ください。

</para>

  <sect3>
<!--
  <title>System Freeze During the PCMCIA Configuration Phase</title>
-->
  <title>PCMCIA 設定中のシステムフリーズ</title>
<para>

<!--
Some laptop models produced by Dell are known to crash when PCMCIA device
detection tries to access some hardware addresses. Other laptops may display
similar problems. If you experience such a problem and you don't need PCMCIA
support during the installation, you can disable PCMCIA using the
<userinput>hw-detect/start_pcmcia=false</userinput> boot parameter. You can
then configure PCMCIA after the installation is completed and exclude the
resource range causing the problems.
-->
DELL のラップトップモデルのいくつかは、
PCMCIA デバイス検出でいくつかハードウェアアドレスをアクセスしようとすると、
クラッシュすることが知られています。
他のラップトップコンピュータでも、同様の問題が起きるかもしれません。
そのような問題に遭遇しても、PCMCIA サポートが必要でなければ、
<userinput>hw-detect/start_pcmcia=false</userinput> というブートパラメータで、
PCMCIA を無効にできます。
インストールが完了した後、PCMCIA を設定し、
問題が起きるリソース範囲を除外することができます。

</para><para>

<!--
Alternatively, you can boot the installer in expert mode. You will
then be asked to enter the resource range options your hardware
needs. For example, if you have one of the Dell laptops mentioned
above, you should enter <userinput>exclude port
0x800-0x8ff</userinput> here. There is also a list of some common
resource range options in the <ulink
url="http://pcmcia-cs.sourceforge.net/ftp/doc/PCMCIA-HOWTO-1.html#ss1.12">System
resource settings section of the PCMCIA HOWTO</ulink>. Note that you
have to omit the commas, if any, when you enter this value in the
installer.
-->
その他、インストーラをエキスパートモードで起動することができます。
その後、ハードウェアが必要とするリソース範囲オプションを、
入力するように促されます。
例えば、上記の Dell のラップトップコンピュータを持っていれば、
ここで <userinput>exclude port 0x800-0x8ff</userinput> と入力するといいです。
さらにいくつかの共通のリソース範囲オプションのリストが<ulink
url="http://pcmcia-cs.sourceforge.net/ftp/doc/PCMCIA-HOWTO-1.html#ss1.12">
PCMCIA HOWTO のシステムリソースの設定</ulink> にあります。
インストーラにこの値を入力する場合、
もしあればカンマを省略しなければならないことに注意してください。

</para>
   </sect3>

   <sect3>
<!--
   <title>System Freeze while Loading USB Modules</title>
-->
   <title>USB モジュールロード中のシステムフリーズ</title>
<para>

<!--
The kernel normally tries to install USB modules and the USB keyboard driver
in order to support some non-standard USB keyboards. However, there are some
broken USB systems where the driver hangs on loading. A possible workaround
may be disabling the USB controller in your mainboard BIOS setup. Another option
is passing the <userinput>debian-installer/probe/usb=false</userinput> parameter
at the boot prompt, which will prevent the modules from being loaded.
-->
カーネルは、非標準 USB キーボードをサポートするように、
通常 USB モジュールと USB キーボードドライバをインストールしようとします。
しかし、ドライバがロード中に止まるような、
イカレた USB システムがまれにあります。
これはマザーボードの BIOS セットアップで、
USB コントローラを無効にできるかもしれません。
別の方法は、
ブートプロンプトに <userinput>debian-installer/probe/usb=false</userinput> 
パラメータを渡すことです。
これにより USB ハードウェアが検知されても、
モジュールがロードされるのを防ぐことができます。

</para>
   </sect3>
  </sect2>

  <sect2 arch="sparc" id="sparc-boot-problems">
<!--
  <title>Common &arch-title; Installation Problems</title>
-->
  <title>&arch-title; へのインストールに共通の問題</title>
<para>

<!--
There are some common installation problems that are worth mentioning.
-->
以下、言及しておくべき、インストール時の一般的な問題があります。

</para>
   <sect3>
<!--
   <title>Misdirected video output</title>
-->
   <title>ビデオ出力先の間違い</title>
<para>

<!--
It is fairly common for &arch-title; to have two video cards in one machine,
for example an ATI card and a Sun Creator 3D. In some cases, this may result
in the video output getting misdirected soon after the system boots. In
typical cases, the display will only show:
-->
ATI カードと Sun Creator 3D といった、
1 台のマシンに 2 枚のビデオカードがある構成は、&arch-title; によくあります。
その場合、システム起動後にビデオ出力先を間違うということがあります。
典型的なケースは、
ディスプレイに以下のメッセージしか表示されないといった場合です。

<informalexample><screen>
Remapping the kernel... done
Booting Linux...
</screen></informalexample>

<!--
To work around this, you can either pull out one of the video cards, or
disable the one not used during the OpenProm boot phase using a kernel
parameter. For example, to disable an ATI card, you should boot the
installer with <userinput>video=atyfb:off</userinput>.
-->
これに対処するには、ビデオカードを引き抜くか、
OpenProm ブートフェイズでカーネルパラメータを与え、
使用しないカードを無効にしてください。
例えば、ATI カードを無効にするには、
<userinput>video=atyfb:off</userinput> をインストーラに与えて起動してください。

</para><para>

<!--
Note that you may also have to manually add this parameter to the silo
configuration (edit <filename>/target/etc/silo.conf</filename> before
rebooting) and, if you installed X11, modify the video driver in
<filename>/etc/X11/xorg.conf</filename>.
-->
このパラメータを silo の設定に、手で加えなければならない 
(再起動の前に <filename>/target/etc/silo.conf</filename> を編集する) 
ことに注意してください。
また、X11 をインストールしている場合、
<filename>/etc/X11/xorg.conf</filename> のビデオドライバを修正してください。

</para>
   </sect3>

   <sect3>
<!--
   <title>Failure to Boot or Install from CD-ROM</title>
-->
   <title>起動に失敗ないし CD-ROM からインストール</title>
<para>

<!--
Some Sparc systems are notoriously difficult to boot from CD-ROM and
even if they do boot, there may be inexplicable failures during the
installation. Most problems have been reported with SunBlade systems.
-->
いくつかの Sparc システムは、CD-ROM から起動するのが難しく、
起動してもインストール時に不可解な失敗をすることが知られています。
ほとんどの問題は SunBlade システムで報告されています。

</para><para>

<!--
We recommend to install such systems by netbooting the installer.
-->
そのようなシステムでは、インストーラをネットから起動するのをお奨めします。

</para>
   </sect3>
  </sect2>

  <sect2 id="kernel-msgs">
  <title>カーネルの起動時メッセージの意味</title>

<para>

<!--
During the boot sequence, you may see many messages in the form
<computeroutput>can't find <replaceable>something</replaceable></computeroutput>,
or <computeroutput><replaceable>something</replaceable>
not present</computeroutput>, <computeroutput>can't initialize
<replaceable>something</replaceable></computeroutput>,
or even <computeroutput>this driver release depends
on <replaceable>something</replaceable></computeroutput>.
Most of these messages are harmless. You
see them because the kernel for the installation system is built to
run on computers with many different peripheral devices. Obviously, no
one computer will have every possible peripheral device, so the
operating system may emit a few complaints while it looks for
peripherals you don't own. You may also see the system pause for a
while. This happens when it is waiting for a device to respond, and
that device is not present on your system. If you find the time it
takes to boot the system unacceptably long, you can create a
custom kernel later (see <xref linkend="kernel-baking"/>).
-->
ブートシーケンスの途中で、
<computeroutput>can't find 
<replaceable>something</replaceable></computeroutput> (〜が見つからない),
 <computeroutput><replaceable>something</replaceable> 
not present</computeroutput> (〜が存在しない),
<computeroutput>can't initialize 
<replaceable>something</replaceable></computeroutput> (〜を初期化できない),
<computeroutput>this driver release depends on 
<replaceable>something</replaceable></computeroutput> (このドライバには〜が必要だ)
などのメッセージがたくさん出力されることがあります。
これらのメッセージのほとんどは無害です。
これらが出力される理由は、インストールシステムのカーネルが、
いろいろな周辺デバイスのできるだけ多くに対応しようとしているからです。
そのため、OS が実際には存在しない周辺機器を探すことになるので、
文句を吐くわけです。
システムがしばらく止まったように見えることもあります。
これはデバイスが反応するのを待っているために起こるものです
(実際にはそのデバイスは存在しないので、止まってみえるわけです)。
システムの起動に要する時間が堪えがたいほど長い場合は、
後で自前のカーネルを作ることもできます
(<xref linkend="kernel-baking"/> 参照)。

</para>
  </sect2>


  <sect2 id="problem-report">
<!--
  <title>Reporting Installation Problems</title>
-->
  <title>インストールで発生した問題の報告</title>
<para>

<!--
If you get through the initial boot phase but cannot complete the install,
the menu option <guimenuitem>Save debug logs</guimenuitem> may be helpful.
It lets you store system error logs and configuration information from the
installer to a floppy, or download them using a web browser.
-->
最初の起動段階は通過したのに、インストールが完了できなかった場合は、
メニューから <guimenuitem>デバッグログを保存</guimenuitem> 
を選択するといいかもしれません。
インストーラからのシステムのエラーログや設定情報をフロッピーに格納したり、
web ブラウザでダウンロードしたりできるようになります。

<!--       
This information may provide clues as to what went wrong and how to
fix it.  If you are submitting a bug report, you may want to attach
this information to the bug report.
-->
この情報は、何が間違っていてどのように修正するか、
といった手がかりを示しているかもしれません。
バグ報告を送る際に、バグ報告にこの情報を付けることができます。

</para><para>

<!--
Other pertinent installation messages may be found in
<filename>/var/log/</filename> during the
installation, and <filename>/var/log/installer/</filename>
after the computer has been booted into the installed system.
-->
その他のインストールメッセージは、インストール中では
<filename>/var/log/</filename> で、
インストールしたシステムが起動した後では
<filename>/var/log/installer/</filename> にあるはずです。

</para>
  </sect2>

  <sect2 id="submit-bug">
  <title>インストールレポートの送信</title>
<para>

<!--
If you still have problems, please submit an installation report. We also
encourage installation reports to be sent even if the installation is
successful, so that we can get as much information as possible on the largest
number of hardware configurations. 
-->
まだ問題がある場合には、インストールレポートをお送りください。
また、インストールが成功したときのインストールレポートもお送りください。
そうすると、たくさんのハードウェア設定情報を手に入れることができます。

</para><para>

<!--
Note that your installation report will be published in the Debian Bug
Tracking System (BTS) and forwarded to a public mailing list. Make sure that
you use an e-mail address that you do not mind being made public.
-->
あなたのインストールレポートは、Debian バグ追跡システム (BTS) で公開され、
公開メーリングリストに転送されることに留意してください。
必ず、公開されても問題ない e-mail アドレスを使用してください。

</para><para>

<!--
If you have a working Debian system, the easiest way to send an installation
report is to install the <classname>installation-report</classname> and
<classname>reportbug</classname> packages
(<command>aptitude install installation-report reportbug</command>),
configure <classname>reportbug</classname> as explained in
<xref linkend="mail-outgoing"/>, and run the command <command>reportbug
installation-reports</command>.
-->
動作する Debian システムがある場合、
インストールレポートを送る最も簡単な方法は以下のようになります。
<classname>installation-report</classname> と 
<classname>reportbug</classname> パッケージをインストール
(<command>aptitude install installation-report reportbug</command>) し、
<xref linkend="mail-outgoing"/> で説明しているように 
<classname>reportbug</classname> を設定して、
<command>reportbug installation-reports</command> を実行してください。

</para><para>

<!--
Alternatively you can use this template when filling out
installation reports, and file the report as a bug report against the
<classname>installation-reports</classname> pseudo package, by sending it to
<email>submit@bugs.debian.org</email>.
-->
その他、インストールレポートを記入する際には、
以下のテンプレートも使用できます。
そのファイルを、<classname>installation-reports</classname> 
疑似パッケージのバグ報告として、
<email>submit@bugs.debian.org</email> 宛にお送りください。

<informalexample><screen>
Package: installation-reports


Boot method: &lt;インストーラの起動方法は? CD? フロッピー? ネットワーク?&gt;
Image version: &lt;イメージをダウンロードした URL がベスト&gt;
Date: &lt;インストールした日時&gt;

Machine: &lt;マシンの説明 (例 IBM Thinkpad R32)&gt;
Processor:
Memory:
Root Device: &lt;IDE?  SCSI?  デバイス名は?&gt;
Partitions: &lt;df -Tl の結果; 優先される生のパーティションテーブル&gt;

Output of lspci -nn and lspci -vnn:

Base System Installation Checklist:
[O] = OK, [E] = Error (please elaborate below), [ ] = didn't try it

Initial boot:           [ ]
Detect network card:    [ ]
Configure network:      [ ]
Detect CD:              [ ]
Load installer modules: [ ]
Detect hard drives:     [ ]
Partition hard drives:  [ ]
Install base system:    [ ]
Clock/timezone setup:   [ ]
User/password setup:    [ ]
Install tasks:          [ ]
Install boot loader:    [ ]
Overall install:        [ ]

Comments/Problems:

&lt;簡単なインストールの説明、初期インストールの考察、コメント、アイデアなど&gt;
</screen></informalexample>

<!--
In the bug report, describe what the problem is, including the last
visible kernel messages in the event of a kernel hang.  Describe the
steps that you did which brought the system into the problem state.
-->
バグ報告の際には、
カーネルがハングした直前に表示されたカーネルメッセージを添えて、
何が問題なのかを説明してください。
また、問題が起きるまでにシステムに対して行ったことも記述してください。

</para>
  </sect2>
 </sect1>
