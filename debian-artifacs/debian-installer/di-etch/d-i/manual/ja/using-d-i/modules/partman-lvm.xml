<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43577 -->

   <sect3 id="partman-lvm">
<!--
   <title>Configuring the Logical Volume Manager (LVM)</title>
-->
   <title>論理ボリュームマネージャ (LVM) の設定</title>
<para>

<!--
If you are working with computers at the level of system administrator
or <quote>advanced</quote> user, you have surely seen the situation
where some disk partition (usually the most important one) was short on
space, while some other partition was grossly underused and you had to
manage this situation with moving stuff around, symlinking, etc.
-->
システム管理者や<quote>上級</quote>ユーザとしてコンピュータを動かしていると、
ディスク内のあるパーティション (たいてい最も重要なもの) が足らなくなり、
他のパーティションは全体的にあまり使用されていないという状況が確実にあります。
このような場合は、
内容を移動したりシンボリックリンクを張るといった管理を行うことになります。

</para><para>

<!--
To avoid the described situation you can use Logical Volume Manager
(LVM). Simply said, with LVM you can combine your partitions
(<firstterm>physical volumes</firstterm> in LVM lingo) to form
a virtual disk (so called <firstterm>volume group</firstterm>), which
can then be divided into virtual partitions (<firstterm>logical
volumes</firstterm>). The point is that logical volumes (and of course
underlying volume groups) can span across several physical disks.
-->
上記のような状況を避けるために、論理ボリュームマネージャ (LVM) を利用できます。
簡単に言うと、LVM では複数のパーティション
(LVM 用語で <firstterm>物理ボリューム (physical volumes)</firstterm>) 
を仮想ディスクの形に結合でき、このディスクを仮想パーティション 
(<firstterm>論理ボリューム (logical volumes)</firstterm>) に分割できます。
ポイントは、論理ボリュームは (もちろんその下のボリュームグループも)、
複数の物理ディスクをまたがって定義できると言うことです。

</para><para>

<!--
Now when you realize you need more space for your old 160GB
<filename>/home</filename> partition, you can simply add a new 300GB
disk to the computer, join it with your existing volume group and then
resize the logical volume which holds your <filename>/home</filename>
filesystem and voila &mdash; your users have some room again on their
renewed 460GB partition.  This example is of course a bit
oversimplified. If you haven't read it yet, you should consult the
<ulink url="&url-lvm-howto;">LVM HOWTO</ulink>.
-->
例えば、古い 160GB の<filename>/home</filename> パーティションに、
もっと容量を追加することを考えます。
単にあなたは新しい 300GB のディスクをコンピュータに追加し、
既存のボリュームグループに入れます。
その後 <filename>/home</filename> ファイルシステムを保持したまま
論理ボリュームをリサイズします。
するとほら、パーティションが 460GB へと新品交換されたので、
ユーザの空き容量がすこしばかり増えたことになります。
もちろんこの例は少し単純にしすぎです。
まだ読んでいないようなら、
<ulink url="&url-lvm-howto;">LVM HOWTO</ulink> を調べるべきです。

</para><para>

<!--
LVM setup in &d-i; is quite simple and completely supported inside
<command>partman</command>. First, you have to mark the partition(s)
to be used as physical volumes for LVM. This is done in the
<guimenu>Partition settings</guimenu> menu where you should select
<menuchoice> <guimenu>Use as:</guimenu> <guimenuitem>physical volume
for LVM</guimenuitem> </menuchoice>.

-->
&d-i; での LVM のセットアップはかなりシンプルで、
<command>partman</command> 内部で完全にサポートしています。
始めに、パーティションを LVM の物理ボリュームとして使用するよう、
マークをつけねばなりません。
これは、<guimenu>パーティション設定</guimenu> メニューの 
<command>partman</command> 内で
<menuchoice> <guimenu>利用方法:</guimenu> <guimenuitem>LVM 
の物理ボリューム</guimenuitem> </menuchoice> を選ぶことで行います

</para><para>

<!--
When you return to the main <command>partman</command> screen, you will see
a new option <guimenuitem>Configure the Logical Volume Manager</guimenuitem>.
When you select that, you will first be asked to confirm pending changes to
the partition table (if any) and after that the LVM configuration menu will
be shown. Above the menu a summary of the LVM configuration is shown. The
menu itself is context sensitive and only shows valid actions. The possible
actions are:
-->
メインの <command>partman</command> 画面に戻ると、
<guimenuitem>論理ボリュームマネージャの設定</guimenuitem> 
が新しく選択できるようになっています。
これを選択すると、まず決定していないパーティションテーブルへの変更 (があれば) 
確認を行い、その後 LVM 設定メニューを表示します。
メニューの上部には LVM 設定の概要を表示します。
メニュー自体はそのときに実行できる操作のみ表示します。
行える操作は以下の通りです。

<itemizedlist>
  <listitem><para>
<!--
    <guimenuitem>Display configuration details</guimenuitem>:
    shows LVM device structure, names and sizes of logical volumes and more
-->
    <guimenuitem>設定の詳細表示</guimenuitem>:
    LVM デバイスの構造、論理ボリュームの名称やサイズなどを表示します
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Create volume group</guimenuitem>
-->
    <guimenuitem>ボリュームグループの作成</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Create logical volume</guimenuitem>
-->
    <guimenuitem>論理ボリュームの作成</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Delete volume group</guimenuitem>
-->
    <guimenuitem>ボリュームグループの削除</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Delete logical volume</guimenuitem>
-->
    <guimenuitem>論理ボリュームの削除</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Extend volume group</guimenuitem>
-->
    <guimenuitem>ボリュームグループの拡張</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Reduce volume group</guimenuitem>
-->
    <guimenuitem>ボリュームグループの縮小</guimenuitem>
  </para></listitem>
  <listitem><para>
<!--
    <guimenuitem>Finish</guimenuitem>:
    return to the main <command>partman</command> screen
-->
    <guimenuitem>完了</guimenuitem>:
    メインの <command>partman</command> 画面に戻ります
  </para></listitem>
</itemizedlist>

</para><para>

<!--
Use the options in that menu to first create a volume group and then create
your logical volumes inside it.
-->
はじめにボリュームグループを作成し、その中に論理ボリュームを作成するのに、
このメニューのオプションを使用してください。

</para><para>

<!--
After you return to the main <command>partman</command> screen, any created
logical volumes will be displayed in the same way as ordinary partitions
(and you should treat them as such).
-->
メインの <command>partman</command> 画面に戻ると、
作成した論理ボリュームが通常のボリュームと同じように表示されています。
(そして同じように扱えます)

</para>
   </sect3>
