<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45347 -->

 <sect1 id="what-is-debian">
 <title>Debian とは?</title>
<para>

<!--
Debian is an all-volunteer organization dedicated to developing free
software and promoting the ideals of the Free Software community.
The Debian Project began in 1993, when Ian Murdock issued an open
invitation to software developers to contribute to a complete and
coherent software distribution based on the relatively new Linux
kernel.  That relatively small band of dedicated enthusiasts,
originally funded by the
<ulink url="&url-fsf;">Free Software Foundation</ulink>
and influenced by the
<ulink url="&url-gnu-intro;">GNU</ulink>
philosophy, has grown over the years into an organization of around
&num-of-debian-developers; <firstterm>Debian Developers</firstterm>.
-->
Debian は、有志の集まってできた団体で、フリーソフトウェアを
開発し、フリーソフトウェアコミュニティの理想を推進することを目的としています。
Debian プロジェクトは 1993 年に、
比較的新しい Linux カーネルをもとにした、完全で一貫性ある
ディストリビューションの制作のために、Ian Murdock
が開発者を広く募ったときに始まりました。
献身的なファンたちの比較的小さな団体は、最初
<ulink url="&url-fsf;">Free Software Foundation</ulink>によって支援を受け、
<ulink url="&url-gnu-intro;">GNU</ulink>の哲学に影響されていましたが、
数年後には &num-of-debian-developers; 人もの 
<firstterm>Debian 開発者</firstterm> を抱える組織になりました。

</para><para>

<!--
Debian Developers are involved in a variety of activities, including
<ulink url="&url-debian-home;">Web</ulink>
and <ulink url="&url-debian-ftp;">FTP</ulink>
site administration, graphic design, legal analysis of
software licenses, writing documentation, and, of course, maintaining
software packages.
-->
Debian 開発者は様々な活動に参加しています。例えば、
<ulink url="&url-debian-home;">Web</ulink> や
<ulink url="&url-debian-ftp;">FTP</ulink> サイトの管理、
グラフィックデザイン、ソフトウェアライセンスの法律的な分析、
文書の執筆、そしてもちろん、ソフトウェアパッケージの
メンテナンスです。

</para><para>

<!--
In the interest of communicating our philosophy and attracting
developers who believe in the principles that Debian stands for, the
Debian Project has published a number of documents that outline our
values and serve as guides to what it means to be a Debian Developer:
-->
私たちの哲学を伝え、Debian が支持する原則を信じている開発者を
引き寄せるために、Debian プロジェクトは、私たちの価値の概略を
述べ、Debian 開発者であるとはどういうことかという指針とする
ために、多数の文書を発表しています:

<itemizedlist>
<listitem><para>

<!--
The
<ulink url="&url-social-contract;">Debian Social Contract</ulink> is
a statement of Debian's commitments to the Free Software Community.
Anyone who agrees to abide to the Social Contract may become a
<ulink url="&url-new-maintainer;">maintainer</ulink>.
Any maintainer can introduce new software into Debian &mdash; provided
that the software meets our criteria for being free, and the package
follows our quality standards.
-->
<ulink url="&url-social-contract;">Debian 社会契約</ulink>
は、Debian のフリーソフトウェアコミュニティへの関与について述べたもの
です。この社会契約を守ることに同意する人は、誰でも
<ulink url="&url-new-maintainer;">メンテナ</ulink> になることができます。
メンテナは誰でも、Debian に新しいソフトウェアを追加することができます
&mdash; そのソフトウェアが私たちの条件に照らしてフリーであり、
パッケージの品質が基準を満たしていれば。

</para></listitem>
<listitem><para>

<!--
The
<ulink url="&url-dfsg;">Debian Free Software Guidelines</ulink> are a
clear and concise statement of Debian's criteria for free software.
The DFSG is a very influential document in the Free Software Movement,
and was the foundation of the
<ulink url="&url-osd;">The Open Source Definition</ulink>.
-->
<ulink url="&url-dfsg;">Debian フリーソフトウェアガイドライン</ulink> (DFSG)
 は、フリーソフトウェアに関する Debian の基準を明確かつ簡潔に述べたもの
です。この DFSG は、フリーソフトウェア運動において非常に影響力のある
文書で、<ulink url="&url-osd;">オープンソースの定義</ulink> のもととなったものです。

</para></listitem>
<listitem><para>

<!--
The
<ulink url="&url-debian-policy;">Debian Policy Manual</ulink> is an
extensive specification of the Debian Project's standards of quality.
-->
<ulink url="&url-debian-policy;">Debian ポリシーマニュアル</ulink>は、
Debian プロジェクトの品質基準を詳しく定めたものです。

</para></listitem>
</itemizedlist>
</para><para>

<!--
Debian developers are also involved in a number of other projects;
some specific to Debian, others involving some or all of the Linux
community.  Some examples include:
-->
Debian 開発者は、ほかの多数のプロジェクトにも関与しています。
それらのプロジェクトには、Debian 固有のものもあり、Linux
コミュニティの一部や全体に関係するものもあります。例えば、

<itemizedlist>
<listitem><para>

<!--
The
<ulink url="&url-lsb-org;">Linux Standard Base</ulink>
(LSB) is a project aimed at standardizing the basic GNU/Linux system,
which will enable third-party software and hardware developers to
easily design programs and device drivers for Linux-in-general, rather
than for a specific GNU/Linux distribution.
-->
<ulink url="&url-lsb-org;">Linux Standard Base</ulink> (LSB) は、
基本的な GNU/Linux システムを標準化し、
サードパーティのソフトウェア・ハードウェア開発者が
 (特定の GNU/Linux ディストリビューションではなく)
一般的に Linux 向けにプログラムやデバイスドライバを簡単に
設計することができるようにすることを目的としたプロジェクトです。

</para></listitem>
<listitem><para>

<!--
The
<ulink url="&url-fhs-home;">Filesystem Hierarchy Standard</ulink>
(FHS) is an effort to standardize the layout of the Linux
file system. The FHS will allow software developers to concentrate
their efforts on designing programs, without having to worry about how
the package will be installed in different GNU/Linux distributions.
-->
<ulink url="&url-fhs-home;">Filesystem Hierarchy Standard</ulink>
 (FHS) は、Linux のファイルシステムのレイアウトを標準化しようと
いう試みです。これによって、ソフトウェア開発者は
パッケージが様々な GNU/Linux ディストリビューションにどのように
インストールされるかを心配することなしに、プログラムのデザインに
努力を集中することができます。

</para></listitem>
<listitem><para>

<!--
<ulink url="&url-debian-jr;">Debian Jr.</ulink>
is an internal project, aimed at making sure Debian has something to
offer to our youngest users.
-->
<ulink url="&url-debian-jr;">Debian Jr.</ulink> は、
Debian を若年ユーザに提供できるようなものにするための内部プロジェクトです。

</para></listitem>
</itemizedlist>

</para><para>

<!--
For more general information about Debian, see the
<ulink url="&url-debian-faq;">Debian FAQ</ulink>.
-->
より一般的な情報については、
<ulink url="&url-debian-faq;">Debian FAQ</ulink> を参照して下さい。

</para>

 </sect1>

