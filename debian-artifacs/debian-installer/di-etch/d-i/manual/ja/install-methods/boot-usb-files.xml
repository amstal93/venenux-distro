<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45616 -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>USB メモリでの起動用ファイルの準備</title>

<para>

<!--
To prepare the USB stick, you will need a system where GNU/Linux is
already running and where USB is supported. You should ensure that the
usb-storage kernel module is loaded (<userinput>modprobe
usb-storage</userinput>) and try to find out which SCSI device the USB
stick has been mapped to (in this example
<filename>/dev/sda</filename> is used). To write to your stick, you
may have to turn off its write protection switch.
-->
USB メモリの準備をするには、
GNU/Linux が既に動いていて、USB をサポートしているシステムが必要になります。
usb-storage カーネルモジュールをきちんとロードして 
(<userinput>modprobe usb-storage</userinput>)、
USB メモリをマッピングしているのがどの SCSI デバイスかを検出してみるべきです。
(この例では <filename>/dev/sda</filename> を使用します) 
USB メモリに書き込むために、
ライトプロテクトスイッチを切る必要があります。

</para><para>

<!--
Note, that the USB stick should be at least 128 MB in size (smaller
setups are possible if you follow <xref linkend="usb-copy-flexible"/>).
-->
USB メモリは少なくとも 128 MB のサイズがなければなりません
(<xref linkend="usb-copy-flexible"/> のようにすれば、
より小さなセットアップが可能です)。

</para>

  <sect2 id="usb-copy-easy">
  <title>ファイルのコピー &mdash; 簡単な方法</title>
<para arch="x86">

<!--
There is an all-in-one file <filename>hd-media/boot.img.gz</filename>
which contains all the installer files (including the kernel) as well
as <command>SYSLINUX</command> and its configuration file. You only
have to extract it directly to to your USB stick:
-->
<command>SYSLINUX</command> や その設定ファイルと同様に、
(カーネルを含む) インストーラの全ファイルが入った
<filename>hd-media/boot.img.gz</filename> という
オールインワンファイルがあります。
以下のように USB メモリに直接展開してください。

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para><para arch="powerpc">

<!--
There is an all-in-one file <filename>hd-media/boot.img.gz</filename>
which contains all the installer files (including the kernel) as well
as <command>yaboot</command> and its configuration file. Create a
partition of type "Apple_Bootstrap" on your USB stick using
<command>mac-fdisk</command>'s <userinput>C</userinput> command and
extract the image directly to that:
-->
<command>yaboot</command> や その設定ファイルと同様に、
(カーネルを含む) インストーラの全ファイルが入った
<filename>hd-media/boot.img.gz</filename> という
オールインワンファイルがあります。
<command>mac-fdisk</command> の <userinput>C</userinput> コマンドで
USB メモリに "Apple_Bootstrap" タイプのパーティションを作成し、
以下のように USB メモリに直接展開してください。

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sda2</replaceable>
</screen></informalexample>

</para>
<warning><para>

<!--
Using this method will destroy anything already on the device. Make sure
that you use the correct device name for your USB stick.
-->
この方法を使うとデバイス上の既存の物は破壊されてしまいます。
USB メモリの正しいデバイス名を確認して使用してください。

</para></warning>
<para>

<!--
After that, mount the USB memory stick (<userinput>mount
<replaceable arch="x86">/dev/sda</replaceable>
<replaceable arch="powerpc">/dev/sda2</replaceable>
/mnt</userinput>), which will now have
<phrase arch="x86">a FAT filesystem</phrase>
<phrase arch="powerpc">an HFS filesystem</phrase>
on it, and copy a Debian netinst or businesscard ISO image to it
(see <xref linkend="usb-add-iso"/>).
Unmount the stick (<userinput>umount /mnt</userinput>) and you are done.
-->
その後、USB メモリ 
(<phrase arch="x86">FAT ファイルシステム</phrase>
<phrase arch="powerpc">HFS ファイルシステム</phrase>になっている) をマウントし 
(<userinput>mount <replaceable arch="x86">/dev/sda</replaceable>
<replaceable arch="powerpc">/dev/sda2</replaceable>
/mnt</userinput>)、そこに Debian netinst か、
名刺型 ISO イメージをコピーしてください (<xref linkend="usb-add-iso"/> 参照)。
終わったら、USB メモリをアンマウントしてください 
(<userinput>umount /mnt</userinput>)。

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>ファイルのコピー &mdash; 柔軟な方法</title>
<para>

<!--
If you like more flexibility or just want to know what's going on, you
should use the following method to put the files on your stick.
-->
もっと柔軟なものがよかったり、何が起きているか知りたいのなら、
以下に説明する USB メモリにファイルを置く方法を使用すべきです。

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

   </sect2>

   <sect2 id="usb-add-iso">
<!--
   <title>Adding an ISO image</title>
-->
   <title>ISO イメージの追加</title>
<para>

<!--
The installer will look for a Debian ISO image on the stick as its source
for additional data needed for the installation. So your next step is to
copy a Debian ISO image (businesscard, netinst or even a full CD image)
onto your stick (be sure to select one that fits). The file name of the
image must end in <filename>.iso</filename>.
-->
インストーラは、USB メモリ上の Debian ISO イメージを探し、
インストールに必要な追加データの取得元とします。
ですから次のステップは、
Debian ISO イメージ (名刺、netinst、完全版のいずれか) を、
USB メモリに (入るものを選んで) 入れることです。
ISO イメージのファイル名は <filename>.iso</filename> 
で終わっていなければなりません。

</para><para>

<!--
If you want to install over the network, without using an ISO image,
you will of course skip the previous step. Moreover you will have to
use the initial ramdisk from the <filename>netboot</filename>
directory instead of the one from <filename>hd-media</filename>,
because <filename>hd-media/initrd.gz</filename> does not have network
support.
-->
ISO イメージを使わずネットワーク越しにインストールしたければ、
もちろん前の手順をスキップしてください。
さらに、<filename>hd-media/initrd.gz</filename> 
はネットワークをサポートしないので、
<filename>hd-media</filename> のものの代わりに、 
<filename>netboot</filename> ディレクトリにある
初期 RAM ディスクを使わなければならないでしょう。

</para><para>

<!--
When you are done, unmount the USB memory stick (<userinput>umount
/mnt</userinput>) and activate its write protection switch.
-->
終わったら、USB メモリをアンマウントし 
(<userinput>umount /mnt</userinput>)、
ライトプロテクトスイッチを有効にしてください。

</para>
  </sect2>

  <!-- TODO: doesn't this section belong later? -->
  <sect2 arch="x86">
<!--
  <title>Booting the USB stick</title>
-->
  <title>USB メモリからの起動</title>
<warning><para>

<!--
If your system refuses to boot from the memory stick, the stick may
contain an invalid master boot record (MBR). To fix this, use the
<command>install-mbr</command> command from the package
<classname>mbr</classname>:
-->
システムが USB メモリから起動できなければ、
この USB メモリには無効なマスターブートレコード (MBR) があります。
これを修正するために、<classname>mbr</classname> パッケージの 
<command>install-mbr</command> コマンドを次のように使用してください。

<informalexample><screen>
# install-mbr /dev/<replaceable>sda</replaceable>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
