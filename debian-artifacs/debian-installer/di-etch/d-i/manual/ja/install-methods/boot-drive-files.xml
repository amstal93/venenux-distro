<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 39614 -->

 <sect1 condition="bootable-disk" id="boot-drive-files">
 <title>ハードディスク起動ファイルの準備</title>
<para>

<!--
The installer may be booted using boot files placed on an
existing hard drive partition, either launched from another operating
system or by invoking a boot loader directly from the BIOS.
-->
このインストーラは、
ハードディスクパーティションに配置したファイルから起動できます。
別の OS から起動することもできますし、
BIOS から直接ブートローダを起動することもできます。

</para><para>

<!--
A full, <quote>pure network</quote> installation can be achieved using this
technique. This avoids all hassles of removable media, like finding
and burning CD images or struggling with too numerous and
unreliable floppy disks.
-->
この方法を使えば、
完全な <quote>ピュアネットワーク</quote> インストールを行うことができます。
これは、CD メディアを探して焼いたり、
たくさんの信頼できないフロッピーディスクを相手に苦闘したり、
といったリムーバブルメディアのすべての激論を避けることができます。


</para><para arch="x86">

<!--
The installer cannot boot from files on an NTFS file system.
-->
インストーラは NTFS ファイルシステムからは起動できません。

</para><para arch="powerpc">

<!--
The installer cannot boot from files on an HFS+ file system.  MacOS
System 8.1 and above may use HFS+ file systems; NewWorld PowerMacs all
use HFS+. To determine whether your existing file system is HFS+,
select <userinput>Get Info</userinput> for the volume in question. HFS
file systems appear as <userinput>Mac OS Standard</userinput>, while
HFS+ file systems say <userinput>Mac OS Extended</userinput>. You must
have an HFS partition in order to exchange files between MacOS and
Linux, in particular the installation files you download.
-->
インストーラは、HFS+ ファイルシステムにあるファイルから起動できます。
MacOS System 8.1 以上ならば HFS+ ファイルシステムが使えます。
NewWorld PowerMac はすべて HFS+ を使っています。
現在のファイルシステムが HFS+ かどうかを調べるには、
調べたいボリュームに対して <userinput>情報をみる (Get Info)</userinput>
を実行してください。
HFS ファイルシステムなら <userinput>Mac OS Standard</userinput>、
HFS+ ファイルシステムなら <userinput>Mac OS Extended</userinput> となります。
MacOS と Linux の間でファイル
(特にインストール用にダウンロードしたファイル)
をやりとりするためには、HFS パーティションが必要です。

</para><para arch="powerpc">

<!--
Different programs are used for hard disk installation system booting,
depending on whether the system is a <quote>NewWorld</quote> or an
<quote>OldWorld</quote> model.
-->
ハードディスクからのインストールシステムの起動には、
システムが <quote>NewWorld</quote> モデルか <quote>OldWorld</quote> モデルかによって、
別々のプログラムを使います。

</para>

  <sect2 arch="x86" id="files-lilo">
  <title><command>LILO</command> や
  <command>GRUB</command> を使用したハードディスクからのインストーラの起動</title>
<para>

<!--
This section explains how to add to or even replace an existing linux
installation using either <command>LILO</command> or
<command>GRUB</command>.
-->
本節では、<command>LILO</command> や <command>GRUB</command> 
のどちらかを使用して、linux を追加したり、
既存の linux を交換する方法について説明します。

</para><para>

<!--
At boot time, both bootloaders support loading in memory not
only the kernel, but also a disk image. This RAM disk can be used as
the root file-system by the kernel.
-->
起動時に、カーネルだけでなくディスクイメージをメモリに読み込むのを、
どちらのブートローダでもサポートしています。
この RAM ディスクは カーネルが root ファイルシステムとして使うことができます。

</para><para>

<!--
Copy the following files from the Debian archives to a
convenient location on your hard drive, for instance to
<filename>/boot/newinstall/</filename>.
-->
以下のファイルを、
Debian アーカイブからあなたのハードディスクの適当な場所
 (例 <filename>/boot/newinstall/</filename>) にコピーしてください。


<itemizedlist>
<listitem><para>

<filename>vmlinuz</filename> (カーネルバイナリ)

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename> (RAM ディスクイメージ)

</para></listitem>
</itemizedlist>

</para><para>

<!--
Finally, to configure the bootloader proceed to
<xref linkend="boot-initrd"/>.
-->
最後に、ブートローダを設定するため、
<xref linkend="boot-initrd"/> に進んでください。

</para>
  </sect2>


  <sect2 arch="powerpc" id="files-oldworld">
  <title>OldWorld Mac におけるハードディスクからのインストーラの起動</title>
<para>

<!--
The <filename>boot-floppy-hfs</filename> floppy uses
<application>miBoot</application> to launch Linux installation, but
<application>miBoot</application> cannot easily be used for hard disk
booting. <application>BootX</application>, launched from MacOS,
supports booting from files placed on the hard
disk. <application>BootX</application> can also be used to dual-boot
MacOS and Linux after your Debian installation is complete. For the
Performa 6360, it appears that <command>quik</command> cannot make the
hard disk bootable.  So <application>BootX</application> is required
on that model.
-->
<filename>boot-floppy-hfs</filename> フロッピーは、Linux インストールシステムの
起動に <application>miBoot</application> を使います。
しかし <application>miBoot</application> をハードディスクからの起動に使うのは簡単ではありません。
MacOS から起動する <application>BootX</application> は、
ハードディスクに置かれたファイルからの起動をサポートしています。
<application>BootX</application> は、Debian のインストールが完了した後に、
MacOS と Linux をデュアルブートさせるのにも使えます。
Performa 6360 では、<command>quik</command> はハードディスクを起動できるようにはできません。
そのためこのモデルでは <application>BootX</application> が必要です。

</para><para>

<!--
Download and unstuff the <application>BootX</application>
distribution, available from <ulink url="&url-powerpc-bootx;"></ulink>,
or in the
<filename>dists/woody/main/disks-powerpc/current/powermac</filename>
directory on Debian http/ftp mirrors and official Debian CDs. Use
<application>Stuffit Expander</application> to extract it from its
archive. Within the package, there is an empty folder called
<filename>Linux Kernels</filename>. Download
<filename>linux.bin</filename> and
<filename>ramdisk.image.gz</filename> from the
<filename>disks-powerpc/current/powermac</filename> folder, and place
them in the <filename>Linux Kernels</filename> folder. Then place the
<filename>Linux Kernels</filename> folder in the active System Folder.
-->
<application>BootX</application> の配布パッケージを <ulink url="&url-powerpc-bootx;"></ulink> から、
または Debian http/ftp ミラーや 公式 Debian CD の
<filename>dists/woody/main/disks-powerpc/current/powermac</filename>
ディレクトリから入手して、unstuff してください。
アーカイブから取り出すには <application>Stuffit Expander</application> を使ってください。
パッケージの中には、<filename>Linux Kernels</filename> という名前の空のフォルダがあります。
<filename>linux.bin</filename> と <filename>ramdisk.image.gz</filename> を
<filename>disks-powerpc/current/powermac</filename> フォルダからダウンロードし、
これらを <filename>Linux Kernels</filename> フォルダに置いてください。
その後、<filename>Linux Kernels</filename> フォルダをアクティブな
System Folder に置いてください。

</para>
  </sect2>

  <sect2 arch="powerpc" id="files-newworld">
  <title>NewWorld Mac におけるハードディスクからのインストーラの起動</title>
<para>

<!--
NewWorld PowerMacs support booting from a network or an ISO9660
CD-ROM, as well as loading ELF binaries directly from the hard
disk. These machines will boot Linux directly via
<command>yaboot</command>, which supports loading a kernel and RAMdisk
directly from an ext2 partition, as well as dual-booting with
MacOS. Hard disk booting of the installer is particularly appropriate
for newer machines without floppy drives. <command>BootX</command> is
not supported and must not be used on NewWorld PowerMacs.
-->
NewWorld PowerMac は、ネットワークや
ISO9660 CD-ROM からの起動をサポートしており、
またハードディスクに置かれた ELF バイナリを直接ロードして起動することも可能です。
これらのマシンでは <command>yaboot</command> を使って直接 Linux を起動できます。
<command>yaboot</command> はカーネルと RAM ディスクを直接 ext2 パーティションから
ロードでき、MacOS とのデュアルブートにすることもできます。
ハードディスクからのインストーラの起動は、
特にフロッピードライブのない最近のマシンには向いているでしょう。
<command>BootX</command> は NewWorld PowerMac をサポートしておらず、
使うべきではありません。

</para><para>

<!--
<emphasis>Copy</emphasis> (not move) the following four files which
you downloaded earlier from the Debian archives, onto the root level
of your hard drive (this can be accomplished by
<keycap>option</keycap>-dragging each file to the hard drive icon).
-->
前に Debian アーカイブからダウンロードした次に示すファイルを、
ハードドライブの root 階層に <emphasis>コピー</emphasis> (移動では駄目)
してください (各ファイルを、
<keycap>option</keycap> を押しながらハードドライブアイコンにドラッグします)。

<itemizedlist>
<listitem><para>

<filename>vmlinux</filename>

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename>

</para></listitem>
<listitem><para>

<filename>yaboot</filename>

</para></listitem>
<listitem><para>

<filename>yaboot.conf</filename>

</para></listitem>
</itemizedlist>

</para><para>

<!--
Make a note of the partition number of the MacOS partition where you
place these files. If you have the MacOS <command>pdisk</command>
program, you can use the <command>L</command> command to check for the
partition number. You will need this partition number for the command
you type at the Open Firmware prompt when you boot the installer.
-->
これらのファイルを置いた MacOS パーティションの
パーティション番号を記録しておきましょう。
MacOS の <command>pdisk</command> プログラムがあれば、
<command>L</command> コマンドでパーティション番号を確認できます。
このパーティション番号は、インストーラを起動させたときに
Open Firmware プロンプトから入力するコマンドで必要となります。

</para><para>

To boot the installer, proceed to <xref linkend="boot-newworld"/>.
インストーラを起動させるには、
<xref linkend="boot-newworld"/> に進んでください。

</para>
  </sect2>
 </sect1>
