<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43939 -->

 <sect1 id="needed-info">
<!--
 <title>Information You Will Need</title>
-->
 <title>必要な情報</title>

  <sect2>
<!--
  <title>Documentation</title>
-->
  <title>文書</title>

   <sect3>
<!--
   <title>Installation Manual</title>
-->
   <title>インストールマニュアル</title>

<para condition="for_cd">

<!--
This document you are now reading, in plain ASCII, HTML or PDF format.
-->
現在ご覧になっている文書です。
それぞれプレーンテキスト、HTML、PDF 形式です。

</para>

<itemizedlist condition="for_cd">

&list-install-manual-files;

</itemizedlist>

<para condition="for_wdo">

<!--
The document you are now reading, which is the official version of the
Installation Guide for the &releasename; release of Debian; available
in <ulink url="&url-release-area;/installmanual">various formats and
translations</ulink>.
-->
現在ご覧になっている文書は、
Debian の &releasename; リリース用インストールガイドの正式版です。
これは <ulink url="&url-release-area;/installmanual">様々な形式と様々な言語</ulink> で利用できます。

</para>

<para condition="for_alioth">

<!--
The document you are now reading, which is a development version of the
Installation Guide for the next release of Debian; available in
<ulink url="&url-d-i-alioth-manual;">various formats and
translations</ulink>.
-->
現在ご覧になっている文書は、
Debian の次期リリース用インストールガイドの開発版です。
これは <ulink url="&url-d-i-alioth-manual;">様々な形式と様々な言語</ulink> で利用できます。

</para>

</sect3>


<!--
   <sect3><title>Hardware documentation</title>
-->
   <sect3><title>ハードウェアの文書</title>
<para>

<!--
Often contains useful information on configuring or using your hardware.
-->
しばしば、ハードウェアの設定や使用についての有用な情報を含んでいます。

</para>

 <!-- We need the arch dependence for the whole list to ensure proper xml
      as long as not architectures have a paragraph -->
 <itemizedlist arch="x86;m68k;alpha;sparc;mips;mipsel">
<listitem arch="x86"><para>

<!--
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
-->
<ulink url="&url-hardware-howto;">Linux ハードウェア互換性 HOWTO</ulink>

</para></listitem>

<listitem arch="m68k"><para>

<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>

</para></listitem>

<listitem arch="alpha"><para>

<ulink url="&url-alpha-faq;">Linux/Alpha FAQ</ulink>

</para></listitem>

<listitem arch="sparc"><para>

<!--
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>
-->
<ulink url="&url-sparc-linux-faq;">SPARC プロセッサ用 Linux FAQ</ulink>

</para></listitem>

<listitem arch="mips;mipsel"><para>

<!--
<ulink url="&url-linux-mips;">Linux/Mips website</ulink>
-->
<ulink url="&url-linux-mips;">Linux/Mips ウェブサイト</ulink>

</para></listitem>

</itemizedlist>
   </sect3>


   <sect3 arch="s390">
<!--
   <title>&arch-title; Hardware References</title>
-->
   <title>&arch-title; ハードウェアリファレンス</title>
<para>


<!--
Installation instructions and device drivers (DASD, XPRAM, Console,
      tape, z90 crypto, chandev, network) for Linux on &arch-title; using
      kernel 2.4
-->
&arch-title; に 2.4 カーネルの Linux をインストールするための説明と、
デバイスドライバ (DASD, XPRAM, コンソール, テープ,
z90 crypto, chandev, ネットワーク) に関する情報です。

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://oss.software.ibm.com/developerworks/opensource/linux390/docu/l390dd08.pdf">Device Drivers and Installation Commands</ulink>

</para></listitem>
</itemizedlist>

<para>

<!--
IBM Redbook describing how Linux can be combined with z/VM on
zSeries and &arch-title; hardware.
-->
zSeries と &arch-title; ハードウェアの z/VM への
Linux の組み込み方を説明した IBM の仕様書です。

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg244987.pdf">
Linux for &arch-title;</ulink>

</para></listitem>
</itemizedlist>

<para>

<!--
IBM Redbook describing the Linux distributions available for the
mainframe. It has no chapter about Debian but the basic installation
concepts are the same across all &arch-title; distributions.
-->
メインフレームで使える Linux ディストリビューションについて説明した
IBM の仕様書です。Debian に関する章はありませんが、
基本的なインストールの考え方はすべての 
&arch-title; ディストリビューションで同じです。

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg246264.pdf">
Linux for IBM eServer zSeries and &arch-title;: Distributions</ulink>

</para></listitem>
</itemizedlist>
   </sect3>

  </sect2>

  <sect2 id="fsohi">
<!--
  <title>Finding Sources of Hardware Information</title>
-->
  <title>ハードウェア情報の取得先</title>
<para>

<!--
In many cases, the installer will be able to automatically detect your
hardware. But to be prepared, we do recommend familiarizing
yourself with your hardware before the install.
-->
多くの場合、インストーラはハードウェアを自動的に検出することができます。
しかし、準備としてインストール前にハードウェアに習熟することをお奨めします。

</para><para>

<!--
Hardware information can be gathered from:
-->
ハードウェアの情報は次のようなところから集められます。

</para>

<itemizedlist>
<listitem><para>

<!--
The manuals that come with each piece of hardware.
-->
各ハードウェアに付属してきたマニュアル。

</para></listitem>
<listitem><para>

<!--
The BIOS setup screens of your computer. You can view these screens
when you start your computer by pressing a combination of keys. Check
your manual for the combination. Often, it is the <keycap>Delete</keycap> key.
-->
コンピュータの BIOS 設定画面。この画面を表示させるには、
コンピュータの起動時に何らかのキーの組合せを入力します。
この組合せについてはマニュアルを見てください。
<keycap>Delete</keycap> キーの場合が多いようです。

</para></listitem>
<listitem><para>

<!--
The cases and boxes for each piece of hardware.
-->
各ハードウェアのケースや箱。

</para></listitem>

<listitem arch="x86"><para>

<!--
The System window in the Windows Control Panel.
-->
Windows のコントロールパネルの「システム」ウィンドウ。



</para></listitem>
<listitem><para>

<!--
System commands or tools in another operating system, including file
manager displays. This source is especially useful for information
about RAM and hard drive memory.
-->
他の OS のシステムコマンドやシステムツール、
ファイルマネージャの表示など。こちらからは、
RAM やハードドライブのメモリに関する情報が得られることが多いです。

</para></listitem>
<listitem><para>

<!--
Your system administrator or Internet Service Provider. These
sources can tell you the settings you need to set up your
networking and e-mail.
-->
あなたの部門のシステム管理者や、インターネットサービスプロバイダ。
こちらからは、ネットワークや電子メールに関する設定情報が得られます。

</para></listitem>
</itemizedlist>

<para>

<table>
<!--
<title>Hardware Information Needed for an Install</title>
-->
<title>インストールに必要なハードウェア情報</title>
<tgroup cols="2">
<thead>
<row>
<!--
  <entry>Hardware</entry><entry>Information You Might Need</entry>
-->
  <entry>ハードウェア</entry><entry>必要な情報</entry>
</row>
</thead>

<tbody>
<!--
<row arch="not-s390">
  <entry morerows="5">Hard Drives</entry>
  <entry>How many you have.</entry>
</row>
<row arch="not-s390"><entry>Their order on the system.</entry></row>
<row arch="not-s390;not-m68k">
 "not-m68k;not-s390" would really turn out to be everything...
<row arch="alpha;arm;hppa;x86;ia64;mips;mipsel;powerpc;sparc">
  <entry>Whether IDE or SCSI (most computers are IDE).</entry>
</row>
<row arch="m68k">
  <entry>Whether IDE or SCSI (most m68k computers are SCSI).</entry>
</row>
<row arch="not-s390"><entry>Available free space.</entry></row>
<row arch="not-s390"><entry>Partitions.</entry></row>
<row arch="not-s390">
  <entry>Partitions where other operating systems are installed.</entry>
</row>
-->
<row arch="not-s390">
  <entry morerows="5">ハードディスク</entry>
  <entry>ドライブの台数</entry>
</row>
<row arch="not-s390"><entry>システムでの接続順序</entry></row>
<!-- "not-m68k;not-s390" would really turn out to be everything... -->
<row arch="alpha;arm;hppa;x86;ia64;mips;mipsel;powerpc;sparc">
  <entry>IDE か SCSI か (大抵のコンピュータは IDE)</entry>
</row>
<row arch="m68k">
  <entry>IDE か SCSI か (大抵の m68k コンピュータは SCSI)</entry>
</row>
<row arch="not-s390"><entry>利用できる空き領域</entry></row>
<row arch="not-s390"><entry>パーティション</entry></row>
<row arch="not-s390">
  <entry>他の OS がインストールされているパーティション</entry>
</row>

<!--
<row arch="not-s390">
  <entry morerows="5">Monitor</entry>
  <entry>Model and manufacturer.</entry>
</row>
<row arch="not-s390"><entry>Resolutions supported.</entry></row>
<row arch="not-s390"><entry>Horizontal refresh rate.</entry></row>
<row arch="not-s390"><entry>Vertical refresh rate.</entry></row>
<row arch="not-s390">
  <entry>Color depth (number of colors) supported.</entry>
</row>
<row arch="not-s390"><entry>Screen size.</entry></row>
-->
<row arch="not-s390">
  <entry morerows="5">モニタ</entry>
  <entry>メーカーと型番</entry>
</row>
<row arch="not-s390"><entry>サポートする解像度</entry></row>
<row arch="not-s390"><entry>水平同期周波数</entry></row>
<row arch="not-s390"><entry>垂直同期周波数</entry></row>
<row arch="not-s390">
  <entry>サポートする色深度 (色数)</entry>
</row>
<row arch="not-s390"><entry>スクリーンサイズ</entry></row>

<!--
<row arch="not-s390">
  <entry morerows="3">Mouse</entry>
  <entry>Type: serial, PS/2, or USB.</entry>
</row>
<row arch="not-s390"><entry>Port.</entry></row>
<row arch="not-s390"><entry>Manufacturer.</entry></row>
<row arch="not-s390"><entry>Number of buttons.</entry></row>
-->
<row arch="not-s390">
  <entry morerows="3">マウス</entry>
  <entry>形式: シリアル, PS/2, USB のいずれか</entry>
</row>
<row arch="not-s390"><entry>ポート</entry></row>
<row arch="not-s390"><entry>メーカー</entry></row>
<row arch="not-s390"><entry>ボタンの数</entry></row>

<!--
<row arch="not-s390">
  <entry morerows="1">Network</entry>
  <entry>Model and manufacturer.</entry>
</row>
<row arch="not-s390"><entry>Type of adapter.</entry></row>
-->
<row arch="not-s390">
  <entry morerows="1">ネットワーク</entry>
  <entry>メーカーと型番</entry>
</row>
<row arch="not-s390"><entry>アダプタの形式</entry></row>

<!--
<row arch="not-s390">
  <entry morerows="1">Printer</entry>
  <entry>Model and manufacturer.</entry>
</row>
<row arch="not-s390"><entry>Printing resolutions supported.</entry></row>
-->
<row arch="not-s390">
  <entry morerows="1">プリンタ</entry>
  <entry>メーカーと型番</entry>
</row>
<row arch="not-s390"><entry>サポートする印刷解像度</entry></row>

<!--
<row arch="not-s390">
  <entry morerows="2">Video Card</entry>
  <entry>Model and manufacturer.</entry>
</row>
<row arch="not-s390"><entry>Video RAM available.</entry></row>
<row arch="not-s390">
  <entry>Resolutions and color depths supported (these should be
  checked against your monitor's capabilities).</entry>
</row>
-->
<row arch="not-s390">
  <entry morerows="2">ビデオカード</entry>
  <entry>メーカーと型番</entry>
</row>
<row arch="not-s390"><entry>利用できるビデオ RAM</entry></row>
<row arch="not-s390">
  <entry>サポートする解像度と色深度 (モニタの機能もチェックすること)</entry>
</row>

<!--
<row arch="s390">
  <entry morerows="1">DASD</entry>
  <entry>Device number(s).</entry>
</row>
<row arch="s390"><entry>Available free space.</entry></row>
-->
<row arch="s390">
  <entry morerows="1">DASD</entry>
  <entry>デバイス番号</entry>
</row>
<row arch="s390"><entry>利用できる空き領域</entry></row>

<!--
<row arch="s390">
  <entry morerows="2">Network</entry>
  <entry>Type of adapter.</entry>
</row>
<row arch="s390"><entry>Device numbers.</entry></row>
<row arch="s390"><entry>Relative adapter number for OSA cards.</entry></row>
-->
<row arch="s390">
  <entry morerows="2">ネットワーク</entry>
  <entry>アダプタの形式</entry>
</row>
<row arch="s390"><entry>デバイス番号</entry></row>
<row arch="s390"><entry>OSA カードの相対アダプタ番号</entry></row>

</tbody></tgroup></table>

</para>
  </sect2>

  <sect2>
<!--
  <title>Hardware Compatibility</title>
-->
  <title>ハードウェア互換性</title>

<para>

<!--
Many brand name products work without trouble on Linux. Moreover,
hardware support in Linux is improving daily. However, Linux still does
not run as many different types of hardware as some operating systems.
-->
ブランドメーカーの製品の多くは、問題なく Linux で動作します。
また Linux でサポートするハードウェアも日々進歩しています。
しかし、それでもまだ Linux は、
ある種の OS ほどには多種多様なハードウェアに対応していません。

</para><para arch="x86">

<!--
In particular, Linux usually cannot run hardware that requires a
running version of Windows to work.
-->
特に、Linux は通常、特定のバージョンの Windows を必要とする
ハードウェアを動かすことはできません。

</para><para arch="x86">

<!--
Although some Windows-specific hardware can be made to run on Linux,
doing so usually requires extra effort. In addition, Linux drivers
for Windows-specific hardware are usually specific to one Linux
kernel. Therefore, they can quickly become obsolete.
-->
Windows に特化したハードウェアのうちにも、
Linux で動作できるものはありますが、通常余分な労力が必要となります。
さらに、Windows に特化したハードウェア向けの Linux ドライバは、
普通特定の Linux カーネルに依存したものになります。
従って、すぐに古いものになってしまいます。

</para><para arch="x86">

<!--
So called win-modems are the most common type of this hardware.
However, printers and other equipment may also be Windows-specific.
-->
いわゆる Win モデムは、このようなハードウェアの中でも最も知られたものです。
しかし他にも、プリンタなどに Windows に特化したものがあります。

</para><para>

<!--
You can check hardware compatibility by:
-->
以下のようにハードウェアの互換性をチェックできます。

<itemizedlist>
<listitem><para>

<!--
Checking manufacturers' web sites for new drivers.
-->
新しいドライバが出ていないか、メーカーの web サイトを調べます。

</para></listitem>
<listitem><para>

<!--
Looking at web sites or manuals for information about emulation.
Lesser known brands can sometimes use the drivers or settings for
better-known ones.
-->
エミュレーション情報を web サイトやマニュアルで調べます。
あまり有名でないブランドの場合、
もっと有名なブランドのドライバや設定が使えることもあります。

</para></listitem>
<listitem><para>

<!--
Checking hardware compatibility lists for Linux on web sites
dedicated to your architecture.
-->
Linux のハードウェア互換性情報を、
自分のアーキテクチャ向けの web サイトで調べます。

</para></listitem>
<listitem><para>

<!--
Searching the Internet for other users' experiences.
-->
他に使ったことのあるユーザがいないか、インターネットで調べます。

</para></listitem>
</itemizedlist>

</para>
  </sect2>

  <sect2>
<!--
  <title>Network Settings</title>
-->
  <title>ネットワークの設定</title>

<para>

<!--
If your computer is connected to a network 24 hours a day (i.e., an
Ethernet or equivalent connection &mdash; not a PPP connection), you
should ask your network's system administrator for this information.
-->
インストール対象のコンピュータがネットワークに
24 時間フルに接続されているならば
(つまり、PPP 接続ではなく Ethernet やそれと同等な接続の場合)、
ネットワーク管理者に以下の情報を尋ねておかなければなりません。

<itemizedlist>
<listitem><para>

<!--
Your host name (you may be able to decide this on your own).
-->
ホスト名 (自分で決められるかもしれません)

</para></listitem>
<listitem><para>

<!--
Your domain name.
-->
ドメイン名

</para></listitem>
<listitem><para>

<!--
Your computer's IP address.
-->
コンピュータの IP アドレス

</para></listitem>
<listitem><para>

<!--
The netmask to use with your network.
-->
ネットワークのネットマスク

</para></listitem>
<listitem><para>

<!--
The IP address of the default gateway system you should route to, if
your network <emphasis>has</emphasis> a gateway.
-->
ネットワークにゲートウェイが<emphasis>ある</emphasis>場合は、
経路を向けるデフォルトゲートウェイシステムの IP アドレス

</para></listitem>
<listitem><para>

<!--
The system on your network that you should use as a DNS (Domain Name
Service) server.
-->
DNS (Domain Name Service) サーバとして使用するネットワーク上のホスト

</para></listitem>
</itemizedlist>

</para><para>

<!--
On the other hand, if your administrator tells you that a DHCP server
is available and is recommended, then you don't need this information
because the DHCP server will provide it directly to your computer
during the installation process.
-->
一方、管理者に DHCP サーバが利用でき推奨すると言われたなら、
DHCP サーバがインストールプロセスの間、コンピュータに直接提供するので、
この情報は必要ありません。

</para><para>

<!--
If you use a wireless network, you should also find out:
-->
ワイヤレスネットワークが使用できるなら以下の情報も探さねばなりません。

<itemizedlist>
<listitem><para>

<!--
ESSID of your wireless network.
-->
ワイヤレスネットワークの ESSID

</para></listitem>
<listitem><para>

<!--
WEP security key (if applicable).
-->
(適用できるなら) WEP セキュリティキー

</para></listitem>
</itemizedlist>

</para>
  </sect2>

 </sect1>
