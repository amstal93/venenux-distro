<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45438 -->

<chapter id="boot-new">
<!--
 <title>Booting Into Your New Debian System</title>
-->
 <title>新しい Debian システムを起動させる</title>

<!--
 <sect1 id="base-boot"><title>The Moment of Truth</title>
-->
 <sect1 id="base-boot"><title>決着のとき</title>
<para>

<!--
Your system's first boot on its own power is what electrical engineers
call the <quote>smoke test</quote>.
-->
新しいシステムが初めて自力で起動することを、
電気を扱うエンジニアは<quote>スモークテスト</quote>と呼んでいます。

</para><para arch="x86">

<!--
If you did a default installation, the first thing you should see when you
boot the system is the menu of the <classname>grub</classname> or possibly
the <classname>lilo</classname> bootloader.
The first choices in the menu will be for your new Debian system. If you
had any other operating systems on your computer (like Windows) that were
detected by the installation system, those will be listed lower down in the
menu.
-->
デフォルトのインストールをした場合、システムを起動してまず最初に
<classname>grub</classname> のメニューか、<classname>lilo</classname> ブート
ローダをおそらく目にするはずです。メニューの一番目の選択肢は、
インストールした Debian システムです。インストールシステムが
(Windows のような) 他のオペレーティングシステムをコンピュータ上に見つけた場合、
メニューのもっと下の方にリストアップされているでしょう。

</para><para>

<!--
If the system fails to start up correctly, don't panic. If the installation
was successful, chances are good that there is only a relatively minor
problem that is preventing the system from booting Debian. In most cases
such problems can be fixed without having to repeat the installation.
One available option to fix boot problems is to use the installer's
built-in rescue mode (see <xref linkend="rescue"/>).
-->
たとえシステムが正常に起動しなかったとしても、パニックにならないでください。
インストールが正常に終了したのなら、システムが Debian を起動するのを
妨げる比較的小さな問題だけがある可能性が高いです。ほとんどの場合、
そのような問題はインストールを繰り返すことなしに解決することができます。
ブート時の問題を修正する一つの選択肢は、インストーラ内蔵の
レスキューモード (<xref linkend="rescue"/> をご覧ください) を使用することです。

</para><para>

<!--
If you are new to Debian and Linux, you may need some help from more
experienced users.
<phrase arch="x86">For direct on-line help you can try the IRC channels
#debian or #debian-boot on the OFTC network. Alternatively you can contact
the <ulink url="&url-list-subscribe;">debian-user mailing list</ulink>.</phrase>
<phrase arch="not-x86">For less common architectures like &arch-title;,
your best option is to ask on the
<ulink url="&url-list-subscribe;">debian-&arch-listname; mailing
list</ulink>.</phrase>
You can also file an installation report as described in
<xref linkend="submit-bug"/>. Please make sure that you describe your problem
clearly and include any messages that are displayed and may help others to
diagnose the issue.
-->
もし Debian や Linux に不馴れなら、より経験のあるユーザの手助けが必要かもしれません。 
<phrase arch="x86">直接的なオンラインヘルプとして、OFTC ネットワーク上
の IRC チャネル (#debian あるいは #debian-boot) を試してみてください。  
あるいは、<ulink url="&url-list-subscribe;">debian-user メーリングリスト</ulink>に
連絡してみてください。</phrase>
<phrase arch="not-x86">&arch-title; のようにそれほど一般的でないアーキテクチャでは、
<ulink url="&url-list-subscribe;">debian-&arch-listname; メーリングリスト</ulink>
で尋ねるのが最も良い方法です。</phrase>
<xref linkend="submit-bug"/> にインストールレポートを提出することもできます。
レポートには、問題についてはっきりと説明され、表示されたすべてのメッセージが
含まれており、他の人が問題の原因を突き止める助けになるようにしてください。

</para><para arch="x86">

<!--
If you had any other operating systems on your computer that were not detected
or not detected correctly, please file an installation report.
-->
もしインストールシステムがコンピュータ上にある他のオペレーティングシステムを
見つけられなかったり、誤認識するようなら、インストールレポートを提出してください。

</para>

<!--
  <sect2 arch="m68k"><title>BVME 6000 Booting</title>
-->
  <sect2 arch="m68k"><title>BVME 6000 の起動</title>
<para>

<!--
If you have just performed a diskless install on a BVM or Motorola
VMEbus machine: once the system has loaded the
<command>tftplilo</command> program from the TFTP server, from the
<prompt>LILO Boot:</prompt> prompt enter one of:
-->
BVM マシンや Motorola VMEbus マシンでディスクレスインストール
をした場合、システムは TFTP サーバから
<command>tftplilo</command> プログラムをロードするので、その後に
<prompt>LILO Boot:</prompt> プロンプトから以下のいずれかを入力してください。

<itemizedlist>
<listitem><para>

<!--
<userinput>b6000</userinput> followed by &enterkey;
to boot a BVME 4000/6000

</para></listitem><listitem><para>

<userinput>b162</userinput> followed by &enterkey;
to boot an MVME162

</para></listitem><listitem><para>

<userinput>b167</userinput> followed by &enterkey;
to boot an MVME166/167
-->
<userinput>b6000</userinput> に続けて &enterkey;
を押して BVME 4000/6000 を起動する

</para></listitem><listitem><para>

<userinput>b162</userinput> に続けて &enterkey;
を押して MVME162 を起動する

</para></listitem><listitem><para>

<userinput>b167</userinput> に続けて &enterkey;
を押して MVME166/167 を起動する

</para></listitem>
</itemizedlist>

</para>

   </sect2>

<!--
  <sect2 arch="m68k"><title>Macintosh Booting</title>
-->
  <sect2 arch="m68k"><title>Macintosh の起動</title>

<para>

<!--
Go to the directory containing the installation files and start up the
<command>Penguin</command> booter, holding down the
<keycap>command</keycap> key.  Go to the
<userinput>Settings</userinput> dialogue (<keycombo>
<keycap>command</keycap> <keycap>T</keycap> </keycombo>), and locate
the kernel options line which should look like
<userinput>root=/dev/ram ramdisk_size=15000</userinput> or similar.
-->
インストールファイルが格納されているディレクトリに移動し、
<keycap>command</keycap> キーを押しながら
<command>Penguin</command> ブータを実行してください。
<userinput>Settings</userinput> ダイアログに移動し (<keycombo>
<keycap>command</keycap> <keycap>T</keycap> </keycombo>)、
<userinput>root=/dev/ram ramdisk_size=15000</userinput> のような
カーネルオプションを入力してください。

</para><para>

<!--
You need to change the entry to
<userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>.
Replace the <replaceable>yyyy</replaceable> with the Linux name of the
partition onto which you installed the system (e.g.
<filename>/dev/sda1</filename>); you wrote this down earlier.  For users
with tiny screens, adding <userinput>fbcon=font:VGA8x8</userinput> (or
<userinput>video=font:VGA8x8</userinput> on pre-2.6 kernels) may help
readability. You can change this at any time.
-->
<userinput>root=/dev/<replaceable>yyyy</replaceable></userinput> の
<replaceable>yyyy</replaceable> 部分を変更しなければなりません。
ここには、システムをインストールしたパーティションの (Linux から見た) 名前を入れます
(例: <filename>/dev/sda1</filename>)。これは以前にも入力したはずです。
小さな画面をお使いの方には、<userinput>fbcon=font:VGA8x8</userinput> 
(2.6 より前のカーネルでは <userinput>video=font:VGA8x8</userinput>) 
を追加すると見やすくなるでしょう。これはいつでも変更できます。

</para><para>

<!--
If you don't want to start GNU/Linux immediately each time you start,
uncheck the <userinput>Auto Boot</userinput> option. Save your
settings in the <filename>Prefs</filename> file using the
<userinput>Save Settings As Default</userinput> option.
-->
起動のたびに GNU/Linux をすぐにスタートさせたくない場合は、
<userinput>Auto Boot</userinput> オプションを無効にしてください。
<userinput>Save Settings As Default</userinput> オプションを用いて
設定を <filename>Prefs</filename> ファイルにセーブしてください。

</para><para>

<!--
Now select <userinput>Boot Now</userinput> (<keycombo>
<keycap>command</keycap> <keycap>B</keycap> </keycombo>) to start your
freshly installed GNU/Linux instead of the RAMdisk installer system.
-->
では <userinput>Boot Now</userinput> (<keycombo>
<keycap>command</keycap> <keycap>B</keycap> </keycombo>) を選択し、
RAM ディスクのインストーラシステムではなく、
インストールしたての GNU/Linux を起動させましょう。

</para><para>

<!--
Debian should boot, and you should see the same messages as
when you first booted the installation system, followed by some new
messages.
-->
今度はうまく Debian が起動するはずです。
画面には初めてインストーラを起動した時と同じメッセージが表示
され、今回はその後さらに新しいメッセージが続きます。

</para>
   </sect2>


  <sect2 arch="powerpc"><title>OldWorld PowerMacs</title>
<para>

<!--
If the machine fails to boot after completing the installation, and
stops with a <prompt>boot:</prompt> prompt, try typing
<userinput>Linux</userinput> followed by &enterkey;. (The default boot
configuration in <filename>quik.conf</filename> is labeled Linux). The
labels defined in <filename>quik.conf</filename> will be displayed if
you press the <keycap>Tab</keycap> key at the <prompt>boot:</prompt>
prompt. You can also try booting back into the installer, and editing
the <filename>/target/etc/quik.conf</filename> placed there by the
<guimenuitem>Install Quik on a Hard Disk</guimenuitem> step. Clues
for dealing with <command>quik</command> are available at
<ulink url="&url-powerpc-quik-faq;"></ulink>.
-->
インストール終了後に起動が失敗し、<prompt>boot:</prompt>というプロンプト
のところで停止してしまったら、<userinput>Linux</userinput> と
入力して &enterkey; を押してみてください (<filename>quik.conf</filename>
のデフォルトの起動設定には「Linux」というラベルがついているのです)。
<filename>quik.conf</filename> 中で定義されているラベルは、
<prompt>boot:</prompt>というプロンプトが出た時に <keycap>Tab</keycap>
キーを押すと表示されます。また、もう 1 度インストーラを起動し直して、
<guimenuitem>ハードディスクへの quik のインストール</guimenuitem> の段階で置いた
<filename>/target/etc/quik.conf</filename> を編集してみるのもよいでしょう。
<command>quik</command> を扱う上での情報は、
<ulink url="&url-powerpc-quik-faq;"></ulink> から得られます。

</para><para>

<!--
To boot back into MacOS without resetting the nvram, type
<userinput>bye</userinput> at the OpenFirmware prompt (assuming MacOS
has not been removed from the machine). To obtain an OpenFirmware
prompt, hold down the <keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>o</keycap> <keycap>f</keycap>
</keycombo> keys while cold booting the machine. If you need to reset
the OpenFirmware nvram changes to the MacOS default in order to boot
back to MacOS, hold down the <keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap>
</keycombo> keys while cold booting the machine.
-->
nvram をリセットせずに MacOS を起動するには、OpenFirmware プロンプトで
<userinput>bye</userinput> と入力してください (MacOS がマシンから
削除されていないことが前提です)。OpenFirmware プロンプトに入るには、
マシンがコールドブートするまで <keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>o</keycap> <keycap>f</keycap>
</keycombo> のキーを押し続けてください。OpenFirmware nvram を
リセットして MacOS をデフォルトにし、MacOS に戻るようにするには、
マシンがコールドブートしている間、<keycombo> <keycap>command</keycap>
<keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap>
</keycombo> キーを押し続けてください。

</para><para>

<!--
If you use <command>BootX</command> to boot into the installed system,
just select your desired kernel in the <filename>Linux
Kernels</filename> folder, un-choose the ramdisk option, and add
a root device corresponding to your installation;
e.g. <userinput>/dev/hda8</userinput>.
-->
インストールしたシステムをブートするのに <command>BootX</command>
を使うなら、<filename>Linux Kernels</filename> フォルダで
希望するカーネルを選び、ramdisk オプションを非選択にして、
インストールに対応したルートデバイス
(例えば <userinput>/dev/hda8</userinput>) を加えるだけです。

</para>
   </sect2>


  <sect2 arch="powerpc"><title>NewWorld PowerMacs</title>
<para>

<!--
On G4 machines and iBooks, you can hold down the
<keycap>option</keycap> key and get a graphical screen with a button
for each bootable OS, &debian; will be a button with a small penguin
icon.
-->
G4 マシンと iBook では、<keycap>option</keycap> キーを押すと、
起動可能な OS のボタンが並んだグラフィカルな画面になります。&debian; 
は小さなペンギンのアイコンで示されます。

</para><para>

<!--
If you kept MacOS and at some point it changes the OpenFirmware
<envar>boot-device</envar> variable you should reset OpenFirmware to
its default configuration.  To do this hold down the <keycombo>
<keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap>
<keycap>r</keycap> </keycombo> keys while cold booting the machine.
-->
MacOS を残しており、どこかの時点で OpenFirmware の
<envar>ブートデバイス</envar>変数を変更した場合には、OpenFirmware を
デフォルトの設定に戻す必要があります。これには、マシンがコールドブートするまで
<keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap>
<keycap>r</keycap> </keycombo> キーを押し続けます。

</para><para>

<!--
The labels defined in <filename>yaboot.conf</filename> will be
displayed if you press the <keycap>Tab</keycap> key at the
<prompt>boot:</prompt> prompt.
-->
<prompt>boot:</prompt> プロンプトが表示されたときに
<keycap>Tab</keycap> キーを押すと、<filename>yaboot.conf</filename>
で定義されているラベルが表示されます。

</para><para>

<!--
Resetting OpenFirmware on G3 or G4 hardware will cause it to boot
&debian; by default (if you correctly partitioned and placed the
Apple_Bootstrap partition first).  If you have &debian; on a SCSI disk
and MacOS on an IDE disk this may not work and you will have to enter
OpenFirmware and set the <envar>boot-device</envar> variable,
<command>ybin</command> normally does this automatically.
-->
G3 や G4 において OpenFirmware をリセットすると、
&debian; がデフォルトで起動するようになります
(パーティションが正しく作成され、Apple_Bootstrap
パーティションが最初に置かれている場合)。
&debian; が SCSI ディスクに置かれていて
MacOS が IDE ディスクに置かれている場合には、
うまく働かないかもしれません。その場合は
OpenFirmware に入って <envar>ブートデバイス</envar> 変数を設定する
必要がありますが、通常は <command>ybin</command> が
自動的にこの作業をします。

</para><para>

<!--
After you boot &debian; for the first time you can add any additional
options you desire (such as dual boot options) to
<filename>/etc/yaboot.conf</filename> and run <command>ybin</command>
to update your boot partition with the changed configuration.  Please
read the <ulink url="&url-powerpc-yaboot-faq;">yaboot HOWTO</ulink>
for more information.
-->
いちど &debian; が起動できたら、あなたが望む
オプション (デュアルブートなど) を
<filename>/etc/yaboot.conf</filename> に追加して
<command>ybin</command> を実行すれば、その変更をブートパーティションに
反映させることができます。詳しくは、
<ulink url="&url-powerpc-yaboot-faq;">yaboot HOWTO</ulink>
を参照してください。

</para>
   </sect2>
 </sect1>

&mount-encrypted.xml;

 <sect1 id="login">
<!--
 <title>Log In</title>
-->
 <title>ログイン</title>

<para>

<!--
Once your system boots, you'll be presented with the login
prompt.  Log in using the personal login and password you
selected during the installation process. Your system is now ready for use.
-->
システムが起動するとすぐに、ログインプロンプトが現れます。
インストールプロセス中にあなたが指定した一般ユーザのアカウント名と
パスワードを入力して、ログインしてください。これで、システムは準備完了です。

</para><para>

<!--
If you are a new user, you may want to explore the documentation which
is already installed on your system as you start to use it. There are
currently several documentation systems, work is proceeding on
integrating the different types of documentation. Here are a few
starting points.
-->
初心者のユーザは、システムを使い始めながら、
すでにインストールされている文書を読んでみると良いでしょう。
現在はまだ文書システムが数種類存在しており、別々の形式の文書を
統合するための作業が進められているところです。以下に
出発点をいくつか示します。

</para><para>

<!--
Documentation accompanying programs you have installed can be found in
<filename>/usr/share/doc/</filename>, under a subdirectory named after the
program (or, more precise, the Debian package that contains the program).
However, more extensive documentation is often packaged separately in
special documentation packages that are mostly not installed by default.
For example, documentation about the package management tool
<command>apt</command> can be found in the packages
<classname>apt-doc</classname> or <classname>apt-howto</classname>.
-->
インストールしたプログラムに付属する文書は、
<filename>/usr/share/doc/</filename> 以下のそのプログラム (より正確には、
そのプログラムを含む Debian パッケージ) にちなんで命名されたサブディレクトリ
の下で見ることができます。しかし多くの場合、より豊富な文書が、独立した
文書パッケージ (ほとんどの場合、デフォルトではインストールされません) として
特別に用意されます。例えば、パッケージ管理ツール <command>apt</command> に
関する文書は、<classname>apt-doc</classname> や 
<classname>apt-howto</classname> パッケージで見ることができます。

</para><para>

<!--
In addition, there are some special folders within the
<filename>/usr/share/doc/</filename> hierarchy. Linux HOWTOs are
installed in <emphasis>.gz</emphasis> (compressed) format, in
<filename>/usr/share/doc/HOWTO/en-txt/</filename>. After installing
<classname>dhelp</classname>, you will find a browse-able index of
documentation in <filename>/usr/share/doc/HTML/index.html</filename>.
-->
また、<filename>/usr/share/doc/</filename> 階層構造の中には、
いくつか特別なフォルダがあります。Linux HOWTO は、
<filename>/usr/share/doc/HOWTO/en-txt/</filename> の中に、
<emphasis>.gz</emphasis> (圧縮) フォーマットで収められています。
<classname>dhelp</classname> をインストールした後に、
<filename>/usr/share/doc/HTML/index.html</filename> に拾い読みできる
文書のインデックスを見つけるでしょう。

</para><para>

<!--
One easy way to view these documents using a text based browser is to
enter the following commands:
-->
テキストベースのブラウザを使用して以下のコマンドを入力することで、
それらの文書を簡単に見ることができます:

<informalexample><screen>
$ cd /usr/share/doc/
$ w3c .
</screen></informalexample>

<!--
The dot after the <command>w3c</command> command tells it to show the
contents of the current directory.
-->
<command>w3c</command> コマンドの後のドットは、カレントディレクトリの
内容を表示させるためのものです。

</para><para>

<!--
If you have a graphical desktop environment installed, you can also use
its web browser. Start the web browser from the application menu and
enter <userinput>/usr/share/doc/</userinput> in the address bar.
-->
グラフィカルデスクトップ環境をインストールした場合には、Web ブラウザも
利用できます。アプリケーションメニューから Web ブラウザを起動し、
アドレスバーに <userinput>/usr/share/doc/</userinput> と入力してください。

</para><para>

<!--
You can also type <userinput>info
<replaceable>command</replaceable></userinput> or <userinput>man
<replaceable>command</replaceable></userinput> to see documentation on
most commands available at the command prompt. Typing
<userinput>help</userinput> will display help on shell commands. And
typing a command followed by <userinput>-\-help</userinput> will
usually display a short summary of the command's usage. If a command's
results scroll past the top of the screen, type
<userinput>|&nbsp;more</userinput> after the command to cause the results
to pause before scrolling past the top of the screen. To see a list of all
commands available which begin with a certain letter, type the letter
and then two tabs.
-->
また、コマンドプロンプトから使えるほとんどのコマンドに対し、
<userinput>info <replaceable>コマンド</replaceable></userinput> または
<userinput>man <replaceable>コマンド</replaceable></userinput> によって
その文書が参照できます。
<userinput>help</userinput> と入力すると、シェルコマンドのヘルプが
読めます。コマンドを <userinput>--help</userinput> つきで入力すると、
たいていそのコマンドの簡単な使い方が表示されます。
その結果が画面からスクロールして消えてしまう場合には、
コマンドのあとに <userinput>|&nbsp;more</userinput> を追加すると、
画面ごとに一時停止してくれます。ある文字で始まるコマンドの
一覧を知りたいときは、その文字を入力してからタブを 2 回押します。

</para>

 </sect1>
</chapter>
