<!-- retain these comments for translator revision tracking -->
<!-- original version: 44002 -->


  <sect2 arch="x86">
  <!-- <title>Booting from a CD-ROM</title> -->
  <title>Avvio da CD-ROM</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!--
CD #1 of official Debian CD-ROM sets for &arch-title; will present a
<prompt>boot:</prompt> prompt on most hardware. Press
<keycap>F4</keycap> to see the list of kernel options available
from which to boot. Just type your chosen flavor name (idepci,
vanilla, compact, bf24) at the <prompt>boot:</prompt> prompt
followed by &enterkey;.

Il primo CD della serie ufficiale di CD-ROM per &arch-title; presenta
sulla maggior parte dell'hardware il prompt <prompt>boot:</prompt>.
Premere <keycap>F4</keycap> per vedere l'elenco delle opzioni di avvio
disponibili, al prompt <prompt>boot:</prompt> inserire il nome della
versione (idepci, vanilla, compact o bf24) e poi premere &enterkey;.

</para><para>

If your hardware doesn't support booting of multiple images, put one
of the other CDs in the drive. It appears that most SCSI CD-ROM drives
do not support <command>isolinux</command> multiple image booting, so users
with SCSI CD-ROMs should try either CD2 (vanilla) or CD3 (compact),
or CD5 (bf2.4).

Se il proprio hardware non supporta l'avvio di più immagini inserire uno
degli altri CD nel lettore. Pare che la maggior parte dei lettori CD-ROM
SCSI non supportino l'avvio delle immagini multiple
<command>isolinux</command>, quindi gli utenti con lettori SCSI devono
usare direttamente uno fra il CD2 (vanilla), il CD3 (compact) o il CD5
(bf2.4).

</para><para>

CD's 2 through 5 will each boot a
different ``flavor'' depending on which CD-ROM is
inserted. See <xref linkend="kernel-choice"/> for a discussion of the
different flavors.  Here's how the flavors are laid out on the
different CD-ROMs:

I CD da 2 a 5 fanno partire versioni diverse a seconda del CD-ROM
inserito. Si consulti <xref linkend="kernel-choice"/> per la descrizione
delle differenze fra le versioni, di seguito è descritto come sono
distribuite le varie versioni sui CD-ROM.

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

Allows a selection of kernel images to boot from (the idepci flavor is
the default if no selection is made).

Permette la scelta delle immagini del kernel da avviare (la versione
predefinita è <quote>idepci</quote>).

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Boots the <quote>vanilla</quote> flavor.

Avvia la versione <quote>vanilla</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Boots the <quote>compact</quote> flavor.

Avvia la versione <quote>compact</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Boots the <quote>idepci</quote> flavor.

Avvia la versione <quote>idepci</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Boots the <quote>bf2.4</quote> flavor.

Avvia la versione <quote>bf2.4</quote>.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

  </sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
     until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Booting from a DOS partition</title>
  <title>Avvio da una partizione DOS</title>

&boot-installer-intro-hd.xml;

<para>

Boot into DOS (not Windows) without any drivers being loaded.  To do
this, you have to press <keycap>F8</keycap> at exactly the right
moment (and optionally select the <quote>safe mode command prompt only</quote>
option).  Enter the subdirectory for the flavor you chose, e.g.,

Avviare in modalità DOS (non Windows) e senza caricare nessun driver.
Per ferlo si deve premere <keycap>F8</keycap> al momento guisto (e poi
scegliere <quote>safe mode command prompt only</quote>). Inserire la
sottodirectory scelta, per esempio

<informalexample><screen>
cd c:\install
</screen></informalexample>.

Next, execute <command>install.bat</command>.
The kernel will load and launch the installer system.

Poi eseguire <command>install.bat</command> per caricare il kernel e
avviare il sistema d'installazione.

</para><para>

Please note, there is currently a loadlin problem (#142421) which
precludes <filename>install.bat</filename> from being used with the
bf2.4 flavor. The symptom of the problem is an
<computeroutput>invalid compressed format</computeroutput> error.

Notare che attualmente c'è un problema (#147421) con loadlin che
impedisce di usare <filename>install.bat</filename> con la versione
bf2.4. Il sintomo di questo problema è l'errore <computeroutput>invalid
compressed format</computeroutput>.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <!-- <title>Booting from Linux Using <command>LILO</command> or
  <command>GRUB</command></title> -->
  <title>Avvio di Linux usando <command>LILO</command> o
  <command>GRUB</command></title>
<para>

<!--
To boot the installer from hard disk, you must first download
and place the needed files as described in <xref linkend="boot-drive-files"/>.
-->

Per avviare l'installatore dall'hard disk si devono prima
scaricare e posizionare i file necessari come descritto in
<xref linkend="boot-drive-files"/>.

</para><para>

<!--
If you intend to use the hard drive only for booting and then
download everything over the network, you should download the
<filename>netboot/debian-installer/&architecture;/initrd.gz</filename> file and its
corresponding kernel
<filename>netboot/debian-installer/&architecture;/linux</filename>. This will allow you
to repartition the hard disk from which you boot the installer, although you
should do so with care.
-->

Se si vuole usare il disco fisso solo per l'avvio e poi scaricare
tutto il resto dalla rete si dovrebbe scaricare il file
<filename>netboot/debian-installer/&architecture;/initrd.gz</filename> e il
relativo kernel <filename>netboot/debian-installer/&architecture;/linux</filename>.
Questo permette di ripartizionare il disco fisso da
cui è stato avviato l'installatore, anche se questa operazione deve
essere fatta con molta attenzione.

</para><para>

<!--
Alternatively, if you intend to keep an existing partition on the hard
drive unchanged during the install, you can download the
<filename>hd-media/initrd.gz</filename> file and its kernel, as well as
copy a CD iso to the drive (make sure the file is named ending in
<literal>.iso</literal>). The installer can then boot from the drive
and install from the CD image, without needing the network.
-->

In alternativa, se si vuole lasciare intoccata una partizione già esistente
sul disco, si può scaricare il file <filename>hd-media/initrd.gz</filename>
e il relativo kernel e copiare un'immagine iso di un CD sul disco
(assicurandosi che il nome termini in <literal>.iso</literal>).
In questo modo l'installatore si avvia dal disco e l'installazione avviene
usando l'immagine del CD senza la necessità della rete.

</para><para>

<!--
For <command>LILO</command>, you will need to configure two
essential things in <filename>/etc/lilo.conf</filename>:
-->

Per usare <command>LILO</command> si devono essenzialmente configurare
due cose in <filename>/etc/lilo.conf</filename>:

<itemizedlist>
<listitem><para>

<!--
to load the <filename>initrd.gz</filename> installer at boot time;
-->

far caricare all'avvio <filename>initrd.gz</filename> dell'installatore;

</para></listitem>
<listitem><para>

<!--
have the <filename>vmlinuz</filename> kernel use a RAM disk as
its root partition.
-->

fare in modo che il kernel <filename>vmlinuz</filename> usi un RAM disk
come partizione di root.

</para></listitem>
</itemizedlist>

<!--
Here is a <filename>/etc/lilo.conf</filename> example:
-->

Questo è un esempio di <filename>/etc/lilo.conf</filename>:

</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

<!--
For more details, refer to the
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> and
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry> man pages. Now run
<userinput>lilo</userinput> and reboot.
-->

Per maggiori dettagli si consultino le pagine di manuale di
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> e
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>. Lanciare
<userinput>lilo</userinput> e riavviare.

</para><para>

<!--
The procedure for <command>GRUB</command> is quite similar. Locate your
<filename>menu.lst</filename> in the <filename>/boot/grub/</filename>
directory (sometimes in the <filename>/boot/boot/grub/</filename>),
add the following lines:
-->

La procedura per usare <command>GRUB</command> è abbastanza simile.
Trovare il file <filename>menu.lst</filename> nella directory
<filename>/boot/grub/</filename> (oppure in
<filename>/boot/boot/grub/</filename>), aggiungere queste righe:

<informalexample><screen>
title  New Install
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

<!--
and reboot.
-->

e riavviare.

</para><para>

<!--
Note that the value of the <userinput>ramdisk_size</userinput> may need to be
adjusted for the size of the initrd image.
From here on, there should be no difference between <command>GRUB</command>
or <command>LILO</command>.
-->

Notare che potrebbe essere necessario modificare il valore di
<userinput>ramdisk_size</userinput> alla dimensione dell'immagine initrd.
D'ora in poi non ci sono differenze fra <command>GRUB</command> e
<command>LILO</command>.

</para>
  </sect2>

  <sect2 arch="x86" condition="bootable-usb" id="usb-boot">
  <!-- <title>Booting from USB Memory Stick</title> -->
  <title>Avvio da chiavetta USB</title>
<para>

<!--
Let's assume you have prepared everything from <xref
linkend="boot-dev-select"/> and <xref linkend="boot-usb-files"/>.  Now
just plug your USB stick into some free USB connector and reboot the
computer.  The system should boot up, and you should be presented with
the <prompt>boot:</prompt> prompt.  Here you can enter optional boot
arguments, or just hit &enterkey;.
-->

Una volta che tutto è stato preparato seguendo
<xref linkend="boot-dev-select"/> e <xref linkend="boot-usb-files"/> si
deve inserire la chiavetta USB in uno qualsiasi dei connettori USB liberi e
riavviare la macchina. Il sistema si dovrebbe avviare e presentare il
prompt <prompt>boot:</prompt>. Adesso si possono inserire dei parametri
opzionali oppure premere &enterkey;.

</para>
  </sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <!-- <title>Booting from Floppies</title> -->
  <title>Avvio da dischetti</title>
<para>

<!--
You will have already downloaded the floppy images you needed and
created floppies from the images in <xref linkend="create-floppy"/>.
-->
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see
<xref linkend="rescue-replace-kernel"/>. -->

Dopo aver scaricato le immagini e creato i dischetti dalle immagini come
descritto in <xref linkend="create-floppy"/>.
<!-- FIXME Se serve si può anche modificare il dischetto d'avvio, si consulti
<xref linkend="rescue-replace-kernel"/>. -->

</para><para>

<!--
To boot from the installer boot floppy, place it in the primary floppy
drive, shut down the system as you normally would, then turn it back
on.
-->

Per fare l'avvio usando i dischetti inserire il dischetto nel lettore
principale, spegnere normalmente il sistema e poi riaccenderlo.

</para><para>

<!--
For installing from an LS-120 drive (ATAPI version) with a set of
floppies, you need to specify the virtual location for the floppy
device.  This is done with the <emphasis>root=</emphasis> boot
argument, giving the device that the ide-floppy driver maps the device
to. For example, if your LS-120 drive is connected as the first IDE
device (master) on the second cable, you enter
<userinput>install root=/dev/hdc</userinput> at the boot prompt.
-->

Per installare da una serie di dischetti per il lettore LS-120 (versione
ATAPI) è necessario specificare qual è il device del lettore. Questo si
può fare con il parametro d'avvio <emphasis>root=</emphasis> passando il
nome del device ide che mappa il lettore. Per esempio se il proprio LS-120
è collegato come dispositivo master sul secondo canale IDE si deve inserire
al prompt boot <userinput>install root=/dev/hdc</userinput>.

</para><para>

<!--
Note that on some machines, <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Delete</keycap></keycombo> does not
properly reset the machine, so a <quote>hard</quote> reboot is recommended.  If
you are installing from an existing operating system (e.g., from a DOS
box) you don't have a choice. Otherwise, please do a hard reboot when
booting.
-->

Si noti che su alcune macchine non è possibile usare
<keycombo><keycap>Ctrl</keycap> <keycap>Alt</keycap>
<keycap>Canc</keycap></keycombo> per riavviare la macchina, quindi si
raccomanda l'uso del tasto reset. Nel caso si faccia l'installazione da
un altro sistema operativo (per esempio da un sistema DOS) non si hanno
scelte e comunque si consiglia sempre un riavvio hardware.

</para><para>

<!--
The floppy disk will be accessed, and you should then see a screen
that introduces the boot floppy and ends with the <prompt>boot:</prompt>
prompt.
-->

A questo punto il dischetto verrà letto e si vedrà una schermata che
contiene un'introduzione al dischetto di avvio e finisce con il prompt
<prompt>boot:</prompt>.

</para><para>

<!--
Once you press &enterkey;, you should see the message
<computeroutput>Loading...</computeroutput>, followed by
<computeroutput>Uncompressing Linux...</computeroutput>, and
then a screenfull or so of information about the hardware in your
system.  More information on this phase of the boot process can be
found below in <xref linkend="kernel-msgs"/>.
-->

Dopo aver premuto &enterkey; dovrebbe apparire il messaggio
<computeroutput>Loading...</computeroutput>, seguito da
<computeroutput>Uncompressing Linux...</computeroutput> e poi da una
lunga fila di informazioni sull'hardware presente nel proprio sistema.
Ulteriori informazioni sul processo d'avvio possono essere trovate
in <xref linkend="kernel-msgs"/>.

</para><para>

<!--
After booting from the boot floppy, the root floppy is
requested. Insert the root floppy and press &enterkey;, and the
contents are loaded into memory. The installer program
<command>debian-installer</command> is automatically launched.
-->

Dopo aver fatto l'avvio con il dischetto di boot viene richiesto l'inserimento
del dischetto root, inserirlo e poi premere &enterkey;. Il programma di
installazione <command>debian-installer</command> parte automaticamente.

</para>
  </sect2>

  <sect2 arch="x86" id="boot-tftp">
  <!-- <title>Booting with TFTP</title> -->
  <title>Avvio con TFTP</title>

&boot-installer-intro-net.xml;

<para>

<!--
There are various ways to do a TFTP boot on i386.
-->

Esistono diversi modi per fare l'avvio da TFTP su i386.

</para>

   <sect3>
   <!-- <title>NIC or Motherboard that support PXE</title> -->
   <title>Schede di rete (NIC) o schede madri con supporto PXE</title>
<para>

<!--
It could be that your Network Interface Card or Motherboard provides
PXE boot functionality.
This is a <trademark class="trade">Intel</trademark> re-implementation
of TFTP boot. If so, you may be able to configure your BIOS to boot from the
network.
-->

È possibile che la scheda di rete o la scheda madre supporti la funzione
d'avvio PXE (PXE è una reimplementazione fatta da
<trademark class="trade">Intel</trademark> di TFTP), in questo caso si
dovrebbe essere in grado di configurare il BIOS in modo da fare l'avvio
dalla rete.

</para>
   </sect3>

   <sect3>
   <!-- <title>NIC with Network BootROM</title> -->
   <title>Schede di rete con Network BootROM</title>
<para>

<!--
It could be that your Network Interface Card provides
TFTP boot functionality.
-->

È possibile che la propria scheda di rete supporti la funzione
d'avvio TFTP.

</para><para condition="FIXME">

<!--
Let us (<email>&email-debian-boot-list;</email>) know how did you manage it.
Please refer to this document.
-->

In questo caso fateci sapere (<email>&email-debian-boot-list;</email>) come
lo si è gestito facendo riferimento a questo manuale.

</para>
   </sect3>

   <sect3>
   <!-- <title>Etherboot</title> -->
   <title>Etherboot</title>
<para>

<!--
The <ulink url="http://www.etherboot.org">etherboot project</ulink>
provides bootdiskettes and even bootroms that do a TFTPboot.
-->

Il <ulink url="http://www.etherboot.org">progetto etherboot</ulink>
fornisce dischetti d'avvio e bootrom per l'avvio TFTP.

</para>
   </sect3>
  </sect2>

  <sect2 arch="x86">
  <!-- <title>The Boot Prompt</title> -->
  <title>Il prompt boot</title>
<para>

<!--
When the installer boots, you should be presented with a friendly graphical
screen showing the Debian logo and the boot prompt:
-->

Quando l'installatore parte viene mostrata una schermata con il logo di
Debian e il prompt di avvio:

<informalexample><screen>
Press F1 for help, or ENTER to boot:
</screen></informalexample>

<!--
At the boot prompt
you can either just press &enterkey; to boot the installer with
default options or enter a specific boot method and, optionally, boot
parameters.
-->

si può premere &enterkey; per avviare l'installatore con le opzioni
predefinite oppure inserire un particolare metodo di avvio e,
opzionalmente, i parametri di avvio.

</para><para>

<!--
Information on available boot methods and on boot parameters which might
be useful can be found by pressing <keycap>F2</keycap> through
<keycap>F8</keycap>. If you add any parameters to
the boot command line, be sure to type the boot method (the default is
<userinput>install</userinput>) and a space before the first parameter (e.g.,
<userinput>install fb=false</userinput>).
-->

Le informazioni sui metodi d'avvio disponibili e sui parametri si
possono ricavare premendo i tasti da <keycap>F2</keycap> a
<keycap>F8</keycap>. Quando si
inseriscono dei parametri nella riga di comando assicurarsi di
lasciare uno spazio fra il metodo di avvio (quello predefinito è
<userinput>install</userinput>) e il primo parametro (per esempio
<userinput>install fb=false</userinput>).

<note><para>

<!--
If you are installing the system via a remote management device that
provides a text interface to the VGA console, you may not be able to
see the initial graphical splash screen upon booting the installer;
you may even not see the boot prompt. Examples of these devices include
the text console of Compaq's <quote>integrated Lights Out</quote> (iLO)
and HP's <quote>Integrated Remote Assistant</quote> (IRA).
You can blindly press F1<footnote>
-->

Se si installa usando un sistema di gestione remota che fornisce
un'interfaccia testuale su una console VGA potrebbe essere impossibile vedere
lo splash screen iniziale dell'installatore; anche il prompt boot potrebbe
essere invisibile. Degli esempi di questo tipo di dispositivi sono la
console testuale <quote>integrated Lights Out</quote> (iLO) della Compaq
e <quote>Integrated Remote Assistant</quote> (IRA) della HP. In questo
caso premere alla cieca F1<footnote>

<para>

<!--
In some cases these devices will require special escape sequences to
enact this keypress, for example the IRA uses <keycombo> <keycap>Ctrl</keycap>
<keycap>F</keycap> </keycombo>,&nbsp;<keycap>1</keycap>.
-->

In alcuni casi questi dispositivi richiedono delle sequenze di escape
speciali per emulare la pressione di questo tasto, per esempio le IRA
usa <keycombo> <keycap>Ctrl</keycap> <keycap>F</keycap>
</keycombo>,&nbsp;<keycap>1</keycap>.

</para>

<!--
</footnote> to bypass this screen and view the help text. Once you are
past the splash screen and at the help text your keystrokes will be echoed
at the prompt as expected. To prevent the installer from using the
framebuffer for the rest of the installation, you will also want to add
<userinput>fb=false</userinput> to the boot prompt,
as described in the help text.
-->

</footnote> per saltare questa schermata e consultare l'aiuto testuale.
Una volta saltato lo splash screen e passati in modalità testuale si
dovrebbero vedere i tasti premuti sullo schermo. Per evitare che
l'installatore usi il framebuffer per il resto dell'installazione si deve
aggiungere <userinput>fb=false</userinput> al prompt boot.

</para></note>
</para>
  </sect2>
