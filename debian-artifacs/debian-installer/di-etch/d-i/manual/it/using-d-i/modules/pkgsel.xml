<!-- retain these comments for translator revision tracking -->
<!-- original version: 43734 -->


   <sect3 id="pkgsel">
   <!-- <title>Selecting and Installing Software</title> -->
   <title>Selezione e installazione dei pacchetti</title>
<para>

<!--
During the installation process, you are given the opportunity to select
additional software to install. Rather than picking individual software
packages from the &num-of-distrib-pkgs; available packages, this stage of
the installation process focuses on selecting and installing predefined
collections of software to quickly set up your computer to perform various
tasks.
-->

Durante il processo d'installazione viene data l'opportunità di scegliere
e installare altri programmi. Anziché scegliere i singoli pacchetti fra
gli oltre &num-of-distrib-pkgs; disponibili, in questa fase del processo
d'installazione si possono installare solo delle raccolte predefinite di
programmi che permettono di preparare rapidamente il proprio computer per
diverse attività.

</para><para>

<!-- TODO: Should explain popcon first -->
<!--
So, you have the ability to choose <emphasis>tasks</emphasis> first,
and then add on more individual packages later.  These tasks loosely
represent a number of different jobs or things you want to do with
your computer, such as <quote>Desktop environment</quote>,
<quote>Web server</quote>, or <quote>Print server</quote><footnote>
-->

Così si avrà la possibilità di scegliere dei <emphasis>task</emphasis>
(funzionalità) prima, e poi aggiungervi in seguito più pacchetti singoli.
Questi task rappresentano con semplicità molti diversi lavori o cose che si
intendano fare con il computer, come <quote>l'ambiente desktop</quote>,
<quote>il server web</quote>, o <quote>il server di stampa</quote><footnote>

<para>

<!--
You should know that to present this list, the installer is merely
invoking the <command>tasksel</command> program. It can be run at any
time after installation to install more packages (or remove them), or
you can use a more fine-grained tool such as <command>aptitude</command>.
If you are looking for a specific single package, after
installation is complete, simply run <userinput>aptitude install
<replaceable>package</replaceable></userinput>, where
<replaceable>package</replaceable> is the name of the package you are
looking for.
-->

Si deve sapere che nel visualizzare quest'elenco, l'installatore sta
soltanto richiamando il programma <command>tasksel</command>; questo
programma può essere eseguito in qualunque momento dopo l'installazione
per aggiungere (o rimuovere) più pacchetti. Per la selezione
manuale dei pacchetti si può usare il programma <command>aptitude</command>.
Se si cerca un singolo pacchetto specifico, dopo che l'installazione è
stata compiuta, semplicemente si deve avviare <userinput>aptitude install
<replaceable>pacchetto</replaceable></userinput>, dove
<replaceable>pacchetto</replaceable> è il nome del pacchetto che si sta
cercando.

</para>

<!--
</footnote>. <xref linkend="tasksel-size-list"/> lists the space
requirements for the available tasks.
-->

</footnote>. <xref linkend="tasksel-size-list"/> mostra lo spazio
necessario per ogni task disponibile.

</para><para>

<!--
Some tasks may be pre-selected based on the characteristics of the
computer you are installing. If you disagree with these selections you can
un-select the tasks. You can even opt to install no tasks at all at this point.
-->

Alcuni task possono essere preselezionati dal sistema
d'installazione in base alle caratteristiche del computer su cui si sta
facendo l'installazione, se i task preselezionati non fossero di proprio
piacimento è possibile deselezionarli. A questo punto dell'installazione
è possibile anche non installare alcun task.

</para>
<note><para>

<!-- TODO: Explain the "Standard system" task first -->
<!--
The <quote>Desktop environment</quote> task will install the GNOME desktop
environment. The options offered by the installer currently do not allow to
select a different desktop environment such as for example KDE.
-->

Il task <quote>Ambiente Desktop</quote> installa l'ambiente desktop GNOME.
Attualmente l'installatore non consente di scegliere un ambiente desktop
differente, per esempio KDE.

</para><para>

<!--
It is possible to get the installer to install KDE by using preseeding
(see <xref linkend="preseed-pkgsel"/>) or by adding
<literal>tasks="standard, kde-desktop"</literal> at the boot prompt
when starting the installer. However, this will only work if the packages
needed for KDE are actually available. If you are installing using a full
CD image, they will need to be downloaded from a mirror as KDE packages are
not included on the first full CD; installing KDE this way should work fine
if you are using a DVD image or any other installation method.
-->

È possibile far installare KDE dall'installatore tramite la
preconfigurazione (si veda <xref linkend="preseed-pkgsel"/>) oppure
specificando <literal>tasks="standard, kde-desktop"</literal> al prompt
boot all'avvio dell'installatore. Purtroppo funziona solo se i pacchetti
necessari a KDE sono disponibili; se l'installazione avviene con l'immagine
completa di un CD è necessario scaricare da un mirror tutti i pacchetti di
KDE non presenti nel CD, se l'installazione avviene con l'immagine di un
DVD oppure con un altro metodo l'installazione di KDE dovrebbe avvenire
correttamente.

</para><para>

<!--
The various server tasks will install software roughly as follows.
DNS server: <classname>bind9</classname>;
File server: <classname>samba</classname>, <classname>nfs</classname>;
Mail server: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Print server: <classname>cups</classname>;
SQL database: <classname>postgresql</classname>;
Web server: <classname>apache</classname>.
-->

Con i diversi task di tipo server sono installati i seguenti programmi.
DNS server: <classname>bind9</classname>;
File server: <classname>samba</classname>, <classname>nfs</classname>;
Mail server: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Print server: <classname>cups</classname>;
SQL database: <classname>postgresql</classname>;
Web server: <classname>apache</classname>.

</para></note>
<para>

<!--
Once you've selected your tasks, select <guibutton>OK</guibutton>. At this
point, <command>aptitude</command> will install the packages that are part
of the tasks you've selected.
-->

Dopo aver scelto quali task si desidera installare, premere
<guibutton>OK</guibutton>; adesso <command>aptitude</command> si occupa
dell'installazione dei pacchetti che fanno parte dei task prescelti.

</para>
<note><para>

<!--
In the standard user interface of the installer, you can use the space bar
to toggle selection of a task.
-->

Con la normale interfaccia utente del programma d'installazione si può usare
la barra spaziatrice per selezionare e deselezionare un task.

</para></note>
<para>

<!--
You should be aware that especially the Desktop task is very large.
Especially when installing from a normal CD-ROM in combination with a
mirror for packages not on the CD-ROM, the installer may want to retrieve
a lot of packages over the network. If you have a relatively slow
Internet connection, this can take a long time. There is no option to
cancel the installation of packages once it has started.
-->

Attenzione, il task Desktop può essere molto grosso. In particolare se
l'installazione avviene da un normale CD-ROM e in combinazione con un
mirror per i pacchetti che non sono sul CD-ROM, l'installatore potrebbe
voler recuperare parecchi pacchetti dalla rete. Se si dispone di una
connessione a Internet lenta, questa operazione potrebbe richiedere
molto tempo. Non c'è modo di arrestare l'installazione dei pacchetti
una volta che è stata avviata.

</para><para>

<!--
Even when packages are included on the CD-ROM, the installer may still
retrieve them from the mirror if the version available on the mirror is
more recent than the one included on the CD-ROM. If you are installing
the stable distribution, this can happen after a point release (an update
of the original stable release); if you are installing the testing
distribution this will happen if you are using an older image.
-->

Anche quando i pacchetti sono presenti sul CD-ROM, l'installatore potrebbe
comunque recuperare i pacchetti da un mirror se la versione del pacchetto
disponibile sul mirror è più recente di quella del pacchetto sul CD-ROM.
Se si installa la distribuzione stable, questo può accadere dopo un rilascio
intermedio (un aggiornamento del rilascio stable originale); se si installa
la distribuzione testing, questo accade se si usa un'immagine datata.

</para><para>

<!--
Each package you selected with <command>tasksel</command> is downloaded,
unpacked and then installed in turn by the <command>apt-get</command> and
<command>dpkg</command> programs. If a particular program needs more
information from the user, it will prompt you during this process.
-->

Ogni pacchetto che si seleziona con <command>tasksel</command> viene
scaricato e installato usando i programmi <command>apt-get</command> e
<command>dpkg</command>. Se un programma ha bisogno di ulteriori
informazioni dall'utente, queste informazioni sono richieste in questa
fase del processo d'installazione.

</para>
   </sect3>
