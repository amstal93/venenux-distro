<!-- retain these comments for translator revision tracking -->
<!-- original version: 43693 -->


 <sect1 id="network-cards">
 <!-- <title>Network Connectivity Hardware</title> -->
 <title>Schede di rete</title>
<para>

<!--
Almost any network interface card (NIC) supported by the Linux kernel
should also be supported by the installation system; modular drivers
should normally be loaded automatically.

<phrase arch="x86">This includes most PCI and PCMCIA cards.</phrase>
<phrase arch="i386">Many older ISA cards are supported as well.</phrase>

<phrase arch="m68k">Again, see <ulink url="&url-m68k-faq;"></ulink>
for complete details.</phrase>
-->

Quasi tutte le schede di rete (NIC) supportate dal kernel Linux dovrebbero
essere supportate dal sistema d'installazione; normalmente i driver modulari
sono caricati automaticamente.
<phrase arch="x86">Fra queste sono comprese molte schede PCI e PCMCIA.</phrase>
<phrase arch="i386">Sono supportate anche molte delle vecchie schede ISA.</phrase>
<phrase arch="m68k">Si veda <ulink url="&url-m68k-faq;"></ulink> per tutti i dettagli.</phrase>

</para><para arch="sparc">

<!--
This includes a lot of generic PCI cards (for systems that have PCI) and
the following NICs from Sun:
-->

Fra queste sono incluse molte delle schede PCI generiche (per i sistemi che
hanno un bus PCI) e le seguenti schede di rete della Sun:

<itemizedlist>
<listitem><para>

Sun LANCE

</para></listitem>
<listitem><para>

Sun Happy Meal

</para></listitem>
<listitem><para>

Sun BigMAC

</para></listitem>
<listitem><para>

Sun QuadEthernet

</para></listitem>
<listitem><para>

MyriCOM Gigabit Ethernet

</para></listitem>
</itemizedlist>

</para><para arch="mipsel">

<!--
Due to kernel limitations only the onboard network interfaces on
DECstations are supported, TurboChannel option network cards currently
do not work.
-->

A causa di alcune limitazioni del kernel solo le interfacce di rete integrate
sulle DECstation sono supportate, le schede di rete opzionali TurboChannel
attualmente non funzionano.

</para><para arch="s390">

<!--
The list of supported network devices is:
-->

Le schede supportate sono:

<itemizedlist>
<listitem><para>

<!--
Channel to Channel (CTC) and ESCON connection (real or emulated)
-->

Channel to Channel (CTC) e ESCON connection (reale o emulate)

</para></listitem>
<listitem><para>

<!--
OSA-2 Token Ring/Ethernet and OSA-Express Fast Ethernet (non-QDIO)
-->

OSA-2 Token Ring/Ethernet e OSA-Express Fast Ethernet (non-QDIO)

</para></listitem>
<listitem><para>

<!--
OSA-Express in QDIO mode, HiperSockets and Guest-LANs
-->

OSA-Express in QDIO modalità, HiperSocket e Guest-LAN

</para></listitem>
</itemizedlist>

</para><para arch="arm">

<!--
On &arch-title;, most built-in Ethernet devices are supported and modules
for additional PCI and USB devices are provided.  The major exception is
the IXP4xx platform (featuring devices such as the Linksys NSLU2) which
needs a proprietary microcode for the operation of its built-in Ethernet
device.  Unofficial images for Linksys NSLU2 with this proprietary
microcode can be obtained from the <ulink
url="&url-slug-firmware;">Slug-Firmware site</ulink>.
-->

Su &arch-title;, la maggior parte dei dispositivi Ethernet integrati sono
supportati e i moduli per i dispositivi PCI e USB aggiuntivi sono disponibili.
L'eccezione più importante è la piattaforma IXP4xx (che comprende dispositivi
come il Linksys NSLU2) che ha bisogno di un microcodice proprietario per il
funzionamento del device Ethernet integrato. Sul <ulink
url="&url-slug-firmware;">sito Slug-Firmware</ulink> sono disponibili le
immagini non ufficiali per il Linksys NSLU2 con il microcodice proprietario.

</para><para arch="x86">

<!--
As for ISDN, the D-channel protocol for the (old) German 1TR6 is not
supported; Spellcaster BRI ISDN boards are also not supported by the
&d-i;. Using ISDN during the installation is not supported.
-->

Per quanto riguarda l'ISDN, il protocollo D-channel per il (vecchio) 1TR6
tedesco non è supportato; le schede ISDN Spellcaster BRI ISDN non sono
supportate dal &d-i;. L'uso di ISDN durante l'installazione non è supportato.

</para>

  <sect2 arch="not-s390" id="nics-firmware">
  <!-- <title>Drivers Requiring Firmware</title> -->
  <title>Driver che richiedono un firmware</title>
<para>

<!--
The installation system currently does not support retrieving firmware.
This means that any network cards that use a driver that requires firmware
to be loaded, is not supported by default.
-->

Il sistema d'installazione attualmente non supporta il recupero del firmware.
Questo vuol dire che qualsiasi scheda di rete il cui driver ha bisogno di
caricare il firmware non è supportata nella configurazione predefinita
dell'installatore.

</para><para>

<!--
If there is no other NIC you can use during the installation, it is still
possible to install &debian; using a full CD-ROM or DVD image. Select the
option to not configure a network and install using only the packages
available from the CD/DVD. You can then install the driver and firmware you
need after the installation is completed (after the reboot) and configure
your network manually. Note that the firmware may be packaged separately
from the driver and may not be available in the <quote>main</quote> section
of the &debian; archive.
-->

Se durante l'installazione non sono disponibili altre schede di rete è
comunque possibile portare a termine l'installazione di &debian; usando le
immagini complete di CD-ROM o DVD. Scegliere di non configurare la rete
ed effettuare l'installazione usando solo i pacchetti disponibili sul
CD/DVD. Il driver e il firmware possono essere installati al termine
dell'installazione (dopo aver riavviato la macchina) e quindi procedere
con la configurazione manuale della rete. Notare che driver e firmware
potrebbero essere contenuti in pacchetti diversi e che il pacchetto con
il firmware potrebbe non essere disponibile nella sezione
<quote>main</quote> dell'archivio &debian;.

</para><para>

<!--
If the driver itself <emphasis>is</emphasis> supported, you may also be
able to use the NIC during installation by copying the firmware from some
medium to <filename>/usr/lib/hotplug/firmware</filename>. Don't forget to
also copy the firmware to that location for the installed system before
the reboot at the end of the installation.
-->

Se il driver <emphasis>è</emphasis> supportato allora è possibile usare la
scheda di rete durante l'installazione a patto di copiare il firmware da
un qualsiasi supporto in <filename>/usr/lib/hotplug/firmware</filename>. Non
scordarsi di copiare il firmware anche nella stessa directory del sistema
installato prima del riavvio al termine dell'installazione.

</para>
  </sect2>

  <sect2 condition="supports-wireless" id="nics-wireless">
  <!-- <title>Wireless Network Cards</title> -->
  <title>Schede per reti wireless</title>
<para>

<!--
Wireless NICs are in general supported as well, with one big proviso.
A lot of wireless adapters require drivers that are either non-free or have
not been accepted into the official Linux kernel. These NICs can generally
be made to work under &debian;, but are not supported during the installation.
-->

Le schede di rete wireless sono generalmente supportate, ma con un importante
prerequisito. Molte schede di rete wireless richiedono dei driver che
non sono liberi oppure che non sono stati accettati nel kernel Linux
ufficiale. Queste schede di rete possono funzionare correttamente con &debian;
ma non sono supportate durante l'installazione.

</para><para>

<!--
If there is no other NIC you can use during the installation, it is still
possible to install &debian; using a full CD-ROM or DVD image. Use the same
procedure as described above for NICs that require firmware.
-->

Se durante l'installazione non sono disponibili altre schede di rete è
comunque possibile portare a termine l'installazione di &debian; usando le
immagini complete di CD-ROM o DVD. Seguire la stessa procedura descritta
in precedenza per le schede di rete che richiedono il firmware.

</para><para>

<!--
In some cases the driver you need may not be available as a Debian package.
You will then have to look if there is source code available in the internet
and compile the driver yourself. How to do this is outside the scope of this
manual.
<phrase arch="x86">If no Linux driver is available, your last resort is to
use the <classname>ndiswrapper</classname> package, which allows you to use
a Windows driver.</phrase>
-->

In certi casi il driver di cui si ha bisogno non è disponibile come un
pacchetto Debian. In questo caso si devono cercare i sorgenti del driver su
Internet e compilare il driver da soli, come fare questa operazione non
rientra nello scopo di questo manuale.
<phrase arch="x86">Se non è disponibile un dirver per Linux, l'unica
possibilità è usare il pacchetto <classname>ndiswrapper</classname> che
permette di usare un driver per Windows.</phrase>

</para>
  </sect2>

  <sect2 arch="sparc" id="nics-sparc-trouble">
  <!-- <title>Known Issues for &arch-title;</title> -->
  <title>Problemi noti su &arch-title;</title>
<para>

<!--
There are a couple of issues with specific network cards that are worth
mentioning here.
-->

Ci sono alcuni problemi con delle sche di rete particolari che vale la
pena ricordare.

</para>

   <sect3>
   <!-- <title>Conflict between tulip and dfme drivers</title> -->
   <title>Conflitto fra i driver tulip e dfme</title>
<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
<para>

<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
<!--
There are various PCI network cards that have the same PCI identification,
but are supported by related, but different drivers. Some cards work with
the <literal>tulip</literal> driver, others with the <literal>dfme</literal>
driver. Because they have the same identification, the kernel cannot
distinguish between them and it is not certain which driver will be loaded.
If this happens to be the wrong one, the NIC may not work, or work badly.
-->

Esistono diverse schede di rete PCI che hanno lo stesso identificativo PCI
ma che sono supportate da driver simili ma comunque diversi. Alcune schede
funzionano con il driver <literal>tulip</literal>, altre con il driver
<literal>dfme</literal>. Dato che hanno lo stesso identificativo il kernel
non è in grado di distinguerle e non può scegliere con certezza quale
driver deve essere caricato. Nel caso venga caricato il driver sbagliato
la scheda di rete potrebbe non funzionare o funzionare male.

</para><para>

<!--
This is a common problem on Netra systems with a Davicom (DEC-Tulip
compatible) NIC. In that case the <literal>tulip</literal> driver is
probably the correct one.

During the installation the solution is to switch to a shell and unload the
wrong driver module using
<userinput>modprobe -r <replaceable>module</replaceable></userinput> (or
both, if they are both loaded). After that you can load the correct module
using <userinput>modprobe <replaceable>module</replaceable></userinput>.
-->

Questo è un problema molto comune sui sistemi Netra con una scheda di
rete Davicom (compatibile con DEC-Tulip). In questo caso il driver
corretto è quello <literal>tulip</literal>.
Durante l'installazione si può attivare una shell, rimuovere il modulo
con il driver errato usando <userinput>modprobe -r
<replaceable>modulo</replaceable></userinput> (o entrambi, se sono
stati caricati ambedue). Poi si può caricare il modulo corretto con
<userinput>modprobe <replaceable>modulo</replaceable></userinput>.

</para>
   </sect3>

   <sect3>
   <!-- <title>Sun B100 blade</title> -->
   <title>Sun B100 blade</title>
<!-- BTS: #384549; should be checked for kernels >2.6.18 -->
<para>

<!--
The <literal>cassini</literal> network driver does not work with Sun B100
blade systems.
-->

Il driver di rete <literal>cassini</literal> non funziona con i sistemi
Sun B100 blade.

</para>
   </sect3>
  </sect2>
 </sect1>
