<!-- retain these comments for translator revision tracking -->
<!-- original version: 43773 -->


 <sect1 id="installation-media">
 <!-- <title>Installation Media</title> -->
 <title>Supporti per l'installazione</title>
<para>

<!--
This section will help you determine which different media types you can use to
install Debian. For example, if you have a floppy disk drive on your machine,
it can be used to install Debian. There is a whole chapter devoted to media,
<xref linkend="install-methods"/>, which lists the advantages and
disadvantages of each media type. You may want to refer back to this page once
you reach that section.
-->

Questa sezione aiuta a determinare quali tipi di supporto è possibile
usare per installare Debian. Per esempio, se sulla propria macchina si
dispone di un lettore per dischetti, è possibile usarlo per installare
Debian. C'è un intero capitolo dedicato ai supporti,
<xref linkend="install-methods"/>, che elenca i vantaggi e gli svantaggi
di ogni supporto. È possibile rileggere questa pagina una volta letta
quella sezione.

</para>

  <sect2 condition="supports-floppy-boot">
  <!-- <title>Floppies</title> -->
  <title>Dischetti</title>
<para>

<!--
In some cases, you'll have to do your first boot from floppy disks.
Generally, all you will need is a
high-density (1440 kilobytes) 3.5 inch floppy drive.
-->

In alcuni casi si deve avviare il computer dai dischetti. In generale
è necessario un drive che supporta i floppy da 3,5 pollici ad alta
densità (1440&nbsp;kilobyte).

</para><para arch="powerpc">

<!--
For CHRP, floppy support is currently broken.
-->

Attualmente il supporto per i floppy per CHRP non funziona.

</para>
  </sect2>

  <sect2>
  <!-- <title>CD-ROM/DVD-ROM</title> -->
  <title>CD-ROM/DVD-ROM</title>
<note><para>

<!--
Whenever you see <quote>CD-ROM</quote> in this manual, it applies to both
CD-ROMs and DVD-ROMs, because both technologies are really
the same from the operating system's point of view, except for some very
old nonstandard CD-ROM drives which are neither SCSI nor IDE/ATAPI.
-->

Tutte le volte che in questo manuale è riportato <quote>CD-ROM</quote>,
devono essere considerati sia i CD-ROM che i DVD-ROM, perché le due
tecnologie sono molto simili dal punto di vista del sistema operativo,
con l'eccezione di alcuni vecchi lettori CD-ROM non standard che non sono
né SCSI né IDE/ATAPI.

</para></note><para>

<!--
CD-ROM based installation is supported for some architectures.
On machines which support bootable CD-ROMs, you should be able to do a
completely
<phrase arch="not-s390">floppy-less</phrase>
<phrase arch="s390">tape-less</phrase>
installation.  Even if your system doesn't
support booting from a CD-ROM, you can use the CD-ROM in conjunction
with the other techniques to install your system, once you've booted
up by other means; see <xref linkend="boot-installer"/>.
-->

L'installazione basata sui CD-ROM è supportata per alcune
architetture. Sulle macchine con il supporto dei CD-ROM avviabili
è possibile fare una completa installazione
<phrase arch="not-s390">senza floppy</phrase>
<phrase arch="s390">senza unità a nastro</phrase>.
Anche se il proprio sistema non supporta l'avvio da CD-ROM, è
possibile usare il CD-ROM assieme ad un'altra tecnica per l'installazione
del sistema, dopo che il sistema d'installazione è stato avviato
in un altro modo; vedere <xref linkend="boot-installer"/>.

</para><para arch="x86">

<!--
Both SCSI and IDE/ATAPI CD-ROMs are supported.  In addition, all
non-standard CD interfaces supported by Linux are supported by the
boot disks (such as Mitsumi and Matsushita drives).  However, these
models might require special boot parameters or other massaging to get
them to work, and booting off these non-standard interfaces is
unlikely.  The <ulink url="&url-cd-howto;">Linux CD-ROM HOWTO</ulink>
contains in-depth information on using CD-ROMs with Linux.
-->

Sono supportati sia i CD-ROM SCSI che quelli IDE/ATAPI. Sono inoltre
supportate dai dischi d'avvio tutte le interfacce CD non standard che sono
supportate da Linux (come per esempio le unità Mitsumi e Matsushita).
Purtroppo questi modelli posso richiedere degli speciali parametri da
passare all'avvio, questo rende improbabile la possibilità di effettuare
l'avvio da queste interfacce. Il <ulink url="&url-cd-howto;">Linux CD-ROM
HOWTO</ulink> contiene delle informazioni più approfondite sull'uso dei
CD-ROM con Linux.

</para><para arch="x86">

<!--
USB CD-ROM drives are also supported, as are FireWire devices that
are supported by the ohci1394 and sbp2 drivers.
-->

Sono supportati anche i CD-ROM con interfaccia USB, così come i
dispositivi FireWire che sono supportati dai driver ohci1394 e sbp2.

</para><para arch="alpha">

<!--
Both SCSI and IDE/ATAPI CD-ROMs are supported on &arch-title;, as long
as the controller is supported by the SRM console.  This rules out many
add-on controller cards, but most integrated IDE and SCSI chips and
controller cards that were provided by the manufacturer can be expected
to work.  To find out whether your device is supported from the SRM
console, see the <ulink url="&url-srm-howto;">SRM HOWTO</ulink>.
-->

Sia i CD-ROM SCSI che quelli IDE/ATAPI sono supportati su &arch-title;,
sempre che il controller sia supportato dalla console SRM. Questo esclude
molti dei controller su schede aggiuntive, però parecchi dei chip
IDE e SCSI integrati e le schade controller fornite dal produttore dovrebbero
funzionare. Per scoprire se un dispositivo è supportato dalla console
SRM si consulti l'<ulink url="&url-srm-howto;">SRM HOWTO</ulink>.

</para><para arch="arm">

<!--
IDE/ATAPI CD-ROMs are supported on all ARM machines.
On RiscPCs, SCSI CD-ROMs are also supported.
-->

I CD-ROM IDE/ATAPI sono supportati su tutte le macchine ARM. Sui RiscPC
sono supportati anche i CD-ROM SCSI.

</para><para arch="mips">

<!--
On SGI machines, booting from CD-ROM requires a SCSI CD-ROM drive
capable of working with a logical blocksize of 512 bytes. Many of the
SCSI CD-ROM drives sold on the PC market do not have this
capability. If your CD-ROM drive has a jumper labeled
<quote>Unix/PC</quote> or <quote>512/2048</quote>, place it in the
<quote>Unix</quote> or <quote>512</quote> position.
To start the install, simply choose the <quote>System installation</quote>
entry in the firmware.  The Broadcom BCM91250A supports standard IDE devices,
including CD-ROM drives, but CD images for this platform are currently not
provided because the firmware doesn't recognize CD drives.  In order to
install Debian on an Broadcom BCM91480B evaluation board, you need an PCI
IDE, SATA or SCSI card.
-->

Sulle macchine SGI l'avvio da CD-ROM richiede un lettore CD-ROM in grado di
lavorare con blocchi logici di dimensione pari a 512&nbsp;byte. Parecchi dei
lettori di CD-ROM SCSI sul mercato non hanno questa funzionalità; se il
proprio lettore ha un jumper etichettato con <quote>Unix/PC</quote> o con
<quote>512/2048</quote> spostarlo sulla posizione <quote>Unix</quote> o
<quote>512</quote>.
Per avviare l'installazione basta semplicemente selezionare la voce
<quote>System installation</quote> nel firmware. La Broadcom BCM91250A
supporta i dispositivi IDE, tra cui anche le unità CD-ROM, ma le
immagini dei CD per questa piattaforma non sono attualmente disponibili
perché il firmware non riconosce i CD. Per poter installare Debian su una
scheda per prototipazione BCM91480B è necessaria una scheda PCI IDE, SATA
o SCSI.

</para><para arch="mipsel">

<!--
On DECstations, booting from CD-ROM requires a SCSI CD-ROM drive
capable of working with a logical blocksize of 512 bytes. Many of the
SCSI CD-ROM drives sold on the PC market do not have this capability.
If your CD-ROM drive has a jumper labeled <quote>Unix/PC</quote> or
<quote>512/2048</quote>, place it in the <quote>Unix</quote> or
<quote>512</quote> position.
-->

Sulle DECstation per avviare l'installazione da CD-ROM è necessario
un CD-ROM SCSI capace di lavorare con i blocchi logici da 512&nbsp;byte.
Molti dei CD-ROM SCSI venduti per il mercato PC non hanno questa
funzionalità. Se il proprio lettore CD-ROM ha un jumper etichettato con
<quote>Unix/PC</quote> o con <quote>512/2048</quote>, spostarlo sulla
posizione <quote>Unix</quote> o <quote>512</quote>.

</para><para arch="mipsel">

<!--
CD 1 contains the installer for the r3k-kn02 subarchitecture
(the R3000-based DECstations 5000/1xx and 5000/240 as well as
the R3000-based Personal DECstation models), CD 2 the
installer for the r4k-kn04 subarchitecture (the R4x00-based
DECstations 5000/150 and 5000/260 as well as the Personal DECstation
5000/50).
-->

Il CD 1 contiene l'Installatore per la sottoarchitettura r3k-kn02 (le
DECstation 5000/1xx e 5000/240 basate su R3000 e i modelli Personal
DECstation basati su R3000), mentre il CD 2 contiene l'Installatore per
la sottoarchitettura r4k-kn04 (le DECstation 5000/150 e 5000/260 basate
su R4x00 e le Personal DECstation 5000/50).

</para><para arch="mipsel">

<!--
To boot from CD, issue the command <userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
on the firmware prompt, where <replaceable>#</replaceable> is the
number of the TurboChannel device from which to boot (3 on most
DECstations) and <replaceable>id</replaceable> is the SCSI ID of the
CD-ROM drive.  If you need to pass additional parameters, they can
optionally be appended with the following syntax:
-->

Per avviare da CD, eseguire il comando <userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
dal prompt dei comandi del firmware, dove <replaceable>#</replaceable>
è il numero del dispositivo TurboChannel da cui si vuole avviare
l'installazione (3 sulla maggior parte delle DECstation) e
<replaceable>id</replaceable> è l'ID SCSI del lettore CD-ROM. Se
si ha bisogno di passare dei parametri addizionali, questi possono essere
appesi usando la seguente sintassi.

</para><para arch="mipsel">

<!--
<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=value1 param2=value2 ...</userinput>
-->

<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=valore1 param2=valore2 ...</userinput>

</para>
  </sect2>

  <sect2>
  <!-- <title>Hard Disk</title> -->
  <title>Dischi fissi</title>
<para>

<!--
Booting the installation system directly from a hard disk is another option
for many architectures. This will require some other operating system
to load the installer onto the hard disk.
-->

L'avvio dell'installazione del sistema direttamente dal disco fisso è
un'altra possibilità disponibile per molte architetture. Questa
richiede un altro sistema operativo per caricare l'Installatore sul disco
fisso.

</para><para arch="m68k">

<!--
In fact, installation from your local disk is the preferred
installation technique for most &architecture; machines.
-->

Infatti, l'installazione dal disco fisso è la tecnica d'installazione
preferita per le macchine &architecture;.

</para><para arch="sparc">

<!--
Although the &arch-title; does not allow booting from SunOS
(Solaris), you can install from a SunOS partition (UFS slices).
-->

Sebbene &arch-title; non permetta di effettuare l'avvio da SunOS (Solaris),
è possibile installare da una partizione SunOS (UFS slice).

</para>
  </sect2>

  <sect2 condition="bootable-usb">
  <!-- <title>USB Memory Stick</title> -->
  <title>Penne USB</title>
<para>

<!--
Many Debian boxes need their floppy and/or CD-ROM drives only for
setting up the system and for rescue purposes. If you operate some
servers, you will probably already have thought about omitting those
drives and using an USB memory stick for installing and (when
necessary) for recovering the system. This is also useful for small
systems which have no room for unnecessary drives.
-->

Molti computer Debian hanno bisogno del floppy o del lettore CD-ROM solo
per installare il sistema o per operazioni di ripristino del sistema. Se
ci si trova a operare con dei server probabilmente questi lettori sono
già stati rimossi ed è possibile usare una chiavetta USB per
installare e (quando necessario) recuperare il sistema. Questo è
utile anche per i piccoli sistemi che non hanno spazio per lettori
inutili.

</para>
  </sect2>

  <sect2>
  <!-- <title>Network</title> -->
  <title>Rete</title>
<para>

<!--
The network can be used during the installation to retrieve files needed
for the installation. Whether the network is used or not depends on the
installation method you choose and your answers to certain questions that
will be asked during the installation. The installation system supports
most types of network connections (including PPPoE, but not ISDN or PPP),
via either HTTP or FTP. After the installation is completed, you can also
configure your system to use ISDN and PPP.
-->

La rete può essere usata durante l'installazione per recuperare i file
necessari all'installazione. L'uso della rete dipende dal metodo
d'installazione scelto e dalle risposte date ad alcune delle domande poste
durante l'installazione. Il sistema d'installazione supporta connessioni
alla rete di diversi tipi (tra questi PPPoE ma non ISDN e PPP), via FTP o
HTTP. Al termine dell'installazione è possibile configurare il proprio
sistema per l'uso di ISDN e PPP.

</para><para condition="supports-tftp">

<!--
You can also <emphasis>boot</emphasis> the installation system over the
network. <phrase arch="mips">This is the preferred installation technique
for &arch-title;.</phrase>
-->

È anche possibile <emphasis>avviare</emphasis> il sistema d'installazione
dalla rete. <phrase arch="mips">Questo è il metodo d'installazione preferito
per &arch-title;.</phrase>

</para><para condition="supports-nfsroot">

<!--
Diskless installation, using network booting from a local area network
and NFS-mounting of all local filesystems, is another option.
-->

L'installazione senza dischi, usando l'avvio dalla rete da una LAN e montare
l'intero filesystem attraverso NFS è un'altra opzione.

</para>
  </sect2>

  <sect2>
  <!-- <title>Un*x or GNU system</title> -->
  <title>Un*x o sistemi GNU</title>
<para>

<!--
If you are running another Unix-like system, you could use it to install
&debian; without using the &d-i; described in the rest of this
manual. This kind of install may be useful for users with otherwise
unsupported hardware or on hosts which can't afford downtime.  If you
are interested in this technique, skip to the <xref
linkend="linux-upgrade"/>.
-->

Se si usano altri sistemi Unix, è possibile usarli anche per installare
&debian; senza l'uso di &d-i; descritto nel resto del manuale. Questo
tipo d'installazione può essere utile per gli utenti che hanno
dell'hardware che altrimenti non è supportato o sui computer che
non possono tollerare interruzioni. Se interessa a questa tecnica,
si veda la sezione <xref linkend="linux-upgrade"/>.

</para>
  </sect2>

  <sect2>
  <!-- <title>Supported Storage Systems</title> -->
  <title>Sistemi d'archiviazione supportati</title>
<para>

<!--
The Debian boot disks contain a kernel which is built to maximize the
number of systems it runs on.  Unfortunately, this makes for a larger
kernel, which includes many drivers that won't be used for your
machine (see <xref linkend="kernel-baking"/> to learn how to
build your own kernel).  Support for the widest possible range of
devices is desirable in general, to ensure that Debian can be
installed on the widest array of hardware.
-->

I dischi d'avvio di Debian contengono un kernel che è stato compilato
per massimizzare il numero di sistemi su cui può girare.
Sfortunatamente questo comporta un kernel di dimensione maggiore che
include molti driver che non verranno usati sulla propria macchina (vedere
<xref linkend="kernel-baking"/> per imparare a compilare un kernel
personalizzato). Il supporto per la maggior quantità di dispositivi
è desiderabile in generale per permettere che Debian possa essere
installata sul numero maggiore possibile di combinazioni di hardware.

</para><para arch="x86">

<!--
Generally, the Debian installation system includes support for floppies,
IDE drives, IDE floppies, parallel port IDE devices, SCSI controllers and
drives, USB, and FireWire.  The supported file systems include FAT,
Win-32 FAT extensions (VFAT) and NTFS.
-->

In generale il sistema d'installazione di Debian include il supporto per
i floppy, i driver IDE, i floppy IDE, i dispositivi IDE collegati alla
porta parallela, i controller e i dispositivi SCSI, USB e FireWire. I
filesystem supportati comprendono FAT, le estensioni Win-32
FAT (VFAT) e NTFS.

</para><para arch="i386">

<!--
Disk interfaces that emulate the <quote>AT</quote> hard disk interface
&mdash; often called MFM, RLL, IDE, or ATA &mdash; are supported.  Very old
8&ndash;bit hard disk controllers used in the IBM XT computer are supported
only as a module. SCSI disk controllers from many different manufacturers
are supported. See the
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
for more details.
-->

Le interfacce dei dischi che emulano l'interfaccia <quote>AT</quote>,
spesso chiamate MFM, RLL, IDE o ATA, sono supportate. I vecchissimi controller
dei dischi ad 8&nbsp;bit usati nei computer IBM XT sono supportati solo come
modulo. I controller SCSI dei dischi di molti costruttori sono supportati.
Per maggiori dettagli si veda il <ulink url="&url-hardware-howto;">Linux
Hardware Compatibility HOWTO</ulink>.

</para><para arch="m68k">

<!--
Pretty much all storage systems supported by the Linux kernel are
supported by the Debian installation system.  Note that the current
Linux kernel does not support floppies on the Macintosh at all, and
the Debian installation system doesn't support floppies for Amigas.
Also supported on the Atari is the Macintosh HFS system, and AFFS as a
module.  Macs support the Atari (FAT) file system.  Amigas support the
FAT file system, and HFS as a module.
-->

Buona parte dei sistemi di archiviazione supportati dal kernel Linux
sono supportati dal sistema d'installazione di Debian. Notare che
l'attuale kernel Linux non supporta i floppy su Macintosh e il
sistema di installazione di Debian non supporta i floppy su Amiga.
Per Atari è inoltre supportato il filesystem Macintosh HFS,
e AFFS è supportato come modulo. Mac supporta il file system
Atari (FAT). Su Amiga è supportato il file system FAT e anche
HFS come modulo.

</para><para arch="sparc">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system. The following SCSI drivers are supported in the default
kernel:
-->

Tutti i sistemi di archiviazione supportati dal kernel Linux sono
supportati dal sistema d'avvio. I seguenti controller SCSI sono
supportati dal kernel predefinito:

<itemizedlist>
<listitem><para>

Sparc ESP

</para></listitem>
<listitem><para>

PTI Qlogic,ISP

</para></listitem>
<listitem><para>

Adaptec AIC7xxx

</para></listitem>
<listitem><para>

<!--
NCR and Symbios 53C8XX
-->

NCR e Symbios 53C8XX

</para></listitem>
</itemizedlist>

<!--
IDE systems (such as the UltraSPARC 5) are also supported. See
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>
for more information on SPARC hardware supported by the Linux kernel.
-->

Anche i sottosistemi IDE (come per esempio quello delle UltraSPARC 5)
sono supportati. Si vedano le <ulink url="&url-sparc-linux-faq;">Linux
for SPARC Processors FAQ</ulink> per avere più informazioni
sull'hardware SPARC supportato dal kernel Linux.

</para><para arch="alpha">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  This includes both SCSI and IDE disks.  Note, however,
that on many systems, the SRM console is unable to boot from IDE drives,
and the Jensen is unable to boot from floppies.  (see
<ulink url="&url-jensen-howto;"></ulink>
for more information on booting the Jensen)
-->

Ogni sistema di archiviazione supportato dal kernel Linux è
supportato anche dal sistema d'avvio, quindi sia dischi SCSI che IDE.
Notare, tuttavia, che su molti sistemi la console SRM non è in
grado di eseguire l'avvio dai dischi IDE e che Jensen non è in grado
di fare l'avvio da dischetto (si veda <ulink url="&url-jensen-howto;"></ulink>
per maggiori informazioni sull'avvio di Jensen).

</para><para arch="powerpc">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
floppies on CHRP systems at all.
-->

Ogni sistema di archiviazione supportato dal kernel Linux è
supportato anche dal sistema d'avvio. Notare che il kernel Linux
non supporta i floppy sui sistemi CHRP.

</para><para arch="hppa">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
the floppy drive.
-->

Ogni sistema di archiviazione supportato dal kernel Linux è
supportato anche dal sistema d'avvio. Notare che il kernel Linux non
supporta il lettore floppy.

</para><para arch="mips">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.
-->

Ogni sistema di archiviazione supportato dal kernel Linux è
supportato anche dal sistema d'avvio.

</para><para arch="s390">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  This means that FBA and ECKD DASDs are supported with
the old Linux disk layout (ldl) and the new common S/390 disk layout (cdl).
-->

Ogni sistema di archiviazione supportato dal kernel Linux è
supportato anche dal sistema d'avvio. Questo significa che FBA e ECKD DASD
sono supportati con il vecchio layout dei dischi di Linux (ldl) e con il
nuovo layout dei dischi comune S/390 (cdl).

</para>
  </sect2>
 </sect1>
