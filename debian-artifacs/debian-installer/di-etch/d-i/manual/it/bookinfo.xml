<!-- retain these comments for translator revision tracking -->
<!-- original version: 43744 -->


<bookinfo id="debian_installation_guide">
<!-- <title>&debian; Installation Guide</title> -->
<title>Guida all'installazione di &debian;</title>

<abstract>
<para>

<!--
This document contains installation instructions for the &debian;
&release; system (codename <quote>&releasename;</quote>),
for the &arch-title; (<quote>&architecture;</quote>)
architecture.  It also contains pointers to more information and
information on how to make the most of your new Debian system.
-->

Questo documento contiene le istruzioni per l'installazione del sistema
&debian; &release; (nome in codice <quote>&releasename;</quote>), per
l'architettura &arch-title; (<quote>&architecture;</quote>). Contiene anche
riferimenti a informazioni più approfondite e informazioni su come ottenere
il meglio dal proprio sistema Debian.

</para>

<para>
<note arch="m68k"><para>

<!--
Because the &arch-title; port is not a release architecture for
&releasename;, there is no official version of this manual for
&arch-title; for &releasename;. However, because the port is still
active and there is hope that &arch-title; may be included again
in future official releases, this development version of the
Installation Guide is still available.
-->

Poiché &arch-title; non è una delle architettura su cui è rilasciata
&releasename;, non esiste una versione ufficiale di questo manuale su
&arch-title; per &releasename;. Però il port è ancora attivo e quindi è
possibile che &arch-title; possa essere nuovamente inclusa in un altro
rilascio ufficiale, per questa ragione la versione di sviluppo della Guida
all'installazione continua a essere disponibile.

</para><para>

<!--
Because &arch-title; is not an official architecture, some of the
information, and especially some links, in this manual may be
incorrect. For additional information, please check the
<ulink url="&url-ports;">webpages</ulink> of the port or contact the
<ulink url="&url-list-subscribe;">debian-&arch-listname; mailing
list</ulink>.
-->

Siccome &arch-title; non è una delle architetture ufficiali alcune
informazioni, in particolare alcuni collegamenti, presenti in questo manuale
potrebbero essere sbagliate. Per maggiori informazioni si consultino le
<ulink url="&url-ports;">pagine web</ulink> del port oppure si contatti la
<ulink url="&url-list-subscribe;">mailing list debian-&arch-listname;</ulink>.

</para></note>

<warning condition="not-checked"><para>

<!--
This installation guide is based on an earlier manual written for
the old Debian installation system (the <quote>boot-floppies</quote>), and has
been updated to document the new Debian installer. However, for
&architecture;, the manual has not been fully updated and fact checked
for the new installer. There may remain parts of the manual that are
incomplete or outdated or that still document the boot-floppies
installer. A newer version of this manual, possibly better documenting
this architecture, may be found on the Internet at the
<ulink url="&url-d-i;">&d-i; home page</ulink>. You may also be able
to find additional translations there.
-->

Questa guida all'installazione è basata sul manuale scritto per il
vecchio sistema d'installazione di Debian (<quote>boot-floppies</quote>)
che è stato aggiornato per documentare il nuovo programma
d'installazione di Debian. Comunque, per &architecture;, il manuale non è
stato aggiornato completamente e verificato per il nuovo programma di
installazione, ci potrebbero essere parti del manuale che sono incomplete
o non aggiornate o che documentano ancora il programma di installazione
boot-floppies. Una versione più recente di questo manuale, che
probabilmente documenta meglio questa architettura, può essere trovata
su Internet nella <ulink url="&url-d-i;">pagina del &d-i;</ulink>.
Nella stessa pagina ci potrebbero essere anche altre traduzioni.

</para></warning>

<note condition="checked"><para>

<!--
Although this installation guide for &architecture; is mostly up-to-date,
we plan to make some changes and reorganize parts of the manual after the
official release of &releasename;. A newer version of this manual may be
found on the Internet at the <ulink url="&url-d-i;">&d-i; home page</ulink>.
You may also be able to find additional translations there.
-->

Sebbene questa guida all'installazione per &architecture; è per lo più
aggiornata abbiamo pianificato alcuni cambiamenti e abbiamo riorganizzato
le parti del manuale dopo il rilascio ufficiale di &releasename;. Una
versione più recente può essere trovata su Internet nella
<ulink url="&url-d-i;">pagina del &d-i;</ulink>. Nella stessa pagina ci
potrebbero essere anche altre traduzioni.

</para></note>
</para>

<para condition="translation-status">

<!-- La compilazione di questo paragrafo è attivata -->

<!--
Translators can use this paragraph to provide some information about
the status of the translation, for example if the translation is still
being worked on or if review is wanted (don't forget to mention where
comments should be sent!).

See build/lang-options/README on how to enable this paragraph.
Its condition is "translation-status".
-->

Commenti, critiche e suggerimenti sulla traduzione possono essere inviati
alla lista di messaggi dei traduttori Debian italiani
<email>debian-l10n-italian@lists.debian.org</email>.

</para>
</abstract>

<copyright>
 <year>2004</year>
 <year>2005</year>
 <year>2006</year>
 <year>2007</year>
 <holder>Debian Installer team</holder>
</copyright>

<legalnotice>
<para>

<!--
This manual is free software; you may redistribute it and/or modify it
under the terms of the GNU General Public License. Please refer to the
license in <xref linkend="appendix-gpl"/>.
-->

Questo manuale è software libero; può essere redistribuito e/o modificato
nei termini della GNU General Public Licence. Si faccia riferimento alla
licenza in <xref linkend="appendix-gpl"/>.

</para>
</legalnotice>
</bookinfo>
