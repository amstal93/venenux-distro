mdcfg (1.21) unstable; urgency=low

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 16:55:03 +0100

mdcfg (1.20) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by drtvasudevan

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:47:12 +0100

mdcfg (1.19) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hindi (hi.po) by Nishant Sharma
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Romanian (ro.po) by Eddy Petrișor
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 15:13:12 +0200

mdcfg (1.18) unstable; urgency=low

  [ Colin Watson ]
  * Mark ${DEVICES} in mdcfg/deletemenu choices and mdcfg/deleteverify as
    untranslatable.

  [ Frans Pop ]
  * Kernels >=2.6.18 use have module raid456 instead of raid5.
    Thanks to Sven Luther for the heads-up. Closes: #389079.
  * Add error checking when deleting raid devices.
  * Don't continue if no partitions are available.
  * Properly initialize MISSING_DEVICES for RAID1 setups.

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Fri,  6 Oct 2006 02:41:27 +0200

mdcfg (1.17) unstable; urgency=low

  * mdrun is being deprecated, use mdadm instead; based on patch by
    Martin Krafft.
  * Only write mdadm.conf for the target system if not already present; mdadm
    writes one on installation; patch again by Martin Krafft.
  * Add Lintian override for standards-version

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Panjabi (pa.po) by A S Alam
  * Portuguese (pt.po) by Miguel Figueiredo
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 15 Aug 2006 02:35:05 +0200

mdcfg (1.16) unstable; urgency=low

  * mdcfg.sh:
    * delete db_stop calls which should not be used inside d-i
    * actually cancel if user selects cancel during selection of RAID level
    * use proper loging; adds dependency on di-utils
  * Set maintainer to d-i team and add myself to uploaders.
  * Put debhelper in Build-Depends rather than in Build-Depends-Indep.
  * Remove Standards-Version as it is not relevant for udebs.

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll

 -- Frans Pop <fjp@debian.org>  Wed, 12 Jul 2006 18:44:56 +0200

mdcfg (1.15) unstable; urgency=low

  [ David Härdeman ]
  * Use mapdevfs where appropriate. Closes: #304677

  [ Joey Hess ]
  * prebaseconfig transition

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Giuseppe Sacco
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Jun 2006 22:07:22 -0400

mdcfg (1.14) unstable; urgency=low

  [ Joey Hess ]
  * De-hardcode path to mdrun.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 24 Jan 2006 21:51:37 +0100

mdcfg (1.13) unstable; urgency=low

  * In 2.6.14, the md kernel module was renamed to md-mod. Deal with this
    change by trying to modprobe it if modprobe md fails.

  [ Updated translations ]
  * Bengali (bn.po) by Baishampayan Ghose
  * Icelandic (is.po) by David Steinn Geirsson
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Romanian (ro.po) by Eddy Petrişor
  * Swedish (sv.po) by Daniel Nylander

 -- Joey Hess <joeyh@debian.org>  Tue, 15 Nov 2005 14:14:46 -0500

mdcfg (1.12) unstable; urgency=low

  [ Colin Watson ]
  * Update GPL notices with the FSF's new address.

  [ Updated translations ]
  * German (de.po) by Holger Wansing
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Esperanto (eo.po) by Serge Leblanc
  * Japanese (ja.po) by Kenshi Muto
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrisor
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 17:11:57 +0200

mdcfg (1.11) unstable; urgency=low

  * Christian Perrier
    - Add a comment about the Choices length for translators
      in the debconf templates file
  * Colin Watson
    - Only write mdadm.conf if RAID is in use.
    - Remove useless dependency on parted-udeb.
    - Use --auto=yes parameter to mdadm when creating arrays, to avoid
      failing with udev.
    - Create /dev/md/ at mdcfg startup, for udev. (Perhaps this should be
      done in mdadm instead?)

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Hebrew (he.po) by Lior Kaplan
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagasy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 17:40:17 +0300

mdcfg (1.10) unstable; urgency=low

  * Matt Kraai
    - Fix the spelling of "file system".
  * Updated translations: 
    - Catalan (ca.po) by Jordi Mallach
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed, 16 Feb 2005 22:07:16 -0500

mdcfg (1.09) unstable; urgency=low

  * Note that this includes fix(es) for variable substitution bugs in
    translated templates.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 17:15:49 -0500

mdcfg (1.08) unstable; urgency=low

  * Updated translations: 
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team (Note, includes fix for broken subst var.)

 -- Joey Hess <joeyh@debian.org>  Tue, 16 Nov 2004 15:47:42 -0500

mdcfg (1.07) unstable; urgency=low

  * Joshua Kwan
    - remove myself from Uploaders, I no longer have time to help keep this
      package in shape.
  * Joey Hess
    - Applied a patch from Andrew Pollock to add support for making raid1
      arrays in degraded mode. Closes: #253693, #276079

 -- Joey Hess <joeyh@debian.org>  Mon, 25 Oct 2004 20:35:51 -0400

mdcfg (1.06) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Lithuanian (lt.po) by Kęstutis Biliūnasn

 -- Joey Hess <joeyh@debian.org>  Wed, 20 Oct 2004 14:31:40 -0400

mdcfg (1.05) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Italian (it.po) by Giuseppe Sacco
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Oct 2004 14:56:20 -0400

mdcfg (1.04) unstable; urgency=low

  * Joey Hess
    - Apply patch from Jimmy Kaplowitz to add support for raid 5
      and fix some bugs in raid1 checks for required number of
      partitions. Closes: #253212, #259900
  * Updated translations: 
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Indonesian (id.po) by Arief S Fitrianto
    - Bøkmal, Norwegian (nb.po) by Bjørn Steensrud
    - Romanian (ro.po) by Eddy Petrisor

 -- Joey Hess <joeyh@debian.org>  Wed, 29 Sep 2004 15:59:40 -0400

mdcfg (1.03) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Bøkmal, Norwegian (nb.po) by Axel Bojer
    - Swedish (sv.po) by Per Olofsson

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 21:37:52 -0400

mdcfg (1.02) unstable; urgency=low

  * Colin Watson
    - mdcfg now works when d-i is running at priority critical. There didn't
      seem to be anything that could be left out, so I just changed all
      non-error questions to be asked at critical.
  * Joey Hess
    - Remove hack around bug 250427 in prebaseconfig, mdadm defaults to
      autostart now.
    - Create mdadm.cfg by running mdadm inside the /target chroot, as outside
      the chroot it gets devfs device names in there and doesn't work.
  * Updated translations: 
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - French (fr.po) by French Team
    - Gallegan (gl.po) by Héctor Fenández López
    - Russian (ru.po) by Russian L10N Team

 -- Joey Hess <joeyh@debian.org>  Fri, 17 Sep 2004 21:12:05 -0400

mdcfg (1.01) unstable; urgency=low

  * Joey Hess
    - Remove unnecessary seen flag fsetting.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Matjaz Horvat
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon,  6 Sep 2004 20:39:15 -0400

mdcfg (1.00) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Frederik Dannemare
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Persian (fa.po) by Arash Bijanzadeh
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VERÓK István
    - Italian (it.po) by Giuseppe Sacco
    - Bøkmal, Norwegian (nb.po) by Knut Yrvin
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Sun, 25 Jul 2004 19:28:30 -0400

mdcfg (0.23) unstable; urgency=medium

  * Joshua Kwan
    - Create a mdadm.conf and copy it into /target/etc/mdadm/ during
      prebaseconfig. Works around #231823, possibly unbreaks 2.6 installs
      over software RAID.
  * Updated translations:
    - Albanian (sq.po) by Elian Myftiu
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Alwin Meschede
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Marius Gedminas
    - Dutch (nl.po) by Bart Cornelis
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Ming Hua
    
 -- Joshua Kwan <joshk@triplehelix.org>  Mon, 21 Jun 2004 13:32:25 -0700

mdcfg (0.22) unstable; urgency=low

  * Martin Michlmayr
    - Use a better way to find out which devices are available as
      spare devices.  Closes: #250771.
    - Use a better way to make sure that enough spare devices have
      been selected.
  * Joshua Kwan
    - Remove heinous cat abuse.
    - Add RAID 0 support.
    - Add myself to Uploaders.

 -- Joshua Kwan <joshk@triplehelix.org>  Tue, 15 Jun 2004 22:22:48 -0700

mdcfg (0.21) unstable; urgency=low

  * Martin Michlmayr
    - When creating a new RAID device using the 2.6 kernel, don't show
      partitions which are already part of an MD device.  Closes:
      #250973.
  * Updated translations:
    - Bosnian (bs.po) by Safir Šećerović
    - Korean (ko.po) by Changwoo Ryu
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 27 May 2004 01:59:25 -0300

mdcfg (0.20) unstable; urgency=low

  * Joey Hess
    - Add a prebaseconfig script to hack /etc/mdadm/debian.conf to
      turn on AUTOSTART so the raid will be available at boot.
      Closes: #250427
  * Joshua Kwan
    - Add Joey to Uploaders.
  * Updated translations:
    - Italian (it.po) by Stefano Melchior

 -- Joey Hess <joeyh@debian.org>  Mon, 24 May 2004 18:12:39 -0300

mdcfg (0.19) unstable; urgency=medium

  * Martin Michlmayr
    - Run mdadm --create with the -R parameter so it won't hang waiting for
      input when the partition has an existing filesystem.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Finnish (fi.po) by Tapio Lehtonen
    - Norwegian (nb.po) by Knut Yrvin

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 24 May 2004 02:55:11 +0100

mdcfg (0.18) unstable; urgency=low

  * Martin Michlmayr
    - Print a debconf error if you try to delete an MD device when
      none exist.
    - Call find-partitions with the new --ignore-fstype argument in order
      to see new RAID partitions which have an existing filesystem.
    - Make the search for RAID partitions slightly more reliable.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Alwin Meschede
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by Christian Perrier
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by I Gede Wijaya S
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Marius Gedminas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 19 May 2004 13:10:12 +0100

mdcfg (0.17) unstable; urgency=low

  * Christian Perrier
    - Improve the templates.
  * Martin Michlmayr
    - Complain in advance if not enough partitions are available for
      active + spare.
    - Set a default number of active (2) and spare (0) partitions for
      RAID1.
    - Add myself as uploader.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Alwin Meschede
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by I Gede Wijaya S
    - Italian (it.po) by Stefano Melchior
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Marius Gedminas
    - Norwegian (nb.po) by Bjørn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Nikolai Prokoschenko
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 05 May 2004 00:06:14 +0100

mdcfg (0.16) never-released; urgency=low

  * It should now *really* be possible to create more than two RAID device :-)

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 22 Jan 2004 23:00:19 +0200

mdcfg (0.15) never-released; urgency=low

  * It should now be possible to create more than two RAID device
  * Fix a typo in the Depends line (Martin Michlmayr).

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 21 Jan 2004 02:47:23 +0200

mdcfg (0.14) never-released; urgency=low

  * mdcfg-utils is now generated for use with partman-md

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 20 Jan 2004 21:51:50 +0200

mdcfg (0.13) never-released; urgency=low

  * db_capb and db_stop no more called, hopefully this fixed some
    issues with partman-md

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 20 Jan 2004 21:13:41 +0200

mdcfg (0.12) never-released; urgency=low

  * Deleting md devices should now work properly

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 10 Jan 2004 21:29:09 +0200

mdcfg (0.11) never-released; urgency=low

  * The script is now also installed as /bin/mdcfg

 -- Paul Fleischer <proguy@proguy.dk>  Sat, 10 Jan 2004 20:01:55 +0200

mdcfg (0.10) never-released; urgency=low

  * mdadm should now be displayed *after* lvmcfg

 -- Paul Fleischer <proguy@proguy.dk>  Fri, 29 Jan 2004 21:57:00 +0200

mdcfg (0.09) never-released; urgency=low

  * mdadm is installed via apt-install in the beginning of the script

 -- Paul Fleischer <proguy@proguy.dk>  Fri, 27 Jan 2004 22:56:00 +0200

mdcfg (0.07) never-released; urgency=low

  * mdadm should now *really* be installed.
  * The --force option should be placed properly when creating arrays.

 -- Paul Fleischer <proguy@proguy.dk>  Fri, 27 Jan 2004 22:56:00 +0200

mdcfg (0.06) never-released; urgency=low

  * Request mdadm to be installed no matter what.

 -- Paul Fleischer <proguy@proguy.dk>  Fri, 23 Jan 2004 22:46:00 +0200

mdcfg (0.05) never-released; urgency=low

  * Fixed the text in the templates to be a bit more useable.
  * We ask for mdadm to be installed on the system, if any MD partitions
    exist.

 -- Paul Fleischer <proguy@proguy.dk>  Fri, 23 Jan 2004 17:08:00 +0200

mdcfg (0.04) never-released; urgency=low

  * RAID1 device should now be creatable.
  * When stopping a device, it is actually deleted now.

 -- Paul Fleischer <proguy@proguy.dk>  Thu, 22 Jan 2004 18:31:00 +0200

mdcfg (0.03) never-released; urgency=low

  * A little menu-system built on cdebconf is in place... not usabel yet,
    though.

 -- Paul Fleischer <proguy@proguy.dk>  Tue, 21 Jan 2004 01:10:00 +0200

mdcfg (0.02) never-released; urgency=low

  * file in /var/lib/parted/block.d is now called mdcfg and not lvmcfg

 -- Paul Fleischer <proguy@proguy.dk>  Tue, 20 Jan 2004 22:32:00 +0200

mdcfg (0.01) never-released; urgency=low

  * Initial version

 -- Paul Fleischer <proguy@proguy.dk>  Tue, 20 Jan 2004 22:01:25 +0200

