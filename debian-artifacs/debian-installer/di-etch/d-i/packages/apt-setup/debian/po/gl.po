# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/gl.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Galician messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: 2006-05-26 20:32+0200\n"
"Last-Translator: Jacobo Tarrio <jtarrio@debian.org>\n"
"Language-Team: Galician <trasno@ceu.fi.udc.es>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. MUST be kept below 55 characters/columns
#: ../apt-setup-udeb.templates:5
msgid "Configure the package manager"
msgstr "Configurar o xestor de paquetes"

#. Type: text
#. Description
#. Translators, "apt" is the program name
#. so please do NOT translate it
#: ../apt-setup-udeb.templates:11
msgid "Configuring apt"
msgstr "A configurar apt..."

#. Type: text
#. Description
#: ../apt-setup-udeb.templates:15
msgid "Running ${SCRIPT}..."
msgstr "A executar ${SCRIPT}..."

#. Type: text
#. Description
#: ../apt-setup-udeb.templates:19
msgid "Scanning local repositories..."
msgstr "A examinar os repositorios locais..."

#. Type: text
#. Description
#: ../apt-setup-udeb.templates:23
msgid "Scanning the security updates repository..."
msgstr "A examinar o repositorio de actualizacións de seguridade..."

#. Type: error
#. Description
#: ../apt-setup-udeb.templates:33
msgid "Cannot access security updates"
msgstr "Non se pode acceder ás actualizacións de seguridade"

#. Type: error
#. Description
#: ../apt-setup-udeb.templates:33
msgid ""
"The security updates on ${SECURITY_HOST} couldn't be accessed, so those "
"updates will not be made available to you at this time. You should "
"investigate this later."
msgstr ""
"Non se puido acceder ás actualizacións de seguridade de ${SECURITY_HOST}, "
"así que non se lle han deixar dispoñibles de momento. Debería investigar "
"este problema máis tarde."

#. Type: error
#. Description
#: ../apt-setup-udeb.templates:33
msgid ""
"Commented out entries for ${SECURITY_HOST} have been added to the /etc/apt/"
"sources.list file."
msgstr ""
"Engadíronse entradas comentadas para ${SECURITY_HOST} no ficheiro /etc/apt/"
"sources.list"

#. Type: text
#. Description
#: ../apt-cdrom-setup.templates:3
msgid "Scanning the CD-ROM..."
msgstr "A examinar o CD-ROM..."

#. Type: error
#. Description
#: ../apt-cdrom-setup.templates:7
msgid "apt configuration problem"
msgstr "Problema de configuración de apt"

#. Type: error
#. Description
#: ../apt-cdrom-setup.templates:7
msgid ""
"An attempt to configure apt to install additional packages from the CD "
"failed."
msgstr ""
"Fallou unha tentativa de configurar apt para que instale paquetes adicionais "
"do CD."

#. Type: text
#. Description
#: ../apt-mirror-setup.templates:3
msgid "Scanning the mirror..."
msgstr "A examinar a réplica..."

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:8
msgid "Use non-free software?"
msgstr "¿Empregar software non libre?"

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:8
msgid ""
"Some non-free software has been made to work with Debian. Though this "
"software is not at all a part of Debian, standard Debian tools can be used "
"to install it. This software has varying licenses which may prevent you from "
"using, modifying, or sharing it."
msgstr ""
"Hai algún software non libre que se fixo funcionar con Debian. Aínda que "
"este software non é para nada parte de Debian, pódense empregar as "
"ferramentas estándar de Debian para o instalar. Este software ten varias "
"licencias que poden impedir que o use, modifique ou comparta."

#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates:8 ../apt-mirror-setup.templates-ubuntu:5
msgid "Please choose whether you want to have it available anyway."
msgstr "Indique se o quere ter dispoñible igualmente."

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:19
msgid "Use contrib software?"
msgstr "¿Empregar software contribuído?"

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:19
msgid ""
"Some additional software has been made to work with Debian. Though this "
"software is free, it depends on non-free software for its operation. This "
"software is not a part of Debian, but standard Debian tools can be used to "
"install it."
msgstr ""
"Hai algún software adicional que se fixo funcionar con Debian. Aínda que "
"este software é libre, depende de software non libre para o seu "
"funcionamento. Este software non é para nada parte de Debian, pero pódense "
"empregar ferramentas estándar de Debian para o instalar."

#. Type: boolean
#. Description
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates:19 ../apt-mirror-setup.templates-ubuntu:41
msgid ""
"Please choose whether you want this software to be made available to you."
msgstr "Indique se quere que este software estea dispoñible."

#. Type: select
#. Choices
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:32
msgid "Retry"
msgstr "Volver tentalo"

#. Type: select
#. Choices
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:32
msgid "Change mirror"
msgstr "Cambiar a réplica"

#. Type: select
#. Choices
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:32
msgid "Ignore"
msgstr "Ignorar"

#. Type: select
#. Description
#: ../apt-mirror-setup.templates:33
msgid "Downloading a file failed:"
msgstr "Non se puido descargar un ficheiro:"

#. Type: select
#. Description
#: ../apt-mirror-setup.templates:33
msgid ""
"The installer failed to access the mirror. This may be a problem with your "
"network, or with the mirror. You can choose to retry the download, select a "
"different mirror, or ignore the problem and continue without all the "
"packages from this mirror."
msgstr ""
"O instalador non puido acceder á réplica. Isto pode ser por un problema coa "
"rede ou coa réplica. Pode volver tentar a descarga, escoller unha réplica "
"diferente, ou ignorar o problema e continuar sen os paquetes da réplica."

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:42
msgid "Use a network mirror?"
msgstr "¿Empregar unha réplica na rede?"

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:42
msgid ""
"A network mirror can be used to supplement the software that is included on "
"the CD-ROM. This may also make newer versions of software available."
msgstr ""
"Pódese empregar unha réplica na rede para suplementar o software incluído no "
"CD-ROM. Tamén pode facer dispoñibles novas versións do software."

#. Type: boolean
#. Description
#: ../apt-mirror-setup.templates:42
msgid ""
"If you are installing from a netinst CD and you choose not to use a mirror, "
"you will end up with only a very minimal base system."
msgstr ""
"Se instala dun CD netinst e decide non empregar unha réplica, só ha ter un "
"sistema base moi mínimo."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:5
msgid "Use restricted software?"
msgstr "¿Empregar software restrinxido?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:5
msgid ""
"Some non-free software is available in packaged form. Though this software "
"is not a part of the main distribution, standard package management tools "
"can be used to install it. This software has varying licenses which may "
"prevent you from using, modifying, or sharing it."
msgstr ""
"Hai algún software non libre empaquetado. Aínda que este software non forma "
"parte da distribución principal, pódense empregar as ferramentas de xestión "
"de paquetes estándar para o instalar. Este software ten varias licencias que "
"poden impedirlle empregalo, modificalo ou compartilo."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:17
msgid "Use software from the \"universe\" component?"
msgstr "¿Empregar software do compoñente \"universe\"?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:17
msgid ""
"Some additional software is available in packaged form. This software is "
"free and, though it is not a part of the main distribution, standard package "
"management tools can be used to install it."
msgstr ""
"Hai algún software adicional empaquetado. Este software é libre e, aínda que "
"non forma parte da distribución principal, pódense empregar as ferramentas "
"de xestión de paquetes estándar para o instalar."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:28
msgid "Use software from the \"multiverse\" component?"
msgstr "¿Empregar software do compoñente \"multiverse\"?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:28
msgid ""
"Some non-free software is available in packaged form. Though this software "
"is not a part of the main distribution, standard package management tools "
"can be used to install it. This software has varying licenses and (in some "
"cases) patent restrictions which may prevent you from using, modifying, or "
"sharing it."
msgstr ""
"Hai algún software non libre empaquetado. Aínda que este software non forma "
"parte da distribución principal, pódense empregar as ferramentas de xestión "
"de paquetes estándar para o instalar. Este software ten varias licencias e "
"(nalgúns casos) restricións por patentes que poden impedirlle empregalo, "
"modificalo ou compartilo."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:41
msgid "Use backported software?"
msgstr "¿Empregar software retrotraído?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#: ../apt-mirror-setup.templates-ubuntu:41
msgid ""
"Some software has been backported from the development tree to work with "
"this release. Although this software has not gone through such complete "
"testing as that contained in the release, it includes newer versions of some "
"applications which may provide useful features."
msgstr ""
"Algún software tróuxose da árbore de desenvolvemento para que funcione con "
"esta versión. Aínda que este software non pasou por unhas probas tan "
"completas coma o incluído na distribución, inclúe versións máis recentes "
"dalgunhas aplicacións que poden fornecer características útiles."
