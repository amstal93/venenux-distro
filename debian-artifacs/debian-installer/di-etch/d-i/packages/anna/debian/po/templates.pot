# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#: ../anna.templates:4
msgid ""
"All components of the installer needed to complete the install will be "
"loaded automatically and are not listed here. Some other (optional) "
"installer components are shown below. They are probably not necessary, but "
"may be interesting to some users."
msgstr ""

#. Type: multiselect
#. Description
#: ../anna.templates:16
msgid "Installer components to load:"
msgstr ""

#. Type: multiselect
#. Description
#: ../anna.templates:16
msgid ""
"To save memory, only components that are certainly needed for an install are "
"selected by default. The other installer components are not all necessary "
"for a basic install, but you may need some of them, especially certain "
"kernel modules, so look through the list carefully and select the components "
"you need."
msgstr ""

#. Type: multiselect
#. Description
#: ../anna.templates:16
msgid ""
"Note that if you select a component that requires others, those components "
"will also be loaded."
msgstr ""

#. Type: text
#. Description
#. (Progress bar) title displayed when loading udebs
#. TRANSLATORS : keep short
#: ../anna.templates:30
msgid "Loading additional components"
msgstr ""

#. Type: text
#. Description
#. (Progress bar)
#. TRANSLATORS : keep short
#: ../anna.templates:36
msgid "Retrieving ${PACKAGE}"
msgstr ""

#. Type: error
#. Description
#: ../anna.templates:40
msgid "Failed to load installer component"
msgstr ""

#. Type: error
#. Description
#: ../anna.templates:40
msgid "Loading ${PACKAGE} failed for unknown reasons. Aborting."
msgstr ""

#. Type: boolean
#. Description
#: ../anna.templates:46
msgid "Continue the install without loading kernel modules?"
msgstr ""

#. Type: boolean
#. Description
#: ../anna.templates:46
msgid ""
"No kernel modules were found. This probably is due to a mismatch between the "
"kernel used by this version of the installer and the kernel version "
"available in the archive."
msgstr ""

#. Type: boolean
#. Description
#: ../anna.templates:46
msgid ""
"If you're installing from a mirror, you can work around this problem by "
"choosing to install a different version of Debian. The install will probably "
"fail to work if you continue without kernel modules."
msgstr ""
