# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:4
msgid "Partition for boot loader installation:"
msgstr ""

#. Type: select
#. Description
#: ../elilo-installer.templates:4
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""

#. Type: error
#. Description
#: ../elilo-installer.templates:10
msgid "No boot partitions detected"
msgstr ""

#. Type: error
#. Description
#: ../elilo-installer.templates:10
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:17
msgid "Install the elilo boot loader on a hard disk"
msgstr ""

#. Type: text
#. Description
#: ../elilo-installer.templates:21
msgid "Installing the ELILO package"
msgstr ""

#. Type: text
#. Description
#: ../elilo-installer.templates:25
msgid "Running ELILO for ${bootdev}"
msgstr ""

#. Type: boolean
#. Description
#: ../elilo-installer.templates:30
msgid "ELILO installation failed.  Continue anyway?"
msgstr ""

#. Type: boolean
#. Description
#: ../elilo-installer.templates:30
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""

#. Type: error
#. Description
#: ../elilo-installer.templates:37
msgid "ELILO installation failed"
msgstr ""

#. Type: error
#. Description
#: ../elilo-installer.templates:37
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr ""
