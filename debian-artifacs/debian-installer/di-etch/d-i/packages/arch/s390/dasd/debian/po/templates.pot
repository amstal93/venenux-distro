# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. TRANSLATORS, please translate "Finish" the same way you translate it in
#. the " Select "Finish" at the bottom of the list when you are done" string
#: ../s390-dasd.templates:6
msgid "Finish"
msgstr ""

#. Type: select
#. Description
#: ../s390-dasd.templates:7
msgid "Available disks:"
msgstr ""

#. Type: select
#. Description
#: ../s390-dasd.templates:7
msgid ""
"The following disk access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""

#. Type: select
#. Description
#: ../s390-dasd.templates:7
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr ""

#. Type: string
#. Description
#: ../s390-dasd.templates:15
msgid "Choose disk:"
msgstr ""

#. Type: string
#. Description
#: ../s390-dasd.templates:15
msgid ""
"Please choose a disk. You have to specify the complete device number, "
"including leading zeros."
msgstr ""

#. Type: error
#. Description
#: ../s390-dasd.templates:21
msgid "Invalid disk"
msgstr ""

#. Type: error
#. Description
#: ../s390-dasd.templates:21
msgid "An invalid device number has been chosen."
msgstr ""

#. Type: boolean
#. Description
#: ../s390-dasd.templates:26
msgid "Format the disk?"
msgstr ""

#. Type: boolean
#. Description
#: ../s390-dasd.templates:26
msgid ""
"The installer is unable to detect if the device ${device} has already been "
"formatted or not. Disks need to be formatted before you can create "
"partitions."
msgstr ""

#. Type: boolean
#. Description
#: ../s390-dasd.templates:26
msgid ""
"If you are sure the disk has already been correctly formatted, you don't "
"need to do so again."
msgstr ""

#. Type: text
#. Description
#: ../s390-dasd.templates:36
msgid "Formatting ${device}..."
msgstr ""

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#: ../s390-dasd.templates:41
msgid "Configure disk access storage devices (DASD)"
msgstr ""
