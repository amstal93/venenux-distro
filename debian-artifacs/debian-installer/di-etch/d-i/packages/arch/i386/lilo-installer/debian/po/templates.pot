# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../lilo-installer.templates:6
msgid "${disc}: Master Boot Record"
msgstr ""

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../lilo-installer.templates:6
msgid "${part}: new Debian partition"
msgstr ""

#. Type: select
#. Description
#: ../lilo-installer.templates:7
msgid ""
"The LILO program needs to be installed to make your new system bootable. By "
"installing it onto your disk's Master Boot Record, LILO will take complete "
"control of the boot process, but if you want to use a different boot "
"manager, just install LILO on the new Debian partition instead."
msgstr ""

#. Type: select
#. Description
#: ../lilo-installer.templates:7
msgid "If unsure, install LILO into the Master Boot Record."
msgstr ""

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../lilo-installer.templates:20
msgid "${disc}: software RAID array"
msgstr ""

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../lilo-installer.templates:20
msgid "Other choice (Advanced)"
msgstr ""

#. Type: select
#. Description
#: ../lilo-installer.templates:21
msgid ""
"The LILO program needs to be installed to make your new system bootable. You "
"may choose to install it onto a software RAID array or another device."
msgstr ""

#. Type: string
#. Description
#: ../lilo-installer.templates:29
msgid "LILO installation target:"
msgstr ""

#. Type: string
#. Description
#: ../lilo-installer.templates:29
msgid ""
"Please enter the partition or disk onto which LILO should be installed. "
"Devfs style names, such as those that start with /dev/ide, /dev/scsi, and /"
"dev/discs are accepted, as well as traditional names, such as /dev/hda or /"
"dev/sda."
msgstr ""

#. Type: error
#. Description
#: ../lilo-installer.templates:37
msgid "Invalid partition name"
msgstr ""

#. Type: error
#. Description
#: ../lilo-installer.templates:37
msgid ""
"The path ${path} does not represent a partition or hard disk device. Please "
"try again."
msgstr ""

#. Type: text
#. Description
#: ../lilo-installer.templates:43
msgid "Installing LILO..."
msgstr ""

#. Type: text
#. Description
#: ../lilo-installer.templates:47
msgid "Activating partition ${bootdev}"
msgstr ""

#. Type: text
#. Description
#: ../lilo-installer.templates:51
msgid "Creating lilo.conf"
msgstr ""

#. Type: text
#. Description
#: ../lilo-installer.templates:55
msgid "Installing the LILO package"
msgstr ""

#. Type: text
#. Description
#: ../lilo-installer.templates:59
msgid "Running LILO for ${bootdev}"
msgstr ""

#. Type: note
#. Description
#: ../lilo-installer.templates:63
msgid "LILO configured to use a serial console"
msgstr ""

#. Type: note
#. Description
#: ../lilo-installer.templates:63
msgid ""
"LILO is configured to use serial port ${PORT} as the console. The serial "
"port speed is set to ${SPEED}."
msgstr ""

#. Type: boolean
#. Description
#: ../lilo-installer.templates:70
msgid "Would you like to make this partition active?"
msgstr ""

#. Type: boolean
#. Description
#: ../lilo-installer.templates:70
msgid ""
"You have chosen to install LILO on a partition that is not the active one. "
"If this partition is not marked active, then LILO will not be loaded by the "
"boot loader.  This may cause you to be unable to boot into the system that "
"is being installed."
msgstr ""

#. Type: boolean
#. Description
#: ../lilo-installer.templates:70
msgid ""
"You should make this partition active unless you have another boot loader "
"that will allow you to access your new Linux installation."
msgstr ""

#. Type: boolean
#. Description
#: ../lilo-installer.templates:82
msgid "LILO installation failed.  Continue anyway?"
msgstr ""

#. Type: boolean
#. Description
#: ../lilo-installer.templates:82
msgid ""
"The lilo package failed to install into /target/.  Installing LILO as a boot "
"loader is a required step.  The install problem might however be unrelated "
"to LILO, so continuing the installation may be possible."
msgstr ""

#. Type: error
#. Description
#: ../lilo-installer.templates:89
msgid "LILO installation failed"
msgstr ""

#. Type: error
#. Description
#: ../lilo-installer.templates:89
msgid "Running \"/sbin/lilo\" failed with error code \"${ERRCODE}\"."
msgstr ""

#. Type: text
#. Description
#. Main menu item
#: ../lilo-installer.templates:95
msgid "Install the LILO boot loader on a hard disk"
msgstr ""
