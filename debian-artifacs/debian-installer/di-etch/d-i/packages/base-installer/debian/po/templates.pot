# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../base-installer.templates:4
msgid "Proceed with installation to unclean target?"
msgstr ""

#. Type: boolean
#. Description
#: ../base-installer.templates:4
msgid ""
"The target file system contains files from a past installation. These files "
"could cause problems with the installation process, and if you proceed, some "
"of the existing files may be overwritten."
msgstr ""

#. Type: error
#. Description
#. The base system is the minimal Debian system
#. See http://www.debian.org/doc/debian-policy/ch-binary.html#s3.7
#: ../base-installer.templates:13
msgid "Cannot install base system"
msgstr ""

#. Type: error
#. Description
#. The base system is the minimal Debian system
#. See http://www.debian.org/doc/debian-policy/ch-binary.html#s3.7
#: ../base-installer.templates:13
msgid ""
"The installer cannot figure out how to install the base system. No "
"installable CD-ROM was found and no valid mirror was configured."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:19
msgid "Failed to determine the codename for the release."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:24
msgid "No file system mounted on /target"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:24
msgid ""
"Before the installation can proceed, a root file system must be mounted on /"
"target. The partitioner and formatter should have done this for you."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:30
msgid "Not installing to unclean target"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:30
msgid ""
"The installation to the target file system has been canceled. You should go "
"back and erase or format the target file system before proceeding with the "
"install."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:37
msgid "Failed to install the base system"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:37
msgid "The base system installation into /target/ failed."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:44
msgid ""
"The debootstrap program exited with an error (return value ${EXITCODE})."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:51
msgid "Base system installation error"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:51
msgid "The debootstrap program exited abnormally."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:59
msgid "The following error occurred:"
msgstr ""

#. Type: select
#. Description
#: ../base-installer.templates:69
msgid "Tool to use to generate boot initrd:"
msgstr ""

#. Type: select
#. Description
#: ../base-installer.templates:69
msgid ""
"The list shows the available tools. If you are unsure which to select, you "
"should select the default. If your system fails to boot, you can retry the "
"installation using the other options."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:76
msgid "No initrd generator"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:76
msgid ""
"The kernel that has been selected for installation needs an initrd to boot. "
"However, no package to generate that initrd is available that supports your "
"configuration. This would leave your system unable to boot."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:83
msgid "Unsupported initrd generator"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:83
msgid ""
"The package ${GENERATOR} that was selected to generate the initrd is not "
"supported."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:89
msgid "Unable to install the selected kernel"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:89
msgid ""
"An error was returned while trying to install the kernel into the target "
"system."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:89
msgid "Kernel package: '${KERNEL}'."
msgstr ""

#. Type: select
#. Choices
#. For base-installer, "none" is an additional choice to a list of kernels.
#. It means "no kernel"
#: ../base-installer.templates:102
msgid ""
"none[ Do not translate what's inside the brackets and just put the "
"translation for the word \"none\" in your language without any brackets. "
"This \"none\" means \"no kernel\" ]"
msgstr ""

#. Type: select
#. Description
#: ../base-installer.templates:103
msgid "Kernel to install:"
msgstr ""

#. Type: select
#. Description
#: ../base-installer.templates:103
msgid ""
"The list shows the available kernels. Please choose one of them in order to "
"make the system bootable from the hard drive."
msgstr ""

#. Type: boolean
#. Description
#: ../base-installer.templates:110
msgid "Continue without installing a kernel?"
msgstr ""

#. Type: boolean
#. Description
#: ../base-installer.templates:110
msgid "No installable kernel was found in the defined APT sources."
msgstr ""

#. Type: boolean
#. Description
#: ../base-installer.templates:110
msgid ""
"You may try to continue without a kernel, and manually install your own "
"kernel later. This is only recommended for experts, otherwise you will "
"likely end up with a machine that doesn't boot."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:119
msgid "Cannot install kernel"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:119
msgid "The installer cannot find a suitable kernel package to install."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:124
msgid "Unable to install ${PACKAGE}"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:124
msgid ""
"An error was returned while trying to install the ${PACKAGE} package onto "
"the target system."
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:124
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a Release file name.
#: ../base-installer.templates:133
msgid "Failed getting Release file ${SUBST0}."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a Release.gpg file name
#: ../base-installer.templates:139
msgid "Failed getting Release signature file ${SUBST0}."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a gpg key ID
#: ../base-installer.templates:145
msgid "Release file signed by unknown key (key id ${SUBST0})"
msgstr ""

#. Type: error
#. Description
#: ../base-installer.templates:150
msgid "Invalid Release file: no valid components."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a filename
#: ../base-installer.templates:156
msgid "Invalid Release file: no entry for ${SUBST0}."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a filename or package name
#. Debootstrap is a program name: should not be translated
#: ../base-installer.templates:163
msgid "Debootstrap Error"
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a filename or package name
#. Debootstrap is a program name: should not be translated
#: ../base-installer.templates:163
msgid ""
"Couldn't retrieve ${SUBST0}. This may be due to a network problem or a bad "
"CD, depending on your installation method."
msgstr ""

#. Type: error
#. Description
#. SUBST0 is a filename or package name
#. Debootstrap is a program name: should not be translated
#: ../base-installer.templates:163
msgid ""
"If you are installing from CD-R or CD-RW, burning the CD at a lower speed "
"may help."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:176
msgid "Preparing to install the base system..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:180
msgid "Running ${SCRIPT}..."
msgstr ""

#. Type: text
#. Description
#. Release is a filename which should not be translated
#: ../base-installer.templates:185
msgid "Retrieving Release file"
msgstr ""

#. Type: text
#. Description
#. Release is a filename which should not be translated
#: ../base-installer.templates:190
msgid "Retrieving Release file signature"
msgstr ""

#. Type: text
#. Description
#. "packages" here can be translated
#: ../base-installer.templates:195
msgid "Finding package sizes"
msgstr ""

#. Type: text
#. Description
#. Packages is a filename which should not be translated
#: ../base-installer.templates:200
msgid "Retrieving Packages files"
msgstr ""

#. Type: text
#. Description
#. Packages is a filename which should not be translated
#: ../base-installer.templates:205
msgid "Retrieving Packages file"
msgstr ""

#. Type: text
#. Description
#. "packages" here can be translated
#: ../base-installer.templates:210
msgid "Retrieving packages"
msgstr ""

#. Type: text
#. Description
#. "packages" here can be translated
#: ../base-installer.templates:215
msgid "Extracting packages"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:219
msgid "Installing the base system"
msgstr ""

#. Type: text
#. Description
#. Core packages are packages that are part of the Debian base system
#. The "core" packages are indeed packages that are specifically
#. recorded as part of the base system. Other packages may
#. be installed on the base system because of dependency resolution
#: ../base-installer.templates:227
msgid "Installing core packages"
msgstr ""

#. Type: text
#. Description
#. Required packages are packages which installation is triggered
#. by the dependency chain of core packages
#. In short, they are "required" because at least one of the
#. packages from the core packages depends on them
#: ../base-installer.templates:235
msgid "Unpacking required packages"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:239
msgid "Configuring required packages"
msgstr ""

#. Type: text
#. Description
#. The base system is the minimal Debian system
#. See http://www.debian.org/doc/debian-policy/ch-binary.html#s3.7
#: ../base-installer.templates:245
msgid "Unpacking the base system"
msgstr ""

#. Type: text
#. Description
#. The base system is the minimal Debian system
#. See http://www.debian.org/doc/debian-policy/ch-binary.html#s3.7
#: ../base-installer.templates:251
msgid "Configuring the base system"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:255
msgid "${SECTION}: ${INFO}..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:260
msgid "Validating ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:265
msgid "Retrieving ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:270
msgid "Extracting ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:275
msgid "Unpacking ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:280
msgid "Configuring ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#. Release is a filename which should not be translated
#: ../base-installer.templates:285
msgid "Checking Release signature"
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a gpg key id
#. Release is a filename which should not be translated
#: ../base-installer.templates:291
msgid "Valid Release signature (key id ${SUBST0})"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:295
msgid "Resolving dependencies of base packages..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a list of packages
#: ../base-installer.templates:300
msgid "Found additional base dependencies: ${SUBST0}"
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a list of packages
#: ../base-installer.templates:305
msgid "Found additional required dependencies: ${SUBST0}"
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a list of packages
#: ../base-installer.templates:310
msgid "Found packages in base already in required: ${SUBST0}"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:314
msgid "Resolving dependencies of required packages..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is an archive component (main, etc)
#. SUBST1 is a mirror
#: ../base-installer.templates:320
msgid "Checking component ${SUBST0} on ${SUBST1}..."
msgstr ""

#. Type: text
#. Description
#. Core packages are packages that are part of the Debian base system
#. The "core" packages are indeed packages that are specifically
#. recorded as part of the base system. Other packages may
#. be installed on the base system because of dependency resolution
#: ../base-installer.templates:328
msgid "Installing core packages..."
msgstr ""

#. Type: text
#. Description
#. Required packages are packages which installation is triggered
#. by the dependency chain of core packages
#. In short, they are "required" because at least one of the
#. packages from the core packages depends on them
#: ../base-installer.templates:336
msgid "Unpacking required packages..."
msgstr ""

#. Type: text
#. Description
#. Required packages are packages which installation is triggered
#. by the dependency chain of core packages
#. In short, they are "required" because at least one of the
#. packages from the core packages depends on them
#: ../base-installer.templates:344
msgid "Configuring required packages..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:348
msgid "Installing base packages..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:352
msgid "Unpacking the base system..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:356
msgid "Configuring the base system..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:360
msgid "Base system installed successfully."
msgstr ""

#. Type: error
#. Description
#. Debootstrap is a program name: should not be translated
#: ../base-installer.templates:365
msgid "Debootstrap warning"
msgstr ""

#. Type: error
#. Description
#. Debootstrap is a program name: should not be translated
#: ../base-installer.templates:365
msgid "Warning: ${INFO}"
msgstr ""

#. Type: text
#. Description
#. SUBST0 is an url
#: ../base-installer.templates:371
msgid "Retrying failed download of ${SUBST0}"
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:375
msgid "Configuring APT sources..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:379
msgid "Updating the list of available packages..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:383
msgid "Installing extra packages..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:388
msgid "Installing extra packages - retrieving and installing ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:392
msgid "Creating device files..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:396
msgid "Selecting the kernel to install..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:400
msgid "Installing the kernel..."
msgstr ""

#. Type: text
#. Description
#. SUBST0 is a package name
#: ../base-installer.templates:405
msgid "Installing the kernel - retrieving and installing ${SUBST0}..."
msgstr ""

#. Type: text
#. Description
#: ../base-installer.templates:409
msgid "Installing the PCMCIA modules"
msgstr ""

#. Type: text
#. Description
#. Item in the main menu to select this package
#. TRANSLATORS: <65 columns
#: ../base-installer.templates:415
msgid "Install the base system"
msgstr ""
