#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

CFLAGS = -Wall -W -Os

ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif

export CFLAGS

config.status:
	dh_testdir

	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-small \
		--enable-lvm

build: build-stamp
build-stamp: config.status
	dh_testdir

	$(MAKE)

	touch $@

clean:
	dh_testdir
	dh_testroot

	rm -f build-stamp
	rm -f debian/autopartkit.postinst

	-$(MAKE) distclean

	$(RM) config.status

	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_installdirs etc/autopartkit usr/bin
	$(MAKE) install DESTDIR=$(CURDIR)/debian/autopartkit

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_strip
	dh_compress
	dh_fixperms
	dh_install debian/isinstallable DEBIAN
	dh_installdeb 
	dh_shlibdeps
	dh_gencontrol
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
