# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../autopartkit.templates:11
msgid ""
"Please choose the disk which has the free space required (column Free) to "
"install Debian GNU/Linux. If there's not enough free space, space can be "
"liberated by resizing the FAT partitions (column FreeFat indicates how much "
"space will be freed by resizing the existing FAT partitions)."
msgstr ""

#. Type: select
#. Description
#. This should be hard-formatted THE SAME WAY in translations and
#. original English
#. Example for French:
#. msgid  " Device   Model               Size   Free   FreeFAT NbPart"
#. msgstr " Periph.  Modele              Taille Libre  Fat-lib N Part"
#: ../autopartkit.templates:11
msgid " Device   Model               Size   Free   FreeFAT NbPart"
msgstr ""

#. Type: string
#. Description
#: ../autopartkit.templates:24
msgid "Device to partition:"
msgstr ""

#. Type: string
#. Description
#: ../autopartkit.templates:24
msgid ""
"No disk was automatically detected.  Please enter the path to the device "
"file for the disk you want to partition."
msgstr ""

#. Type: boolean
#. Description
#: ../autopartkit.templates:31
msgid "Really use the automatic partitioning tool?"
msgstr ""

#. Type: boolean
#. Description
#: ../autopartkit.templates:31
msgid ""
"This will destroy the partition table on all disks in the machine. REPEAT: "
"THIS WILL WIPE CLEAN ALL HARD DISKS IN THE MACHINE! If you have important "
"data that are not backed up, you may want to stop now in order to do a "
"backup. In that case, you'll have to restart the installation later."
msgstr ""

#. Type: error
#. Description
#: ../autopartkit.templates:41
msgid "An error occurred during the previous operation"
msgstr ""

#. Type: error
#. Description
#: ../autopartkit.templates:41
msgid "You may be able to continue. Here is some more information:"
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:48
msgid "Automatic partitioning impossible"
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:48
msgid ""
"This version of the autopartitioner cannot handle the partitioning of the "
"selected disk. It may partition only an empty disk or a disk with no more "
"than 2 FAT partitions (and no extended partitions or other non-FAT "
"partitions)."
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:48
msgid ""
"If you want to continue the installation process, switch to the second "
"console (ALT+F2), partition the disk as you like using parted and the other "
"available tools, and then mount all your partitions in /target. /target is "
"the root directory of your new system, so your root partition will be "
"mounted on /target and your /usr partition must be mounted on /target/usr."
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:48
msgid ""
"You can also consider launching this program again but selecting another "
"disk."
msgstr ""

#. Type: error
#. Description
#: ../autopartkit.templates:66
msgid "Not enough space on disk"
msgstr ""

#. Type: error
#. Description
#: ../autopartkit.templates:66
msgid ""
"The autopartitioner makes some assumptions about partition sizes, and "
"because of that it requires at least 3GB of free space. The free space on "
"this disk appears to be below this limit."
msgstr ""

#. Type: error
#. Description
#: ../autopartkit.templates:66
msgid ""
"If you really want to install, you'll have to free some space. You may also "
"launch this program again and select another disk with more free space."
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:78
msgid "Debug information about ${variable}"
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:78
msgid "Here is some debug information. The value of ${variable} is:"
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:85
msgid "Successful partitioning"
msgstr ""

#. Type: note
#. Description
#: ../autopartkit.templates:85
msgid ""
"The required partitions have been created and mounted on /target. You can "
"now continue the installation process."
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:92
msgid "File containing the requested partition table:"
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:92
msgid ""
"The automatic partitioning will be created using the partitions and sizes "
"specified in this file."
msgstr ""

#. Type: text
#. Description
#. Item in the main menu to select this package
#: ../autopartkit.templates:99
msgid "Automatically partition hard drives"
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:103
msgid "Creating '${FSTYPE}' storage space on '${MOUNTPOINT}'"
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:107
msgid "Estimated time left: ${HOURS} hour(s) ${MINUTES} minute(s)"
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:111
msgid "Estimated time left: ${MINUTES} minute(s)"
msgstr ""

#. Type: text
#. Description
#: ../autopartkit.templates:115
msgid "Estimated time left: less than 1 minute"
msgstr ""
