/*
* WPA module for netcfg
*
* Copyright (C) 2008 Glenn Saberton <gsaberton@foomagic.org>
*
* Licensed under the terms of the GNU General Public License version 2
*
*/

#include "netcfg.h"
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <debian-installer.h>

pid_t wpa_supplicant_pid = -1;
int requested_wpa_supplicant = 0;

/*
* Build an /etc/wpa_supplicant.conf
* file.
*/

int netcfg_write_wpa (char *essid, char *passphrase)
{
    FILE *fp;

    if ((fp = file_open (WPASUPP_CONF, "w"))) {
        fprintf (fp, "\n# wpa_supplicant.conf generated during install\n"
      		     "ctrl_interface=/var/run/wpa_supplicant\n"
    		     "ctrl_interface_group=0\n"
    		     "eapol_version=1\n"
    		     "ap_scan=1\n\n"	/* So we can associate to hidden ssid's  */
	    	     "network={\n");
        fprintf (fp, "\t\tssid=\"%s\"\n", essid);
        fprintf (fp, "\t\tpsk=\"%s\"\n", passphrase);
        fprintf (fp, "\t\tscan_ssid=1\n" "}\n");
    }
    fclose (fp);
    return 0;
}

int have_wpa_supplicant(void)
{
    extern int requested_wpa_supplicant;

    if (access("/sbin/wpa_supplicant", F_OK) == 0)
        requested_wpa_supplicant = 0;
    else {
        requested_wpa_supplicant = 2;
        di_info("Wpasupplicant not found on the system. Disabling WPA options");
    }
    return 0;
}

int kill_wpa_supplicant(void)
{
    int ret;
    pid_t wpa_pid;
    FILE *fp;

    fp = (file_open (WPAPID, "r"));
    if (fp == NULL) {
        di_warning ("Couldn't read Wpasupplicant pid file, not trying to kill.");
        return 0;
    }
    else {
        fscanf (fp, "%d", &wpa_pid);
        fclose (fp);
    }
      if ((ret = kill(wpa_pid, SIGTERM)) == 0)
          return 0;
      else {
          kill(wpa_pid, SIGKILL);
          return 0;
      }
}

int start_wpa_supplicant (struct debconfclient *client)
{
    wpa_supplicant_pid = fork();

    if (wpa_supplicant_pid == 0) {
        fclose(client->out);
        if (execlp ("wpa_supplicant", "wpa_supplicant", "-i", interface, "-c",
                    WPASUPP_CONF, "-P", WPAPID, "-B", NULL) == -1) {
            di_error("could not exec wpasupplicant: %s", strerror(errno));
            return 1;
        }
        else
            return 0;
    }
    else {
        waitpid(wpa_supplicant_pid, NULL, 0);
        return 0;
    }
}
