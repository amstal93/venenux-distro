This udeb modifies the Etch installer to support Sarge installations.
It reintruduces some functionality that was dropped for Etch but is needed
when installing Sarge.
Note that the aim is not to perfectly reproduce a Sarge install, just to get
things working.

The udeb will be installed automatically by either cdrom-retriever,
net-retriever or iso-scan if Sarge is selected for installation.

FEATURES
========
- if network-console is not used, inittab is set up to start base-config
  on reboot
- if network-console is used, the 'installer' user is set up in the target
  system; base-config is run on (remote) login using this user
- installs modified termwrap because the default one has limited UTF-8 support
- create /var/log/debian-installer symlink to /var/log/installer
- disable 'hotplug' networking configuration for eth0 as otherwise it will
  probably not be brought up on boot
- provides a "quirks" script where workarounds can be implemented for
  basically any regression, as long as it can be strictly identified

DIFFERENCES
===========
There are some notable differences with a real Sarge install.
Note: this list is i386 centric.
- udev will be installed by default when using 2.6 based install
- environment will be set up to use UTF-8 by default (which is not extremely
  well supported in Sarge); this may be avoided by running localechooser at
  medium priority and selecting a non-UTF-8 locale as default

TODO
====
- base-installer needs to be fixed so it will fall back to initrd-tools if
  neither yaird or initramfs-tools are available (maybe with a kernel version
  check)
- base-installer selects smp kernel by default for 2.6.8 kernel (vmware, 686)
- for some arches kernel selection may be completely broken for Sarge
  installations due to 2.4->2.6 switch; maybe even explicitly not support these
- clock setup (UTC/local), timezone iselection and apt-setup are run
  unconditionally, so these should be disabled in 1st stage
- functionality for network-config installs is untested
- maybe add a postinst and templates with a disclaimer and with checks and
  warnings for situations that are known not to be supported
