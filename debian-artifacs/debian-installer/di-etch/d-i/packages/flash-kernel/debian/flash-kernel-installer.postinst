#!/bin/sh
set -e

. /usr/share/debconf/confmodule

log() {
	logger -t flash-kernel-installer "$@"
}

error() {
	log "error: $@"
}

db_progress START 0 2 flash-kernel-installer/progress
db_progress INFO flash-kernel-installer/prepare

apt-install initramfs-tools || true # probably already installed; make sure

if ! apt-install flash-kernel; then
	error "apt-install flash-kernel failed"
	exit 1
fi

# Set up flash-kernel to run on kernel upgrades.
if ! grep -q flash-kernel /target/etc/kernel-img.conf; then
	echo "postinst_hook = flash-kernel" >> \
		/target/etc/kernel-img.conf
fi

machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
case "$machine" in
	"Linksys NSLU2")
		for i in NPE-A NPE-B NPE-C; do
			if [ -e /lib/firmware/$i ]; then
				log "Installing microcode $i"
				file=$(basename $(readlink /lib/firmware/$i))
				cp /lib/firmware/$file /target/lib/firmware/
				ln -s $file /target/lib/firmware/$i
			fi
		done
		if [ -e /lib/firmware/IxNpeMicrocode.dat ]; then
			cp /lib/firmware/IxNpeMicrocode.dat /target/lib/firmware/
		fi
		ixp400_found=0
		for file in $(find /lib/modules -type f | grep ixp400); do
			ixp400_found=1
			dir=$(dirname $file)
			mkdir -p /target/$dir
			cp -a $file /target/$file
		done
		if [ $ixp400_found -gt 0 ]; then
			log "copied ixp400 driver to /target"
			chroot /target depmod -a || true
		fi
		if ! apt-install nslu2-utils; then
			error "apt-install nslu2-utils failed"
			exit 1
		fi
		if ! apt-install apex-nslu2; then
			error "apt-install apex-nslu2 failed"
			exit 1
		fi
	;;
	"Thecus N2100" | "Thecus N4100")
		in-target update-initramfs -u || true
	;;
esac

db_progress STEP 1
db_progress INFO flash-kernel-installer/flashing

# We need the udev /dev which has the MTD devices
mount -o bind /dev /target/dev
if ! in-target flash-kernel; then
	umount /target/dev || true
	error "flash-kernel failed"
	exit 1
fi
umount /target/dev || true

db_progress STEP 1
db_progress STOP
