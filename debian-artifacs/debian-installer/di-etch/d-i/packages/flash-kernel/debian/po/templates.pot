# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. This item is a progress bar item when the system configures
#. some flashable memory used by many embedded devices
#: ../flash-kernel-installer.templates:5
msgid "Configuring flash memory to boot the system"
msgstr ""

#. Type: text
#. Description
#. This is "preparing the system" to flash the kernel and initrd
#. on a flashable memory
#: ../flash-kernel-installer.templates:11
msgid "Preparing the system"
msgstr ""

#. Type: text
#. Description
#. This is a progress bar showing up when the system
#. write the kernel to the flashable memory of the embedded device
#: ../flash-kernel-installer.templates:17
msgid "Writing the kernel to flash memory"
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. This item is a menu entry for a step where the system configures
#. the flashable memory used by many embedded devices
#. (writing the kernel and initrd to it)
#: ../flash-kernel-installer.templates:25
msgid "Configure flash memory to boot the system"
msgstr ""
