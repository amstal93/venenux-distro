partitioner (0.39) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Feb 2007 20:11:29 +0000

partitioner (0.38) unstable; urgency=low

  [ Frans Pop ]
  * Remove Thorsten Sauter from uploaders as requested by MIA team.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 01 Feb 2007 16:29:13 +0100

partitioner (0.37) unstable; urgency=low

  [ Colin Watson ]
  * Note that get_all_disks() should not be used as example code.

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Gujarati (gu.po) by Kartik Mistry
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Panjabi; Punjabi (pa.po) by A S Alam
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Colin Watson <cjwatson@debian.org>  Tue, 24 Oct 2006 19:30:00 +0100

partitioner (0.36) unstable; urgency=low

  * Fix m68k case (dh_gencontrol wasn't called for m68k ATM).

 -- Wouter Verhelst <wouter@debian.org>  Thu, 22 Jun 2006 14:17:39 +0200

partitioner (0.35) unstable; urgency=low

  [ Frans Pop ]
  * Improved description of disk selection template.

  [ Christian Perrier ]
  * Split Choices in debconf templates

  [ Martin Michlmayr ]
  * Add arm to the architecture field since RiscPC needs acorn-fdisk.

  [ Otavio Salvador ]
  * Change build-depends for new parted 1.7;
  * Remove Standards-Version since udebs don't follow Debian Policy;

  [ Bastian Blank ]
  * Remove s390 from architecture list. 

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Bastian Blank <waldi@debian.org>  Tue, 30 May 2006 22:29:43 +0200

partitioner (0.34) unstable; urgency=low

  [ Martin Michlmayr ]
  * The sb1-bcm91250a and sb1a-bcm91480b platforms use cfdisk.  In
    reality, these platforms use partman rather than partitioner
    anyway, though.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Hungarian (hu.po) by SZERVÑC Attila
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 17 Mar 2006 22:32:10 +0000

partitioner (0.33) unstable; urgency=low

  [ Updated translations ]
  * Greek (el.po) by Konstantinos Margaritis
  * Italian (it.po) by Giuseppe Sacco
  * Latvian (lv.po) by Aigars Mahinovs
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Fri, 27 Jan 2006 23:06:05 +0100

partitioner (0.32) unstable; urgency=low

  * Colin Watson
    - Build the architecture name into the binary rather than making a
      messy call to /usr/bin/udpkg --print-architecture (with hardcoded
      path!).
    - Unhardcode path to update-dev to allow a smoother transition to a
      generic /bin/update-dev that handles udev too.
  * Thiemo Seufer
    - Fix find use in debian/rules

 -- Thiemo Seufer <ths@debian.org>  Wed, 30 Nov 2005 22:04:22 +0000

partitioner (0.31) unstable; urgency=low

  * Frans Pop
    - Set correct dependency for libparted (1.6.24).

 -- Frans Pop <fjp@debian.org>  Sat, 17 Sep 2005 20:26:26 +0200

partitioner (0.30) unstable; urgency=low

  * Christian Perrier
    - Rename the templates file to help out translators working
      on a single file
  * Frans Pop
    - Update libparted build dependency.
    - Add myself to uploaders.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Greek (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Kurdish (ku.po) by Erdal Ronahi
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagazy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd

 -- Frans Pop <fjp@debian.org>  Sat, 17 Sep 2005 19:55:04 +0200

partitioner (0.29) unstable; urgency=low

  * Colin Watson
    - Explicitly exclude read-only devices from the output of
      get_all_disks(). As of parted 1.6.19, parted doesn't do this for us.
  * Thiemo Seufer
    - Update libparted build dependency.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide
    - Finnish (fi.po) by Tapio Lehtonen
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Italian (it.po) by Giuseppe Sacco
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor

 -- Thiemo Seufer <ths@debian.org>  Wed, 02 Feb 2005 18:21:30 +0100

partitioner (0.28) unstable; urgency=low

  * Joey Hess
    - Depend on fdisk-udeb for mips. Needed because it is not standard
      priority (because we cannot have differing priorities by arch),
      and so anna will not load it. This had been previously avoided by
      bloating the mips initrds with fdisk.
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - French (fr.po) by French Team

 -- Stephen R. Marenka <smarenka@debian.org>  Thu, 21 Oct 2004 07:31:23 -0500

partitioner (0.27) unstable; urgency=low

  * Joey Hess
    - Remove unnecessary seen flag fsetting.
  * Petter Reinholdtsen
    - Remove myself as uploader.  I'm no longer developing this package.
  * Updated translations:
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Å eÄeroviÄ
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino PeÃ±a
    - Basque (eu.po) by Piarres Beobide EgaÃ±a
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by KÄstutis BiliÅ«nasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjorn Steensrud
    - Norwegian (nn.po) by HÃ¥vard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by AndrÃ© LuÃ­s Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Äuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai OktaÅ
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Stephen R. Marenka <smarenka@debian.org>  Thu,  7 Oct 2004 07:33:30 -0500

partitioner (0.26) unstable; urgency=low

  * Stephen R. Marenka
    - m68k: make userdevfs support consistent.
    - add self to uploaders.
  * Updated translations: 
    - Danish (da.po) by Frederik Dannemare
    - Persian (fa.po) by Arash Bijanzadeh
    - Croatian (hr.po) by Kruno
    - Norwegian (nn.po) by HÃ¥vard Korsvoll

 -- Stephen R. Marenka <smarenka@debian.org>  Tue, 27 Jul 2004 13:07:12 -0500

partitioner (0.25) unstable; urgency=low

  * Colin Watson
    - Stop hardcoding path to archdetect.
    - Fix English in log message.
  * Stephen R. Marenka
    - Add support for q40 and sun(x) m68k subarchs.
  * Updated translations: 
    - German (de.po) by Alwin Meschede
    - Basque (eu.po) by Piarres Beobide EgaÃ±a
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Lithuanian (lt.po) by KÄstutis BiliÅ«nas
    - BÃ¸kmal, Norwegian (nb.po) by BjÃ¸rn Steensrud
    - Norwegian Nynorsk (nn.po) by HÃ¥vard Korsvoll
    - Russian (ru.po) by Nikolai Prokoschenko
    - Swedish (sv.po) by AndrÃ© Dahlqvist
    - Turkish (tr.po) by Osman YÃ¼ksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Sat, 15 May 2004 10:02:29 -0300

partitioner (0.24) unstable; urgency=low

  * Joey Hess
    - Only used on m68k, mips, and s390 anymore, so limit the list
      of arches accordingly.
  * Martin Michlmayr
    - Use cfdisk for MIPS based Cobalt machines.
  * Updated translations:
    - Basque (eu.po) by Piarres Beobide EgaÃ±a
    - Gallegan (gl.po) by HÃ©ctor FernÃ¡ndez LÃ³pez
    - Romanian (ro.po) by Eddy Petrisor

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 06 Apr 2004 02:33:32 +0100

partitioner (0.23) unstable; urgency=low

  * Updated translations: 
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Parlin Imanuel Toh

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Mar 2004 15:18:00 -0500

partitioner (0.22) unstable; urgency=low

  * Martin Michlmayr
    - Handle mips sub-architectures: use fdisk for SGI machines, cfdisk for
      the Broadcom MIPS development board "SWARM" (BCM91250A), and fdisk
      as fallback option.
    - Add a script to handle mipsel sub-architectures: use fdisk for
      DECstations, cfdisk for the SWARM board, and fdisk as fallback.
  * Bastian Blank
    - Calculate correct dependencies.
  * Updated translations: 
    - Polish (pl.po) by Bartosz Fenski
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Osman YÃ¼ksel

 -- Joey Hess <joeyh@debian.org>  Mon, 22 Mar 2004 13:48:45 -0500

partitioner (0.21) unstable; urgency=low

  * Bastian Blanl
    - Use stdin/-out copies from debconf.
    - Use new debhelper and strip binary.
  * Updated translations
    - Russian by Nikolai Prokoschenko

 -- Steinar H. Gunderson <sesse@debian.org>  Sun, 14 Mar 2004 19:40:29 +0100

partitioner (0.20) unstable; urgency=low

  * Bastian Blank
    - Use sector size for size calculation.
    - Ignore ignorable exceptions.
  * Kenshi Muto
    - util-linux maintainer didn't response anything...
      It is time to run cfdisk as LANG=C. I hope this change would be
      reverted before Sarge release.
  * Translations:
    - Dafydd Harries : Added Welsh translation (cy.po)
    - KÄstutis BiliÅ«nas: Updated Lithuanian translation (lt.po) 
    - Jordi Mallach: Updated Catalan translation (ca.po)

 -- Joey Hess <joeyh@debian.org>  Sat, 13 Mar 2004 00:07:48 -0500

partitioner (0.19) unstable; urgency=low

  * Jeff Bailey:
    - Apply patch from Thomas Poindessous <thomas@poindessous.com>
      for sparc partitioning (Closes: #228518)
    - Actually, use parted instead.
  * Translations:
    - HÃ¥vard Korsvoll
      - Updated Norwegian (Nynorsk) translation.
    - Changwoo Ryu
      - Added Korean translation (ko.po)
    - Ming Hua
      - Initial Traditional Chinese translation (zh_TW.po), by Tetralet
      - Updated Traditional Chinese translation (zh_TW.po), by Tetralet
    - HÃ¥vard Korsvoll
      - Updated Norwegian, bokmÃ¥l translation, (nb.po). From Axel Bojer

 -- Joey Hess <joeyh@debian.org>  Tue,  2 Mar 2004 13:42:00 -0500

partitioner (0.18) unstable; urgency=low

  * Translations:
    - Jordi Mallach: Updated Catalan translation.
    - Carlos Z.F. Liu
      - fix some serious errors in Simplified Chinese translation.
    - Giuseppe Sacco
      - Applied patch for normalizing menus and some italian translation (it.po)
    - h3li0s
      - added albanian translation (sq.po)
    - Eugen Meshcheryakov 
      - added Ukrainian translation (uk.po)

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Feb 2004 22:21:37 -0500

partitioner (0.17) unstable; urgency=low

  * Safir Secerovic
    - Update Bosnian translation (bs.po).
  * Stephen R. Marenka
    - Initial m68k support.

 -- Joey Hess <joeyh@debian.org>  Thu, 22 Jan 2004 20:17:38 -0500

partitioner (0.16) unstable; urgency=low

  * Miguel Figueiredo
    - Added Portuguese translation (pt.po)
  * Ming Hua
    - Initial Simplified Chinese translation (zh_CN.po)
  * Bart Cornelis
    - Merged Norwegian Nynorsk (nn.po) translation from skolelinux-cvs
  * AndrÃ© Dahlqvist
    - Update Swedish translation (sv.po)
  * Anmar Oueja
    - created and translated to Arabic (ar.po)
  * Christian Perrier
    - main.c: set LANGUAGE variable before calling fdisk programs
              this may cause problems with terminals

 -- Joey Hess <joeyh@debian.org>  Thu,  1 Jan 2004 15:01:11 -0500

partitioner (0.15) unstable; urgency=low

  * Bart Cornelis
    - Merged Norwegian Bokmael (nb.po) from skolelinux-cvs
  * TeÃ³filo Ruiz SuÃ¡rez
    - Updated Spanish translations (po/es.po)
  * Peter Mann
    - Updated Slovak translation
  * Giuseppe Sacco
    - Added italian translation (it.po)
  * Thiemo Seufer
    - Fix memory leaks.

 -- Joey Hess <joeyh@debian.org>  Thu, 25 Dec 2003 19:58:59 -0500

partitioner (0.14) unstable; urgency=low

  * Bartosz Fenski
    - Updated Polish (pl) translation.
  * Verok Istvan
    - Initial Hungarian translation
  * AndrÃ© Dahlqvist
    - Update Swedish translation. (sv.po)
  * David MartÃ­nez Moreno
    - Updated Spanish translation (es.po) and converted to UTF-8.
  * Konstantinos Margaritis
    - Updated Greek translation (el.po)
  * Thiemo Seufer
    - Change CFLAGS to -W -Wall -Os.
  * Alwin Meschede
    - Updated German translation (de.po)
  * Bart Cornelis
    - Updated Dutch translation (nl.po)
    - incorporated debian-l10n-dutch review comments into nl.po
  * Steinar H. Gunderson
    - Updated Norwegian BokmÃ¥l translation (nb.po).
  * Otavio Salvador
    - Change priority of question about disc to 'critical'.
      Closes: #220258
    - Fix a trivial spell error in pt_BR translation.
    - Fixed to not call the partitioner tool without a disk select.
  * Thiemo Seufer
    - Use always fdisk for mips.
    - Remove main.h, fix broken function prototypes and static variable
      definition in the header file by that.
    - Simplify extract_choice().
    - Respect MAX_DISKS.
  * Christian Perrier
    - Commented the hardcoded "Finish" string in main.c.
    - Changed templates for adding "Finish" to the possible choices. It
      then becomes translatable
  * Ognyan Kulev
    - Added/updated bulgarian translation (bg.po).
  * Miroslav Kure
    - Added/updated czech translation (bg.po).
  * AndrÃ© LuÃ­s Lopes
    - Updated pt_BR (Brazilian Portuguese) translation as per Christian's last
       change (Finish string availability).
  * Petter Reinholdtsen
    - Updated Norwegian BokmÃ¥l translation (nb.po).
  * KÄstutis BiliÅ«nas
    - Updated Lithuanian translation (lt.po).
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * Claus Hindsgaul
    - Update Danish translation (da.po).
  * Joey Hess
    - Add myself to uploaders.

 -- Bart Cornelis <cobaco@linux.be>  Tue, 23 Dec 2003 16:03:26 +0100

partitioner (0.13) unstable; urgency=low

  * Peter Mann
    - Initial Slovak translation (sk.po).
  * Ilgiz Kalmetev
    - Initial Russian translation.
  * Konstantinos Margaritis
    - Initial Greek translation (el.po).
  * Christian Perrier
    - Refined and standardized templates. Closes: #220026
    - Update French translation.
  * Claus Hindsgaul
    - Add Danish translation (da.po).
  * Safir Å eÄeroviÄ
    - Update Bosnian translation (bs.po)
  * Tommi Vainikainen
    - Update Finnish translation.
  * Miroslav Kure
    - Update Czech translation.
  * Jordi Mallach
    - Update Catalan translation.
  * Kenshi Muto
    - Update Japaense po (ja.po)
  * AndrÃ© LuÃ­s Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * KÄstutis BiliÅ«nas 
    - Update Lithuanian translation.
 	
 -- Joey Hess <joeyh@debian.org>  Tue,  9 Dec 2003 16:06:20 -0500

partitioner (0.12) unstable; urgency=low

  * Safir Secerovic, Amila Akagic
    - Add Bosnian translation (bs.po)

 -- Joey Hess <joeyh@debian.org>  Fri,  7 Nov 2003 13:31:46 -0500

partitioner (0.11) unstable; urgency=low

  * Gaudenz Steinlin
    - scripts/powerpc: replace /usr/bin/archdetect with /bin/archdetect

 -- Petter Reinholdtsen <pere@debian.org>  Sat,  1 Nov 2003 12:38:53 +0100

partitioner (0.10) unstable; urgency=low

  * Alastair McKinstry
    - Added versioned depends on libdebconfclient-dev to ensure we get
      working debonf_* macros.
  * AndrÃ© LuÃ­s Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * Denis Barbier
    - Add a comment in templates file to flag the main menu item.
  * Chris Tillman
    - change ppcdetect to archdetect and corresponding CASEs
  * Christian Perrier
    - Update French translation.
  * KÄstutis BiliÅ«nas
    - Add Lithuanian translation (lt.po).
  * Claus Hindsgaul
    - Add Danish translation (da.po).
  * Miroslav Kure
    - Update Czech translation (cs.po).
  * Tommi Vainikainen
    - Add Finnish (fi.po) translation
  * Matt Kraai
    - Remove the leading space from the menu entry (closes: #216109).
    - Build-depend on libdebconfclient0-dev instead of
      libdebconfclient-dev.
    - Add self to uploaders.

 -- Matt Kraai <kraai@debian.org>  Fri, 24 Oct 2003 01:24:14 -0700

partitioner (0.09) unstable; urgency=low

  * Alastair McKinstry
    - Set title via debian-installer template for i18n.
    - Move to new cdebconf macros.
  * Bart Cornelis
    - Updated dutch translation
  * Petter Reinholdtsen
    - Update nb.po.
  * Kenshi Muto
    - Update Japaense po (ja.po)

 -- Sebastian Ley <ley@debian.org>  Sun, 12 Oct 2003 19:25:59 +0200

partitioner (0.08) unstable; urgency=low

  * Search via ppcdetect for ppc subarchs
  * fix script/powerpc.sh
  * Richard Hirst
    - Don't count disks for which ped_device_get() fails.  IDE floppy with no
      disk inserted crashed partitioner.
  * Bastian Blank
    - Port to new libdebian-installer API.
  * Petter Reinholdtsen
    - Update dependency from libdebian-installer3-dev to
      libdebian-installer4-dev, patch from Goswin von Brederlow.
  * Miroslav Kure
    - Added Czech translation.

 -- Sebastian Ley <ley@debian.org>  Thu,  9 Oct 2003 17:19:30 +0200

partitioner (0.07) unstable; urgency=low

  * Aigars Mahinovs
    - Add latvian translation (lv.po).
  * Kenshi Muto
    - Added Japanese translation (ja.po)
    - Update ja.po
  * Jordi Mallach
    - Added Catalan (ca) translation.
  * Thorsten Sauter
    - Include patch for alpha architecture. (Closes: #211623)
    - Fix cfdisk "blank-screen" problem. (Closes: #209286)
    - debian/rules: don't install files under CVS/ directories

 -- Thorsten Sauter <tsauter@debian.org>  Sat, 20 Sep 2003 13:49:07 +0200

partitioner (0.06) unstable; urgency=low

  * Thorsten Sauter
    - change my email address
  * Alastair McKinstry
    - Remove unwanted menutest

 -- Alastair McKinstry <mckinstry@computer.org>  Tue, 17 Jun 2003 21:00:53 +0200

partitioner (0.05) unstable; urgency=low

  * Thorsten Sauter
    - Insert script for powerpc fdisk. (Closes: #194590)
  * Bastian Blank
    - Add german translation
  * Martin SjÃ¶gren
    - Run cfdisk also when the newt frontend is running.

 -- Thorsten Sauter <tsauter@debian.org>  Tue, 17 Jun 2003 20:50:56 +0200

partitioner (0.04) unstable; urgency=low

  * Petter Reinholdtsen
    - Change depend from disk-detect to new hw-detect-full.
    - Add Norwegian BokmÃ¥l (nb.po) and Nynorsk translation (nn.po).
  * Thorsten Sauter
    - pump priority from optional to standard
  * TeÃ³filo Ruiz SuÃ¡rez
    - Added es.po
  * AndrÃ© LuÃ­s Lopes :
    - Fixed some mistakes and improved translation in general.
  * Martin SjÃ¶gren
    - Recompile with libdebconfclient.

 -- Martin Sjogren <sjogren@debian.org>  Sat, 24 May 2003 22:24:23 +0200

partitioner (0.03) unstable; urgency=low

  * Thorsten Sauter
    - debian/control: fix build-depends. (Closes: #188323)
  * Pierre Machard
    - Add French po-debconf translation

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 10 Apr 2003 01:37:34 +0200

partitioner (0.02) unstable; urgency=low

  * Thorsten Sauter
    - Closes: #188010. Change binary-indep depends to binary.
  * Petter Reinholdtsen
    - Add myself to uploaders.

 -- Petter Reinholdtsen <pere@debian.org>  Wed,  9 Apr 2003 10:22:02 +0200

partitioner (0.01) unstable; urgency=low

  * Thorsten Sauter
     - Initial Release.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 29 Mar 2003 15:02:19 +0200

