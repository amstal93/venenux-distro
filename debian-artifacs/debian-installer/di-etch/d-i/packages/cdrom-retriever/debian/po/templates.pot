# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#: ../load-cdrom.templates:4
msgid "Load installer components from CD"
msgstr ""

#. Type: boolean
#. Description
#: ../cdrom-retriever.templates:4
msgid "Failed to copy file from CD-ROM. Retry?"
msgstr ""

#. Type: boolean
#. Description
#: ../cdrom-retriever.templates:4
msgid ""
"There was a problem reading data from the CD-ROM. Please make sure it is in "
"the drive. If retrying does not work, you should check the integrity of your "
"CD-ROM."
msgstr ""
