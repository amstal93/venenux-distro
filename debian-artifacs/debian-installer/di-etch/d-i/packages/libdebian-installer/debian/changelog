libdebian-installer (0.50etch1) UNRELEASED; urgency=low

  [ Martin Michlmayr ]
  * Match SGI O2 machines with 300MHz RM5200SC (Nevada) CPUs.

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 20 Jun 2007 11:35:49 +0200

libdebian-installer (0.50) unstable; urgency=low

  [ Joey Hess ]
  * Add back subarch-armeb-linux.c, and modify the Makefile to include it,
    so it's used after all.
  * Add support for armel.

 -- Frans Pop <fjp@debian.org>  Thu, 22 Feb 2007 12:11:32 +0100

libdebian-installer (0.49) unstable; urgency=low

  [ Joey Hess ]
  * Remove subarch-armeb-linux.c, which was never used. subarch-arm-linux.c
    is used for all arm variants.

 -- Frans Pop <fjp@debian.org>  Sun, 18 Feb 2007 13:44:15 +0100

libdebian-installer (0.48) unstable; urgency=low

  [ Sven Luther ]
  * Added support for Efika /proc/cpuinfo machine field. Closes: #403293.

 -- Frans Pop <fjp@debian.org>  Tue, 13 Feb 2007 23:24:24 +0100

libdebian-installer (0.47) unstable; urgency=low

  * Fix FTBFS on mips/mipsel.

 -- Thiemo Seufer <ths@debian.org>  Wed, 22 Nov 2006 15:59:38 +0000

libdebian-installer (0.46) unstable; urgency=low

  [ Thiemo Seufer ]
  * archdetect: Add mips/qemu-mips32 and mipsel/qemu-mips32.

  [ Colin Watson ]
  * include/debian-installer/macros.h: Add DI_GNUC_PREREQ macro useful for
    making use of __attribute__ in various places throughout d-i.

 -- Frans Pop <fjp@debian.org>  Wed, 22 Nov 2006 15:06:29 +0100

libdebian-installer (0.45) unstable; urgency=low

  * archdetect: Add the ARM Versatile platform.
  * archdetect: Return ixp4xx rather than nslu2 on Linksys NSLU2 since
    the NSLU2-specific flavour is no longer needed.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 30 Sep 2006 17:10:13 +0200

libdebian-installer (0.44) unstable; urgency=low

  * archdetect: Add the arm/iop32x platform, listing a number if Intel
    evaluation boards, the Thecus N2100 and N4100 and the GLAN Tank.
  * archdetect: Add the arm/iop33x platform, listing a number if Intel
    evaluation boards.
  * archdetect: Add the arm/ixp4xx platform.

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 21 Aug 2006 00:21:24 +0200

libdebian-installer (0.43) unstable; urgency=low

  * Add mapping for I20 hard disks. Patch from Robert Millan
    Closes: #373730
  * Call dh_makeshlibs w/o -n to get postinsts created.
  * Current policy.

 -- Joey Hess <joeyh@debian.org>  Thu, 15 Jun 2006 14:48:35 -0400

libdebian-installer (0.42) unstable; urgency=low

  [ Bastian Blank ]
  * Bump shared library revision to 6.
  * Add small AVL tree implementation.

  [ Martin Michlmayr ]
  * Remove (incomplete) BAST and LAST support.
  * Drop Riscstation since it's no longer supported in 2.6 kernels.
  * Rename arm/riscpc to arm/rpc for consistency.

  [ Colin Watson ]
  * Fix Hurd detection at build time.

  [ Joey Hess ]
  * Move more resolver debug logging into ENABLE_EXTENSIVE_DEBUG ifdefs.

 -- Bastian Blank <waldi@debian.org>  Wed, 31 May 2006 23:02:10 +0200

libdebian-installer (0.41) unstable; urgency=low

  * --add-udeb only works for one udeb, correct dh_makeshlibs call
    to not generate bad udeb shlibs.

 -- Joey Hess <joeyh@debian.org>  Sat, 18 Mar 2006 14:15:07 -0500

libdebian-installer (0.40) unstable; urgency=low

  [ Martin Michlmayr ]
  * archdetect: Add support for the Broadcom BCM91480B evaluation board
    (aka "BigSur").
  * archdetect: Rename sb1-swarm-bn to sb1-bcm91250a for consistency.
  * archdetect: Remove ARM platforms we don't actually support.
  * archdetect: Remove mips/mipsel platforms we don't actually support.

  [ Joey Hess ]
  * Make libdebian-installer-extra4-udeb depend on
    libdebian-installer-udeb, not the deb.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 17 Mar 2006 22:42:16 +0000

libdebian-installer (0.39) unstable; urgency=low

  * Add udeb lines to shlibs file.
  * Drop libdebian-installer4-udeb's provide of libdebian-installer4
    since it's on the initrd and packages will get a proper dep once
    they're rebuilt against this.

 -- Joey Hess <joeyh@debian.org>  Wed, 15 Mar 2006 15:10:49 -0500

libdebian-installer (0.38) unstable; urgency=low

  [ Martin Michlmayr ]
  * Fix synatx error in mipsel file.

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 18 Jan 2006 20:11:56 +0000

libdebian-installer (0.37) unstable; urgency=low

  [ Martin Michlmayr ]
  * Define Broadcom BCM947XX for archdetect.
  * Define Linksys NSLU2 for archdetect.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Jan 2006 23:06:14 +0000

libdebian-installer (0.36) unstable; urgency=low

  [ Sven Luther ]
  * No need to list CHRP IBM models explicitly, as this breaks with models we
    don't know about or newer models, which is unnecessary. (Closes: #338045)

 -- Colin Watson <cjwatson@debian.org>  Mon, 14 Nov 2005 18:58:35 +0000

libdebian-installer (0.35) unstable; urgency=low

  * Add archdetect support for ADS boards running the 2.6 kernel,
    including the BitsyXb.

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Sep 2005 15:07:32 -0400

libdebian-installer (0.34) unstable; urgency=low

  * Collapse all ADS boards into one ads subarch.

 -- Joey Hess <joeyh@debian.org>  Thu, 18 Aug 2005 10:10:07 -0400

libdebian-installer (0.33) unstable; urgency=low

  * Add various ADS arm boards to the archdetect list
    (AGX, VGX, GCX, Sphere).

 -- Joey Hess <joeyh@debian.org>  Wed, 17 Aug 2005 09:53:00 -0400

libdebian-installer (0.32) unstable; urgency=low

  * Update GPL notices with the FSF's new address.
  * Use -ggdb instead of -gstabs. The latter doesn't work on ia64.

 -- Colin Watson <cjwatson@debian.org>  Wed,  3 Aug 2005 10:36:54 +0100

libdebian-installer (0.31) unstable; urgency=low

  [ Colin Watson ]
  * When compiling with gcc, perform printf format string checking on the
    arguments to di_snprintfcat.
  * Add di_system_subarch_analyze, moved here from hw-detect.
  * Bump shared library revision to 5.

  [ Joey Hess ]
  * Add loop devices to devfs table. Closes: #320039

 -- Colin Watson <cjwatson@debian.org>  Thu, 28 Jul 2005 10:08:44 +0100

libdebian-installer (0.30) unstable; urgency=low

  * Bastian Blank
    - Bump shared library revision to 4.
    - Deprecate di_prebaseconfig_append and di_system_prebaseconfig_append.
    - Add di_exec_path(_full) (execvp wrapper).
  * Joey Hess
    - Patch log.c to build under gcc 4.0. Closes: #287384
  * Colin Watson
    - doxygen doesn't work yet on the Hurd, so avoid building documentation
      there.
    - Add myself to Uploaders.
  * Matt Zimmerman
    - Add devfs-mapping support for UML ubd devices (closes: #258176).

 -- Colin Watson <cjwatson@debian.org>  Wed, 25 May 2005 12:49:09 +0100

libdebian-installer (0.29) unstable; urgency=low

  * Joey Hess
    - Re-enable character device mapping. Though it often gets it wrong, the
      cases where it gets it right are used by prebaseconfig.

 -- Joey Hess <joeyh@debian.org>  Tue, 15 Jun 2004 12:41:12 -0400

libdebian-installer (0.28) unstable; urgency=low

  * Joey Hess
    - fix devfs.c test mode (to really work)
    - fix /dev/ida support off by one
    - fix /dev/ataraid support, leave off the cN part of device name
    - don't map character devices to block device namesb

 -- Joey Hess <joeyh@debian.org>  Mon, 14 Jun 2004 11:34:03 -0400

libdebian-installer (0.27) unstable; urgency=low

  * Bastian Blank
    - anna dependency resolver stops on already installed packages.
    - Make debugging output a little bit better to read.
    - Make anna dependency resolver permissive again.
    - Add an option to enable extensive debugging output.
    - Fix ABI. (closes: #250299, #253037)
  * Joshua Kwan
    - quote an argument in configure.ac to fix test complaining about no
      LHS for extensive-debug

 -- Bastian Blank <waldi@debian.org>  Mon, 14 Jun 2004 14:23:10 +0200

libdebian-installer (0.23) unstable; urgency=low

  * Bastian Blank
    - Bump revision to 3.
    - Add a special dependency resolver type for anna (checks Kernel-Version
      and Subarchitecture) (unstable interface).
    - Mark the old Kernel-Version resolver as deprecated.
    - Remove backward compatiblity from dependency resolver.
    - Remove two undocumented symbols.
    - Use debhelper udeb support.
    - Add environment support to execution helpers.
    - The size of a hash map is the number of elements, not the size of the
      array.
    - RFC822 parser only allows space and horizontal tab before a value.

 -- Bastian Blank <waldi@debian.org>  Thu, 13 May 2004 17:50:15 +0200

libdebian-installer (0.22) unstable; urgency=low

  * Bastian Blank
    - Fix handling of nonresolvable virtual packages.
    - Fix handling of subarchitecture strings with different length
      (closes: #243692).
  * Joey Hess
    - Add more major numbers for cciss devices.
    - Add support for all of the /dev/ida/ devices. Closes: #243411
    - Add support for /dev/ataraid/ devices.
    - Allow devfs.c to be built as a standalone binary, for simplified
      testing.

 -- Joey Hess <joeyh@debian.org>  Wed, 21 Apr 2004 23:13:06 -0400

libdebian-installer (0.21) unstable; urgency=low

  * Bastian Blank
    - Support diskarrays in mapdevfs. (closes: #235617, #239291)
  * Thiemo Seufer
    - Minor code cleanup in the subarchitecture check.

 -- Joey Hess <joeyh@debian.org>  Mon,  5 Apr 2004 18:49:56 -0400

libdebian-installer (0.20) unstable; urgency=low

  * Bastian Blank
    - Bump revision to 2.
    - Add conversation functions for parts of a package entry
      (exported interface).
    - Add support for Kernel-Version to Packages parser.
    - Fix dependency resolver marker reset.
    - Split dependency resolver (internal interface).
    - Add two special dependency resolver types for anna (checks
      Kernel-Version) and main-menu (never fails) (unstable interface).
    - Fix Subarchitecture check.
  * Stephen R. Marenka
    - Add scsi cd (scd?) device info to mapdevfs.

 -- Bastian Blank <waldi@debian.org>  Mon, 15 Mar 2004 11:13:17 +0100

libdebian-installer (0.19) unstable; urgency=low

  * Bastian Blank
    - Minimal Packages parser parses Size and Priority.
    - Packages parser uses simpler matches.

 -- Bastian Blank <waldi@debian.org>  Fri, 06 Feb 2004 14:07:18 +0100

libdebian-installer (0.18) unstable; urgency=low

  * Alastair McKinstry
    - Add di_info(), di_debug() logging macros.
  * Bastian Blank
    - Bump revision to 1.
    - Remove mapping of di_log.
    - Add any symbol to the version script.
    - Use size_t in hash map and mem_chunk allocer. (closes: #215448)
    - Improve and install documentation.
    - Add -extra lib.
    - Fix description parsing. (closes: #219902)
    - Remove some references to the chunk allocator.
    - Add version info to installed headers.
    - Default log handler suppress debug messages.
    - Enable support for Subarchitecure.
    - Correctly terminate argv.
  * Petter Reinholdtsen
    - Define PATH_MAX if it is undefined, to get this building on
      GNU/Hurd.  Patch from Santiago Vila. (Closes: #219464)
  * Matt Kraai
    - Prefer a package that is installed to one that isn't when satisfying
      a dependency on a virtual package.

 -- Bastian Blank <waldi@debian.org>  Wed, 21 Jan 2004 17:56:11 +0100

libdebian-installer (0.17) unstable; urgency=low

  * Bastian Blank
    - disable subarch stuff

 -- Bastian Blank <waldi@debian.org>  Fri, 03 Oct 2003 18:07:44 +0200

libdebian-installer (0.16) experimental; urgency=low

  * Bastian Blank
    - rework lib
      - add rfc822 parser
      - add logging
      - add packages file parsing
    - dump major to 4

 -- Bastian Blank <waldi@debian.org>  Thu, 28 Aug 2003 17:46:27 +0200

libdebian-installer (0.15) unstable; urgency=low

  * Bastian Blank
    - Don't log to stderr and close stdin.
    - Never cast malloc return values.
  * Joey Hess
    - added di_status_read and di_config_package

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 22 Jul 2003 13:19:17 +0200

libdebian-installer (0.14) unstable; urgency=low

  * Bastian Blank
    - Add Enhances parsing.
  * Petter Reinholdtsen
    - Add support in di_mapdevfs for SW RAID devices, /dev/md#.
      (Closes: #185574)

 -- Bastian Blank <waldi@debian.org>  Sun, 01 Jun 2003 20:54:45 +0200

libdebian-installer (0.13) unstable; urgency=low

  * Martin Sjögren
    - Add di_mapdevfs function.
    - Change section of libdebian-installer3-dev to libdevel.
    - Add Recommends parsing.
    - Don't log to stderr, it will interfere with frontends.
    - Revert to major/minor/micro. current/revision/age is something
      else.
  * Bastian Blank
    - cleanup di_mapdevfs.
  * Petter Reinholdtsen
    - Add new functin di_logf(), a vararg version of di_log().

 -- Martin Sjogren <sjogren@debian.org>  Sun,  4 May 2003 16:20:20 +0200

libdebian-installer (0.12) unstable; urgency=low

  * Martin Sjögren
    - Build-dep on d-shlib >= 0.10 (Closes: #178073).

 -- Martin Sjogren <sjogren@debian.org>  Fri, 24 Jan 2003 20:24:00 +0100

libdebian-installer (0.11) unstable; urgency=low

  * Martin Sjögren
    - Fix depends/provides parser (Closes: #177061).

 -- Martin Sjogren <sjogren@debian.org>  Fri, 17 Jan 2003 10:49:23 +0100

libdebian-installer (0.10) unstable; urgency=low

  * Martin Sjögren
    - Add di_pkg_alloc and di_pkg_free for memory management of package
      structs.
    - Add di_list_free for freeing an entire linked list.

 -- Martin Sjogren <sjogren@debian.org>  Sun, 29 Dec 2002 17:11:57 +0100

libdebian-installer (0.09) unstable; urgency=low

  * Martin Sjögren
    - Add di_pkg_toposort_{list,arr} to be used by anna and main-menu.

 -- Martin Sjogren <sjogren@debian.org>  Sat,  7 Dec 2002 12:14:08 +0100

libdebian-installer (0.08) unstable; urgency=low

  * Martin Sjögren
    - Fix di_pkg_is_installed, now it even works!

 -- Tollef Fog Heen <tfheen@debian.org>  Thu,  5 Dec 2002 00:45:28 +0100

libdebian-installer (0.07) unstable; urgency=low

  * Martin Sjögren
    - Add a linked list data structure, decoupling next pointers from the
      node data
    - Add a package_dependency struct that has package name and a pointer
      to a package_t struct
    - Add functions di_pkg_find, di_pkg_provides, di_pkg_is_virtual,
      di_pkg_is_installed and di_pkg_resolve_deps
    - Change maintainer to debian-boot and set dwhedon, tfheen and sjogren as
      uploaders
    - Bump standards-version and fix copyright file so lintian shuts up

 -- Martin Sjogren <sjogren@debian.org>  Fri, 29 Nov 2002 16:36:45 +0100

libdebian-installer (0.06) unstable; urgency=low

  * Martin Sjögren
    - Multiple provides
    - Remove libd-i-pic
    - Priority parsing
    - Version number parsing and comparison

 -- Tollef Fog Heen <tfheen@debian.org>  Wed,  6 Nov 2002 01:46:42 +0100

libdebian-installer (0.05) unstable; urgency=low

  * change an fprintf to vfprintf.

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 24 Oct 2002 12:34:26 +0200

libdebian-installer (0.04) unstable; urgency=low

  * Fix up section according to override file.
  * Build "proper" library packages.
  David Kimdon
    - add di_prebaseconfig_append()
    - change my name (s/Whedon/Kimdon/g)
  Junichi Uekawa
    - require d-shlibs 0.3 or greater for build
    - -dev: add devlibs:Depends, require libdebian-installer ${Source-Version}
    - -pic: depend on exact version of -dev to avoid nasty surprises
  Martin Sjögren
    - Add 'Packages' file parsing functionality (di_pkg_parse).
    - Case insensitive strstr (di_stristr).
  Michael Cardenas
    - fix install target so it can be ran more than once

 -- Tollef Fog Heen <tfheen@debian.org>  Tue,  6 Aug 2002 16:03:45 +0200

libdebian-installer (0.03) unstable; urgency=low

  * Fix up postinst name.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue,  6 Aug 2002 15:13:39 +0200

libdebian-installer (0.02) unstable; urgency=low

  * New name, which will hopefully be accepted by ftp-master.
  * Removed emacs cruft from changelog.
  * Add di_snprintfcat, courtesy of thomas poindessous

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 20 May 2002 23:21:24 +0200

libd-i (0.01) unstable; urgency=low

  * Initial Release.

 -- David Whedon <dwhedon@debian.org>  Thu,  8 Feb 2001 21:54:45 -0800


