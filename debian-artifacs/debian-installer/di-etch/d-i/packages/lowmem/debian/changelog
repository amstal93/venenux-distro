lowmem (1.25) unstable; urgency=low

  [ Joey Hess ]
  * Add armel lowmem level. Should be close to the same as arm.

  [ Steve Langasek ]
  * Add alpha lowmem level.  Closes: #410596.

 -- Frans Pop <fjp@debian.org>  Wed, 21 Feb 2007 10:10:38 +0100

lowmem (1.24) unstable; urgency=low

  [ Martin Michlmayr ]
  * Make sure that SGI machines with 64 MB of RAM don't run in lowmem
    mode. Closes: #405971.

  [ Frans Pop ]
  * Update lowmem levels for x86 for Etch RC2 as tested in VMware.

 -- Frans Pop <fjp@debian.org>  Wed, 24 Jan 2007 02:20:24 +0100

lowmem (1.23) unstable; urgency=low

  * trimtemplates: don't remove "Choices-C" from templates. Closes: #400263.
  * Add myself to uploaders.

 -- Frans Pop <fjp@debian.org>  Sun, 10 Dec 2006 03:23:38 +0100

lowmem (1.22) unstable; urgency=low

  * Installations on ARM and MIPS with 32 MB require lowmem level 2.
  * Installations on ARM and MIPS with 64 MB should not need lowmem.

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 26 Nov 2006 11:44:34 +0100

lowmem (1.21) unstable; urgency=low

  [ Colin Watson ]
  * Use list-devices to discover whether any discs have been found. Requires
    di-utils 1.33.

  [ Frans Pop ]
  * Remove standards-version and add Lintian override for it. 

 -- Frans Pop <fjp@debian.org>  Sat, 16 Sep 2006 11:42:29 +0200

lowmem (1.20) unstable; urgency=low

  [ Martin Michlmayr ]
  * Level1 installations work again on ARM with 32 MB RAM thanks to the
    temporary removal of crypto modules.

  [ Frans Pop ]
  * Assume amd64 needs similar limits as i386.
  * Update lowmem settings for Etch Beta 3; settings are somewhat conservative
    and can hopefully be lowered again in the future. Main culprit for
    increased memory usage is the number of supported languages which also
    explains the large differences between level 1 and 2 limits.

 -- Frans Pop <fjp@debian.org>  Mon, 31 Jul 2006 13:33:05 +0200

lowmem (1.19) unstable; urgency=low

  [ Philip Hands ]
  * get rid of a couple of redundant greps from the postinst
  * don't bother "truncating" already empty log files

  [ Martin Michlmayr ]
  * Increase level2 on ARM.

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 12 Jul 2006 21:45:28 +0200

lowmem (1.18) unstable; urgency=low

  * Do not delete modules when partman is started if no discs have been found
    yet. This allows the user to go back to anna to load additional module
    udebs and run hw-detect again without having to find out that core modules
    have already been deleted.
  * Standards-version is not required for udebs.

 -- Frans Pop <fjp@debian.org>  Wed, 21 Jun 2006 13:41:21 +0200

lowmem (1.17) unstable; urgency=low

  * Change limits for level2 and minimum for i386.
    Level2 is needed below 44MB
  * Add a Standards field in control. Shut up, lintian

 -- Christian Perrier <bubulle@debian.org>  Thu, 19 Jan 2006 19:33:37 +0100

lowmem (1.16) unstable; urgency=low

  * Move anna-install lowmem to after rootskel has loaded debconf templates;
    anna-install requires a functional debconf and will fail to install
    lowmem otherwise.

 -- Colin Watson <cjwatson@debian.org>  Wed, 18 Jan 2006 13:38:23 +0000

lowmem (1.15) unstable; urgency=low

  * Fix syntax error introduced in last upload.

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 16 Jan 2006 01:33:41 +0000

lowmem (1.14) unstable; urgency=low

  * Add armeb lowmem levels.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Jan 2006 23:09:42 +0000

lowmem (1.13) unstable; urgency=low

  * Add arm lowmem levels (based on Linksys NSLU2).

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Jan 2006 10:37:43 +0000

lowmem (1.12) unstable; urgency=low

  [ Sylvain Ferriol ]
  * Optimize trimtemplates.c
  * Add a script to trimtemplate before debconf_loadtemplate

  [ Joey Hess ]
  * Rename the debian-installer-startup.d scripts for clarity.
  * Misc cleanups.
  * Remove svn exclusion stuff in rules file; if building from a svn checkout
    you should use standard workarounds to avoid svn directories.
  * Clean up kernel fuzz in minimum memory checking.
  * Update mipsel lowmem levels to match the numbers in the changelog for
    1.07.

  [ Christian Perrier ]
  * Set the locale to C in lowmem mode. Closes: #341540 (with localechooser
    0.24)
  * Add self to Uploaders

 -- Christian Perrier <bubulle@debian.org>  Mon,  5 Dec 2005 18:51:27 +0100

lowmem (1.11) unstable; urgency=low

  * Fix preseeding of localechooser by using db_fset to set
    debian-installer/locale as seen
    Closes: #329175
  * Avoid installing .svn directories in /lib (well, mostly cosmectic)

 -- Christian Perrier <bubulle@debian.org>  Tue, 20 Sep 2005 18:12:08 +0200

lowmem (1.10) unstable; urgency=low

  * Mark debian-installer/locale seen to properly preseed it and avoid the
    localechooser questions being asked.

 -- Joey Hess <joeyh@debian.org>  Mon, 20 Jun 2005 22:28:00 -0400

lowmem (1.09) unstable; urgency=low

  * Joey Hess
    - Modify control file priority to match overrides.
    - Update preseeding for localechooser.

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Jun 2005 14:53:23 -0400

lowmem (1.08) unstable; urgency=low

  * Joey Hess
    - Try to reword the templates to avoid referring to installation, since
      the system may be used in rescue mode.
    - Set a "low memory mode" info message (rescue mode comes after and
      beats this message). Requires cdebconf-udeb 0.75 and main-menu 1.03.

 -- Joey Hess <joeyh@debian.org>  Thu,  5 May 2005 10:58:37 -0400

lowmem (1.07) unstable; urgency=low

  * Thiemo Seufer
    - Ajust memory usage values for mips.
    - Exclude .svn directories from build.
  * Martin Michlmayr
    - Test and update Cobalt (mipsel): 36+ real memory: no lowmem,
      25-35: lowmem 1, 22-24: lowmem 2.

 -- Thiemo Seufer <ths@debian.org>  Mon, 14 Feb 2005 01:02:18 +0100

lowmem (1.06) unstable; urgency=low

  * Joey Hess
    - Preseed countrychooser/shortlist, needed due to preseeding change in
      countrychooser. Closes: #277493

 -- Joey Hess <joeyh@debian.org>  Wed, 20 Oct 2004 13:13:34 -0400

lowmem (1.05) unstable; urgency=low

  * Joey Hess
    - Add a keymap file removal hack, post-anna.
    - Add partman hook and module removal hack.
    - i386 can run in lowmem level 1 for 32 mb installs again.

 -- Joey Hess <joeyh@debian.org>  Fri,  1 Oct 2004 17:01:18 -0400

lowmem (1.04) unstable; urgency=low

  * Frans Pop
    - Template name change in countrychooser:
      country-name-shortlist -> shortlist

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 20:46:12 -0400

lowmem (1.03) unstable; urgency=low

  * Martin Michlmayr
    - Set level2 and min values for mipsel (Cobalt).
  * Bastian Blank
    - Set values for s390.

 -- Joey Hess <joeyh@debian.org>  Mon,  6 Sep 2004 20:27:36 -0400

lowmem (1.02) unstable; urgency=low

  * Joey Hess
    - Add Sylvain Ferriol's lowmem level patch.
    - Modifed to store the level in /var/lib/lowmem. Level 1 is default
      lowmem mode, level 2 is super lowmem mode. The file still doesn't exist
      in non-lowmem mode.
    - Documented this.
    - Add error message if the machine doesn't have even the minimum
      supported memory. Closes: #266984

 -- Joey Hess <joeyh@debian.org>  Fri, 27 Aug 2004 16:12:07 -0400

lowmem (1.01) unstable; urgency=low

  * Bastian Blank
    - Move script into startup code to follow rootskel changes. Needs rootskel
      0.89.
  * Joey Hess
    - Patch from Bubulle to hack in non-removal of -en "translations".
      Closes: #245166

 -- Joey Hess <joeyh@debian.org>  Tue, 17 Aug 2004 16:28:55 +0100

lowmem (1.00) unstable; urgency=low

  * Thiemo Seufer
    - Increase userspace RAM trigger for mips to 33 MB.

 -- Joey Hess <joeyh@debian.org>  Thu, 29 Jul 2004 23:31:55 -0400

lowmem (0.14) unstable; urgency=low

  * Martin Michlmayr
    - Before running the "kill" command, check whether there is actually
      any suitable "tail" process to kill; otherwise the postinst will
      fail when there is no such process which is always the case in
      serial console installs and might be the case when the postinst
      is run twice.  Closes: #260949
    - Also, be super cautious and use kill $pid || true because there
      is the possibilty for a race occuring; thanks, Joey Hess.
    - Increase needed ram on mipsel from 25 to 33: installations on
      Cobalt machines without lowmem do not work with 32 MB but work
      with 36 MB, so enable lowmem when less than 36 MB are found.
      (Machines with 35 MB RAM are shown to have 32, 32 MB are shown
      as 29, so pick 33 for needed ram.)

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 23 Jul 2004 16:04:27 +0100

lowmem (0.13) unstable; urgency=low

  * Fixes for when countrychooser is not installed, as in a floppy install,
    and for general robustness if other debconf questions are missing.
    Closes: #260628

 -- Joey Hess <joeyh@debian.org>  Wed, 21 Jul 2004 20:05:54 -0400

lowmem (0.12) unstable; urgency=low

  * Move the stuff in S31lowmem to S10lowmem, so it runs before the code that
    decides whether to enable framebuffer. I think this code moved during
    recent rootskel changes.

 -- Joey Hess <joeyh@debian.org>  Tue, 20 Jul 2004 18:19:50 -0400

lowmem (0.11) unstable; urgency=low

  * Rebuild without .svn directory in deb. Closes: #256953

 -- Joey Hess <joeyh@debian.org>  Wed, 30 Jun 2004 11:19:05 -0400

lowmem (0.10) unstable; urgency=low

  * Put back the code coping with recent languagechooser changes
    Closes: #254631

 -- Christian Perrier <bubulle@debian.org>  Wed, 16 Jun 2004 07:17:25 +0200

lowmem (0.9) unstable; urgency=low

  * Re-upload, 0.8 release was botched.

 -- Joey Hess <joeyh@debian.org>  Fri, 11 Jun 2004 13:11:38 -0400

lowmem (0.8) unstable; urgency=low

  * Joey Hess
    - Languagechooser has been reverted back to old behavior, follow suite.
  * Stephen R. Marenka
    - Add m68k minimum ram.
  * Joey Hess
    - Typo fix. Closes: #247934
  * Sylvain Ferriol
    - To be compliant with new languagechooser/language-name template

 -- Joey Hess <joeyh@debian.org>  Fri, 11 Jun 2004 12:32:02 -0400

lowmem (0.6) unstable; urgency=low

  * Print a message before running expensive trimtemplates operation at boot
    time.

 -- Joey Hess <joeyh@debian.org>  Thu, 29 Apr 2004 22:25:31 -0400

lowmem (0.5) unstable; urgency=HIGH

  * Don't always nuke all translations; test for flag file when operating
    on single files too. Closes: #245418

 -- Joey Hess <joeyh@debian.org>  Fri, 23 Apr 2004 00:00:18 -0400

lowmem (0.4) unstable; urgency=low

  * Sylvain Ferriol
    - To be compliant with the previous trimtemplates in shell,
      trimtemplates can have directory or file as argument.
  * Joey Hess
    - Converted to use MemTotal, which will work with thre 2.4 and 2.6
      kernels.
    - Don't crash the boot process if the free memory cannot be detected.

 -- Joey Hess <joeyh@debian.org>  Sat, 27 Mar 2004 01:11:03 -0500

lowmem (0.3) unstable; urgency=low

  * Sylvain Ferriol
    - Re-code trimtemplates in C.
  * Joey Hess
    - Clean the binary.
    - lowmemcheck is binary any, not all.
    - Build it in in binary-arch.
    - Buils -Os.

 -- Joey Hess <joeyh@debian.org>  Tue, 23 Mar 2004 20:46:09 -0500

lowmem (0.2) unstable; urgency=low

  * Thiemo Seufer
    - Make amount of memory for lowmem installs arch-dependent.

 -- Joey Hess <joeyh@debian.org>  Mon, 22 Mar 2004 15:14:42 -0500

lowmem (0.1) unstable; urgency=low

  * First release. 32 mb installs work again on i386 with this package.

 -- Joey Hess <joeyh@debian.org>  Mon,  8 Mar 2004 13:31:36 -0500
