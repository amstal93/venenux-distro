# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/gl.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Galician messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: 2006-05-19 14:08-0500\n"
"Last-Translator: Jacobo Tarrio <jtarrio@debian.org>\n"
"Language-Team: Galician <trasno@ceu.fi.udc.es>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#: ../rescue-check.templates:10
msgid "Rescue mode"
msgstr "Modo de rescate"

#. Type: text
#. Description
#: ../rescue-mode.templates:3
msgid "Enter rescue mode"
msgstr "Entrar no modo de rescate"

#. Type: select
#. Description
#: ../rescue-mode.templates:8
msgid "Device to use as root file system:"
msgstr "Dispositivo a empregar coma sistema de ficheiros raíz:"

#. Type: select
#. Description
#: ../rescue-mode.templates:8
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Introduza o dispositivo que quere empregar coma sistema de ficheiros raíz. "
"Ha poder escoller entre varias operacións de rescate para realizar neste "
"sistema de ficheiros."

#. Type: error
#. Description
#: ../rescue-mode.templates:14
msgid "No such device"
msgstr "Non existe o dispositivo"

#. Type: error
#. Description
#: ../rescue-mode.templates:14
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"O dispositivo que introduciu para o sistema de ficheiros raíz (${DEVICE}) "
"non existe. Volva tentalo."

#. Type: error
#. Description
#: ../rescue-mode.templates:20
msgid "Mount failed"
msgstr "Non se puido montar"

#. Type: error
#. Description
#: ../rescue-mode.templates:20
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Houbo un erro ao montar o dispositivo que introduciu para o sistema de "
"ficheiros raíz (${DEVICE}) en /target."

#. Type: error
#. Description
#: ../rescue-mode.templates:20
msgid "Please check the syslog for more information."
msgstr "Consulte o rexistro do sistema para obter máis información."

#. Type: select
#. Description
#: ../rescue-mode.templates:31
msgid "Rescue operations"
msgstr "Operacións de rescate"

#. Type: error
#. Description
#: ../rescue-mode.templates:35
msgid "Rescue operation failed"
msgstr "A operación de rescate fallou"

#. Type: error
#. Description
#: ../rescue-mode.templates:35
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"A operación de rescate \"${OPERATION}\" fallou co código de saída ${CODE}."

#. Type: text
#. Description
#: ../rescue-mode.templates:41
msgid "Execute a shell in ${DEVICE}"
msgstr "Executar un intérprete de ordes en ${DEVICE}"

#. Type: text
#. Description
#: ../rescue-mode.templates:46
msgid "Execute a shell in the installer environment"
msgstr "Executar un intérprete de ordes no ambiente do instalador"

#. Type: text
#. Description
#: ../rescue-mode.templates:51
msgid "Choose a different root file system"
msgstr "Escoller un sistema de ficheiros raíz diferente"

#. Type: text
#. Description
#: ../rescue-mode.templates:56
msgid "Reboot the system"
msgstr "Reiniciar o sistema"

#. Type: text
#. Description
#: ../rescue-mode.templates:60
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Despois desta mensaxe háselle dar un intérprete de ordes con ${DEVICE} "
"montado en \"/\". Se precisa de outros sistemas de ficheiros (coma un \"/usr"
"\" separado), ha ter que os montar vostede por si mesmo."

#. Type: error
#. Description
#: ../rescue-mode.templates:67
msgid "Error running shell in /target"
msgstr "Erro ao executar o intérprete de ordes en /target"

#. Type: error
#. Description
#: ../rescue-mode.templates:67
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Atopouse un intérprete de ordes (${SHELL}) no seu sistema de ficheiros raíz "
"(${DEVICE}), pero houbo un erro ao executalo."

#. Type: error
#. Description
#: ../rescue-mode.templates:73
msgid "No shell found in /target"
msgstr "Non se atopou un intérprete de ordes en /target"

#. Type: error
#. Description
#: ../rescue-mode.templates:73
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Non se atopou un intérprete de ordes utilizable no seu sistema de ficheiros "
"raíz (${DEVICE})."

#. Type: text
#. Description
#: ../rescue-mode.templates:78
msgid "Executing a shell"
msgstr "A executar un intérprete de ordes"

#. Type: text
#. Description
#: ../rescue-mode.templates:78
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Despois desta mensaxe háselle dar un intérprete de ordes con ${DEVICE} "
"montado en \"/target\". Pode traballar nel empregando as ferramentas "
"dispoñibles no ambiente do instalador. Se quere convertilo no seu sistema de "
"ficheiros raíz de xeito temporal, execute \"chroot /target\". Se precisa de "
"outros sistemas de ficheiros (coma un \"/usr\" separado), ha ter que os "
"montar vostede por si mesmo."
