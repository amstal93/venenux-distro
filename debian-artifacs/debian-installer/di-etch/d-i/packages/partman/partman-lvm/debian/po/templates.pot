# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#: ../partman-lvm.templates:3
msgid "Unallocated physical volumes:"
msgstr ""

#. Type: text
#. Description
#: ../partman-lvm.templates:7
msgid "Volume groups:"
msgstr ""

#. Type: text
#. Description
#: ../partman-lvm.templates:11
msgid "Uses physical volume:"
msgstr ""

#. Type: text
#. Description
#: ../partman-lvm.templates:15
msgid "Provides logical volume:"
msgstr ""

#. Type: text
#. Description
#. "none" here == "No Physical Volumes"
#: ../partman-lvm.templates:20
msgid ""
"none[ Do not translate what's inside the brackets and just put the "
"translation for the word \"none\" in your language without any brackets. "
"This \"none\" relates to \"Physical Volumes:\" ]"
msgstr ""

#. Type: text
#. Description
#. "none" here == "No Volume Groups"
#: ../partman-lvm.templates:25
msgid ""
"none[ Do not translate what's inside the brackets and just put the "
"translation for the word \"none\" in your language without any brackets. "
"This \"none\" relates to \"Volume Groups:\" ]"
msgstr ""

#. Type: text
#. Description
#. Translators: use the acronym for "Physical Volume" in your language here
#: ../partman-lvm.templates:34
msgid "PV"
msgstr ""

#. Type: text
#. Description
#. What is "in use" is a partition
#: ../partman-lvm.templates:39
msgid "In use by LVM volume group ${VG}"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:45
msgid "Display configuration details"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:51
msgid "Create volume group"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:57
msgid "Delete volume group"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:63
msgid "Extend volume group"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:69
msgid "Reduce volume group"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:75
msgid "Create logical volume"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:81
msgid "Delete logical volume"
msgstr ""

#. Type: text
#. Description
#. Menu entry
#. Use infinitive form
#: ../partman-lvm.templates:87
msgid "Finish"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:93
msgid "Write the changes to disks and configure LVM?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:93
msgid ""
"Before the Logical Volume Manager can be configured, the current "
"partitioning scheme has to be written to disk.  These changes cannot be "
"undone."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:93
msgid ""
"After the Logical Volume Manager is configured, no additional changes to the "
"partitioning scheme of disks containing physical volumes are allowed during "
"the installation. Please decide if you are satisfied with the current "
"partitioning scheme before continuing."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:108
msgid "Keep current partition layout and configure LVM?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:108
msgid ""
"After the Logical Volume Manager is configured, no additional changes to the "
"partitions in the disks containing physical volumes are allowed. Please "
"decide if you are satisfied with the current partitioning scheme in these "
"disks before continuing."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:116
msgid "LVM configuration failure"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:116
msgid "An error occurred while writing the changes to the disks."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:116
msgid "The configuration of the Logical Volume Manager is aborted."
msgstr ""

#. Type: text
#. Description
#: ../partman-lvm.templates:123
msgid "physical volume for LVM"
msgstr ""

#. Type: text
#. Description
#. keep it short (ideally a 3-letter acronym)
#: ../partman-lvm.templates:128
msgid "lvm"
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. Use infinitive form
#: ../partman-lvm.templates:134
msgid "Configure the Logical Volume Manager"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:139
msgid "Activate existing volume groups?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:139
msgid ""
"${COUNT} existing volume groups have been found. Please indicate whether you "
"want to activate them."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:146
msgid "LVM configuration action:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:146
msgid "Summary of current LVM configuration:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:146
msgid ""
" Free Physical Volumes:  ${FREE_PVS}\n"
" Used Physical Volumes:  ${USED_PVS}\n"
" Volume Groups:          ${VGS}\n"
" Logical Volumes:        ${LVS}"
msgstr ""

#. Type: note
#. Description
#: ../partman-lvm.templates:157
msgid "Current LVM configuration:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:163
msgid "Devices for the new volume group:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:163
msgid "Please select the devices for the new volume group."
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:170
msgid "Volume group name:"
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:170
msgid "Please enter the name you would like to use for the new volume group."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:175
msgid ""
"No physical volumes were selected. The creation of a new volume group was "
"aborted."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:181
msgid "No volume group name entered"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:181
msgid "No name for the volume group has been entered.  Please enter a name."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:187
msgid "Volume group name already in use"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:187
msgid ""
"The selected volume group name is already in use. Please choose another name."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:193
msgid "Volume group name overlaps with device name"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:193
msgid ""
"The selected volume group name overlaps with an existing device name. Please "
"choose another name."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:199
msgid "The volume group ${VG} could not be created."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:207
msgid "Volume group to delete:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:207
msgid "Please select the volume group you wish to delete."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:212
msgid "The volume group may have already been deleted."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:220
msgid "Really delete the volume group?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-lvm.templates:220
msgid "Please confirm the ${VG} volume group removal."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:225
msgid "Error while deleting volume group"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:225
msgid ""
"The selected volume group could not be deleted. One or more logical volumes "
"may currently be in use."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:231
msgid "No volume group can be deleted."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:239
msgid "Volume group to extend:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:239
msgid "Please select the volume group you wish to extend."
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:245
msgid "Devices to add to the volume group:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:245
msgid "Please select the devices you wish to add to the volume group."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:252
msgid ""
"No physical volumes were selected. Extension of the volume group was aborted."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:258
msgid "Error while extending volume group"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:258
msgid ""
"The physical volume ${PARTITION} could not be added to the selected volume "
"group."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:264
msgid "No volume group has been found."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:264
msgid "No volume group can be reduced."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:272
msgid "Volume group to reduce:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:272
msgid "Please select the volume group you wish to reduce."
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:278
msgid "Devices to remove from the volume group:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:278
msgid "Please select the devices you wish to remove from the volume group."
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-lvm.templates:278
msgid "You can select one or more devices."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:285
msgid "No physical volumes selected"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:285
msgid ""
"No physical volumes were selected. Reduction of the volume group was aborted."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:291
msgid "Error while reducing volume group"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:291
msgid ""
"The physical volume ${PARTITION} could not be removed from the selected "
"volume group."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:299
msgid "No volume group found"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:299
msgid ""
"No free volume groups were found for creating a new logical volume. Please "
"create more physical volumes and volume groups, or reduce an existing volume "
"group."
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:306
msgid "Logical volume name:"
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:306
msgid "Please enter the name you would like to use for the new logical volume."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:312
msgid "Volume group:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:312
msgid ""
"Please select the volume group where the new logical volume should be "
"created."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:318
msgid "No logical volume name entered"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:318
msgid "No name for the logical volume has been entered.  Please enter a name."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:324
msgid ""
"The name ${LV} is already in use by another logical volume on the same "
"volume group (${VG})."
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:330
msgid "Logical volume size:"
msgstr ""

#. Type: string
#. Description
#: ../partman-lvm.templates:330
msgid ""
"Please enter the size of the new logical volume. The size may be entered in "
"the following formats: 10K (Kilobytes), 10M (Megabytes), 10G (Gigabytes), "
"10T (Terabytes). The default unit is Megabytes."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:337
msgid "Error while creating a new logical volume"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:337
msgid ""
"Unable to create a new logical volume (${LV}) on ${VG} with the new size "
"${SIZE}."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:345
msgid "No logical volume found"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:345
msgid ""
"No logical volume has been found.  Please create a logical volume first."
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:351
msgid "Logical volume:"
msgstr ""

#. Type: select
#. Description
#: ../partman-lvm.templates:351
msgid "Please select the logical volume to delete."
msgstr ""

#. Type: text
#. Description
#: ../partman-lvm.templates:356
msgid "in VG ${VG}"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:360
msgid "Error while deleting the logical volume"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:360
msgid "The logical volume (${LV}) on ${VG} could not be deleted."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:367
msgid "No usable physical volumes found"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:367
msgid ""
"No physical volumes (i.e. partitions) were found in your system. All "
"physical volumes may already be in use. You may also need to load some "
"required kernel modules or re-partition the hard drives."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:374
msgid "Logical Volume Manager not available"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:374
msgid ""
"The current kernel doesn't support the Logical Volume Manager. You may need "
"to load the lvm-mod module."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:380
msgid "Error while initializing physical volume"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:380
msgid "The physical volume ${PV} could not be initialized."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:380
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:387
msgid "Invalid logical volume or volume group name"
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:387
msgid ""
"Logical volume or volume group names may only contain alphanumeric "
"characters, hyphen, plus, period and underscore. They must be 128 characters "
"or less and may not begin with a hyphen. The names \".\" and \"..\" are not "
"allowed. In addition, logical volume names may not begin with \"snapshot\"."
msgstr ""

#. Type: error
#. Description
#: ../partman-lvm.templates:387
msgid "Please choose another name."
msgstr ""
