# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#: ../partman-ext2r0.templates:3
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:7
msgid "Go back to the menu and correct errors?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:7
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:7
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""

#. Type: error
#. Description
#: ../partman-ext2r0.templates:16
msgid "Failed to create a file system"
msgstr ""

#. Type: error
#. Description
#: ../partman-ext2r0.templates:16
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:22
msgid "Do you want to return to the partitioning menu?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:22
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:22
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""

#. Type: select
#. Choices
#. what's to be entered is a mount point
#: ../partman-ext2r0.templates:36
msgid "Enter manually"
msgstr ""

#. Type: select
#. Choices
#. "it" is a partition
#: ../partman-ext2r0.templates:36
msgid "Do not mount it"
msgstr ""

#. Type: string
#. Description
#: ../partman-ext2r0.templates:41
msgid "Mount point for this partition:"
msgstr ""

#. Type: error
#. Description
#: ../partman-ext2r0.templates:45
msgid "Invalid mount point"
msgstr ""

#. Type: error
#. Description
#: ../partman-ext2r0.templates:45
msgid "The mount point you entered is invalid."
msgstr ""

#. Type: error
#. Description
#: ../partman-ext2r0.templates:45
msgid "Mount points must start with \"/\". They cannot contain spaces."
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "noatime - do not update inode access times at each access"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "nodev - do not support character or block special devices"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "nosuid - ignore set-user-identifier or set-group-identifier bits"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "noexec - do not allow execution of any binaries"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "ro - mount the file system read-only"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "sync - all input/output activities occur synchronously"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "usrquota - user disk quota accounting enabled"
msgstr ""

#. Type: multiselect
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partman-ext2r0.templates:60
msgid "grpquota - group disk quota accounting enabled"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-ext2r0.templates:61
msgid "Mount options:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-ext2r0.templates:61
msgid "Mount options can tune the behavior of the file system."
msgstr ""

#. Type: text
#. Description
#. File system name
#: ../partman-ext2r0.templates:67
msgid "old Ext2 (revision 0) file system"
msgstr ""

#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:72
msgid "ext2r0"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:76
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:87
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:98
msgid "Go back to the menu and correct this problem?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:98
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-ext2r0.templates:98
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
