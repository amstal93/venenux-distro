Template: partman-ext2r0/progress_checking
Type: text
_Description: Checking the ext2 (revision 0) file system in partition #${PARTITION} of ${DEVICE}...

Template: partman-ext2r0/check_failed
Type: boolean
_Description: Go back to the menu and correct errors?
 The test of the file system with type ext2 (revision 0) in partition
 #${PARTITION} of ${DEVICE} found uncorrected errors.
 .
 If you do not go back to the partitioning menu and correct these errors,
 the partition will not be used at all.

Template: partman-ext2r0/create_failed
Type: error
_Description: Failed to create a file system
 The ext2 (revision 0) file system creation in partition
 #${PARTITION} of ${DEVICE} failed.

Template: partman-ext2r0/no_mount_point
Type: boolean
_Description: Do you want to return to the partitioning menu?
 No mount point is assigned for the ext2 (revision 0) file system in
 partition #${PARTITION} of ${DEVICE}.
 .
 If you do not go back to the partitioning menu and assign a mount point
 from there, this partition will not be used at all.

Template: partman-ext2r0/mountpoint
Type: select
#flag:translate!:1,2
#flag:comment:3
# what's to be entered is a mount point
#flag:comment:4
# "it" is a partition
__Choices: /, /boot, Enter manually, Do not mount it
_Description: Mount point for this partition:

Template: partman-ext2r0/mountpoint_manual
Type: string
_Description: Mount point for this partition:

Template: partman-ext2r0/bad_mountpoint
Type: error
_Description: Invalid mount point
 The mount point you entered is invalid.
 .
 Mount points must start with "/". They cannot contain spaces.

Template: partman-ext2r0/text/ext2r0
Type: text
# File system name (untranslatable in many languages)
_Description: ext2r0

Template: partman-ext2r0/options
Type: multiselect
# Note to translators : Please keep your translations of the choices
# below a 65 columns limit (which means 65 characters 
# in single-byte languages)
__Choices: noatime - do not update inode access times at each access, nodev - do not support character or block special devices, nosuid - ignore set-user-identifier or set-group-identifier bits, noexec - do not allow execution of any binaries, ro - mount the file system read-only, sync - all input/output activities occur synchronously, usrquota - user disk quota accounting enabled, grpquota - group disk quota accounting enabled
_Description: Mount options:
 Mount options can tune the behavior of the file system.

Template: partman/filesystem_long/ext2r0
Type: text
# File system name
_Description: old Ext2 (revision 0) file system

Template: partman/filesystem_short/ext2r0
Type: text
# Short file system name (untranslatable in many languages)
_Description: ext2r0

Template: partman-ext2r0/boot_not_ext2r0
Type: boolean
_Description: Go back to the menu and correct this problem?
 Your boot partition has not been configured with the old ext2 (revision 0)
 file system.  This is needed by your machine in order to boot.  Please go
 back and use the old ext2 (revision 0) file system.
 .
 If you do not go back to the partitioning menu and correct this error,
 the partition will be used as is.  This means that you may not be able
 to boot from your hard disk.

Template: partman-ext2r0/boot_not_first_partition
Type: boolean
_Description: Go back to the menu and correct this problem?
 Your boot partition is not located on the first primary partition of your
 hard disk.  This is needed by your machine in order to boot.  Please go
 back and use your first primary partition as a boot partition.
 .
 If you do not go back to the partitioning menu and correct this error,
 the partition will be used as is.  This means that you may not be able
 to boot from your hard disk.

Template: partman-ext2r0/root_not_primary
Type: boolean
_Description: Go back to the menu and correct this problem?
 Your root partition is not a primary partition of your hard disk.  This is
 needed by your machine in order to boot.  Please go back and use a
 primary partition for your root partition.
 .
 If you do not go back to the partitioning menu and correct this error,
 the partition will be used as is.  This means that you may not be able
 to boot from your hard disk.

