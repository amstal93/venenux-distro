# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:47+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#: ../partman-auto.templates:3
msgid "Please wait..."
msgstr ""

#. Type: text
#. Description
#: ../partman-auto.templates:7
msgid "Computing the new partitions..."
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:11
msgid ""
"This probably happened because the selected disk or free space is too small "
"to be automatically partitioned."
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:17
msgid "Failed to partition the selected disk"
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:17
msgid ""
"This probably happened because there are too many (primary) partitions in "
"the partition table."
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:24
msgid ""
"The installer can guide you through partitioning a disk (using different "
"standard schemes) or, if you prefer, you can do it manually. With guided "
"partitioning you will still have a chance later to review and customise the "
"results."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-auto.templates:36
msgid "Remove existing logical volume data?"
msgstr ""

#. Type: boolean
#. Description
#: ../partman-auto.templates:36
msgid ""
"The selected device already contains logical volumes and/or volume groups "
"from a previous LVM installation, which must be removed from the disk before "
"creating any partitions."
msgstr ""

#. Type: boolean
#. Description
#: ../partman-auto.templates:36
msgid ""
"Note that this will also permanently erase any data currently on the logical "
"volumes."
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:46
msgid "LVM physical volume already exists on the selected device"
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:46
msgid ""
"The selected device already contains one or more LVM physical volumes which "
"cannot be removed. It is therefore not possible to automatically partition "
"this device."
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:64
msgid "Partitioning method:"
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:64
msgid ""
"If you choose guided partitioning for an entire disk, you will next be asked "
"which disk should be used."
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:79
msgid "Partitioning scheme:"
msgstr ""

#. Type: select
#. Description
#. "Selected for partitioning" can be either an entire disk
#. of "the largest continuous free space" on an existing disk
#. TRANSLATORS, please take care to choose something appropriate for both
#.
#. It is followed by a variable giving the chosen disk, hence the colon
#. at the end of the sentence. Please keep it.
#: ../partman-auto.templates:79
msgid "Selected for partitioning:"
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:79
msgid ""
"The disk can be partitioned using one of several different schemes. If you "
"are unsure, choose the first one."
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:89
msgid "Unusable free space"
msgstr ""

#. Type: error
#. Description
#: ../partman-auto.templates:89
msgid ""
"Partitioning failed because the chosen free space may not be used. There are "
"probably too many (primary) partitions in the partition table."
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:108
msgid "Guided partitioning"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:113
msgid "Guided - use the largest continuous free space"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:118
msgid "Guided - use entire disk"
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:123
msgid "Select disk to partition:"
msgstr ""

#. Type: select
#. Description
#: ../partman-auto.templates:123
msgid ""
"Note that all data on the disk you select will be erased, but not before you "
"have confirmed that you really want to make the changes."
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-auto.templates:130
msgid "Select disk(s) to partition:"
msgstr ""

#. Type: multiselect
#. Description
#: ../partman-auto.templates:130
msgid ""
"Note that all data on the disk(s) you select will be erased, but not before "
"you have confirmed that you really want to make the changes."
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#. This is a method for partioning - as in guided (automatic) versus manual
#: ../partman-auto.templates:138
msgid "Manual"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:143
msgid "Automatically partition the free space"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:148
msgid "All files in one partition (recommended for new users)"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:153
msgid "Separate /home partition"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:158
msgid "Separate /home, /usr, /var, and /tmp partitions"
msgstr ""

#. Type: text
#. Description
#. TRANSLATORS: This is a menu entry. Keep in under 55 columns/characters
#: ../partman-auto.templates:163
msgid "Small-disk (< 1GB) partitioning scheme"
msgstr ""
