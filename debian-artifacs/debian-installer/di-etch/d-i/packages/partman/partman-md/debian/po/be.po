# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/be.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# translation of be-20061017-cristian-in.po to Belarusian
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Andrei Darashenka <adorosh2@it.org.by>, 2005, 2006.
# Pavel Piatruk <berserker@neolocation.com>, 2006.
# Nasciona Piatrouskaja <naska1@tut.by>, 2006.
# Hleb Rubanau <g.rubanau@gmail.com>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: be\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-01-06 22:48+0100\n"
"PO-Revision-Date: 2006-10-17 23:34+0300\n"
"Last-Translator: Andrei Darashenka <adorosh2@it.org.by>\n"
"Language-Team: Belarusian <i18n@mova.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"

#. Type: text
#. Description
#: ../partman-md.templates:3
msgid "Software RAID device"
msgstr "Прылада праграмнага RAID"

#. Type: text
#. Description
#: ../partman-md.templates:7
msgid "Configure software RAID"
msgstr "Наладка праграмнага RAID"

#. Type: boolean
#. Description
#: ../partman-md.templates:13
msgid "Write the changes to the storage devices and configure RAID?"
msgstr "Запісаць змены на прылады сховішчаў і наладзіць RAID?"

#. Type: boolean
#. Description
#: ../partman-md.templates:13
msgid ""
"Before RAID can be configured, the changes have to be written to the storage "
"devices.  These changes cannot be undone."
msgstr ""
"Перад тым, як наладжваць RAID, неабходна запісаць змены на прылады "
"сховішчаў. Гэтыя змены немагчыма будзе скасаваць."

#. Type: boolean
#. Description
#: ../partman-md.templates:28
msgid "Keep current partition layout and configure RAID?"
msgstr "Запісаць актуальную схему падзелаў на дыск і наладзіць RAID?"

#. Type: boolean
#. Description
#: ../partman-md.templates:28
msgid ""
"When RAID is configured, no additional changes to the partitions in the "
"disks containing physical volumes are allowed.  Please convince yourself "
"that you are satisfied with the current partitioning scheme in these disks."
msgstr ""
"Пасля наладкі RAID нельга будзе дадаткова змяняць ніякія падзелы на дысках, "
"дзе месцяцца фізічныя тамы. Пераканайцеся, што Вас задавальняе актуальная "
"схема падзелу гэтых дыскаў. "

#. Type: error
#. Description
#: ../partman-md.templates:36
msgid "RAID configuration failure"
msgstr "Памылка пры наладцы RAID"

#. Type: error
#. Description
#: ../partman-md.templates:36
msgid "An error occurred while writing the changes to the storage devices."
msgstr "Падчас запісу зменаў на фізічную прыладу адбылася памылка."

#. Type: error
#. Description
#: ../partman-md.templates:36
msgid "The configuration of RAID is aborted."
msgstr "Працэс наладкі RAID перарваны."

#. Type: text
#. Description
#: ../partman-md.templates:43
msgid "physical volume for RAID"
msgstr "фізічны том для RAID"

#. Type: text
#. Description
#: ../partman-md.templates:47
msgid "raid"
msgstr "raid"
