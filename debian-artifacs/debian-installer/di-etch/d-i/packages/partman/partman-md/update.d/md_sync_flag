#!/bin/sh

# This script removes the raid flag for partitions whose method is not
# raid and sets it for partition whose method is raid

. /lib/partman/definitions.sh

dev=$1
num=$2
id=$3
size=$4
type=$5
fs=$6
path=$7

cd $dev

if [ $fs = free ]; then
    exit 0
fi

method=
if [ -f $id/method ]; then
    method=$(cat $id/method)
fi

has_raid=no
flags=''
open_dialog GET_FLAGS $id
while { read_line flag; [ "$flag" ]; }; do
    if [ "$flag" != raid ]; then
	if [ "$flags" ]; then
	    flags="$flags
$flag"
	else
	    flags="$flag"
	fi
    else
	has_raid=yes
    fi
done
close_dialog

# Some flags make no sense in combination with raid
cleanflags=''
for flag in $flags; do
	if [ "$flag" = lvm ]; then
		continue
	elif [ "$flag" = swap ]; then
		continue
	elif [ -n "$cleanflags" ]; then
		cleanflags="$cleanflags
$flag"
	else
		cleanflags="$flag"
	fi
done

if [ "$method" = '' ] && [ "$has_raid" = yes ]; then
    echo raid >$dev/$id/method
    rm -f $dev/$id/use_filesystem
    rm -f $dev/$id/format
elif [ "$method" = raid -a "$has_raid" = no ]; then
    open_dialog SET_FLAGS $id
    write_line "$cleanflags"
    write_line raid
    write_line NO_MORE
    close_dialog
elif [ "$method" != raid -a "$has_raid" = yes ]; then
    open_dialog SET_FLAGS $id
    write_line "$flags"
    write_line NO_MORE
    close_dialog
fi
