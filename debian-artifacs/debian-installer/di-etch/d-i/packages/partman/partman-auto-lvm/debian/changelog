partman-auto-lvm (22) unstable; urgency=low

  [ Updated translations ]
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 18:16:15 +0100

partman-auto-lvm (21) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 12:20:12 +0100

partman-auto-lvm (20) unstable; urgency=low

  * Double hyphens in VG/LV names for the device name under /dev/mapper.
    Closes: #402410. Thanks to Mike Hommey for analysis.

  [ Updated translations ]
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Tue, 12 Dec 2006 11:39:07 +0100

partman-auto-lvm (19) unstable; urgency=low

  * Skip check for /boot partition for powerpc/prep systems as they have the
    kernel and initrd in the prep partition.

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Galician (gl.po) by Jacobo Tarrio
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Tue,  5 Dec 2006 23:28:24 +0100

partman-auto-lvm (18) unstable; urgency=low

  [ David Härdeman ]
  * Use hostname as the default VG name, fall back to Debian if not set.
    Closes: #381244.

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Basque (eu.po) by Piarres Beobide
  * Hindi (hi.po) by Nishant Sharma
  * Indonesian (id.po) by Arief S Fitrianto
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Albanian (sq.po) by Elian Myftiu
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 15:47:07 +0200

partman-auto-lvm (17) unstable; urgency=low

  * Display selected target for partitioning when choosing a recipe.

  [ Updated translations ]
  * Bengali (bn.po) by Jamil Ahmed
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Finnish (fi.po) by Tapio Lehtonen
  * Hindi (hi.po) by Nishant Sharma
  * Tamil (ta.po) by Damodharan Rajalingam

 -- Frans Pop <fjp@debian.org>  Sat, 14 Oct 2006 02:10:36 +0200

partman-auto-lvm (16) unstable; urgency=low

  * Show confirmation dialog before making changes to the partition table.
    Closes: #368633.
  * Change description for method.
  * Avoid double sourcing of common functions.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Wed, 11 Oct 2006 04:38:01 +0200

partman-auto-lvm (15) unstable; urgency=low

  [ David Härdeman ]
  * Fix broken <Go Back> logic.

  [ Frans Pop ]
  * Wipe disk only after selecting a recipe to allow backing out.
  * Remove standards-version and add Lintian override for it.
  * Add dependency for debconf.

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Fri, 15 Sep 2006 06:27:10 +0200

partman-auto-lvm (14) unstable; urgency=low

  [ David Härdeman ]
  * Use the lvm wiping functionality from partman-auto >= 55
  * Do not suggest part'ing RAID/LVM volumes
  * Make sure the /boot partition is not marked lvmok. Closes: #350450
  * Move most of the autopartition-lvm code to a separate file to allow
    more code sharing between autopartition-lvm and autopartition-crypto
  * Adapt to partman-auto two-level menu change

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Gujarati (gu.po) by Kartik Mistry
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Thu, 31 Aug 2006 17:18:31 -0400

partman-auto-lvm (13) unstable; urgency=low

  * Support preseeding of partman-auto-lvm/disk. Closes: #375265.

 -- Frans Pop <fjp@debian.org>  Mon, 26 Jun 2006 12:05:15 +0200

partman-auto-lvm (12) unstable; urgency=low

  * Use new function pv_on_device to check for existing PVs. Closes: #375002.
    (needs partman-lvm 40)

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Portuguese (pt.po) by Miguel Figueiredo
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş

 -- Frans Pop <fjp@debian.org>  Fri, 23 Jun 2006 19:21:51 +0200

partman-auto-lvm (11) unstable; urgency=low

  [ Christian Perrier ]
  * Fix English in templates. Thanks to Jens Seidel.
    Unfuzzy translations.

  [ David Härdeman ]
  * Move lvm_tools.sh to partman-lvm, change dependencies accordingly.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hungarian (hu.po) by SZERVÑC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Portuguese (pt.po) by Miguel Figueiredo
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Joey Hess <joeyh@debian.org>  Mon, 29 May 2006 18:22:53 -0400

partman-auto-lvm (10) unstable; urgency=low

  * If there are no recipes that support LVM, then don't offer auto-lvm.
    Requires partman-auto (50).
  * Take definition for /boot partition from regular recipes; if no /boot
    partition is wanted for regular auto partitioning, it can be marked
    $defaultignore. Requires partman-auto (51).
  * Add (probably redundant) check that disk is usable after wiping it.
  * Add myself to uploaders.

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by hok kakada
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Frans Pop <fjp@debian.org>  Sun,  9 Apr 2006 22:47:40 +0200

partman-auto-lvm (9) unstable; urgency=low

  * Fix code that checks if a /boot partition is already present in the
    selected scheme. Closes: #350444.
  * Improve handling of the VG name question. Thanks to Fabio M. Di Nitto.
  * Bail out in situations we cannot really deal with currently.
    Closes: #347479.
  * Use tab for indentation.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Punjabi (Gurmukhi) (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Frans Pop <fjp@debian.org>  Mon, 13 Feb 2006 12:41:56 +0100

partman-auto-lvm (8) unstable; urgency=low

  [ Colin Watson ]
  * The new update-dev that handles udev has been around for a while, so
    drop old calls to udevstart/udevsynthesize.
  * Discard error messages from trying to load dm-mod and lvm-mod modules
    (https://launchpad.net/distros/ubuntu/+source/partman-auto-lvm/+bug/29257).

  [ Frans Pop ]
  * Move some_device_lvm from 40 to 60 so autopartitioning without LVM is
    listed before autopartitioning with LVM (closes: #348648).

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hindi (hi.po) by Nishant Sharma
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Punjabi (Gurmukhi) (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Jan 2006 21:52:56 +0100

partman-auto-lvm (7) unstable; urgency=low

  * Set priority to standard so it will be available in standard installs.

 -- Joey Hess <joeyh@debian.org>  Wed, 14 Dec 2005 17:00:01 -0500

partman-auto-lvm (6) unstable; urgency=low

  [ Colin Watson ]
  * Call update-dev if available. udevstart/udevsynthesize calls will be
    removed from here soon.

  [ Fabio M. Di Nitto ]
  * Start implementing bits required to avoid code duplication with
    partman-auto as per discussion on debian-boot mailing list.
    (See http://lists.debian.org/debian-boot/2005/10/msg00842.html)
  * Remove arch barrier from automatically_partition/some_device_lvm/choices.
  * Switch the code to use lvmok tag.
  * Fix free_space calculation to not assume a totally empty disk.
  * Use wipe_disk, create_primary_partitions and create_partitions common
    function.
  * Move recipe check before performing any operation on disk and add a
    template to show the error.
  * Few lines of code cleanup and more comments on what's left.
  * Depends: partman-auto (46) for auto-shared.sh
  * Fix call to update-dev.

  [ Updated translations ]
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Joey Hess <joeyh@debian.org>  Wed,  7 Dec 2005 21:57:09 -0500

partman-auto-lvm (5) unstable; urgency=low

  * Make automatically_partition/some_device_lvm/* executable.

  [ Updated translations ]
  * Romanian (ro.po) by Eddy Petrişor

 -- Colin Watson <cjwatson@debian.org>  Thu, 24 Nov 2005 12:35:10 +0000

partman-auto-lvm (4) unstable; urgency=low

  * If udevstart is missing, try udevsynthesize instead to try to get udev
    to create pending device nodes.

  [ Updated translations ]
  * French (fr.po) by Christian Perrier
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes

 -- Colin Watson <cjwatson@debian.org>  Sun, 20 Nov 2005 12:30:13 +0000

partman-auto-lvm (3) unstable; urgency=low

  [ Frans Pop ]
  * Changed dependency from partman to partman-base.

  [ Fabio M. Di Nitto ]
  * Rename new_VG to VG_create (name more similar to vgcreate).
  * Avoid useless loop in VG_create.
  * Rename detach_PV to VG_reduce.
  * Rename attach_PV to VG_extend.
  * Rename new_LV to LV_create.
  * Rename delete_LV to LV_remove.
  * Flush udev queue in vg_all_free/do_option otherwise the devices might
    not be there yet.

  [ Joey Hess ]
  * Use log-output.
  * Merge remainder of Ubuntu patches from Fabio and Colin. Some manual
    merging due to other changes and the patch including some of the
    changes listed above. The other changes in this patch follow.

  [ Colin Watson ]
   * Disable ourselves on powerpc (closes: Ubuntu #14438).
   * Disable ourselves on powerpc in a slightly more effective way.

  [ Fabio M. Di Nitto ]
   * Add 40 some_device_lvm based on partman-auto counterpart.
   NOTE: all the next changes will make partman-auto-lvm useful only with
   lvm2 and the lvm_tools.sh API rename is done to keep a bit of consistency
   with what real lvm2 tools actually do.
   * Use vgs to simplify VG_list (lvm_tools.sh).
   * Use pvs to simplify VG_of_PV.
   * Simplify VG_reduce.
   * Implement the "full" option in LV_create.
   * Cleanup LV_list.
   NOTE: the following change requires partman-auto-41ubuntu3 or higher.
   * Use expand_scheme and clean_method from recipe.sh to remove duplicate
     code.
   * expand_scheme needs a valid free_size value to return a proper $scheme
     back. Also note the size hack in perform_recipe_by_lvm due to lvm
     rounding issues. In order to fill up the space, the last partition will
     be created using all the free_size and a 3% of the overall space is
     reserved to ensure that the last partition is big enough.
     There might be better methods to calculate, but right now this one seems
     to fullfil its task.
   * Map devfs names to real devices in vg_all_free/do_option since lvm2
     tools don't like devfs names.
   * Create a separate /boot and a vg on the selected device.
   * Allign vg creation between vg_all_free and some_device_lvm.
   * Add notes on scheme manipulation.
   * Remove wrong call to clean_method in perform_recipe_by_lvm.
   * Fix device mapper naming.
   * Restart partman at the end of the process. This is required to get some
     LVM info right.
   * Add missing noheadings to LV_create.
   * Strip the decimal in LV_create.
   * Try to give more sensible names to partitions that are not mounted.
   * Add db_progress info to perform_recipe_by_lvm.
   * Fix partition size calculation using kbytes instead of Mbytes.
     This operation removes any weird hack in calculating the partitions.
   * brownpaper bag.. it's ok to switch to kbytes.. but not everything can
     handle so big numbers.
   * Remove "Use free space option".
     (see http://bugzilla.ubuntu.com/show_bug.cgi?id=15017)

  [ Joey Hess ]
  * Add lvm2-udeb to deps since this now needs lvm2
  * Version dependency on partman-auto due to above changes.
  * Remove currently dead code for vg_all_free, and add TODO so we don't
    forget to try to fix it sometime.

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.
  * Use stop_parted_server and restart_partman functions from partman-base
    73 instead of duplicating the code.

  [ Frans Pop ]
  * Use gpt instead of msdos disklabel for disks larger than 2TB.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Catalan (ca.po) by Guillem Jover
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Persian (fa.po) by Arash Bijanzadeh
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Indonesian (id.po) by Arief S Fitrianto
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Joey Hess <joeyh@debian.org>  Tue, 15 Nov 2005 19:25:46 -0500

partman-auto-lvm (2) unstable; urgency=low

  * Colin Watson
    - Change "new Debian system" to "new system" to help with derivative
      branding.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Guillem Jover
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Jacobo Tarrio
    - Indonesian (id.po) by Arief S Fitrianto
    - Italian (it.po) by Giuseppe Sacco
    - Japanese (ja.po) by Kenshi Muto
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter Mann
    - Albanian (sq.po) by Elian Myftiu
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall

 -- Colin Watson <cjwatson@debian.org>  Wed,  6 Jul 2005 11:57:42 +0100

partman-auto-lvm (1) unstable; urgency=low

  * Anton Zinoviev
    - initial version made at Debcamp 2004
  * Joey Hess
    - various bug fixes
  * Colin Watson
    - Lower priority to optional for now.

 -- Colin Watson <cjwatson@debian.org>  Mon, 23 May 2005 09:30:34 +0100
