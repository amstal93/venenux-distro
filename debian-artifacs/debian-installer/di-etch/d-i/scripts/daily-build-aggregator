#!/usr/bin/perl
# This program downloads summary log files for the daily image builds, and
# constructs a web page overview of the d-i builds. If you run a daily
# build, add the info for your build to the list. 
#
# The url field points to the url your build can be downloaded from, the
# email address is a contact address, the description is a breif
# description of the build. The logurl points to wherever log files are.
# The logext is an extention that is appended to the log filename.
#
# In the log directory you should have a summary$logext file, that contains
# lines in the following format:
#    arch (date) user@host ident status
#    
# Where ident describes what is being built, arch is the architecture that
# it is being built for, date is the output of the date command at the end
# of the build (in the C locale), user@host is who did the build, and
# status describes how it went (usually "success" or "failed"). A log file
# for the build should in in the file named ident$logext in the log directory.
#
# Example:
# i386 (Thu Apr 22 21:08:03 EDT 2004) joey@kite build_floppy_boot success
# i386 (Thu Apr 22 21:08:33 EDT 2004) joey@kite build_floppy_speakup success
# i386 (Thu Apr 22 21:09:29 EDT 2004) joey@kite build_floppy_root success

# Note: only add items to the end of the list, do not reorder items or it
# will mess up the stats file used for the graph.
my @buildlist = (
	{
		url => 'http://people.debian.org/~vorlon/d-i/alpha/daily/',
		logurl => 'http://people.debian.org/~vorlon/d-i/alpha/daily/',
		email => 'vorlon@debian.org',
		description => 'daily alpha images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~aba/d-i/images/daily/',
		logurl => 'http://people.debian.org/~aba/d-i/images/daily/',
		email => 'aba@not.so.argh.org',
		description => 'daily amd64 images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},
	
	{
		url => 'http://people.debian.org/~kmuto/d-i/images/daily/',
		logurl => 'http://people.debian.org/~kmuto/d-i/images/daily/',
		email => 'kmuto@debian.org',
		description => 'daily arm images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},
	
	{
		url => 'http://people.debian.org/~kyle/d-i/hppa/daily/',
		logurl => 'http://people.debian.org/~kyle/d-i/hppa/daily/',
		email => 'kyle@debian.org',
		description => 'daily hppa images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},
	
	{
		url => 'http://people.debian.org/~joeyh/d-i/images/daily/',
		logurl => 'http://people.debian.org/~joeyh/d-i/images/daily/',
		email => 'joeyh@debian.org',
		description => 'bi-daily i386 images build',
		logext => ".log",
		frequency => 0.5,
		notes => '',
	},
	
	{
		url => 'http://people.debian.org/~jbailey/d-i/ia64/daily/',
		logurl => 'http://people.debian.org/~jbailey/d-i/ia64/daily/',
		email => 'jbailey@debian.org',
		description => 'daily ia64 images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~smarenka/d-i/images-m68k/daily/',
		logurl => 'http://people.debian.org/~smarenka/d-i/images-m68k/daily/',
		email => 'smarenka@debian.org',
		description => 'daily m68k images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~ths/d-i/images/daily/',
		logurl => 'http://people.debian.org/~ths/d-i/images/daily/',
		email => 'ths@debian.org',
		description => 'daily mips images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~ths/d-i/mipsel/images/daily/',
		logurl => 'http://people.debian.org/~ths/d-i/mipsel/images/daily/',
		email => 'ths@debian.org',
		description => 'daily mipsel images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~wouter/d-i/powerpc/daily/',
		logurl => 'http://people.debian.org/~wouter/d-i/powerpc/daily/',
		email => 'wouter@debian.org',
		description => 'daily powerpc images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://lophos.multibuild.org/d-i/images/daily/',
		logurl => 'http://lophos.multibuild.org/d-i/images/daily/',
		email => 'waldi@debian.org',
		description => 'daily s390 images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},

	{
		url => 'http://people.debian.org/~stappers/d-i/images/daily/',
		logurl => 'http://people.debian.org/~stappers/d-i/images/daily/',
		email => 'stappers@debian.org',
		description => 'daily sparc images build',
		logext => ".log",
		frequency => 1,
		notes => '',
	},
	
	{
		url => 'http://cdimage.debian.org/cdimage/',
		logurl => 'http://farbror.acc.umu.se/cdimage-log/',
		email => '93sam@debian.org',
		description => 'CD image builds',
		logext => "",
		frequency => 7, # some build only weekly
		notes => '',
	},
	
#	{
#		url => 'http://somehost/',
#		logurl => 'http://somehost/',
#		email => 'you@debian.org',
#		description => 'put something informative here',
#		logext => ".log",
#		frequency => 1,
#	},
);

use warnings;
use strict;
require "aggregator.pl";

my $basename=shift;
if (! defined $basename) {
	die "usage: $0 basename\n";
}

open (OUT, ">$basename.html.new") || die "$basename.html.new: $!";

print OUT <<EOS;
<html>
<head>
<title>debian-installer build overview</title>
</head>
<body>
<ul>
EOS

my ($total, $failed, $success, $old) = aggregate(*OUT, $basename, @buildlist);

my $date=`LANG=C TZ=GMT date`;
chomp $date;
print OUT <<"EOS";
</ul>

Totals: $total builds ($failed failed, $old old)

<hr>

See also: <a href="http://buildd.debian.org/build.php?arch=&pkg=debian-installer">d-i autobuild logs</a>
and <a href="http://people.debian.org/~igloo/status.php?packages=debian-installer">d-i buildd status</a>

<hr>
$date
</body>
</html>
EOS

close OUT;
rename("$basename.html.new", "$basename.html") || die "rename: $!";
