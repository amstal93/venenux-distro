# Directory configuration
ARCH=$(dpkg --print-architecture)
. conf-$ARCH.sh

KABI=$(echo $KERNELVER|cut -d- -f2)
BASE=$HOME/d-i/d-i.bpo/etch-$ARCH
DI=$BASE/d-i
DEBIANCD=$BASE/debian-cd
DISCOVER=$BASE/discover
INITRAMFS=$BASE/initramfs-tools
MDADM=$BASE/mdadm
DEBCONF=$BASE/debconf
PCIUTILS=$BASE/pciutils
BPOCONFIG=$BASE/base-config
PARTED=$BASE/parted
LOCALAPT=$BASE/local-apt
LOCALPACKAGES=$LOCALAPT/local-deb
TMPMOUNT=/cdrom
STAMPDIR=$LOCALAPT/stamp

case "$ARCH" in
i386)
	ORIGISO=http://hanzubon.jp/debian-cd/4.0_r2/i386/iso-cd/debian-40r2-i386-netinst.iso
	;;
amd64)
	ORIGISO=http://hanzubon.jp/debian-cd/4.0_r2/amd64/iso-cd/debian-40r2-amd64-netinst.iso
	;;
esac
ISOIMAGE=$(echo "$ORIGISO" | sed -e "s/.*\///")
CDIMAGE=$BASE/cdimage
CDPOOLDIR=$CDIMAGE/archive/pool/main
LOCALCDIMAGE=$CDIMAGE/etch-custom-`date +'%m%d'`.iso

CP="cp -lf"
LOCALAPTOPT="-o Dir::Etc::SourceList=$LOCALAPT/sources.list \
	-o Dir::Cache::Archives=$LOCALAPT/cache \
	-o Dir::State::Lists=$LOCALAPT/lists \
	-o Dir::State=$LOCALAPT/state \
	-o APT::GPGV::TrustedKeyring=$LOCALAPT/trusted.gpg \
	-o Debug::NoLocking=true"
BPOPOOL="http://www.backports.org/debian/pool/main/"
INDICES="http://cdn.debian.or.jp/debian/indices/"
