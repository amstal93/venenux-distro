<!-- original version: 39622 -->

 <sect1 id="non-debian-partitioning">
 <title>Uw harde schijf vooraf indelen voor een multi-boot systeem</title>
<para>

Met 'het indelen van uw harde schijf' wordt bedoeld het verdelen van de
totale capaciteit van uw schijf in parten. Elk part is vervolgens
onafhankelijk van de andere. Het is enigszins vergelijkbaar met het bouwen
van muren in een huis; als u daarna meubelen in één kamer plaatst, heeft
dit geen invloed op de andere kamers.

</para><para arch="s390">

Waar in deze paragraaf wordt gesproken over <quote>harde schijven</quote>,
dient u dit voor de &arch-title;-wereld te lezen als DASDs of VM-minidisks.
Analoog dient u systeem of machine te lezen als LPAR of VM-guest.

</para><para>

Als er al een besturingssysteem op uw systeem aanwezig is

<phrase arch="x86">
(Windows 9x, Windows NT/2000/XP, OS/2, MacOS, Solaris, FreeBSD, &hellip;)
</phrase>

<phrase arch="alpha">
(Tru64 (Digital UNIX), OpenVMS, Windows NT, FreeBSD, &hellip;)
</phrase>

<phrase arch="s390">
(VM, z/OS, OS/390, &hellip;)
</phrase>

<phrase arch="m68k">
(Amiga OS, Atari TOS, Mac OS, &hellip;)
</phrase>

en u wilt Linux op dezelfde harde schijf installeren, dan zult u de schijf
moeten herindelen. Debian vereist eigen partities op de harde schijf.
Het kan niet worden geïnstalleerd op Windows- of MacOS-partities. Sommige
partities zouden kunnen worden gedeeld met andere Linux systemen, maar dit
valt buiten de scope van deze handleiding. U zult tenminste een afzonderlijke
partitie nodig hebben voor het root-bestandssysteem van Debian.

</para><para>

U kunt informatie over uw huidige partitie-indeling vinden door gebruik te maken
van een schijfindelingsprogramma voor uw huidige besturingssysteem<phrase
arch="x86">, zoals fdisk of PartitionMagic</phrase><phrase
arch="powerpc">, zoals Drive Setup, HD Toolkit of MacTools</phrase><phrase
arch="m68k">, zoals HD SC Setup, HDToolBox of SCSITool</phrase><phrase
arch="s390">, zoals de VM diskmap</phrase>. Schijfindelingsprogramma's
beschikken altijd over een functie om bestaande partities te tonen zonder
wijzigingen aan te brengen.

</para><para>

Algemeen geldt dat het wijzigen van een partititie waarop reeds een bestandssysteem
aanwezig is, de daarop aanwezige informatie zal vernietigen. Het is daarom raadzaam
om altijd reservekopieën te maken voordat u een schijf gaat herindelen. Als we nogmaals
de analogie van het huis gebruiken: waarschijnlijk zou u eerst alle meubelen opzij
zetten voordat u een muur uitbreekt om het risico dat meubelen worden beschadigd uit te
sluiten.

</para><para arch="hppa" condition="FIXME">

<emphasis>FIXME: write about HP-UX disks?</emphasis>

</para><para>

Als uw computer over meer dan één harde schijf beschikt, zou u één daarvan
volledig kunnen reserveren voor Debian. Als dat het geval is, hoeft u deze
harde schijf niet in te delen voordat u het installatiesysteem opstart; het
schijfindelingsprogramma van het installatiesysteem kan dit zonder problemen
verzorgen.

</para><para>

Als uw machine over slechts één harde schijf beschikt en u het bestaande
besturingssysteem volledig wilt vervangen door &debian;, kunt u eveneens het
indelen van de schijf uitstellen tot tijdens de installatieprocedure
(<xref linkend="partman"/>), dus nadat u de computer heeft opgestart met het
installatiesysteem. Dit kan echter alleen als u van plan bent om het
installatiesysteem op te starten vanaf magneetband, CD of vanaf een andere met uw
computer verbonden machine. Bedenk het volgende: als u de computer opstart met
behulp van bestanden op de harde schijf en vervolgens deze harde schijf opnieuw
indeelt vanuit het installatiesysteem en daarmee de opstartbestanden verwijdert,
dan moet u maar hopen dat de installatie in één keer goed gaat. U zou in deze
situatie tenminste moeten beschikken over een alternatieve methode om uw
machine weer tot leven te wekken, zoals de originele installatietapes of -CDs
van het systeem.
<!-- FJP: naast tape, CD ook diskette als opstartbron noemen (2 maal). //-->

</para><para>

Als op uw machine reeds meerdere partities aanwezig zijn en er kan voldoende
ruimte worden vrijgemaakt door één of meerdere daarvan te verwijderen en
vervangen, dan kunt u eveneens gebruik maken van het schijfindelingsprogramma
van het Debian installatiesysteem. U wordt echter aangeraden om toch de
informatie hieronder door te lezen omdat er bijzondere omstandigheden kunnen
zijn &mdash; zoals de volgorde van bestaande partities in de partitie-index
&mdash; waardoor u alsnog wordt gedwongen om te herindelen vóór de installatie.

</para><para arch="x86">

Als uw machine een FAT of NTFS bestandssysteem heeft, zoals wordt gebruikt door
DOS en Windows, kunt u wachten en het schijfindelingsprogramma van het Debian
installatiesysteem gebruiken om de grootte van het bestandssysteem te wijzigen.

</para><para>

Als geen van bovenstaande situaties van toepassing is, zult u, om ruimte te
creëren voor Debian-partities, uw harde schijf moeten herindelen voordat u met
de installatie begint. Als sommige van de partities bestemd zijn voor andere
besturingssystemen, zou u deze moeten creëren met behulp van de eigen
schijfindelingsprogrammatuur van die besturingssystemen. Wij adviseren u
<emphasis>niet</emphasis> te proberen om partities voor &debian; te maken met
de programma's van een ander besturingssysteem. Beperkt u zich tot het maken van
de partities die u wilt behouden voor het oorspronkelijke besturingssysteem.

</para><para>

Als u meerdere besturingssystemen op dezelfde machine wilt installeren, wordt
aangeraden om eerst alle andere systemen te installeren voordat u verder gaat
met de installatie van Linux. Windows en andere besturingssystemen kunnen de
mogelijkheid om Linux op te starten verstoren, of kunnen u aanmoedigen om
'vreemde' partities opnieuw te fomateren.

</para><para>

Het is mogelijk om dergelijke problemen te herstellen of te voorkomen, maar u
bespaart uzelf moeite door het oorspronkelijke besturingssysteem eerst te
installeren.

</para><para arch="powerpc">

Om &debian; automatisch te laten opstarten door OpenFirmware, dienen de
Linux partities zich te bevinden vóór alle andere partities, in het
bijzonder MacOS opstartpartities. U dient hiermee rekening te houden
wanneer u de schijfindeling voorbereidt: u zou  ten behoeve van Linux een
dummy-partitie moeten maken die <emphasis>voor</emphasis> de andere
opstartpartities op de harde schijf komt. (De kleine partities die zijn
gereserveerd voor de besturingsprogramma's van de harde schijven van Apple
zijn geen opstartpartities.) U kunt deze dummy-partitie later, tijdens de
eigenlijke installatie, verwijderen en vervangen door de Linux partities.

</para><para>

Als u op dit moment beschikt over een harde schijf met één partitie (een
gebruikelijke situatie voor desktop systemen) en u wilt kunnen opstarten met
zowel het huidige besturingssysteem als met Debian, dan zult u de volgende
stappen moeten doorlopen.

  <orderedlist>
<listitem><para>

Maak een reservekopie van alles op de computer.
</para></listitem>
<listitem><para>

Start de computer op met behulp van het installatiemedium (zoals een CD of
magneetband) van het oorspronkelijke besturingssysteem.

<phrase arch="powerpc">Als u de computer opstart vanaf een MacOS CD, houd dan
de <keycap>c</keycap> toets ingedrukt tijdens het opstarten om af te dwingen
dat de CD het actieve MacOS systeem wordt.</phrase>

</para></listitem>
<listitem><para>

Gebruik de schijfindelingsprogramma's behorend bij het oorspronkelijke
besturingssysteem om partities daarvoor te maken. Maak ten behoeve van &debian;
een dummy-partitie of laat ongebruikte ruimte vrij.

</para></listitem>
<listitem><para>

Installeer het oorspronkelijke besturingssysteem op haar nieuwe partitie.

</para></listitem>
<listitem><para>

Start het oorspronkelijke besturingssysteem opnieuw om te controleren dat
alles in orde is en om de opstartbestanden van Debian te downloaden.
<!-- FJP: Waarom nu opeens downloaden? Is helemaal geen onderdeel van dit traject!
Het lijkt me logischer om eerst een restore te doen. //-->

</para></listitem>
<listitem><para>

Start het Debian installatiesysteem op om te vervolgen met de installatie
van Debian.

</para></listitem>
</orderedlist>

</para>

&nondeb-part-alpha.xml;
&nondeb-part-x86.xml;
&nondeb-part-m68k.xml;
&nondeb-part-sparc.xml;
&nondeb-part-powerpc.xml;

 </sect1>
