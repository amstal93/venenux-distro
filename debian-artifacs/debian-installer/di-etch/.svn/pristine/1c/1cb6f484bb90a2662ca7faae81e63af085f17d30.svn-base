<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43790 -->

 <sect1 id="minimum-hardware-reqts">
<title>必要な最低限のハードウェア</title>
<para>

<!--
Once you have gathered information about your computer's hardware,
check that your hardware will let you do the type of installation
that you want to do.
-->
コンピュータのハードウェアに関する情報が集まったら、
そのハードウェアが今から行おうとしている
インストールの条件に足るものであるかどうかをチェックしましょう。

</para><para arch="not-s390">

<!--
Depending on your needs, you might manage with less than some of the
recommended hardware listed in the table below. However, most users
risk being frustrated if they ignore these suggestions.
-->
やむを得ない場合は、以下に載っているリストよりは
性能の劣るハードウェアでなんとかしなければならないこともあるでしょう。
しかし、これらのお奨めを無視した場合は、
結局不満を感じる可能性が高くなってしまうと思います。

</para><para arch="x86">

<!--
A Pentium 4, 1GHz system is the minimum recommended for a desktop
systems.
-->
デスクトップシステムには最低 Pentium 4, 1GHz をお奨めします。

</para><para arch="m68k">

<!--
A 68030 or better processor is recommended for m68k
installs. You may get by with a little less drive space than shown.
-->
m68k のインストールには 68030 以上のプロセッサをお勧めします。
以下に示してあるドライブ容量よりは、
多少、少なくてもなんとかなるかもしれません。

</para><para arch="powerpc">

<!--
Any OldWorld or NewWorld PowerPC can serve well as a desktop system.
-->
デスクトップシステムには OldWorld PowerPC, NewWorld PowerPC
のいずれのモデルでも充分でしょう。

</para>

<table>
<!--
<title>Recommended Minimum System Requirements</title>
-->
<title>最低限必要なシステム (推奨値)</title>
<tgroup cols="4">
<thead>
<row>
<!--
  <entry>Install Type</entry>
  <entry>RAM (minimal)</entry>
  <entry>RAM (recommended)</entry>
  <entry>Hard Drive</entry>
-->
  <entry>インストールタイプ</entry>
  <entry>RAM (最小)</entry>
  <entry>RAM (推奨)</entry>
  <entry>ハードディスク</entry>
</row>
</thead>

<tbody>
<row>
<!--
  <entry>No desktop</entry>
  <entry>64 megabytes</entry>
  <entry>256 megabytes</entry>
  <entry>1 gigabyte</entry>
-->
  <entry>デスクトップなし</entry>
  <entry>64 メガバイト</entry>
  <entry>256 メガバイト</entry>
  <entry>1 ギガバイト</entry>
</row><row arch="not-s390">
<!--
  <entry>With Desktop</entry>
  <entry>64 megabytes</entry>
  <entry>512 megabytes</entry>
  <entry>5 gigabyte</entry>
-->
  <entry>デスクトップあり</entry>
  <entry>64 メガバイト</entry>
  <entry>512 メガバイト</entry>
  <entry>5 ギガバイト</entry>
</row>

</tbody></tgroup></table>

<para>

<!--
The actual minimum memory requirements are a lot less then the numbers
listed in this table. Depending on the architecture, it is possible to
install Debian with as little as 20MB (for s390) to 48MB (for i386 and
amd64). The same goes for the disk space requirements, especially if you
pick and choose which applications to install; see
<xref linkend="tasksel-size-list"/> for additional information on disk
space requirements.
-->
実際に必要な最小メモリはこの表に挙げた物よりも少なくなります。
アーキテクチャに依存しますが、最小 20MB (s390) から 48MB (i386, amd64) で
Debian をインストールできます。
必要なディスクスペースにも同じことが言え、
特にインストールするアプリケーションを選択する場合、
必要なディスクスペースについての追加情報は、
<xref linkend="tasksel-size-list"/> をご覧ください。

</para><para arch="not-s390">

<!--
It is possible to run a graphical desktop environment on older or
low-end systems, but in that case it is recommended to install
a window manager that is less resource-hungry than those of the
GNOME or KDE desktop environments; alternatives include
<classname>xfce4</classname>, <classname>icewm</classname> and
<classname>wmaker</classname>, but there are others to choose from.
-->
旧式ないしローエンドシステムでも、
グラフィカルデスクトップ環境を実行できますが、
GNOME や KDE といったデスクトップ環境よりも、
リソースを消費しないウィンドウマネージャをインストールするのをお奨めします。
代替品には、<classname>xfce4</classname>, <classname>icewm</classname>, 
<classname>wmaker</classname> が含まれますが、他にも選択できます。

</para><para>

<!--
It is practically impossible to give general memory or disk space
requirements for server installations as those very much depend on
what the server is to be used for.
-->
サーバが何に使用されるかによって、サーバのインストール時に必要な、
大量の一般的なメモリやディスク領域を与えるのは実際には不可能です。

</para><para>

<!--
Remember that these sizes don't include all the other materials which
are usually to be found, such as user files, mail, and data.  It is
always best to be generous when considering the space for your own
files and data.
-->
これらのサイズには、通常存在するユーザファイル、メール、
データなどは含まれていないことにご注意ください。
自分のファイルやデータに必要な容量は、
気前良く確保しておくに越したことはありません。

</para><para>

<!--
Disk space required for the smooth operation of the &debian; system
itself is taken into account in these recommended system requirements.
Notably, the <filename>/var</filename> partition contains
a lot of state information specific to Debian in addition to its regular
contents, like logfiles.  The
<command>dpkg</command> files (with information on all installed
packages) can easily consume 40MB. Also,
<command>apt-get</command> puts downloaded packages here before they are
installed. You should
usually allocate at least 200MB for <filename>/var</filename>, and a lot
more if you install a graphical desktop environment.
-->
&debian; システムを円滑に操作するのに必要なディスクスペースについては、
お奨めするシステム要件で考慮されています。
特に、<filename>/var</filename> パーティションには、
ログファイルのような一般的な内容に加え、Debian 特有の状態情報が多く置かれます。
<command>dpkg</command> のファイル 
(インストールされたパッケージすべてに関する情報)
は、簡単に 40MB を消費します。
また <command>apt-get</command> は、
インストールする前にダウンロードしたパッケージをここに置きます。
<filename>/var</filename> には最低 200MB は割り当てておくべきですし、
グラフィカルデスクトップ環境をインストールする場合には、
もっと割り当てるべきでしょう。

</para>

 </sect1>

