<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 untranslated -->


  <sect2 arch="alpha"><title>Partitioning in Tru64 UNIX</title>
<para>

Tru64 UNIX, formerly known as Digital UNIX, which is in turn formerly
known as OSF/1, uses the partitioning scheme similar to the BSD `disk
label', which allows for up to eight partitions per disk drive.  The
partitions are numbered `1' through to `8' in
Linux and ``lettered'' `a' through to `h' in UNIX.
Linux kernels 2.2 and higher always correspond `1' to
`a', `2' to `b' and so on.  For
example, <filename>rz0e</filename> in Tru64 UNIX would most likely be called
<filename>sda5</filename> in Linux.

</para><para>

Partitions in the disk label may overlap.  Moreover, the `c' partition
is required to span the entire disk (thus overlapping all other
non-empty partitions).  Under Linux this makes <filename>sda3</filename>
identical to <filename>sda</filename> (<filename>sdb3</filename> to <filename>sdb</filename>, if
present, and so on).  Apart from satisfying this requirement, you
should carefully avoid creating overlapping partitions.

</para><para>

Another conventional requirement is for the `a' partition to start from
the beginning of the disk, so that it always includes the boot block
with the disk label. If you intend to boot Debian from that disk, you
need to size it at least 2MB to fit aboot and perhaps a kernel.

</para><para>

Note that these two partitions are only required for compatibility;
you must not put a file system onto them, or you'll destroy data.  If
you're not going to share the disk with Tru64 Unix or one of the free
4.4BSD-Lite derived operating systems (FreeBSD, OpenBSD, or NetBSD),
you can ignore these requirements, and use the partitioning tool from
the Debian boot disks.  See <xref linkend="partition-programs"/> for details.

</para><para>

Disks can be partitioned with the graphical disk configuration tool that
is accessible through the Application Manager, or with the command-line
<command>disklabel</command> utility.  Partition type for the Linux file
system should be set to `resrvd8'.  This can only be done via
<command>disklabel</command>; however, all other configuration can easily be
performed with the graphical tool.

</para><para>

It is possible, and indeed quite reasonable, to share a swap partition
between UNIX and Linux.  In this case it will be needed to do a
<command>mkswap</command> on that partition every time the system is rebooted
from UNIX into Linux, as UNIX will damage the swap signature.  You may
want to run <command>mkswap</command> from the Linux start-up scripts before
adding swap space with <command>swapon -a</command>.

</para><para>

If you want to mount UNIX partitions under Linux, note that Digital UNIX
can use two different file system types, UFS and AdvFS, of which Linux
only understands the former.

</para>
  </sect2>

  <sect2 arch="alpha"><title>Partitioning in Windows NT</title>

<para>

Windows NT uses the PC-style partition table.  If you are manipulating
existing FAT or NTFS partitions, it is recommended that you use the
native Windows NT tools (or, more conveniently, you can also
repartition your disk from the AlphaBIOS setup menu).  Otherwise, it
is not really necessary to partition from Windows; the Linux
partitioning tools will generally do a better job.  Note that when you
run NT, the Disk Administrator may offer you to write a ``harmless
signature'' on non-Windows disks if you have any.  <emphasis>Never</emphasis> let
it do that, as this signature will destroy the partition information.

</para><para>

If you plan to boot Linux from an ARC/AlphaBIOS/ARCSBIOS console, you
will need a (small) FAT partition for MILO.  5 Mb is quite
sufficient.  If Windows NT is installed, its 6 Mb bootstrap partition
can be employed for this purpose.

</para>
  </sect2>
