<!-- retain these comments for translator revision tracking -->
<!-- original version: 43773 -->

 <sect1 id="installation-media">
 <title>Installationsmedien</title>

<para>

Dieses Kapitel wird Ihnen helfen, festzustellen, welche unterschiedlichen
Medientypen Sie nutzen können, um Debian zu installieren. Wenn Sie beispielsweise
ein Diskettenlaufwerk in Ihrem Rechner haben, können Sie es benutzen, um Debian
zu installieren. Den Installationsmedien ist ein eigenes Kapitel gewidmet
(<xref linkend="install-methods"/>), das die Vor- und Nachteile jedes Medientyps
auflistet. Sie können noch einmal hierher zurückblättern, wenn Sie das Kapitel
gelesen haben.

</para>

  <sect2 condition="supports-floppy-boot"><title>Disketten</title>
<para>

In einigen Fällen müssen Sie zunächst von Disketten booten.
Normalerweise ist alles, was Sie benötigen, ein High-Density- (1440 Kilobytes)
3,5 Zoll-Diskettenlaufwerk.

</para><para arch="powerpc">

Unter CHRP ist die Disketten-Unterstützung momentan nicht nutzbar.

</para>
  </sect2>

  <sect2><title>CD-ROM/DVD-ROM</title>

<note><para>

Wann immer in diesem Manual die Rede von <quote>CD-ROM</quote> ist, ist damit
<quote>CD-ROM oder DVD-ROM</quote> gemeint, da beide Technologien aus Sicht
des Betriebssystems fast gleich sind (mit Ausnahme einiger
sehr alter nicht-standardkonformer CD-ROM-Laufwerke, die weder SCSI- noch
IDE/ATAPI-kompatibel sind).

</para></note><para>

Eine CD-ROM-basierte Installation wird für einige Architekturen unterstützt.
Auf Geräten, die das Starten von CD-ROM erlauben, sollte es
Ihnen möglich sein, eine Installation komplett
<phrase arch="non-s390">ohne Disketten</phrase>
<phrase arch="s390">ohne Tapes</phrase>
durchzuführen. Sogar auf einem System, das das Starten von CD-ROM nicht
unterstützt, können Sie die CD-ROM in Kombination mit den anderen
Techniken verwenden, um Ihr System zu installieren, sobald Sie auf
anderem Wege gebootet haben; siehe <xref linkend="boot-installer"/>.

</para><para arch="x86"> 

Sowohl SCSI- als auch IDE/ATAPI-CD-ROMs werden unterstützt. Zusätzlich
werden alle nicht-standardkonformen CD-Anschlüsse, die von Linux unterstützt
werden (wie die Mitsumi- und Matsushita-Laufwerke), auch von den
Boot-Disketten unterstützt. Diese Modelle können jedoch spezielle
Boot-Parameter oder andere Einstellungen benötigen, um sie zum Laufen
zu bringen, und das Booten von diesen Nicht-Standard-Anschlüssen
ist eher unwahrscheinlich. Das <ulink url="&url-cd-howto;">Linux-CD-ROM-HowTo</ulink>
enthält ausführliche Informationen über die Verwendung von CD-ROMs unter Linux.

</para><para arch="x86"> 

Es können auch externe USB-CD-ROM-Geräte verwendet werden, genauso wie
FireWire-Geräte, die von den ohci1394 und sbp2-Treibern unterstützt werden.

</para><para arch="alpha">

Sowohl SCSI- als auch IDE/ATAPI-CD-ROMs werden unter &arch-title; unterstützt,
sofern der Controller von der SRM-Konsole unterstützt wird. Dies schließt
viele nachgerüstete Controller-Karten aus, aber die
meisten auf dem Motherboard integrierten IDE- und SCSI-Chips und
Controllerkarten, die vom Hersteller angeboten werden, werden wahrscheinlich
funktionieren. Um herauszufinden, ob Ihr Gerät von der SRM-Konsole unterstützt
wird, lesen Sie das <ulink url="&url-srm-howto;">SRM-Firmware-HowTo</ulink>.

</para><para arch="arm"> 

IDE/ATAPI CD-ROMs werden auf allen ARM-Systemen unterstützt.
Auf RiscPCs werden auch SCSI-CD-ROMS unterstützt.

</para><para arch="mips">

Um auf SGI-Systemen von CD-ROM zu booten, erfordert es ein SCSI-CD-ROM-Laufwerk,
das fähig ist, mit einer logischen Blockgröße von 512 Bytes umzugehen. Viele der
auf dem PC-Markt erhältlichen SCSI-CD-ROM-Laufwerke haben diese Fähigkeit nicht.
Wenn Ihr CD-ROM einen Jumper hat, der mit <quote>Unix/PC</quote> oder
<quote>512/2048</quote> bezeichnet ist, stellen Sie ihn auf <quote>Unix</quote>
bzw. <quote>512</quote>.
Um die Installation zu starten, wählen Sie einfach den Eintrag
<quote>System-Installation</quote> in der Firmware. Das Broadcom BCM91250A
unterstützt Standard-IDE-Geräte inklusive CD-ROMs, aber momentan werden keine
CD-Images für diese Plattform angeboten, da die Firmware keine CD-Laufwerke
erkennt. Um Debian auf einem Broadcom BCM91480B-Evaluation-Board zu
installieren, benötigen Sie eine PCI IDE-, SATA- oder SCSI-Karte.

</para><para arch="mipsel"> 

Auf DECstations benötigt das Starten von CD-ROM ein SCSI-CD-ROM-Laufwerk,
das mit einer logischen Blockgröße von 512 Bytes umgehen kann.
Wenn Ihr CD-ROM einen Jumper hat, der mit <quote>Unix/PC</quote> oder
<quote>512/2048</quote> bezeichnet ist, stellen Sie ihn auf <quote>Unix</quote>
bzw. <quote>512</quote>.

</para><para arch="mipsel">

CD 1 beinhaltet den Installer für die r3k-kn02 Subarchitektur
(die R3000-basierten DECstations 5000/1xx und 5000/240 sowie
die R3000-basierten Personal DECstation-Modelle), CD 2 den
Installer für die r4k-kn04 Subarchitektur (die R4x00-basierten
DECstations 5000/150 und 5000/260 sowie die Personal DECstation
5000/50).

</para><para arch="mipsel">

Um von CD zu starten, geben Sie das Kommando <userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
an der Kommandozeile der Firmware an, wobei <replaceable>#</replaceable>
die Nummer des TurboChannel-Gerätes ist, von dem gestartet werden soll (auf
den meisten DECstations ist dies 3) und <replaceable>id</replaceable> die
SCSI-ID des CD-ROM-Laufwerks. Wenn noch weitere Parameter angegeben werden
müssen, können sie optional mit der folgenden Syntax angehängt werden:

</para><para arch="mipsel">

<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=value1 param2=value2 ...</userinput>

</para>
  </sect2>

  <sect2><title>Festplatten</title>

<para>

Das Starten des Installationssystem von einer Festplatte ist eine
weitere Option für viele Architekturen. Dies erfordert, dass ein anderes
Betriebssystem den Installer auf die Festplatte lädt.

</para><para arch="m68k">

Tatsächlich ist die Installation von der lokalen Platte die bevorzugte
Installationsmethode für die meisten &architecture;-Geräte.

</para><para arch="sparc">

Obwohl &arch-title; das Starten des Installers von SunOS (Solaris)
nicht erlaubt, können Sie trotzdem von einer SunOS-Partition (UFS Slices)
installieren.

</para>
  </sect2>

  <sect2 condition="bootable-usb"><title>USB-Memory-Stick</title>

<para>

Viele Debian-Geräte benötigen ihre Disketten- und/oder CD-ROM Laufwerke
nur für die Installation und für Rettungsfälle. Wenn Sie einige
Server betreiben, haben Sie wahrscheinlich schon daran gedacht, diese
Laufwerke wegzulassen und einen USB-Memory-Stick für die Installation
und (wenn nötig) zum Wiederherstellen des Systems zu verwenden. Das ist
auch für kleine Systeme sinnvoll, die keinen Platz für überflüssige Laufwerke
haben.

</para>
  </sect2>

  <sect2><title>Netzwerk</title>

<para>

Das Netzwerk kann während der Installation verwendet werden, um für die
Installation benötigte Dateien zu beziehen. Ob das Netzwerk genutzt wird oder
nicht, hängt von der gewählten Installationsmethode ab sowie Ihren Antworten
auf gewisse Fragen, die während der Installation gestellt werden. Das
Installationssystem unterstützt die meisten Arten von Netzwerkverbindungen
(inklusive PPPoE, allerdings nicht ISDN oder PPP), entweder über HTTP oder
FTP. Nachdem die Installation abgeschlossen ist, können Sie Ihr System
konfigurieren, ISDN oder PPP zu verwenden.

</para><para condition="supports-tftp">

Sie können Ihr System auch über das Netzwerk <emphasis>booten</emphasis>.
<phrase arch="mips">Dies ist die bevorzugte Installationsmethode für
&arch-title;.</phrase>

</para><para condition="supports-nfsroot">

Die diskettenlose Installation per Netzwerk-Boot von einem Local Area Network
(LAN-Netzwerk) incl. dem Einbinden aller lokalen Dateisystemen per NFS ist eine
weitere Möglichkeit.

</para>
  </sect2>

  <sect2><title>Un*x- oder GNU-System</title>

<para>

Wenn Sie ein anderes Unix-ähnliches System laufen haben, könnten Sie dieses
zur Installation von &debian; verwenden, ohne den &d-i;, der im Rest dieses
Handbuches beschrieben ist, zu verwenden. Diese Installationsmethode ist
sinnvoll für Benutzer mit ansonsten nicht unterstützter Hardware oder auf
Servern, die sich keine Abschaltung des Systems leisten können.
Wenn Sie sich für diese Technik interessieren, lesen Sie <xref
linkend="linux-upgrade"/>.

</para>
  </sect2>

  <sect2><title>Unterstützte Speichersysteme</title>

<para>

Die Debian-Bootmedien beinhalten einen Kernel, der kompiliert ist,
um die Anzahl der Systeme, auf denen er läuft, zu maximieren. Unglücklicherweise
erzeugt dies einen größeren Kernel, der viele Treiber beinhaltet, die für
Ihr Gerät gar nicht verwendet werden (siehe <xref linkend="kernel-baking"/>,
um zu lernen, wie Sie einen eigenen Kernel kompilieren können).
Grundsätzlich ist Unterstützung für möglichst viele Geräte wünschenswert, um
sicherzustellen, dass Debian auf einer umfangreichen Palette von Hardware
installiert werden kann.

</para><para arch="x86">

Grundsätzlich beinhaltet das Debian-Installationssystem Unterstützung für
Disketten, IDE-Laufwerke, IDE-Disketten, Parallel-Port-IDE-Geräte,
SCSI-Controller und -Laufwerke, USB und FireWire. Zu den unterstützten
Dateisystemen gehören FAT, Win-32 FAT-Erweiterungen (VFAT) und NTFS.

</para><para arch="i386">

Die Festplatten-Schnittstellen, die das <quote>AT</quote>-Festplatten-Interface
emulieren &ndash; oft auch MFM, RLL, IDE oder ATA genannt &ndash; werden
unterstützt. Sehr alte 8&ndash;Bit Festplatten-Controller, wie sie in
IBM-XT-Computern verwendet werden, werden nur als Kernelmodul unterstützt.
SCSI-Disk-Controller von vielen verschiedenen Herstellern werden unterstützt.
Lesen Sie das
<ulink url="&url-hardware-howto;">Linux-Hardware-Kompatibilitäts-HowTo</ulink>
für weitere Informationen.

</para><para arch="m68k">

So gut wie alle Speichersysteme mit Unterstützung für den Linux-Kernel
werden vom Debian-Installationssystem unterstützt. Beachten Sie, dass der
aktuelle Linux-Kernel Disketten auf Macintosh überhaupt nicht unterstützt,
und das Debian-Installationssystem unterstützt keine Disketten für Amigas.
Auf dem Atari wird auch das Macintosh HFS-System unterstützt und
AFFS als Kernelmodul. Macs unterstützen das Atari-(FAT-)Dateisystem. Amigas
unterstützen das FAT-Dateisystem und HFS als Kernelmodul.

</para><para arch="sparc"> 

Jedes vom Kernel unterstützte Speichersystem wird auch vom Boot-System
unterstützt. Die folgenden SCSI-Treiber werden im Standard-Kernel
unterstützt:

<itemizedlist>
<listitem><para>

Sparc ESP

</para></listitem>
<listitem><para>

PTI Qlogic, ISP

</para></listitem>
<listitem><para>

Adaptec AIC7xxx

</para></listitem>
<listitem><para>

NCR und Symbios 53C8XX

</para></listitem>
</itemizedlist>

IDE-Systeme (wie der UltraSPARC 5) werden auch unterstützt. Siehe die
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors F.A.Q.</ulink>
für weitere Informationen über vom Linux-Kernel unterstützte SPARC-Hardware.

</para><para arch="alpha">

Alle vom Linux-Kernel unterstützten Speichersysteme werden auch vom
Boot-System unterstützt. Das schließt sowohl SCSI- als auch IDE-Festplatten ein.
Beachten Sie aber, dass auf vielen Systemen die SRM-Konsole nicht imstande ist,
von IDE-Laufwerken zu booten und der Jensen ist unfähig, von Floppy zu starten
(siehe <ulink url="&url-jensen-howto;"></ulink> für mehr Informationen über
das Booten des Jensen)

</para><para arch="powerpc"> 

Alle Speichersysteme, die vom Linux-Kernel unterstützt werden, werden
auch vom Boot-System unterstützt. Beachten Sie, dass der aktuelle Linux-Kernel
Floppies auf CHRP-Systemen generell nicht unterstützt.

</para><para arch="hppa">

Alle Speichersysteme, die vom Linux-Kernel unterstützt werden, werden
auch vom Boot-System unterstützt. Beachten Sie, dass der aktuelle Linux-Kernel
Diskettenlaufwerke nicht unterstützt.

</para><para arch="mips">

Alle vom Linux-Kernel unterstützten Speichersysteme werden auch vom
Boot-System unterstützt.

</para><para arch="s390">

Alle vom Linux-Kernel unterstützten Speichersysteme werden auch vom
Boot-System unterstützt. Das heißt, dass FBA und ECKD DASDs mit dem
alten Linux-Disk-Layout (ldl) und dem neuen gängigen S/390-Disk-Layout
(cdl) unterstützt werden.

</para>

  </sect2>

 </sect1>
