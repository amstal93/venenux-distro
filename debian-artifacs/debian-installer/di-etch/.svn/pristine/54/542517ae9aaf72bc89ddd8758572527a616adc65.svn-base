<!-- retain these comments for translator revision tracking -->
<!-- original version: 43693 -->

 <sect1 id="network-cards">
 <title>Maquinari per a la connexió de xarxes</title>
<para>

Pràcticament qualsevol targeta de xarxa (NIC) suportada pel nucli
Linux és també suportada pel sistema d'instal·lació; els controladors
modulars s'haurien de carregar automàticament.

<phrase arch="x86">Inclou la majoria de targetes PCI i PCMCIA.</phrase>
<phrase arch="i386">Moltes targetes ISA antigues també són
suportades.</phrase>

<phrase arch="m68k">Novament, vegeu <ulink url="&url-m68k-faq;"></ulink>
per a més detalls.</phrase>

</para><para arch="sparc">

Això inclou diverses targetes PCI genèriques (per a sistemes que tenen
PCI) i les següents NIC de Sun:

<itemizedlist>
<listitem><para>

Sun LANCE

</para></listitem>
<listitem><para>

Sun Happy Meal

</para></listitem>
<listitem><para>

Sun BigMAC

</para></listitem>
<listitem><para>

Sun QuadEthernet

</para></listitem>
<listitem><para>

MyriCOM Gigabit Ethernet

</para></listitem>
</itemizedlist>

</para><para arch="mipsel">

Degut a limitacions del nucli només es suporten en DECstations les
interfícies de xarxa integrades, l'opció de targetes de xarxa TurboChannel
no funciona actualment.

</para><para arch="s390">

La llista de dispositius suportats és:

<itemizedlist>
 <listitem><para>

Channel to Channel (CTC) i connexió ESCON (real o emulada)

</para></listitem>
 <listitem><para>

OSA-2 Token Ring/Ethernet i OSA-Express Fast Ethernet (no QDIO)

</para></listitem>
<listitem><para>

OSA-Express en mode QDIO, HiperSockets i Guest-LANs

</para></listitem>
</itemizedlist>

</para>

<para arch="arm">

En &arch-title;, la majoria de dispositius Ethernet integrats són
suportats i es proveeixen mòduls addicionals per a dispositius PCI i
USB. La principal excepció és la plataforma IXP4xx (amb dispositius com
el Linksys NSLU2) la qual necessita un microcodi propietari per a poder
operar amb el seu dispositiu Ethernet integrat. Es poden obtenir imatges
no oficials per al Linksys NSLU2 amb el seu microcodi propietari del
<ulink url="&url-slug-firmware;">lloc Slug-Firmware</ulink>.

</para><para arch="x86">

Tal i com passa amb l'XDSI, el protocol D-channel per a les (antigues)
German 1TR6 no és suportat; &d-i; tampoc suporta les plaques Spellcaster
BRI ISDN. Durant la instal·lació no se suporta l'ús de l'XDSI.

</para>

  <sect2 arch="not-s390" id="nics-firmware">
  <title>Controladors que requereixen microprogramari</title>
<para>

Actualment, el sistema d'instal·lació no suporta la recuperació de
microprogramari. Això vol dir que qualsevol targeta de xarxa que utilitzi
un controlador el qual requereixi carregar microprogramari no és suportada
per defecte.

</para><para>

Si no teniu disponible cap altra NIC durant la instal·lació, encara és
possible instal·lar &debian; utilitzant la imatge completa d'un CD-ROM o
DVD. Seleccioneu l'opció de no configurar la xarxa i instal·leu només
aquells paquets que estiguin disponibles al CD/DVD. Podreu instal·lar
el controlador i el microprogramari que necessiteu quan la instal·lació
s'hagi completat (després de reiniciar) i configurar la xarxa manualment.
Adoneu-vos de què el microprogramari i el controlador poden estar en
paquets diferents, i que poden no estar disponibles a la secció
<quote>main</quote> de l'arxiu de &debian;.

</para><para>

Si el controlador en si <emphasis>sí</emphasis> que està suportat, podreu
utilitzar la NIC durant la instal·lació copiant el microprogramari d'algun
mitjà a <filename>/usr/lib/hotplug/firmware</filename>.
No oblideu copiar-lo a la mateixa ubicació del sistema instal·lat abans de
reiniciar al final de la instal·lació.

</para>
  </sect2>

  <sect2 condition="supports-wireless" id="nics-wireless">
  <title>Targetes de xarxa sense fils</title>
<para>

Les targetes de xarxa sense fils també estan, en general, suportades,
però amb una condició. Molts adaptadors sense fils necessiten controladors
que o no són lliures o no s'han acceptat al nucli oficial de Linux. Aquestes
NIC poden ser usades en &debian;, però no estan suportades durant la
instal·lació.

</para><para>


Si no teniu disponible cap altra NIC durant la instal·lació, encara és
possible instal·lar &debian; utilitzant la imatge completa d'un CD-ROM o
DVD. Utilitzeu el mateix procediment que el que s'ha descrit anteriorment
per aquelles NIC que necessiten microprogramari.

</para><para>

En alguns casos, el controlador que necessiteu no estarà disponible en un
paquet Debian. Haureu de buscar si hi ha disponible el codi font a Internet
i, si hi és, compilar-lo. Com fer-ho està fora de l'abast d'aquest manual.
<phrase arch="x86">Si no hi ha disponible cap controlador per a Linux,
l'últim recurs que teniu és utilitzar el paquet
<classname>ndiswrapper</classname>, el qual us permet utilitzar-ne un de
Windows.</phrase>

</para>
  </sect2>

  <sect2 arch="sparc" id="nics-sparc-trouble">
  <title>Problemes coneguts per &arch-title;</title>
<para>

Hi ha un parell de problemes coneguts amb unes targetes de xarxa específiques
que val la pena mencionar aquí.

</para>

   <sect3><title>Conflictes entre els controladors tulip i dfme</title>
<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
<para>

<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
Hi ha diverses targetes de xarxa PCI que tenen el mateix identificador PCI,
les quals són suportades mitjançant controladors relacionats, però diferents.
Algunes utilitzen el controlador <literal>tulip</literal>, altres amb el
<literal>dfme</literal>. Atès que comparteixen l'identificador, el nucli no
les pot distingir i no se sap amb seguretat quin controlador emprarà. Si
escull el que no toca, la NIC pot no funcionar, o fer-ho de forma incorrecta.

</para><para>

Aquest és un problema típic dels sistemes Netra amb NIC Davicom (compatibles
amb DEC-Tulip). En aquest cas, el controlador <literal>tulip</literal> és
probablement el correcte.

Durant la instal·lació la solució consisteix en canviar d'intèrpret d'ordres
i desactivar el mòdul del controlador erroni utilitzant
<userinput>modprobe -r <replaceable>mòdul</replaceable></userinput> (o ambdós,
si els dos estan carregats). A continuació, podreu carregar el mòdul correcte
utilitzant <userinput>modprobe <replaceable>mòdul</replaceable></userinput>.

</para>
   </sect3>

   <sect3><title>Sun B100 blade</title>
<!-- BTS: #383549; should be checked for kernels >2.6.18 -->
<para>

El controlador de xarxa <literal>cassini</literal> no funciona amb els sistemes
Sun B100 blade.

</para>
   </sect3>
  </sect2>
 </sect1>
