<!-- retain these comments for translator revision tracking -->
<!-- original version: 43735 -->


 <sect1 id="memory-disk-requirements">
 <!-- <title>Memory and Disk Space Requirements</title> -->
 <title>Requisiti di memoria e spazio su disco</title>
<para>

<!--
You must have at least &minimum-memory; of memory and &minimum-fs-size; of hard disk
space. Note that these are really bare-minimum numbers. For more realistic
figures, see <xref linkend="minimum-hardware-reqts"/>.
-->

Sono necessari almeno &minimum-memory; di memoria e &minimum-fs-size; di
spazio su disco. Notare che questi numeri sono il minimo indispensabile; per
dei valori più realistici si veda <xref linkend="minimum-hardware-reqts"/>.

</para><para arch="m68k">

<!--
On the Amiga the size of FastRAM is relevant towards the total memory
requirements.  Also, using Zorro cards with 16-bit RAM is not
supported; you'll need 32-bit RAM.  The <command>amiboot</command>
program can be used to disable 16-bit RAM; see the
<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>.  Recent kernels should
disable 16-bit RAM automatically.
-->

Sull'Amiga la dimensione della FastRAM è importante nei
prerequisiti della memoria complessiva. Inoltre, l'utilizzo delle schede
Zorro con RAM a 16&nbsp;bit non è supportato. Il comando
<command>amiboot</command> può essere utilizzato per disabilitare
la RAM a 16&nbsp;bit; si vedano le
<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>. I kernel più
recenti dovrebbero disabilitare la RAM a 16&nbsp;bit automaticamente.

</para><para arch="m68k">

<!--
On the Atari, both ST-RAM and Fast RAM (TT-RAM) are used by Linux.
Many users have reported problems running the kernel itself in Fast
RAM, so the Atari bootstrap will place the kernel in ST-RAM. The
minimum requirement for ST-RAM is 2 MB. You will need an additional
12 MB or more of TT-RAM.
-->

Sull'Atari, sia la ST-RAM che la Fast RAM (TT-RAM) vengono utilizzate
da Linux. Molti utenti hanno riportato problemi eseguendo il kernel
nella Fast RAM, per questo motivo il processo di avvio dell'Atari
metterà il kernel nella ST-RAM. Il requisito minimo per la ST-RAM
è 2&nbsp;MB. Avrete inoltre bisogno di 12 o più MB di TT-RAM.

</para><para arch="m68k">

<!--
On the Macintosh, care should be taken on machines with RAM-based
video (RBV). The RAM segment at physical address 0 is used as screen
memory, making the default load position for the kernel unavailable.
The alternate RAM segment used for kernel and RAMdisk must be at least
4 MB.
-->

Su Macintosh è necessario stare attenti nel caso di macchine con
video basato sulla memoria (RBV). Il segmento RAM all'indirizzo fisico 0
viene usato come memoria dello schermo, rendendo non disponibile la
posizione predefinita di caricamento del kernel. Il segmento di RAM
alternativo usato per il kernel e il RAMdisk deve essere di almeno
4&nbsp;MB.

</para><para arch="m68k">

<!-- <emphasis condition="FIXME">FIXME: is this still true?</emphasis> -->
<emphasis condition="FIXME">FIXME: è ancora vero?</emphasis>

</para>
 </sect1>
