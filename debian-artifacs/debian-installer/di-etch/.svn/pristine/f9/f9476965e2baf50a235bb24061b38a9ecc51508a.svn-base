partman-lvm (53) unstable; urgency=low

  * Fix syntax error resulting in failure to set partition type.

 -- Frans Pop <fjp@debian.org>  Sat, 10 Mar 2007 17:53:50 +0100

partman-lvm (52) unstable; urgency=low

  [ David Härdeman ]
  * Make sure that the lvm, raid and swap flags are used in a mutually
    exclusive manner. Closes: #413183.

 -- Frans Pop <fjp@debian.org>  Wed,  7 Mar 2007 22:48:58 +0100

partman-lvm (51) unstable; urgency=low

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 18:23:38 +0100

partman-lvm (50) unstable; urgency=low

  [ David Härdeman ]
  * Check for presence of dmsetup before trying to execute it.

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Hebrew (he.po) by Lior Kaplan

 -- Frans Pop <fjp@debian.org>  Tue, 30 Jan 2007 22:00:06 +0100

partman-lvm (49) unstable; urgency=low

  * Properly check for <Go Back> during selection of LV or VG for deletion.
    Thanks to Joel Johnson for spotting this issue. Closes: #407039.
  * General review of debconf handling in main LVM script.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovenian (sl.po) by Matej Kovačič
  * Tamil (ta.po) by drtvasudevan

 -- Frans Pop <fjp@debian.org>  Wed, 24 Jan 2007 02:21:55 +0100

partman-lvm (48) unstable; urgency=low

  [ David Härdeman ]
  * Be more tolerant about spaces in user input. Thanks to Miroslav Kure
    <kurem@upcase.inf.upol.cz> for the patch. Closes: #394428.
  * Correct USED_PVS calculation. Thanks to Miroslav Kure
    <kurem@upcase.inf.upol.cz> for spotting the error. Closes: #394437.

  [ Christian Perrier ]
  * Differentiate between the "none" string for Volume Groups and for Physical
    Volumes.
  * Add a bracketed comment in the "none" strings so that it may be different
    from "none" in other D-I packages.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bengali (bn.po) by Jamil Ahmed
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÁC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Frans Pop <fjp@debian.org>  Thu, 21 Dec 2006 16:43:17 +0100

partman-lvm (47) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Giuseppe Sacco
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 16:02:39 +0200

partman-lvm (46) unstable; urgency=low

  [ Frans Pop ]
  * Update reference to logs on errors. We no longer use VT3. Closes: #383104.

  [ David Härdeman ]
  * Check validity of VG and LV names. Closes: #375161.
  * Add pv_delete() to lvm_tools.sh

  [ Colin Watson ]
  * Disable backup while displaying current configuration.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Thu, 31 Aug 2006 17:22:21 -0400

partman-lvm (45) unstable; urgency=low

  * Avoid expected message in syslog when scanning for volume groups.
  * Reorder main menu options to list most logical options first.

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Finnish (fi.po) by Tapio Lehtonen
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Frans Pop <fjp@debian.org>  Wed, 19 Jul 2006 22:20:09 +0200

partman-lvm (44) unstable; urgency=low

  * Fix forgotten function rename.

 -- Frans Pop <fjp@debian.org>  Wed, 12 Jul 2006 19:17:04 +0200

partman-lvm (43) unstable; urgency=low

  * Set LVM_SUPPRESS_FD_WARNINGS in two scripts that don't call lvm_tools.sh.

 -- Frans Pop <fjp@debian.org>  Wed, 12 Jul 2006 14:23:17 +0200

partman-lvm (42) unstable; urgency=low

  * lvm_tools.sh: avoid warnings from lvm2 tools about open file descriptors
    by setting the envar LVM_SUPPRESS_FD_WARNINGS. Doing this in the include
    script should avoid the warnings for most of partman.
  * choose_partition/lvm/do_option: avoid double sourcing of definitions.sh.

 -- Frans Pop <fjp@debian.org>  Tue, 11 Jul 2006 14:08:45 +0200

partman-lvm (41) unstable; urgency=low

  [ David Härdeman ]
  * Remove parted_names since its of no use for virtual filesystems
    (closes: #329765)

  [ Frans Pop ]
  * Major whitespace cleanup in scripts + some minor syntax changes.
  * Sync undo.d/lvm script with init.d/lvm; adds missing hack for RAID devices.
  * As setting flags on RAID devices does not work and causes errors from
    libparted, do not attempt to sync flags in case of LVM on RAID. Partman
    itself does not rely on the lvm flag, but uses the "method" instead.
    This is in line with existing hacks in init.d/lvm and undo.d/lvm.
    Closes: #377391.
  * Remove duplicate depends; add debconf dependency.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Czech (cs.po) by Miroslav Kure
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Japanese (ja.po) by Kenshi Muto
  * Macedonian (mk.po) by Georgi Stanojevski
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Sun,  9 Jul 2006 17:48:12 +0200

partman-lvm (40) unstable; urgency=low

  [ David Härdeman ]
  * Use the generic confirm_changes from partman-base.
    (needs partman-base 87)

  [ Frans Pop ]
  * Fix menu handling so it also works for languages other than English.
  * lvm-tools.sh: add function pv_on_device for use in partman-auto-lvm.

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski

 -- Frans Pop <fjp@debian.org>  Fri, 23 Jun 2006 19:23:31 +0200

partman-lvm (39) unstable; urgency=low

  * Add "Finish" option to LVM configuration menu for consistency with other
    similar menus.

  [ Updated translations ]
  * Estonian (et.po) by Siim Põder
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Slovak (sk.po) by Peter Mann
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Frans Pop <fjp@debian.org>  Wed, 21 Jun 2006 13:58:23 +0200

partman-lvm (38) unstable; urgency=low

  [ David Härdeman ]
  * Lock/unlock partitions when added to/removed from a VG (requires
    partman-base 86).
  * Fix template alignment.
  * Make the LVM questions and error messages critical.
  * Fix some errors in lvm_tools.sh which broke partman-auto-lvm.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş

 -- Frans Pop <fjp@debian.org>  Thu, 15 Jun 2006 20:09:45 +0200

partman-lvm (37) unstable; urgency=low

  [ Christian Perrier ]
  * Remove extra capitalization in templates
  * Unmark some strings for translation
  * Change enumeration style in the summary templates

  [ David Härdeman ]
  * Minor template changes to help translators.
  * Clarify template. Closes: #347248.

  [ Updated translations ]
  * Czech (cs.po) by Miroslav Kure
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon,  5 Jun 2006 12:31:13 -0400

partman-lvm (36) unstable; urgency=low

  [ David Härdeman ]
  * Drop dependency on lvmcfg and make partman-lvm stand-alone
  * Make all user visible text translatable
  * Flatten menu structure and make menus dynamic (i.e. only show actions
    which it is possible to perform)

  [ Joey Hess ]
  * Since it no longer depends on lvmcfg-utils, it needs to depend on that
    package's dependencies: md-modules, lvm2-udeb, and parted-udeb.
  * Remove stadards-version (it's a udeb..).
  * Add myself to uploaders.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Georgian (ka.po) by Aiet Kolkhi

 -- Joey Hess <joeyh@debian.org>  Mon, 29 May 2006 18:21:40 -0400

partman-lvm (35) unstable; urgency=low

  [ David Härdeman ]
  * Make dm-crypt partitions usable as LVM PV's
  * Avoid checking partition flags if possible

  [ Updated translations ]
  * Danish (da.po) by Claus Hindsgaul
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Thai (th.po) by Theppitak Karoonboonyanan

 -- Joey Hess <joeyh@debian.org>  Fri, 19 May 2006 13:32:34 -0500

partman-lvm (34) unstable; urgency=low

  [ Fabio M. Di Nitto ]
  * Do not allow the partition starting at block 0 to be LVM or bad things
    will happen.

  [ Updated translations ]
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Sonam Rinchen
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hungarian (hu.po) by SZERVÑC Attila
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Sat, 29 Apr 2006 03:37:05 +0200

partman-lvm (33) unstable; urgency=low

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.
  * Use stop_parted_server and restart_partman functions from partman-base
    73 instead of duplicating the code.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Baishampayan Ghose
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Icelandic (is.po) by David Steinn Geirsson
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Norwegian Nynorsk (nn.po)
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrişor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 24 Jan 2006 22:09:55 +0100

partman-lvm (32) unstable; urgency=low

  [ Frans Pop ]
  * Changed dependency from partman to partman-base.

  [ Joey Hess ]
  * Use log-output.
  
  [ Updated translations ]
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Romanian (ro.po) by Eddy Petrisor
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 18:16:42 +0200

partman-lvm (31) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagasy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 17:24:19 +0300

partman-lvm (30) unstable; urgency=low

  * This includes fixes for substitution bugs in translated templates.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Recai Oktaş

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 17:39:29 -0500

partman-lvm (29) unstable; urgency=low

  * Joey Hess
    - Don't grep for translated string in $dev/model, look at $dev/device
      instead.
  * Updated translations: 
    - Italian (it.po) by Stefano Canepa

 -- Joey Hess <joeyh@debian.org>  Mon,  8 Nov 2004 14:19:11 -0500

partman-lvm (28) unstable; urgency=low

  * Colin Watson
    - Ask "Keep current partition layout and configure LVM?" at critical
      priority.
  * Updated translations:
    - Finnish (fi.po) by Tapio Lehtonen
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann

 -- Colin Watson <cjwatson@debian.org>  Sat, 30 Oct 2004 10:41:45 +0100

partman-lvm (27) unstable; urgency=low

  * Joey Hess
    - Pass "partman" option to lvmcfg so it knows it is running under
      partman and can use partman to find devices for lvm (including raid
      devices).
    - Don't stop parted_server until lvmcfg has run since lvmcfg now uses
      it.
    - Add special case into choose_method/lvm/choices to allow raid devices to
      be used for lvm, despite not having lvm listed in VALID_FLAGS.
      Closes: #275586, #265252, #265868, #270424
    - In init.d/lvm, mark devices that pvdisplay shows as being used for
      lvm as having the lvm flag set. This is a workaround to make raid
      devices that are used as lvm continue to show as such when the
      partitioner restarts after lvmcfg.
    - Thanks to Alex Owen for his help.
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek (el.po) by Greek Translation Team
    - French (fr.po) by French Team

 -- Joey Hess <joeyh@debian.org>  Sat, 16 Oct 2004 21:04:02 -0400

partman-lvm (26) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Oct 2004 15:28:00 -0400

partman-lvm (25) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Bøkmal, Norwegian (nb.po) by Axel Bojer
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Russian L10N Team
    - Slovenian (sl.po) by Jure Čuhalev
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 21:29:15 -0400

partman-lvm (24) unstable; urgency=low

  * Remove seen flag setting and fix reset calls for preseeding.

 -- Joey Hess <joeyh@debian.org>  Wed,  1 Sep 2004 16:15:31 -0400

partman-lvm (23) unstable; urgency=low

  * Joey Hess
    - Remove the warning about root or /boot on LVM. Debian supports this, for
      at least some kernels and initrds now (tested i386 2.6 with / LVM,
      success), and lilo-installer will automatically be run.
      Closes: #267677

 -- Joey Hess <joeyh@debian.org>  Tue, 31 Aug 2004 13:18:57 -0400

partman-lvm (22) unstable; urgency=low

  * Anton Zinoviev
    - update.d/lvm_sync_flag: do not create "format" files in the
      partition directories.  Look at #248062.
  * Updated translations: 
    - Arabic (ar.po) by Christian Perrier
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Russian (ru.po) by Yuri Kozlov
    - Turkish (tr.po) by Recai Oktaş

 -- Joey Hess <joeyh@debian.org>  Mon, 30 Aug 2004 12:21:26 -0400

partman-lvm (21) unstable; urgency=low

  * Updated translations: 
    - Albanian (sq.po) by Elian Myftiu

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Jul 2004 13:33:24 -0400

partman-lvm (20) unstable; urgency=low

  * Christian Perrier
    - rename templates to partman-lvm.templates. POTFILES.in updated also
    - Typo correction in templates. Translations unfuzzied
  * Joey Hess
    - Fix new template to use proper English. Translations not unfuzzized
      as many are word for word translartions of a nonsense phrase.
  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by I Gede Wijaya S
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuriy Talakan'
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Ming Hua
    - Traditional Chinese (zh_TW.po) by Tetralet
    - Albanian (sq.po) by Elian Myftiu

 -- Joey Hess <joeyh@debian.org>  Sun, 25 Jul 2004 19:24:04 -0400

partman-lvm (19) unstable; urgency=low

  * Anton Zinoviev
    - update.d/lvm_sync_flag: autoset lvm-method for partitions with lvm
      flag and no method instead of unsetting the lvm flag.  Thanks to
      Joey Hess, closes: #250033. 
    - templates: remove partman-lvm/text/method
    - choose_method/lvm/choices: use partman/method_long/lvm instead of
      partman-lvm/text/method
    - templates: s/storage devices/disks/
    - templates: new template confirm_nochanges
    - choose_partition/lvm/do_option: move the confirmation dialog in a
      separate function confirm_changes. 
    - choose_partition/lvm/do_option: Show not only a list of the
      partitions to be formatted but also a list of changed partition
      tables.  Show a customized dialog when there are no changes.  Thanks
      to Joey Hess, closes: #250971
    - choose_method/lvm/do_option: do not create a `format' file in the
      partition directory.  Thanks to Martin Michlmayr, closes: #248062
  * Updated translations:
    - Albanian (sq.po) by Elian Myftiu
    - Croatian (hr.po) by Krunoslav Gernhard

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 20 Jul 2004 16:01:58 +0300

partman-lvm (18) unstable; urgency=low

  * Martin Michlmayr
    - Only prompt about writing changes to disk when there actually
      are any changes.  Closes: #250971
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by I Gede Wijaya S
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Ming Hua

 -- Joey Hess <joeyh@debian.org>  Sat, 17 Jul 2004 14:26:17 -0400

partman-lvm (17) unstable; urgency=low

  * Joey Hess
    - Add a warning if / or /boot is on lvm, as the debian initrd does not
      (yet) support that.
  * Updated translations:
    - Welsh (cy.po) by Dafydd Harries
    - French (fr.po) by Christian Perrier
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 28 May 2004 19:01:00 -0300

partman-lvm (16) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - Hungarian (hu.po) by VERÓK István
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Bøkmal, Norwegian (nb.po) by Knut Yrvin
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Tue, 25 May 2004 12:37:51 -0300

partman-lvm (15) unstable; urgency=low

  * Colin Watson
    - Depend on partman.
  * Anton Zinoviev
    - move the item in the main partitioning menu up.
    - init.d/lvm: inform parted_server for whether the newly created
      partition contains some file system; use the command "open_dialog
      DISK_UNCHANGED" to inform parted_server that the newly created loop
      "partition table" and partition do not exist only in the internal
      data structures of libparted, but are real.  Thanks to Andrew Pollock
      and Martin Michlmayr, closes: #247780
    - in the confirmation dialog say which partitions are going to be
      formatted.  Thanks to Martin Michlmayr, closes: #247343
 
  * Updated translations: 
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - French (fr.po) by Christian Perrier
    - Japanese (ja.po) by Kenshi Muto
    - Dutch (nl.po) by Bart Cornelis
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 15 May 2004 07:43:49 +0300

partman-lvm (14) unstable; urgency=low

  * Change dependency from lvm10 to lvm2.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Slovak (sk.po) by Peter KLFMANiK Mann

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 10 May 2004 18:41:52 +0100

partman-lvm (13) unstable; urgency=low

  * Martin Michlmayr
    - Support LVM 1 and LVM 2.
    - Add myself as uploader.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 02 May 2004 21:33:22 +0100

partman-lvm (12) unstable; urgency=low

  * Updated translations: 
    - Bokmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Slovenian (sl.po) by Matjaz Horvat
    - Turkish (tr.po) by Osman Yüksel

 -- Joey Hess <joeyh@debian.org>  Fri, 23 Apr 2004 13:19:55 -0400

partman-lvm (11) unstable; urgency=low

  * Updated translations: 
    - Catalan (ca.po) by Jordi Mallach
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by I Gede Wijaya S
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Nikolai Prokoschenko
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Osman Yüksel
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Tue, 20 Apr 2004 11:09:38 -0400

partman-lvm (10) unstable; urgency=low

  * Anton Zinoviev
    - rules: remove .svn directories from the package
    - update the package to take advantage of the newly used file
      `use_filesystem' 
    - renumber the lvm method (18->50)
    - do not remove $DEVICES/* after LVM-configuration; thanks to Martin
      Michlmayr and Brad Schick, closes: #238382, #240298.
  * Joshua Kwan
    - Switch to new debhelper udeb support.
  * Joey Hess
    - Template polishing.
  * Updated translations: 
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Basque (eu.po) by Piarres Beobide Egaña
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by André Dahlqvist
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Sun, 11 Apr 2004 21:25:59 -0400

partman-lvm (9) unstable; urgency=low

  * Updated translations: 
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - French (fr.po) by Christian Perrier
    - Polish (pl.po) by Bartosz Fenski
    - Swedish (sv.po) by André Dahlqvist
    - Turkish (tr.po) by Osman Yüksel
    - Albanian (sq.po) by Elian Myftiu

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Mar 2004 15:01:24 -0500

partman-lvm (7) unstable; urgency=low

  * Updated translations: 
    - Bokmal, Norwegian (nb.po) by Steinar H. Gunderson
    - Russian (ru.po) by Nikolai Prokoschenko
    - Turkish (tr.po) by Osman Yüksel

 -- Joey Hess <joeyh@debian.org>  Sun, 14 Mar 2004 13:17:32 -0500

partman-lvm (6) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Italian (it.po) by Stefano Canepa
    - Russian (ru.po) by Nikolai Prokoschenko
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Osman Yüksel

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 14 Mar 2004 17:24:32 +0200

partman-lvm (5) unstable; urgency=low

  * Updated translations: 
    - Czech (cs.po) by Miroslav Kure
    - Bokmal, Norwegian (nb.po) by Petter Reinholdtsen
    - Albanian (sq.po) by Elian Myftiu

 -- Joey Hess <joeyh@debian.org>  Sat, 13 Mar 2004 12:39:18 -0500

partman-lvm (4) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Finnish (fi.po) by Tapio Lehtonen
    - Hungarian (hu.po) by VERÓK István
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Portuguese (pt.po) by Nuno Sénica
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet
    - Elian Myftiu
      - Added Albanian translation (sq.po)

 -- Joey Hess <joeyh@debian.org>  Fri, 12 Mar 2004 12:56:50 -0500

partman-lvm (3) unstable; urgency=low

  * Use the new lvmcfg-utils package instead of lvmcfg.

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Mar 2004 13:40:50 +0100

partman-lvm (2) unstable; urgency=low

  * Anton Zinoviev
    - run debconf-updatepo
  * Joey Hess
    - remove autogenerated postrm
  * Maybe some translations changed. I don't know.

 -- Joey Hess <joeyh@debian.org>  Mon,  8 Mar 2004 19:40:50 -0500

partman-lvm (1) unstable; urgency=low

  * Took partman-palo and s/palo/lvm/.
  * The method `lvm' is available only if parted_server reports that the
    flag `lvm' is valid.
  * templates: add translatable names of lvm.
  * init.d/lvm: discover activated logical volumes and automaticaly create
    loop partition table and a partition in them.
  * add menu-item to the main menu to configure LVM.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat,  6 Mar 2004 19:26:28 +0200

Local Variables:
coding: utf-8
End:
