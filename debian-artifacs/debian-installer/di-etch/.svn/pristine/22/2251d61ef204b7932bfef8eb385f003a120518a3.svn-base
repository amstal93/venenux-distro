<!-- $Id: components.xml 44147 2007-01-13 15:50:09Z mck-guest $ -->
<!-- original version: 43965 -->

 <sect1 id="module-details">
 <title>Použití jednotlivých komponent</title>
<para>

V této části podrobně popíšeme každou komponentu instalačního
programu. Komponenty jsou seskupeny do celků podle kontextu, ve kterém
se během instalace vyskytují. Poznamenejme, že při instalaci se nemusí
vždy využít všechny komponenty &mdash; to závisí na způsobu instalace
a na dostupném hardwaru.

</para>

  <sect2 id="di-setup">
  <title>Nastavení instalačního programu a rozpoznání hardwaru</title>
<para>

Předpokládejme, že &d-i; úspěšně nastartoval a nyní se díváte na jeho
první obrazovku. V tento okamžik je &d-i; ještě poměrně hloupý
a nepoužitelný. Neví nic o hardwaru vašeho počítače, nezná váš
preferovaný jazyk a dokonce ani netuší, jaký úkol mu byl přidělen. Ale
nebojte se. Jako správný průzkumník začne &d-i; zkoumat své okolí a po
nějaké době bude mít slušný přehled o okolním hardwaru. Poté se pokusí
nalézt zbytek svých komponent a sám sebe přemění ve schopný, dobře
vychovaný program.

Bohužel, stále existují věci, se kterými si &d-i; nedokáže poradit
a proto mu musíte trošku pomoci (například vybrat jazyk, ve kterém má
instalace probíhat, odsouhlasit rozložení klávesnice a podobně).

</para><para>

Během této fáze si jistě všimnete, že &d-i; několikrát
<firstterm>rozpoznává hardware</firstterm>. Poprvé je úzce zaměřen na
hardware, který by mohl obsahovat další části instalačního programu,
tj. CD mechaniky nebo síťové karty. Další rozpoznávání hardwaru
probíhá později, když se hledají pevné disky, protože před prvním
rozpoznáváním ještě nemusí být k dispozici všechny ovladače.

</para>

&module-lowmem.xml;
&module-localechooser.xml;
&module-kbd-chooser.xml;
&module-s390-netdevice.xml;
&module-ddetect.xml;
&module-cdrom-detect.xml;
&module-iso-scan.xml;
&module-anna.xml;
&module-netcfg.xml;
&module-choose-mirror.xml;
  </sect2>

  <sect2 id="di-partition">
  <title>Rozdělení disku a výběr přípojných bodů</title>
<para>

Nyní, po posledním rozpoznávání hardwaru, by již měl být &d-i; v plné
síle, přizpůsoben podle uživatelských požadavků a připraven na
opravdovou práci.

Jak praví název této části, bude se několik následujících komponent
zabývat rozdělením disků, vytvořením souborových systémů, přiřazením
přípojných bodů a volitelně nastavením souvisejících záležitostí, jako
jsou LVM a RAID zařízení.

</para>

&module-s390-dasd.xml;
&module-partman.xml;
&module-autopartkit.xml;
&module-partitioner.xml;
&module-partconf.xml;
&module-mdcfg.xml;
&module-partman-lvm.xml;
&module-partman-crypto.xml;
  </sect2>

  <sect2 id="di-system-setup">
  <title>Nastavení systému</title>
<para>

Po rozdělení disku se instalační systém zeptá několik otázek, které
použije pro nastavení instalovaného systému.

</para>

&module-tzsetup.xml;
&module-clock-setup.xml;
&module-user-setup.xml;
</sect2>

  <sect2 id="di-install-base">
  <title>Instalace základního systému</title>
<para>

Přestože je tato část nejméně problematická, zabere dosti času,
protože musí stáhnout, ověřit a rozbalit celý základní systém. Pokud
máte pomalý počítač a/nebo síťové připojení, může to chvíli trvat.

</para>

&module-base-installer.xml;
  </sect2>

  <sect2 id="di-install-software">
  <title>Instalace dodatečného softwaru</title>
<para>

Po instalaci základního systému máte sice použitelný, nicméně poněkud
omezený systém. Většina uživatelů si bude chtít do systému doinstalovat
další software a upravit tak systém svým potřebám, s čímž vám instalátor
ochotně pomůže. Jestliže máte pomalejší počítač nebo síťové připojení,
může tento krok trvat ještě déle než instalace základního systému.

</para>
&module-apt-setup.xml;
&module-pkgsel.xml;
  </sect2>

  <sect2 id="di-make-bootable">
  <title>Nastavení zavádění systému</title>
<para condition="supports-nfsroot">

Pokud instalujete bezdiskovou stanici, zavádění systému z lokálního
disku evidentně nebude nejsmysluplnější volba - tento krok přeskočte.
<phrase arch="sparc"> Možná budete chtít nastavit OpenBoot tak, aby
implicitně zaváděl systém ze sítě, viz <xref
linkend="boot-dev-select-sun"/>.</phrase>

<!--
</para><para>

Zavádění více operačních systémů na jednom počítači je stále něco jako
černá magie. Tento dokument se ani nesnaží pokrýt všechny možné
zavaděče, které se liší na jednotlivých architekturách a dokonce i na
jejich podarchitekturách. Měli byste si dobře prostudovat dokumentaci
vašeho zavaděče a pamatujete: třikrát měř a jednou řež.
-->

</para>

&module-os-prober.xml;
&module-alpha-aboot-installer.xml;
&module-hppa-palo-installer.xml;
&module-x86-grub-installer.xml;
&module-x86-lilo-installer.xml;
&module-ia64-elilo-installer.xml;
&module-mips-arcboot-installer.xml;
&module-mipsel-colo-installer.xml;
&module-mipsel-delo-installer.xml;
&module-powerpc-yaboot-installer.xml;
&module-powerpc-quik-installer.xml;
&module-s390-zipl-installer.xml;
&module-sparc-silo-installer.xml;
&module-nobootloader.xml;
  </sect2>

  <sect2 id="di-finish">
  <title>Dokončení instalace</title>
<para>

Toto jsou poslední drobnosti, které je třeba vykonat před zavedením
nového systému. Většina práce spočívá v uklizení po &d-i;u.

</para>

&module-finish-install.xml;
  </sect2>

  <sect2 id="di-miscellaneous">
  <title>Nejrůznější</title>
<para>

Následující komponenty se obvykle do instalačního procesu nezapojují,
ale tiše čekají v pozadí, aby vám pomohly v případě, že se něco
pokazí.

</para>

&module-save-logs.xml;
&module-cdrom-checker.xml;
&module-shell.xml;
&module-network-console.xml;
  </sect2>
 </sect1>
