partman-palo (8) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 19:38:56 +0100

partman-palo (7) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 13:36:35 +0100

partman-palo (6) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Panjabi (pa.po) by A S Alam
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 17:33:37 +0200

partman-palo (5) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hungarian (hu.po) by SZERVÑC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Fri, 14 Jul 2006 03:53:45 +0200

partman-palo (4) unstable; urgency=low

  [ Frans Pop ]
  * Changed dependency from partman to partman-base.

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.

  [ dann frazier ]
  * Update Standards-Version
  * Add myself to Uploaders
  * Remove Richard Hirst from Uploaders; I don't believe this e-mail address
    is correct any longer, and I don't think he has upload privs anyway.

 -- dann frazier <dannf@debian.org>  Sat, 28 Jan 2006 15:12:17 -0700

partman-palo (3) unstable; urgency=low

  * Anton Zinoviev
    - rules: remove .svn directories from the package
    - choose_method/_numbers: renumber the method number (20->45)
    - choose_method/palo/do_option: remove file use_filesystem
    - remove the valid_filesystems directory (that hack is not necessary
      any more)
    - update.d/palo:
       1. rename as palo_sync_flag
       2. remove the obsoleted hack at the end of the file
    - update.d/palo_visuals: new file
    - templates: add partman/method_long/palo and partman/method_short/palo
    - templates: remove partman-palo/text/method
    - choose_method/palo/choices: use partman/method_long/palo instead of
      partman-palo/text/method
  * Joshua Kwan
    - switch to debhelper's new udeb support
  * Colin Watson
    - Depend on partman.
  * Christian Perrier
    - Rename templates to <package>.templates
  * Joey Hess
    - Remove some unnecessary seen flag unsetting calls, to better support
      preseeding.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Belarusian (be.po) by Andrei Darashenka
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Guillem Jover
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po)
    - Greek (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po)
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Hindi (hi.po) by Ravishankar Shrivastava
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Arief S Fitrianto
    - Italian (it.po) by Giuseppe Sacco
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Malagazy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Norwegian (nb.po) by Hans Fredrik Nordhaug
    - Dutch (nl.po) by Frans Pop
    - Norwegian (nn.po) by Håvard Korsvoll
    - Norwegian (pa_IN.po) by Amanpreet Singh Alam
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Tagalog (tl.po) by Eric Pareja
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Thu,  4 Aug 2005 17:09:12 -0400

partman-palo (2) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - cy (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Hungarian (hu.po) by VERÓK István
    - Italian (it.po) by Stefano Canepa
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Norwegian (nb.po) by Petter Reinholdtsen
    - Dutch (nl.po) by Bart Cornelis
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Portuguese (pt.po) by Nuno Sénica
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

  * Anton Zinoviev
    - Replace Mb by MiB in the templates.  Unfuzy translations.
    - Replace MB by GB in templates.  Unfuzzy most of the translations.
  * Christian Perrier
    - Replace MiB by MB in templates..:-)
    - replace palo by PALO in templates
    - s/is found/was found
    - Unfuzzy translations when possible
  * Joey Hess
    - Remove autogenerated postrm script.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 14 Mar 2004 11:26:59 +0200

partman-palo (1) unstable; urgency=low
        
  * Updated translations: 
    - da.po (Danish) by Claus Hindsgaul
    - ko.po (Korean) by Changwoo Ryu
    - pt_BR.po (Portuguese (Brazil)) by André Luís Lopes

  * Included in the CVS repository.
  * init.d/palo: read the flags only of partitions, omit free spaces.
  * rules: swap binary-arch and binary-indep
  * update.d/palo: use SET_FLAGS only if the flags change.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat,  6 Mar 2004 21:14:52 +0200
