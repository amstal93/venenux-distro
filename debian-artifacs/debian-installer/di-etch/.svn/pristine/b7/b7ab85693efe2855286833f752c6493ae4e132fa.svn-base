<!-- retain these comments for translator revision tracking -->
<!-- original version: 43939 -->


 <sect1 id="supported-peripherals">
 <!-- <title>Peripherals and Other Hardware</title> -->
 <title>Periferiche e altro hardware</title>
<para arch="not-s390">

<!--
Linux supports a large variety of hardware devices such as mice,
printers, scanners, PCMCIA and USB devices.  However, most of these
devices are not required while installing the system.
-->

Linux supporta una grande varietà di dispositivi hardware
come per esempio mouse, stampanti, dispositivi PCMCIA e USB. Tuttavia
molte di queste periferiche non sono necessarie durante l'installazione
del sistema.

</para><para arch="x86">

<!--
USB hardware generally works fine, only some
USB keyboards may require additional configuration
(see <xref linkend="hardware-issues"/>).
-->

L'hardware USB generalmente funziona correttamente, solo qualche tastiera
USB a volte può richiedere qualche configurazione aggiuntiva
(si veda <xref linkend="hardware-issues"/>).

</para><para arch="x86">

<!--
Again, see the
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
to determine whether your specific hardware is supported by Linux.
-->

Si veda inoltre il
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
per sapere se un particolare dispositivo è supportato da Linux.

</para><para arch="s390">

<!--
Package installations from XPRAM and tape are not supported by this
system.  All packages that you want to install need to be available on a
DASD or over the network using NFS, HTTP or FTP.
-->

L'installazione dei pacchetti da XPRAM e unità a nastro non è
supportata da questo sistema. Tutti i pacchetti che si vogliono installare
devono essere disponibili mediante DASD o dalla rete utilizzando NFS, HTTP
o FTP.

</para><para arch="mips">

<!--
The Broadcom BCM91250A evaluation board offers standard 3.3v 32 bit and 64
bit PCI slots as well as USB connectors.  The Broadcom BCM91480B evaluation
board features four 64 bit PCI slots.
-->

La scheda per prototipazione Broadcom BCM91250A offre slot PCI da 3.3v a 32
e 64 bit e connettori USB. La scheda per prototipazione BCM91480B dispone di
quattro slot PCI a 64 bit.

</para><para arch="mipsel">

<!--
The Broadcom BCM91250A evaluation board offers standard 3.3v 32 bit and 64
bit PCI slots as well as USB connectors.  The Broadcom BCM91480B evaluation
board features four 64 bit PCI slots.  The Cobalt RaQ has no support for
additional devices but the Qube has one PCI slot.
-->

La scheda per prototipazione Broadcom BCM91250A offre slot PCI da 3.3v a 32
e 64 bit e connettori USB. La scheda per prototipazione BCM91480B dispone di
quattro slot PCI a 64 bit. La Cobalt RaQ non ha il supporto per i dispositivi
aggiuntivi, ma la Qube ha uno slot PCI.

</para>
 </sect1>

 <sect1 arch="not-s390">
 <!-- <title>Purchasing Hardware Specifically for GNU/Linux</title> -->
 <title>Acquistare hardware specifico per GNU/Linux</title>
<para>

<!--
There are several vendors, who ship systems with Debian or other
distributions of GNU/Linux
<ulink url="&url-pre-installed;">pre-installed</ulink>. You might pay more
for the privilege, but it does buy a level of peace of mind, since you can
be sure that the hardware is well-supported by GNU/Linux.
-->

Ci sono molti rivenditori che distribuiscono computer con Debian o altre
distribuzioni GNU/Linux <ulink url="&url-pre-installed;">preinstallate</ulink>.
È possibile che costino di più, ma questo acquisto
permette di stare tranquilli, perché si può essere sicuri che
l'hardware è ben supportato da GNU/Linux.

</para><para arch="m68k">

<!--
Unfortunately, it's quite rare to find any vendor shipping
new &arch-title; machines at all.
-->

Sfortunatamente, è abbastanza difficile trovare un venditore
che distribuisca macchine &arch-title; nuove.

</para><para arch="x86">

<!--
If you do have to buy a machine with Windows bundled, carefully read
the software license that comes with Windows; you may be able to
reject the license and obtain a rebate from your vendor.  Searching
the Internet for <quote>windows refund</quote> may get you some useful
information to help with that.
-->

Se si deve comprare una macchina con Windows preinstallato, leggere
attentamente la licenza che viene data con Windows; si può rifiutare
la licenza e ottenere un rimborso dal produttore. Facendo una ricerca su
internet di <quote>windows refund</quote> si potrebbero recuperare delle
informazioni utili per fare questa operazione.

</para><para>

<!--
Whether or not you are purchasing a system with Linux bundled, or even
a used system, it is still important to check that your hardware is
supported by the Linux kernel.  Check if your hardware is listed in
the references found above.  Let your salesperson (if any) know that
you're shopping for a Linux system.  Support Linux-friendly hardware
vendors.
-->

Indipendentemente dall'acquisto di un sistema con Linux preinstallato o
meno o addirittura un computer usato, è necessario assicurarsi che
l'hardware sia supportato dal kernel Linux. Controllare se il proprio
hardware è presente nelle liste elencate precedentemente. Si deve far
sapere al rivenditore che si stà acquistando un computer su cui verrà
installato Linux. Si dovrebbe supportare i produttori che appoggiano Linux.

</para>

  <sect2>
  <!-- <title>Avoid Proprietary or Closed Hardware</title> -->
  <title>Evitare hardware chiuso o proprietario</title>
<para>

<!--
Some hardware manufacturers simply won't tell us how to write drivers
for their hardware. Others won't allow us access to the documentation
without a non-disclosure agreement that would prevent us from
releasing the Linux source code.
-->

Alcuni costruttori di hardware non danno informazioni su come scrivere
i driver per i loro dispositivi. Altri non permettono di accedere alla
documentazione senza un accordo di non divulgazione che impedirebbe di
rilasciare il codice sorgente di Linux.

</para><para arch="m68k">

<!--
Another example is the proprietary hardware in the older
Macintosh line. In fact, no specifications or documentation have ever
been released for any Macintosh hardware, most notably the ADB
controller (used by the mouse and keyboard), the floppy controller,
and all acceleration and CLUT manipulation of the video hardware
(though we do now support CLUT manipulation on nearly all internal
video chips).  In a nutshell, this explains why the Macintosh Linux
port lags behind other Linux ports.
-->

Un altro esempio è l'hardware proprietario dei vecchi
Macintosh. Infatti non è mai stata rilasciata nessuna
documentazione o specifica per alcun hardware Macintosh, in
particolare per il controller ADB (usato per il mouse e la tastiera),
per il controller del floppy, per tutta l'accelerazione dell'hardware video
e per la manipolazione CLUT (sebbene la manipolazione CLUT sia
supportata su quasi tutti i chip interni). In breve, questo spiega
perché il port di Linux su Macintosh è più
indietro degli altri.

</para><para>

<!--
Since we haven't been granted access to the documentation on these
devices, they simply won't work under Linux. You can help by asking
the manufacturers of such hardware to release the documentation. If
enough people ask, they will realize that the free software community
is an important market.
-->

Poiché non è stato dato accesso alla documentazione di questi
dispositivi, questi non funzioneranno sotto Linux. È possibile aiutare
chiedendo ai produttori di questi dispositivi di rilasciare la
documentazione. Se molte persone lo chiederanno, capiranno che la
comunità del software libero è un mercato importante.

</para>
  </sect2>

  <sect2 arch="x86">
  <!-- <title>Windows-specific Hardware</title> -->
  <title>Hardware specifico per Windows</title>
<para>

<!--
A disturbing trend is the proliferation of Windows-specific modems and
printers. In some cases these are specially designed to be operated by
the Microsoft Windows operating system and bear the legend <quote>WinModem</quote>
or <quote>Made especially for Windows-based computers</quote>. This
is generally done by removing the embedded processors of the hardware
and shifting the work they do over to a Windows driver that is run by
your computer's main CPU. This strategy makes the hardware less
expensive, but the savings are often <emphasis>not</emphasis> passed on to the
user and this hardware may even be more expensive than equivalent
devices that retain their embedded intelligence.
-->

Una moda scomoda è la proliferazione di modem e stampanti specifici
per Windows. In alcuni casi questi sono progettati specialmente per
essere pilotati dal sistema operativo Microsoft Windows e hanno una
targhetta <quote>WinModem</quote> o <quote>Made especially for Windows-based
computers</quote>. Questo in genere è realizzato rimovendo i
processori integrati nei dispositivi e delegando il lavoro ad un driver
Windows che viene eseguito dal processore del computer. Questa strategia
rende l'hardware meno costoso, ma questi risparmi spesso
<emphasis>non</emphasis> si trasferiscono all'utente e questo hardware
può essere perfino più costoso dell'equivalente che mantiene
l'intelligenza integrata.

</para><para>

<!--
You should avoid Windows-specific hardware for two reasons. The first
is that the manufacturers do not generally make the resources
available to write a Linux driver. Generally, the hardware and
software interface to the device is proprietary, and documentation is
not available without a non-disclosure agreement, if it is available
at all. This precludes it being used for free software, since free
software writers disclose the source code of their programs. The
second reason is that when devices like these have had their embedded
processors removed, the operating system must perform the work of the
embedded processors, often at <emphasis>real-time</emphasis> priority,
and thus the CPU is not available to run your programs while it is
driving these devices. Since the typical Windows user does not
multi-process as intensively as a Linux user, the manufacturers hope
that the Windows user simply won't notice the burden this hardware
places on their CPU.  However, any multi-processing operating system,
even Windows 2000 or XP, suffers from degraded performance when
peripheral manufacturers skimp on the embedded processing power of
their hardware.
-->

Si dovrebbe evitare l'hardware specifico per Windows per due motivi. Il
primo consiste nel fatto che in generale questi produttori non
rendono disponibili le risorse necessarie alla scrittura dei driver per
Linux. Generalmente, l'hardware e l'interfaccia tra il software e il
dispositivo sono proprietari e la documentazione non è accessibile
senza un accordo di non divulgazione, sempre ammesso che sia accessibile.
Questo impedisce che venga usata per il software libero, perché
i programmatori di software libero divulgano il sorgente dei loro
programmi. Il secondo motivo è che con questi dispositivi
a cui sono stati rimossi i processori, il sistema operativo dove svolgere il
lavoro al posto loro, spesso con priorità
<emphasis>real-time</emphasis>, e quindi la CPU non è disponibile per
eseguire i programmi mentre pilota i dispositivi. Poiché gli utenti
Windows non fanno uso così intenso di processi paralleli come
fanno invece gli utenti Linux, i produttori sperano che gli utenti
Windows non notino il sovraccarico che i dispositivi comportano sul
processore. Tuttavia, qualsiasi sistema operativo che supporta l'esecuzione
di processi in parallelo, anche Windows 2000 o XP, soffre di una
degradazione delle prestazioni quando i produttori di hardware riducono
la potenza di calcolo dei loro dispositivi.

</para><para>

<!--
You can help improve this situation by encouraging these manufacturers
to release the documentation and other resources necessary for us to
program their hardware, but the best strategy is simply to avoid this
sort of hardware until it is listed as working in the
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>.
-->

È possibile aiutare a migliorare questa situazione incoraggiando
questi produttori a rilasciare la documentazione e le altre risorse per
permettere di programmare il loro hardware, ma la migliore strategia
consiste semplicemente nell'evitare questi tipi di dispositivi fino a
quando non verranno elencati nel
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>.

</para>
  </sect2>
 </sect1>
