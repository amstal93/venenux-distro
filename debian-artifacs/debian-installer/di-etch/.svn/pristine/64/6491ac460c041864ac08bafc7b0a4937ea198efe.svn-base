<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 39920 -->


  <sect2 arch="alpha"><title>&arch-title; でのパーティション分割</title>
<para>

<!--
Booting Debian from the SRM console (the only disk boot method supported
by &releasename;) requires you to have a BSD disk label, not a DOS
partition table, on your boot disk.  (Remember, the SRM boot block is
incompatible with MS-DOS partition tables &mdash; see
<xref linkend="alpha-firmware"/>.)  As a result, <command>partman</command>
creates BSD disk labels when running on &architecture;, but if your disk
has an existing DOS partition table the existing partitions will need to be
deleted before <command>partman</command> can convert it to use a disk label.
-->
SRM コンソール (&releasename; でサポートされる唯一のディスク起動方法) から
Debian を起動するには、ブートディスクに DOS パーティションテーブルではなく、
BSD ディスクラベルがある必要があります。(SRM ブートブロックと MS-DOS
パーティションテーブルとに互換性がないことにご注意を。
<xref linkend="alpha-firmware"/> をご覧ください。) その結果、&architecture;
上で動作させる場合、<command>partman</command> は BSD ディスクラベルを作成します。
しかし、すでにディスク上に DOS パーティションテーブルがある場合、
<command>partman</command> がディスクラベルを使用するためにパーティションを変換する前に、
既存のパーティションを削除する必要があります。

</para><para>

<!--
If you have chosen to use <command>fdisk</command> to partition your
disk, and the disk that you have selected for partitioning does not
already contain a BSD disk label, you must use the <quote>b</quote>
command to enter disk label mode.
-->
ディスクを分割するのに <command>fdisk</command> を使用することに決めていて、
分割するよう選択したディスクにまだ BSD ディスクラベルがなければ、
<quote>b</quote> コマンドを使ってディスクラベルモードに入ってください。

</para><para>

<!--
Unless you wish to use the disk you are partitioning from Tru64 Unix
or one of the free 4.4BSD-Lite derived operating systems (FreeBSD,
OpenBSD, or NetBSD), you should <emphasis>not</emphasis> create the
third partition as a <quote>whole disk</quote> partition (i.e. with
start and end sectors to span the whole disk), as this renders the
disk incompatible with the tools used to make it bootable with aboot.
This means that the disk configured by the installer for use as the
Debian boot disk will be inaccessible to the operating systems mentioned
earlier.
-->
Tru64 Unix やフリーな 4.4BSD-Lite 派生オペレーティングシステム
(FreeBSD, OpenBSD, NetBSD)
からパーティションを作成したディスクを使いたくない場合、
aboot で起動できるようにするために使用されるツールでディスクに互換性が
なくなってしまうので、<quote>ディスク全体</quote> をひとつの (つまり
開始セクタと終了セクタがディスク全体に及ぶような) パーティションとして、
第 3 パーティションを <emphasis>作成すべきではありません</emphasis>。
これは、Debian のブートディスクとして使用するようにインストーラによって
構成されたディスクが、前述のオペレーティングシステムにはアクセスできない
ということを意味します。

</para><para>

<!--
Also, because <command>aboot</command> is written to the first few
sectors of the disk (currently it occupies about 70 kilobytes, or 150
sectors), you <emphasis>must</emphasis> leave enough empty space at
the beginning of the disk for it.  In the past, it was suggested that
you make a small partition at the beginning of the disk, to be left
unformatted.  For the same reason mentioned above, we now suggest that
you do not do this on disks that will only be used by GNU/Linux.  When
using <command>partman</command>, a small partition will still be
created for <command>aboot</command> for convenience reasons.

-->
また、<command>aboot</command> はディスクの最初の数セクタ
(今のところ 70KB、つまり 150 セクタ必要になります) に書き込まれるため、
ディスクの先頭部分に充分な空きスペースを残しておかなければ<emphasis>なりません</emphasis>。
以前は、ディスクの最初に小さなパーティションを作成し、
フォーマットしないでおくことが勧められていました。
同じ理由から、GNU/Linux のみで使うディスク上には
この処置を行わないことをお勧めします。
<command>partman</command> を使用する時、便宜上の理由から今までどおり
<command>aboot</command> のために小さなパーティションが作成されます。

</para><para condition="FIXME">

<!--
For ARC installations, you should make a small FAT partition at the
beginning of the disk to contain <command>MILO</command> and
<command>linload.exe</command> &mdash; 5 megabytes should be sufficient, see
<xref linkend="non-debian-partitioning"/>. Unfortunately, making FAT
file systems from the menu is not yet supported, so you'll have to do
it manually from the shell using <command>mkdosfs</command> before
attempting to install the boot loader.
-->
ARC インストーラを使う場合、
<command>MILO</command> と <command>linload.exe</command> を収めるために、
ディスクの先頭部分に小さな FAT パーティションを作成しなければなりません。
5 MB もあれば充分です。
こちらに関しては <xref linkend="non-debian-partitioning"/> をご覧ください。
残念ながら、まだメニューから FAT ファイルシステムを作成することはできません。
したがってブートローダをインストールする前に、
シェルから <command>mkdosfs</command> を使うなどして、
手動で作成しなければなりません。

</para>
  </sect2>
