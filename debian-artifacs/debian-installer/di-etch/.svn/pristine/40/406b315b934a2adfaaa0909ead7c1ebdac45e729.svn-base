<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44002 -->

 <sect1 condition="supports-tftp" id="install-tftp">
 <title>Pr�parer les fichiers pour amorcer depuis le r�seau avec TFTP</title>
<para>

Si votre machine est connect�e � un r�seau local, vous pouvez l'amorcer 
directement � partir d'une autre machine de ce r�seau en utilisant 
TFTP. Pour cela, les fichiers d'amor�age doivent �tre 
plac�s � un endroit sp�cifique de cette machine et celle-ci doit savoir 
amorcer votre machine.

</para><para>

Vous devez configurer un serveur TFTP et, pour beaucoup de machines, 
un serveur DHCP<phrase condition="supports-rarp">, un serveur RARP</phrase>
<phrase condition="supports-bootp">ou un serveur BOOTP</phrase>.

</para><para>

<phrase condition="supports-rarp">Le protocole de recherche des adresses 
inverses (<emphasis>Reverse address Resolution Protocol</emphasis> ou RARP)
est une solution pour indiquer � votre client l'adresse IP qu'il doit 
utiliser pour lui-m�me. Une autre solution est d'utiliser le protocole BOOTP.
</phrase>

<phrase condition="supports-bootp">BOOTP est un protocole IP qui indique � un 
ordinateur quelle est son adresse IP et lui dit o� obtenir sur le r�seau une 
image d'amor�age.</phrase>

<phrase arch="m68k">Il existe d�sormais une autre solution pour les syst�mes
VMEbus&nbsp;: l'adresse IP peut �tre configur�e manuellement dans la ROM 
d'amor�age.</phrase>

Le protocole DHCP 
(�&nbsp;Dynamic Host Configuration Protocole&nbsp;�, Protocole de 
configuration dynamique des h�tes) est une extension bien plus flexible 
de BOOTP (et respectant la compatibilit� ascendante). Certains syst�mes ne 
peuvent �tre configur�s que par DHCP.

</para><para arch="powerpc">

Pour les PowerPC, si vous avez une machine Power Macintosh NewWorld, il vaut 
mieux utiliser DHCP plut�t que BOOTP. Certaines machines r�centes ne peuvent pas 
s'amorcer avec BOOTP.

</para><para arch="alpha"> 

� la diff�rence du microprogramme Open Firmware que l'on trouve sur les 
machines SPARC et PowerPC, la console SRM n'utilisera 
<emphasis>pas</emphasis> RARP pour obtenir les adresses IP&nbsp;; vous devrez 
donc utiliser BOOTP pour amorcer votre Alpha <footnote>

<para>
Les syst�mes Alpha peuvent aussi s'amorcer depuis le r�seau en utilisant 
MOP DECNet (�&nbsp;Maintenance Operations Protocol&nbsp;�, protocole des 
op�rations de maintenance), mais l'on n'en dira rien ici. Il est 
vraisemblable que votre op�rateur local OpenVMS sera ravi de vous assister si
vous ne pouvez r�sister � l'envie d'utiliser MOP pour amorcer Linux sur votre 
Alpha.
</para>

</footnote> depuis le r�seau. Vous pouvez aussi entrer directement depuis la console VRM 
la configuration IP des interfaces r�seau.
</para><para arch="hppa"> 

Quelques vieilles machines HPPA (p. ex. 715/75) utilisent RBOOTD plut�t que 
BOOTP. Un paquet <classname>rbootd</classname> est disponible dans l'archive Debian.

</para><para>

Le protocole trivial de transfert de fichiers (<emphasis>Trivial File Transfert
Protocol, TFTP</emphasis>) est utilis� pour transf�rer l'image d'amor�age 
au client. Th�oriquement, tout serveur sur les plateformes qui
impl�mentent ces protocoles peut �tre utilis�. Dans les exemples qui vont 
suivre, on donnera les commandes pour SunOS 4.x, SunOS 5.x (mieux connu sous 
le nom de Solaris) et GNU/Linux.

<note arch="x86"><para>

Pour utiliser la m�thode de d�marrage par l'ex�cution d'un environnement de
pr�-amor�age (PXE) de TFTP, vous avez besoin d'un serveur TFTP avec
<userinput>tsize</userinput>. Sur un serveur &debian;, les paquets
<classname>atftpd</classname> et <classname>tftpd-hpa</classname> 
sont bons&nbsp;; nous vous conseillons <classname>tftpd-hpa</classname>.

</para></note>

</para>

&tftp-rarp.xml;
&tftp-bootp.xml;
&tftp-dhcp.xml;

  <sect2 id="tftpd">
  <title>Activer le serveur TFTP</title>
<para>

Pour faire fonctionner le serveur TFTP, vous devez vous assurer au pr�alable 
que <command>tftpd</command> est activ�. Ce dernier est g�n�ralement activ� 
gr�ce � la ligne suivante dans <filename>/etc/inetd.conf</filename>&nbsp;:

<informalexample><screen>
tftp dgram udp wait root /usr/sbin/tcpd in.tftpd /tftpboot
</screen></informalexample>

Les paquets Debian, quand ils sont install�s, placent correctement cette
ligne.
</para>

<note><para>
Les serveurs TFTP utilisaient habituellement le r�pertoire <filename>/tftpboot</filename> pour
stocker les images. Cependant les paquets &debian; se servent d'autres r�pertoires
pour ob�ir au <ulink url="&url-fhs-home;">standard sur l'organisation des fichiers</ulink>.
Par exemple, <classname>tftpd-hpa</classname> utilise par d�faut
<filename>/var/lib/tftpboot</filename>. Vous aurez � modifier les exemples de cette section.
</para></note>

<para> 

Lisez le fichier <filename>/etc/inetd.conf</filename> et m�morisez le r�pertoire
pass� en param�tre � <command>in.tftpd</command>
<footnote>
<para>
L'option <userinput>-l</userinput> autorise certaines versions de 
<command>in.tftpd</command> � enregistrer toutes les requ�tes dans le journal 
du syst�me&nbsp;; c'est extr�mement pratique en cas d'erreur d'amor�age.
</para>
</footnote>&nbsp;; vous en aurez besoin ult�rieurement. 
Si vous avez d� modifier <filename>/etc/inetd.conf</filename>, vous devrez le 
signaler au processus <command>inetd</command>. Sur une machine Debian,
lancez <userinput>/etc/init.d/inetd reload</userinput>&nbsp;; sur les autres 
machines, retrouvez le num�ro de processus de <command>inetd</command> et 
ex�cutez la commande 
<userinput>kill -HUP <replaceable>inetd-pid</replaceable></userinput>.

</para><para arch="mips">

Si vous voulez installer Debian sur une machine SGI et si votre serveur TFTP 
est une machine GNU/Linux avec Linux 2.4.X, il vous faudra effectuer la 
man&oelig;uvre suivante sur votre serveur&nbsp;:

<informalexample><screen>
# echo 1 &gt; /proc/sys/net/ipv4/ip_no_pmtu_disc
</screen></informalexample>

de fa�on � emp�cher la d�tection du MTU, sinon la PROM de la machine SGI ne pourra 
pas t�l�charger le nouveau noyau. En outre, assurez-vous que les paquets TFTP 
transitent par un port source inf�rieur � 32767 ou bien le t�l�chargement 
s'arr�tera apr�s le premier paquet. Vous pouvez toujours contourner ce
bogue de la PROM gr�ce au noyau 2.4.X en ajustant

<informalexample><screen>
# echo "2048 32767" &gt; /proc/sys/net/ipv4/ip_local_port_range
</screen></informalexample>

pour fixer l'intervalle des ports source que le serveur TFTP peut utiliser.

</para>
  </sect2>

  <sect2 id="tftp-images">
  <title>Mettre les images TFTP en place</title>
<para>

Ensuite, placez les images TFTP dont vous avez besoin (d�crites dans la 
<xref linkend="where-files"/>) dans le r�pertoire des images d'amor�age 
de <command>tftpd</command>. G�n�ralement, ce r�pertoire s'appelle 
<filename>/tftpboot</filename>. Vous aurez � faire un lien depuis ce fichier 
vers le fichier que <command>tftpd</command> utilisera pour amorcer le client.
Malheureusement, le nom du fichier est d�termin� par le client 
TFTP et il n'y a pas vraiment de standard.

</para>
<para arch="powerpc">

Sur les machines Power Macintosh NewWorld, vous devrez configurer le programme 
d'amor�age <command>yaboot</command> comme une image d'amor�age TFTP. 
<command>Yaboot</command> chargera les images du noyau et du disque virtuel � 
travers TFTP. Pour amorcer sur le r�seau, utilisez
<filename>yaboot-netboot.conf</filename>. Renommez-le simplement en
<filename>yaboot.conf</filename> dans le r�pertoire TFTP.

</para>
<para arch="x86">

Pour le d�marrage PXE, tout ce dont vous avez besoin est dans l'archive
<filename>netboot/netboot.tar.gz</filename>. Extrayez les fichiers dans le
r�pertoire de l'image <command>tftpd</command>. Assurez-vous que le serveur
DHCP donnera bien le fichier <filename>pxelinux.0</filename> comme fichier
d'amor�age � <command>tftpd</command>.
</para>
<para arch="ia64">
Pour le d�marrage PXE, tout ce dont vous avez besoin est dans l'archive
<filename>netboot/netboot.tar.gz</filename>. Extrayez les fichiers dans le
r�pertoire de l'image <command>tftpd</command>. Assurez-vous que le serveur
DHCP donnera bien le fichier 
<filename>/debian-installer/ia64/elilo.efi</filename> comme fichier
d'amor�age � <command>tftpd</command>.
</para>

   <sect3 arch="mipsel">
   <title>Images TFTP pour les DECstation</title>
<para>

Pour les DECstation, il y a des fichiers tftpimage pour chaque 
sous-architecture&nbsp;; ils contiennent � la fois un noyau et un 
installateur en un seul fichier. La convention
de nommage est <filename><replaceable>subarchitecture</replaceable>/netboot-boot.img</filename>.
Copiez l'image tftp que vous allez utiliser dans
<userinput>/tftpboot/tftpboot.img</userinput> si vous travaillez avec
l'exemple de configuration de BOOTP/DHCP d�crit plus haut.

</para><para>

Le microprogramme des DECstation autorise l'amor�age via TFTP par
la commande <userinput>boot <replaceable>#</replaceable>/tftp</userinput> 
o� <replaceable>#</replaceable> est le num�ro de p�riph�rique 
�&nbsp;TurboChannel&nbsp;� sur lequel amorcer. Sur la plupart des DECstation, 
c'est le num�ro <quote>3</quote>. Si le serveur BOOTP/DHCP ne fournit pas le
nom du fichier ou si vous avez besoin de passer des param�tres 
suppl�mentaires, cela peut se faire avec la syntaxe suivante&nbsp;:

</para><para>

<userinput>boot #/tftp/filename param1=value1 param2=value2 ...</userinput>

</para><para>

Plusieurs r�visions des microprogrammes des DECstation ont un probl�me avec 
l'amor�age sur le r�seau&nbsp;: le transfert d�bute mais apr�s un certain 
temps, il s'arr�te avec <computeroutput>a.out err</computeroutput>. 
Il peut y avoir plusieurs causes&nbsp;:

<orderedlist>
<listitem><para>

Le microprogramme ne r�pond pas � une requ�te ARP durant un transfert TFTP. 
Cela conduit � un <quote>timeout</quote> d'ARP et le transfert s'arr�te. La solution est 
d'ajouter l'adresse MAC de la carte Ethernet dans la table 
ARP du serveur TFTP. On peut le faire avec
<userinput>arp -s <replaceable>IP-address</replaceable>
<replaceable>MAC-address</replaceable></userinput> en tant que root sur la 
machine serveur TFTP. On peut lire l'adresse MAC de la DECstation en entrant
<command>cnfg</command> � l'invite du microprogramme de la DECstation.

</para></listitem>
<listitem><para>

Le microprogramme impose une taille limite aux fichiers utilis�s pour amorcer par TFTP.

</para></listitem>
</orderedlist>

Il y aussi des r�visions de microprogramme qui ne peuvent pas s'amorcer du 
tout par TFTP, voyez 
<ulink url="http://www.netbsd.org/Ports/pmax/board-list.html#proms">les pages web de NetBSD</ulink>.

</para>
   </sect3>

   <sect3 arch="alpha">
   <title>Amor�age TFTP pour Alpha</title>
<para>
Sur Alpha, vous devez sp�cifier le nom de fichier (le chemin �tant
relatif au r�pertoire de l'image d'amor�age) en utilisant l'argument 
<userinput>-file</userinput> de la commande <userinput>boot</userinput> 
de SRM ou en configurant la variable d'environnement 
<userinput>BOOT_FILE</userinput>. Vous pouvez aussi passer le nom de fichier 
via BOOTP (pour <command>dhcpd</command> ISC, utilisez la directive 
<userinput>filename</userinput>). � la diff�rence d'Open Firmware, il n'y a 
pas de <emphasis>nom de fichier par d�faut</emphasis> dans SRM et vous 
<emphasis>devez</emphasis> utiliser l'une de ces m�thodes pour sp�cifier un nom 
de fichier.

</para>
   </sect3>

   <sect3 arch="sparc">
   <title>Amor�age TFTP pour SPARC</title>
<para>

Certaines architectures SPARC ajoutent au nom de fichier la sous-architecture, comme 
<quote>SUN4M</quote> ou <quote>SUN4C</quote>. Ainsi, si votre sous-architecture syst�me
est SUN4C et que son adresseIP est 192.168.1.3, le nom de fichier sera
<filename>C0A80103.SUN4C</filename>. Cependant, pour certaines architectures, le fichier
recherch� est simplement <filename>client-ip-en-hex</filename>.
Pour d�terminer facilement ce nom et en supposant que l'adresse IP est 
10.0.0.4, lancez un interpr�teur de commandes et faites&nbsp;:

<informalexample><screen>
$ printf '%.2x%.2x%.2x%.2x\n' 10 0 0 4
</screen></informalexample>

Il suffira de mettre les lettres en majuscule et d'ajouter le nom de la sous-architecture
pour obtenir le nom du fichier.

</para>

<para>

Si tout a �t� fait correctement, la commande <userinput>boot net</userinput>
depuis l'OpenPROM devrait charger l'image. Si l'image n'est pas trouv�e,
v�rifiez dans les journaux du serveur tftp sous quel nom elle a �t� demand�e.
</para>
<para>

Vous pouvez aussi forcer certains syst�mes SPARC � rechercher un nom de 
fichier sp�cifique en l'ajoutant � la fin de la commande d'amor�age de 
OpenPROM, p.&nbsp;ex. <userinput>boot net my-sparc.image</userinput>. 
Ce fichier doit bien s�r �tre pr�sent dans un r�pertoire connu du serveur TFTP.

</para>
   </sect3>

   <sect3 arch="m68k">
   <title>Amor�age TFTP pour BVM/Motorola</title>
<para>

Pour BVM et les syst�mes VMEbus Motorola, recopiez les fichiers 
&bvme6000-tftp-files; dans <filename>/tftpboot/</filename>.

</para><para>

Ensuite, configurez votre ROM d'amor�age et votre serveur BOOTP pour charger 
en premier les fichiers <filename>tftplilo.bvme</filename> ou 
<filename>tftplilo.mvme</filename> du serveur TFTP. Reportez-vous au fichier
<filename>tftplilo.txt</filename> de votre sous-architecture pour obtenir 
des informations suppl�mentaires sur la configuration de votre syst�me.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Amor�age TFTP pour SGI</title>
<para>

Sur les machines SGI, vous pouvez compter sur <command>bootpd</command> pour
obtenir le nom du fichier TFTP. Il est donn� soit par 
<userinput>bf=</userinput> dans <filename>/etc/bootptab</filename> ou 
bien par l'option <userinput>filename=</userinput> dans 
<filename>/etc/dhcpd.conf</filename>.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Amorcer avec TFTP les cartes Broadcom BCM91250A et BCM91480B</title>
<para>

Il n'est pas utile de configurer DHCP car vous indiquerez � CFE l'adresse 
exacte du fichier qui doit �tre charg�.

</para>
   </sect3>
  </sect2>
<!-- FIXME: commented out since it seems too old to be usable and a current
            way is not known


  <sect2 id="tftp-low-memory">
  <title>Installation de TFTP sur syst�me avec peu de m�moire</title>
<para>

Sur certains syst�mes, le disque virtuel d'installation
          standard, combin� avec les exigences en m�moire de l'image
          d'amor�age TFTP, ne peuvent tenir en m�moire. Dans ce cas,
          vous pouvez quand m�me utiliser TFTP mais vous aurez �
          passer par une �tape suppl�mentaire pour monter votre
          r�pertoire racine � travers le r�seau. Ce type de
          configuration est aussi appropri� pour les clients sans
          disque  et les clients sans donn�es.

</para><para>

Commencez par suivre toutes les �tapes ci-dessus dans 
<xref linkend="install-tftp"/>.

<orderedlist>
<listitem><para>

Copiez l'image du noyau Linux sur votre serveur TFTP en
utilisant l'image <userinput>a.out</userinput> de l'architecture sur
           laquelle vous �tes en train d'amorcer.

</para></listitem>
<listitem><para>

�&nbsp;D�tarez&nbsp;� l'archive de root sur votre serveur
           NFS (qui peut �tre le m�me que votre serveur TFTP)&nbsp;:

<informalexample><screen>
# cd /tftpboot
# tar xvzf root.tar.gz
</screen></informalexample>

Assurez-vous d'utiliser le <command>tar</command> de GNU (les
           autres programmes, comme celui de SunOS, manipulent
           incorrectement certains p�riph�riques comme les fichiers
           ordinaires).

</para></listitem>
<listitem><para>

Exportez votre r�pertoire
<filename>/tftpboot/debian-sparc-root</filename> avec les acc�s
           root pour votre client. Vous devez ajouter la ligne
           suivante � <filename>/etc/exports</filename> (syntaxe GNU/Linux,
           cela devrait �tre similaire pour SunOS jusqu'� la version
           4.1.x)&nbsp;:

<informalexample><screen>

/tftpboot/debian-sparc-root client(rw,no_root_squash)

</screen></informalexample>

Note&nbsp;: <replaceable>client</replaceable> est le nom d'h�te ou bien
           l'adresse IP reconnue par le serveur pour le syst�me que
           vous allez amorcer.

</para></listitem>
<listitem><para>

Cr�ez un lien symbolique depuis votre adresse IP cliente
           sous forme de nombres s�par�s par des points dans le fichier
<filename>debian-sparc-root</filename> du r�pertoire
           <filename>/tftpboot</filename>. Par exemple, si l'adresse IP client
           est 192.168.1.3, faites&nbsp;:

<informalexample><screen>
# ln -s debian-sparc-root 192.168.1.3
</screen></informalexample>

</para></listitem>
</orderedlist>

</para>

  </sect2>

  <sect2>
  <title>Installation avec une racine TFTP et NFS</title>
<para>

C'est tr�s proche de l'installation pour syst�me avec peu de
m�moire <xref linkend="tftp-low-memory"/> parce que vous ne voulez pas 
charger le disque virtuel mais amorcer depuis le syst�me de fichier nfs-root
          cr�� il y a peu. Vous n'avez qu'� remplacer le lien vers
          l'image tftpboot par un lien vers l'image du noyau
(p. ex. <filename>linux-a.out</filename>).

</para><para>

RARP/TFTP requires all daemons to be running on the same server (the
workstation is sending a TFTP request back to the server that replied
to its previous RARP request).

</para><para>

Pour amorcer la machine cliente, allez � <xref linkend="boot-tftp"/>.

</para>

  </sect2>
END FIXME -->
 </sect1>
