<!-- $Id: boot-usb-files.xml 45650 2007-03-04 09:00:52Z mck-guest $ -->
<!-- original version: 45616 -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Příprava souborů pro zavedení z USB zařízení</title>

<para>

Pro přípravu USB zařízení budete potřebovat počítač s nainstalovaným
GNU/Linuxem a podporou USB. Pokud používáte jaderný modul usb-storage,
ujistěte se, že je nahraný (<userinput>modprobe
usb-storage</userinput>) a zkuste zjistit, na které zařízení je
navázána vaše USB klíčenka (v tomto příkladu používáme
<filename>/dev/sda</filename>). Pokud klíčenka obsahuje ochranu proti
zápisu, vypněte ji.

</para><para>

USB klíčenka by měla mít velikost alespoň 256 MB. Menší velikosti jsou
podporovány pouze při ruční výrobě podle <xref
linkend="usb-copy-flexible"/>.

</para>

  <sect2 id="usb-copy-easy">
  <title>Kopírování souborů &mdash; jednoduchá cesta</title>
<para arch="x86">

K dispozici máte soubor <filename>hd-media/boot.img.gz</filename>,
který obsahuje všechny instalační soubory (včetně jádra), zavaděč
<command>SYSLINUX</command> a jeho konfigurační soubor. Jediné co
musíte udělat, je rozbalit ho na USB zařízení:

<informalexample><screen>
<prompt>#</prompt> <userinput>gzip -dc boot.img.gz &gt;/dev/<replaceable>sda</replaceable></userinput>
</screen></informalexample>

</para><para arch="powerpc">

K dispozici máte soubor <filename>hd-media/boot.img.gz</filename>,
který obsahuje všechny instalační soubory (včetně jádra), zavaděč
<command>yaboot</command> a jeho konfigurační soubor. Na USB zařízení
vytvořte <command>mac-fdisk</command>em oblast typu
<quote>Apple_Bootstrap</quote> (příkaz <userinput>C</userinput>)
a rozbalte do ní stažený obraz:

<informalexample><screen>
<prompt>#</prompt> <userinput>gzip -dc boot.img.gz &gt;/dev/<replaceable>sda2</replaceable></userinput>
</screen></informalexample>

</para><warning><para>

Tímto zničíte veškerá data na zařízení, takže se raději dvakrát
přesvědčte, že pro svou klíčenku používáte správné jméno zařízení.

</para></warning><para>

Na klíčence nyní bude jedna velká oblast typu <phrase
arch="x86">FAT16.</phrase> <phrase arch="powerpc">HFS.</phrase>
Připojte ji (<userinput>mount <replaceable
arch="x86">/dev/sda</replaceable> <replaceable
arch="powerpc">/dev/sda2</replaceable> /mnt</userinput>) a nakopírujte
na ni ISO obraz malého instalačního CD (viz <xref
linkend="usb-add-iso"/>). Nyní stačí klíčenku odpojit
(<userinput>umount /mnt</userinput>) a je hotovo.

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>Kopírování souborů &mdash; pružná cesta</title>
<para>

Pokud máte rádi více pružnosti, nebo jen chcete zjistit <quote>co se
děje za oponou</quote>, můžete použít následující metodu<phrase
arch="not-x86">.</phrase><phrase arch="x86">, ve které mj. ukážeme,
jak místo celého USB zařízení použít pouze první oblast.</phrase>

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

  </sect2>

  <sect2 id="usb-add-iso">
  <title>Přidání ISO obrazu</title>
<para>

Instalační systém bude na USB zařízení hledat ISO obraz instalačního
CD, ze kterého si zkopíruje další data potřebná pro instalaci. Dalším
krokem je tedy nakopírování ISO obrazu instalačního CD na
klíčenku. Použít můžete obrazy typu businesscard, netinst nebo, pokud
se vejde, dokonce celé první CD. Na názvu obrazu nezáleží, ale musí
končit příponou <filename>.iso</filename>.

</para><para>

Pokud nechcete použít ISO obraz a místo toho budete chtít instalovat
ze sítě, předchozí krok samozřejmě přeskočte. Kromě toho budete muset
použít ramdisk (<filename>initrd.gz</filename>) z adresáře
<filename>netboot</filename>, protože ramdisk v adresáři
<filename>hd-media</filename> neobsahuje podporu pro instalaci ze sítě.

</para><para>

Jestliže jste hotovi, odpojte klíčenku (<userinput>umount
/mnt</userinput>) a zapněte ochranu proti zápisu.

</para>
  </sect2>

  <!-- TODO: doesn't this section belong later? -->
  <sect2 arch="x86">
  <title>Problémy se zaváděním z USB klíčenky</title>
<warning><para>

Pokud váš systém odmítá zavádění z klíčenky, může to být tím, že je na
klíčence neplatný hlavní zaváděcí záznam (MBR). Opravit jej můžete
programem <command>install-mbr</command> z balíku
<classname>mbr</classname>:

<informalexample><screen>
<prompt>#</prompt> <userinput>install-mbr /dev/<replaceable>sda</replaceable></userinput>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
