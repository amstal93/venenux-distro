<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43773 -->

 <sect1 id="installation-media">
 <title>インストールに利用できるメディア</title>

<para>

<!--
This section will help you determine which different media types you can use to
install Debian. For example, if you have a floppy disk drive on your machine,
it can be used to install Debian. There is a whole chapter devoted to media,
<xref linkend="install-methods"/>, which lists the advantages and
disadvantages of each media type. You may want to refer back to this page once
you reach that section.
-->
本節は、Debian をインストールするのに、
どのメディアを使用するかを決める助けとなるでしょう。
例えば、マシンにフロッピーディスクドライブがあれば、
Debian をインストールするのに使用することができます。
各メディアに対して利点と欠点を挙げた、
章全体をメディアに費やした章 (<xref linkend="install-methods"/>) があります。
その章から、このページをもう一度参照するかもしれません。

</para>

  <sect2 condition="supports-floppy-boot"><title>フロッピーディスク</title>
<para>

<!--
In some cases, you'll have to do your first boot from floppy disks.
Generally, all you will need is a
high-density (1440 kilobytes) 3.5 inch floppy drive.
-->
いくつかの状況では、最初の起動はフロッピーディスクから行う必要があるでしょう。
通常 3.5 インチ高密度 (1440 kB) フロッピードライブがあれば充分です。

</para><para arch="powerpc">

<!--
For CHRP, floppy support is currently broken.
-->
CHRP フロッピーのサポートは、現在中断しています。

</para>
  </sect2>

  <sect2><title>CD-ROM/DVD-ROM</title>

<note><para>

<!--
Whenever you see <quote>CD-ROM</quote> in this manual, it applies to both
CD-ROMs and DVD-ROMs, because both technologies are really
the same from the operating system's point of view, except for some very
old nonstandard CD-ROM drives which are neither SCSI nor IDE/ATAPI.
-->
このマニュアルで <quote>CD-ROM</quote> と記述してある場合は、
オペレーティングシステムから見て等価なので、
CD-ROM・DVD-ROM と見なしてください。
(SCSI でも IDE/ATAPI でもないような、非常に古く非標準な CD-ROM ドライブを除く)

</para></note><para>

<!--
CD-ROM based installation is supported for some architectures.
On machines which support bootable CD-ROMs, you should be able to do a
completely
<phrase arch="not-s390">floppy-less</phrase>
<phrase arch="s390">tape-less</phrase>
installation.  Even if your system doesn't
support booting from a CD-ROM, you can use the CD-ROM in conjunction
with the other techniques to install your system, once you've booted
up by other means; see <xref linkend="boot-installer"/>.
-->
いくつかのアーキテクチャでは CD-ROM ベースのインストールをサポートしています。
起動可能な CD-ROM をサポートしたマシンでは、
<phrase arch="not-s390">フロッピーを必要としない</phrase>
<phrase arch="s390">テープを必要としない</phrase>
完全なインストールが可能です。
CD-ROM からの起動ができないシステムでは、
そのほかのテクニックを組み合わせれば
インストールに CD-ROM を使えます。
<xref linkend="boot-installer"/> を参照して一度他の方法で起動してください。

</para><para arch="x86">

<!--
Both SCSI and IDE/ATAPI CD-ROMs are supported.  In addition, all
non-standard CD interfaces supported by Linux are supported by the
boot disks (such as Mitsumi and Matsushita drives).  However, these
models might require special boot parameters or other massaging to get
them to work, and booting off these non-standard interfaces is
unlikely.  The <ulink url="&url-cd-howto;">Linux CD-ROM HOWTO</ulink>
contains in-depth information on using CD-ROMs with Linux.
-->
SCSI・IDE/ATAPI の CD-ROM はともにサポートされています。
さらに、Linux でサポートされている (ミツミや松下などの) 非標準の CD 
インターフェースも、起動ディスクでサポートされています。
しかし、これらのモデルには特別なブートパラメータや、動作のための他の処置が
必要なこともあります。また、これらの非標準インターフェースの CD-ROM 
から起動することは困難です。
Linux 上で CD-ROM を利用するための詳細な情報は、
<ulink url="&url-cd-howto;">Linux CD-ROM HOWTO</ulink>
にあります。

</para><para arch="x86">

<!--
USB CD-ROM drives are also supported, as are FireWire devices that
are supported by the ohci1394 and sbp2 drivers.
-->
ohci1394 や sbp2 ドライバでサポートしている FireWire デバイスと同様、
USB CD-ROM ドライブもサポートしています。

</para><para arch="alpha">

<!--
Both SCSI and IDE/ATAPI CD-ROMs are supported on &arch-title;, as long
as the controller is supported by the SRM console.  This rules out many
add-on controller cards, but most integrated IDE and SCSI chips and
controller cards that were provided by the manufacturer can be expected
to work.  To find out whether your device is supported from the SRM
console, see the <ulink url="&url-srm-howto;">SRM HOWTO</ulink>.
-->
コントローラが SRM コンソールでサポートされている限り、
SCSI・IDE/ATAPI の CD-ROM はともに &arch-title; でサポートされています。
これにより多くのアドオンコントローラーカードが使えなくなりますが、
メーカーが提供している統合 IDE・SCSI チップやコントローラカードが、
動作すると予想できます。
デバイスが SRM コンソールでサポートされているかを知るには、
<ulink url="&url-srm-howto;">SRM HOWTO</ulink> をご覧ください。

</para><para arch="arm">

<!--
IDE/ATAPI CD-ROMs are supported on all ARM machines.
On RiscPCs, SCSI CD-ROMs are also supported.
-->
すべての ARM マシンで IDE/ATAPI CD-ROM をサポートしています。
RiscPC では SCSI CD-ROM もサポートしています。

</para><para arch="mips">

<!--
On SGI machines, booting from CD-ROM requires a SCSI CD-ROM drive
capable of working with a logical blocksize of 512 bytes. Many of the
SCSI CD-ROM drives sold on the PC market do not have this
capability. If your CD-ROM drive has a jumper labeled
<quote>Unix/PC</quote> or <quote>512/2048</quote>, place it in the
<quote>Unix</quote> or <quote>512</quote> position.
To start the install, simply choose the <quote>System installation</quote>
entry in the firmware.  The Broadcom BCM91250A supports standard IDE devices,
including CD-ROM drives, but CD images for this platform are currently not
provided because the firmware doesn't recognize CD drives.  In order to
install Debian on an Broadcom BCM91480B evaluation board, you need an PCI
IDE, SATA or SCSI card.
-->
SGI マシンで CD-ROM から起動するには、512 バイトの論理ブロックサイズを
扱える SCSI CD-ROM ドライブが必要です。
PC 向けに売られている SCSI CD-ROM の多くは、この機能がありません。
お持ちの CD-ROM ドライブに <quote>Unix/PC</quote> とか
<quote>512/2048</quote> というラベルのついたジャンパがあったら、
<quote>Unix</quote> または <quote>512</quote> の方にしてください。
インストールを始めるには、単にファームウェアの 
<quote>System installation</quote> エントリを選択してください。
Broadcom BCM91250A は、
(CD-ROM ドライブを含む) 標準 IDE デバイスをサポートしていますが、
ファームウェアが CD ドライブを認識できないため、
現在このプラットフォーム用の CD イメージは提供されていません。
Broadcom BCM91480B 評価ボードに Debian をインストールするには、
PCI の IDE, SATA, SCSI いずれかのカードが必要です。

</para><para arch="mipsel">

<!--
On DECstations, booting from CD-ROM requires a SCSI CD-ROM drive
capable of working with a logical blocksize of 512 bytes. Many of the
SCSI CD-ROM drives sold on the PC market do not have this capability.
If your CD-ROM drive has a jumper labeled <quote>Unix/PC</quote> or
<quote>512/2048</quote>, place it in the <quote>Unix</quote> or
<quote>512</quote> position.
-->
DECstations で CD-ROM から起動するには、512 バイトの論理ブロックサイズを
扱える SCSI CD-ROM ドライブが必要です。
PC 向けに売られている SCSI CD-ROM の多くは、この機能がありません。
お持ちの CD-ROM ドライブに <quote>Unix/PC</quote> とか
<quote>512/2048</quote> というラベルのついたジャンパがあったら、
<quote>Unix</quote> または <quote>512</quote> の方にしてください。

</para><para arch="mipsel">

<!--
CD 1 contains the installer for the r3k-kn02 subarchitecture
(the R3000-based DECstations 5000/1xx and 5000/240 as well as
the R3000-based Personal DECstation models), CD 2 the
installer for the r4k-kn04 subarchitecture (the R4x00-based
DECstations 5000/150 and 5000/260 as well as the Personal DECstation
5000/50).
-->
CD 1 に入っているインストーラは、r3k-kn02 サブアーキテクチャ用です
(R3000 ベースの DECstations 5000/1xx と 5000/240 と、
R3000 ベースの Personal DECstation モデル)。
CD 2 に入っているインストーラは r4k-kn04 サブアーキテクチャ用です
(R4x00 ベースの DECstations 5000/150 と 5000/260 と、
Personal DECstation 5000/50)。

</para><para arch="mipsel">

<!--
To boot from CD, issue the command <userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
on the firmware prompt, where <replaceable>#</replaceable> is the
number of the TurboChannel device from which to boot (3 on most
DECstations) and <replaceable>id</replaceable> is the SCSI ID of the
CD-ROM drive.  If you need to pass additional parameters, they can
optionally be appended with the following syntax:
-->
CD から起動するには、ファームウェアのプロンプトで<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
というコマンドを実行してください。ここで <replaceable>#</replaceable>
は起動しようとする TurboChannel デバイスの番号
(ほとんどの DECstation では 3) で、
<replaceable>id</replaceable> は CD-ROM ドライブの SCSI ID です。
他にも渡すべきパラメータがある場合は、次の書式に従えば追加できます。

</para><para arch="mipsel">

<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=value1 param2=value2 ...</userinput>

</para>
  </sect2>

  <sect2><title>ハードディスク</title>

<para>

<!--
Booting the installation system directly from a hard disk is another option
for many architectures. This will require some other operating system
to load the installer onto the hard disk.
-->
ハードディスクからインストールシステムを直接ブートするのは、
多くのアーキテクチャで使える方法です。
これは他の OS に、
ハードディスク上にあるインストーラをロードするよう要求します。

</para><para arch="m68k">

<!--
In fact, installation from your local disk is the preferred
installation technique for most &architecture; machines.
-->
実際に &architecture; のマシンのほとんどでは、
ローカルディスクからのインストールが好ましいでしょう。

</para><para arch="sparc">

<!--
Although the &arch-title; does not allow booting from SunOS
(Solaris), you can install from a SunOS partition (UFS slices).
-->
&arch-title; では SunOS (Solaris) からの起動は
サポートされていませんが、SunOS パーティション (UFS スライス) 
からインストールすることは可能です。

</para>
  </sect2>

  <sect2 condition="bootable-usb"><title>USB メモリ</title>

<para>

<!--
Many Debian boxes need their floppy and/or CD-ROM drives only for
setting up the system and for rescue purposes. If you operate some
servers, you will probably already have thought about omitting those
drives and using an USB memory stick for installing and (when
necessary) for recovering the system. This is also useful for small
systems which have no room for unnecessary drives.
-->
多くの Debian ボックスではシステムのセットアップやレスキュー用途のみに
フロッピー・CD-ROM ドライブが必要です。
複数のサーバの操作をしている場合、
そんなドライブを省略して USB メモリを使用して、
システムのインストールや (必要なら) 修復することを既に考えていることでしょう。
また、不要なドライブを格納する余地のない、
小さなシステムに対しても便利です。

</para>
  </sect2>

<!--
  <sect2><title>Network</title>
-->
  <sect2><title>ネットワーク</title>

<para>

<!--
The network can be used during the installation to retrieve files needed
for the installation. Whether the network is used or not depends on the
installation method you choose and your answers to certain questions that
will be asked during the installation. The installation system supports
most types of network connections (including PPPoE, but not ISDN or PPP),
via either HTTP or FTP. After the installation is completed, you can also
configure your system to use ISDN and PPP.
-->
インストールに必要なファイルをインストール中に取得するのに、
ネットワークを使用できます。
ネットワークを使用するかどうかは、あなたが選択したインストール方法と、
インストール中の質問への答に依存します。
インストールシステムは、ネットワークへのほとんどの接続法 (PPPoE を含む。
ISDN や PPP は不可) 上での、HTTP と FTP のどちらともサポートしています。
インストール完了後に、ISDN や PPP を使用するようにシステムの設定ができます。

</para><para condition="supports-tftp">

<!--
You can also <emphasis>boot</emphasis> the installation system over the
network. <phrase arch="mips">This is the preferred installation technique
for &arch-title;.</phrase>
-->
また、インストールシステムを、
ネットワーク越しに<emphasis>起動</emphasis>することもできます。
<phrase arch="mips">&arch-title; ではこれが好ましいでしょう。</phrase>

</para><para condition="supports-nfsroot">

<!--
Diskless installation, using network booting from a local area network
and NFS-mounting of all local filesystems, is another option.
-->
ネットワーク越しに起動を行い、
すべてのローカルファイルシステムを NFS でマウントして、
ディスクレスインストールをすることも一つの選択です。

</para>
  </sect2>

  <sect2><title>Un*x・GNU システム</title>

<para>

<!--
If you are running another Unix-like system, you could use it to install
&debian; without using the &d-i; described in the rest of this
manual. This kind of install may be useful for users with otherwise
unsupported hardware or on hosts which can't afford downtime.  If you
are interested in this technique, skip to the <xref
linkend="linux-upgrade"/>.
-->
他の Unix ライクシステムが稼働していれば、本マニュアルの残りで説明している 
&d-i; を使用しないで、 &debian; をインストールできます。
このインストール方法なら、他の方法ではサポートしないハードウェアや、
ダウンタイムを用意できないユーザにとって便利です。
この方法に興味があれば、<xref linkend="linux-upgrade"/> へ
スキップしてください。

</para>
  </sect2>

  <sect2><title>サポートする記憶装置</title>

<para>

<!--
The Debian boot disks contain a kernel which is built to maximize the
number of systems it runs on.  Unfortunately, this makes for a larger
kernel, which includes many drivers that won't be used for your
machine (see <xref linkend="kernel-baking"/> to learn how to
build your own kernel).  Support for the widest possible range of
devices is desirable in general, to ensure that Debian can be
installed on the widest array of hardware.
-->
Debian の起動ディスクには、様々なシステムに最大限対応したカーネルが
収められています。そのため残念ながら、まったく使われることのないたくさんの
ドライバがカーネルを肥大化させています
 (再構築の仕方は <xref linkend="kernel-baking"/> をご覧ください)。
しかし、様々なハードウェアへ確実に Debian をインストールするには
できるだけ幅広いデバイスをサポートするのが望ましいでしょう。

</para><para arch="x86">

<!--
Generally, the Debian installation system includes support for floppies,
IDE drives, IDE floppies, parallel port IDE devices, SCSI controllers and
drives, USB, and FireWire.  The supported file systems include FAT,
Win-32 FAT extensions (VFAT) and NTFS.
-->
一般的に Debian のインストーラは、
フロッピー、IDE ドライブ、IDE フロッピー、パラレルポートの IDE デバイス、
SCSI コントローラとドライブ、USB、FireWire をサポートしています。
サポートしているファイルシステムは、FAT、Win-32 拡張 FAT (VFAT)、NTFS です。

</para><para arch="i386">

<!--
Disk interfaces that emulate the <quote>AT</quote> hard disk interface
&mdash; often called MFM, RLL, IDE, or ATA &mdash; are supported.  Very old
8&ndash;bit hard disk controllers used in the IBM XT computer are supported
only as a module. SCSI disk controllers from many different manufacturers
are supported. See the
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
for more details.
-->
MFM、RLL、IDE、ATA といった <quote>AT</quote> 
ハードディスクインターフェースをエミュレートする、
ディスクインターフェースをサポートしています。
IBM XT コンピュータで使用される、
非常に古い 8&ndash;ビットハードディスクコントローラも、
モジュールとしてのみですがサポートしています。
また、多くのメーカーの SCSI ディスクコントローラもサポートしています。
詳細は
<ulink url="&url-hardware-howto;">Linux ハードウェア互換性 HOWTO</ulink>
をご覧ください。

</para><para arch="m68k">

<!--
Pretty much all storage systems supported by the Linux kernel are
supported by the Debian installation system.  Note that the current
Linux kernel does not support floppies on the Macintosh at all, and
the Debian installation system doesn't support floppies for Amigas.
Also supported on the Atari is the Macintosh HFS system, and AFFS as a
module.  Macs support the Atari (FAT) file system.  Amigas support the
FAT file system, and HFS as a module.
-->
Linux カーネルでサポートされている外部記憶装置のほとんどすべて
が、Debian のインストーラでサポートされています。
ただし、現行の Linux カーネルが Macintosh のフロッピーをまったくサポート
していないこと、Debian のインストーラが Amiga
のフロッピーをサポートしていないことにはご注意ください。
また Atari では、Macintosh HFS システムと、モジュールとして AFFS
 がサポートされています。
Mac は Atari (FAT) ファイルシステムをサポートしています。
Amiga は、FAT ファイルシステムと、モジュールとして HFS をサポートしています。

</para><para arch="sparc">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system. The following SCSI drivers are supported in the default
kernel:
-->
Linux カーネルでサポートされる外部記憶装置は、
すべてこのブートシステムでもサポートされています。
以下の SCSI ドライバがデフォルトのカーネルでサポートされています。

<itemizedlist>
<listitem><para>

Sparc ESP

</para></listitem>
<listitem><para>

PTI Qlogic,ISP

</para></listitem>
<listitem><para>

Adaptec AIC7xxx

</para></listitem>
<listitem><para>

NCR and Symbios 53C8XX

</para></listitem>
</itemizedlist>

<!--
IDE systems (such as the UltraSPARC 5) are also supported. See
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>
for more information on SPARC hardware supported by the Linux kernel.
-->
また、(UltraSPARC 5 のような) IDE システムもサポートされています。
Linux カーネルによる SPARC ハードウェアのサポートに関する詳しい情報は
<ulink url="&url-sparc-linux-faq;">SPARC プロセッサ用 Linux FAQ</ulink>
をご覧ください。

</para><para arch="alpha">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  This includes both SCSI and IDE disks.  Note, however,
that on many systems, the SRM console is unable to boot from IDE drives,
and the Jensen is unable to boot from floppies.  (see
<ulink url="&url-jensen-howto;"></ulink>
for more information on booting the Jensen)
-->
Linux カーネルでサポートしている外部記憶装置は、
すべてこの起動システムでもサポートしています。
これには SCSI ディスクや IDE ディスクも含まれています。
しかし多くの機種において、SRM コンソールが IDE ドライブから起動できないことや、
Jensen がフロッピーから起動できないことにご注意ください。
(Jensen の起動に関するより詳しい情報については
<ulink url="&url-jensen-howto;"></ulink> をご覧ください)

</para><para arch="powerpc">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
floppies on CHRP systems at all.
-->
Linux カーネルでサポートされる外部記憶装置は、
すべてこのブートシステムでもサポートされています。
ただし現行の Linux カーネルは、 CHRP システムのフロッピーを
まったくサポートしていないことにご注意ください。

</para><para arch="hppa">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
the floppy drive.
-->
Linux カーネルでサポートされる外部記憶装置は、
すべてこのブートシステムでもサポートされています。
ただし、現行の Linux カーネルは、フロッピードライブを
サポートしていないことにご注意ください。

</para><para arch="mips">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.
-->
Linux カーネルでサポートされる外部記憶装置は、
すべてこのブートシステムでもサポートされています。

</para><para arch="s390">

<!--
Any storage system supported by the Linux kernel is also supported by
the boot system.  This means that FBA and ECKD DASDs are supported with
the old Linux disk layout (ldl) and the new common S/390 disk layout (cdl).
-->
Linux カーネルでサポートされる外部記憶装置は、
すべてこのブートシステムでもサポートされています。
これは FBA や ECKD DASD が古い Linux ディスクレイアウト (ldl) や、
新しい S/390 共通ディスクレイアウト (cdl) をサポートするということです。

</para>

  </sect2>

 </sect1>
