#!/bin/sh
set -e

if [ -f /etc/mtab ]; then
	MTAB=/etc/mtab
else
	MTAB=/proc/mounts
fi

# detect which is the root partition: /target
rootfs=`sed -ne '/^[^ ]* \/target /s/ .*//p' $MTAB`

# detect which hard drive has /target/boot mounted on it. this does *not*
# have to be the same as the drive /target is mounted on.
bootfs=`sed -ne '/^[^ ]* \/target\/boot /s/ .*//p' $MTAB`

if [ "$bootfs" = '' ]; then
	bootfs=$rootfs
fi

# kernels go in /boot, so palo must go on that device
rootdev=`echo $rootfs | sed 's/part[0-9]\+$/disc/'` 
bootdev=`echo $bootfs | sed 's/part[0-9]\+$/disc/'` 

bootpart=`echo $bootfs | sed 's/^.*part\([0-9]\+\)$/\1/'`

if [ "$rootfs" = "$bootfs" ]; then
	kernel=$bootpart/boot/vmlinux
else
	kernel=$bootpart/vmlinux
fi

# See if there's an initrd.img, if so add an initrd= parameter.
if [ -e /target/boot/initrd.img ]; then
	if [ "$rootfs" = "$bootfs" ]; then
		initrd=$bootpart/boot/initrd.img
	else
		initrd=$bootpart/initrd.img
	fi
	initrd_param="initrd=$initrd"
fi

bootdev=`mapdevfs $bootdev`
rootfs=`mapdevfs $rootfs`

vmlinux=`cd /target/boot && ls -l vmlinux | sed 's/^.* -> \(.*\)$/\1/'`

# write out palo.conf
echo "--commandline=$kernel root=$rootfs $initrd_param HOME=/" > /target/etc/palo.conf
echo "--recoverykernel=/boot/$vmlinux" >> /target/etc/palo.conf
echo "--init-partitioned=$bootdev" >> /target/etc/palo.conf

set +e
apt-install palo
set -e

chroot /target /sbin/palo
