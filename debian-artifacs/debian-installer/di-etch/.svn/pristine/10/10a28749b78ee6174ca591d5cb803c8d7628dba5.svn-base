<!-- retain these comments for translator revision tracking -->
<!-- original version: 43734 -->
<!-- translated by Herbert Parentes Fortes Neto (hpfn) 2006.02.05 -->
<!-- revised by Herbert Parentes Fortes Neto (hpfn) 2006.12.11 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.12.25 -->
<!-- updated 43512:43734 by Felipe Augusto van de Wiel (faw) 2007.01.15 -->

   <sect3 id="pkgsel">
   <title>Selecionando e Instalando Programas</title>

<para>

Durante o processo de instalação, lhe será dada a oportunidade de selecionar
programas adicionais para serem instalados. Ao invés de pegar pacotes
individuais de programas em &num-of-distrib-pkgs; pacotes disponíveis, esse
estágio do processo de instalação foca na seleção e instalação de coleções
de programas pré-definidas para rapidamente configurar seu computador a 
executar várias tarefas.

</para><para>

<!-- TODO: Should explain popcon first -->

Então, você tem a habilidade de escolher <emphasis>tarefas
(<quote>tasks</quote>)</emphasis> primeiro, e depois adicionar mais pacotes
individuais.  Essas tarefas representam, de forma ampla, um número de
diferentes serviços ou itens que você quer fazer com seu computador, como
<quote>Ambiente Desktop</quote>, <quote>Servidor Web</quote>, ou
<quote>Servidor de Impressão</quote><footnote>

<para>

Você deve ter isso definido no momento que esta lista for apresentada, o
instalador está meramente invocando o programa <command>tasksel</command>. Ele
pode ser executado a qualquer momento após a instalação para instalar mais
pacotes (ou removê-los), ou você pode usar uma ferramenta mais aprimorada
como o <command>aptitude</command>.
Se você estiver procurando por um pacote específico, após ter sido
concluída a instalação, simplesmente execute 
<userinput>aptitude install<replaceable>pacote</replaceable></userinput>, 
onde <replaceable>pacote</replaceable> é o nome do pacote que você
está procurando.

</para>

</footnote>. <xref linkend="tasksel-size-list"/> lista o espaço
requerido para as tarefas disponíveis.

</para><para>

Algumas tarefas podem estar pré-selecionadas com base nas características do
computador que está sendo usado. Se você discordar dessas seleções você
pode desmarcá-las. Você pode até mesmo optar por não instalar nenhuma das
tarefas neste momento.

</para>
<note><para>

<!-- TODO: Explain the "Standard system" task first -->

A tarefa <quote>Ambiente Desktop</quote> irá instalar o ambiente de desktop
GNOME. As opções oferecidas pelo instalador atualmente não permitem selecionar
um ambiente de desktop diferente, como por exemplo o KDE.

</para><para>

É possível fazer com que o instalador instale o KDE usando a pré-configuração
(veja <xref linkend="preseed-pkgsel"/>) ou adicionando
<literal>tasks="standard, kde-desktop"</literal> no prompt da inicialização
ao iniciar o instalador. Contudo, isso funcionará apenas se os pacotes
de que o KDE depende estiverem realmente disponíveis. Se você estiver fazendo a
instalação usando uma imagem de CD completa, eles precisarão ser baixados a
partir de um espelho, já que os pacotes do KDE não são incluídos no primeiro
CD completo; fazer a instalação desta maneira deve funcionar bem se você
estiver usando uma imagem de DVD ou qualquer outro método de instalação.

</para><para>

As várias tarefas de servidor irão instalar programas de forma aproximada ao
que se segue.
Servidor DNS: <classname>bind9</classname>;
Servidor de Arquivo: <classname>samba</classname>, <classname>nfs</classname>;
Servidor de correio eletrônico: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Servidor de Impressão: <classname>cups</classname>;
Banco de Dados SQL: <classname>postgresql</classname>;
Servidor web: <classname>apache</classname>.

</para></note>
<para>

Uma vez que você tenha selecionado suas tarefas, selecione
<guibutton>OK</guibutton>. Neste ponto, o <command>aptitude</command> irá
instalar os pacotes que são parte das tarefas que você selecionou.

</para>
<note><para>

Na interface de usuário padrão do instalador, você pode usar a barra de espaço
para selecionar uma tarefa.

</para></note>
<para>

Você deveria estar ciente de que especialmente a tarefa Desktop é muito
grande. Especialmente quando instalando de um CD-ROM normal em combinação
com um espelho de pacotes que não está no CD-ROM, o instalador pode querer
obter vários pacotes através da rede. Se você tem uma conexão de Internet
relativamente lenta, isto pode demorar bastante. Não há opção para cancelar
a instalação de pacotes uma vez que ela tenha começado.

</para><para>

Mesmo quando pacotes estão inclusos no CD-ROM, o instalador pode obtê-los
de um espelho se a versão disponível no espelho é mais recente que a
inclusa no CD-ROM. Se você está instalando a distribuição estável
(<quote>stable</quote>), isto pode acontecer após um lançamento pontual
(<quote>point release</quote> -- uma atualização da lançamento original da
versão estável); se você está instalando a distribuição de testes
(<quote>testing</quote>) isto irá acontecer se você estiver usando uma
imagem antiga.

</para><para>

Cada pacote que você selecionar com o <command>tasksel</command> é baixado,
desempacotado e então instalado pelos programas <command>apt-get</command> e
<command>dpkg</command>. Caso um programa em particular necessite de mais
informações do usuário, ele irá perguntar à você durante o processamento.

</para>
   </sect3>
