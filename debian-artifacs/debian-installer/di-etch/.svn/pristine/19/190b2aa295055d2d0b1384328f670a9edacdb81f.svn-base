#!/bin/sh
# verify and optionally save out the file
set -e

NL="
"

NOTEST=""
if [ "$1" = "--invalid" ]; then
	NOTEST=1
	shift
fi

file="$1"
saveto="$2"

logoutput=""
if [ "$CATCHLOG" ]; then
	logoutput="log-output -t apt-setup"
fi

chroot=
if [ "$ROOT" ]; then
	chroot=chroot
fi

saveline () {
	if [ "$saveto" ]; then
		echo "$*" >> $saveto
	fi
}

valid () {
	line="$1"

	tmp=$($chroot $ROOT tempfile)
	echo "$line" > $ROOT$tmp
	
	if $logoutput $chroot $ROOT apt-get -o APT::Get::List-Cleanup=false \
		-o Dir::Etc::sourcelist=$tmp $ASV_TIMEOUT update
	then
		rm -f $ROOT$tmp
	else
		rm -f $ROOT$tmp
		false
	fi
}

items=0
gooditems=0

OLDIFS="$IFS"
IFS="$NL"
# Can't just iterate over $(cat $file) because that kills newlines, so
# introduce a dummy colon.
for line in $(sed 's/^/:/' $file); do
	IFS="$OLDIFS"
	line="${line#:}"
	if expr "$line" : '.*[^ ].*' >/dev/null && [ "$(expr "$line" : "#")" != 1 ]; then
		items=$(expr "$items" + 1)
		# Write blank line between generators
		if [ $items = 1 ] && [ -f "$saveto" ]; then
			saveline ""
		fi

		if [ -z "$NOTEST" ] && valid "$line"; then
			gooditems=$(expr "$gooditems" + 1)
			saveline "$line"
		else
			saveline "# Line commented out by installer because it failed to verify:"
			saveline "#$line"
		fi
	else
		# Ignore leading empty lines
		if [ $items != 0 ] || [ "$line" ]; then
			saveline "$line"
		fi
	fi
done

if [ "$gooditems" != "$items" ]; then
	exit 1
fi
