<!-- original version: 44026 -->

   <sect3 id="mdcfg">
   <title>Meervoudige schijfapparaten configureren (Software-RAID)</title>
<para>

Als u in uw computer meer dan één harde schijf<footnote><para>

U zou zelfs een Software-RAID kunnen samenstellen uit verschillende
partities op één fysieke harde schijf, maar daarmee bereikt u niets
zinvols.

</para></footnote> heeft, kunt u <command>mdcfg</command> gebruiken
om uw schijfeenheden te configureren voor betere prestaties en/of
grotere betrouwbaarheid van uw gegevens. Het resultaat wordt een
<firstterm>Multidisk Device</firstterm> (MD) of, naar zijn meest
bekende variant, <firstterm>Software-RAID</firstterm>).

</para><para>

MD is in feite niets meer dan een verzameling partities op verschillende
harde schijven die worden gecombineerd om gezamelijk één
<emphasis>logisch</emphasis> apparaat te vormen. Dit apparaat kan vervolgens
worden gebruikt als een gewone partitie (dat wil zeggen dat u het in
<command>partman</command> kunt formatteren en een aanhechtpunt kunt toewijzen.

</para><para>

Het voordeel dat u behaalt, is afhankelijk van het soort MD-apparaat dat u
creëert. Op dit moment worden ondersteund:

<variablelist>
<varlistentry>

<term>RAID0</term><listitem><para>

Levert hoofdzakelijk prestatieverbetering. RAID0 splitst alle binnenkomende
gegevens op in zogenaamde <firstterm>stripes</firstterm> en verdeelt deze
gelijkmatig over elke schijf in de reeks. Dit kan de snelheid van lees- en
schrijfoperaties verhogen, maar als één van de schijven defect raakt,
verliest u <emphasis>alle</emphasis> gegevens (een deel van de informatie
staat nog wel op de 'gezonde' schijf/schijven, maar een ander deel
<emphasis>stond</emphasis> op de defecte schijf).

</para><para>

RAID0 wordt bijvoorbeeld veel toegepast voor video editing.

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID1</term><listitem><para>

Is geschikt voor situaties waar betrouwbaarheid de belangrijkste
overweging is. Het bestaat uit verschillende (gewoonlijk twee) partities
van gelijke grootte; beide partities bevatten exact dezelfde gegevens.
In essentie betekent dit drie dingen. Ten eerste heeft u, als één van de
schijven defect raakt, nog altijd de gegevens op de tweede schijf. Ten
tweede kunt u effectief slechts een deel van de beschikbare capaciteit
gebruiken (meer exact: de grootte van de kleinste partitie in de RAID).
Ten derde wordt de belasting bij het lezen van gegevens verdeeld over
de schijven; dit kan de prestaties verbeteren als op een server over het
algemeen meer lees- dan schrijfacties plaatsvinden, zoals bij een
bestandsserver.

</para><para>

Desgewenst kunt u een reserve harde schijf in de reeks opnemen die bij
een storing de plaats zal innemen van de defecte schijf.

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID5</term><listitem><para>

Is een goed compromis tussen snelheid, betrouwbaarheid en redundantie van
gegevens. Bij RAID5 worden binnenkomende gegevens gesplitst in stripes en
gelijkmatig verdeeld over alle harde schijven op één na (vergelijkbaar met
RAID0). In afwijking van RAID0, wordt bij RAID5 ook
<firstterm>pariteits</firstterm>informatie berekend die op de overgebleven
schijf wordt weggeschreven. De harde schijf met de pariteitsinformatie is
niet statisch (dat zou RAID4 zijn), maar wordt periodiek gewijzigd zodat de
pariteitsinformatie gelijkelijk wordt verdeeld over de schijven. Als één van
de schijven defect raakt, kunnen de ontbrekende gegevens worden berekend op
basis van de resterende gegevens en hun pariteit. RAID5 dient te zijn
opgebouwd uit tenminste drie actieve partities. Desgewenst kunt u een reserve
harde schijf in de reeks opnemen die bij een storing de plaats zal innemen
van de defecte schijf.

</para><para>

Zoals u kunt zien, heeft RAID5 een vergelijkbare mate van betrouwbaarheid
als RAID1 terwijl de opslag minder redundant is. Aan de andere kant is het,
in verband met de berekening van de partiteitsinformatie, mogelijk iets
trager bij schrijfoperaties dan RAID0 .

</para></listitem>
</varlistentry>
</variablelist>

Samenvattend:

<informaltable>
<tgroup cols="5">
<thead>
<row>
  <entry>Type</entry>
  <entry>Minimum apparaten</entry>
  <entry>Reserve schijf</entry>
  <entry>Overleeft een storing?</entry>
  <entry>Beschikbare ruimte</entry>
</row>
</thead>

<tbody>
<row>
  <entry>RAID0</entry>
  <entry>2</entry>
  <entry>nee</entry>
  <entry>nee</entry>
  <entry>Grootte van de kleinste partitie vermenigvuldigd met het aantal apparaten</entry>
</row>

<row>
  <entry>RAID1</entry>
  <entry>2</entry>
  <entry>optioneel</entry>
  <entry>ja</entry>
  <entry>Grootte van de kleinste partitie in de RAID</entry>
</row>

<row>
  <entry>RAID5</entry>
  <entry>3</entry>
  <entry>naar keuze</entry>
  <entry>ja</entry>
  <entry>
    Grootte van de kleinste partitie vermenigvuldigd met
    (het aantal apparaten in de RAID minus één)
  </entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

Als u meer wilt weten over Software-RAID, raadpleeg dan de
<ulink url="&url-software-raid-howto;">Software RAID HOWTO</ulink>.

</para><para>

Om een MD-apparaat aan te maken, dient u de partities die u ervan onderdeel
wilt laten uitmaken, te markeren voor gebruik in een RAID.
U doet dit in <command>partman</command> in het menu met <guimenu>partitie
instellingen</guimenu>. Daar selecteert u <menuchoice><guimenu>Gebruiken
als:</guimenu> <guimenuitem>Fysiek volume voor RAID</guimenuitem></menuchoice>.

</para><warning><para>

Ondersteuning voor meervoudige schijfapparaten is relatief nieuwe
functionaliteit in het installatiesysteem. Het is mogelijk dat u daardoor
problemen ondervindt bij sommige RAID-varianten, of in de combinatie van MD
met sommige opstartladers als probeert een MD te gebruiken voor het
bestandssysteem root (<filename>/</filename>). Ervaren gebruikers kunnen
proberen om deze problemen op te lossen door bepaalde configuratie- of
installatiestappen handmatig uit te voeren vanuit een opdrachtschil.

</para></warning><para>

Vervolgens dient u in het hoofdmenu van <command>partman</command>
te kiezen voor <guimenuitem>Software-RAID instellen</guimenuitem>.
(N.B. Dit menu verschijnt pas nadat u tenminste één partitie heeft
gemarkeerd voor gebruik als <guimenuitem>Fysiek volume voor
RAID</guimenuitem>.)
Kies dan op het eerste scherm van <command>mdcfg</command> voor
<guimenuitem>MD-apparaat aanmaken</guimenuitem>. Er zal een lijst
met ondersteunde typen MD-apparaten worden getoond, waaruit u er één
kunt kiezen (bijvoorbeeld RAID1). Het vervolg is afhankelijk van het
geselecteerde type MD.

</para>

<itemizedlist>
<listitem><para>

RAID0 is eenvoudig &mdash; er zal een overzicht met beschikbare
RAID-partities worden getoond en uw enige taak is het selecteren van de
partities die u voor het MD-apparaat wilt gebruiken.

</para></listitem>
<listitem><para>

RAID1 is iets lastiger. Allereerst zal u worden gevraagd om het aantal
actieve en het aantal reserve eenheden voor het MD-apparaat in te
geven. Vervolgens dient u uit de lijst met beschikbare RAID-partities
eerst de partities te kiezen die actief moeten zijn en vervolgens de
reserve partities. Het aantal partities dat u selecteert moet gelijk zijn
aan het aantal dat u zojuist heeft opgegeven. Maak u geen zorgen; als u
een fout maakt en een afwijkend aantal partities selecteert, zal &d-i;
u beletten verder te gaan totdat dit gecorrigeerd is.

</para></listitem>
<listitem><para>

De configuratieprocedure voor RAID5 is vergelijkbaar met die voor RAID1.
Het enige verschil is dat u tenminste <emphasis>3</emphasis> actieve
partities dient te gebruiken.

</para></listitem>
</itemizedlist>

<para>

Het is zonder meer mogelijk om verschillende typen MD-apparaten naast
elkaar te hebben. Als u bijvoorbeeld drie 200GB harde schijven heeft
ten behoeve van MD, elk met twee 100 GB partities, kunt u de eerste
partities op alle drie de schijven samenvoegen tot één RAID0 (snelle
partitie van 300GB voor video editing) en de andere drie partities
(2 actief en 1 reserve) gebruiken voor RAID1 (als zeer betrouwbare
100GB partitie voor <filename>/home</filename>).

</para><para>

Nadat u de MD-apparaten naar uw tevredenheid heeft ingesteld, kunt u
<command>mdcfg</command> afsluiten om terug te keren naar
<command>partman</command> om op uw nieuwe MD-apparaten bestandssystemen
aan te maken en daaraan de gebruikelijke kenmerken, zoals aanhechtpunten,
toe te kennen.

</para>
   </sect3>
