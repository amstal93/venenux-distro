<!-- retain these comments for translator revision tracking -->
<!-- original version: 33725 -->
<!-- reviewed by Felipe Augusto van de Wiel (faw) 2006.04.07 -->

   <sect3 arch="powerpc">
   <title>Particionamento da memória stick USB na &arch-title;</title>
<para>

A maioria das memórias sticks USB não vêm pré-configuradas de uma
forma que possam ser inicializadas através do Open Firmware, você 
terá que reparticionar a memória stick. Em sistemas Mac, execute
o comando <userinput>mac-fdisk /dev/sda</userinput>, inicialize
o mapa da nova partição usando o comando <userinput>i</userinput>
e crie uma nova partição do tipo Apple_Bootstrap usando o comando
<userinput>C</userinput>. (note que a primeira <quote>partição</quote> será 
sempre o próprio mapa da partição). Então digite

<informalexample><screen>
$ hformat /dev/<replaceable>sda2</replaceable>
</screen></informalexample>

Tenha atenção de estar usando o nome de dispositivo correto para seu 
dispositivo de memória stick. O comando
<command>hformat</command> vem junto com o pacote da Debian
<classname>hfsutils</classname>.

</para><para>

Para iniciar um kernel após inicializar através de um dispositivo USB,
nós colocaremos um gerenciador de partida na memória stick. O 
gerenciador de partida <command>yaboot</command> pode ser instalado em
um sistema de arquivos HFS e pode ser reconfigurado simplesmente pela
edição de um arquivo de textos. Qualquer sistema operacional que 
suporte o sistema de arquivos HFS pode ser usado para realizar
alterações na configuração d gerenciador de partida.

</para><para>

A ferramenta <command>ybin</command> que vem com o 
<command>yaboot</command> não entende o formato de dispositivos de 
armazenamento USB, assim você terá que instalar o 
<command>yaboot</command> manualmente usando as ferramentas
<classname>hfsutils</classname>. Digite

<informalexample><screen>
$ hmount /dev/sda2
$ hcopy -r /usr/lib/yaboot/yaboot :
$ hattrib -c UNIX -t tbxi :yaboot
$ hattrib -b :
$ humount
</screen></informalexample>

Novamente, tenha cuidado ao usar o nome de dispositivos.
A partição não deverá estar montada durante este processo. Este procedimento
gravará um setor de partida na partição e usará os utilitários HFS para
marca-lo de forma que o Open Firmware possa inicializar através dele. 
Tendo feito isto, o resto da memória stick USB poderá ser preparada usando
utilitários padrões do Unix.

</para><para>

Monte a partição (<userinput>mount /dev/sda2 /mnt</userinput>) e
copie os seguintes arquivos do repositório da Debian para a memória
stick:

<itemizedlist>
<listitem><para>

<filename>vmlinux</filename> (binário do kernel)

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename> (imagem inicial de disco na ram)

</para></listitem>
<listitem><para>

<filename>yaboot.conf</filename> (arquivo de configuração do yaboot)

</para></listitem>
<listitem><para>

<filename>boot.msg</filename> (mensagem de inicialização personalizada)

</para></listitem>
<listitem><para>

Módulos opcionais do kernel

</para></listitem>
</itemizedlist>

</para><para>

O arquivo de configuração <filename>yaboot.conf</filename> deverá conter
as seguintes linhas:

<informalexample><screen>
default=install
root=/dev/ram

message=/boot.msg

image=/vmlinux
        label=install
        initrd=/initrd.gz
	initrd-size=10000
        read-only
</screen></informalexample>

Note que pode ser necessário aumentar o parâmetro
<userinput>initrd-size</userinput> 
dependendo do tamanho da imagem que estiver inicializando.

</para>
   </sect3>
