<!-- retain these comments for translator revision tracking -->
<!-- original version: 45435 -->

  <sect2 arch="m68k"><title>Eine Installationsmethode auswählen</title>

<para>

Einige &arch-title;-Unterarchitekturen bieten die Möglichkeit, entweder
einen 2.4.x- oder einen 2.2.x-Linux-Kernel zu booten. Wenn diese Auswahl
existiert, versuchen Sie den 2.4.x-Linux-Kernel. Der Installer sollte
dann weniger Speicher benötigen, da für einen 2.2.x-Kernel eine RAM-Disk
fester Größe erforderlich ist, wohingegen 2.4.x tmpfs benutzt.

</para><para>

Wenn Sie einen 2.2.x Linux-Kernel verwenden, müssen Sie den Kernelparameter
&ramdisksize; benutzen.

</para><para>

Stellen Sie sicher, dass Sie <userinput>root=/dev/ram</userinput> als einen der
Kernelparameter angeben.

</para><para>

Wenn Sie Probleme haben, besuchen Sie
<ulink url="&url-m68k-cts-faq;">cts's &arch-title; Debian-Installer FAQ</ulink>.

</para>

<itemizedlist>
       <listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
       <listitem><para><xref linkend="m68k-boot-atari"/></para></listitem>
       <listitem><para><xref linkend="m68k-boot-bvme6000"/></para></listitem>
       <listitem><para><xref linkend="m68k-boot-mac"/></para></listitem>
       <listitem><para><xref linkend="m68k-boot-mvme"/></para></listitem>
       <listitem><para><xref linkend="m68k-boot-q40"/></para></listitem>
</itemizedlist>


    <sect3 id="m68k-boot-amiga"><title>Amiga</title>
<para>

Die einzige Installationsmethode für Amiga-Systeme ist die von
Festplatte (siehe auch <xref linkend="m68k-boot-hd"/>).
<emphasis>Mit anderen Worten: die CD-ROM ist hier nicht bootfähig.</emphasis>

</para><para>


Amiga arbeitet derzeit nicht mit bogl, wenn Sie also diesbezügliche
Fehlermeldungen erhalten, müssen Sie den Bootparameter
<userinput>fb=false</userinput> verwenden.

</para>
    </sect3>

    <sect3 id="m68k-boot-atari"><title>Atari</title>
<para>

Auf Atari-Systemen kann der Installer entweder von Festplatte
(siehe <xref linkend="m68k-boot-hd"/>) oder von Floppy
(siehe <xref linkend="boot-from-floppies"/>) gebootet werden.
<emphasis>Mit anderen Worten: die CD-ROM ist hier nicht bootfähig.</emphasis>

</para><para>

Atari arbeitet derzeit nicht mit bogl, wenn Sie also diesbezügliche
Fehlermeldungen erhalten, müssen Sie den Bootparameter
<userinput>fb=false</userinput> verwenden.

</para>
    </sect3>

    <sect3 id="m68k-boot-bvme6000"><title>BVME6000</title>
<para>

Der Installer für BVME6000 kann entweder von CD-ROM
(siehe <xref linkend="m68k-boot-cdrom"/>), von Floppy
(siehe <xref linkend="boot-from-floppies"/>) oder per Netzwerk
(siehe <xref linkend="boot-tftp"/>) gestartet werden.

</para>
    </sect3>

    <sect3 id="m68k-boot-mac"><title>Macintosh</title>
<para>

Die einzige Installationsmethode für Mac-Systeme ist die mittels
Festplatte (siehe auch <xref linkend="m68k-boot-hd"/>).
<emphasis>Mit anderen Worten: die CD-ROM ist hier nicht bootfähig.</emphasis>
Für Mac gibt es keinen funktionierenden 2.4.x-Kernel.

</para><para>

Wenn Ihre Hardware einen 53c9x-basierten SCSI-Bus benutzt, müssen
Sie möglicherweise den Kernelparameter <userinput>mac53c9x=1,0</userinput>
anhängen. Systeme mit zwei solchen SCSI-Bus-Systemen, wie der Quadra 950,
benötigen stattdessen das Argument <userinput>mac53c9x=2,0</userinput>.
Alternativ kann der Parameter auch als <userinput>mac53c9x=-1,0</userinput>
angeben werden; dies läßt die Autoerkennung eingeschaltet, jedoch sind
keine SCSI-Abschaltungen möglich. Beachten Sie, dass es nur nötig ist,
diesen Parameter zu verwenden, falls Sie mehr als eine Festplatte haben;
andernfalls wird das System schneller laufen, wenn Sie ihn nicht verwenden.

</para>
    </sect3>

    <sect3 id="m68k-boot-mvme"><title>MVME147 und MVME16x</title>
<para>

Der Installer für MVME147 und MVME16x kann entweder von
Floppy (siehe <xref linkend="boot-from-floppies"/>)
oder per Netzwerk (siehe <xref linkend="boot-tftp"/>) gestartet werden.
<emphasis>Mit anderen Worten: die CD-ROM ist hier nicht bootfähig.</emphasis>

</para>
    </sect3>

    <sect3 id="m68k-boot-q40"><title>Q40/Q60</title>
<para>

Die einzige Installationsmethode für Q40/Q60-Systeme ist die mittels
Festplatte (siehe auch <xref linkend="m68k-boot-hd"/>).
<emphasis>Mit anderen Worten: die CD-ROM ist hier nicht bootfähig.</emphasis>

</para>
    </sect3>

  </sect2>

  <sect2 arch="m68k" id="m68k-boot-hd"><title>Von Festplatte booten</title>

&boot-installer-intro-hd.xml;

<para>

Es können mindestens sechs verschiedene RAM-Disks verwendet werden, um
von Festplatte zu starten, drei verschiedene Typen jeweils mit und ohne
Support für 2.2.x-Linux-Kernel
(das <ulink url="&disturl;/main/installer-&architecture;/current/images/MANIFEST">MANIFEST</ulink> 
enthält weitere Details).

</para><para>

Die drei verschiedenen RAM-Disk-Typen sind <filename>cdrom</filename>,
<filename>hd-media</filename> und <filename>nativehd</filename>.
Sie unterscheiden sich nur darin, woher Sie die zur Installation
nötige Pakete beziehen. Die <filename>cdrom</filename>-RAM-Disk
nutzt eine CD-ROM, um Debian-Installer-Pakete zu bekommen. Die
<filename>hd-media</filename>-RAM-Disk benutzt die iso-Image-Datei einer
CD-ROM, die auf der Festplatte liegt. Die <filename>nativehd</filename>-RAM-Disk
schließlich bekommt Installationspakete über das Netzwerk.

</para>

<itemizedlist>
       <listitem><para><xref linkend="m68k-boothd-amiga"/></para></listitem>
       <listitem><para><xref linkend="m68k-boothd-atari"/></para></listitem>
       <listitem><para><xref linkend="m68k-boothd-mac"/></para></listitem>
       <listitem><para><xref linkend="m68k-boothd-q40"/></para></listitem>
</itemizedlist>


    <sect3 id="m68k-boothd-amiga"><title>Von AmigaOS aus booten</title>
<para>

Starten Sie die Linux-Installation, indem Sie im <command>Workbench</command>
auf das <guiicon>StartInstall</guiicon>-Icon im
<filename>debian</filename>-Verzeichnis doppelklicken.

</para><para>

Sie müssen unter Umständen zweimal &enterkey; drücken, nachdem das
Amiga-Installer-Programm einige Debug-Informationen in einem
Fenster angezeigt hat. Danach wird der Bildschirm grau, es folgen
ein paar Sekunden Verzögerung, bis ein schwarzer Bildschirm mit
weißer Schrift erscheint, der alle möglichen Kernel-Debug-Informationen
enthält. Diese Meldungen huschen vielleicht so schnell vorbei, dass Sie
sie nicht lesen können, aber das macht nichts. Nach ein paar Sekunden
sollte automatisch das Installationsprogramm starten, so dass Sie
zum Kapitel <xref linkend="d-i-intro"/> wechseln können.

</para>
    </sect3>


    <sect3 id="m68k-boothd-atari"><title>Von Atari TOS aus booten</title>
<para>

Starten Sie die Linux-Installation, indem Sie auf der GEM-Arbeitsoberfläche
auf das <guiicon>bootstra.prg</guiicon>-Icon im
<filename>debian</filename>-Verzeichnis doppelklicken. In der Options-Dialogbox
des Programms klicken Sie auf <guibutton>Ok</guibutton>.

</para><para>

Sie müssen unter Umständen &enterkey; drücken, nachdem der
Atari-Bootstrap einige Debug-Informationen in einem
Fenster angezeigt hat. Danach wird der Bildschirm grau, es folgen
ein paar Sekunden Verzögerung, bis ein schwarzer Bildschirm mit
weißer Schrift erscheint, der alle möglichen Kernel-Debug-Informationen
enthält. Diese Meldungen huschen vielleicht so schnell vorbei, dass Sie
sie nicht lesen können, aber das macht nichts. Nach ein paar Sekunden
sollte automatisch das Installationsprogramm starten, so dass Sie
zum Kapitel <xref linkend="d-i-intro"/> wechseln können.

</para>
    </sect3>


    <sect3 id="m68k-boothd-mac"><title>Von MacOS aus booten</title>
<para>

Sie müssen das originale Mac-Betriebssystem erhalten und von dort aus booten.
Es ist <emphasis>unbedingt notwendig</emphasis>, dass Sie die
<keycap>Shift</keycap>-Taste drücken, während Sie zur Vorbereitung des
Linux-Starts das MacOS-System booten; hierdurch wird vermieden,
dass MacOS zusätzliche Erweiterungen lädt. Wenn Sie MacOS nur benutzen,
um Linux zu starten, können Sie das Gleiche erreichen, indem Sie alle
Erweiterungen und Kontrollpanels aus dem Mac-System-Ordner löschen.
Ansonsten könnten Erweiterungen gestartet werden und verschiedene Probleme
mit dem laufenden Linux-Kernel verursachen.

</para><para>

Macs erfordern den <command>Penguin</command>-Bootloader, den Sie von
der Seite des <ulink url="&url-m68k-mac;">Linux/mac68k
Sourceforge.net - Projekts</ulink> herunterladen können.
Wenn Sie nicht die erforderlichen Programme haben, mit einem
<command>Stuffit</command>-Archiv umzugehen, können Sie es auch auf
eine MacOS-formatierte Floppy bekommen, indem Sie einen zweiten
GNU/Linux-Rechner jeglicher Architektur benutzen, der
<command>hmount</command>,
<command>hcopy</command> und <command>humount</command> aus der
<classname>hfsutils</classname>-Suite enthält.

</para><para>

Starten Sie die Linux-Installation, indem Sie auf dem MacOS-Desktop
auf das <guiicon>Penguin Prefs</guiicon>-Icon im
<filename>Penguin</filename>-Verzeichnis doppelklicken. Der
<command>Penguin</command>-Booter startet. Wählen Sie
<guimenuitem>Settings</guimenuitem> im <guimenu>File</guimenu>-Menü,
klicken Sie dann auf den Reiter <guilabel>Kernel</guilabel>. In der
rechten oberen Ecke gibt es Knöpfe für <quote>Kernel</quote> und
<quote>RAM-Disk</quote>. Wählen
Sie dort über den Dateiauswahl-Dialog für den Kernel <filename>vmlinuz</filename>
und für die RAM-Disk <filename>initrd.gz</filename> (jeweils aus dem
Verzeichnis <filename>install</filename>) aus.

</para><para>

Um Boot-Parameter in Penguin einzustellen, wählen Sie
<guimenu>File</guimenu> -&gt; <guimenuitem>Settings...</guimenuitem>,
klicken Sie dann auf den Reiter <guilabel>Options</guilabel>. Boot-Parameter
können in die Textbox eingetragen werden. Wenn Sie die eingetragenen
Einstellungen immer benutzen wollen, wählen Sie
<guimenu>File</guimenu> -&gt; <guimenuitem>Save Settings as
Default</guimenuitem>.

</para><para>

Schließen Sie den <guilabel>Settings</guilabel>-Dialog, speichern Sie
die Einstellungen und starten Sie den Bootstrap mit dem Eintrag
<guimenuitem>Boot Now</guimenuitem> im <guimenu>File</guimenu>-Menü.

</para><para>

Der <command>Penguin</command>-Booter gibt in einem Fenster einige
Debug-Meldungen aus. Danach wird der Bildschirm grau, es folgen
ein paar Sekunden Verzögerung, bis ein schwarzer Bildschirm mit
weißer Schrift erscheint, der alle möglichen Kernel-Debug-Informationen
enthält. Diese Meldungen huschen vielleicht so schnell vorbei, dass Sie
sie nicht lesen können, aber das macht nichts. Nach ein paar Sekunden
sollte automatisch das Installationsprogramm starten, so dass Sie
zum Kapitel <xref linkend="d-i-intro"/> wechseln können.

</para>

    </sect3>

    <sect3 id="m68k-boothd-q40"><title>Booten von Q40/Q60</title>

<para>

FIXME

</para><para>

Das Installationsprogramm sollte automatisch starten, so dass Sie
zum Kapitel <xref linkend="d-i-intro"/> wechseln können.

</para>

    </sect3>
  </sect2>

  <sect2 arch="m68k" id="m68k-boot-cdrom"><title>Booten von CD-ROM</title>
<para>

Im Moment ist BVME6000 die einzige &arch-title;-Subarchitektur, die das
Booten von CD-ROM unterstützt.

</para>

&boot-installer-intro-cd.xml;

  </sect2>

  <sect2 arch="m68k" id="boot-tftp"><title>Booten per TFTP</title>

&boot-installer-intro-net.xml;

<para>

Nach dem Starten des VMEbus-Systems wird der
LILO-<prompt>Boot:</prompt>-Prompt angezeigt. Geben Sie hier einen
Eintrag aus der folgenden Liste ein, um Linux zu booten und die
Debian-Installation zu starten, wobei eine vt102-Terminal-Emulation
benutzt wird.

<!-- Because the &enterkey; definition uses <keycap>,    -->
<!-- we use <screen> instead of <userinput> in this list -->

<itemizedlist>
<listitem><para>

Geben Sie <screen>i6000 &enterkey;</screen> ein,
um eine BVME4000/6000-Maschine zu installieren.

</para></listitem>
<listitem><para>

Geben Sie <screen>i162 &enterkey;</screen> ein,
um eine MVME162-Maschine zu installieren.

</para></listitem>
<listitem><para>

Geben Sie <screen>i167 &enterkey;</screen> ein,
um eine MVME166/167-Maschine zu installieren.

</para></listitem>
    </itemizedlist>

</para><para>

Sie können evtl. zusätzlich <screen>TERM=vt100</screen> angeben,
um die vt100-Terminal-Emulation zu nutzen,
also z.b. <screen>i6000 TERM=vt100 &enterkey;</screen>.

</para>
  </sect2>

  <sect2 arch="m68k" id="boot-from-floppies">
  <title>Booten von Floppy</title>
<para>

Für die meisten &arch-title;-Unterarchitekturen ist das Booten von einem
lokalen Dateisystem die empfohlene Methode.

</para><para>

Das Starten von einer Boot-Floppy wird zurzeit nur auf Atari und VME
(auf VME mit einem SCSI-Floppy-Laufwerk) unterstützt.

</para>
 </sect2>

