<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 36639 -->

  <sect2 arch="arm" id="boot-tftp"><title>Arrancar desde TFTP</title>

&boot-installer-intro-net.xml;

  <sect3 arch="arm"><title>Arrancar desde TFTP en NetWinder</title>

<para>

Las m�quinas NetWinder tienen dos interfaces de red: una tarjeta de
10 Mbps compatible con NE2000 (que habitualmente se la refiere como
<filename>eth0</filename>) y una tarjeta
Tulip de 100 Mbps. Si utiliza la tarjeta de 100Mbps puede tener problemas
durante la descarga de la imagen a trav�s de TFTP por lo que se recomienda el
uso de la interfaz de 10Mbps (el marcado como <literal>10 Base-T</literal>).

</para>
<note><para>

Necesita NeTTrom 2.2.1 o superior para arrancar el sistema de
instalaci�n. Se recomienda la versi�n 2.3.3. Desgraciadamente,
los archivos de �firmware� no se pueden descargar actualmente debido
a problemas de licencia. Podr� encontrar nuevas im�genes en <ulink
url="http//www.netwinder.org/"></ulink> si esta situaci�n cambia en el futuro.

</para></note>
<para>

Cuando arranque Netboot debe interrumpir el proceso de arranque durante la
cuenta hacia atr�s. Esto le permite establecer algunos valores del
�firmware� necesarios para arrancar el instalador. En primer lugar,
deber�a cargar los valores por omisi�n:

<informalexample><screen>
   NeTTrom command-&gt; load-defaults
</screen></informalexample>

Despu�s, puede configurar la red asignando una direcci�n est�tica:

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 flash
    NeTTrom command-&gt; setenv eth0_ip 192.168.0.10/24
</screen></informalexample>

donde 24 es el n�mero de conjunto de bits en la m�scara de red. O bien,
puede utilizar una direcci�n din�mica:

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 dhcp
</screen></informalexample>

Tambi�n podr�a necesitar configurar los valores de
<userinput>route1</userinput> en el servidor TFTP, si es que no est�
en la misma subred. 

Si sigue los pasos de configuraci�n indicados m�s abajo tendr� que
modificar la direcci�n del servidor de TFTP y la ubicaci�n
de la imagen. Una vez hecho esto deber� guardar sus cambios en la flash.

<informalexample><screen>
    NeTTrom command-&gt; setenv kerntftpserver 192.168.0.1
    NeTTrom command-&gt; setenv kerntftpfile boot.img
    NeTTrom command-&gt; save-all
    NeTTrom command-&gt; setenv netconfig_eth0 flash
</screen></informalexample>

Tras esto deber� decir al �firmware� que deber�a arrancar la imagen TFTP:

<informalexample><screen>
    NeTTrom command-&gt; setenv kernconfig tftp
    NeTTrom command-&gt; setenv rootdev /dev/ram
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram
</screen></informalexample>

Tambi�n necesitar establecer el valor mostrado a continuaci�n si
quiere utilizar la consola serie para instalar su Netwinder:

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram console=ttyS0,115200
</screen></informalexample>

En lugar de la consola puede utilizar un teclado o monitor para la instalaci�n
para lo que debe configurar:

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram
</screen></informalexample>

Puede utilizar la orden <command>printenv</command> para revisar los valores de
su entorno. Puede cargar la imagen una vez haya verificado que son correctos:

<informalexample><screen>
    NeTTrom command-&gt; boot
</screen></informalexample>

Si tiene problemas le recomendamos que consulte el 
<ulink url="http://www.netwinder.org/howto/Firmware-HOWTO.html">COMO
detallado</ulink> disponible.

</para>
  </sect3>

  <sect3 arch="arm"><title>Arrancar desde TFTP en CATS</title>

<para>

En m�quinas CATS, use <command>boot de0:</command> o similar en el
cursor Cyclone.

</para>
   </sect3>
  </sect2>


  <sect2 arch="arm"><title>Arrancar desde CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

Para arrancar desde CD-ROM desde el cursor de la consola Cyclone,
use la orden <command>boot cd0:cats.bin</command>

</para>
  </sect2>


  <sect2 arch="arm" id="boot-firmware"><title>Arranque desde Firmware</title>

&boot-installer-intro-firmware.xml;

   <sect3 arch="arm" id="boot-firmware-nslu2"><title>Arranque del NSLU2</title>
<para>

Hay tres formas distintas de poner el �firmware� del instalador en flash:

</para>

    <sect4 arch="arm"><title>Con la interfaz web del NSLU2 </title>
<para>

Vaya a la secci�n de administraci�n de la interfaz y elija el elemento
del men� <literal>Upgrade</literal> (�Actualizaci�n�, N. del T.).
Hecho esto podr� buscar en su disco la imagen del instalador que ha
descargado previamente. Hecho esto, pulse el bot�n
<literal>Start Upgrade</literal> (�Comenzar la actualizaci�n�, N. del T.),
confirme la operaci�n, espere unos minutos y vuelva a confirmar. El
sistema deber�a arrancar directamente el instalador despu�s.

</para>
    </sect4>

    <sect4 arch="arm"><title>A trav�s de una red con Linux/Unix</title>
<para>

Puede usar <command>upslug2</command> desde cualquier sistema Linux o
Unix para actualizar el sistema por la red. Este programa est� empaquetado
para Debian.

En primer lugar tiene que poner el sistema NSLU2 en modo actualizaci�n:

<orderedlist>
<listitem><para>

Desconecte cualquier disco y/o dispositivo de los puertos USB.

</para></listitem>
<listitem><para>

Apague el NSLU2.

</para></listitem>
<listitem><para>

Pulse y mantenga apretado el bot�n reset (accesible a trav�s de un peque�o agujero en la parte trasera del equipo encima de la entrada de corriente).

</para></listitem>
<listitem><para>

Pulse y mantenga el bot�n de encendido para arrancar el NSLU2.

</para></listitem>
<listitem><para>

Espere diez segundos y mire el LED de preparado/estado. Una vez transcurridos
diez segundos deber�a cambiar de �mbar a rojo. Suelte el bot�n de reset
inmediatamente. 

</para></listitem>
<listitem><para>

El LED de preparado/estado del NSLU2 empezara a parpadear de forma alternativa
entre rojo y verde (hay una demora de un segundo antes de que aparezca el verde
la primera vez). Cuando suceda esto el NSLU2 estar� ya en modo actualizaci�n.

</para></listitem>
</orderedlist>

Si tiene problemas con estos pasos consulte las <ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">p�ginas NSLU2-Linux
</ulink>.

Una vez su NSLU2 est� en modo actualizaci�n puede guardar en la flash 
la nueva imagen:

<informalexample><screen>
sudo upslug2 -i di-nslu2.bin
</screen></informalexample>

Tenga en cuenta que esta herramienta muestra la direcci�n MAC de su NSLU2.
Este dato le puede resultar �til para configurar su servidor de DHCP.  Una vez
se haya escrito y verificado la imagen completa el sistema reiniciar� de forma
autom�tica. Aseg�rese de que vuelve a conectar su disco USB, si no lo hace el
instalador no lo podr� encontrar m�s adelante.

</para>
    </sect4>

    <sect4 arch="arm"><title>A trav�s de la red con Windows</title>
<para>

Existe una <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">herramienta</ulink>
para Windows para actualizar el firmware a trav�s de la red.

</para>
    </sect4>
   </sect3>
  </sect2>
