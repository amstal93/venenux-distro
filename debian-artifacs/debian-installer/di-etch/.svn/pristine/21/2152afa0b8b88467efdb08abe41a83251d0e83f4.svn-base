<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 33725-->

   <sect3 arch="powerpc">
   <title>&arch-title; での USB メモリのパーティション分割</title>
<para>

<!--
Most USB sticks do not come pre-configured in such a way that Open
Firmware can boot from them, so you will need to repartition the stick.
On Mac systems, run <userinput>mac-fdisk /dev/sda</userinput>,
initialise a new partition map using the <userinput>i</userinput>
command, and create a new partition of type Apple_Bootstrap using the
<userinput>C</userinput> command. (Note that the first "partition" will
always be the partition map itself.) Then type
-->
ほとんどの USB メモリは、
Open Firmware で起動できるようにあらかじめ設定されていません。
Mac システムでは、<userinput>mac-fdisk /dev/sda</userinput> を起動して、
<userinput>i</userinput> コマンドで新規パーティションマップを初期化、
<userinput>C</userinput> コマンドで Apple_Bootstrap タイプの
パーティションを作成してください。
(注: 先頭のパーティションはいつもパーティションマップそのものになっています)
その後、以下を入力してください。

<informalexample><screen>
$ hformat /dev/<replaceable>sda2</replaceable>
</screen></informalexample>

<!--
Take care that you use the correct device name for your USB stick. The
<command>hformat</command> command is contained in the
<classname>hfsutils</classname> Debian package.
-->
USB メモリの正確なデバイス名を使用する事に注意してください。
<command>hformat</command> は、
<classname>hfsutils</classname> Debian パッケージに含まれています。

</para><para>

<!--
In order to start the kernel after booting from the USB stick, we will
put a boot loader on the stick. The <command>yaboot</command> boot
loader can be installed on an HFS filesystem and can be reconfigured by
just editing a text file. Any operating system which supports the HFS
file system can be used to make changes to the configuration of the boot
loader.
-->
USB メモリから起動してカーネルをスタートするには、
USB メモリにブートローダを配置します。
<command>yaboot</command> ブートローダは、
HFS ファイルシステムにインストールでき、
テキストファイルを編集するだけで再設定できます。
HFS ファイルシステムをサポートする OS であれば、
ブートローダの設定を変更できます。

</para><para>

<!--
The normal <command>ybin</command> tool that comes with
<command>yaboot</command> does not yet understand USB storage devices,
so you will have to install <command>yaboot</command> by hand using the
<classname>hfsutils</classname> tools. Type
-->
<command>yaboot</command> とともにある通常の <command>ybin</command> ツールは、
USB ストレージデバイスを認識しません。
そのため、<classname>hfsutils</classname> ツールを使って、
<command>yaboot</command>を手動でインストールしなければなりません。
以下を入力してください。

<informalexample><screen>
$ hmount /dev/sda2
$ hcopy -r /usr/lib/yaboot/yaboot :
$ hattrib -c UNIX -t tbxi :yaboot
$ hattrib -b :
$ humount
</screen></informalexample>

<!--
Again, take care that you use the correct device name. The partition
must not be otherwise mounted during this procedure. This procedure
writes the boot loader to the partition, and uses the HFS utilities to
mark it in such a way that Open Firmware will boot it. Having done this,
the rest of the USB stick may be prepared using the normal Unix
utilities.
-->
繰り返しますが、正確なデバイス名を使用するよう注意してください。
この手順中でパーティションをマウントする必要はありません。
この手順ではパーティションにブートローダを書き込み、
Open Firmware が起動できるように HFS ユーティリティを使って印を付けます。
これが終われば、
USB メモリに通常の Unix ユーティリティを使う準備ができたことになります。

</para><para>

<!--
Mount the partition (<userinput>mount /dev/sda2 /mnt</userinput>) and
copy the following files from the Debian archives to the stick:
-->
このパーティションをマウントし (<userinput>mount /dev/sda2 /mnt</userinput>)、
以下のファイルを Debian アーカイブから USB メモリへコピーしてください。

<itemizedlist>
<listitem><para>

<filename>vmlinux</filename> (カーネルバイナリ)

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename> (初期 RAM ディスクイメージ)

</para></listitem>
<listitem><para>

<filename>yaboot.conf</filename> (yaboot 設定ファイル)

</para></listitem>
<listitem><para>

<filename>boot.msg</filename> (追加起動メッセージ)

</para></listitem>
<listitem><para>

<!--
Optional kernel modules
-->
追加カーネルモジュール

</para></listitem>
</itemizedlist>

</para><para>

<!--
The <filename>yaboot.conf</filename> configuration file should
contain the following lines:
-->
<filename>yaboot.conf</filename> 設定ファイルには、
以下の行を含まなければなりません。

<informalexample><screen>
default=install
root=/dev/ram

message=/boot.msg

image=/vmlinux
        label=install
        initrd=/initrd.gz
        initrd-size=10000
        read-only
</screen></informalexample>

<!--
Please note that the <userinput>initrd-size</userinput> parameter
may need to be increased, depending on the image you are booting.
-->
起動するイメージに応じて、<userinput>initrd-size</userinput> パラメータを、
増やす必要があることに注意してください。

</para>
   </sect3>
