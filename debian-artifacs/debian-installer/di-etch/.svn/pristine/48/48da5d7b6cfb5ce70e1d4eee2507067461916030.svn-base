<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43558 -->


  <sect2 condition="supports-bootp" id="tftp-bootp">
<!--
  <title>Setting up a BOOTP server</title>
-->
  <title>BOOTP サーバの設定</title>
<para>

<!--
There are two BOOTP servers available for GNU/Linux. The first is CMU
<command>bootpd</command>. The other is actually a DHCP server: ISC
<command>dhcpd</command>. In &debian; these are contained in the
<classname>bootp</classname> and <classname>dhcp3-server</classname>
packages respectively.
-->
GNU/Linux で使える BOOTP サーバは 2 つあります。
ひとつは CMU の <command>bootpd</command> です。
もう 1 つは実際は DHCP サーバですが、ISC の <command>dhcpd</command> です。
&debian; では、
<classname>bootp</classname> パッケージと <classname>dhcp3-server</classname>
パッケージにそれぞれ入っています。

</para><para>

<!--
To use CMU <command>bootpd</command>, you must first uncomment (or
add) the relevant line in <filename>/etc/inetd.conf</filename>.  On
&debian;, you can run <userinput>update-inetd -\-enable
bootps</userinput>, then <userinput>/etc/init.d/inetd
reload</userinput> to do so. Just in case your BOOTP server does not
run Debian, the line in question should look like:
-->
CMU <command>bootpd</command> を使う場合は、まず <filename>/etc/inetd.conf</filename> ファイルの
該当行をアンコメント (または追加) する必要があります。
&debian; では <userinput>update-inetd --enable bootps</userinput> を実行し、
続いて <userinput>/etc/init.d/inetd reload</userinput> とすれば OK です。
BOOTP サーバが Debian で動かない場合は、以下のようにします。

<informalexample><screen>
bootps  dgram  udp  wait  root  /usr/sbin/bootpd  bootpd -i -t 120
</screen></informalexample>

<!--
Now, you must create an <filename>/etc/bootptab</filename> file.  This
has the same sort of familiar and cryptic format as the good old BSD
<filename>printcap</filename>, <filename>termcap</filename>, and
<filename>disktab</filename> files.  See the
<filename>bootptab</filename> manual page for more information.  For
CMU <command>bootpd</command>, you will need to know the hardware
(MAC) address of the client.  Here is an example
<filename>/etc/bootptab</filename>:
-->
ここで次に <filename>/etc/bootptab</filename> を作成します。
このファイルの書式は、
<filename>printcap</filename>, <filename>termcap</filename>, 
<filename>disktab</filename> ファイルなどでお馴染みの、
例のわかりにくい形式になっています。
詳細は <filename>bootptab</filename> マニュアルページを見てください。
CMU <command>bootpd</command> では、
クライアントのハードウェア (MAC) アドレスを知っておかなければなりません。
<filename>/etc/bootptab</filename> の例を示します。

<informalexample><screen>
client:\
  hd=/tftpboot:\
  bf=tftpboot.img:\
  ip=192.168.1.90:\
  sm=255.255.255.0:\
  sa=192.168.1.1:\
  ha=0123456789AB:
</screen></informalexample>

<!--
You will need to change at least the <quote>ha</quote> option, which
specifies the hardware address of the client.  The <quote>bf</quote>
option specifies the file a client should retrieve via TFTP; see
<xref linkend="tftp-images"/> for more details.
-->
少なくともクライアントのハードウェアアドレスを指定している
<quote>ha</quote> オプションは変更する必要があるでしょう。
<quote>bf</quote> オプションはクライアントが TFTP で取得するファイルを指定しています。
詳細は <xref linkend="tftp-images"/> を参照してください。

<phrase arch="mips">
<!--
On SGI machines you can just enter the command monitor and type
<userinput>printenv</userinput>.  The value of the
<userinput>eaddr</userinput> variable is the machine's MAC address.
-->
SGI のマシンでは、
コマンドモニタから <userinput>printenv</userinput> と入力してください。
<userinput>eaddr</userinput> 変数がマシンの MAC アドレスです。
</phrase>

</para><para>

<!--
By contrast, setting up BOOTP with ISC <command>dhcpd</command> is
really easy, because it treats BOOTP clients as a moderately special
case of DHCP clients.  Some architectures require a complex
configuration for booting clients via BOOTP.  If yours is one of
those, read the section <xref linkend="dhcpd"/>.  In that case, you
will probably be able to get away with simply adding the
<userinput>allow bootp</userinput> directive to the configuration
block for the subnet containing the client, and restart
<command>dhcpd</command> with <userinput>/etc/init.d/dhcpd3-server
restart</userinput>.
-->
対照的に、ISC <command>dhcpd</command> を使っての BOOTP の設定は実に簡単です。
<command>dhcpd</command> では、BOOTP クライアントは
やや特殊な DHCP クライアントとして取り扱われます。
アーキテクチャによっては、BOOTP によるクライアントの起動には
複雑な設定が必要になります。
これに該当してしまったら、<xref linkend="dhcpd"/> の節を読んでください。
この場合、クライアントの含まれるサブネットの設定ブロックに
<userinput>allow bootp</userinput> というディレクティブを追加し、
<userinput>/etc/init.d/dhcpd3-server restart</userinput> で
<command>dhcpd</command> を再起動するだけです。

</para>
  </sect2>
