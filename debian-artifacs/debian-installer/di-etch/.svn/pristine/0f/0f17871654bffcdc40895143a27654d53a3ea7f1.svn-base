<!-- retain these comments for translator revision tracking -->
<!-- original version: 43528 -->


  <sect2 arch="sparc" id="sparc-cpus">
  <!-- <title>CPU and Main Boards Support</title> -->
  <title>CPU e schede madri supportate</title>
<para>

<!--
Sparc-based hardware is divided into a number of different subarchitectures,
identified by one of the following names: sun4, sun4c, sun4d, sun4m, sun4u
or sun4v. The following list describes what machines they include and what
level of support may be expected for each of them.
-->

L'hardware Sparc si divide in un certo numero di sottoarchitetture differenti
identificate da uno dei seguenti nomi: sun4, sun4c, sun4d, sun4m, sun4u o
sun4v. L'elenco che segue indica quali macchine sono comprese in ciascuna
sottoarchitettura e qual è il livello del supporto disponibile.

</para>

<variablelist>
<varlistentry>
<term>sun4, sun4c, sun4d</term>

<listitem><para>

<!--
These subarchitectures include some very old 32-bit machines, which are
no longer supported. For a complete list please consult the
<ulink url="http://en.wikipedia.org/wiki/SPARCstation">Wikipedia
SPARCstation page</ulink>.
-->

Queste sottoarchitetture includono macchine a 32&nbsp;bit molto vecchie che
non sono più supportate. L'elenco completo è disponibile nella pagina di
<ulink url="http://en.wikipedia.org/wiki/SPARCstation">Wikipedia per
SPARCstation</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4m</term>

<listitem><para>

<!--
sun4m is the only 32-bit subarchitecture (sparc32) that is currently
supported. The most popular machines belonging to this class are
Sparcstation 4, 5, 10 and 20.
-->

sun4m è l'unica sottoarchitettura a 32&nbsp;bit (sparc32) attualmente
supportata. Le macchine più popolari che appartengono a questa classe
sono le Sparcstation 4, 5, 10 e 20.

</para><para>

<!--
Note that symmetric multiprocessing (SMP) &mdash; the ability to run
processes on multiple processors &mdash; is not supported on this hardware,
due to stability problems with such configurations. The available
uniprocessor (UP) sparc32 kernel will boot fine on multiprocessor
machines, although it will activate and use only the first CPU.
-->

Notare il supporto multiprocessore o SMP, cioè la capacità di eseguire
processi su più processori, non è disponibile su questo hardware a causa
di problemi di stabilità in alcune configurazioni. Il kernel per sparc32 è
stato compilato solo per la modalità uniprocessore (UP); questo kernel
funziona correttamente anche su macchine multiprocessore, ma è in grado di
attivare e usare solo il primo processore.
 
</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4u</term>

<listitem><para>

<!--
This subarchitecture includes all 64-bit machines (sparc64) based on
the UltraSparc processor and its clones. Most of the machines are well
supported, even though for some you may experience problems booting from
CD due to firmware or bootloader bugs (this problem may be worked around
by using netbooting). Use the sparc64 or sparc64-smp kernel in UP and SMP
configurations respectively.
-->

Questa sottoarchitettura comprende tutte le macchine a 64&nbsp;bit (sparc64)
basate sul processore UltraSparc e i suoi cloni. La maggior parte delle
macchine sono supportate anche se si potrebbero verificare dei problemi con
l'avvio da CD dovuti a bug nel firmware o nel bootloader (questi problemi
possono essere aggirati usando l'avvio dalla rete). Usare il kernel sparc64
su macchine UP e sparc64-smp su macchine SMP.

</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4v</term>

<listitem><para>

<!--
This is the newest addition to the Sparc family, which includes machines
based on the Niagara multi-core CPUs. At the moment such CPUs are only
available in T1000 and T2000 servers by Sun, and are well supported. Use
the sparc64-smp kernel.
-->

Questa è l'ultima arrivata nella famiglia Sparc, include le macchine basate
sulle CPU multi-core Niagara. Attualmente queste CPU sono montate solo nei
server Sun T1000 e T2000 e sono ben supportate. Con queste macchine usare
il kernel sparc64-smp.

</para></listitem>
</varlistentry>
</variablelist>

<para>

<!--
Note that Fujitsu's SPARC64 CPUs used in PRIMEPOWER family of servers are not
supported due to lack of support in the Linux kernel.
-->

Notare che le CPU SPARC64 prodotte da Fujitsu e usate nei server della
famiglia PRIMEPOWER non sono supportate dato che manca il supporto per
tali CPU nel kernel Linux.

</para>
  </sect2>
