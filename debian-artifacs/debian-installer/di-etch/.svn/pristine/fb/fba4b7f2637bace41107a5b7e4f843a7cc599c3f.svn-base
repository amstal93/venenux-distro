<!-- retain these comments for translator revision tracking -->
<!-- $Id: parameters.xml 45239 2007-02-18 19:38:12Z fjp $ -->

 <sect1 id="boot-parms"><title>Boot Parameters</title>
<para>

Boot parameters are Linux kernel parameters which are generally used
to make sure that peripherals are dealt with properly.  For the most
part, the kernel can auto-detect information about your peripherals.
However, in some cases you'll have to help the kernel a bit.

</para><para>

If this is the first time you're booting the system, try the default
boot parameters (i.e., don't try setting parameters) and see if it works
correctly. It probably will.  If not, you can reboot later and look for
any special parameters that inform the system about your hardware.

</para><para>

Information on many boot parameters can be found in the
<ulink url="http://www.tldp.org/HOWTO/BootPrompt-HOWTO.html"> Linux
BootPrompt HOWTO</ulink>, including tips for obscure hardware.  This
section contains only a sketch of the most salient parameters.  Some
common gotchas are included below in
<xref linkend="boot-troubleshooting"/>.

</para><para>

When the kernel boots, a message

<informalexample><screen>
Memory:<replaceable>avail</replaceable>k/<replaceable>total</replaceable>k available
</screen></informalexample>

should be emitted early in the process.
<replaceable>total</replaceable> should match the total amount of RAM,
in kilobytes.  If this doesn't match the actual amount of RAM you have
installed, you need to use the
<userinput>mem=<replaceable>ram</replaceable></userinput> parameter,
where <replaceable>ram</replaceable> is set to the amount of memory,
suffixed with <quote>k</quote> for kilobytes, or <quote>m</quote> for
megabytes.  For example, both <userinput>mem=65536k</userinput> and
<userinput>mem=64m</userinput> mean 64MB of RAM.

</para><para condition="supports-serial-console">

If you are booting with a serial console, generally the kernel will
autodetect
this<phrase arch="mipsel"> (although not on DECstations)</phrase>.
If you have a videocard (framebuffer) and a keyboard also attached to
the computer which you wish to boot via serial console, you may have
to pass the
<userinput>console=<replaceable>device</replaceable></userinput>
argument to the kernel, where <replaceable>device</replaceable> is
your serial device, which is usually something like
<filename>ttyS0</filename>.

</para><para arch="sparc">

For &arch-title; the serial devices are <filename>ttya</filename> or
<filename>ttyb</filename>.
Alternatively, set the <envar>input-device</envar> and
<envar>output-device</envar> OpenPROM variables to
<filename>ttya</filename>.

</para>


  <sect2 id="installer-args"><title>Debian Installer Parameters</title>
<para>

The installation system recognizes a few additional boot parameters<footnote>

<para>

With current kernels (2.6.9 or newer) you can use 32 command line options and
32 environment options. If these numbers are exceeded, the kernel will panic.

</para>

</footnote> which may be useful.

</para><para>

A number of parameters have a <quote>short form</quote> that helps avoid
the limitations of the kernel command line options and makes entering the
parameters easier. If a parameter has a short form, it will be listed in
brackets behind the (normal) long form. Examples in this manual will
normally use the short form too.

</para>

<variablelist>
<varlistentry>
<term>debconf/priority (priority)</term>
<listitem><para>

This parameter sets the lowest priority of messages to be displayed.

</para><para>

The default installation uses <userinput>priority=high</userinput>.
This means that both high and critical priority messages are shown, but medium
and low priority messages are skipped.
If problems are encountered, the installer adjusts the priority as needed.

</para><para>

If you add <userinput>priority=medium</userinput> as boot parameter, you
will be shown the installation menu and gain more control over the installation.
When <userinput>priority=low</userinput> is used, all messages are shown
(this is equivalent to the <emphasis>expert</emphasis> boot method).
With <userinput>priority=critical</userinput>, the installation system
will display only critical messages and try to do the right thing without fuss.

</para></listitem>
</varlistentry>


<varlistentry>
<term>DEBIAN_FRONTEND</term>
<listitem><para>

This boot parameter controls the type of user interface used for the
installer. The current possible parameter settings are:

<itemizedlist>
<listitem>
<para><userinput>DEBIAN_FRONTEND=noninteractive</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=text</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=newt</userinput></para>
</listitem><listitem>
<para><userinput>DEBIAN_FRONTEND=gtk</userinput></para>
</listitem>
</itemizedlist>

The default frontend is <userinput>DEBIAN_FRONTEND=newt</userinput>.
<userinput>DEBIAN_FRONTEND=text</userinput> may be preferable for
serial console installs. Generally, only the
<userinput>newt</userinput> frontend is available on default install
media. On architectures that support it, the graphical installer uses
the <userinput>gtk</userinput> frontend.

</para></listitem>
</varlistentry>


<varlistentry>
<term>BOOT_DEBUG</term>
<listitem><para>

Setting this boot parameter to 2 will cause the installer's boot process
to be verbosely logged. Setting it to 3 makes debug shells
available at strategic points in the boot process. (Exit the shells to
continue the boot process.)

<variablelist>
<varlistentry>
<term><userinput>BOOT_DEBUG=0</userinput></term>
<listitem><para>This is the default.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=1</userinput></term>
<listitem><para>More verbose than usual.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=2</userinput></term>
<listitem><para>Lots of debugging information.</para></listitem>
</varlistentry>

<varlistentry>
<term><userinput>BOOT_DEBUG=3</userinput></term>
<listitem><para>

Shells are run at various points in the boot process to allow detailed
debugging. Exit the shell to continue the boot.

</para></listitem>
</varlistentry>
</variablelist>

</para></listitem>
</varlistentry>


<varlistentry>
<term>INSTALL_MEDIA_DEV</term>
<listitem><para>

The value of the parameter is the path to the device to load the
Debian installer from. For example,
<userinput>INSTALL_MEDIA_DEV=/dev/floppy/0</userinput>

</para><para>

The boot floppy, which normally scans all floppies it can to find the
root floppy, can be overridden by this parameter to only look at the
one device.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/framebuffer (fb)</term>
<listitem><para>

Some architectures use the kernel framebuffer to offer installation in
a number of languages. If framebuffer causes a problem on your system
you can disable the feature by the parameter
<userinput>fb=false</userinput>. Problem symptoms are error messages
about bterm or bogl, a blank screen, or a freeze within a few minutes after
starting the install.

</para><para arch="x86">

The <userinput>video=vga16:off</userinput> argument may also be used
to disable the kernel's use of the framebuffer. Such problems have been
reported on a Dell Inspiron with Mobile Radeon card.

</para><para arch="m68k">

Such problems have been reported on the Amiga 1200 and SE/30.

</para><para arch="hppa">

Such problems have been reported on hppa.

</para><note arch="sparc"><para>

Because of display problems on some systems, framebuffer support is
<emphasis>disabled by default</emphasis> for &arch-title;. This can result
in ugly display on systems that do properly support the framebuffer, like
those with ATI graphical cards.
If you see display problems in the installer, you can try booting with
parameter <userinput>debian-installer/framebuffer=true</userinput> or
<userinput>fb=true</userinput> for short.

</para></note></listitem>
</varlistentry>

<varlistentry arch="not-s390">
<term>debian-installer/theme (theme)</term>
<listitem><para>

A theme determines how the user interface of the installer looks (colors,
icons, etc.). What themes are available differs per frontend.  Currently
both the newt and gtk frontends only have a <quote>dark</quote> theme that was
designed for visually impaired users. Set the theme by booting with 
<userinput>theme=<replaceable>dark</replaceable></userinput>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/probe/usb</term>
<listitem><para>

Set to <userinput>false</userinput> to prevent probing for USB on
boot, if that causes problems.

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

By default, the &d-i; automatically probes for network configuration
via DHCP. If the probe succeeds, you won't have a chance to review and
change the obtained settings. You can get to the manual network setup
only in case the DHCP probe fails.

</para><para>

If you have a DHCP server on your local network, but want to avoid it
because e.g. it gives wrong answers, you can use the parameter
<userinput>netcfg/disable_dhcp=true</userinput> to prevent configuring
the network with DHCP and to enter the information manually.

</para></listitem>
</varlistentry>

<varlistentry>
<term>hw-detect/start_pcmcia</term>
<listitem><para>

Set to <userinput>false</userinput> to prevent starting PCMCIA
services, if that causes problems. Some laptops are well known for
this misbehavior.

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/url (url)</term>
<listitem><para>

Specify the url to a preconfiguration file to download and use for
automating the install. See <xref linkend="automatic-install"/>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/file (file)</term>
<listitem><para>

Specify the path to a preconfiguration file to load for
automating the install. See <xref linkend="automatic-install"/>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>preseed/interactive</term>
<listitem><para>

Set to <userinput>true</userinput> to display questions even if they have
been preseeded. Can be useful for testing or debugging a preconfiguration
file. Note that this will have no effect on parameters that are passed as
boot parameters, but for those a special syntax can be used.
See <xref linkend="preseed-seenflag"/> for details.

</para></listitem>
</varlistentry>

<varlistentry>
<term>auto-install/enable (auto)</term>
<listitem><para>

Delay questions that are normally asked before preseeding is possible until
after the network is configured.
See <xref linkend="preseed-auto"/> for details about using this to
automate installs.

</para></listitem>
</varlistentry>

<varlistentry>
<term>cdrom-detect/eject</term>
<listitem><para>

By default, before rebooting, &d-i; automatically ejects the optical
media used during the installation. This can be unnecessary if the system
does not automatically boot off the CD. In some cases it may even be
undesirable, for example if the optical drive cannot reinsert the media
itself and the user is not there to do it manually. Many slot loading,
slim-line, and caddy style drives cannot reload media automatically.

</para><para>

Set to <userinput>false</userinput> to disable automatic ejection, and
be aware that you may need to ensure that the system does not
automatically boot from the optical drive after the initial
installation.

</para></listitem>
</varlistentry>

<varlistentry>
<term>debian-installer/allow_unauthenticated</term>
<listitem><para>

By default the installer requires that repositories be authenticated
using a known gpg key. Set to <userinput>true</userinput> to 
disable that authentication. 
<emphasis role="bold">Warning: insecure, not recommended.</emphasis>

</para></listitem>
</varlistentry>

<varlistentry arch="alpha;m68k;mips;mipsel">
<term>ramdisk_size</term>
<listitem><para>

This parameter should already be set to a correct value where needed;
set it only it you see errors during the boot that indicate the ramdisk
could not be loaded completely. The value is in kB.

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>mouse/left</term>
<listitem><para>

For the gtk frontend (graphical installer), users can switch the mouse to
left-handed operation by setting this parameter to <userinput>true</userinput>.

</para></listitem>
</varlistentry>

<varlistentry condition="gtk">
<term>directfb/hw-accel</term>
<listitem><para>

For the gtk frontend (graphical installer), hardware acceleration in directfb
is disabled by default. To enable it, set this parameter to
<userinput>true</userinput> when booting the installer.

</para></listitem>
</varlistentry>

<varlistentry>
<term>rescue/enable</term>
<listitem><para>

Set to <userinput>true</userinput> to enter rescue mode rather than
performing a normal installation. See <xref linkend="rescue"/>.

</para></listitem>
</varlistentry>

</variablelist>

   <sect3 id="preseed-args">
   <title>Using boot parameters to answer questions</title>
<para>

With some exceptions, a value can be set at the boot prompt for any question
asked during the installation, though this is only really useful in specific
cases. General instructions how to do this can be found in
<xref linkend="preseed-bootparms"/>. Some specific examples are listed below.

</para>

<variablelist>

<varlistentry>
<term>debian-installer/locale (locale)</term>
<listitem><para>

Can be used to set both the language and country for the installation.
This will only work if the locale is supported in Debian.
For example, use <userinput>locale=de_CH</userinput> to select German as
language and Switzerland as country.

</para></listitem>
</varlistentry>

<varlistentry>
<term>anna/choose_modules (modules)</term>
<listitem><para>

Can be used to automatically load installer components that are not loaded
by default.
Examples of optional components that may be useful are
<classname>openssh-client-udeb</classname> (so you can use
<command>scp</command> during the installation)<phrase arch="not-s390"> and
<classname>ppp-udeb</classname> (see <xref linkend="pppoe"/>)</phrase>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>netcfg/disable_dhcp</term>
<listitem><para>

Set to <userinput>true</userinput> if you want to disable DHCP and instead
force static network configuration.

</para></listitem>
</varlistentry>

<varlistentry>
<term>mirror/protocol (protocol)</term>
<listitem><para>

By default the installer will use the http protocol to download files from
Debian mirrors and changing that to ftp is not possible during installations
at normal priority. By setting this parameter to <userinput>ftp</userinput>,
you can force the installer to use that protocol instead. Note that you
cannot select an ftp mirror from a list, you have to enter the hostname
manually.

</para></listitem>
</varlistentry>

<varlistentry>
<term>tasksel:tasksel/first (tasks)</term>
<listitem><para>

Can be used to select tasks that are not available from the interactive task
list, such as the <literal>kde-desktop</literal> task.
See <xref linkend="pkgsel"/> for additional information.

</para></listitem>
</varlistentry>

</variablelist>

   </sect3>

   <sect3 id="module-parms">
   <title>Passing parameters to kernel modules</title>
<para>

If drivers are compiled into the kernel, you can pass parameters to them
as described in the kernel documentation. However, if drivers are compiled
as modules and because kernel modules are loaded a bit differently during
an installation than when booting an installed system, it is not possible
to pass parameters to modules as you would normally do. Instead, you need
to use a special syntax recognized by the installer which will then make
sure that the parameters are saved in the proper configuration files and
will thus be used when the modules are actually loaded. The parameters
will also be propagated automatically to the configuration for the installed
system.

</para><para>

Note that it is now quite rare that parameters need to be passed to modules.
In most cases the kernel will be able to probe the hardware present in a
system and set good defaults that way. However, in some situations it may
still be needed to set parameters manually.

</para><para>

The syntax to use to set parameters for modules is:

<informalexample><screen>
<replaceable>module_name</replaceable>.<replaceable>parameter_name</replaceable>=<replaceable>value</replaceable>
</screen></informalexample>

If you need to pass multiple parameters to the same or different modules,
just repeat this. For example, to set an old 3Com network interface card
to use the BNC (coax) connector and IRQ 10, you would pass:

<informalexample><screen>
3c509.xcvr=3 3c509.irq=10
</screen></informalexample>

</para>
   </sect3>

   <sect3 id="module-blacklist">
   <title>Blacklisting kernel modules</title>
<para>

Sometimes it may be necessary to blacklist a module to prevent it from
being loaded automatically by the kernel and udev. One reason could be that
a particular module causes problems with your hardware. The kernel also
sometimes lists two different drivers for the same device. This can cause
the device to not work correctly if the drivers conflict or if the wrong
driver is loaded first.

</para><para>

You can blacklist a module using the following syntax:
<userinput><replaceable>module_name</replaceable>.blacklist=yes</userinput>.
This will cause the module to be blacklisted in
<filename>/etc/modprobe.d/blacklist.local</filename> both during the
installation and for the installed system.

</para><para>

Note that a module may still be loaded by the installation system itself.
You can prevent that from happening by running the installation in expert
mode and unselecting the module from the list of modules displayed during
the hardware detection phases.

</para>
   </sect3>
  </sect2>
 </sect1>
