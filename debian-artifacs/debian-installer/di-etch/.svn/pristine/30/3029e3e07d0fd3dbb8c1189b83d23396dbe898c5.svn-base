<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->


 <sect1 id="debian-orientation"><title>Debian に慣れる</title>
<para>

<!--
Debian is a little different from other distributions.  Even if you're
familiar with Linux in other distributions, there are things you
should know about Debian to help you to keep your system in a good,
clean state.  This chapter contains material to help you get oriented;
it is not intended to be a tutorial for how to use Debian, but just a
very brief glimpse of the system for the very rushed.
-->
Debian は他のディストリビューションとは少々異なっています。
他のディストリビューションで Linux に精通された方でも、
システムを整然とした状態に保つためには、
Debian について知っておかなくてはならないことがあります。
この章では Debian に慣れる手助けとなる資料を紹介します。
Debian の使い方を逐一説明することは意図していません。
すごく急いでいる人にシステムをざっとつかんでもらうだけのものです。

</para>

  <sect2><title>Debian パッケージングシステム</title>
<para>

<!--
The most important concept to grasp is the Debian packaging system.
In essence, large parts of your system should be considered under the
control of the packaging system.  These include:
-->
まず理解すべき最も重要な考え方に、Debian のパッケージングシステムがあります。
基本的に、システムの大部分はパッケージングシステムの管理下にあると考えられています。
このパッケージングシステムによって管理されるディレクトリには、
以下のディレクトリが含まれています。

<itemizedlist>
<listitem><para>

<filename>/usr</filename> (<filename>/usr/local</filename> を除く)

</para></listitem>
<listitem><para>

<filename>/var</filename> (<filename>/var/local</filename> を作成し、
それ以下のディレクトリを自由に使うことは可能です)

</para></listitem>
<listitem><para>

<filename>/bin</filename>

</para></listitem>
<listitem><para>

<filename>/sbin</filename>

</para></listitem>
<listitem><para>

<filename>/lib</filename>

</para></listitem>
</itemizedlist>

<!--
For instance, if you replace <filename>/usr/bin/perl</filename>, that
will work, but then if you upgrade your <classname>perl</classname>
package, the file you put there will be replaced.  Experts can get
around this by putting packages on <quote>hold</quote> in
<command>aptitude</command>.
-->
例えば、<filename>/usr/bin/perl</filename> をあなたが別に用意したファイルで
置き換えたとしても、その動作には問題はありません。ただし、後で
<classname>perl</classname> パッケージを更新すると、
あなたが置いたファイルはパッケージによって置き換えられてしまいます。
これを避けるには、
<command>aptitude</command> でパッケージを <quote>hold</quote> (保留)
するという操作を行います。

</para><para>

<!--
One of the best installation methods is apt. You can use the command
line version <command>apt-get</command> or full-screen text version
<application>aptitude</application>.  Note apt will also let you merge
main, contrib, and non-free so you can have export-restricted packages
as well as standard versions.
-->
ベストなインストール方法の一つに apt があります。コマンドライン版の
<command>apt-get</command> を利用することもできますし、フルスクリーンテキスト版の
<application>aptitude</application> を利用することもできます。apt は
main、contrib、non-free を統一的に処理するので、輸出制限パッケージも
スタンダードパッケージも同じ樣に扱うことができます。

</para>
  </sect2>

  <sect2><title>アプリケーションの種類の管理</title>
<para>


<!--
Alternative versions of applications are managed by update-alternatives. If
you are maintaining multiple versions of your applications, read the
update-alternatives man page.
-->
似たような種類のものが複数あるようなアプリケーションは、update-alternatives
で管理されています。同種のアプリケーションを複数保守している人は、
update-alternatives の man ページをご覧ください。

</para>
  </sect2>

  <sect2><title>cron ジョブ管理</title>
<para>

<!--
Any jobs under the purview of the system administrator should be in
<filename>/etc</filename>, since they are configuration files.  If you
have a root cron job for daily, weekly, or monthly runs, put them in
<filename>/etc/cron.{daily,weekly,monthly}</filename>.  These are
invoked from <filename>/etc/crontab</filename>, and will run in
alphabetic order, which serializes them.
-->
システム管理者権限のもとで実行するジョブは、設定ファイルのある
<filename>/etc</filename> に置いてください。
毎日、毎週、毎月 root で実行する cron ジョブがあれば、
<filename>/etc/cron.{daily,weekly,monthly}</filename> に置いてください。
これらは <filename>/etc/crontab</filename> から呼び出され、
アルファベット順に実行されます。

</para><para>

<!--
On the other hand, if you have a cron job that (a) needs to run as a
special user, or (b) needs to run at a special time or frequency, you
can use either <filename>/etc/crontab</filename>, or, better yet,
<filename>/etc/cron.d/whatever</filename>.  These particular files
also have an extra field that allows you to stipulate the user account
under which the cron job runs.
-->
一方、特定のユーザで実行する必要がある cron ジョブや、
特定の時間または頻度で実行する必要がある cron ジョブには、
<filename>/etc/crontab</filename> あるいは
<filename>/etc/cron.d/whatever</filename> が使えます (後者の方が望ましい)。
これらのファイルには cron ジョブを実行するユーザアカウントを明記する
特別なフィールドがあります。

</para><para>

<!--
In either case, you just edit the files and cron will notice them
automatically. There is no need to run a special command. For more
information see cron(8), crontab(5), and
<filename>/usr/share/doc/cron/README.Debian</filename>.
-->
どちらの場合も、ファイルを編集するだけで cron が自動的に実行してくれます。
特別なコマンドを実行する必要はありません。詳しい
情報は cron(8)、crontab(5)、
<filename>/usr/share/doc/cron/README.Debian</filename> をご覧ください。

</para>
  </sect2>
 </sect1>
