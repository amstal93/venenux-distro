<!-- original version: 40811 -->

 <sect1 id="install-overview">
 <title>Overzicht van het installatieproces</title>
<para>

Allereerst een opmerking over herinstallaties. Bij Debian zal zich
slechts zeer zelden een omstandigheid voordoen die een volledige
herinstallatie noodzakelijk maakt; waarschijnlijk is een storing in de
harde schijf nog de meest voorkomende situatie.

</para><para>

Veel gangbare besturingssystemen kunnen een volledige herinstallatie
noodzakelijk maken als zich ernstige fouten voordoen of bij opwaardering
naar een nieuwe versie van het besturingssysteem. Zelfs als geen volledig
nieuwe installatie nodig is, moeten programma's die u gebruikt veelal
opnieuw geïnstalleerd worden voordat zij fatsoenlijk werken onder het
nieuwe besturingssysteem.

</para><para>

Onder &debian; is het veel waarschijnlijker dat uw besturingssysteem
gerepareerd kan worden als er iets mis gaat. Bij een opwaardering zal nooit
een complete installatie nodig zijn: het is altijd mogelijk om het bestaande
systeem op te waarderen. En de programma's zijn vrijwel altijd compatibel
met opeenvolgende releases van het besturingssysteem. Als een nieuwe versie
van een programma ook nieuwe ondersteunende software vereist, zorgt de manier
waarop Debian pakketten maakt ervoor dat alle software die benodigd is,
automatisch wordt geïdentificeerd en geïnstalleerd. Omdat zoveel aandacht
is besteed aan het voorkomen van de noodzaak om opnieuw te installeeren,
zou u dat als uw allerlaatste redmiddel moeten beschouwen. Het installatiesysteem
is <emphasis>niet</emphasis> ontworpen om een installatie over een bestaand
systeem heen uit te voeren.

</para><para>

Hierna volgt een routekaart voor de stappen die u zult volgen tijdens het
installatieproces.
<!-- 'Road map'? Ze lijken Bush wel ;-( //-->

</para>

<orderedlist>
<listitem><para>

Maak een reservekopie van bestaande gegevens en documenten op de harde schijf
waarop u van plan bent Debian te installeren.

</para></listitem>
<listitem><para>

Verzamel informatie over uw computer en benodigde documentatie voordat u
met de installatie begint.

</para></listitem>
<listitem><para>

Maak op uw harde schijf ruimte vrij waarin door Debian partities kunnen worden
aangemaakt.

</para></listitem>
<listitem><para>

Pak of download de installatiesoftware en eventueel bestanden met specifieke
stuurbestanden die voor uw machine nodig zijn (dit geldt niet voor gebruikers
van de Debian Installatie CD).

</para></listitem>
<listitem><para>

Prepareer magneetbanden/diskettes/USB-sticks voor het opstarten van uw systeem
of plaats opstartbestanden (de meeste gebruikers van Debian Installatie CDs
kunnen opstarten vanaf één van de CDs).
<!-- FJP Wat wordt bedoeld met 'place boot files'? Bug in origineel. //-->

</para></listitem>
<listitem><para>

Start de computer op met het installatiesysteem.

</para></listitem>
<listitem arch="not-s390"><para>

Selecteer de taal voor het installatiesysteem.

</para></listitem>
<listitem arch="not-s390"><para>

Activeer de Ethernet netwerkverbinding (indien beschikbaar).

</para></listitem>

<listitem arch="s390"><para>

Configureer één netwerkinterface.

</para></listitem>
<listitem arch="s390"><para>

Open een ssh-verbinding met het nieuwe systeem.

</para></listitem>
<listitem arch="s390"><para>

Maak verbinding met één of meerdere DASDs (Direct Access Storage Device).

</para></listitem>

<listitem><para>

Maak en mount de partities waarop Debian zal worden geïnstalleerd.

</para></listitem>
<listitem><para>

Volg de volledig automatische download/installatie/instelling van het
<firstterm>basissysteem</firstterm>.

</para></listitem>
<listitem><para>

Installeer een <firstterm>opstartlader</firstterm> die &debian; en/of uw
bestaande systeem kan opstarten.

</para></listitem>
<listitem><para>

Start de computer opnieuw op met het nieuw geïnstalleerde systeem.

</para></listitem>
</orderedlist>

<para>

Voor het geval u problemen tegenkomt tijdens de installatie, kan het nuttig zijn
om een beeld te hebben van de pakketten die een rol spelen in de verschillende
stappen. Hieronder introduceren wij de belangrijkste acteurs in dit
installatietheater.

</para><para>

De installatiesoftware, <classname>debian-installer</classname>, is het
voornaamste onderwerp van deze handleiding. Zij verzorgt de herkenning van
hardware en laadt geschikte stuurprogramma's, gebruikt
<classname>dhclient</classname> voor het opzetten van de netwerkverbinding,
en start <classname>debootstrap</classname> voor de installatie van de pakketten
voor het basissysteem.
Er zijn nog veel meer actoren die elk een kleinere rol spelen in dit proces, maar
<classname>debian-installer</classname> heeft haar taak volbracht op het moment
dat u het nieuwe systeem voor het eerst opstart.

</para><para>

Om het systeem aan te passen aan uw behoeften, biedt <classname>tasksel</classname>
u de mogelijkheid om diverse voorgedefinieerde softwarebundels, zoals een
webserver of desktopomgeving, te installeren.

</para><para>

Als <classname>debian-installer</classname> klaar is, beschikt u slechts
over een zeer eenvoudig systeem dat opdrachtregel-gestuurd is. De grafische
gebruikersinterface die 'windows' op uw beeldscherm weergeeft (onder Linux het
'X Window System' genaamd), wordt niet geïnstalleerd tenzij u het selecteert
met behulp van <classname>tasksel</classname>. Installatie van het X Window
System is optioneel omdat veel &debian; systemen in gebruik zijn als server die
voor het vervullen van hun taak eigenlijk geen behoefte hebben aan een grafische
gebruikersinterface.
<!-- FJP Origineel is voor verbeteringen vatbaar:
- X Window System kan beter geïntroduceerd worden (naamgeving consequent!).
Is mijn vrije vertaling een verbetering?
Zou dit hele onderwerp niet beter eerder in de handleiding behandeld kunnen worden? //-->

</para><para arch="not-s390">

U dient zich ervan bewust te zijn dat het X Window System volledig los staat
van <classname>debian-installer</classname> en in feite veel complexer is.
De installatie van het X Window System en het oplossen van problemen tijdens
de installatie hiervan, vallen buiten de scope van deze handleiding.

</para>
 </sect1>
