<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 44002 -->

 <sect1 condition="supports-tftp" id="install-tftp">
 <title>TFTP ネットブート用ファイルの準備</title>
<para>

<!--
If your machine is connected to a local area network, you may be able
to boot it over the network from another machine, using TFTP. If you
intend to boot the installation system from another machine, the
boot files will need to be placed in specific locations on that machine,
and the machine configured to support booting of your specific machine.
-->
インストール対象のマシンが LAN に接続されている場合、
TFTP を用いると、そのマシンをネットワーク越しに他のマシンから起動できます。
インストールシステムを別のマシンから起動するには、
その「別のマシン」の特定の場所に起動ファイルを置き、
またインストール対象のマシンの起動をサポートするよう
設定しなければなりません。

</para><para>

<!--
You need to setup a TFTP server, and for many machines a DHCP
server<phrase condition="supports-rarp">, or RARP
server</phrase><phrase condition="supports-bootp">, or BOOTP
server</phrase>.
-->
TFTP サーバをセットアップする必要があります。
また多くのマシンでは DHCP サーバ<phrase condition="supports-rarp">、
または RARP サーバ</phrase><phrase condition="supports-bootp">、
または BOOTP サーバ</phrase>のセットアップも必要です。

</para><para>

<!--
<phrase condition="supports-rarp">The Reverse Address Resolution Protocol (RARP) is
one way to tell your client what IP address to use for itself. Another
way is to use the BOOTP protocol.</phrase>
-->
<phrase condition="supports-rarp">Reverse Address Resolution Protocol (RARP) は、
どの IP を用いるべきかをクライアントに伝える方法のひとつです。
同種の方法には BOOTP プロトコルがあります。</phrase>

<!--
<phrase condition="supports-bootp">BOOTP is an IP protocol that
informs a computer of its IP address and where on the network to obtain
a boot image.</phrase>
-->
<phrase condition="supports-bootp">BOOTP は IP プロトコルのひとつです。
クライアントに対して、使うべき IP アドレスと、
ブートイメージをネットワークのどこから取得するかを伝えます。</phrase>

<!--
<phrase arch="m68k">Yet another alternative exists on VMEbus
systems: the IP address can be manually configured in boot ROM.</phrase>
-->
<phrase arch="m68k">VMEbus システムではまた違った選択肢も存在します。
IP アドレスはブート ROM から手動で設定できます。</phrase>

<!--
The DHCP (Dynamic Host Configuration Protocol) is a more flexible,
backwards-compatible extension of BOOTP.
Some systems can only be configured via DHCP.
-->
DHCP (Dynamic Host Configuration Protocol) は、
BOOTP との後方互換性を保ちつつ、より柔軟に拡張させたものです。
システムによっては DHCP でしか設定できないこともあります。

</para><para arch="powerpc">

<!--
For PowerPC, if you have a NewWorld Power Macintosh machine, it is a
good idea to use DHCP instead of BOOTP.  Some of the latest machines
are unable to boot using BOOTP.
-->
PowerPC では、NewWorld Power Macintosh マシンを使っている場合は、
BOOTP ではなく DHCP を使う方が良いです。
最近のマシンには、BOOTP では起動できないものがあります。

</para><para arch="alpha">

<!--
Unlike the Open Firmware found on Sparc and PowerPC machines, the SRM
console will <emphasis>not</emphasis> use RARP to obtain its IP
address, and therefore you must use BOOTP for net booting your
Alpha<footnote>
-->
Sparc や PowerPC マシンの Open Firmware とは異なり、
SRM コンソールは IP アドレスの取得に RARP を使い<emphasis>ません</emphasis>。
従って Alpha をネットワークブートするには BOOTP を使う必要があります<footnote>
<para>
<!--
Alpha systems can also be net-booted using the DECNet MOP (Maintenance
Operations Protocol), but this is not covered here.  Presumably, your
local OpenVMS operator will be happy to assist you should you have
some burning need to use MOP to boot Linux on your Alpha.
-->
Alpha システムは DECNet MOP (Maintenance Operations Protocol)
を使ってもネットワークブートできます。
しかしここでは詳細については扱いません。
MOP を使って Alpha で Linux を起動する差し迫った必要が生じたら、
おそらくあなたの部門の OpenVMS 管理者が助けてくれるでしょう。
</para>
<!--
</footnote>. You can also enter the IP configuration for network
interfaces directly in the SRM console.
-->
</footnote>。
ネットワークインターフェースの IP の設定は、
直接 SRM コンソールから入力することもできます。

</para><para arch="hppa">

<!--
Some older HPPA machines (e.g. 715/75) use RBOOTD rather than BOOTP.
There is an <classname>rbootd</classname> package available in Debian.
-->
古い HPPA マシンのいくつか (例えば 715/75) は、BOOTP ではなく RBOOTD
を使います。
Debian には <classname>rbootd</classname> パッケージが用意されています。

</para><para>

<!--
The Trivial File Transfer Protocol (TFTP) is used to serve the boot
image to the client.  Theoretically, any server, on any platform,
which implements these protocols, may be used.  In the examples in
this section, we shall provide commands for SunOS 4.x, SunOS 5.x
(a.k.a. Solaris), and GNU/Linux.
-->
Trivial File Transfer Protocol (TFTP) は、
ブートイメージをクライアントに提供するために用います。
理論的には、どんなサーバでも、どんなプラットフォームでも、
これらのプロトコルを実装してさえいれば利用できます。
この節では、SunOS 4.x, SunOS 5.x (Solaris),
GNU/Linux での例を示します。

<note arch="x86"><para>

<!--
To use the Pre-boot Execution Environment (PXE) method of TFTP
booting, you will need a TFTP server with <userinput>tsize</userinput>
support.  On a &debian; server, the <classname>atftpd</classname> and
<classname>tftpd-hpa</classname> packages qualify; we recommend
<classname>tftpd-hpa</classname>.
</para><note arch="x86"><para>
-->
TFTP ブートで Pre-boot Execution Environment (PXE) 法を使用するには、
<userinput>tsize</userinput>をサポートする TFTP サーバが必要になります。
&debian; サーバでは、<classname>atftpd</classname> と 
<classname>tftpd-hpa</classname> がその資格があります。
<classname>tftpd-hpa</classname> をお奨めします。


</para></note>

</para>

&tftp-rarp.xml;
&tftp-bootp.xml;
&tftp-dhcp.xml;

  <sect2 id="tftpd">
  <title>TFTP サーバの立ち上げ</title>
<para>

<!--
To get the TFTP server ready to go, you should first make sure that
<command>tftpd</command> is enabled.  This is usually enabled by having
something like the following line in <filename>/etc/inetd.conf</filename>:
-->
TFTP サーバを立ち上げるには、
まず <command>tftpd</command> が有効になっているか確認します。
<filename>/etc/inetd.conf</filename> に次のような行があればおそらく大丈夫です。

<informalexample><screen>
tftp dgram udp wait nobody /usr/sbin/tcpd in.tftpd /tftpboot
</screen></informalexample>

<!--
Debian packages will in general set this up correctly by default when they
are installed.
-->
Debian パッケージは一般的にインストールする際、
デフォルトで正しくセットアップできます。

</para>
<note><para>

<!--
Historically, TFTP servers used <filename>/tftpboot</filename> as directory
to serve images from. However, &debian; packages may use other directories
to comply with the <ulink url="&url-fhs-home;">Filesystem Hierarchy
Standard</ulink>. For example, <classname>tftpd-hpa</classname> by default
uses <filename>/var/lib/tftpboot</filename>. You may have to adjust the
configuration examples in this section accordingly.
-->
歴史的に TFTP サーバは、
イメージを提供するディレクトリに <filename>/tftpboot</filename> を使用します。
しかし &debian; のパッケージでは、
<ulink url="&url-fhs-home;">Filesystem Hierarchy Standard</ulink> 
を満たす別のディレクトリを使用する可能性があります。
例えば、<classname>tftpd-hpa</classname> では 
<filename>/var/lib/tftpboot</filename> をデフォルトで使用します。
必要に応じて、本節の設定例を調整してください。

</para></note>
<para>

<!--
Look in <filename>/etc/inetd.conf</filename> and remember the directory which
is used as the argument of <command>in.tftpd</command><footnote>
-->
<filename>/etc/inetd.conf</filename> を見て、<command>in.tftpd</command> 
の引数に与えられているディレクトリを覚えておいてください<footnote>

<para>
<!--
The <userinput>-l</userinput> argument enables some versions of
<command>in.tftpd</command> to log all requests to the system logs;
this is useful for diagnosing boot errors.
-->
<command>in.tftpd</command> のバージョンによっては、
<userinput>-l</userinput> 引数をつけると、
すべての要求をシステムログに記録できます。
これは起動エラーの診断に有用です。
</para>

<!--
</footnote>; you'll need that below.
If you've had to change <filename>/etc/inetd.conf</filename>, you'll have to
notify the running <command>inetd</command> process that the file has changed.
On a Debian machine, run <userinput>/etc/init.d/inetd reload</userinput>; on
other machines, find out the process ID for <command>inetd</command>, and run
<userinput>kill -HUP <replaceable>inetd-pid</replaceable></userinput>.
-->
</footnote>。後でこのディレクトリを使います。
<filename>/etc/inetd.conf</filename> を変更したら、
変更したことを <command>inetd</command> に伝えなければなりません。
Debian マシンでは <userinput>/etc/init.d/inetd reload</userinput>
を実行します。
他のマシンでは、<command>inetd</command> のプロセス ID を見つけて、
<userinput>kill -HUP <replaceable>inetd-pid</replaceable></userinput> 
を実行します。

</para><para arch="mips">

<!--
If you intend to install Debian on an SGI machine and your TFTP server is a
GNU/Linux box running Linux 2.4, you'll need to set the following on your
server:
-->
SGI マシンに Debian をインストールする際、
TFTP サーバが Linux 2.4 で動作している GNU/Linux の場合は、
次の設定を行う必要があります。

<informalexample><screen>
# echo 1 &gt; /proc/sys/net/ipv4/ip_no_pmtu_disc
</screen></informalexample>

<!--
to turn off Path MTU discovery, otherwise the SGI's PROM can't
download the kernel. Furthermore, make sure TFTP packets are sent from
a source port no greater than 32767, or the download will stall after
the first packet.  Again, it's Linux 2.4.X tripping this bug in the
PROM, and you can avoid it by setting
-->
これを行わないと、SGI の PROM はカーネルをダウンロードできません。
さらに、TFTP パケットのソースポートは 32767 以上であってはいけません。
さもないと最初のパケットを受信しただけでダウンロードが停止します。
この PROM のバグに引っかかるのも Linux 2.4.X です。
これを避けるには

<informalexample><screen>
# echo "2048 32767" &gt; /proc/sys/net/ipv4/ip_local_port_range
</screen></informalexample>

<!--
to adjust the range of source ports the Linux TFTP server uses.
-->
を実行し、Linux TFTP サーバが用いるポートの範囲を調整してください。

</para>
  </sect2>

  <sect2 id="tftp-images">
  <title>TFTP イメージを適切な場所に配置する</title>
<para>

<!--
Next, place the TFTP boot image you need, as found in
<xref linkend="where-files"/>, in the <command>tftpd</command>
boot image directory.  You may have to make a link from that
file to the file which <command>tftpd</command> will use for booting a
particular client.  Unfortunately, the file name is determined by the
TFTP client, and there are no strong standards.
-->
次に行うことは、
<xref linkend="where-files"/> の記述にある、必要な TFTP ブートイメージを、
<command>tftpd</command> のブートイメージディレクトリに置く作業です。
<command>tftpd</command> が特定のクライアントの起動に用いるファイルへのリンクを、
ここに作成してください。
残念ながら、ファイルの名前は TFTP クライアントによって決まり、
強制力のある標準は存在しません。

</para><para arch="powerpc">

<!--
On NewWorld Power Macintosh machines, you will need to set up the
<command>yaboot</command> boot loader as the TFTP boot image.
<command>Yaboot</command> will then retrieve the kernel and RAMdisk
images via TFTP itself.  For net booting, use the
<filename>yaboot-netboot.conf</filename>.  Just rename this to
<filename>yaboot.conf</filename> in the TFTP directory.
-->
NewWorld Power Macintosh マシンでは、
<command>yaboot</command> ブートローダを TFTP ブートイメージに設定します。
<command>yaboot</command> は次にカーネルと RAM ディスクイメージを、
同じく TFTP によって取得します。
ネットワークブートには、<filename>yaboot-netboot.conf</filename> を使います。
これを <filename>yaboot.conf</filename> という名前に変えて、
TFTP のディレクトリに置いてください。

</para><para arch="x86">

<!--
For PXE booting, everything you should need is set up in the
<filename>netboot/netboot.tar.gz</filename> tarball. Simply extract this
tarball into the <command>tftpd</command> boot image directory. Make sure
your dhcp server is configured to pass <filename>pxelinux.0</filename>
to <command>tftpd</command> as the filename to boot.
-->
PXE 起動では、必要なことがすべて <filename>netboot/netboot.tar.gz</filename> 
tarball の中にセットアップされています。
単にこの tarball を、
<command>tftpd</command> ブートイメージディレクトリに展開してください。
<filename>pxelinux.0</filename> を、
起動するファイル名として <command>tftpd</command> へ渡すよう、
dhcp サーバが設定されていることを確認してください。

</para><para arch="ia64">

<!--
For PXE booting, everything you should need is set up in the
<filename>netboot/netboot.tar.gz</filename> tarball. Simply extract this
tarball into the <command>tftpd</command> boot image directory. Make sure
your dhcp server is configured to pass
<filename>/debian-installer/ia64/elilo.efi</filename>
to <command>tftpd</command> as the filename to boot.
-->
PXE 起動では、必要なことがすべて <filename>netboot/netboot.tar.gz</filename> 
tarball の中にセットアップされています。
単にこの tarball を、
<command>tftpd</command> ブートイメージディレクトリに展開してください。
<filename>/debian-installer/ia64/elilo.efi</filename> を、
起動するファイル名として <command>tftpd</command> へ渡すよう、
dhcp サーバが設定されていることを確認してください。

</para>

   <sect3 arch="mipsel">
   <title>DECstation TFTP イメージ</title>
<para>

<!--
For DECstations, there are tftpimage files for each subarchitecture,
which contain both kernel and installer in one file. The naming
convention is
<filename><replaceable>subarchitecture</replaceable>/netboot-boot.img</filename>.
Copy the tftpimage file you would like to use to
<userinput>/tftpboot/tftpboot.img</userinput> if you work with the
example BOOTP/DHCP setups described above.
-->
DECstation には、各サブアーキテクチャごとに tftp イメージがあり、
カーネルとインストーラがひとつのイメージに収められています。
命名規則は <filename><replaceable>subarchitecture</replaceable>/netboot-boot.img</filename> です。
上述の例のように BOOTP/DHCP を設定したのでしたら、
使いたい tftp イメージを <userinput>/tftpboot/tftpboot.img</userinput>
にコピーします。

</para><para>

<!--
The DECstation firmware boots by TFTP with the command <userinput>boot
<replaceable>#</replaceable>/tftp</userinput>, where
<replaceable>#</replaceable> is the number of the TurboChannel device
from which to boot. On most DECstations this is <quote>3</quote>.  If the
BOOTP/DHCP server does not supply the filename or you need to pass
additional parameters, they can optionally be appended with the
following syntax:
-->
DECstation ファームウェアから TFTP ブートするには、
<userinput>boot <replaceable>#</replaceable>/tftp</userinput>
というコマンドを使います。ここで <replaceable>#</replaceable> は起動元の
TurboChannel デバイスの番号です。
ほとんどの DECstation では、これは <quote>3</quote> です。
BOOTP/DHCP サーバがファイル名を与えない場合や、
パラメータを追加して渡したい場合は、
次の書式に従ってこれらを追加できます。

</para><para>

<userinput>boot #/tftp/filename param1=value1 param2=value2 ...</userinput>

</para><para>

<!--
Several DECstation firmware revisions show a problem with regard to
net booting: the transfer starts, but after some time it stops with
an <computeroutput>a.out err</computeroutput>. This can have several reasons:
-->
DECstation ファームウェアのリビジョンによっては、
ネットワークブートに問題があることがあります。
転送は開始するのですが、しばらく経つと
<computeroutput>a.out err</computeroutput> で停止してしまうのです。
これにはいくつかの理由が考えられます。

<orderedlist>
<listitem><para>

<!--
The firmware does not respond to ARP requests during a TFTP
transfer. This leads to an ARP timeout and the transfer stops.  The
solution is to add the MAC address of the Ethernet card in the
DECstation statically to the ARP table of the TFTP server.  This is
done by running <userinput>arp -s
<replaceable>IP-address</replaceable>
<replaceable>MAC-address</replaceable></userinput> as root on the
machine acting as TFTP server. The MAC-address of the DECstation can
be read out by entering <command>cnfg</command> at the DECstation
firmware prompt.
-->
ファームウェアが TFTP 転送の最中に ARP リクエストに反応しない。
すると ARP タイムアウトが起こり、転送が停止します。
これを解決するには、DEC station のイーサネットカードの MAC アドレスを、
TFTP サーバの ARP テーブルに静的に追加することです。
これを行うには TFTP サーバマシンの root 権限で
<userinput>arp -s
<replaceable>IP-address</replaceable>
<replaceable>MAC-address</replaceable></userinput> とします。
DECstation の MAC-address は、DECstation ファームウェアのプロンプトで
<command>cnfg</command> と入力すればわかります。

</para></listitem>
<listitem><para>

<!--
The firmware has a size limit on the files that can be booted
by TFTP.
-->
TFTP から起動できるファイルサイズの制限がファームウェアにある。

</para></listitem>
</orderedlist>

<!--
There are also firmware revisions that cannot boot via TFTP at all. An
overview about the different firmware revisions can be found at the
NetBSD web pages:
<ulink url="http://www.netbsd.org/Ports/pmax/board-list.html#proms"></ulink>.
-->
そもそも全く TFTP からは起動できないファームウェアリビジョンもあります。
ファームウェアリビジョンごとの違いについての概説は、
NetBSD web ページ:
<ulink url="http://www.netbsd.org/Ports/pmax/board-list.html#proms"></ulink>.
にあります。

</para>
   </sect3>

   <sect3 arch="alpha">
   <title>Alpha TFTP ブート</title>
<para>
<!--
On Alpha, you must specify the filename (as a relative path to the
boot image directory) using the <userinput>-file</userinput> argument
to the SRM <userinput>boot</userinput> command, or by setting the
<userinput>BOOT_FILE</userinput> environment variable.  Alternatively,
the filename can be given via BOOTP (in ISC <command>dhcpd</command>,
use the <userinput>filename</userinput> directive).  Unlike Open
Firmware, there is <emphasis>no default filename</emphasis> on SRM, so
you <emphasis>must</emphasis> specify a filename by either one of
these methods.
-->
Alpha では、SRM の <userinput>boot</userinput> コマンドの 
<userinput>-file</userinput> 引数を使うか、
<userinput>BOOT_FILE</userinput> 環境変数を使うかして、
ファイル名を (ブートイメージディレクトリからの相対パスとして) 
与える必要があります。
あるいはファイル名は、BOOTP から (ISC <command>dhcpd</command> なら 
<userinput>filename</userinput> ディレクティブによって)
与えることもできます。 Open Firmware とは異なり、SRM には
<emphasis>デフォルトのファイル名はありません</emphasis>。
従って、これらの方法のいずれかでファイル名を与えなければなりません。

</para>
   </sect3>

   <sect3 arch="sparc">
   <title>SPARC TFTP ブート</title>
<para>

<!--
Some SPARC architectures add the subarchitecture names, such as
<quote>SUN4M</quote> or <quote>SUN4C</quote>, to the filename. Thus,
if your system's subarchitecture is a SUN4C, and its IP is 192.168.1.3,
the filename would be <filename>C0A80103.SUN4C</filename>. However,
there are also subarchitectures where the file the client looks for is
just <filename>client-ip-in-hex</filename>. An easy way to determine the
hexadecimal code for the IP address is to enter the following command
in a shell (assuming the machine's intended IP is 10.0.0.4).
-->
<!--nabetaro 訳注残しました -->
SPARC アーキテクチャでは、
<quote>SUN4M</quote> や <quote>SUN4C</quote> のような
サブアーキテクチャの名前をファイル名に追加します。
例えば、システムのサブアーキテクチャが SUN4C で
IP アドレスが 192.168.1.3 の場合、
ファイル名は <filename>C0A80103.SUN4C</filename> となります。
しかし、クライアントが探すファイルが
<filename>client-ip-in-hex</filename> となるサブアーキテクチャもあります。
IP アドレスの 16 進表記を簡単に得るには、
以下のコマンドをシェルに入力してください。
(想定している IP アドレスは 10.0.0.4 です)

<informalexample><screen>
$ printf '%.2x%.2x%.2x%.2x\n' 10 0 0 4
</screen></informalexample>
 
<!--
To get to the correct filename, you will need to change all letters to
uppercase and if necessary append the subarchitecture name.
-->
正しいファイル名を取得するには、文字をすべて大文字に変更し、
(必要なら) サブアーキテクチャ名を追加しなければなりません。

</para><para>

<!--
If you've done all this correctly, giving the command <userinput>boot
net</userinput> from the OpenPROM should load the image. If the image
cannot be found, try checking the logs on your tftp server to see which
image name is being requested.
-->
すべて正しく行うと、
OpenPROM から <userinput>boot net</userinput> コマンドを与えて、
イメージをロードできます。
イメージが見つからない場合、tftp サーバのログをチェックし、
どのような名前のイメージを要求されているかを確認してください。

</para><para>

<!--
You can also force some sparc systems to look for a specific file name
by adding it to the end of the OpenPROM boot command, such as
<userinput>boot net my-sparc.image</userinput>. This must still reside
in the directory that the TFTP server looks in.
-->
OpenPROM の boot コマンドの末尾にファイル名を追加して、
sparc システムにその名前のファイルを探すよう指定することもできます
(<userinput>boot net my-sparc.image</userinput> のようになります)。
ただしこのファイルを置くのは、いずれにしても
TFTP サーバが見るディレクトリの内部でなければいけません。

</para>
   </sect3>

   <sect3 arch="m68k">
   <title>BVM/Motorola TFTP ブート</title>
<para>

<!--
For BVM and Motorola VMEbus systems copy the files
&bvme6000-tftp-files; to <filename>/tftpboot/</filename>.
-->
BVM システムや Motorola VMEbus システムでは、
&bvme6000-tftp-files; を <filename>/tftpboot/</filename>
にコピーしてください。

</para><para>

<!--
Next, configure your boot ROMs or BOOTP server to initially load the
<filename>tftplilo.bvme</filename> or
<filename>tftplilo.mvme</filename> files from the TFTP server.  Refer
to the <filename>tftplilo.txt</filename> file for your subarchitecture
for additional system-specific configuration information.
-->
次に boot ROM や BOOTP サーバを設定して、
<filename>tftplilo.bvme</filename> または 
<filename>tftplilo.mvme</filename> といったファイルを
TFTP サーバから最初にロードするようにしてください。
お使いのサブアーキテクチャについてや、
システム特有の設定情報に関しては、
<filename>tftplilo.txt</filename> を参照してください。

</para>
   </sect3>

   <sect3 arch="mips">
<!--
   <title>SGI TFTP Booting</title>
-->
   <title>SGI TFTP ブート</title>
<para>

<!--
On SGI machines you can rely on the <command>bootpd</command> to supply
the name of the TFTP file. It is given either as the
<userinput>bf=</userinput> in <filename>/etc/bootptab</filename> or as
the <userinput>filename=</userinput> option in
<filename>/etc/dhcpd.conf</filename>.
-->
SGI のマシンでは、TFTP ファイルの名前の指定を
<command>bootpd</command> に行わせることができます。
これは
<filename>/etc/bootptab</filename> の <userinput>bf=</userinput> に指定するか、
<filename>/etc/dhcpd.conf</filename> の <userinput>filename=</userinput> 
オプションに指定します。

</para>
   </sect3>

   <sect3 arch="mips">
<!--
   <title>Broadcom BCM91250A and BCM91480B TFTP Booting</title>
-->
   <title>Broadcom BCM91250A, BCM91480B の TFTP ブート</title>
<para>

<!--
You don't have to configure DHCP in a special way because you'll pass the
full path of the file to be loaded to CFE.
-->
CFE にロードするファイルのフルパスを渡しているため、
特殊な DHCP の設定をする必要はありません。

</para>
   </sect3>

  </sect2>

<!-- FIXME: commented out since it seems too old to be usable and a current
            way is not known
            訳注 訳文をそのままにコメントアウトします。

  <sect2 id="tftp-low-memory">
  <title>メモリの少ないシステムでの TFTP インストール</title>
<para>

On some systems, the standard installation RAMdisk, combined with the
memory requirements of the TFTP boot image, cannot fit in memory.  In
this case, you can still install using TFTP, you'll just have to go
through the additional step of NFS mounting your root directory over
the network as well.  This type of setup is also appropriate for
diskless or dataless clients.
システムによっては、標準のインストール RAM ディスクと、
TFTP ブートイメージに必要なメモリとをあわせると、
メモリが足りないことがあります。
この場合でも TFTP を用いたインストールは可能で、
ルートディレクトリを同様にネットワークから
NFS でマウントするようにすればいいのです。
このタイプの設定は、
ディスクレスクライアントやデータレスクライアントにも適しています。

</para><para>

First, follow all the steps above in <xref linkend="install-tftp"/>.
まず、<xref linkend="install-tftp"/> に説明されている全ての手順を踏んでください。

<orderedlist>
<listitem><para>

Copy the Linux kernel image on your TFTP server using the
<userinput>a.out</userinput> image for the architecture you are
booting.
Linux カーネルイメージを TFTP サーバにコピーします。
起動させようとしているアーキテクチャの 
<userinput>a.out</userinput> イメージにしてください。

</para></listitem>
<listitem><para>

Untar the root archive on your NFS server (can be the same system as
your TFTP server):
root アーカイブを NFS サーバで untar します
(NFS サーバは TFTP サーバと同じマシンでも構いません)。

<informalexample><screen>
# cd /tftpboot
# tar xvzf root.tar.gz
</screen></informalexample>

Be sure to use the GNU <command>tar</command> (other tar programs, like the
SunOS one, badly handle devices as plain files).
GNU <command>tar</command> を使ってください。
(SunOS のものなど、他の tar プログラムでは、
デバイスを通常のファイルとして扱ってしまいます)

</para></listitem>
<listitem><para>

Export your <filename>/tftpboot/debian-sparc-root</filename> directory
with root access to your client.  E.g., add the following line to
<filename>/etc/exports</filename> (GNU/Linux syntax, should be similar
for SunOS):
<filename>/tftpboot/debian-sparc-root</filename> ディレクトリを、
クライアントから root でアクセスできるようにします。
つまり次の行を <filename>/etc/exports</filename> に追加します
(GNU/Linux の書式ですが、SunOS でも同じようなもののはず):

<informalexample><screen>
/tftpboot/debian-sparc-root <replaceable>client</replaceable>(rw,no_root_squash
</screen></informalexample>

NOTE: <replaceable>client</replaceable> is the host name or IP address recognized
by the server for the system you are booting.
注意: <replaceable>client</replaceable> は、起動しようとしているシステムの
(このサーバから見た) ホスト名または IP アドレスにします。

</para></listitem>
<listitem><para>

Create a symbolic link from your client IP address in dotted notation
to <filename>debian-sparc-root</filename> in the
<filename>/tftpboot</filename> directory.  For example, if the client
IP address is 192.168.1.3, do
<filename>/tftpboot</filename> ディレクトリに、
<filename>debian-sparc-root</filename> に対するシンボリックリンクを、
IP アドレスの名前 (ドット区切り表記) で作ります。
例えばクライアントの IP アドレスが 192.168.1.3 なら次のようにします。

<informalexample><screen>
# ln -s debian-sparc-root 192.168.1.3
</screen></informalexample>

</para></listitem>
</orderedlist>

</para>

  </sect2>

  <sect2 condition="supports-nfsroot">
  <title>TFTP と NFS Root でのインストール</title>
<para>

Installing with TFTP and NFS Root is similar to
<xref linkend="tftp-low-memory"/> because you don't want to
load the RAMdisk anymore but boot from the newly created NFS-root file
system.  You then need to replace the symlink to the tftpboot image by
a symlink to the kernel image (for example,
<filename>linux-a.out</filename>).
<xref linkend="tftp-low-memory"/> の場合と似ています。
RAM ディスクをロードせず、
新しく作った NFS-root ファイルシステムから起動させたいわけですから。
tftpboot イメージへのシンボリックリンクは、
カーネルイメージへのシンボリック
(例えば <filename>linux-a.out</filename>) に置き換える必要があります。

</para><para>

RARP/TFTP requires all daemons to be running on the same server (the
workstation is sending a TFTP request back to the server that replied
to its previous RARP request).
RARP/TFTP は、すべてのデーモンが同じサーバで動作している必要がありました
(sparc ワークステーションは TFTP リクエストを以前の RARP リクエストに
返答したサーバに送ります)。
</para>


  </sect2>
END FIXME -->
 </sect1>
