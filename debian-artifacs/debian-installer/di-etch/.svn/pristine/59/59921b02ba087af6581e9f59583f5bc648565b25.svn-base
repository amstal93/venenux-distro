#!/bin/sh

# Copyright (C) 2005, 2006  Martin Michlmayr <tbm@cyrius.com>

# This code is covered by the GNU General Public License.

set -e

. /usr/lib/oldsys-preseed/functions

log() {
	logger -t oldsys-preseed "$@"
}

exit_unknown() {
	log "Unknown system - not writing preseed file"
	exit 0
}

# If this is set to "yes", the user *absolutely* cannot input anything
# before network-console comes up.  Therefore, preseed some info which
# is not optimal but which will ensure that network-console is reached
# without prompting for user input.
NONINTERACTIVE="yes"
FILE=/preseed.cfg

case "`archdetect`" in
	arm*/ixp4xx | arm*/nslu2 )
		machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
		if echo "$machine" | grep -q "^Linksys NSLU2"; then
			check_file /proc/mtd
			sysconf=$(get_mtdblock "SysConf")
			if [ -z "$sysconf" ]; then
				log "Can't find SysConf MTD partition"
				exit
			fi
			parse_sysconf "/dev/$sysconf"
			# The original NSLU2 uses a different name for the network interface
			if [ "$INTERFACE" = "ixp0" ]; then
				# IXP4xx NPE ethernet interface seems to come
				# up as eth0 (#407460) therefore only use eth0
				# for the installer if the NPE microcode is
				# present
				if [ -e /lib/firmware/NPE-B ]; then
					INTERFACE=eth0
				else
					INTERFACE=eth1
				fi
			fi
			# Use DHCP if the original IP from the firmware has never been changed
			if [ "$IPADDRESS" = "192.168.1.77" ]; then
				unset NET_CONFIG
			fi
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.77
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			if [ "$NONINTERACTIVE" = "yes" ]; then
				add "$FILE" "ethdetect/use_firewire_ethernet" "boolean" "false"
			fi
		fi
	;;
	arm*/iop32x)
		machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
		if echo "$machine" | grep -q "^Thecus"; then
			check_file /proc/mtd
			config=$(get_mtdblock "user")
			if [ -z "$config" ]; then
				log "Can't find MTD partition holding config data"
				exit
			fi
			path=/tmp/oldsys-preseed
			mkdir -p $path/mnt $path/defaults
			mount -t jffs2 /dev/$config $path/mnt
			if [ -e $path/mnt/default.tar.gz ]; then
				(cd $path/defaults && tar -xzf $path/mnt/default.tar.gz)
				if [ -e $path/defaults/app/etc/HOSTNAME ]; then
					DEFAULT_HOSTNAME=$(cut -d . -f 1 $path/defaults/app/etc/HOSTNAME)
					DEFAULT_DOMAIN=$(cut -d . -f 2- $path/defaults/app/etc/HOSTNAME)
				fi
				if [ -e $path/defaults/app/cfg/cfg_nic0 ]; then
					parse_ifconfig $path/defaults/app/cfg/cfg_nic0
					[ -n "$IPADDRESS" ] && DEFAULT_IPADDRESS=$IPADDRESS
					unset IPADDRESS NETMASK GATEWAY
				fi
			fi
			parse_unix_tree $path/mnt
			INTERFACE=eth0
			if grep -q udhcpc $path/mnt/cfg/cfg_nic0; then
				NET_CONFIG=dhcp
			else
				NET_CONFIG=static
				parse_ifconfig $path/mnt/cfg/cfg_nic0
			fi
			# Use DHCP if the original IP from the firmware has never been changed.
			# The default IP address 192.168.1.100 but defaults/app/cfg/cfg_nic0
			# could theoretically contain a different value so take both into account.
			# Set the fallback to 192.168.1.100 so a fixed value can be documented.
			if [ "$IPADDRESS" = "$DEFAULT_IPADDRESS" -o "$IPADDRESS" = "192.168.1.100" ]; then
				unset NET_CONFIG
				IPADDRESS=192.168.1.100
				unset NETMASK GATEWAY
			fi
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.100
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			HOSTNAME=$(cut -d . -f 1 $path/mnt/etc/HOSTNAME)
			# work around a bug in busybox's cut
			DOMAIN=$( (cat $path/mnt/etc/HOSTNAME ; echo) | cut -d . -f 2-)
			unset_matching_var "HOSTNAME" "$DEFAULT_HOSTNAME"
			# The default says Thecus_N2100 but in fact it is set to N2100....
			unset_matching_var "HOSTNAME" "N2100"
			unset_matching_var "HOSTNAME" "N4100"
			unset_matching_var "DOMAIN" "$DEFAULT_DOMAIN"
			unset_matching_var "DOMAIN" "thecus.com"
			umount $path/mnt
			rm -rf $path/defaults
			rmdir $path/mnt $path || true
		else
			exit_unknown
		fi
	;;
	mipsel/bcm947xx)
		# Some BCM947xx devices use NVRAM to store their Linux
		# configuration, some use a Linux filesystem.  We only
		# support the latter at the moment.
		if [ 1 ]; then
			check_file /proc/mtd
			config=$(get_mtdblock "config")
			if [ -z "$config" ]; then
				log "Can't find MTD partition holding Linux config"
				exit
			fi
			path=/tmp/oldsys-preseed
			mkdir -p $path/mnt $path/lpr
			mount -t minix /dev/$config $path/mnt
			if [ ! -e $path/mnt/config.lrp ]; then
				log "config.lrp does not exist in $path/mnt"
				umount $path/mnt
				exit
			fi
			cd $path/lpr
			tar -xzf $path/mnt/config.lrp
			parse_unix_tree $path/lpr
			parse_leaf_tree $path/lpr
			parse_network_interfaces $path/lpr/etc/network/interfaces "vlan6"
			if [ "$INTERFACE" = "vlan6" ]; then
				INTERFACE=eth0
			fi
			DEFAULT_HOSTNAME="WGT634U"
			umount $path/mnt
			rm -rf $path/lpr
			rmdir $path/mnt $path || true
		else
			parse_bcm947xx_nvram "$(nvram show)"
		fi
	;;
	*)
		exit_unknown
	;;
esac

if [ "$NONINTERACTIVE" = "yes" ]; then
	# localechooser yet needs a way to run after network-console;
	# in the meantime, preseed this information. However, don't preseed
	# countrychooser/country-name in order to force the installer to
	# prompt the user for a country when configuring the clock. Note
	# that for this method to work, localechooser must not be included
	# in the installer image.
	add "$FILE" "debian-installer/locale" "string" "C"
	# Just continue if d-i enters lowmem mode
	add "$FILE" "lowmem/low" "note"
	# Any hostname and domain names assigned from DHCP take precedence
	# over values set here.  However, setting the values still prevents
	# the questions from being shown, even if values come from dhcp.
	add "$FILE" "netcfg/get_hostname" "string" "debian"
	add "$FILE" "netcfg/get_domain" "string" "example.org"
	# I'm not terribly happy to preseed a generic password but I guess
	# there's no other way on some machines.
	add "$FILE" "network-console/password" "password" "install"
	add "$FILE" "network-console/password-again" "password" "install"
fi

# Workaround for broken partconf
add "$FILE" "partconf/already-mounted" "boolean" "false"

generate_preseed_file $FILE

