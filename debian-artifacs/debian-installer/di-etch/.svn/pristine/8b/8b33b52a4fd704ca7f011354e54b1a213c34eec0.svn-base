<!-- original version: 31173 -->

   <sect3 id="network-console">
   <title>Installeren over het netwerk</title>

<para arch="not-s390">

Een van de meer interressante componenten is
<firstterm>network-console</firstterm>. Deze component maakt het mogelijk
om een groot deel van de installatie via SSH over het netwerk uit te voeren.
Het gebruik van het netwerk impliceert dat u de eerste stappen van de
installatie vanaf de console zult moeten uitvoeren: in ieder geval tot en
met de configuratie van het netwerk. (Het is echter mogelijk om dat deel van
de installatie te automatiseren; zie <xref linkend="automatic-install"/>.)

</para><para arch="not-s390">

Deze component wordt niet automatisch getoond in het installatie menu en
dus moet u hem expliciet laden.

Als u installeert vanaf CD-ROM, dient u de installatie te starten met
prioriteit <quote>medium</quote> of op een andere wijze het hoofdmenu
zichtbaar te maken. Bij de optie <guimenuitem>Installatiemodules van CD
laden</guimenuitem> selecteert u <guimenuitem>network-console: Continue
installation remotely using SSH</guimenuitem>. De component is succesvol
geladen als in het hoofdmenu een nieuwe optie <guimenuitem>Installatie
verder van op afstand doorlopen via SSH</guimenuitem> aanwezig is. 

</para><para arch="s390">

Voor installaties op &arch-title; is dit de standaard methode nadat
het netwerk is geconfigureerd.

</para><para>

<phrase arch="not-s390">Nadat u deze nieuwe optie heeft geselecteerd,
zal u</phrase><phrase arch="s390">U zal</phrase> worden gevraagd
naar een wachtwoord waarmee de verbinding met het installatiesysteem
zal worden gerealiseerd en een bevestiging daarvan. Dat is alles.
Vervolgens zal een melding worden getoond met instructies om vanaf een
ander systeem in te loggen als gebruiker <emphasis>installer</emphasis>
met het wachtwoord dat u daarnet heeft ingevoerd. Een ander belangrijk
detail op dit scherm is de <quote>vingerafdruk</quote> van dit systeem.
U dient deze op een veilige manier beschikbaar te stellen aan degene die
de installatie op afstand zal vervolgen.

</para><para>

Indien u besluit de installatie locaal te vervolgen, kunt u altijd met
behulp van de &enterkey; terugkeren naar het hoofdmenu, vanwaar u de
installatie kunt vervolgen.

</para><para>

Laten we ons nu verplaatsen naar het andere einde van de netwerkkabel.
Voordat u de verbinding kunt opzetten, dient u uw terminal te configureren
voor UTF-8 codering aangezien dat is wat het installatiesysteem gebruikt.
Als u dit niet doet is installatie nog wel mogelijk, maar kunt u vreemde
effecten op uw scherm tegenkomen als ontbrekende randen van dialogen of
onleesbare niet-ASCII karakters. U brengt de verbinding met het
installatiesysteem eenvoudig tot stand met:

<informalexample><screen>
<prompt>$</prompt> <userinput>ssh -l installer <replaceable>nieuw_systeem</replaceable></userinput>
</screen></informalexample>

Hierbij is <replaceable>nieuw_systeem</replaceable> ofwel de systeemnaam
ofwel het IP-adres van het systeem dat wordt geïnstalleerd. Voordat
daadwerkelijk wordt aangelogd zal de vingerafdruk van dat systeem worden
getoond en zult u moeten bevestigen dat dit correct is.

</para><note><para>

Als u meerdere systemen achter elkaar installeert en deze hetzelfde IP-adres
of dezelfde systeemnaam krijgen, zal <command>ssh</command> weigeren om de
verbinding te maken. De reden is dat opeenvolgende systemen een andere
vingerafdruk zullen hebben, wat meestal een indicatie is van een
<quote>spoofing</quote> aanval. Als u er zeker van bent dat dit niet het
geval is, dient u de betreffende regel te verwijderen uit
<filename>~/.ssh/known_hosts</filename> waarna u een nieuwe poging kunt doen.

</para></note><para>

Nadat u bent aangelogd zal een aanvangsscherm worden getoond met twee
opties genaamd <guimenuitem>Menu opstarten</guimenuitem> en
<guimenuitem>Shell opstarten</guimenuitem>. Eerstgenoemde optie biedt
toegang tot het hoofdmenu van het installatiesysteem vanwaar u de
installatie kunt vervolgen. Laatstgenoemde optie start een shell op het
nieuwe systeem die u kunt gebruiken om bijvoorbeeld logs te bekijken of
commando's uit te voeren. U dient maximaal één SSH-sessie te starten met
een installatiemenu, maar kunt desgewenst meerdere sessies starten met
een shell.

</para><warning><para>

Nadat u via SSH de installatie op afstand heeft gestart, kunt u de
installatiesessie op de locale console niet meer hervatten. Als u dat toch
doet, is de kans groot dat de gegevensbank met de configuratie van het
nieuwe systeem beschadigd raakt. Dit kan weer tot gevolg hebben dat de
installatie mislukt of resulteren in problemen met het nieuwe systeem.

</para><para>

Daarnaast wordt afgeraden om, als u de SSH-sessie uitvoert in een
X-terminal, de grootte van het venster te wijzigen aangezien het gevolg
zal zijn dat de verbinding wordt verbroken.

</para></warning>

   </sect3>
