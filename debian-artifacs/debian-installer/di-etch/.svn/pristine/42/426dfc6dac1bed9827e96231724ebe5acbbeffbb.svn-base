<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 45291 -->

<!--
<appendix id="installation-howto">
<title>Installation Howto</title>
-->
<appendix id="installation-howto">
<title>インストール Howto</title>

<para>

<!--
This document describes how to install &debian; &releasename; for
the &arch-title; (<quote>&architecture;</quote>) with the
new &d-i;. It is a quick walkthrough of the installation process
which should contain all the information you will need for most installs.
When more information can be useful, we will link to more detailed
explanations in other parts of this document.
-->
この文書は、新しい &d-i; で &arch-title; (<quote>&architecture;</quote>) に
&debian; &releasename; をインストールする方法について説明します。
これは、インストール作業の迅速なリハーサルで、
たいていの導入のために必要となるであろうすべての情報を含んでいます。
もっと多くの情報が有用な場合には、この文書内の他の部分にある、
より詳細な説明にリンクします。

</para>

 <sect1 id="howto-preliminaries">
<!--
 <title>Preliminaries</title>
-->
 <title>前置き</title>
<para>

<phrase condition="unofficial-build">
<!--
The debian-installer is still in a beta state.
-->
debian-installer はまだベータ版の状態です。
</phrase>
<!--
If you encounter bugs during your install, please refer to
<xref linkend="submit-bug" /> for instructions
on how to report them. If you have questions which cannot be
answered by this document, please direct them to the debian-boot
mailing list (&email-debian-boot-list;) or ask on IRC (#debian-boot
on the OFTC network).
-->
インストール中にバグに遭遇した場合には、それらを報告する方法の説明のために
<xref linkend="submit-bug" /> を参照してください。
この文書で答えることができない質問があれば、debian-boot
メーリングリスト (&email-debian-boot-list;) で直接質問するか、
IRC (OFTC ネットワーク上の #debian-boot) で訊ねてください。

</para>
 </sect1>

 <sect1 id="howto-getting-images">
<!--
 <title>Booting the installer</title>
-->
 <title>インストーラを起動する</title>
<para>

<phrase condition="unofficial-build">
<!--
For some quick links to CD images, check out the <ulink url="&url-d-i;">
&d-i; home page</ulink>.
-->
CD イメージへのリンクが直ちに必要な方は、<ulink url="&url-d-i;">
&d-i; ホームページ</ulink>を確認してください。
</phrase>
<!--
The debian-cd team provides builds of CD images using &d-i; on the
<ulink url="&url-debian-cd;">Debian CD page</ulink>.
For more information on where to get CDs, see <xref linkend="official-cdrom" />.
-->
debian-cd チームが &d-i; を使用してビルドした CD イメージは、
<ulink url="&url-debian-cd;">Debian CD ページ</ulink>から入手できます。
どこで CD を手に入れられるかについてのより詳細に関しては、
<xref linkend="official-cdrom" />をご覧ください。

</para><para>

<!--
Some installation methods require other images than CD images.
-->
一部のインストール方法では、CD イメージ以外のイメージを必要とします。
<phrase condition="unofficial-build">
<!--
The <ulink url="&url-d-i;">&d-i; home page</ulink> has links to
other images.
-->
<ulink url="&url-d-i;">&d-i; ホームページ</ulink>には、
他のイメージへのリンクがあります。
</phrase>
<!--
<xref linkend="where-files" /> explains how to find images on Debian
mirrors.
-->
<xref linkend="where-files" />は、Debian ミラーサイトでイメージを
探す方法について説明しています。

</para><para>

<!--
The subsections below will give the details about which images you should
get for each possible means of installation.
-->
以下の小節では、インストール可能なそれぞれの手段のために
どのイメージを取得するべきかを詳しく説明します。

</para>

  <sect2 id="howto-getting-images-cdrom">
<!--
  <title>CDROM</title>
-->
  <title>CD-ROM</title>

<para>

<!--
There are two different netinst CD images which can be used to install
&releasename; with the &d-i;. These images are intended to boot from CD
and install additional packages over a network, hence the name 'netinst'.
The difference between the two images is that on the full netinst image
the base packages are included, whereas you have to download these from
the web if you are using the business card image. If you'd rather, you can
get a full size CD image which will not need the network to install. You
only need the first CD of the set.
-->
&d-i; での &releasename; のインストールに使用できる 2 つの異なる netinst CD
イメージがあります。これらは、CD から起動し、ネットワーク越しに追加パッケージを
インストールするように意図されているので、`netinst' という名前がついています。
2 つのイメージの違いは、完全な netinst イメージの方にはベースパッケージが
含まれているのに対して、名刺サイズの CD イメージを使用する場合は、ウェブからそれらを
ダウンロードしなければならないということです。インストールするのにはむしろ
ネットワークを必要としないフルサイズ CD イメージを手に入れる方が良いでしょう。
それなら CD セットの最初の 1 枚を必要とするだけです。

</para><para>

<!--
Download whichever type you prefer and burn it to a CD.
-->
好みのタイプをダウンロードして、CD に焼いてください。
<!--
<phrase arch="x86">To boot the CD, you may need to change your BIOS
configuration, as explained in <xref linkend="bios-setup" />.</phrase>
-->
<phrase arch="x86">CD から起動するには、<xref linkend="bios-setup" />
で説明しているように、BIOS 設定を変更する必要があるかもしれません。</phrase>
<phrase arch="powerpc">
<!--
To boot a PowerMac from CD, press the <keycap>c</keycap> key while booting. See
<xref linkend="boot-cd" /> for other ways to boot from CD.
-->
CD から PowerMac を起動するには、起動中ずっと <keycap>c</keycap> キーを押し続けてください。
CD から起動するその他の方法は、<xref linkend="boot-cd" />をご覧ください。

</phrase>

</para>
  </sect2>

  <sect2 condition="supports-floppy-boot" id="howto-getting-images-floppy">
  <title>Floppy</title>
<para>

<!--
If you can't boot from CD, you can download floppy images to install
Debian. You need the <filename>floppy/boot.img</filename>, the
<filename>floppy/root.img</filename> and one or more of the driver disks.
-->
CD から起動できない場合は、ダウンロードしたフロッピーイメージで Debian を
インストールできます。<filename>floppy/boot.img</filename> と
<filename>floppy/root.img</filename>、そしてたぶん 1 枚か数枚のドライバディスクを必要とします。

</para><para>

<!--
The boot floppy is the one with <filename>boot.img</filename> on it.
This floppy, when booted, will prompt you to insert a second floppy &mdash;
use the one with <filename>root.img</filename> on it.
-->
中に <filename>boot.img</filename> を含んでいるのが、ブートフロッピーです。
このフロッピーで起動すると、2 番目のフロッピーを挿入するよう指示されます &mdash;
そこで <filename>root.img</filename> を含むフロッピーを使用します。

</para><para>

<!--
If you're planning to install over the network, you will usually need
the <filename>floppy/net-drivers-1.img</filename>. For PCMCIA or USB
networking, and some less common network cards, you will also need a second
driver floppy, <filename>floppy/net-drivers-2.img</filename>.
-->
ネットワーク越しのインストールを計画している場合は、一般に
<filename>floppy/net-drivers-1.img</filename> が必要です。PCMCIA や USB
ネットワーク、あるいはあまり一般的でないネットワークカードを使用するには、
2 枚目のドライバフロッピー <filename>floppy/net-drivers-2.img</filename>
が必要になります。

</para><para>

<!--
If you have a CD, but cannot boot from it, then boot from floppies and use
<filename>floppy/cd-drivers.img</filename> on a driver disk to complete the
install using the CD.
-->
CD は持っているけれどそこから起動できない場合は、まずフロッピーから起動して、
CD を使ったインストールを完了するためにドライバディスクの
<filename>floppy/cd-drivers.img</filename> を使ってください。

</para><para>

<!--
Floppy disks are one of the least reliable media around, so be prepared for
lots of bad disks (see <xref linkend="unreliable-floppies" />). Each
<filename>.img</filename> file you downloaded goes on a single floppy;
you can use the dd command to write it to /dev/fd0 or some other means
(see <xref linkend="create-floppy" /> for details).
Since you'll have more than one floppy, it's a good idea to label them.
-->
フロッピーディスクは、現存しているメディアの中で最も信頼性が低いものの 1 つなので、
たくさんの不良ディスクが出る覚悟をしてください (<xref linkend="unreliable-floppies" />をご覧ください)。
ダウンロードしたそれぞれの <filename>.img</filename> ファイルは単一のフロッピーに
入れられます。/dev/fd0 やなにか他の装置に書き込むには dd コマンドが使えます
(詳細は <xref linkend="create-floppy" />をご覧ください)。
2 枚以上のフロッピーがあるなら、ラベルを付けておくのは良い考えです。

</para>
  </sect2>

  <sect2 condition="bootable-usb" id="howto-getting-images-usb">
<!--
  <title>USB memory stick</title>
-->
  <title>USB メモリ</title>
<para>

<!--
It's also possible to install from removable USB storage devices. For
example a USB keychain can make a handy Debian install medium that you
can take with you anywhere.
-->
取り外し可能な USB 記憶装置からもインストールできます。例えば、
USB メモリは、どんな場所ででも手軽に Debian をインストールできる
媒体です。

</para><para>

<!--
The easiest way to prepare your USB memory stick is to download
<filename>hd-media/boot.img.gz</filename>, and use gunzip to extract the 256 MB
image from that file. Write this image directly to your memory stick, which
must be at least 256 mb in size. Of course this will destroy anything already
on the memory stick. Then mount the memory stick, which will now have a FAT
filesystem on it. Next, download a Debian netinst CD image, and copy that file
to the memory stick; any filename is ok as long as it ends in
<literal>.iso</literal>.
-->
USB メモリを準備する最も簡単な方法は、
<filename>hd-media/boot.img.gz</filename> をダウンロードし gunzip を使用して、
そのファイルから 256MB のイメージを抽出することです。そのイメージを USB メモリ
(少なくとも 256MB のサイズが必要です) に直接書き込んでください。もちろんこれは、
USB メモリ上のすでにある何もかもを破壊してしまいます。それから、今ではもう
FAT ファイルシステムのある USB メモリをマウントしてください。次に、Debian netinst CD
イメージをダウンロードして、USB メモリにそのファイルをコピーしてください。
ファイル名が <literal>.iso</literal> で終わりさえすれば、どんなファイル名でも大丈夫です。

</para><para>

<!--
There are other, more flexible ways to set up a memory stick to use the
debian-installer, and it's possible to get it to work with smaller memory
sticks. For details, see <xref linkend="boot-usb-files" />.
-->
その他にも、debian-installer で使用するための USB メモリを設定する
より柔軟な方法があり、もっと小さなサイズの USB メモリで動作させられます。
詳細は、<xref linkend="boot-usb-files" />をご覧ください。

</para><para arch="x86">

<!--
Some BIOSes can boot USB storage directly, and some cannot. You may need to
configure your BIOS to boot from a <quote>removable drive</quote> or even a
<quote>USB-ZIP</quote> to get it to boot from the USB device. For helpful
hints and details, see <xref linkend="usb-boot" />.
-->
いくつかの BIOS は、USB 記憶装置を直接起動できますが、その他では起動できません。 
<quote>リムーバブルドライブ</quote> から起動するために BIOS を，
あるいは USB デバイスから起動するためにさらに <quote>USB-ZIP</quote> を設定する
必要があるかもしれません。役に立つヒントや詳細に関しては、
<xref linkend="usb-boot" />をご覧ください。

</para><para arch="powerpc">

<!--
Booting Macintosh systems from USB storage devices involves manual use
of Open Firmware. For directions, see <xref linkend="usb-boot" />.
-->
USB 記憶装置から Macintosh システムを起動するには、Open Firmware
のマニュアルを必要とします。方法は、<xref linkend="usb-boot" />をご覧ください。

</para>
  </sect2>

  <sect2 id="howto-getting-images-netboot">
<!--
  <title>Booting from network</title>
-->
  <title>ネットワークからの起動</title>
<para>

<!--
It's also possible to boot &d-i; completely from the net. The
various methods to netboot depend on your architecture and netboot setup.
The files in <filename>netboot/</filename> can be used to netboot &d-i;.
-->
&d-i; をネットから完全に起動することもできます。
netboot のための様々な方法は、アーキテクチャや netboot の設定に依存します。
<filename>netboot/</filename> 以下のファイルは、&d-i; を netboot するために使用できます。

</para><para arch="x86">

<!--
The easiest thing to set up is probably PXE netbooting. Untar the
file <filename>netboot/pxeboot.tar.gz</filename> into
<filename>/var/lib/tftpboot</filename> or
wherever is appropriate for your tftp server. Set up your DHCP server to pass
filename <filename>/pxelinux.0</filename> to clients, and with luck
everything will just work.
For detailed instructions, see <xref linkend="install-tftp" />.
-->
設定するのに最も容易なのは、おそらく PXE netbooting です。
<filename>netboot/pxeboot.tar.gz</filename> ファイルを
<filename>/var/lib/tftpboot</filename> の中か、
あるいは tftp サーバの適切な場所ならどこにでも解凍してください。
DHCP サーバを設定して、クライアントへファイル名 <filename>/pxelinux.0</filename>
を渡してください。運がよければすべてがうまく動作するでしょう。
詳細な説明に関しては、<xref linkend="install-tftp" />をご覧ください。

</para>
  </sect2>

  <sect2 id="howto-getting-images-hard-disk">
<!--
  <title>Booting from hard disk</title>
-->
  <title>ハードディスクからの起動</title>
<para>

<!--
It's possible to boot the installer using no removable media, but just an
existing hard disk, which can have a different OS on it. Download
<filename>hd-media/initrd.gz</filename>, <filename>hd-media/vmlinuz</filename>,
and a Debian CD image to the top-level directory of the hard disk. Make sure
that the CD image has a filename ending in <literal>.iso</literal>. Now
it's just a matter of booting linux with the initrd.
-->
リムーバブルメディアを使用せずに、単に既存のハードディスク (そこに異なる OS があっても
構いません) を使ってインストーラを起動することができます。
<filename>hd-media/initrd.gz</filename>、<filename>hd-media/vmlinuz</filename>
および Debian CD イメージをハードディスクの一番上のディレクトリにダウンロードしてください。
CD イメージのファイル名が <literal>.iso</literal> で終わっていることを確かめてください。
それは initrd を備えた linux の起動にかかわる問題です。
<phrase arch="x86">
<!--
<xref linkend="boot-initrd" /> explains one way to do it.
-->
<xref linkend="boot-initrd" />では、その方法を説明しています。
</phrase>

</para>
  </sect2>
 </sect1>

 <sect1 id="howto-installation">
<!--
<title>Installation</title>
-->
<title>インストール</title>
<para>

<!--
Once the installer starts, you will be greeted with an initial screen. Press
&enterkey; to boot, or read the instructions for other boot
methods and parameters (see <xref linkend="boot-parms" />).
-->
インストーラが立ち上がるとすぐに、歓迎の初期画面が表示されます。
起動するために &enterkey; を押すか、他の起動方法やパラメータのための説明を
読んでください (<xref linkend="boot-parms" />をご覧ください)。

</para><para>

<!--
After a while you will be asked to select your language. Use the arrow keys
to pick a language and press &enterkey; to continue. Next you'll be asked to
select your country, with the choices including countries where your
language is spoken. If it's not on the short list, a list of all the
countries in the world is available.
-->
しばらくして、言語を選択するための質問がされます。矢印キーを使って言語を選び、
継続するために &enterkey; を押してください。次に、その言語が話される国々を含む
選択肢が表示され、国を選択するよう質問されます。
短いリスト上にはない場合は、世界中のすべての国のリストから選択できます。

</para><para>

<!--
You may be asked to confirm your keyboard layout. Choose the default unless
you know better.
-->
キーボードレイアウトを確認するよう尋ねられるかもしれません。もしよく分からなければ、
デフォルトを選択してください。

</para><para>

<!--
Now sit back while debian-installer detects some of your hardware, and
loads the rest of itself from CD, floppy, USB, etc.
-->
debian-installer がハードウェアの一部を検知し、CD やフロッピー、あるいは
USB などからインストーラの残りの部分をロードする間、くつろいでいてください。

</para><para>

<!--
Next the installer will try to detect your network hardware and set up
networking by DHCP. If you are not on a network or do not have DHCP, you
will be given the opportunity to configure the network manually.
-->
次にインストーラは、ネットワークハードウェアを検知し、DHCP によって
ネットワークの設定をしようとします。ネットワーク上にないか、
DHCP が無い場合は、ネットワークを手動で設定する機会が与えられます。

</para><para>

<!--
Now it is time to partition your disks. First you will be given the
opportunity to automatically partition either an entire drive, or available
free space on a drive (guided partitioning).
This is recommended for new users or anyone in a hurry. If you do not want
to autopartition, choose <guimenuitem>Manual</guimenuitem> from the menu.
-->
さあ、ディスクのパーティションを分割しましょう。最初に、ドライブのすべてか、
またはドライブの利用可能な空き領域を自動的にパーティション分割するか
選択する機会が与えられます (ガイドによるパーティショニング)。
これは新規ユーザや急いでいる誰にでも勧められます。自動分割をしたくない場合は、
メニューから <guimenuitem>手動</guimenuitem> を選んでください。

</para><para arch="x86">

<!--
If you have an existing DOS or Windows partition that you want to preserve,
be very careful with automatic partitioning. If you choose manual partitioning,
you can use the installer to resize existing FAT or NTFS partitions to create
room for the Debian install: simply select the partition and specify its new size.
-->
失いたくない既存の DOS か Windows パーティションがあるなら、
パーティションの自動分割には充分に注意してください。手動分割を選択すれば、Debian
をインストールする場所を作るために既存の FAT あるいは NTFS パーティションを
サイズ変更するようにインストーラが使えます。単にパーティションを選択して、
新しいサイズを指定してください。

</para><para>

<!--
On the next screen you will see your partition table, how the partitions will
be formatted, and where they will be mounted. Select a partition to modify or
delete it. If you did automatic partitioning, you should just be able to choose
<guimenuitem>Finish partitioning and write changes to disk</guimenuitem>
from the menu to use what it set up. Remember to assign at least one partition
for swap space and to mount a partition on <filename>/</filename>.
<xref linkend="partitioning" /> has more information about partitioning.
-->
次の画面でパーティションテーブル (パーティションをどうフォーマットするか、それを
どこにマウントするか) を見ることになります。修正や削除をするためには、パーティション
を選択してください。もし自動パーティション分割を行っていれば、設定したものを使用する
メニューから、<guimenuitem>パーティショニングの終了とディスクへの変更の書き込み</guimenuitem> 
で決定できます。スワップスペースのために少なくとも 1 つのパーティションを割り当てることと
<filename>/</filename> にパーティションをマウントすることを忘れないようにしてください。
<xref linkend="partitioning" />にパーティション分割に関するもっと多くの情報があります。

</para><para>

<!--
Now &d-i; formats your partitions and starts to install the base system,
which can take a while. That is followed by installing a kernel.
-->
それから &d-i; はパーティションをフォーマットし、ベースシステムのインストール
(時間がかかることがあります) を始めます。続いてカーネルがインストールされます。

</para><para>

<!--
The next steps are setting up your time zone and clock. The installer will
try to select the correct settings automatically and will only ask if it
cannot. This is followed by setting up user accounts. By default you will
need to provide a password for the <quote>root</quote> (administrator)
account and information necessary to create one regular user account.
-->
次のステップは、タイムゾーンと時計の設定です。
インストーラは正しい設定を自動で選択し、不可能な場合のみ問い合わせてきます。
続いて、ユーザアカウントの設定を行います。
デフォルトでは、<quote>root</quote> (管理者) アカウントのパスワードを設定し、
通常ユーザのアカウントを 1 つ作成することになります。

</para><para>

<!--
The base system that was installed earlier is a working, but very minimal
installation. To make the system more functional the next step allows you
to install additional packages by selecting tasks. Before packages can be
installed <classname>apt</classname> needs to be configured as that defines
from where the packages will be retrieved.
The <quote>Standard system</quote> task will be selected by default and
should normally be installed. Select the <quote>Desktop environment</quote>
task if you would like to have a graphical desktop after the installation.
See <xref linkend="pkgsel"/> for additional information about this step.
-->
最初にインストールしたベースシステムでも動作はしますが、
最低限のものしかインストールされていません。
もっと機能的にするには、次のステップでタスクを選択し、
追加パッケージをインストールしてください。
なお、パッケージをインストールする前に、
パッケージをどこから取得してインストールするかの定義を、
<classname>apt</classname> に設定する必要があります。
<quote>標準システム</quote> タスクはデフォルトで選択され、
通常は既にインストールされているはずです。
インストール後にグラフィカルデスクトップが必要であれば、
<quote>デスクトップ環境</quote> を選択してください。
このステップについてのさらなる情報は、<xref linkend="pkgsel"/> をご覧ください。

</para><para>

<!--
The last step is to install a boot loader. If the installer detects
other operating systems on your computer, it will add them to the boot menu
and let you know.
-->
最後の段階はブートローダをインストールすることです。コンピュータ上に他の
オペレーティングシステムを検出した場合は、インストーラがブートメニューに
それらを加えて知らせます。
<!--
<phrase arch="x86">By default GRUB will be installed to the master boot
record of the first harddrive, which is generally a good choice. You'll be
given the opportunity to override that choice and install it elsewhere.
-->
<phrase arch="x86">GRUB は、デフォルトで第 1 ハードドライブの
マスターブートレコードにインストールされ、一般にそれは良い選択です。
その選択を無効にして他の場所にインストールする機会が与えられます。
</phrase>

</para><para>

<!--
&d-i; will now tell you that the installation has
finished. Remove the cdrom or other boot media and hit &enterkey; to reboot
your machine. It should boot up into the newly installed system and
allow you to log in. This is explained in <xref linkend="boot-new"/>.
-->
次に &d-i; は、インストールが終了したことを伝えます。
CD-ROM やその他の起動メディアを取り出して、マシンを再起動するために &enterkey;
を叩いてください。
新しくインストールしたシステムが起動し、ログインできるはずです。
これは <xref linkend="boot-new"/> で説明しています。

</para><para>

<!--
If you need more information on the install process, see
<xref linkend="d-i-intro" />.
-->
インストール手順についてもっと多くの情報が必要ならば、
<xref linkend="d-i-intro" />をご覧ください。

</para>
 </sect1>

 <sect1 id="howto-installation-report">
<!--
 <title>Send us an installation report</title>
-->
 <title>インストールレポートを送ってください</title>
<para>

<!--
If you successfully managed an installation with &d-i;,
please take time to provide us with a report.
The simplest way to do so is to install the reportbug package
(<command>aptitude install reportbug</command>), configure
<classname>reportbug</classname> as explained in
<xref linkend="mail-outgoing"/>, and run
<command>reportbug installation-reports</command>.
-->
&d-i; で首尾よくインストールをやり遂げられたならば、
レポート提出のためにしばらく時間をかけてください。
reportbug パッケージをインストールして
(<command>aptitude install reportbug</command>)、
<xref linkend="mail-outgoing"/> の説明にあるよう
に <classname>reportbug</classname> を設定し、
<command>reportbug installation-reports</command> と
実行するのが最も簡単な方法です。

</para><para>

<!--
If you did not complete the install, you probably found a bug in
debian-installer. To improve the installer it is necessary that we know
about them, so please take the time to report them. You can use an
installation report to report problems; if the install completely fails,
see <xref linkend="problem-report" />.
-->
もしインストールが完了しなかったのならば、おそらく debian-installer の
バグを発見しました。インストーラを改善するためには、私たちがそれらについて
知っていることが必要ですので、バグ報告するための時間をとってください。
問題を報告するためにはインストールレポートが使用できます。インストールが
完全に失敗する場合は、<xref linkend="problem-report" />をご覧ください。

</para>
 </sect1>

 <sect1 id="howto-installation-finally">
<!--
 <title>And finally&hellip;</title>
-->
 <title>そして最後に&hellip;</title>
<para>

<!--
We hope that your Debian installation is pleasant and that you find Debian
useful. You might want to read <xref linkend="post-install" />.
-->
Debian のインストールが快適であり、Debian が役に立つことに気づいていただければと思います。
<xref linkend="post-install" />を読みたいと思ったかもしれません。

</para>
 </sect1>
</appendix>
