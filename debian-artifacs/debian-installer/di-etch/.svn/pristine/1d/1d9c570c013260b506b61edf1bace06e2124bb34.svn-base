<!-- original version: 44026 -->

   <sect3 id="netcfg">
   <title>Het netwerk configureren</title>

<para>

Als het systeem, wanneer u start met deze stap, detecteert dat u meer
dan één netwerkkaart heeft, zal u worden gevraagd welke van de apparaten
u wilt gebruiken voor uw primaire netwerkinterface, dat wil zeggen welke
u wilt gebruiken voor de installatie. U kunt de overige interfaces
configureren nadat de installatie is voltooid; zie hiervoor de man pagina
<citerefentry> <refentrytitle>interfaces</refentrytitle>
<manvolnum>5</manvolnum> </citerefentry>.

</para><para>

Tijdens een standaardinstallatie zal &d-i; proberen uw netwerkinterface
automatisch met behulp van DHCP in te stellen. Er zijn verschillende
redenen waarom dit zou kunnen falen, variërend van het niet aangesloten
zijn van de netwerkkabel tot een onjuiste DHCP-configuratie. Ook is het
mogelijk dat er helemaal geen DHCP-server in uw lokale netwerk aanwezig
is. Voor nadere diagnose kunt u de boodschappen op de vierde console
raadplegen. In elk geval zal u worden gevraagd of u een nieuwe poging wilt
wagen of dat u het netwerk handmatig wilt configureren. DHCP-servers
kunnen soms zeer traag zijn; probeer het dus nogmaals als u zeker weet dat
alles juist is ingesteld.

</para><para>

Bij handmatige configuratie van de netwerkinterface, zal u een aantal
vragen worden gesteld over uw netwerk, te weten:
<computeroutput>IP-adres</computeroutput>,
<computeroutput>Netwerkmasker</computeroutput>,
<computeroutput>Gateway</computeroutput>,
<computeroutput>Adressen van naamservers</computeroutput>, en
<computeroutput>Computernaam</computeroutput>.
Als u beschikt over een draadloos netwerkapparaat, zal u worden gevraagd
naar de <computeroutput>ESSID</computeroutput> van uw draadloos netwerk
en de <computeroutput>WEP sleutel</computeroutput>. Voer de waarden in
die u heeft verzameld bij <xref linkend="needed-info"/>.

</para><note><para>

De volgende technische details zouden van pas kunnen komen (of niet).
Het programma gaat ervan uit dat het IP-adres van uw netwerk het resultaat
is van een bit-gewijze AND-operatie van het IP-adres van uw systeem en het
netwerkmasker. Het Broadcast-adres wordt ingesteld op het resultaat van een
bit-gewijze OR-operatie van het IP-adres van uw systeem en de bitgewijze
inverse van het netwerkmasker. Gebruik de staandaardwaarden van het
installatiesysteem als u de juiste waarden voor deze vragen niet kunt vinden;
u kunt ze, indien nodig, altijd nog wijzigen nadat de installatie is afgerond
door het bestand <filename>/etc/network/interfaces</filename> te wijzigen.

</para></note>
   </sect3>
