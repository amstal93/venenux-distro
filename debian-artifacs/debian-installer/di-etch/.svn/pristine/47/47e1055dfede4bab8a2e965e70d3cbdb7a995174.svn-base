<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 42302 -->

   <sect3 id="kbd-chooser">
   <title>キーボード選択</title>

<para>

<!--
Keyboards are often tailored to the characters used in a language.
Select a layout that conforms to the keyboard you are using, or
select something close if the keyboard layout you want
isn't represented. Once the system installation is complete, you'll be
able to select a keyboard layout from a wider range of choices (run
<command>kbdconfig</command> as root after you have completed the
installation).
-->
キーボードは、しばしば言語で使用する文字に合わされています。
使用しているキーボードに一致するレイアウトを選択するか、
希望のキーボードレイアウトが表示されなければ、近いものを選択してください。
いったんシステムのインストールが完了すれば、
もっと広い範囲からキーボードレイアウトを選ぶことができます。
(インストールが完了した後に、root で <command>kbdconfig</command> を
実行してください)

</para><para>

<!--
Move the highlight to the keyboard selection you desire and press
&enterkey;.  Use the arrow keys to move the highlight &mdash; they are
in the same place in all national language keyboard layouts, so they
are independent of the keyboard configuration. An 'extended' keyboard
is one with <keycap>F1</keycap> through <keycap>F10</keycap> keys
along the top row.
-->
希望のキーボードにハイライトを移動させて、&enterkey; を押してください。
ハイライトの移動には矢印キーを使用してください。
どの言語のキーボードでも同じ場所にあるため、キーボードの設定に依存しません。
「拡張」キーボードとは、
最上段に <keycap>F1</keycap> から <keycap>F10</keycap> キーを備えたものです。

</para><para arch="mipsel">

<!--
On DECstations there is currently no loadable keymap available,
so you have to skip the keyboard selection and keep the default
kernel keymap (LK201 US). This may change in the future as it
depends on further Linux/MIPS kernel development.
-->
現在 DECstation では、利用できるロード可能キーマップがありません。
そのため、キーボードの選択をスキップして、
デフォルトのカーネルキーマップ (LK201 US) を保持する必要があります。
Linux/MIPS カーネル開発に依存して、これは今後変わるかもしれません。


</para><para arch="powerpc">

<!--
There are two keyboard layouts for US keyboards; the qwerty/mac-usb-us
(Apple USB) layout will place the Alt function on the
<keycap>Command/Apple</keycap> key (in the keyboard position next to
the <keycap>space</keycap> key similar to <keycap>Alt</keycap> on
PC keyboards), while the qwerty/us (Standard) layout will place the
Alt function on the <keycap>Option</keycap> key (engraved with 'alt'
on most Mac keyboards). In other respects the two layouts are similar.
-->
英語キーボードには 2 種類のキーボードレイアウトがあります。
qwerty/mac-usb-us (Apple USB) レイアウトは、Alt 機能が
(PCキーボードの <keycap>Alt</keycap> に似た
<keycap>space</keycap> キーの隣にある) <keycap>Command/Apple</keycap> キー
に割り当てられています。
qwerty/us (Standard) レイアウトでは、Alt 機能が
(多くの Mac キーボードでは 'alt' も刻印されている) <keycap>Option</keycap> キー
に割り当てられています。
その他の点では この 2 つのレイアウトはよく似ています。

</para>
   </sect3>
