<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 36732 -->

  <sect2 arch="mipsel">
<title>Microprocesseurs, cartes m�re et cartes vid�o</title>

<para>
	
Debian sur &arch-title; g�re les plateformes suivantes&nbsp;:

<itemizedlist>
<listitem><para>
		
DECstation&nbsp;: diff�rents mod�les de DECstation sont reconnus.

</para></listitem>
<listitem><para>

Cobalt Microserver&nbsp;: seules les machines Cobalt bas�es sur 
MIPS seront trait�es ici. Cela inclut les plateformes Cobalt Qube 2700 (Qube1),
RaQ, Qube2 et RaQ2, et la passerelle Microserver.

</para></listitem>
<listitem><para>

Broadcom BCM91250A (SWARM)&nbsp;: 'est une carte d'�valuation au format ATX de
Broadcom bas� sur la famille de leur processeur SB1 1250.

</para></listitem>
<listitem><para>

Broadcom BCM91480B (BigSur) : c'est une carte d'�valuation au format ATX de
Broadcom bas� sur la famille de leur processeur SB1A 1480.

</para></listitem>
</itemizedlist>

Une information compl�te � propos des machines mips/mipsel se trouve dans la 
<ulink url="&url-mips-howto;">page Linux-MIPS</ulink>. Dans la suite, seuls les
syst�mes g�r�s par l'installateur Debian seront trait�s. Si vous recherchez de 
l'aide pour d'autres sous-architectures, vous pouvez contacter la 
<ulink url="&url-list-subscribe;">liste de diffusion debian-&arch-listname;</ulink>.

</para>

   <sect3><title>Microprocesseur/types de machine</title>

<para>
Actuellement seules les DECstations utilisant les processeurs R3000 
et R4000/R4400 sont reconnues par le syst�me d'installation de Debian 
pour MIPS petit-boutien. Le syst�me d'installation de Debian fonctionne
sur les machines suivantes&nbsp;:
</para>

<para>
<informaltable>
<tgroup cols="4">
<thead>
<row>
  <entry>Type syst�me</entry><entry>CPU</entry><entry>Nom de code</entry>
  <entry>sous-architecture Debian</entry>
</row>
</thead>

<tbody>
<row>
  <entry>DECstation 5000/1xx</entry>
  <entry>R3000</entry>
  <entry>3MIN</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/150</entry>
  <entry>R4000</entry>
  <entry>3MIN</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>DECstation 5000/200</entry>
  <entry>R3000</entry>
  <entry>3MAX</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/240</entry>
  <entry>R3000</entry>
  <entry>3MAX+</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>DECstation 5000/260</entry>
  <entry>R4400</entry>
  <entry>3MAX+</entry>
  <entry>r4k-kn04</entry>
</row><row>
  <entry>Personal DECstation 5000/xx</entry>
  <entry>R3000</entry>
  <entry>Maxine</entry>
  <entry>r3k-kn02</entry>
</row><row>
  <entry>Personal DECstation 5000/50</entry>
  <entry>R4000</entry>
  <entry>Maxine</entry>
  <entry>r4k-kn04</entry>
</row>
</tbody></tgroup></informaltable>

</para><para>

Toutes les machines Cobalt sont reconnues. Auparavant seules les machines avec
une console s�rie �taient reconnues, c'est-�-dire toutes les machines sauf
Qube 2700, ou Qube1. Cependant on peut faire une installation par ssh.

</para><para>

La carte Broadcom BCM91250A avec une puce SB1 1250 est reconnue en mode SMP
par l'installateur.
De m�me la carte BCM91480B avec une puce SB1A 1480 est reconnue en mode SMP.
</para>
   </sect3>

   <sect3>
<title>Options pour consoles</title>

<para>
	
La console s�rie est disponible sur toutes les DECstations reconnues 
(9600 bps, 8N1). Pour utiliser une console s�rie, vous devez d�marrer l'image 
d'installation en passant l'option <literal>console=ttyS</literal><replaceable>x</replaceable> 
(<replaceable>x</replaceable> �tant le num�ro du port s�rie auquel est connect� le terminal, 
g�n�ralement <literal>2</literal>, mais <literal>0</literal> pour les Personal DECstations). Sur 
3MIN et 3MAX+ (DECstation 5000/1xx, 5000/240 et 5000/260), la console
locale est disponible avec les options graphiques PMAG-BA et PMAGB-B.

</para><para>

Si vous poss�dez un syst�me Linux avec un terminal s�rie, une fa�on simple de 
travailler est d'ex�cuter <command>cu</command> <footnote> 
<para>
Dans Woody, cette commande faisait partie du paquet 
<classname>uucp</classname>, mais elle est maintenant dans un paquet distinct.
</para></footnote>.
Exemple&nbsp;:

<informalexample><screen>
$ cu -l /dev/ttyS1 -s 9600
</screen></informalexample>

o� l'option <literal>-l</literal> (line) repr�sente le port s�rie � utiliser et 
<literal>-s</literal> (speed) la vitesse de transmission (9600 bits par seconde).

</para><para>

les plateformes Cobalt et Broadcom BCM91250A et BCM91480B utilisent une vitesse de 
transmission de 115200 bps.

</para>

   </sect3>
  </sect2>
