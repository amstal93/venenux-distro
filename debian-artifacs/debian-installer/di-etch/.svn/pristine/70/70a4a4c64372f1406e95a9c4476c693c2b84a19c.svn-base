<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43923 -->

  <sect2 arch="powerpc" id="boot-cd"><title>CD-ROM からの起動</title>

&boot-installer-intro-cd.xml;

<para>

<!--
Currently, the only &arch-title; subarchitectures that support CD-ROM
booting are PReP (though not all systems) and New World PowerMacs.
On PowerMacs, hold the <keycap>c</keycap> key, or else the combination of
<keycap>Command</keycap>, <keycap>Option</keycap>,
<keycap>Shift</keycap>, and <keycap>Delete</keycap>
keys together while booting to boot from the CD-ROM.
-->
今のところ、CD-ROM からの起動をサポートしている
&arch-title; のサブアーキテクチャは
PReP (でもすべてのシステムではありません) と New World PowerMac だけです。
PowerMac では、<keycap>c</keycap> キーを押しっぱなしにするか、
または <keycap>Command</keycap>, <keycap>Option</keycap>,
<keycap>Shift</keycap>, <keycap>Delete</keycap> キーを起動時に同時に押せば、
CD-ROM から起動できます。

</para><para>

<!--
OldWorld PowerMacs will not boot a Debian CD, because OldWorld
computers relied on a Mac OS ROM CD boot driver to be present on the CD,
and a free-software version of this driver is not available. All
OldWorld systems have floppy drives, so use the floppy drive to launch
the installer, and then point the installer to the CD for the needed
files.
-->
OldWorld PowerMac では、Debian CD が起動しないでしょう。なぜなら OldWorld
コンピュータは、CD を利用するのに Mac OS ROM CD ブートドライバに依存し、
このドライバにはフリーソフトウェア版が存在しないからです。すべての
OldWorld システムには、フロッピードライブがあるので、インストーラを
開始するためにフロッピードライブを使い、必要とされたファイルのために CD を
指定してください。

</para><para>

<!--
If your system doesn't boot directly from CD-ROM, you can still use
the CD-ROM to install the system. On NewWorlds, you can also use an
OpenFirmware command to boot from the CD-ROM manually. Follow the
instructions in <xref linkend="boot-newworld"/> for booting from
the hard disk, except use the path to <command>yaboot</command> on the
CD at the OF prompt, such as
-->
CD-ROM からの直接起動をサポートしていないシステムでも、
インストールに CD-ROM を用いることは可能です。
NewWorld では、OpenFirmware のコマンドを用いて、
手動で CD-ROM から起動させることもできます。
ハードディスクから起動させる場合は
<xref linkend="boot-newworld"/> の指示に従ってください。
ただし OF プロンプトで与える CD の <command>yaboot</command> へのパスは、
以下のようにしてください。

<informalexample><screen>
0 &gt; boot cd:,\install\yaboot
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="powerpc" id="install-drive">
  <title>ハードディスクからの起動</title>

&boot-installer-intro-hd.xml;

  <sect3><title>OpenFirmware からの CHRP の起動</title>

<para>

<!--
  <emphasis>Not yet written.</emphasis>
-->
  <emphasis>まだ書いていません。</emphasis>
</para>
   </sect3>

   <sect3><title>MacOS からの OldWorld PowerMac の起動</title>
<para>

<!--
If you set up BootX in <xref linkend="files-oldworld"/>, you can
use it to boot into the installation system. Double click the
<guiicon>BootX</guiicon> application icon. Click on the
<guibutton>Options</guibutton> button and select <guilabel>Use
Specified RAM Disk</guilabel>. This will give you the
chance to select the <filename>ramdisk.image.gz</filename> file. You
may need to select the <guilabel>No Video Driver</guilabel> checkbox,
depending on your hardware. Then click the
<guibutton>Linux</guibutton> button to shut down MacOS and launch the
installer.
-->
<xref linkend="files-oldworld"/> で BootX を設定済みなら、
それを使ってインストールシステムを起動できます。
<guiicon>BootX</guiicon> のアプリケーションアイコンをダブルクリックしてください。
<guibutton>Options</guibutton> ボタンをクリックし、
<guilabel>Use Specified RAM Disk</guilabel> を選んでください。
すると <filename>ramdisk.image.gz</filename> ファイルを選択できるようになります。
ハードウェアによっては、
<guilabel>No Video Driver</guilabel> チェックボックスを選ばなければならないかもしれません。
最後に <guibutton>Linux</guibutton> ボタンをクリックすると、MacOS がシャットダウンして、
インストーラが起動します。

</para>
   </sect3>


  <sect3 id="boot-newworld">
  <title>OpenFirmware からの NewWorld Mac の起動</title>
<para>

<!--
You will have already placed the <filename>vmlinux</filename>,
<filename>initrd.gz</filename>, <filename>yaboot</filename>, and
<filename>yaboot.conf</filename> files at the root level of your HFS
partition in <xref linkend="files-newworld"/>.
Restart the computer, and immediately (during the chime) hold down the
<keycap>Option</keycap>, <keycap>Command (cloverleaf/Apple)</keycap>,
<keycap>o</keycap>, and <keycap>f</keycap> keys all together. After
a few seconds you will be presented with the Open Firmware prompt.
At the prompt, type
-->
既に <xref linkend="files-newworld"/> において、
<filename>vmlinux</filename>,
<filename>initrd.gz</filename>, <filename>yaboot</filename>,
<filename>yaboot.conf</filename> といったファイルを、
HFS パーティションの root レベルに配置したことと思います。
コンピュータを再起動し、ただちに (チャイムが鳴っているあいだに)
<keycap>Option</keycap>, <keycap>Command (クローバー/Apple)</keycap>,
<keycap>o</keycap>, <keycap>f</keycap> の各キーを同時に押します。
数秒間たつと、OpenFirmware のプロンプトが表示されます。
プロンプトから次のように入力します。

<informalexample><screen>

0 > boot hd:<replaceable>x</replaceable>,yaboot

</screen></informalexample>

<!--
replacing <replaceable>x</replaceable> with the partition number of
the HFS partition where the
kernel and yaboot files were placed, followed by a &enterkey;. On some
machines, you may need to use <userinput>ide0:</userinput> instead of
<userinput>hd:</userinput>. In a few more seconds you will see a
yaboot prompt
-->
<replaceable>x</replaceable> はカーネルと yaboot ファイルを置いた
HFS パーティションの番号に置き換えてください。
入力したら &enterkey; を押します。
いくつかのマシンでは、
<userinput>hd:</userinput> の代わりに <userinput>ide0:</userinput>
を指定する必要があるかもしれません。さらに何秒か待つと、
yaboot のプロンプトが表示されます。

<informalexample><screen>
boot:
</screen></informalexample>

<!--
At yaboot's <prompt>boot:</prompt> prompt, type either
<userinput>install</userinput> or <userinput>install video=ofonly</userinput>
followed by a &enterkey;.  The
<userinput>video=ofonly</userinput> argument is for maximum
compatibility; you can try it if <userinput>install</userinput>
doesn't work. The Debian installation program should start.
-->
Yaboot の <prompt>boot:</prompt> プロンプトで、
<userinput>install</userinput> か <userinput>install video=ofonly</userinput>
と入力し、&enterkey; を押します。
<userinput>install</userinput> の方がうまくいかないときは、
互換性を最大にするために <userinput>video=ofonly</userinput> 引数を
試してみてください。
これで Debian のインストールプログラムが開始されるはずです。

</para>
   </sect3>
  </sect2>

  <sect2 arch="powerpc" condition="bootable-usb" id="usb-boot">
  <title>USB メモリからの起動</title>
<para>

<!--
Currently, NewWorld PowerMac systems are known to support USB booting.
-->
現在、NewWorld PowerMac は USB からの起動をサポートしています。

</para>

<para>

<!--
Make sure you have prepared everything from <xref
linkend="boot-usb-files"/>. To boot a Macintosh system from a USB stick,
you will need to use the Open Firmware prompt, since Open Firmware does
not search USB storage devices by default.-->
<!-- TODO: although it could be made to; watch this space -->
<!--
To get to the prompt, hold down
<keycombo><keycap>Command</keycap> <keycap>Option</keycap>
<keycap>o</keycap> <keycap>f</keycap></keycombo> all together while
booting (see <xref linkend="invoking-openfirmware"/>).
-->
すでに <xref linkend="boot-usb-files"/> にあるすべての準備が、
終わっていることと思います。
USB メモリから Macintosh システムを起動するには、
Open Firmware は標準で USB ストレージデバイスを検索しませんので、
Open Firmware プロンプトを使う必要があります。
<!-- TODO: 書きましたが、確認してください (訳注 必要ないかな) -->
プロンプトを表示するには、
<keycombo><keycap>Command</keycap> <keycap>Option</keycap>
<keycap>o</keycap> <keycap>f</keycap></keycombo> を
起動中ずっと押したままにしておきます。
(<xref linkend="invoking-openfirmware"/> を参照)

</para><para>

<!--
You will need to work out where the USB storage device appears in the
device tree, since at the moment <command>ofpath</command> cannot work
that out automatically. Type <userinput>dev / ls</userinput> and
<userinput>devalias</userinput> at the Open Firmware prompt to get a
list of all known devices and device aliases. On the author's system
with various types of USB stick, paths such as
<filename>usb0/disk</filename>, <filename>usb0/hub/disk</filename>,
<filename>/pci@f2000000/usb@1b,1/disk@1</filename>, and
<filename>/pci@f2000000/usb@1b,1/hub@1/disk@1</filename> work.
-->
今のところ <command>ofpath</command> は自動でやってはくれませんので、
自分で USB ストレージをデバイスツリーに出す必要があります。 
Open Firmware プロンプトで、
<userinput>dev / ls</userinput> や <userinput>devalias</userinput> と入力し、
認識しているすべてのデバイスとデバイスエイリアスを取得してください。
筆者のシステムでは、いろいろな USB メモリが
<filename>usb0/disk</filename>, <filename>usb0/hub/disk</filename>,
<filename>/pci@f2000000/usb@1b,1/disk@1</filename>,
<filename>/pci@f2000000/usb@1b,1/hub@1/disk@1</filename>
として見えています。

</para><para>

<!--
Having worked out the device path, use a command like this to boot the
installer:
-->
デバイスパスを取得できたら、
以下のようなコマンドでインストーラを起動してください。

<informalexample><screen>
boot <replaceable>usb0/disk</replaceable>:<replaceable>2</replaceable>,\\:tbxi
</screen></informalexample>

<!--
The <replaceable>2</replaceable> matches the Apple_HFS or
Apple_Bootstrap partition onto which you copied the boot image earlier,
and the <userinput>,\\:tbxi</userinput> part instructs Open Firmware to
boot from the file with an HFS file type of "tbxi" (i.e.
<command>yaboot</command>) in the directory previously blessed with
<command>hattrib -b</command>.
-->
<replaceable>2</replaceable> には、はじめに起動イメージをコピーした 
Apple_HFS・Apple_Bootstrap パーティションが該当します。
<userinput>,\\:tbxi</userinput> の部分は、
事前に <command>hattrib -b</command> をしておいたディレクトリにある、
"tbxi" という HFS ファイルタイプのファイル (例: <command>yaboot</command>) 
から起動するということを Open Firmware に指示しています。

</para><para>

<!--
The system should now boot up, and you should be presented with the
<prompt>boot:</prompt> prompt. Here you can enter optional boot
arguments, or just hit &enterkey;.
-->
システムが起動し、<prompt>boot:</prompt> が表示されることと思います。
ここで追加の起動引数を入力するか、単に &enterkey; を押してください。

</para><warning><para>

<!--
This boot method is new, and may be difficult to get to work on some
NewWorld systems. If you have problems, please file an installation
report, as explained in <xref linkend="submit-bug"/>.
-->
この起動法は新しく、
いくつかの NewWorld システムでは動作させるのが難しいかもしれません。
問題が発生したら、<xref linkend="submit-bug"/> にあるように、
インストールレポートを出してください。

</para></warning>
  </sect2>

  <sect2 arch="powerpc" id="boot-tftp"><title>TFTP での起動</title>

&boot-installer-intro-net.xml;

<para>

<!--
Currently, PReP and New World PowerMac systems support netbooting.
-->
現在、PReP と New World PowerMac システムではネットブートをサポートしています。

</para><para>

<!--
On machines with Open Firmware, such as NewWorld Power Macs, enter the
boot monitor (see <xref linkend="invoking-openfirmware"/>) and
use the command <command>boot enet:0</command>.  PReP and CHRP boxes
may have different ways of addressing the network.  On a PReP machine,
you should try
<userinput>boot net:<replaceable>server_ipaddr</replaceable>,<replaceable>file</replaceable>,<replaceable>client_ipaddr</replaceable></userinput>.
On some PReP systems (e.g. Motorola PowerStack machines) the command
<userinput>help boot</userinput> may give a description of syntax and
available options.
-->
NewWorld Power Mac のような OpenFirmware のマシンでは、
ブートモニタに入り
(<xref linkend="invoking-openfirmware"/> を参照)、
<command>boot enet:0</command> というコマンドを使います。
PReP や CHRP のマシンでは、ネットワークの指定が異なるかもしれません。
PReP マシンでは、
<userinput>boot net:<replaceable>server_ipaddr</replaceable>,<replaceable>file</replaceable>,<replaceable>client_ipaddr</replaceable></userinput>
を試してみてください。
いくつかの PReP システム (例: Motorola PowerStack マシン) では、
<userinput>help boot</userinput> コマンドで、
文法と有効なオプションの説明が得られます。

</para>
  </sect2>


  <sect2 arch="powerpc" condition="supports-floppy-boot">
  <title>フロッピーからの起動</title>
<para>

<!--
Booting from floppies is supported for &arch-title;, although it is
generally only applicable for OldWorld systems. NewWorld systems are
not equipped with floppy drives, and attached USB floppy drives are
not supported for booting.
-->
&arch-title; はフロッピーからの起動をサポートしていますが、
用いることができるのは通常 OldWorld システムだけです。
NewWorld システムには、フロッピードライブがついておらず、
起動時には外付け USB フロッピードライブをサポートしていません。

</para><para>

<!--
You will have already downloaded the floppy images you needed and
created floppies from the images in <xref linkend="create-floppy"/>.
-->
おそらく <xref linkend="create-floppy"/> において
必要なフロッピーイメージは既にダウンロードし、
それらのイメージからフロッピーは作成済みかと思います。

</para><para>

<!--
To boot from the <filename>boot-floppy-hfs.img</filename> floppy,
place it in floppy drive after shutting the system down, and before
pressing the power-on button.
-->
<filename>boot-floppy-hfs.img</filename> フロッピーから起動するには、
システムをシャットダウンしたあとにフロッピーをドライブに入れ、
パワーオンボタンを押してください。

</para><note><para>
<!--
For those not familiar with Macintosh
floppy operations: a floppy placed in the machine prior to boot will
be the first priority for the system to boot from. A floppy without a
valid boot system will be ejected, and the machine will then check for
bootable hard disk partitions.
-->
Macintosh のフロッピー操作に慣れていない方へ: 
フロッピーがシステムの起動順で最優先されるには、
起動する前にフロッピーを挿入してください。
有効なブートシステムのないフロッピーは排出されてしまい、
ブート可能なハードディスクのパーティションをチェックします。

</para></note><para>

<!--
After booting, the <filename>root.bin</filename> floppy is
requested. Insert the root floppy and press &enterkey;. The installer
program is automatically launched after the root system has been
loaded into memory.
-->
起動すると、<filename>root.bin</filename> フロッピーが要求されます。
root フロッピーを入れて &enterkey; を押してください。
root システムがメモリにロードされると、
インストーラプログラムが自動的に起動します。

</para>
  </sect2>


  <sect2 arch="powerpc"><title>PowerPC ブートパラメータ</title>
<para>

<!--
Many older Apple monitors used a 640x480 67Hz mode. If your video
appears skewed on an older Apple monitor, try appending the boot
argument <userinput>video=atyfb:vmode:6</userinput> , which will
select that mode for most Mach64 and Rage video hardware. For Rage 128
hardware, this changes to
<userinput>video=aty128fb:vmode:6</userinput> .
-->
多くの古い Apple モニタは、640x480 67Hz モードを使用します。
古い Apple モニタで画面がゆがむ場合、
多くの Mach64・Rage ビデオハードウェアでモードを選択するには、
ブート引数に <userinput>video=atyfb:vmode:6</userinput> を試してみてください。
Rage 128 ハードウェアならこれを <userinput>video=aty128fb:vmode:6</userinput> 
に変えてください。

</para>
  </sect2>
