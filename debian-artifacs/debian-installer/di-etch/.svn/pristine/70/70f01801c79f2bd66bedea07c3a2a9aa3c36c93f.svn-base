<!-- retain these comments for translator revision tracking -->
<!-- original version: 43730 -->
<!-- revised by Herbert Parentes Fortes Neto (hpfn) 2006.05.02 -->
<!-- revised by Felipe Augusto van de Wiel (faw) 2006.06.03 -->
<!-- updated by Felipe Augusto van de Wiel (faw) 2007.01.14 -->

   <sect3 arch="x86">
   <title>Particionamento de memória stick USB na &arch-title;</title>
<para>

Nós iremos mostrar como configurar uma memória stick para usar a primeira
partição ao invés de todo dispositivo.

</para><note><para>

Como a maioria dos dispositivos stick USB vêm com uma partição
contendo um sistema de arquivos FAT16 já configurada, você provavelmente
não precisará reparticionar ou reformatar o stick. Se tiver que fazer
isto de qualquer forma, use o <command>cfdisk</command> ou qualquer
outra ferramenta de particionamento para criar a partição FAT16 e
então crie o sistema de arquivos usando:

<informalexample><screen>
# mkdosfs /dev/<replaceable>sda1</replaceable>
</screen></informalexample>

Tenha atenção de usar o nome de dispositivo correto para o stick USB.
O comando <command>mkdosfs</command> vem junto com o pacote da
Debian <classname>dosfstools</classname>.

</para></note><para>

Para iniciar o kernel após a inicialização da memória stick USB,
nós precisaremos colocar um gerenciador de partida na memória
stick. No entanto, qualquer gerenciador de partida
(e.g. <command>LILO</command>) deverá funcionar. É conveniente usar
o <command>SYSLINUX</command> pois ele usa uma partição FAT16
e pode ser configurado apenas com a edição de um arquivo de textos.
Qualquer sistema operacional que suporte o sistema de arquivos FAT
poderá ser usado para fazer alterações na configuração do gerenciador
de partida.

</para><para>

Para colocar o <command>SYSLINUX</command> em uma partição FAT16 de sua
memória stick USB, instale os pacotes <classname>syslinux</classname> e
<classname>mtools</classname> em seu sistema e execute:

<informalexample><screen>
# syslinux /dev/<replaceable>sda1</replaceable>
</screen></informalexample>

Novamente, tenha atenção ao usar o nome de dispositivo. A partição
não deverá estar montada ao iniciar o <command>SYSLINUX</command>.
Este processo grava um setor de partida na partição e cria um
arquivo <filename>ldlinux.sys</filename> que contém o código do
gerenciador de partida.

</para><para>

Monte a partição (<userinput>mount /dev/sda1 /mnt</userinput>) e
copie os seguintes arquivos de um repositório da Debian para
a memória stick:

<itemizedlist>
<listitem><para>

<filename>vmlinuz</filename> (binário do kernel)

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename> (imagem inicial do disco ram)

</para></listitem>
<listitem><para>

<filename>syslinux.cfg</filename> (arquivo de configuração do SYSLINUX)

</para></listitem>
<listitem><para>

Módulos opcionais de kernel

</para></listitem>
</itemizedlist>

Se quiser renomear os arquivos, tenha atenção ao fato de que o
<command>SYSLINUX</command> somente pode processar nomes de arquivos no
formato (8.3) do DOS.

</para><para>

O arquivo de configuração do SYSLINUX <filename>syslinux.cfg</filename>
deverá conter as seguintes duas linhas:

<informalexample><screen>
default vmlinuz
append initrd=initrd.gz
</screen></informalexample>

</para>
   </sect3>
