<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44026 -->
<!-- revisado jfs, 23 febrero 2005 -->
<!-- revisado Rudy Godoy, 22 feb. 2005 -->
<!-- Revisado por Igor Tamara, enero 2007 -->

 <chapter id="d-i-intro"><title>Usar el instalador de Debian</title>

 <sect1><title>Funcionamiento del instalador</title>
<para>

El instalador de Debian est� compuesto por un conjunto de componentes de
prop�sito espec�fico para realizar cada tarea de la instalaci�n. Cada
componente realiza una tarea, formulando al usuario las preguntas que
sean necesarias para realizar su trabajo. Se asignan prioridades a cada
una de las preguntas, fijando su prioridad al arrancar el instalador.

</para><para>

Cuando se realiza una instalaci�n est�ndar, solamente se formular� las
preguntas esenciales (prioridad alta). Esto tiene como consecuencia un
proceso de instalaci�n altamente automatizado y con poca interacci�n del
usuario. Los componentes son ejecutados autom�ticamente en una secuencia
predeterminada. Los componentes a ejecutar depender�n del m�todo de instalaci�n
que use y de su hardware. El instalador usar� los valores predeterminados
para las preguntas que no son formuladas.

</para><para>

Cuando exista un problema, el usuario ver� el error en pantalla, y
es posible que se muestre el men� del instalador para que elija de �ste alguna
acci�n alternativa. El usuario no ver� el men� del instalador
si no se produce ning�n problema, simplemente tendr� que responder las
preguntas formuladas por cada componente en cada paso.
Se fija prioridad cr�tica (<quote>critical</quote>) para cualquier notificaci�n de un error
serio, por lo que el usuario siempre ser� notificado de estos errores.

</para><para>

Algunos de los valores predeterminados que usa el instalador pueden ser
modificados mediante el paso de argumentos de arranque en el inicio del &d-i;.
Si, por ejemplo, desea forzar la configuraci�n de red est�tica (se
usa DHCP como opci�n predeterminada si este protocolo est� disponible),
puede utilizar el
par�metro de arranque <userinput>netcfg/disable_dhcp=true</userinput>.
Puede consultar todas las opciones disponibles en
<xref linkend="installer-args"/>.

</para><para>

Es posible que los usuarios avanzados est�n m�s c�modos si utilizan
la interfaz basada en men�, donde el control de cada paso lo tiene el usuario
en lugar de que �stos se ejecuten de forma autom�tica en una secuencia
predeterminada por el instalador.
Para usar el instalador en el modo manual, gestionado a trav�s de un men�,
a�ada el argumento de arranque
<userinput>priority=medium</userinput>.

</para><para>

Deber� iniciar el instalador en modo <quote>expert</quote>
si para hacer funcionar o detectar su hardware es necesario
que indique opciones a los m�dulos del n�cleo
conforme se instalen. Esto puede realizarse ya sea usando la orden
<command>expert</command> al iniciar el instalador o bien a�adiendo el
argumento de arranque <userinput>priority=low</userinput>.
El modo experto le da control total del &d-i;.

</para><para>

Las pantallas del instalador est�n basadas en caracteres (distinto de la,
cada vez m�s familiar, interfaz gr�fica). No se puede utilizar el rat�n en este
entorno. A continuaci�n se indican algunas teclas que puede usar para moverse en
los diversos di�logos. El <keycap>Tabulador</keycap> o la tecla con la
flecha <keycap>derecha</keycap> realizan desplazamientos <quote>hacia adelante</quote>,
la combinaci�n tecla <keycombo> <keycap>Shift</keycap>
<keycap>Tabulador</keycap> </keycombo> y la tecla con la flecha
<keycap>izquierda</keycap> desplazan <quote>hacia atr�s</quote> entre los botones y
las opciones.
Las teclas con la flecha <keycap>arriba</keycap> y <keycap>abajo</keycap>
mueven entre los distintos elementos disponibles en una lista desplazable,
y tambi�n desplazan a la lista en s� (cuando se llega al final de la pantalla, N. del t.). Adem�s, en listas largas, usted puede
escribir una letra para hacer que la lista se desplace directamente
a la secci�n con elementos que se inicien con la letra que ha escrito y
usar las teclas <keycap>Re-P�g</keycap> (Retroceso de p�gina) y <keycap>Av-P�g</keycap> (Avance de p�gina) para
desplazarse entre la lista por secciones. La <keycap>barra espaciadora</keycap>
marca un elemento, como en el caso de una casilla. Pulse &enterkey; para
activar las opciones elegidas.

</para><para arch="s390">

S/390 no soporta consolas virtuales. Puede abrir una segunda y
tercera sesi�n de ssh para poder ver los mensajes de registro descritos
a continuaci�n.

</para><para>

Los mensajes de error y de registro son redireccionados a la cuarta consola. Puede
acceder a �sta pulsando
<keycombo><keycap>Alt Izq</keycap><keycap>F4</keycap></keycombo>
(mantenga presionada la tecla <keycap>Alt</keycap> mientras presiona la
tecla de funci�n <keycap>F4</keycap>). Para volver al proceso de instalaci�n
principal pulse
<keycombo><keycap>Alt Izq</keycap><keycap>F1</keycap></keycombo>.

</para><para>

Tambi�n puede encontrar los mensajes de error en
<filename>/var/log/syslog</filename>. Este registro se copia
a <filename>/var/log/installer/syslog</filename>
en su nuevo sistema una vez finalizada la instalaci�n.
Durante el proceso de instalaci�n puede encontrar otros mensajes
en <filename>/var/log/</filename>, y en
<filename>/var/log/installer/</filename> despu�s
de que el ordenador haya sido iniciado con el sistema instalado.

</para>
 </sect1>


  <sect1 id="modules-list"><title>Introducci�n a los componentes</title>
<para>

A continuaci�n se muestra una lista de los componentes del instalador con una breve
descripci�n del prop�sito de cada uno. Puede encontrar los detalles que
necesite conocer de un determinado componente en la
<xref linkend="module-details"/>.

</para>

<variablelist>
<varlistentry>

<term>main-menu</term><listitem><para>

Muestra al usuario la lista de componentes durante el trabajo del instalador, e
inicia el componente elegido cuando se selecciona.  Las preguntas de
<quote>main-menu</quote> tienen prioridad media (<quote>medium</quote>), de
modo que no ver� el men� si define su prioridad a valores alto
(<quote>high</quote>) � cr�tico (<quote>critical</quote>).  El valor
predeterminado es alto.  Por otro lado, se reducir� temporalmente la prioridad
de alguna pregunta si se produce un error que haga necesaria su intervenci�n de
forma que pueda resolver el problema. En este caso es posible que el men�
aparezca.

</para><para>

Puede volver al men� principal pulsando repetidamente el bot�n <quote>Volver</quote>
hasta salir del componente que est� ejecutando.

</para></listitem>
</varlistentry>
<varlistentry>


<term>localechooser</term><listitem><para>

Permite que el usuario seleccione las opciones de localizaci�n tanto para la
instalaci�n como para el sistema a instalar. Estas opciones incluyen idioma,
pa�s y valores de localizaci�n. El instalador mostrar� los mensajes en el
idioma seleccionado a menos que la traducci�n para ese idioma no est� completa,
en cuyo caso podr�n mostrarse algunos mensajes en ingl�s.

</para></listitem>
</varlistentry>
<varlistentry>

<term>kbd-chooser</term><listitem><para>

Muestra una lista de teclados, de la cual el usuario elije el modelo
que corresponda al suyo.

</para></listitem>
</varlistentry>
<varlistentry>

<term>hw-detect</term><listitem><para>

Detecta autom�ticamente la mayor�a del hardware del sistema, incluyendo
tarjetas de red, discos duros y PCMCIA.

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-detect</term><listitem><para>

Busca y monta un CD de instalaci�n de Debian.

</para></listitem>
</varlistentry>
<varlistentry>

<term>netcfg</term><listitem><para>

Configura las conexiones de red del ordenador de modo que �ste pueda
comunicarse a trav�s de Internet.

</para></listitem>
</varlistentry>
<varlistentry>

<term>iso-scan</term><listitem><para>

Busca sistemas de ficheros ISO, que pueden estar en un CD-ROM o en
el disco duro.

</para></listitem>
</varlistentry>
<varlistentry>

<term>choose-mirror</term><listitem><para>

Presenta una lista de los servidores de r�plica del archivo de Debian. El usuario
puede elegir la fuente que se utilizar� para sus paquetes de instalaci�n.

</para></listitem>
</varlistentry>
<varlistentry>

<term>cdrom-checker</term><listitem><para>

Verifica la integridad de un CD-ROM. De esta forma el usuario puede
asegurarse por s� mismo que el CD-ROM de instalaci�n no est�
da�ado.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lowmem</term><listitem><para>

Lowmem intenta detectar sistemas con poca memoria y entonces realiza
varios trucos para eliminar partes innecesarias del &d-i; en la memoria
(a costa de algunas caracter�sticas).

</para></listitem>
</varlistentry>
<varlistentry>

<term>anna</term><listitem><para>

<quote>Anna's Not Nearly APT</quote> (Anna casi no es APT, N. del t.).
Instala paquetes que han sido obtenidos del servidor espejo escogido
o del CD-ROM.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partman</term><listitem><para>

Permite al usuario particionar los discos conectados al sistema, crear
sistemas de ficheros en las particiones seleccionadas y a�adirlos a los
puntos de montaje. Incluye algunas caracter�sticas interesantes
como son un modo totalmente autom�tico de particionado o el
soporte de vol�menes l�gicos (LVM). Se trata de la herramienta de
particionado recomendada para Debian.

</para></listitem>
</varlistentry>
<varlistentry>

<term>autopartkit</term><listitem><para>

Particiona autom�ticamente todo el disco de acuerdo a unas preferencias
de usuario predefinidas.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partitioner</term><listitem><para>

Permite al usuario particionar los discos conectados al sistema.
Se elige un programa de particionado apropiado para la
arquitectura de su ordenador.

</para></listitem>
</varlistentry>
<varlistentry>

<term>partconf</term><listitem><para>

Muestra una lista de particiones y crea sistemas de ficheros en las
particiones seleccionadas de acuerdo a las instrucciones del usuario.

</para></listitem>
</varlistentry>
<varlistentry>

<term>lvmcfg</term><listitem><para>

Ayuda al usuario con la configuraci�n del gestor de vol�menes l�gicos (
Logical Volume Manager � <firstterm>LVM</firstterm>, N. del t.).

</para></listitem>
</varlistentry>
<varlistentry>

<term>mdcfg</term><listitem><para>

Permite al usuario configurar sistemas <firstterm>RAID</firstterm>
(<quote>Redundant Array of Inexpensive Disks</quote>) por software. Este RAID por
software habitualmente es mejor que los controladores baratos RAID IDE
(pseudo hardware) que puede encontrar en placas base nuevas.

</para></listitem>
</varlistentry>
<varlistentry>

<term>tzsetup</term><listitem><para>

Configura la zona horaria, bas�ndose en la localizaci�n seleccionada
anteriormente.

</para></listitem>
</varlistentry>
<varlistentry>

<term>clock-setup</term><listitem><para>

Determina si el reloj est� o no fijado a UTC.

</para></listitem>
</varlistentry>
<varlistentry>

<term>user-setup</term><listitem><para>

Configura la contrase�a del usuario �root� (administrador) y a�ade
un usuario no-administrador.

</para></listitem>
</varlistentry>
<varlistentry>

<term>base-installer</term><listitem><para>

Instala el conjunto de paquetes m�s b�sico que permitir� que el
ordenador opere con Linux cuando se reinicie.

</para></listitem>
</varlistentry>
<varlistentry>

<term>apt-setup</term><listitem><para>

Configura apt, casi todo autom�ticamente, bas�ndose en el medio desde
el que se est� ejecutando el instalador.

</para></listitem>
</varlistentry>
<varlistentry>

<term>pkgsel</term><listitem><para>

Utiliza <classname>tasksel</classname> para seleccionar e instalar
programas adicionales.

</para></listitem>
</varlistentry>
<varlistentry>

<term>os-prober</term><listitem><para>

Detecta los sistemas operativos instalados actualmente en el
ordenador y entrega esta informaci�n a <quote>bootloader-installer</quote>. �ste le
ofrecer� la posibilidad de a�adir estos sistemas
operativos al men� de inicio del gestor de arranque.
De esta manera el usuario podr�a f�cilmente elegir qu� sistema
operativo iniciar en el momento de arrancar su sistema.


</para></listitem>
</varlistentry>
<varlistentry>

<term>bootloader-installer</term><listitem><para>

Los distintos instaladores del gestor de arranque instalan un programa
de gesti�n de arranque en el disco duro. �ste es necesario
para que el ordenador arranque usando Linux sin usar un disco flexible
� CD-ROM. Muchos gestores de arranque permiten al usuario elegir un
sistema operativo alternativo cada vez que el ordenador se reinicia.

</para></listitem>
</varlistentry>
<varlistentry>

<term>shell</term><listitem><para>

Permite al usuario ejecutar un int�rprete de �rdenes ya sea desde el men� o
desde la segunda consola.

</para></listitem>
</varlistentry>
<varlistentry>

<term>save-logs</term><listitem><para> 

Ofrece una forma para que el usuario pueda guardar informaci�n en un
disco flexible, red, disco duro, u otros
dispositivos cuando se encuentre ante un problema. De esta forma puede
informar despu�s, adecuadamente, sobre los problemas que ha tenido con el
programa del instalador a los desarrolladores de Debian.

</para></listitem>
</varlistentry>

</variablelist>

 </sect1>

&using-d-i-components.xml;

</chapter>
