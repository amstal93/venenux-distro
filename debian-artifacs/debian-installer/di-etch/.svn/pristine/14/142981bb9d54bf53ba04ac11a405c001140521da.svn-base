<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 44026 -->

   <sect3 id="mdcfg">
   <title>マルチディスクデバイス (ソフトウェア RAID) の設定</title>
<para>

<!--
If you have more than one harddrive<footnote><para>

To be honest, you can construct MD device even from partitions
residing on single physical drive, but that won't bring you anything
useful.

</para></footnote> in your computer, you can use
<command>mdcfg</command> to setup your drives for increased
performance and/or better reliability of your data. The result is
called <firstterm>Multidisk Device</firstterm> (or after its most
famous variant <firstterm>software RAID</firstterm>).
-->
コンピュータに複数ハードディスクドライブがある<footnote><para>

本当のことをいえば、同一の物理ドライブを分割して MD デバイスを構築できますが、
便利なことはなにもありません。

</para></footnote>なら、
ドライブのパフォーマンスの向上やデータの信頼性向上のために
<command>mdcfg</command> を使用できます。
この結果を <firstterm>マルチディスクデバイス</firstterm>
(<firstterm>ソフトウェア RAID</firstterm> の方が有名) と呼びます。

</para><para>

<!--
MD is basically a bunch of partitions located on different disks and
combined together to form a <emphasis>logical</emphasis> device. This
device can then be used like an ordinary partition (i.e. in
<command>partman</command> you can format it, assign a mountpoint,
etc.).
-->
MD は基本的に別のディスクにあるパーティションを束ねて、
<emphasis>論理</emphasis> デバイスの形に結合したものです。
このデバイスは通常のパーティション 
(例: <command>partman</command> でフォーマットでき、
マウントポイントに割り当てられる等) と同様に使用できます。

</para><para>

<!--
What benefits this brings depends on the type of MD device you are
creating. Currently supported are:
-->
どんな恩恵を受けるかは、作成する MD デバイスの種類に依存します。
現在、以下をサポートしています。

<variablelist>
<varlistentry>

<term>RAID0</term><listitem><para>

<!--
Is mainly aimed at performance.  RAID0 splits all incoming data into
<firstterm>stripes</firstterm> and distributes them equally over each
disk in the array. This can increase the speed of read/write
operations, but when one of the disks fails, you will lose
<emphasis>everything</emphasis> (part of the information is still on
the healthy disk(s), the other part <emphasis>was</emphasis> on the
failed disk).
-->
RAID0 はパフォーマンスに主眼をおいています。
RAID0 は全入力データを <firstterm>stripes</firstterm> へ分割し、
均等にディスクアレイの各ディスクに分配します。
これにより、読み取り・書き込みの処理速度を向上できますが、
ディスクのうちの 1 つが破損したら、
<emphasis>すべて</emphasis>を失ってしまいます。
(情報の一部は正常なディスク上にありますが、
他の部分は<emphasis>破損したディスク上にある</emphasis>からです)

</para><para>

<!--
The typical use for RAID0 is a partition for video editing.
-->
RAID0 の典型的な使用法は映像編集用のパーティションです。

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID1</term><listitem><para>

<!--
Is suitable for setups where reliability is the first concern.  It
consists of several (usually two) equally-sized partitions where every
partition contains exactly the same data. This essentially means three
things.  First, if one of your disks fails, you still have the data
mirrored on the remaining disks. Second, you can use only a fraction
of the available capacity (more precisely, it is the size of the
smallest partition in the RAID). Third, file-reads are load-balanced among
the disks, which can improve performance on a server, such as a file
server, that tends to be loaded with more disk reads than writes.
-->
信頼性第一である場合、RAID1 を構成するとよいでしょう。
全パーティションが正確に同じデータを含むような、
いくつかの (たいてい 2 つ) 等しいサイズのパーティションから成ります。
これは本質的に 3 つのことを意味します。
まずディスクの 1 つが破損した場合、残ったディスクにデータミラーが残ります。
次に利用可能領域の断片だけの使用もできます。
(もっと正確には、RAID で構成する最小のパーティションサイズとなります)
第 3 に、ディスクからのファイルの読み込みをロードバランスする事ができます。
これにより、ファイルサーバのような、
書き込みより読み込みの方が負荷が高くなる傾向のあるサーバの
パフォーマンスを改善できます。
</para><para>

<!--
Optionally you can have a spare disk in the array which will take the
place of the failed disk in the case of failure.
-->
破損した場合に、任意に予備ディスクを破損したディスクの代わりに、
ディスクアレイにつけることができます。

</para></listitem>
</varlistentry>
<varlistentry>

<term>RAID5</term><listitem><para>

<!--
Is a good compromise between speed, reliability and data redundancy.
RAID5 splits all incoming data into stripes and distributes them
equally on all but one disk (similar to RAID0). Unlike RAID0, RAID5
also computes <firstterm>parity</firstterm> information, which gets
written on the remaining disk. The parity disk is not static (that
would be called RAID4), but is changing periodically, so the parity
information is distributed equally on all disks.  When one of the
disks fails, the missing part of information can be computed from
remaining data and its parity. RAID5 must consist of at least three
active partitions. Optionally you can have a spare disk in the array
which will take the place of the failed disk in the case of failure.
-->
RAID5 は速度と信頼性、データの冗長性をうまく折衷しています。
RAID5 はストライプへ入力するデータをすべて分割し、
1 つ以外の全ディスクに (RAID0 のように) 等しく分配します。
RAID0 と違い、RAID5 は (残りのディスクに書かれている) 
<firstterm>パリティ</firstterm>情報も計算します。
パリティディスクは静的 (これを RAID4 と呼ぶ) ではありません。
(定期的に変更され) パリティ情報を全ディスクに等しく分配します。
あるディスクが故障した場合、
情報の失った部分は残ったディスクとそのパリティから計算されます。
RAID5 は少なくとも 3 つのアクティブなパーティションから成ります。
故障した場合に、任意でディスクアレイ中の故障したディスクの箇所に
予備のディスクをセットできます。

</para><para>

<!--
As you can see, RAID5 has a similar degree of reliability to RAID1
while achieving less redundancy. On the other hand, it might be a bit
slower on write operations than RAID0 due to computation of parity
information.
-->
おわかりのように、RAID5 は RAID1 より冗長性が少なく、同程度の信頼性を持ちます。
一方、パリティ情報を計算するため、RAID0 より書き込み操作が少し遅いかもしれません。

</para></listitem>
</varlistentry>
</variablelist>
<!--
To sum it up:
-->
まとめると

<informaltable>
<tgroup cols="5">
<thead>
<row>
  <entry>タイプ</entry>
  <entry>デバイス最小構成数</entry>
  <entry>予備デバイス</entry>
  <entry>ディスク破損に耐えるか?</entry>
  <entry>利用可能領域</entry>
</row>
</thead>

<tbody>
<row>
  <entry>RAID0</entry>
  <entry>2</entry>
  <entry>×</entry>
  <entry>×</entry>
  <entry>RAID にある最小パーティションのサイズ×デバイス数</entry>
</row>

<row>
  <entry>RAID1</entry>
  <entry>2</entry>
  <entry>任意</entry>
  <entry>○</entry>
  <entry>RAID にある最小パーティションのサイズ</entry>
</row>

<row>
  <entry>RAID5</entry>
  <entry>3</entry>
  <entry>任意</entry>
  <entry>○</entry>
  <entry>
    RAID にある最小パーティションのサイズ×(デバイス数 - 1)
  </entry>
</row>

</tbody></tgroup></informaltable>

</para><para>

<!--
If you want to know more about Software RAID, have a look
at <ulink url="&url-software-raid-howto;">Software RAID HOWTO</ulink>.
-->
ソフトウェア RAID に関して、もっと知りたい場合は
<ulink url="&url-software-raid-howto;">Software RAID HOWTO</ulink> 
をご覧ください。

</para><para>

<!--
To create a MD device, you need to have the desired partitions it
should consist of marked for use in a RAID.  (This is done in
<command>partman</command> in the <guimenu>Partition
settings</guimenu> menu where you should select <menuchoice>
<guimenu>Use as:</guimenu> <guimenuitem>physical volume for
RAID</guimenuitem> </menuchoice>.)
-->
MD デバイスを作成するには、RAID で使うための
(これは <menuchoice><guimenu>利用方法:</guimenu> 
<guimenuitem>RAID の物理ボリューム</guimenuitem> </menuchoice>
を選択して出てくる、<guimenu>パーティション設定</guimenu> メニューの
<command>partman</command> で行えます)

</para><warning><para>

<!--
Support for MD is a relatively new addition to the installer.
You may experience problems for some RAID levels and in combination
with some bootloaders if you try to use MD for the root
(<filename>/</filename>) filesystem. For experienced users, it may be
possible to work around some of these problems by executing some
configuration or installation steps manually from a shell.
-->
MD のサポートは、インストーラに比較的新しく追加されました。
root (<filename>/</filename>) ファイルシステム用に MD を使用するなら、
RAID レベルやブートローダと組み合わせた際の問題に行き当たるでしょう。
経験を積んだユーザ向けに、
いくつか設定したりインストールステップをシェルから手動で行ったりして、
問題を回避して動作させることができるかも知れません。

</para></warning><para>

<!--
Next, you should choose <guimenuitem>Configure software
RAID</guimenuitem> from the main <command>partman</command> menu.
(The menu will only appear after you mark at least one partition for
use as <guimenuitem>physical volume for RAID</guimenuitem>.)
On the first screen of <command>mdcfg</command> simply select
<guimenuitem>Create MD device</guimenuitem>. You will be presented with
a list of supported types of MD devices, from which you should choose
one (e.g. RAID1). What follows depends on the type of MD you selected.
-->
次にメインの <command>partman</command> メニューから 
<guimenuitem>ソフトウェア RAID の設定</guimenuitem> を選んでください。
(このメニューは、少なくともパーティションをひとつ 
<guimenuitem>RAID の物理ボリューム</guimenuitem> 
としてマークしないと表示されません)
<command>mdcfg</command> の最初の画面では、
単に <guimenu>MD デバイスの作成</guimenu> を選択してください。
サポートされる MD デバイスのリストも提供されます。
この中から 1 つ (例: RAID1) を選択してください。
その後は選択した MD デバイスに依存します。
</para>

<itemizedlist>
<listitem><para>

<!--
RAID0 is simple &mdash; you will be issued with the list of available
RAID partitions and your only task is to select the partitions which
will form the MD.
-->
RAID0 は単純です。利用可能な RAID パーティションの一覧が提供されますので、
単に MD にするパーティションを選択してください。

</para></listitem>
<listitem><para>

<!--
RAID1 is a bit more tricky. First, you will be asked to enter the
number of active devices and the number of spare devices which will
form the MD. Next, you need to select from the list of available RAID
partitions those that will be active and then those that will be
spare. The count of selected partitions must be equal to the number
provided few seconds ago. Don't worry. If you make a mistake and
select different number of partitions, the &d-i; won't let you
continue until you correct the issue.
-->
RAID1 は少しトリッキーです。
まず MD にするアクティブなデバイスの数、
スペアデバイスの数を入力します。
次に利用可能な RAID パーティションの一覧からアクティブのもの、
次にスペアのものを選ぶ必要があります。
選択したパーティションの数と先ほど入力した数は一致しなければなりません。
心配しないでください。
間違って違う数のパーティションを選択した場合、&d-i; は問題を修正するまで、
先に進ませません。

</para></listitem>
<listitem><para>

<!--
RAID5 has a similar setup procedure as RAID1 with the exception that you
need to use at least <emphasis>three</emphasis> active partitions.
-->
RAID5 では、少なくとも <emphasis>3 つ</emphasis>のアクティブパーティションを
使用する必要があるという例外を除き、
RAID1 と同様のセットアップ手続きを行います。

</para></listitem>
</itemizedlist>

<para>

<!--
It is perfectly possible to have several types of MD at once. For
example, if you have three 200 GB hard drives dedicated to MD, each
containing two 100 GB partitions, you can combine the first partitions on
all three disks into the RAID0 (fast 300 GB video editing partition)
and use the other three partitions (2 active and 1 spare) for RAID1
(quite reliable 100 GB partition for <filename>/home</filename>).
-->
同時に数種の MD を持つことは完全に可能です。
例えば、3 つの 200GB の MD 専用ドライブがあって、
どれも 2 つの 100GB のパーティションに分かれている場合、
3 つのドライブすべての最初のパーティションを RAID0 
(高速な 300GB のビデオ編集パーティション) で結合でき、
その他の 3 つのパーティション (アクティブ 2 基、スペア 1 基) を RAID1 
(<filename>/home</filename> 用に信頼できる 100GB のパーティション) で結合できます。

</para><para>

<!--
After you setup MD devices to your liking, you can
<guimenuitem>Finish</guimenuitem> <command>mdcfg</command> to return
back to the <command>partman</command> to create filesystems on your
new MD devices and assign them the usual attributes like mountpoints.
-->
お好みの通りに MD デバイスの設定をした後で、
<guimenuitem>完了</guimenuitem> <command>mdcfg</command> として
<command>partman</command> に戻れます。
新しい MD デバイスにファイルシステムを作成し、
マウントポイントなどの通常の属性を設定してください。

</para>
   </sect3>
