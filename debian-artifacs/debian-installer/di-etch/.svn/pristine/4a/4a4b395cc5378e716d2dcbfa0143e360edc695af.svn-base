<!-- retain these comments for translator revision tracking -->
<!-- original version: 43773 -->

 <sect1 id="installation-media">
 <title>Mitjans d'instal·lació</title>

<para>

Aquesta secció us ajudarà a determinar quins mitjans podeu utilitzar per
instal·lar Debian. Per exemple, si teniu una disquetera al vostre
ordinador, la podeu utilitzar per instal·lar Debian. Hi ha un capítol
complet dedicat als mitjans consagrats, <xref linkend="install-methods"/>,
que llista els avantatges i desavantatges de cada un d'ells. Una vegada
aplegueu a eixa secció, segurament voldreu tornar a aquesta pàgina.

</para>

  <sect2 condition="supports-floppy-boot"><title>Disquets</title>
<para>

En alguns casos, heu de fer la primera arrencada des de disquet.
Normalment, tot el que necessitareu es un disquet d'alta densitat de
3.5 polsades (1440 KiB).

</para><para arch="powerpc">

El suport per disquets per CHRP no funciona en aquest moment.

</para>
  </sect2>

  <sect2><title>CD-ROM/DVD-ROM</title>

<note><para>

Quan vegeu en aquest manual <quote>CD-ROM</quote>, s'aplica a CD-ROM i
DVD-ROM, ja que les dues tecnologies són en realitat la mateixa des del
punt de vista del sistema operatiu, excepte per algunes unitats de
CD-ROM antigues i no estàndard que no son ni SCSI ni IDE/ATAPI.

</para></note><para>

La instal·lació basada en CD-ROM està suportada per algunes arquitectures.
En ordinadors amb suport per CD-ROM arrencables, podríeu fer una
instal·lació completa <phrase arch="not-s390">sense disquets</phrase>
<phrase arch="s390">sense cinta</phrase>. Fins i tot si el vostre sistema
no suporta arrencar des de CD-ROM, podeu complementar-ho amb les altres
tècniques per instal·lar el vostre sistema, un cop l'heu arrencat per altres
mitjans; mireu <xref linkend="boot-installer"/>.

</para><para arch="x86">

Estan suportats, tant els CD-ROM SCSI com els IDE/ATAPI. També estan
suportats tots els CD no estàndard que suporte Linux als discs d'arrencada
(com els Mitsumi i Matsushita). Aquests models necessiten uns paràmetres
especials per a què funcionen, i és improbable que es pugui arrencar des
d'aquestes interfícies. El <ulink url="&url-cd-howto;">Linux CD-ROM
HOWTO</ulink> conté informació en profunditat de com fer servir CD-ROM a
Linux.

</para><para arch="x86">

Les unitats de CD-ROM USB també estan suportades, així com tots els
dispositius FireWire que suporten els controladors ohci1394 i sbp2.

</para><para arch="alpha">

Tant els CD-ROM SCSI com els IDE/ATAPI estan suportats a &arch-title;,
sempre que ho estiguin per la consola SRM. Açò elimina moltes targetes
controladores, però s'espera que la majoria de xips IDE i SCSI i
targetes controladores proporcionades pel fabricant funcionaran
correctament. Per saber si el vostre dispositiu està
suportat per la consola SRM, vegeu l'enllaç <ulink
url="&url-srm-howto;">SRM HOWTO</ulink>.

</para><para arch="arm">

Estan suportats els CD-ROM IDE/ATAPI a totes les màquines ARM.
També estan suportats els CD-ROM SCSI als RiscPC.

</para><para arch="mips">

A les màquines SGI, per arrencar des de CD-ROM es necessita una unitat
SCSI que pugui treballar amb un bloc de dades de 512 bytes. Molts dels
CD-ROM SCSI venuts al mercat pel PC no tenen aquesta capacitat. Si la
vostra unitat de CD-ROM té un pont amb una etiqueta
<quote>Unix/PC</quote> o <quote>512/2048</quote>, poseu-la a la posició
<quote>Unix</quote> o <quote>512</quote>. Per començar a instal·lar,
escolliu <quote>Instal·lació del sistema</quote> al firmware. El Broadcom
BCM91250A suporta dispositius IDE estàndard, incloent unitats CD-ROM,
però les imatges de CD per aquesta plataforma no es distribueixen perquè
el firmware no reconeix unitats de CD. Per tal d'instal·lar Debian en
una targeta d'avaluació Broadcom BCM91480B, necessitareu una targeta IDE,
SATA o SCSI.

</para><para arch="mipsel">

A estacions DEC, per arrencar des de CD-ROM es necessita un disc SCSI
que pugui treballar amb un bloc de dades de 512 bytes. Molts dels
CD-ROM SCSI venuts al mercat pel PC no tenen aquesta capacitat. Si
la vostra unitat de CD-ROM té un pont amb una etiqueta
<quote>Unix/PC</quote> o <quote>512/2048</quote>, poseu-la a la posició
<quote>Unix</quote> o <quote>512</quote>.

</para><para arch="mipsel">

El CD 1 conté l'instal·lador per la subarquitectura r3k-kn02 (les
estacions basades en R3000 5000/1xx i 5000/240 i els models de estacions
DEC personals basades en R3000), el CD 2 l'instal·lador per la
subarquitectura r4k-kn04 (les estacions DEC basades en R4x00 5000/150 i
5000/260 així com les estacions DEC personals 5000/50).

</para><para arch="mipsel">

Per arrencar des de CD, executeu l'ordre  <userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
a l'indicador del firmware,on <replaceable>#</replaceable> és el nombre
del dispositiu TurboChannel des d'on arrencar (3 a la majoria de les
estacions DEC) i <replaceable>id</replaceable> és el ID SCSI de la unitat
de CD-ROM. Si necessiteu afegir paràmetres addicionals, es poden afegir
amb aquesta sintaxi:

</para><para arch="mipsel">

<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=valor1 param2=valor2 ...</userinput>

</para>
  </sect2>

  <sect2><title>Disc Dur</title>

<para>

Arrencar el sistema d'instal·lació directament des del disc dur és un
altra opció per moltes arquitectures. Açò requerirà un altre sistema
operatiu per copiar l'instal·lador al disc dur.

</para><para arch="m68k">

De fet, la instal·lació des del disc dur és la tècnica d'instal·lació
preferida per les màquines &architecture;.

</para><para arch="sparc">

Malgrat que &arch-title; no permet arrencar des de SunOS
(Solaris), podeu instal·lar des d'una partició SunOS (particions UFS).

</para>
  </sect2>

  <sect2 condition="bootable-usb"><title>Dispositiu de memòria USB</title>

<para>

Moltes màquines Debian només necessiten el seu disquet i/o unitat CD-ROM
per configurar el sistema i per fer rescats. Si gestioneu algun
servidor, segurament haureu pensat en ometre aquestes unitats
i utilitzar la memòria USB per instal·lar, i (si és necessari) per
recuperar el sistema. Aquesta situació també és útil en sistemes petits
on no es té espai per unitats no necessàries.

</para>
  </sect2>

  <sect2><title>Xarxa</title>

<para>

La xarxa es pot utilitzar durant la instal·lació per a obtenir fitxers que
aquesta pugui necessitar. Que la xarxa s'utilitzi o no depèn del mètode
d'instal·lació que s'hagi escollit i de les respostes que s'hagin donat
a algunes preguntes efectuades durant la instal·lació. El sistema
d'instal·lació suporta la majoria de connexions de xarxa (PPPoE inclosa,
però no XDSI o PPP), via HTTP o FTP. Un cop complatada la instal·lació,
podeu configurar el vostre sistema per utilitzar XDSI i PPP.

</para><para condition="supports-tftp">

Podeu <emphasis>arrencar</emphasis> també el vostre sistema utilitzant la
xarxa. <phrase arch="mips">Aquesta és la tècnica preferida per
&arch-title;.</phrase>

</para><para condition="supports-nfsroot">

La instal·lació sense discs, utilitzant l'arrencada des de xarxa a una
d'àrea local i muntant amb NFS tots els sistemes de fitxers locals és un
altra opció.

</para>
  </sect2>

  <sect2><title>Sistema Un*x o GNU</title>

<para>

Si esteu executant un altre sistema tipus Unix, podríeu utilitzar-lo per
instal·lar &debian; sense utilitzar el &d-i; com es descriu a la resta del
manual. Aquest tipus de instal·lació és prou normal per usuaris amb 
maquinari no suportat o a màquines que no es poden permetre el temps
d'apagada. Si esteu interessats en aquesta tècnica, passeu a  <xref
linkend="linux-upgrade"/>.

</para>
  </sect2>

  <sect2><title>Sistemes d'emmagatzemament suportats</title>

<para>

Els discs Debian contenen un nucli que es compila per maximitzar el
nombre de sistemes on funciona. Desafortunadament, açò fa que el nucli
sigui més gran, i que incorpore molts controladors que no fan falta per
la vostra màquina (vegeu <xref linkend="kernel-baking"/> per aprendre com
compilar el vostre nucli). Suportar la major quantitat de dispositius
possible és el que es desitja en general, per assegurar que Debian pugui
instal·lar-se a tot el maquinari possible.

</para><para arch="x86">

Normalment, la instal·lació de Debian inclou el suport per disquets,
discs IDE, disqueteres IDE, dispositius IDE per port paral·lel,
controladores SCSI i discs, USB i FireWire. Els sistemes de fitxers
suportats inclouen FAT, les extensions Win-32 FAT (VFAT), i NTFS.

</para><para arch="i386">

Les interfícies de disc que emulen la interfície de disc dur <quote>AT</quote>
&mdash; que s'anomenen MFM, RLL, IDE o ATA estan suportades.
Les controladores molt antigues de 8&ndash;bits com les dels IBM XT tan
sols es suporten com a mòdul. Les controladores de disc SCSI de molts
fabricants estan suportades. Per obtenir més detalls llegiu <ulink
url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>

</para><para arch="m68k">

Molts altres sistemes d'emmagatzemament suportats pel nucli de Linux
estan suportats a la instal·lació de Debian. Adoneu-vos que el nucli
de Linux no suporta del tot els disquets als Macintosh, i la instal·lació
de Debian no els suporta per Amiga. Es suporta el sistema HFS de
Macintosh als Atari, i AFFS com a mòdul. Els Mac suporten el sistema
de fitxers de Atari (FAT). Els Amiga suporten el sistema de fitxers FAT
i HFS com a mòdul

</para><para arch="sparc">

Qualsevol sistema de emmagatzemament suportat pel nucli de Linux també
està suportat pel sistema d'arrencada. Per defecte, al nucli estan
suportats els següents dispositius SCSI:

<itemizedlist>
<listitem><para>

Sparc ESP

</para></listitem>
<listitem><para>

PTI Qlogic,ISP

</para></listitem>
<listitem><para>

Adaptec AIC7xxx

</para></listitem>
<listitem><para>

NCR i Symbios 53C8XX

</para></listitem>
</itemizedlist>

Els sistemes IDE (com els UltraSPARC 5) també estan suportats. Vegeu
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>
Per tenir més informació del maquinari SPARC suportat pel nucli de Linux.

</para><para arch="alpha">

Qualsevol sistema de emmagatzemament suportat pel nucli de Linux, també
està suportat pel sistema d'arrencada. Açò inclou els discs SCSI i IDE.
Adoneu-vos, per altra banda, que a molts sistemes, la consola SRM no pot
arrencar des de discs IDE, i el Jensen no pot arrencar des de disquets. 
(vegeu  <ulink url="&url-jensen-howto;"></ulink>
per obtenir més informació per arrencar el Jensen)

</para><para arch="powerpc">

Qualsevol sistema d'emmagatzemament suportat pel nucli de Linux, està
també suportat pel sistema d'arrencada. Adoneu-vos que el nucli de Linux
no suporta del tot els disquets a sistemes CHRP.

</para><para arch="hppa">

Qualsevol sistema d'emmagatzemament suportat pel nucli de Linux, està
també suportat pel sistema d'arrencada. Adoneu-vos que el nucli de Linux
no suporta disquets.

</para><para arch="mips">

Qualsevol sistema d'emmagatzemament suportat pel nucli de Linux, està
també suportat pel sistema d'arrencada.

</para><para arch="s390">

Qualsevol sistema d'emmagatzemament suportat pel nucli de Linux, està
també suportat pel sistema d'arrencada. Açò vol dir que FBA i ECKD DASD
estan suporten per l'antic esquema de discs de Linux (ldl) i el nou
esquema de discs S/390 (cdl).

</para>

  </sect2>

 </sect1>
