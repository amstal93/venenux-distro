<!-- retain these comments for translator revision tracking -->
<!-- original version: 43573 -->
<!-- revised by Herbert Parentes  Fortes Neto (hpfn) 2006.02.05 -->
<!-- updated 33820:43573 by Felipe Augusto van de Wiel (faw) 2007.01.20 -->

   <sect3 id="apt-setup">
   <title>Configurando o apt</title>

<para>

Uma das ferramentas usadas para instalar pacotes em um sistema &debian;
é o programa chamado <command>apt-get</command>, do pacote
<classname>apt</classname><footnote>

<para>

Note que o programa que realmente instala os pacotes se chama
<command>dpkg</command>.  Contudo, esse programa é mais uma ferramenta de
baixo nível.  <command>apt-get</command> é uma ferramenta de alto nível
que irá invocar o <command>dpkg</command> de forma apropriada. Ele sabe
como obter pacotes do seu CD, de sua rede, ou de onde for necessário. Ele
também é capaz de instalar automaticamente outros pacotes que são requeridos
para fazer o pacote que você está tentando instalar funcionar corretamente.

</para>

</footnote>.
Outras interfaces para o gerenciador de pacotes, como
<command>aptitude</command> e <command>synaptic</command> também estão
em uso.
Essas interfaces são recomendadas para usuários novos, já que elas integram
algumas características adicionais (procura de pacotes e checagem de estado)
com uma boa interface de usuário. De fato, <command>aptitude</command> é
agora o utilitário recomendado para o gerenciamento de pacotes.

</para><para>

<command>apt</command> deve ser configurado para que ele saiba de onde obter
os pacotes. O instalador cuida disso de forma ampla e automática, baseado no
que sabe sobre seu ambiente de instalação. Os resultados desta configuração
são escritos no arquivo <filename>/etc/apt/sources.list</filename>.  E você
pode examinar e editar esse arquivo de acordo com seu gosto, após o
término da instalação.

</para>
   </sect3>
