<!-- retain these comments for translator revision tracking -->
<!-- original version: 16879 untranslated -->

  <sect2 arch="x86"><title>Booting from a CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

You may need to configure your hardware as indicated in 
<xref linkend="boot-dev-select"/>. Then put the CD-ROM into the drive,
and reboot.  The system should boot up, and you should be presented
with the <prompt>boot:</prompt> prompt.  Here you can enter your
boot arguments, or just hit &enterkey;.

</para><para>

<!-- We'll comment the following section until we know exact layout -->
<!-- 
CD #1 of official Debian CD-ROM sets for &arch-title; will present a
<prompt>boot:</prompt> prompt on most hardware. Press
<keycap>F3</keycap> to see the list of kernel options available
from which to boot. Just type your chosen flavor name (idepci,
vanilla, compact, bf24) at the <prompt>boot:</prompt> prompt
followed by &enterkey;.

</para><para>

If your hardware doesn't support booting of multiple images, put one
of the other CDs in the drive. It appears that most SCSI CD-ROM drives 
do not support <command>isolinux</command> multiple image booting, so users
with SCSI CD-ROMs should try either CD2 (vanilla) or CD3 (compact), 
or CD5 (bf2.4). 

</para><para>

CD's 2 through 5 will each boot a 
different ``flavor'' depending on which CD-ROM is
inserted. See <xref linkend="kernel-choice"/> for a discussion of the
different flavors.  Here's how the flavors are laid out on the
different CD-ROMs:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

Allows a selection of kernel images to boot from (the idepci flavor is
the default if no selection is made). 

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Boots the `vanilla' flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Boots the `compact' flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Boots the `idepci' flavor.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Boots the `bf2.4' flavor.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

If your system can't boot directly from CD-ROM, or you simply can't
seem to get it to work, don't despair; you can simply run
<command>E:\install\boot.bat</command> under DOS (replace
<userinput>E:</userinput> with whatever drive letter DOS assigns to
your CD-ROM drive) to start the installation process. Then, skip down
to <xref linkend="init-config"/>.

</para><para>

Also, if you're going to be installing from a FAT (DOS) partition, you
have the option of booting the installer from the hard disk. See 
<xref linkend="install-from-dos"/> for more information on 
installing via this method.

</para>
  </sect2>



  <sect2 arch="x86" id="install-from-dos">
  <title>Booting from a DOS partition</title>

&boot-installer-intro-hd.xml;

<para>

Boot into DOS (not Windows) without any drivers being loaded.  To do
this, you have to press <keycap>F8</keycap> at exactly the right
moment (and optionally select the `safe mode command prompt only'
option).  Enter the subdirectory for the flavor you chose, e.g.,

<informalexample><screen>

cd c:\current\compact

</screen></informalexample>.  

Next, execute <command>install.bat</command>.
The kernel will load and launch the installer system. 

</para><para>

Please note, there is currently a loadlin problem (#142421) which
precludes <filename>install.bat</filename> from being used with the
bf2.4 flavor. The symptom of the problem is an
<computeroutput>invalid compressed format</computeroutput> error.

</para>
  </sect2>


  <sect2 arch="x86" id="boot-initrd">
  <title>Booting from linux using <command>LILO</command> or
  <command>GRUB</command></title>
<para>

For <command>LILO</command>, you will need to configure two
essentials things in <filename>/etc/lilo.conf</filename>:
<itemizedlist>
<listitem><para>

to load the <filename>initrd.gz</filename> installer at boot time;

</para></listitem>
<listitem><para>

have the <filename>vmlinuz</filename> kernel use a RAM disk as
its root partition.

</para></listitem>
</itemizedlist>

Here is a <filename>/etc/lilo.conf</filename> example:

</para><para>

<informalexample><screen>

image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
       root=/dev/ram
       append="devfs=mount,dall"

</screen></informalexample>

For more details, refer to the
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> and
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry> man pages. Now run
<userinput>lilo</userinput> and reboot.

</para><para>

The procedure for <command>GRUB</command> is quite similar. Locate your
<filename>menu.lst</filename> in the <filename>/boot/grub/</filename>
directory (sometimes in the <filename>/boot/boot/grub/</filename>),
add the following lines:

<informalexample><screen>

title  New Install
kernel (hd0,0)/boot/newinstall/vmlinuz root=/dev/ram devfs=mount,dall
initrd (hd0,0)/boot/newinstall/initrd.gz

</screen></informalexample>

and reboot. Please note, that you may need an additional parameter
<userinput>ramdisk_size=<replaceable>size in KB</replaceable></userinput>,
depending on the image you are booting. From now on, there should be
no difference between <command>GRUB</command> or <command>LILO</command>.

</para><para>

You can trace the <filename>initrd</filename> magic at work several
times during the boot.

<itemizedlist>
<listitem><para>

before the kernel has even been loaded, <command>LILO</command>
displays a much longer <computeroutput>Loading
<replaceable>imagelabel</replaceable>......</computeroutput> line with
more dots than usual, showing the progression of the RAM disk image
loading.

</para></listitem>
<listitem><para>

You should see the <computeroutput>RAM disk driver
initialized</computeroutput>
notice, near the real time clock initialization, proving that your
kernel supports the RAM disk feature.

</para></listitem>
<listitem><para>

Finally, if you don't see <computeroutput>RAMDISK: ext2 filesystem
found at block 0</computeroutput> immediately after the partition
checks, it's probably because
your kernel miss the <filename>initrd</filename> feature.

</para></listitem>
</itemizedlist>

</para><para condition="FIXME">

You should now see the debian installer running. If you do not use any
removable medium, you want to check very early that your network
connection is working and <emphasis>before</emphasis> irreversibly
partitioning your hard disk. So you maybe need to
<userinput>insmod</userinput> some additional kernel modules for this,
for instance for your network interface. It's time
<emphasis>not</emphasis> to follow the order of steps suggested by
<command>debian-installer</command>. Leap directly to <userinput>Mount a
Previously-Initialized Partition</userinput>, and mount the partition
where you stored the modules that you extracted from
<filename>drivers.tgz</filename> (<xref linkend="files-lilo"></xref>).

</para>

<!-- Ideally, configure Device Driver Modules should support the
following (insmod-ing from the hard-disk) and not only from a floppy.
That would avoid the need to open a shell -->

<para>

Then switch to an other virtual terminal and use a shell (see 
<xref linkend="shell"/>) to find drivers
in the just mounted <filename>/target</filename>
directory. <userinput>insmod</userinput> the ones you need.

</para><para>

Go to <xref linkend="netcfg"/> in the
<command>debian-installer</command> installer menus, and
<userinput>ping</userinput> your favorite debian mirror at last.
Congratulations!

</para><para>

Use <userinput>Unmount a Partition</userinput> if you have mounted one
in the previous
paragraph, safely go back to the partitioning steps at the start of
<command>debian-installer</command> and follow the regular procedure,
with the network as a bonus. At this stage, it is even possible
(only a bit risky) to completely wipe out all the previous partitions
on your hard drive for a very clean installation. The only risk is that
your hard drive will be un-bootable for a short period of time.

</para>
  </sect2>

  <sect2 arch="x86" id="usb-boot">
  <title>Booting from USB memory stick</title>
<para>

Lets assume you have prepared everything from <xref
linkend="boot-dev-select"/> and <xref linkend="boot-usb-files"/>.  Now
just plug your USB stick into some free USB connector and reboot the
computer.  The system should boot up, and you should be presented with
the <prompt>boot:</prompt> prompt.  Here you can enter optional boot
arguments, or just hit &enterkey;.

</para><para>

In case your computer doesn't support booting from USB memory devices,
you can still use a single floppy to do the initial boot and then
switch to USB. Advance according to <xref linkend="floppy-boot"/>; the
kernel on boot floppy should detect your USB stick automatically. When
it asks for the root floppy, simply press &enterkey;. You should see
&d-i; starting.

</para>
  </sect2>

  <sect2 arch="x86" id="floppy-boot">
  <title>Booting from Floppies</title>
<para>

You will have already downloaded the floppy images you needed and
created floppies from the images in <xref linkend="create-floppy"/>.
If you need to, you can also modify the boot floppy; see 
<xref linkend="rescue-replace-kernel"/>.

</para><para>

To boot from the installer boot floppy, place it in the primary floppy
drive, shut down the system as you normally would, then turn it back
on.

</para><para>

For installing from a LS-120 drive (ATAPI version) with a set of
floppies, you need to specify the virtual location for the floppy
device.  This is done with the <emphasis>root=</emphasis> boot
argument, giving the device that the ide-floppy driver maps the device
to. For example, if your LS-120 drive is connected as the first IDE
device (master) on the second cable, you enter
<userinput>linux root=/dev/hdc</userinput> at the boot prompt.
Installation from LS-120 is only supported by 2.4 and later kernels.

</para><para>

Note that on some machines, <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Delete</keycap></keycombo> does not
properly reset the machine, so a ``hard'' reboot is recommended.  If
you are installing from an existing operating system (e.g., from a DOS
box) you don't have a choice. Otherwise, please do a hard reboot when
booting.

</para><para>

The floppy disk will be accessed, and you should then see a screen
that introduces the boot floppy and ends with the <prompt>boot:</prompt>
prompt.

</para><para>

You can do two things at the <prompt>boot:</prompt> prompt. You can
press the function keys <keycap>F1</keycap> through
<keycap>F10</keycap> to view a few pages of helpful information, or
you can boot the system.

</para><para>

Information on boot parameters which might be useful can be found by
pressing <keycap>F4</keycap> through <keycap>F7</keycap>.  If you add any
parameters to
the boot command line, be sure to type the boot method (the default is
<userinput>linux</userinput>) and a space before the first parameter (e.g.,
<userinput>linux floppy=thinkpad</userinput>). If you simply press &enterkey;,
that's the same as typing <userinput>linux</userinput> without any special
parameters.

</para><para>

Once you press &enterkey;, you should see the message
<computeroutput>Loading...</computeroutput>, followed by
<computeroutput>Uncompressing Linux...</computeroutput>, and
then a screenful or so of information about the hardware in your
system.  More information on this phase of the boot process can be
found below in <xref linkend="kernel-msgs"/>.

</para><para>

After booting from the boot floppy, the root floppy is
requested. Insert the root floppy and press &enterkey;, and the
contents are loaded into memory. The installer program
<command>debian-installer</command> is automatically launched.

</para>
  </sect2>
  
  <sect2 arch="x86" id="boot-tftp"><title>Booting with TFTP</title>
 
&boot-installer-intro-net.xml;
 
<para>

There are various ways to do a TFTP boot on i386.

</para>

   <sect3><title>Etherboot</title>
<para>

The <ulink url="http://www.etherboot.org">etherboot project</ulink>
provides bootdiskettes and even bootroms that do a TFTPboot.

</para>
   </sect3>

   <sect3><title>NIC with network bootROM</title>
<para>

It could be that your Network Interface Card provides
TFTP boot functionality.

</para><para condition="FIXME">

Let us (<email>&email-debian-boot-list;</email>) know how did you manage it.
Please refer to this document.

</para>
   </sect3>

   <sect3><title>NIC or Motherboard that support PXE</title>
<para>

It could be that your Network Interface Card or Motherboard provides
PXE boot functionality.
Which is a <trademark class="trade">Intel</trademark> re-implemention
of TFTP boot.

</para><para condition="FIXME">

Let us (<email>&email-debian-boot-list;</email>) know how did you manage it.
Please refer to this document.

<!-- from #debian-boot 2004-03-13
06:37 -!- SuperQ [ben@trogdor.likes.to.burninate.net] has joined #debian-boot
06:38 < SuperQ> anyone done much with d-i on pxe boot?
06:39 < SuperQ> I got it all setup, daily build from sjogren's files
06:39 < joshk> yes, it works
06:39 < SuperQ> "Warning: unable to open an initial console."
06:39 < SuperQ> Kernel panic: Attempted to kill init!
06:39 < joshk> pass devfs=mount to the kernel
06:40 < joshk> in pxelinux.cfg/whatever
06:40 < SuperQ> oh.. that's changed since the bug tracking post
06:40 < SuperQ> http://lists.debian.org/debian-testing/2003/debian-testing-200311/msg00098.html
06:40 < SuperQ> that says to devfs=nomount
06:41 < SuperQ> should probably copy the bulk of that message into
debian/dists/sarge/main/installer-i386/current/doc/INSTALLATION-HOWTO
06:41 < SuperQ> :) )
06:41 < joshk> that's from months ago
06:41 < joshk> :P
06:41 < SuperQ> I know
06:42 < SuperQ> but it's still referanced in the howto
06:42 < SuperQ> yay! it works now
06:42  * SuperQ gives his new ThinkPad X31 some sarge love
06:42 < SuperQ> I'll have to write up a page on Sarge/D-I and things for
                linux-laptops.net
06:45 < SuperQ> thanks joshk
-->
</para>
   </sect3>
  </sect2>

  <sect2 arch="x86"><title>i386 Boot Parameters</title>
<para>

If you are booting from the boot floppy or from CD-ROM you will be
presented with the boot prompt, <prompt>boot:</prompt>.  Details
about how to use boot parameters with the boot floppy can be found
in <xref linkend="floppy-boot"/>.  If you are booting from an
existing operating system, you'll have to use other means to set boot
parameters.  For instance, if you are installing from DOS, you can
edit the <filename>install.bat</filename> file with any text editor.

</para><para>

Some systems have floppies with ``inverted DCLs''. If you receive
errors reading from the floppy, even when you know the floppy is good,
try the parameter <userinput>floppy=thinkpad</userinput>.

</para><para>

On some systems, such as the IBM PS/1 or ValuePoint (which have ST-506
disk drivers), the IDE drive may not be properly recognized.  Again,
try it first without the parameters and see if the IDE drive is
recognized properly.  If not, determine your drive geometry
(cylinders, heads, and sectors), and use the parameter
<userinput>hd=<replaceable>cylinders</replaceable>,<replaceable>heads</replaceable>,<replaceable>sectors</replaceable></userinput>.

</para><para>

If you have a very old machine, and the kernel hangs after saying
<computeroutput>Checking 'hlt' instruction...</computeroutput>, then
you should try the <userinput>no-hlt</userinput> boot argument, which
disables this test.

</para><para>

If your screen begins to show a weird picture while the kernel boots,
eg. pure white, pure black or colored pixel garbage, your system may
contain a problematic video card which does not switch to the
framebuffer mode properly.  Then you can use the boot parameter
<userinput>debian-installer/framebuffer=false</userinput> or
<userinput>video=vga16:off</userinput> to disable the framebuffer
console. The language chooser will not appear; only the english
language will be available during the installation due to limited
console features. See <xref linkend="boot-parms"/> for details.

</para>
  </sect2>


  <sect2 arch="x86">
  <title>System freeze during the PCMCIA configuration phase</title>

<para>

Some laptop models produced by Dell are known to crash when PCMCIA device
detection tries to access some hardware addresses. Other laptops may display
similar problems. If you experience such a problem and you don't need PCMCIA
support during the installation, you can disable PCMCIA using the
<userinput>hw-detect/start_pcmcia=false</userinput> boot parameter. You can
then configure PCMCIA after the installation is completed and exclude the
resource range causing the problems.

</para><para>

Alternatively, you can boot the installer in expert mode. You will
then be asked to enter the resource range options your hardware
needs. For example, if you have one of the Dell laptops mentioned
above, you should enter <userinput>exclude port
0x800-0x8ff</userinput> here. There is also a list of some common
resource range options in the <ulink
url="http://pcmcia-cs.sourceforge.net/ftp/doc/PCMCIA-HOWTO-2.html#ss2.5">System
resource settings section of the PCMCIA HOWTO</ulink>. Note that you
have to omit the commas, if any, when you enter this value in the
installer.

</para>
  </sect2>

  <sect2 arch="x86">
  <title>System freeze while loading the USB modules</title>
<para>

The kernel normally tries to install USB modules and the USB keyboard driver
in order to support some non-standard USB keyboards. However, there are some
broken USB systems where the driver hangs on loading. A possible workaround
may be disabling the USB controller in your mainboard BIOS setup. Another option
is passing the <userinput>debian-installer/probe/usb=false</userinput> parameter
at the boot prompt, which will prevent the modules from being loaded.

</para>
  </sect2>
