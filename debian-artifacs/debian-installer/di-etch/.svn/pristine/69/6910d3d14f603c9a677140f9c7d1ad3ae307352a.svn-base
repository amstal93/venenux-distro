<!-- original version: 33772 -->

 <sect1 id="doc-organization">
 <title>Indeling van dit document</title>

<para>

Dit document is bedoeld als handleiding voor mensen die Debian voor
het eerst gebruiken. Het probeert zo weinig mogelijk aannames te doen
over uw kennisniveau. We gaan er echter wel vanuit dat u een algemeen
beeld heeft van de werking van de hardware in uw computer.

</para><para>

Ook gevorderde gebruikers kunnen interessante referentie-informatie
vinden in dit document, waaronder de voor installatie minimaal benodigde
specificaties, details over hardware die door het installatiesysteem van
Debian wordt ondersteund, en dergelijke. We moedigen gevorderde gebruikers
aan om heen en weer te bladeren in het document.

</para><para>

In het algemeen is deze handleiding lineair van opzet waardoor u van
begin tot einde door het installatieproces wordt geleid. Hieronder vindt
u de stappen voor de installatie van &debian;, en de secties van dit
document die met elke stap overeenkomen:

<orderedlist>
<listitem><para>

Bepaal of uw hardware voldoet aan de vereisten om het installatiesysteem
te kunnen gebruiken; zie <xref linkend="hardware-req"/>.

</para></listitem>
<listitem><para>

Maak een veiligheidskopie (backup) van uw systeem, zorg indien nodig voor
een plan van aanpak en voor de configuratie van hardware voorafgaand aan
de installatie van Debian; zie <xref linkend="preparing"/>. Als u een
multi-boot systeem voorbereidt, kan het zijn dat u ruimte op uw harde schijf
moet creëren die kan worden gepartitioneerd voor gebruik door Debian.

</para></listitem>
<listitem><para>

In <xref linkend="install-methods"/> verkrijgt u de installatiebestanden
die noodzakelijk zijn voor de door u gekozen installatiemethode.

</para></listitem>
<listitem><para>

<xref linkend="boot-installer"/> beschrijft het opstarten van de computer
in het installatiesysteem. Dit hoofdstuk beschrijft ook stappen die genomen
kunnen worden als u hierbij problemen ondervindt.

</para></listitem>
<listitem><para>

Voer de eigenlijke installatie uit zoals beschreven in <xref linkend="d-i-intro"/>.
Dit omvat de taalkeuze, het configureren van stuurmodules voor randapparaten,
het configureren van uw netwerkverbinding, het partitioneren van uw harde
schijf, de installatie van een basissysteem en vervolgens de selectie en
installatie van taken. Als u niet vanaf een CD installeert, wordt de
netwerkverbinding gebruikt om andere benodigde installatiebestanden direct
vanaf een Debian server te downloaden. (Enige achtergrondinformatie over het
opzetten van de partities voor uw Debian systeem is te vinden in
<xref linkend="partitioning"/>.)

</para></listitem>
<listitem><para>

Start de computer opnieuw op in uw zojuist geïnstalleerde basissysteem
met behulp van <xref linkend="boot-new"/>.

</para></listitem>
</orderedlist>

</para><para>

Wanneer u de installatie van uw systeem heeft voltooid, kunt u
<xref linkend="post-install"/> lezen. Dat hoofdstuk legt uit waar u
aanvullende informatie over Unix en Debian kunt vinden en hoe u uw kernel
kunt vervangen.

<!-- XXX FIXME: Als u uw eigen installatiesysteem wilt maken op basis van
de broncode, moet u zeker <xref linkend="boot-floppy-techinfo"/> lezen. -->

</para><para>

Tot slot kunt u informatie over dit document en over hoe u hieraan kunt
bijdragen vinden in <xref linkend="administrivia"/>.

</para>
 </sect1>

 <sect1 condition="FIXME">
 <title>Uw hulp bij het opstellen van documentatie is welkom</title>

<para>

Alle hulp, suggesties en (vooral) patches worden bijzonder gewaardeerd.
Ontwikkelversies van dit document zijn beschikbaar op
<ulink url="&url-d-i-alioth-manual;"/>. Daar vindt u een overzicht van de
verschillende platformen en talen waarvoor dit document beschikbaar is.

</para><para>

Ook de bronbestanden zijn vrij beschikbaar; zie <xref linkend="administrivia"/>
voor nader informatie over hoe u een bijdrage kunt leveren.
Suggesties, commentaar, patches en probleemrapporten worden verwelkomd
(gebruik het pakket <classname>installation-guide</classname> voor
probleemrapporten, maar controleer eerst of het probleem niet reeds
gerapporteerd is).
<!--FJP De voorgaande twee para's lijken te overlappen //-->

</para>
 </sect1>
