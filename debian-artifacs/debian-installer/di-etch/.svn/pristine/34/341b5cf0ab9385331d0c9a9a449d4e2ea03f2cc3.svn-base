<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->


  <sect2 arch="alpha">
  <!-- <title>Partitioning in Tru64 UNIX</title> -->
  <title>Partizionare da UNIX Tru64</title>
<para>

<!--
Tru64 UNIX, formerly known as Digital UNIX, which is in turn formerly
known as OSF/1, uses the partitioning scheme similar to the BSD <quote>disk
label</quote>, which allows for up to eight partitions per disk drive.  The
partitions are numbered <quote>1</quote> through to <quote>8</quote> in
Linux and <quote>lettered</quote> <quote>a</quote> through to
<quote>h</quote> in UNIX.  Linux kernels 2.2 and higher always correspond
<quote>1</quote> to <quote>a</quote>, <quote>2</quote> to <quote>b</quote>
and so on.  For example, <filename>rz0e</filename> in Tru64 UNIX would most
likely be called <filename>sda5</filename> in Linux.
-->

UNIX Tru64, conosciuto in precedenza come Digital UNIX e ancor prima come
OSF/1, usa uno schema di partizionamento simile alle <quote>disk label</quote>
BSD, che permette fino a otto partizioni per disco. Le partizioni sono
numerate da <quote>1</quote> a <quote>8</quote> in Linux e contrassegnate
con lettere, da <quote>a</quote> a <quote>h</quote>, in UNIX. A partire
dai kernel Linux 2.2, <quote>1</quote> corrisponde sempre ad
<quote>a</quote>, <quote>2</quote> a <quote>b</quote> e così via. Per
esempio, <filename>rz0e</filename> in Tru64 UNIX corrisponderebbe
verosimilmente a <filename>sda5</filename> in Linux.

</para><para>

<!--
Partitions in a Tru64 disk label may overlap.  Moreover, if this disk
will be used from Tru64, the <quote>c</quote> partition is required to span
the entire disk (thus overlapping all other non-empty partitions).  Under
Linux this makes <filename>sda3</filename> identical to
<filename>sda</filename> (<filename>sdb3</filename> to
<filename>sdb</filename>, if present, and so on).  However, the partman
partitioning tool used by &d-i; cannot handle overlapping partitions at
present.  As a result, it is currently not recommended to share disks
between Tru64 and Debian.  Partitions on Tru64 disks can be mounted
under Debian after installation has been completed.
-->

Le partizioni nella disk label possono sovrapporsi. Per di più la partizione
<quote>c</quote> deve estendersi sull'intero disco, sovrapponendosi in tal
modo a tutte le altre partizioni non vuote. Sotto Linux questo comportamento
rende <filename>sda3</filename> identico a <filename>sda</filename>
(<filename>sdb3</filename> a <filename>sdb</filename>, se presente, e così
via). Purtroppo, al momento, il programma di partizionamento usato dal &d-i;
non è in grado di gestire partizioni sovrapposte, di conseguenza si consiglia
di non condividere dei dischi con Tru64 e Debian. Le partizioni su dischi
Tru64 possono essere montate sotto Debian una volta conclusa l'installazione.

</para><para>

<!--
Another conventional requirement is for the <quote>a</quote> partition to
start from the beginning of the disk, so that it always includes the boot
block with the disk label. If you intend to boot Debian from that disk, you
need to size it at least 2MB to fit aboot and perhaps a kernel.
Note that this partition is only required for compatibility; you must
not put a file system onto it, or you'll destroy data.
-->

Un'altra convenzione è che la partizione <quote>a</quote> cominci dall'inizio
del disco, quindi essa comprende sempre il blocco di boot con la disk label.
Se si ha intenzione di avviare Debian da tale disco, dovrà essere di almeno
2&nbsp;MB per ospitarci aboot e forse un kernel. Notare che queste due
partizioni sono richieste solo per compatibilità, non si deve metterci un
file system oppure i dati andranno persi.

</para><para>

<!--
It is possible, and indeed quite reasonable, to share a swap partition
between UNIX and Linux.  In this case it will be needed to do a
<command>mkswap</command> on that partition every time the system is rebooted
from UNIX into Linux, as UNIX will damage the swap signature.  You may
want to run <command>mkswap</command> from the Linux start-up scripts before
adding swap space with <command>swapon -a</command>.
-->

È possibile, e anche abbastanza ragionevole, condividere una partizione
di swap tra UNIX e Linux. In tal caso è necessario eseguire
<command>mkswap</command> su tale partizione ogni volta che il sistema
viene riavviato da UNIX in Linux, dato che UNIX danneggia la firma di
swap. Si può fare in modo che gli script di avvio di Linux eseguano
<command>mkswap</command> prima di aggiungere spazio di swap con
<command>swapon -a</command>.

</para><para>

<!--
If you want to mount UNIX partitions under Linux, note that Digital UNIX
can use two different file system types, UFS and AdvFS, of which Linux
only understands the former.
-->

Se si vuole montare partizioni UNIX sotto Linux, notare che Digital UNIX può
utilizzare due tipi diversi di file system, UFS e AdvFS, ma Linux può usare
solo il primo dei due.

</para>
  </sect2>

  <sect2 arch="alpha">
  <!-- <title>Partitioning in Windows NT</title> -->
  <title>Partizionare da Windows NT</title>
<para>

<!--
Windows NT uses the PC-style partition table.  If you are manipulating
existing FAT or NTFS partitions, it is recommended that you use the
native Windows NT tools (or, more conveniently, you can also
repartition your disk from the AlphaBIOS setup menu).  Otherwise, it
is not really necessary to partition from Windows; the Linux
partitioning tools will generally do a better job.  Note that when you
run NT, the Disk Administrator may offer to write a <quote>harmless
signature</quote> on non-Windows disks if you have any.
<emphasis>Never</emphasis> let it do that, as this signature will destroy
the partition information.
-->

Windows NT usa tabelle di partizioni di tipo PC. Se si manipolano partizioni
FAT o NTFS esistenti, si raccomanda di usare i programmi nativi di Windows
NT (in caso si può ripartizionare anche dal menu di configurazione di
AlphaBIOS). Del resto non è necessario partizionare da Windows: i
corrispondenti programmi Linux di solito fanno un lavoro migliore. Notare
che quando è in esecuzione NT, il Disk Administrator potrebbe proporre di
contrassegnare in modo (per lui!) inoffensivo eventuali dischi non Windows,
se ce ne sono. <emphasis>Non fateglielo mai fare</emphasis>, dato che in
tal modo verrebbero distrutte le informazioni sulla partizione.

</para><para>

<!--
If you plan to boot Linux from an ARC/AlphaBIOS/ARCSBIOS console, you
will need a (small) FAT partition for MILO.  5 MB is quite
sufficient.  If Windows NT is installed, its 6 MB bootstrap partition
can be employed for this purpose.  Debian &releasename; does not support
installing MILO.  If you already have MILO installed on your system, or
install MILO from other media, Debian can still be booted from ARC.
-->

Se si pensa di fare l'avvio di Linux da una console ARC/AlphaBIOS/ARCSBIOS,
è necessario avete una (piccola) partizione FAT per MILO. Sono sufficienti
5&nbsp;MB. Se è installato Windows NT, si potrebbe impiegare a tale scopo
la sua partizione di bootstrap da 6&nbsp;MB. Debian &releasename; non
supporta l'installazione di MILO. Debian può essere avviata da ARC solo se
MILO è già installato sul proprio sistema oppure se lo si installa da altri
supporti.

</para>
  </sect2>
