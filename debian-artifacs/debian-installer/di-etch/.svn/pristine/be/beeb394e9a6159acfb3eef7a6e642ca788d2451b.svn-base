<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 39465 -->


 <sect1 id="directory-tree">
<!--
 <title>The Directory Tree</title>
-->
 <title>ディレクトリツリー</title>
<para>

<!--
&debian; adheres to the
<ulink url="&url-fhs-home;">Filesystem Hierarchy Standard</ulink>
for directory and file naming. This standard allows users and software
programs to predict the location of files and directories. The root
level directory is represented simply by the slash
<filename>/</filename>. At the root level, all Debian systems include
these directories:
-->
ディレクトリとファイルの名前について、&debian; は
<ulink url="&url-fhs-home;">Filesystem Hierarchy Standard</ulink>
に従っています。この規格を用いると、ユーザやプログラムは、
ファイルやディレクトリの場所を予想しやすくなります。
根っこ (ルート = root) にあるディレクトリは、単にスラッシュ <filename>/</filename>
で表されます。ルートのレベルには、
Debian システムでは必ず以下のようなディレクトリが含まれます。

<informaltable>
<tgroup cols="2">
<thead>
<row>
<!--
  <entry>Directory</entry><entry>Content</entry>
-->
  <entry>ディレクトリ</entry><entry>内容</entry>
</row>
</thead>

<tbody>
<!--
<row>
  <entry><filename>bin</filename></entry>
  <entry>Essential command binaries</entry>
</row><row>
  <entry><filename>boot</filename></entry>
  <entry>Static files of the boot loader</entry>
</row><row>
  <entry><filename>dev</filename></entry>
  <entry>Device files</entry>
</row><row>
  <entry><filename>etc</filename></entry>
  <entry>Host-specific system configuration</entry>
</row><row>
  <entry><filename>home</filename></entry>
  <entry>User home directories</entry>
</row><row>
  <entry><filename>lib</filename></entry>
  <entry>Essential shared libraries and kernel modules</entry>
</row><row>
  <entry><filename>media</filename></entry>
  <entry>Contains mount points for replaceable media</entry>
</row><row>
  <entry><filename>mnt</filename></entry>
  <entry>Mount point for mounting a file system temporarily</entry>
</row><row>
  <entry><filename>proc</filename></entry>
  <entry>Virtual directory for system information (2.4 and 2.6 kernels)</entry>
</row><row>
  <entry><filename>root</filename></entry>
  <entry>Home directory for the root user</entry>
</row><row>
  <entry><filename>sbin</filename></entry>
  <entry>Essential system binaries</entry>
</row><row>
  <entry><filename>sys</filename></entry>
  <entry>Virtual directory for system information (2.6 kernels)</entry>
</row><row>
  <entry><filename>tmp</filename></entry>
  <entry>Temporary files</entry>
</row><row>
  <entry><filename>usr</filename></entry>
  <entry>Secondary hierarchy</entry>
</row><row>
  <entry><filename>var</filename></entry>
  <entry>Variable data</entry>
</row><row>
  <entry><filename>srv</filename></entry>
  <entry>Data for services provided by the system</entry>
</row><row>
  <entry><filename>opt</filename></entry>
  <entry>Add-on application software packages</entry>
</row>
-->
<row>
  <entry><filename>bin</filename></entry>
  <entry>基本的なコマンドバイナリ</entry>
</row><row>
  <entry><filename>boot</filename></entry>
  <entry>ブートローダのスタティックなファイル</entry>
</row><row>
  <entry><filename>dev</filename></entry>
  <entry>デバイスファイル</entry>
</row><row>
  <entry><filename>etc</filename></entry>
  <entry>ホスト固有のシステム設定</entry>
</row><row>
  <entry><filename>home</filename></entry>
  <entry>ユーザのホームディレクトリ</entry>
</row><row>
  <entry><filename>lib</filename></entry>
  <entry>基本的な共有ライブラリとカーネルモジュール</entry>
</row><row>
  <entry><filename>media</filename></entry>
  <entry>取替え可能なメディア用のマウントポイントを含む</entry>
</row><row>
  <entry><filename>mnt</filename></entry>
  <entry>ファイルシステムを一時的にマウントするためのポイント</entry>
</row><row>
  <entry><filename>proc</filename></entry>
  <entry>システム情報を含む仮想ディレクトリ (2.4 および 2.6 カーネル)</entry>
</row><row>
  <entry><filename>root</filename></entry>
  <entry>root ユーザのホームディレクトリ</entry>
</row><row>
  <entry><filename>sbin</filename></entry>
  <entry>基本的なシステムバイナリ</entry>
</row><row>
  <entry><filename>sys</filename></entry>
  <entry>システム情報を含む仮想ディレクトリ (2.6 カーネル)</entry>
</row><row>
  <entry><filename>tmp</filename></entry>
  <entry>一時ファイル用</entry>
</row><row>
  <entry><filename>usr</filename></entry>
  <entry>第 2 階層</entry>
</row><row>
  <entry><filename>var</filename></entry>
  <entry>可変データ</entry>
</row><row>
  <entry><filename>srv</filename></entry>
  <entry>システムによって割り当てられた、サービスのためのデータ</entry>
</row><row>
  <entry><filename>opt</filename></entry>
  <entry>アドオンアプリケーションソフトウェアパッケージ</entry>
</row>
</tbody></tgroup></informaltable>
</para>

<para>

<!--
The following is a list of important considerations regarding
directories and partitions. Note that disk usage varies widely given
system configuration and specific usage patterns. The recommendations
here are general guidelines and provide a starting point for
partitioning.
-->
以下の一覧は、ディレクトリやパーティションについて重要となる考え方を
説明したものです。与えられたシステム構成や特別な使用パターンによって、
ディスク使用状況は大きく変化することに注意して下さい。ここで提案する
のは一般的なガイドラインであり、パーティション分割の第一歩を提供して
います。

</para>
<itemizedlist>
<listitem><para>

<!--
The root partition <filename>/</filename> must always physically
contain <filename>/etc</filename>, <filename>/bin</filename>,
<filename>/sbin</filename>, <filename>/lib</filename> and
<filename>/dev</filename>, otherwise you won't be able to boot.
Typically 150&ndash;250MB is needed for the root partition.
-->
ルートパーティション <filename>/</filename> は、必ず
<filename>/etc</filename>、<filename>/bin</filename>、
<filename>/sbin</filename>、<filename>/lib</filename>、
<filename>/dev</filename> を物理的に含んでいなければなりません
 (つまりこれらのディレクトリを別のパーティションにしてはいけません)。
さもないと起動ができなくなります。
一般的にここは 150&ndash;250MB 程度を必要とします。

</para></listitem>
<listitem><para>

<!--
<filename>/usr</filename>: contains all user programs
(<filename>/usr/bin</filename>), libraries
(<filename>/usr/lib</filename>), documentation
(<filename>/usr/share/doc</filename>), etc.
This is the part of the file system that generally takes up most space.
You should provide at least 500MB of disk space. This amount should
be increased depending on the number and type of packages you plan
to install. A generous workstation or server installation should allow
4&ndash;6GB.
-->
<filename>/usr</filename>: すべてのユーザプログラムを含む
(<filename>/usr/bin</filename>)、ライブラリ
(<filename>/usr/lib</filename>)、文書
(<filename>/usr/share/doc</filename>) など。
ここは一般に、ファイルシステムの中でも最も容量を必要とするところです。
少なくとも 500MB のディスク容量を割り当てるべきでしょう。インストール
しようとするパッケージの数やタイプによっては、もっと多くのディスク容量を
割り当てなければなりません。ディスク容量がたっぷりあるワークステーション
やサーバのインストールでは 4&ndash;6GB を割り当てるべきです。

</para></listitem>
<listitem><para>

<!--
<filename>/var</filename>: variable data like news articles, e-mails,
web sites, databases, the packaging system cache, etc. will be placed
under this directory. The size of this directory depends greatly on
the usage of your system, but for most people will be dictated by
the package management tool's overhead.  If you are going to do a full
installation of just about everything Debian has to offer, all in one
session, setting aside 2 or 3 GB of space for
<filename>/var</filename> should be sufficient. If you are going to
install in pieces (that is to say, install services and utilities,
followed by text stuff, then X, ...), you can get away with 300&ndash;500
MB. If hard drive space is at a premium and you don't plan on doing
major system updates, you can get by with as little as 30 or 40 MB.
-->
<filename>/var</filename>: ニュース記事、電子メール、ウェブコンテンツ、
データベース、パッケージングシステムのキャッシュなど、様々な
可変データがこのディレクトリに収められます。このディレクトリの容量は
システムの利用方法に大きく左右されますが、たいていの場合はパッケージ管理
ツールの使う分が最も大きな影響を持つことになるでしょう。Debian が提供する
ものすべてをいっぺんにフルインストールする場合でも、
<filename>/var</filename> には 2&ndash;3GB ほどの容量を割り当てておけば足りる
はずです。一度にすべてをインストールせず、部分部分を徐々に (例えば、まず
サービスやユーティリティを、次にコンソール用のもの、次に X 用のもの…と
いうように) インストールするなら、300&ndash;500MB の空き容量があれば良いでしょう。
ハードディスクの空き容量が貴重で、メジャーアップデートをする予定がないならば、
30&ndash;40MB ほどでもなんとかやっていけるでしょう。

</para></listitem>
<listitem><para>

<!--
<filename>/tmp</filename>: temporary data created by programs will
most likely go in this directory. 40&ndash;100MB should usually
be enough. Some applications &mdash; including archive manipulators,
CD/DVD authoring tools, and multimedia software &mdash; may use
<filename>/tmp</filename> to temporarily store image files. If you
plan to use such applications, you should adjust the space available
in <filename>/tmp</filename> accordingly.
-->
<filename>/tmp</filename>: プログラムが作成する一時データは、普通この
ディレクトリを利用します。通常は 40&ndash;100MB あれば充分です。いくつかの
アプリケーション (アーカイブマニピュレータ、CD/DVD オーサリングツール、
およびマルチメディアソフトウェアを含む) が、一時イメージファイルを
保存するのに <filename>/tmp</filename> を使用するかもしれません。
そのようなアプリケーションを使用する計画があるのなら、それ相応
に <filename>/tmp</filename> で利用できる容量を調整すべきです。

</para></listitem>
<listitem><para>

<!--
<filename>/home</filename>: every user will put his personal data
into a subdirectory of this directory. Its size depends on how many
users will be using the system and what files are to be stored in
their directories.  Depending on your planned usage you should reserve
about 100MB for each user, but adapt this value to your needs. Reserve
a lot more space if you plan to save a lot of multimedia files (pictures, MP3, movies)
in your home directory.
-->
<filename>/home</filename>: 各ユーザは、個人的なデータをこのディレクトリの
サブディレクトリに収めます。その容量は、このシステムを利用するユーザの数や、
ユーザディレクトリにどのようなファイルが収められるかによって異なってきます。
システムの使い方にもよりますが、ユーザごとに約 100MB ほどが必要でしょう。
しかしこの値は必要に応じて調整しなければなりません。
もし、たくさんのマルチメディアファイル (写真、MP3、動画) をホームディレクトリに
保存するつもりなら、もっと多くの容量を確保しておいてください。

</para></listitem>
</itemizedlist>

 </sect1>
