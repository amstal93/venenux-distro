<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 44410 -->

 <sect1 id="linux-upgrade">
<!--
 <title>Installing &debian; from a Unix/Linux System</title>
-->
 <title>Unix/Linux システムからの &debian; のインストール</title>

<para>

<!--
This section explains how to install &debian; from an existing
Unix or Linux system, without using the menu-driven installer as
explained in the rest of the manual. This <quote>cross-install</quote>
HOWTO has been requested by users switching to &debian; from
Red Hat, Mandrake, and SUSE. In this section some familiarity with
entering *nix commands and navigating the file system is assumed. In
this section, <prompt>$</prompt> symbolizes a command to be entered in
the user's current system, while <prompt>#</prompt> refers to a
command entered in the Debian chroot.
-->
本節は、マニュアルの他の部分で説明されている 
メニュードリブンインストーラを使用せずに、
既存の Unix・Linux システムから &debian; 
をインストールする方法について説明します。
この <quote>クロスインストール</quote> HOWTO は、
Red Hat, Mandrake, SUSE から &debian; に移行するユーザの要望で書かれました。
本節では、*nix コマンドの入力について熟知し、
ファイルシステムを操作できるのが前提となっています。
また、<prompt>#</prompt>が Debian chroot に入力されたコマンドを示し、
<prompt>$</prompt> はユーザの現在のシステムに入力されるコマンドを表します。


</para><para>

<!--
Once you've got the new Debian system configured to your preference,
you can migrate your existing user data (if any) to it, and keep on
rolling. This is therefore a <quote>zero downtime</quote> &debian;
install. It's also a clever way for dealing with hardware that
otherwise doesn't play friendly with various boot or installation
media.
-->
一旦、新しい Debian システムを好みに設定したら、
既存のユーザデータを (あるなら) 稼働したまま移行できます。
したがって、これは <quote>0 ダウンタイム</quote> &debian; インストールになります。
またこれは、
様々な起動・インストールメディアと相性のよくないハードウェアに対処する、
うまい方法です。

</para>

  <sect2>
  <title>はじめに</title>
<para>

<!--
With your current *nix partitioning tools, repartition the hard
drive as needed, creating at least one filesystem plus swap. You
need around 350MB of space available for a console only install,
or about 1GB if you plan to install X (more if you intend to
install desktop environments like GNOME or KDE).
-->
今の *nix のパーティション分割ツールで、
スワップと最低 1 つファイルシステムを作成するよう、
ハードディスクを希望に添って再分割してください。
コンソールのみのインストールには、最低 350MB の空き領域が必要ですし、
X をインストールする予定なら 1GB 
(GNOME や KDE のようなデスクトップ環境をインストールする場合はもっと) 
必要です。

</para><para>

<!--
Next, create file systems on the partitions. For example, to create an
ext3 file system on partition <filename>/dev/hda6</filename> (that's
our example root partition):
-->
次に、パーティションにファイルシステムを作成してください。
例えば、<filename>/dev/hda6</filename> パーティションに、
ext3 ファイルシステムを作成するには、以下のようにします。
(今回の例ではこのパーティションを root パーティションとします)

<informalexample><screen>
# mke2fs -j /dev/<replaceable>hda6</replaceable>
</screen></informalexample>

<!--
To create an ext2 file system instead, omit <userinput>-j</userinput>.
-->
ext3 ではなく ext2 ファイルシステムを作成するには、
<userinput>-j</userinput> を取ってください。

</para><para>

<!--
Initialize and activate swap (substitute the partition number for
your intended Debian swap partition):
-->
スワップを以下のように初期化して有効にしてください。
(パーティション番号は、
Debian スワップパーティションにするパーティション番号に、
読み替えてください)

<informalexample><screen>
# mkswap /dev/<replaceable>hda5</replaceable>
# sync; sync; sync
# swapon /dev/<replaceable>hda5</replaceable>
</screen></informalexample>

<!--
Mount one partition as <filename>/mnt/debinst</filename> (the
installation point, to be the root (<filename>/</filename>) filesystem
on your new system). The mount point name is strictly arbitrary, it is
referenced later below.
-->
パーティションを <filename>/mnt/debinst</filename> (インストールポイント。
新システムの root (<filename>/</filename>) ファイルシステムになります) 
にマウントしてください。
厳密にいうとマウントポイント名は何でも構いません。
以降の説明ではこれを使用します。

<informalexample><screen>
# mkdir /mnt/debinst
# mount /dev/<replaceable>hda6</replaceable> /mnt/debinst
</screen></informalexample>

</para>
<note><para>

<!--
If you want to have parts of the filesystem (e.g. /usr) mounted on
separate partitions, you will need to create and mount these directories
manually before proceding with the next stage.
-->
分割したパーティションをファイルシステムの一部 (例 /usr) にマウントする場合、
次のステージに進む前に、
手動でそのディレクトリを作成・マウントする必要があります。

</para></note>
  </sect2>

  <sect2>
  <title><command>debootstrap</command> のインストール</title>
<para>

<!--
The utility used by the Debian installer, and recognized as the
official way to install a Debian base system, is
<command>debootstrap</command>. It uses <command>wget</command> and
<command>ar</command>, but otherwise depends only on
<classname>/bin/sh</classname> and basic Unix/Linux tools<footnote>
-->
Debian インストーラが使用するユーティリティで、 
Debian 基本システムをインストールする公式の方法と認められているのが 
<command>debootstrap</command> です。
<command>wget</command> と <command>ar</command> を使用しますが、
<classname>/bin/sh</classname> と基本的な Unix/Linux ツール<footnote>

<para>

<!--
These include the GNU core utilities and commands like <command>sed</command>, <command>grep</command>, <command>tar</command> and <command>gzip</command>.
-->
これには、<command>sed</command>, <command>grep</command>, 
<command>tar</command>, <command>gzip</command> といった、
GNU コアユーティリティが含まれます。

</para>

<!--
</footnote>. Install <command>wget</command> and
<command>ar</command> if they aren't already on your current system,
then download and install <command>debootstrap</command>.
-->
</footnote>にのみ依存しています。今のシステムにまだインストールしていなければ、
<command>wget</command> と <command>ar</command> をインストールし、
その後 <command>debootstrap</command> 
をダウンロード・インストールしてください。

</para>

<!-- The files linked to here are from 2004 and thus currently not usable
<para>

If you have an rpm-based system, you can use alien to convert the
.deb into .rpm, or download an rpm-ized version at
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink>
rpm ベースシステムがあるなら、alien を使って .deb を .rpm に変換できます。
もしくは、
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink> 
から rpm 形式のものをダウンロードできます。

</para>
-->

<para>

<!--
Or, you can use the following procedure to install it
manually. Make a work folder for extracting the .deb into:
-->
また、手動でインストールするには、以下の手順になります。
まず .deb を展開するために作業フォルダを次のように作ってください。

<informalexample><screen>
# mkdir work
# cd work
</screen></informalexample>

<!--
The <command>debootstrap</command> binary is located in the Debian
archive (be sure to select the proper file for your
architecture). Download the <command>debootstrap</command> .deb from
the <ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/">
pool</ulink>, copy the package to the work folder, and extract the
files from it. You will need to have root privileges to install
the files.
-->
<command>debootstrap</command> バイナリは、Debian アーカイブ 
(あなたのアーキテクチャに適合するファイルを必ず選ぶこと) にあります。
<ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/">
pool</ulink> から <command>debootstrap</command> .deb をダウンロードして、
作業フォルダにパッケージをコピーし、ファイルを展開してください。
ファイルをインストールする際には root 権限を持つ必要があるでしょう。

<informalexample><screen>
# ar -x debootstrap_0.X.X_all.deb
# cd /
# zcat /full-path-to-work/work/data.tar.gz | tar xv
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title><command>debootstrap</command> の実行</title>
<para>

<!--
<command>debootstrap</command> can download the needed files directly
from the archive when you run it. You can substitute any Debian
archive mirror for <userinput>&archive-mirror;/debian</userinput> in
the command example below, preferably a mirror close to you
network-wise. Mirrors are listed at
<ulink url="http://www.debian.org/misc/README.mirrors"></ulink>.
-->
<command>debootstrap</command> は，実行されると、
アーカイブから必要なファイルを直接ダウンロードできます。
以下のコマンドの例では、
<userinput>&archive-mirror;/debian</userinput> としていますが、
ネットワーク的に近い Debian アーカイブミラーサイトで代用できます。
ミラーサイトは、
<ulink url="http://www.debian.org/misc/README.mirrors"></ulink> 
でリストされています。

</para><para>

<!--
If you have a &releasename; &debian; CD mounted at
<filename>/cdrom</filename>, you could substitute a file URL instead
of the http URL: <userinput>file:/cdrom/debian/</userinput>
-->
&releasename; &debian; CD を持っていて、
<filename>/cdrom</filename> にマウントしていれば、
http URL に代えて file URL (<userinput>file:/cdrom/debian/</userinput>) 
を使用することができます。

</para><para>

<!--
Substitute one of the following for <replaceable>ARCH</replaceable>
in the <command>debootstrap</command> command:
-->
<command>debootstrap</command> コマンドの <replaceable>ARCH</replaceable> は、
以下のうち一つを使用してください。

<userinput>alpha</userinput>,
<userinput>amd64</userinput>,
<userinput>arm</userinput>,
<userinput>hppa</userinput>,
<userinput>i386</userinput>,
<userinput>ia64</userinput>,
<userinput>m68k</userinput>,
<userinput>mips</userinput>,
<userinput>mipsel</userinput>,
<userinput>powerpc</userinput>,
<userinput>s390</userinput>,
<userinput>sparc</userinput>.

<informalexample><screen>
# /usr/sbin/debootstrap --arch ARCH &releasename; \
     /mnt/debinst http://ftp.us.debian.org/debian
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>基本システムの設定</title>
<para>

<!--
Now you've got a real Debian system, though rather lean, on disk.
<command>chroot</command> into it:
-->
さあ、これでディスクに真の Debian システムを 
(いくぶん中がスカスカですが) 手に入れました。
そこに <command>chroot</command> してください。

<informalexample><screen>
# LANG=C chroot /mnt/debinst /bin/bash
</screen></informalexample>

<!--
After chrooting you may need to set the terminal definition to be
compatible with the Debian base system, for example:
-->
chroot した後で、
Debian 基本システムと互換のある端末定義にする必要があるかもしれません。
例えば、以下のようにします。

<informalexample><screen>
# export TERM=<replaceable>xterm-color</replaceable>
</screen></informalexample>

</para>

   <sect3>
   <title>パーティションのマウント</title>
<para>

<!--
You need to create <filename>/etc/fstab</filename>.
-->
<filename>/etc/fstab</filename> を作る必要があります。

<informalexample><screen>
# editor /etc/fstab
</screen></informalexample>

<!--
Here is a sample you can modify to suit:
-->
以下のサンプルを自分に合うように編集できます。

<informalexample><screen>
# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump pass
/dev/XXX         /             ext3    defaults                 0    1
/dev/XXX         /boot         ext3    ro,nosuid,nodev          0    2

/dev/XXX         none          swap    sw                       0    0
proc             /proc         proc    defaults                 0    0

/dev/fd0         /media/floppy auto    noauto,rw,sync,user,exec 0    0
/dev/cdrom       /media/cdrom  iso9660 noauto,ro,user,exec      0    0

/dev/XXX         /tmp          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /var          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /usr          ext3    rw,nodev                 0    2
/dev/XXX         /home         ext3    rw,nosuid,nodev          0    2
</screen></informalexample>

<!--
Use <userinput>mount -a</userinput> to mount all the file systems you
have specified in your <filename>/etc/fstab</filename>, or, to mount
file systems individually, use:
-->
<filename>/etc/fstab</filename> で指定したファイルシステムを、
すべてマウントするには <userinput>mount -a</userinput> としてください。
また、ファイルシステムを別々にマウントするには、以下のようにしてください。

<informalexample><screen>
# mount /path   # e.g.: mount /usr
</screen></informalexample>

<!--
Current Debian systems have mountpoints for removable media under
<filename>/media</filename>, but keep compatibility symlinks in
<filename>/</filename>. Create these as as needed, for example:
-->
現在 Debian システムでは、リムーバブルメディアのマウントポイントを
<filename>/media</filename> にしていますが、
<filename>/</filename> にシンボリックリンクを置き互換性を保っています。
以下の例のように、必要であれば作成してください。

<informalexample><screen>
# cd /media
# mkdir cdrom0
# ln -s cdrom0 cdrom
# cd /
# ln -s media/cdrom
</screen></informalexample>

<!--
You can mount the proc file system multiple times and to arbitrary
locations, though <filename>/proc</filename> is customary. If you didn't use
<userinput>mount -a</userinput>, be sure to mount proc before continuing:
-->
proc ファイルシステムは、どこでも何度でもマウントすることができますが、
慣習的に <filename>/proc</filename> にマウントします。
<userinput>mount -a</userinput> を使用しなかった場合は、
以下のように先に進む前に必ず proc をマウントしてください。

<informalexample><screen>
# mount -t proc proc /proc
</screen></informalexample>

</para><para>

<!--
The command <userinput>ls /proc</userinput> should now show a non-empty
directory. Should this fail, you may be able to mount proc from outside
the chroot:
-->
<userinput>ls /proc</userinput> コマンドは、
今度は空のディレクトリにはならないはずです。これが失敗するようなら、
以下のように chroot の外側から proc をマウントできるかもしれません。

<informalexample><screen>
# mount -t proc proc /mnt/debinst/proc
</screen></informalexample>

</para>
   </sect3>

   <sect3>
<!--
   <title>Setting Timezone</title>
-->
   <title>タイムゾーンの設定</title>
<para>

<!--
An option in the file <filename>/etc/default/rcS</filename> determines
whether the system will interpret the hardware clock as being set to UTC
or local time. The following command allow you to set that and choose
your timezone.
-->
<filename>/etc/default/rcS</filename> ファイルにある設定で、
システムがハードウェアの時計を UTC として解釈するか、
現地時間としてを解釈するかを決定します。
以下のコマンドで、上記の選択とタイムゾーンの選択を行えます。

<informalexample><screen>
# editor /etc/default/rcS
# tzconfig
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>ネットワークの設定</title>
<para>

<!--
To configure networking, edit
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>,
<filename>/etc/hostname</filename> and
<filename>/etc/hosts</filename>.
-->
ネットワークの設定をするには、
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>, 
<filename>/etc/hostname</filename>,
<filename>/etc/hosts</filename> を編集してください。

<informalexample><screen>
# editor /etc/network/interfaces
</screen></informalexample>

<!--
Here are some simple examples from
<filename>/usr/share/doc/ifupdown/examples</filename>:
-->
次は、
<filename>/usr/share/doc/ifupdown/examples</filename> のシンプルな例です。

<informalexample><screen>
######################################################################
# /etc/network/interfaces -- configuration file for ifup(8), ifdown(8)
# See the interfaces(5) manpage for information on what options are
# available.
######################################################################

# We always want the loopback interface.
#
auto lo
iface lo inet loopback

# To use dhcp:
#
# auto eth0
# iface eth0 inet dhcp

# An example static IP setup: (broadcast and gateway are optional)
#
# auto eth0
# iface eth0 inet static
#     address 192.168.0.42
#     network 192.168.0.0
#     netmask 255.255.255.0
#     broadcast 192.168.0.255
#     gateway 192.168.0.1
</screen></informalexample>

<!--
Enter your nameserver(s) and search directives in
<filename>/etc/resolv.conf</filename>:
-->
<filename>/etc/resolv.conf</filename> に、
ネームサーバと search ディレクティブを入力してください。

<informalexample><screen>
# editor /etc/resolv.conf
</screen></informalexample>

<!--
A simple example <filename>/etc/resolv.conf</filename>:
-->
以下は、<filename>/etc/resolv.conf</filename> の簡単な例です。

<informalexample><screen>
search hqdom.local
nameserver 10.1.1.36
nameserver 192.168.9.100
</screen></informalexample>

<!--
Enter your system's host name (2 to 63 characters):
-->
システムのホスト名 (2 から 63 文字) を入力してください。
<informalexample><screen>
# echo DebianHostName &gt; /etc/hostname
</screen></informalexample>

<!--
And a basic <filename>/etc/hosts</filename> with IPv6 support:
-->
また、IPv6 をサポートした基本的な <filename>/etc/hosts</filename> 
は以下のようにします。

<informalexample><screen>
127.0.0.1 localhost DebianHostName

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
</screen></informalexample>

<!--
If you have multiple network cards, you should arrange the names of
driver modules in the <filename>/etc/modules</filename> file into the
desired order. Then during boot, each card will be associated with the
interface name (eth0, eth1, etc.) that you expect.
-->
複数のネットワークカードを持っているなら、
<filename>/etc/modules</filename> ファイルに希望の順番で、
ドライバモジュールの名前を配置してください。
その後起動中に、各カードは期待通りにインターフェース名 (eth0, eth1など) 
と結びつけられます。

</para>
   </sect3>

   <sect3>
<!--
   <title>Configure Apt</title>
-->
   <title>apt の設定</title>
<para>

<!--
Debootstrap will have created a very basic
<filename>/etc/apt/sources.list</filename> that will allow installing
additional packages. However, you may want to add some additional sources,
for example for source packages and security updates:
-->
debootstrap は、追加パッケージをインストールする、
非常に基本的な <filename>/etc/apt/sources.list</filename> を作成します。
しかし、他のパッケージ取得先を追加したくなると思います。
以下の例はソースパッケージとセキュリティ更新を追加しています。

<informalexample><screen>
deb-src http://ftp.us.debian.org/debian etch main

deb http://security.debian.org/ etch/updates main
deb-src http://security.debian.org/ etch/updates main
</screen></informalexample>

<!--
Make sure to run <userinput>aptitude update</userinput> after you have
made changes to the sources list.
-->
sources list を更新したら、
<userinput>aptitude update</userinput> を必ず実行してください。

</para>
   </sect3>

   <sect3>
<!--
   <title>Configure Locales and Keyboard</title>
-->
   <title>ロケールとキーボードの設定</title>
<para>

<!--
To configure your locale settings to use a language other than
English, install the <classname>locales</classname> support package
and configure it. Currently the use of UTF-8 locales is recommended.
-->
英語以外の言語を使用するようロケールの設定をするために、
ロケールをサポートするパッケージ 
(<classname>locales</classname>) をインストール・設定してください。
現在は UTF-8 ロケールを使用するのをお奨めします。

<informalexample><screen>
# aptitude install locales
# dpkg-reconfigure locales
</screen></informalexample>

<!--
To configure your keyboard (if needed):
-->
(必要なら) 以下のようにキーボードの設定を行ってください。

<informalexample><screen>
# aptitude install console-data
# dpkg-reconfigure console-data
</screen></informalexample>

</para><para>

<!--
Note that the keyboard cannot be set while in the chroot, but will be
configured for the next reboot.
-->
chroot 内では、キーボードを設定できませんが、
再起動後に有効になることに注意してください。

</para>
   </sect3>
  </sect2>

  <sect2>
  <title>カーネルのインストール</title>
<para>

<!--
If you intend to boot this system, you probably want a Linux kernel
and a boot loader. Identify available pre-packaged kernels with:
-->
このシステムを起動できるようにするなら、
おそらく Linux カーネルとブートローダが必要でしょう。
以下のようにして、パッケージ化済みカーネルを確認してください。

<informalexample><screen>
# apt-cache search linux-image
</screen></informalexample>

</para><para>

<!--
If you intend to use a pre-packaged kernel, you may want to create the
configuration file <filename>/etc/kernel-img.conf</filename> before you
do so. Here's an example file:
-->
パッケージ化済みカーネルを使用する予定であれば、
事前に <filename>/etc/kernel-img.conf</filename> 
設定ファイルを作成したくなると思います。
以下にサンプルファイルを掲げます。

<informalexample><screen>
# Kernel image management overrides
# See kernel-img.conf(5) for details
do_symlinks = yes
relative_links = yes
do_bootloader = yes
do_bootfloppy = no
do_initrd = yes
link_in_boot = no
</screen></informalexample>

</para><para>

<!--
For detailed information about this file and the various options, consult
its man page which will be available after installing the
<classname>kernel-package</classname> package. We recommend that you check
that the values are appropriate for your system.
-->
このファイルや様々なオプションの詳細は、
<classname>kernel-package</classname> パッケージインストール後に有効になる 
man ページで説明しています。
これで、あなたのシステムに適切な値をチェックするのをお奨めします。

</para><para>

<!--
Then install the kernel package of your choice using its package name.
-->
それから、選んだパッケージ名のカーネルパッケージをインストールしてください。

<informalexample><screen>
# aptitude install linux-image-<replaceable>&kernelversion;-arch-etc</replaceable>
</screen></informalexample>

<!--
If you did not create a <filename>/etc/kernel-img.conf</filename> before
installing a pre-packaged kernel, you may be asked some questions during
its installation that refer to it.
-->
パッケージ化済みカーネルをインストールする前に 
<filename>/etc/kernel-img.conf</filename> を作成しない場合、
インストール時に関連する質問が行われると思います。

</para>
  </sect2>

  <sect2>
<title>ブートローダのセットアップ</title>
<para>

<!--
To make your &debian; system bootable, set up your boot loader to load
the installed kernel with your new root partition. Note that
<command>debootstrap</command> does not install a boot loader, though you
can use <command>aptitude</command> inside your Debian chroot to do so.
-->
&debian; システムを起動できるようにするために、
インストールしたカーネルを新しい root パーティションから読み込むように、
ブートローダをセットアップしてください。<command>debootstrap</command> は、
ブートローダをインストールしないことに注意してください。
とは言っても、セットアップするのに Debian chroot 内部の 
<command>aptitude</command> を使用できます。

</para><para arch="x86">

<!--
Check <userinput>info grub</userinput> or <userinput>man
lilo.conf</userinput> for instructions on setting up the
bootloader.  If you are keeping the system you used to install Debian, just
add an entry for the Debian install to your existing grub 
<filename>menu.lst</filename> or <filename>lilo.conf</filename>.  For
<filename>lilo.conf</filename>, you could also copy it to the new system and
edit it there. After you are done editing, call <command>lilo</command>
(remember it will use
<filename>lilo.conf</filename> relative to the system you call it from).
-->
ブートローダのセットアップについての説明は、
<userinput>info grub</userinput> や 
<userinput>man lilo.conf</userinput> をチェックしてください。
Debian をインストールするのに使用したシステムを保持する場合、
既存の grub <filename>menu.lst</filename> や 
<filename>lilo.conf</filename> に、
Debian インストールへのエントリを単に加えてください。
lilo.conf では新システムにそれをコピーし、そこで編集してください。
編集を終えた後、<command>lilo</command> を呼び出してください。
(lilo を呼び出したシステムと関係あるところの、
<filename>lilo.conf</filename> が使われるということを覚えていてください)

</para><para arch="x86">

<!--
Installing and setting up <classname>grub</classname> is as easy as:
-->
<classname>grub</classname> のインストールと設定は以下のように簡単です。

<informalexample><screen>
# aptitude install grub
# grub-install /dev/<replaceable>hda</replaceable>
# update-grub
</screen></informalexample>

<!--
The second command will install <command>grub</command> (in this case in
the MBR of <literal>hda</literal>). The last command will create a sane
and working <filename>/boot/grub/menu.lst</filename>.
-->
2 つ目のコマンドで、<command>grub</command> を 
(この場合は <literal>hda</literal> の MBR に) インストールします。
最後のコマンドで、
正しく動作する <filename>/boot/grub/menu.lst</filename> を作成します。

</para><para arch="x86">

<!--
Here is a basic <filename>/etc/lilo.conf</filename> as an example:
-->
以下は基本的な <filename>/etc/lilo.conf</filename> の例です。

<informalexample><screen>
boot=/dev/<replaceable>hda6</replaceable>
root=/dev/<replaceable>hda6</replaceable>
install=menu
delay=20
lba32
image=/vmlinuz
label=Debian
</screen></informalexample>

</para><para arch="x86">

<!--
Depending on which bootloader you selected, you can now make some
additional changes in <filename>/etc/kernel-img.conf</filename>.
-->
選択したブートローダによって、
<filename>/etc/kernel-img.conf</filename> にさらに変更を加えても結構です。

</para><para arch="x86">

<!--
For the <classname>grub</classname> bootloader, you should
set the <literal>do_bootloader</literal> option to <quote>no</quote>.
And to automatically update your <filename>/boot/grub/menu.lst</filename>
on installation or removal of Debian kernels, add the following lines:
-->
<classname>grub</classname> ブートローダでは、
<literal>do_bootloader</literal>オプションに <quote>no</quote> 
とセットしてください。
また、Debian のカーネルをインストール・削除で
<filename>/boot/grub/menu.lst</filename> を自動的に更新するには、
以下の行を追加してください。

<informalexample><screen>
postinst_hook = update-grub
postrm_hook   = update-grub
</screen></informalexample>

<!--
For the <classname>lilo</classname> bootloader, the value of
<literal>do_bootloader</literal> needs to remain <quote>yes</quote>.
-->
<classname>lilo</classname> ブートローダでは、
<literal>do_bootloader</literal> の値は <quote>yes</quote> 
である必要があります。

</para><para arch="powerpc">

<!--
Check <userinput>man yaboot.conf</userinput> for instructions on
setting up the bootloader.  If you are keeping the system you used to
install Debian, just add an entry for the Debian install to your
existing <filename>yaboot.conf</filename>.  You could also copy it to
the new system and
edit it there. After you are done editing, call ybin (remember it will
use <filename>yaboot.conf</filename> relative to the system you call it from).
-->
ブートローダのセットアップについての説明は、
<userinput>man yaboot.conf</userinput> をチェックしてください。
Debian をインストールするのに使用したシステムを保持する場合、
既存の <filename>yaboot.conf</filename> に、
Debian インストールへのエントリを単に加えてください。
そして新システムにそれをコピーし、そこで編集してください。
編集を終えた後、ybin を呼び出してください。
(ybin を呼び出したシステムと関係あるところの、
<filename>yaboot.conf</filename> が使われるということを覚えていてください)

</para><para arch="powerpc">

<!--
Here is a basic <filename>/etc/yaboot.conf</filename> as an example:
-->
以下は基本的な <filename>/etc/yaboot.conf</filename> の例です。

<informalexample><screen>
boot=/dev/hda2
device=hd:
partition=6
root=/dev/hda6
magicboot=/usr/lib/yaboot/ofboot
timeout=50
image=/vmlinux
label=Debian
</screen></informalexample>

<!--
On some machines, you may need to use <userinput>ide0:</userinput>
instead of <userinput>hd:</userinput>.
-->
いくつかのマシンでは、
<userinput>hd:</userinput> の代わりに
<userinput>ide0:</userinput> を使う必要があるかもしれません。

</para>
  </sect2>

  <sect2>
<title>仕上げに</title>
<para>

<!--
As mentioned earlier, the installed system will be very basic. If you
would like to make the system a bit more mature, there is an easy method
to install all packages with <quote>standard</quote> priority:
-->
すでに述べたように、インストールしたシステムは非常に基本的な物になります。
もっと成熟したシステムにしたければ、
優先度が <quote>standard</quote> のパッケージを、
すべてインストールする簡単な方法があります。以下のようにしてください。

<informalexample><screen>
# tasksel install standard
</screen></informalexample>

<!--
Of course, you can also just use <command>aptitude</command> to install
packages individually.
-->
もちろん <command>aptitude</command> で、
個々のパッケージをインストールすることもできます。

</para><para>

<!--
After the installation there will be a lot of downloaded packages in
<filename>/var/cache/apt/archives/</filename>. You can free up some
diskspace by running:
-->
インストールが終わると、ダウンロードしたパッケージが 
<filename>/var/cache/apt/archives/</filename> に大量に残っています。
以下のようにして、ディスク領域を解放できます。

<informalexample><screen>
# aptitude clean
</screen></informalexample>

</para>
  </sect2>
 </sect1>
