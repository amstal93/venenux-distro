<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 39614 -->
<!-- traducido por jfs, 30 dic. 2005 -->
<!-- revisado por Igor Tamara, enero 2007 -->

 <sect1 id="rescue">
 <title>Recuperar un sistema roto</title>
<para>

Algunas veces las cosas salen mal y el sistema que ha instalado con mucho
cuidado no puede arrancar. Quiz�s se rompi� la configuraci�n del cargador de
arranque mientras estaba probando un cambio o quiz�s el nuevo n�cleo que
instal� ya no puede arrancar o quiz�s unos rayos c�smicos golpearon su
disco duro y cambiaron un bit de <filename>/sbin/init</filename>.
Independientemente de la raz�n del fallo necesitar� un sistema desde el 
cual poder trabajar para arreglarlo y el modo de rescate puede ser �til para 
esto.

</para><para>

<!-- TODO: describe what to do on arches where this isn't set up in the
     bootloader -->

Para acceder al modo de rescate tiene que introducir
<userinput>rescue</userinput> en el cursor <prompt>boot:</prompt> o arrancar
con el par�metro de arranque <userinput>rescue/enable=true</userinput>. Se le
mostrar�n algunas de las primeras pantallas del instalador, con una nota en el
margen inferior de la pantalla que indica que est� en el modo de rescate y no
en una instalaci�n completa. No se preocupe, �no se va a sobreescribir su
sistema! El modo de rescate s�lo se aprovecha de las funciones de detecci�n
autom�tica de hardware que tiene el sistema de instalaci�n para asegurarse de
que tiene acceso a sus discos y a sus dispositivos de red mientras repara el
sistema.

</para><para>

Se le mostrar� la lista de particiones de su sistema en lugar de la herramienta
de particionado y se le pedir� que seleccione una de �stas. Por regla 
general seleccionar� la partici�n que contiene la ra�z del sistema de ficheros 
que necesita reparar. Puede seleccionar tambi�n particiones en dispositivos RAID
o LVM as� como las creadas directamente en los discos.

</para><para>

El instalador le presentar�, si puede, un int�rprete de l�nea de �rdenes en
el sistema de ficheros que ha seleccionado para que lleve a cabo las reparaciones
que necesite.

<phrase arch="x86">
Por ejemplo, si necesita reinstalar el cargador de arranque GRUB en el 
<quote>master boot record</quote> del primer disco duro, puede ejecutar la orden
<userinput>grub-install '(hd0)'</userinput> para hacerlo.
</phrase>

</para><para>

Si el instalador no puede ejecutar un int�rprete de �rdenes usable en el
sistema de ficheros ra�z que ha seleccionado quiz�s se deba a que el sistema de
ficheros se ha corrompido. En este caso se le mostrar� un aviso y se ofrecer� a
darle un int�rprete de �rdenes en el entorno de instalaci�n en lugar de en la
ra�z. Puede que no tenga tantas herramientas a su alcance en este entorno, pero
deber�an ser suficientes para reparar su sistema. El sistema de ficheros ra�z
que haya seleccionado estar� montado en el directorio
<filename>/target</filename>.

</para><para>

En cualquier caso, el sistema se reiniciar� autom�ticamente cuando salga del
int�rprete de �rdenes.

</para><para>

Una �ltima nota: reparar un sistema roto puede ser dif�cil y este manual no
intentar� detallar todas las cosas que pueden ir mal y c�mo arreglarlas. Si
tiene alg�n problema, consulte con un experto.

</para>
 </sect1>
