<!-- retain these comments for translator revision tracking -->
<!-- original version: 43693 untranslated -->


  <sect2 arch="arm"><title>CPU, Main Boards, and Video Support</title>

<para>

Each distinct ARM architecture requires its own kernel. Because of
this the standard Debian distribution only supports installation on
a number of the most common platforms. The Debian userland however may be
used by <emphasis>any</emphasis> ARM CPU.

</para>

<para>

Most ARM CPUs may be run in either endian mode (big or little). However,
the majority of current system implementation uses little-endian mode.
Debian currently only supports little-endian ARM systems.

</para>

<para>

The supported platforms are:

<variablelist>

<varlistentry>
<term>Netwinder</term>
<listitem><para>

This is actually the name for the group of machines
based upon the StrongARM 110 CPU and Intel 21285 Northbridge (also known
as Footbridge). It
comprises of machines like: Netwinder (possibly one of the most common ARM
boxes), CATS (also known as the EB110ATX), EBSA 285 and Compaq
personal server (cps, aka skiff).

</para></listitem>
</varlistentry>

<varlistentry>
<term>IOP32x</term>
<listitem><para>

Intel's I/O Processor (IOP) line is found in a number of products related
to data storage and processing.  Debian currently supports the IOP32x
platform, featuring the IOP 80219 and 32x chips commonly found in Network
Attached Storage (NAS) devices.  Debian explicitly supports two such
devices: the <ulink url="&url-arm-cyrius-glantank;">GLAN Tank</ulink> from
IO-Data and the <ulink url="&url-arm-cyrius-n2100;">Thecus N2100</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>IXP4xx</term>
<listitem><para>

The IXP4xx platform is based on Intel's XScale ARM core.  Currently, only
one IXP4xx based system is supported, the Linksys NSLU2.
The Linksys NSLU2 (Network Storage Link for USB 2.0 Disk Drives) is a small
device which allows you to easily provide storage via the network.  It
comes with an Ethernet connection and two USB ports to which hard drives
can be connected.  There is an external site with <ulink
url="&url-arm-cyrius-nslu2;">installation instructions</ulink>.

</para></listitem>
</varlistentry>

<varlistentry>
<term>RiscPC</term>
<listitem><para>

This machine is the oldest supported hardware but support for it in
our new installer is incomplete.
It has RISC OS in ROM, Linux can be booted from that OS using
linloader. The RiscPC has a modular CPU card and typically has a 30MHz
610, 40MHz 710 or 233MHz Strongarm 110 CPU fitted. The mainboard has
integrated IDE, SVGA video, parallel port, single serial port, PS/2
keyboard and proprietary mouse port. The proprietary module expansion
bus allows for up to eight expansion cards to be fitted depending on
configuration, several of these modules have Linux drivers.

</para></listitem>
</varlistentry>

</variablelist>

</para>
  </sect2>
