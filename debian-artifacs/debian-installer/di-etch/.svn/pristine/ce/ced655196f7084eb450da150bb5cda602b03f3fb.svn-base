<!-- retain these comments for translator revision tracking -->
<!-- original version: 44580 -->


 <sect1 condition="gtk" id="graphical">
 <!-- <title>The Graphical Installer</title> -->
 <title>Installatore grafico</title>
<para>

<!--
The graphical version of the installer is only available for a limited
number of architectures, including &arch-title;. The functionality of
the graphical installer is essentially the same as that of the regular
installer as it basically uses the same programs, but with a different
frontend.
-->

La versione grafica dell'installatore è disponibile soltanto su un numero
limitato di architetture, fra le quali &arch-title;. La funzionalità
dell'installatore grafico è quasi identica a quella dell'installatore
tradizionale dato che entrambe le versioni hanno come base gli stessi
programmi e differiscono soltanto per l'interfaccia.

</para><para>

<!--
Although the functionality is identical, the graphical installer still has
a few significant advantages. The main advantage is that it supports more
languages, namely those that use a character set that cannot be displayed
with the regular <quote>newt</quote> frontend. It also has a few usability
advantages such as the option to use a mouse, and in some cases several
questions can be displayed on a single screen.
-->

Nonostante la funzionalità sia identica, l'installatore grafico ha alcuni
piccoli ma significativi vantaggi. Il vantaggio principale è che può
supportare un numero maggiore di lingue, cioè le lingue che usano un set
di caratteri che non può essere visualizzato con la tradizionale interfaccia
<quote>newt</quote>. Ha anche vantaggi di usabilità, infatti è possibile
usare il mouse e, in alcuni casi, le domande possono mostrate all'interno
di un'unica schermata.

</para><para arch="x86">

<!--
The graphical installer is available with all CD images and with the
hd-media installation method. As the graphical installer uses a separate
(much larger) initrd than the regular installer, it has to be booted using
<userinput>installgui</userinput> instead of <userinput>install</userinput>.
Analogous, the expert and rescue modes are booted using
<userinput>expertgui</userinput> and <userinput>rescuegui</userinput>
respectively.
-->

L'Installatore grafico è disponibile in tutte le immagini di CD e con il
metodo d'installazione da disco fisso. L'installatore grafico usa un initrd
diverso (molto più grande) dall'installatore tradizionale, e si avvia usando
<userinput>installgui</userinput> al posto di <userinput>install</userinput>.
In modo analogo le modalità esperto e ripristino si avviano rispettivamente
con <userinput>expertgui</userinput> e <userinput>rescuegui</userinput>.

</para><para arch="x86">

<!--
It is also available as a special <quote>mini</quote> ISO
image<footnote id="gtk-miniiso">
-->

Inoltre è disponibile un'immagine ISO speciale denominata
<quote>mini</quote><footnote id="gtk-miniiso">

<para>

<!--
The mini ISO image can be downloaded from a Debian mirror as described
in <xref linkend="downloading-files"/>. Look for <quote>gtk-miniiso</quote>.
-->

L'immagine ISO mini può essere scaricata da qualsiasi mirror Debian con
la stessa procedura descritta in <xref linkend="downloading-files"/>. Si
cerchi <quote>gtk-miniiso</quote>.

</para>

<!--
</footnote>, which is mainly useful for testing; in this case the image is
booted just using <userinput>install</userinput>. There is no graphical
installer image that can be netbooted.
-->

</footnote>, che è particolarmente utile per i test; con questa immagine
l'avvio avviene semplicemente usando <userinput>install</userinput>. Non
esistono immagini dell'installatore grafico che possono essere avviate
dalla rete.

</para><para arch="powerpc">

<!--
For &arch-title;, currently only an experimental <quote>mini</quote> ISO
image is available<footnote id="gtk-miniiso">
-->

Al momento per &arch-title; è disponibile soltanto un'immagine ISO
sperimentale denominata <quote>mini</quote><footnote id="gtk-miniiso">

<para>

<!--
The mini ISO image can be downloaded from a Debian mirror as described
in <xref linkend="downloading-files"/>. Look for <quote>gtk-miniiso</quote>.
-->

L'immagine ISO mini può essere scaricata da qualsiasi mirror Debian con
la stessa procedura descritta in <xref linkend="downloading-files"/>. Si
cerchi <quote>gtk-miniiso</quote>.

</para>

<!--
</footnote>. It should work on almost all PowerPC systems that have
an ATI graphical card, but is unlikely to work on other systems.
-->

</footnote>. Dovrebbe funzionare sulla maggior parte dei sistemi PowerPC
con scheda video ATI, ma è improbabile che funzioni su altri sistemi.

</para><para>

<!--
The graphical installer requires significantly more memory to run than
the regular installer: &minimum-memory-gtk;. If insufficient memory is
available, it will automatically fall back to the regular
<quote>newt</quote> frontend.
-->

L'uso dell'installatore grafico richiede molta più memoria rispetto
all'installatore tradizionale: almeno &minimum-memory-gtk;. Se la memoria
disponibile non è sufficiente, viene automaticamente attivata l'interfaccia
<quote>newt</quote>.

</para><para>

<!--
You can add boot parameters when starting the graphical installer, just as
with the regular installer. One of those parameters allows you to configure
the mouse for left-handed use. See <xref linkend="boot-parms"/> for valid
parameters.
-->

Come per l'installatore tradizionale è possibile aggiungere dei parametri
d'avvio anche per l'installatore grafico. Uno di questi parametri permette
di configurare l'uso del mouse con la mano sinistra; si consulti in
<xref linkend="boot-parms"/> quali sono i parametri validi.

</para>

  <sect2 id="gtk-using">
  <!-- <title>Using the graphical installer</title> -->
  <title>Uso dell'installatore grafico</title>
<para>

<!--
As already mentioned, the graphical installer basically works the same as
the regular installer and thus the rest of this manual can be used to guide
you through the installation process.
-->

Come già detto, l'installatore grafico funziona avendo come base gli stessi
programmi della versione tradizionale e quindi questo manuale può essere
usato come guida durante il processo d'installazione.

</para><para>

<!--
If you prefer using the keyboard over the mouse, there are two things you
need to know. To expand a collapsed list (used for example for the selection
of countries within continents), you can use the <keycap>+</keycap> and
<keycap>-</keycap> keys. For questions where more than one item can be
selected (e.g. task selection), you first need to tab to the
<guibutton>Continue</guibutton> button after making your selections; hitting
enter will toggle a selection, not activate <guibutton>Continue</guibutton>.
-->

Se si preferisce usare la tastiera al posto del mouse si devono tenere
presenti un paio di cose. Per espandere un elenco chiuso (per esempio la
selezione dei paesi all'interno dei continenti) si possono usare i tasti
<keycap>+</keycap> e <keycap>-</keycap>. Per domande che ammettono una
risposta multipla (per esempio la selezione dei task) dopo aver effettuato
le scelte si deve usare usare il tasto tab per spostarsi su
<guibutton>Continua</guibutton>; la pressione del tasto invio cambia lo
stato dell'ultima selezione e non attiva <guibutton>Continua</guibutton>.

</para><para>

<!--
To switch to another console, you will also need to use the
<keycap>Ctrl</keycap> key, just as with the X Window System. For example,
to switch to VT1 you would use: <keycombo> <keycap>Ctrl</keycap>
<keycap>Left Alt</keycap> <keycap>F1</keycap> </keycombo>.
-->

Per passare su un'altra console è necessario usare anche il tasto
<keycap>Ctrl</keycap>, esattamente come in X Window System. Per esempio,
per passare al VT1 si devono premere <keycombo><keycap>Ctrl</keycap>
<keycap>Alt</keycap> <keycap>F1</keycap></keycombo>.

</para>
  </sect2>

  <sect2 id="gtk-issues">
  <!-- <title>Known issues</title> -->
  <title>Problemi noti</title>
<para>

<!--
Etch is the first release that includes the graphical installer and uses
some relatively new technology. There are a few known issues that you may
run into during the installation. We expect to be able to fix these issues
for the next release of &debian;.
-->

Etch è la prima release che include l'installatore grafico il quale usa una
tecnologia relativamente nuova. Ci sono alcuni problemi noti che potrebbero
manifestarsi durante l'installazione. La correzione di questi problemi è
prevista per la prossima release di &debian;.

</para>

<itemizedlist>
<listitem><para>

<!--
Information on some screens is not nicely formatted into columns as it
should be. The most obvious example is the first screen where you select
your language. Another example is the main screen of partman.
-->

In alcune schermate le informazioni non sono ben formattate in colonne.
L'esempio più evidente è la schermata dalla quale si può scegliere la
lingua. Un altro esempio è la schermata principale di partman.

</para></listitem>
<listitem><para>

<!--
Typing some characters may not work and in some cases the wrong character
may be printed. For example, "composing" a character by typing an accent and
then the letter over/under which the accent should appear does not work.
-->

In alcuni casi all'inserimento di certi caratteri corrisponde la
presentazione a video di un carattere diverso. Per esempio, la composizione
un carattere digitando prima l'accento e poi la lettera sopra/sotto cui
deve apparire l'accento non funziona.

</para></listitem>
<listitem><para>

<!--
Support for touchpads is not yet optimal.
-->

Il supporto per i touchpad non è ancora perfetto.

</para></listitem>
<listitem><para>

<!--
You should not switch to a different console while the installer is busy;
this may cause the frontend to crash. The frontend will be restarted
automatically, but this may still cause problems with the installation.
Switching to another console while the installer is waiting for input
should work without causing any problems.
-->

Non si deve passare a un'altra console quando è in corso un'elaborazione;
si potrebbe causare un blocco dell'interfaccia. L'interfaccia viene riavviata
automaticamente ma potrebbero continuare a manifestarsi dei problemi durante
l'installazione. Il cambio di console quando l'installatore è in attesa
dell'input funziona senza causare alcun problema.

</para></listitem>
<listitem><para>

<!--
Support for creating encrypted partitions is limited as it is not possible
to generate a random encryption key. It is possible to set up an encrypted
partition using a passphrase as encryption key.
-->

Il supporto per la creazione di partizioni cifrate è limitato: non è
possibile creare una chiave di cifratura casuale, invece è possibile
impostare una partizione cifrata con una passphrase come chiave di
cifratura.

</para></listitem>
<listitem><para>

<!--
Starting a shell from the graphical frontend is currently not supported.
This means that relevant options to do that (which are available when you
use the textual frontend), will not be shown in the main menu of the
installation system and in the menu for the rescue mode. You will instead
have to switch (as described above) to the shells that are available on
virtual consoles VT2 and VT3.
-->

Attualmente non è possibile avviare una shell dall'installatore grafico.
Questo vuol dire che le opzioni che permettono l'attivazione di una shell
(disponibili quando si usa l'interfaccia testuale) non sono visualizzate
nel menu principale del sistema d'installazione e nemmeno nel menu usato
nella modalità di ripristino. Comunque è possibile usare (come descritto
in precedenza) le shell disponibili sulle console virtuali VT2 e VT3.

</para><para>

<!--
After booting the installer in rescue mode, it may be useful to start a shell
in the root partition of an already installed system. This is possible (after
you have selected the partition to be mounted as the root partition) by
switching to VT2 or VT3 and entering the following command:
-->

Dopo aver avviato l'installatore in modalità di ripristino, si potrebbe
avere la necessità di usare una shell sulla root del sistema già installato.
Questo è possibile (dopo aver scelto quale partizione deve essere montata
come partizione di root) inserendo dal VT2 o dal VT3 il seguente comando:

<informalexample><screen>
# chroot /target
</screen></informalexample>

</para></listitem>
</itemizedlist>
  </sect2>
 </sect1>
