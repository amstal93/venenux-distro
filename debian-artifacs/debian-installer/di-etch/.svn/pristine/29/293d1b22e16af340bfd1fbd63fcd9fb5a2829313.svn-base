<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43841 -->

  <sect2 arch="ia64"><title>Amorcer depuis un c�d�rom</title>

&boot-installer-intro-cd.xml;

<note>
<title>Contenus du c�d�rom</title>
 
<para>
Trois variantes des c�d�roms d'installation Debian existent.
Le c�d�rom <emphasis>Business Card</emphasis> offre une installation
minimale qui tient sur ce support de petite taille. Il exige une connexion
au r�seau pour installer le reste du syst�me de base et rendre le syst�me
utilisable. Le c�d�rom <emphasis>Network Install</emphasis> poss�de tous les
paquets du syst�me de base, mais il exige une connexion au r�seau pour 
installer les paquets suppl�mentaires n�cessaires � un syst�me complet.
L'ensemble des c�d�roms Debian peut installer un syst�me complet sans avoir 
besoin du r�seau.
</para>
</note>

<para>

L'architecture IA-64 utilise la nouvelle interface
<quote>Extensible Firmware Interface</quote> (EFI) d'Intel. Contrairement au
traditionnel BIOS x86 qui ne conna�t rien du p�riph�rique de d�marrage
sinon la table des partitions et le secteur principal d'amor�age (MBR), le 
programme EFI peut lire et �crire des fichiers provenant d'une partition FAT16
ou FAT32. Cela simplifie le processus souvent myst�rieux de l'amor�age du 
syst�me. Le programme d'amor�age du syst�me et le firmware EFI poss�dent un 
syst�me de fichiers complet pour stocker les fichiers n�cessaires au d�marrage
du syst�me. Cela signifie que le disque syst�me sur un syst�me IA-64 poss�de 
une partition additionnelle d�di�e � EFI � la place du simple MBR ou du bloc 
de d�marrage sur les syst�mes plus conventionnels.

</para>
<para>

Le c�d�rom Debian contient une petite partition EFI sur laquelle r�sident le 
programme d'amor�age <command>ELILO</command>, son fichier de configuration, 
le noyau de l'installateur ainsi qu'un syst�me de fichiers initial (initrd). 
Le syst�me en marche contient aussi une partition EFI sur laquelle r�sident 
les fichiers n�cessaires au d�marrage du syst�me. Ces fichiers sont lisibles 
depuis le shell EFI comme d�crit plus bas.

</para>
<para>

La plupart des d�tails sur la mani�re dont <command>ELILO</command> se charge 
et d�marre le syst�me sont transparents pour l'installateur du syst�me. 
Toutefois, celui-ci doit d'abord mettre en place une partition EFI avant 
d'installer le syst�me de base. Autrement, l'installation de 
<command>ELILO</command> �chouera rendant le syst�me non amor�able. La 
partition EFI est allou�e et format�e lors de l'�tape partitionnement de 
l'installation, avant le chargement des paquets sur le disque syst�me. La 
t�che de partitionnement v�rifie aussi qu'une partition EFI convenable est 
pr�sente avant de permettre l'installation. 

</para><para>

Le gestionnaire de d�marrage EFI est la derni�re �tape de l'initialisation du 
firmware. Il montre un menu o� l'utilisateur peut choisir une option. 
D�pendant du mod�le du syst�me ainsi que des autres programmes charg�s, ce 
menu peut �tre diff�rent d'un syst�me � l'autre. Il devrait y avoir au moins 
deux menus affich�s, <command>Boot Option Maintenance Menu</command> et
<command>EFI Shell (Built-in)</command>. L'utilisation de la premi�re option 
est conseill�e&nbsp;; n�anmoins si cette option n'est pas disponible, ou si 
le c�d�rom ne peut pas s'amorcer, utilisez la seconde option.

</para>

  <warning>
  <title>IMPORTANT</title>
<para>

Le gestionnaire de d�marrage EFI choisira une action de d�marrage par d�faut, 
typiquement le premier choix du menu, apr�s un nombre pr�d�fini de secondes. 
C'est indiqu� par un compte � rebours situ� au bas de l'�cran. Une fois ce 
temps expir� et l'action par d�faut lanc�e, vous aurez peut-�tre besoin de 
red�marrer la machine afin de continuer l'installation. Si l'action par 
d�faut est le shell EFI, vous pouvez retourner au gestionnaire d'amor�age en 
tapant <command>exit</command> � l'invite du shell.

</para>
  </warning>

 <sect3 arch="ia64" id="bootable-cd">
  <title>Option 1 : Amorcer � partir du menu <quote>Boot Option Maintenance</quote></title>
<para>

</para>

<itemizedlist>

<listitem><para>

Ins�rez le c�d�rom dans le lecteur DVD/CD et r�amorcez la machine. Le 
firmware affichera la page du gestionnaire d'amor�age EFI ainsi que le menu 
apr�s avoir termin� l'initialisation du syst�me.

</para></listitem>

<listitem><para>

S�lectionnez <command>Boot Maintenance Menu</command> du menu avec les 
touches fl�ches et appuyez sur <command>ENTER</command>. Un nouveau menu 
s'affichera.

</para></listitem>

<listitem><para>

S�lectionnez <command>Amorcer depuis un fichier</command> depuis le menu avec 
les touches fl�ches et appuyez sur <command>ENTER</command>. Une liste des
p�riph�riques test�s par le firmware s'affichera. Vous devriez voir deux
lignes de menu contenant soit l'�tiquette 
<command>Debian Inst [Acpi ...</command> soit 
<command>Removable Media Boot</command>. Si vous examinez le reste de la 
ligne de menu, vous noterez que les informations sur le p�riph�rique et sur
le contr�leur sont les m�mes. 

</para></listitem>

<listitem><para>

Vous pouvez choisir n'importe quelle entr�e qui se r�f�re au lecteur DVD/CD. 
Faites votre choix � l'aide des touches fl�ches et appuyez sur 
<command>ENTER</command>. Si vous choisissez 
<command>Removable Media Boot</command>, la machine d�butera imm�diatement la 
s�quence de d�marrage. Si vous choisissez plut�t 
<command>Debian Inst [Acpi ...</command>, il affichera le contenu de la 
partie amor�able du c�d�rom, vous demandant de passer � la prochaine �tape.

</para></listitem>

<listitem><para>

Vous n'aurez besoin de cette �tape que si vous choisissez
<command>Debian Inst [Acpi ...</command>. L'affichage du contenu du 
r�pertoire montrera �galement 
<command>[Treat like Removable Media Boot]</command> sur l'avant derni�re 
ligne. S�lectionnez cette ligne avec les touches fl�ches et appuyez sur 
<command>ENTER</command>. Cela lancera la s�quence d'amor�age.

</para></listitem>

</itemizedlist>
<para>

Ces �tapes lancent le programme d'amor�age de Debian qui affichera une page 
de menu vous permettant de s�lectionner un noyau et des options. Poursuivez 
en choisissant le noyau et les options.

</para>
</sect3>


  <sect3 arch="ia64" id="boot-with-efi">
  <title>Option 2 : Amorcer depuis le Shell EFI</title>
<para>

Si pour une raison ou pour une autre, l'option 1 n'est pas couronn�e de
succ�s, relancez la machine et lorsque l'�cran du gestionnaire d'amor�age EFI 
appara�t, il devrait y avoir une option appel�e
<command>EFI Shell [Built-in]</command>. Amorcez le c�d�rom d'installation de 
Debian en suivant ces �tapes&nbsp;: 

</para>

<itemizedlist>

<listitem><para>

Ins�rez le c�d�rom dans le lecteur DVD/CD et r�amorcez la machine. Le
firmware affichera la page et le menu du gestionnaire d'amor�age EFI apr�s 
avoir termin� l'initialisation du syst�me.

</para></listitem>

<listitem><para>

S�lectionnez <command>EFI Shell</command> depuis le menu avec les touches
fl�ches et appuyez sur <command>ENTER</command>. Le shell EFI va examiner 
tous les p�riph�riques amor�ables et les affichera sur le terminal avant 
d'afficher son invite. Les partitions amor�ables reconnues sur les 
p�riph�riques auront un nom comme &nbsp;:
<filename>fs<replaceable>n</replaceable>:</filename>. Toutes les autres 
partitions seront nomm�es comme&nbsp;:
<filename>blk<replaceable>n</replaceable>:</filename>.
Si vous avez ins�r� le c�d�rom juste avant d'entrer dans le Shell, cet examen 
pourrait prendre quelques secondes de plus car il initialise le lecteur CD.

</para>
</listitem>

<listitem><para>

Examinez la sortie du shell en cherchant le lecteur de c�d�rom. C'est tr�s 
probablement le p�riph�rique <filename>fs0:</filename> bien que que d'autres 
p�riph�riques ayant une partition amor�able seront affich�es comme
<filename>fs<replaceable>n</replaceable></filename>.
 
</para></listitem>

<listitem><para>

Entrez <command>fs<replaceable>n</replaceable>:</command> et appuyez sur
<command>ENTER</command> afin de s�lectionner le p�riph�rique ayant 
<replaceable>n</replaceable> comme num�ro de partition pour le c�d�rom. 
Le shell affichera maintenant le num�ro de partition dans son invite.

</para></listitem>

<listitem><para>

Entrez <command>elilo</command> et appuyez sur <command>ENTER</command>. 
Cela lancera la s�quence d'amor�age.

</para></listitem>

</itemizedlist>

<para>

Comme avec l'option 1, ces �tapes d�marrent le programme d'amor�age Debian 
qui affichera une page de menu vous permettant de s�lectionner un noyau et 
des options. Vous pouvez �galement entrer la commande plus courte
<command>fs<replaceable>n</replaceable>:elilo</command> � l'invite du shell. 
Poursuivez en choisissant le noyau et les options.

</para>
</sect3>

  <sect3 arch="ia64" id="serial-console">
  <title>Installer avec une console s�rie</title>

<para>

Pour faire l'installation, vous pouvez choisir d'utiliser un �cran et un 
clavier ou d'utiliser une connexion s�rie. Pour choisir une installation
�cran/clavier, s�lectionnez une option contenant la cha�ne de caract�re 
[VGA console]. Pour installer via une connexion s�rie, choisissez une option 
contenant la cha�ne de caract�re [<replaceable>BAUD</replaceable> baud serial 
console], o� <replaceable>BAUD</replaceable> est la vitesse de votre terminal
s�rie. Les articles du menu pour les r�glages les plus typiques des taux sur 
le p�riph�rique ttyS0 sont pr�configur�s. 
</para>

<para>
Dans la plupart des cas, vous voudrez que l'installateur utilise le m�me taux 
que celui de votre connexion au terminal EFI. Si vous n'�tes pas s�r de ce 
r�glage, vous pouvez l'obtenir gr�ce � la commande <command>baud</command>
entr�e dans le shell EFI.
</para>

<para>
Si aucune des options disponibles n'est appropri�e � votre p�riph�rique s�rie 
ou � votre taux baud, vous pouvez passer outre les r�glages de la console
pour une des options existantes du menu. Par exemple, pour utiliser une 
console avec un taux de 57600 bauds sur le p�riph�rique ttyS1, entrez
<command>console=ttyS1,57600n8</command> dans la fen�tre 
<classname>Boot:</classname>.
</para>

<note><para>

La plupart des machines IA-64 sont livr�es avec une console r�gl�e sur
9600 bauds. Ce r�glage est plut�t lent, et le processus normal d'installation 
prendra un temps relativement long pour afficher chaque �cran. Vous devriez 
augmenter le taux baud utilis� pour l'installation, ou alors utiliser une 
installation en mode texte. Voyez le menu d'aide
<classname>Params</classname> pour des instructions sur la mani�re de lancer 
l'installateur en mode texte.
</para></note>

<warning><para>
Si vous choisissez le mauvais type de console, vous aurez la possibilit� de 
choisir le noyau et d'entrer les param�tres, mais tant le terminal que vos 
choix seront perdus d�s que le noyau se chargera, vous obligeant � r�amorcer 
la machine avant de pouvoir d�buter l'installation.
</para></warning>
</sect3>

<sect3 arch="ia64" id="kernel-option-menu">
  <title>S�lection du noyau et des options</title>

<para>

Le programme d'amor�age affichera un formulaire avec une liste et une
fen�tre de texte avec une invite<classname>Boot:</classname>. Les touches
fl�ches s�lectionnent un article du menu et tout texte entr� au clavier
appara�tra dans la fen�tre de texte. Il y a aussi des �crans d'aide qui
peuvent �tre affich�s en appuyant sur la touche de fonction appropri�e.
L'�cran d'aide <classname>General</classname> explique les choix du menu et
l'�cran <classname>Params</classname> expliquent les options communes de
la ligne de commande.

</para>

<para>

Consultez l'�cran d'aide <classname>General</classname> pour la description 
des noyaux et des modes d'installation les plus appropri�s pour votre 
installation. Vous devriez aussi consulter <xref linkend="boot-parms"/> 
plus bas pour tous les param�tres additionnels que vous voudriez r�gler dans 
la fen�tre de texte <classname>Boot:</classname>. La version du noyau que vous
choisissez s�lectionne la version du noyau qui sera utilis�e tant pour le 
processus d'installation que pour le syst�me install�. Les deux prochaines 
�tapes s�lectionneront et lanceront l'installation&nbsp;: 

</para>

<itemizedlist>

<listitem><para>
S�lectionnez avec les touches fl�ches la version du noyau et le mode 
d'installation le plus appropri� � vos besoins.
</para></listitem>

<listitem><para>
Entrez les param�tres de d�marrage au clavier. Le texte sera affich�
directement dans la fen�tre de texte. C'est l� que les param�tres du noyau 
sont sp�cifi�s (tels que les r�glages de la console s�rie).
</para></listitem>

<listitem><para>
Appuyez sur <command>ENTER</command>. Le noyau sera charg�. Il affichera les 
messages d'initialisation usuels suivis par le premier �cran de 
l'installateur Debian.
</para></listitem>

</itemizedlist>

<para>

Passez au prochain chapitre pour continuer l'installation. L� vous pourrez 
r�gler la localisation, le r�seau et les partitions des disques.

</para>
</sect3>
  </sect2>

<sect2 arch="ia64" id="boot-tftp"><title>Amorcer avec TFTP</title>

<para>

Amorcer un syst�me IA-64 depuis le r�seau est similaire � amorcer depuis un 
c�d�rom. La seule diff�rence est la mani�re dont le noyau est charg�. Le 
gestionnaire d'amor�age EFI peut charger et lancer des programmes depuis un 
serveur sur le r�seau. Une fois que le noyau d'installation est charg� et 
d�marr�, l'installation du syst�me suivra les m�mes �tapes que l'installation 
depuis un c�d�rom avec pour seule exception que les paquets seront charg�s
depuis le r�seau plut�t que depuis le c�d�rom.

</para>

&boot-installer-intro-net.xml;

<para>

Amorcer un syst�me IA-64 depuis le r�seau demande deux actions sp�cifiques � 
l'architecture. Sur le serveur d'amor�age, DHCP et TFTP doivent �tre 
configur�s afin de distribuer <command>elilo</command>. Sur le client, une 
nouvelle option d'amor�age doit �tre d�finie dans le gestionnaire d'amor�age 
EFI afin de permettre le chargement via le r�seau.

</para>

  <sect3 arch="ia64" id="boot-tftp-server">
    <title>Configuration du serveur</title>

<para>

Une entr�e TFTP convenable pour l'amor�age via le r�seau ressemble � 
ceci&nbsp;:

<informalexample><screen>
host mcmuffin {
        hardware ethernet 00:30:6e:1e:0e:83;
        fixed-address 10.0.0.21;
        filename "debian-installer/ia64/elilo.efi";
}
</screen></informalexample>

Notez que le but est d'obtenir que <command>elilo.efi</command> soit lanc� 
sur le client.

</para><para>

Extrayez le fichier <filename>netboot.tar.gz</filename> dans le r�pertoire 
racine de votre serveur tftp. Typiquement, les r�pertoires racine du serveur 
tftp incluent <filename>/var/lib/tftp</filename> et
<filename>/tftpboot</filename>. Cela cr�era un r�pertoire
<filename>debian-installer</filename> contenant les fichiers de d�marrage 
pour un syst�me IA-64.

</para><para>


<informalexample><screen>
# cd /var/lib/tftp
# tar xvfz /home/user/netboot.tar.gz
./
./debian-installer/
./debian-installer/ia64/
[...]
</screen></informalexample>

Le fichier <filename>netboot.tar.gz</filename> contient un fichier
<filename>elilo.conf</filename> qui devrait fonctionner avec la plupart des 
configurations. Toutefois, si vous �tes amen� � modifier ce fichier, vous 
pouvez le trouver dans le r�pertoire 
<filename>debian-installer/ia64/</filename>. 

Il est possible d'avoir des fichiers de configurations diff�rents pour
diff�rents clients en les nommant selon l'adresse IP du client en hexad�cimal,
avec un suffixe <filename>.conf</filename>, � la place du fichier 
<filename>elilo.conf</filename>. Pour plus de d�tails, voyez la documentation 
fournie dans le paquet <classname>elilo</classname>.

</para>
    </sect3>

<sect3 arch="ia64" id="boot-tftp-client">
    <title>Configuration du client</title>

<para>

Pour configurer le client et permettre le d�marrage par TFTP, commencez par 
amorcer EFI et entrez dans le menu <guimenu>Boot Option Maintenance</guimenu>.

<itemizedlist>
<listitem><para>

Ajoutez une option de d�marrage.

</para></listitem>
<listitem><para>

Vous devriez voir une ou plusieurs lignes contenant le texte
<guimenuitem>Load File [Acpi()/.../Mac()]</guimenuitem>. Si plus d'une de ces
 entr�es existent, choisissez celle contenant l'adresse MAC de l'interface 
depuis laquelle vous allez amorcer. Utilisez les touches fl�ches pour mettre en
�vidence votre choix, et ensuite appuyez sur entr�e.

</para></listitem>
<listitem><para>

Nommez l'entr�e <userinput>Netboot</userinput> ou quelque chose de semblable, 
sauvegardez, et retournez au menu des options de d�marrage. 

</para></listitem>
</itemizedlist>


Vous devriez voir la nouvelle option de d�marrage que vous venez de cr�er, et 
en la s�lectionnant, cela devrait amorcer une requ�te DHCP, menant au 
chargement depuis le serveur via TFTP du fichier 
<filename>elilo.efi</filename>.

</para><para>

Le programme d'amor�age affichera son invite d�s qu'il aura t�l�charg� et 
trait� son fichier de configuration. � ce stade, l'installation commence avec 
les m�mes �tapes qu'une installation via c�d�rom. S�lectionnez une option de 
d�marrage comme ci-dessus et lorsque le noyau aura termin� de s'installer
depuis le r�seau, il lancera l'installateur Debian.

</para><para>

Allez au prochain chapitre pour poursuivre l'installation. L� vous pourrez 
r�gler la localisation, le r�seau et les partitions des disques. 

</para>
  </sect3>
  </sect2>










