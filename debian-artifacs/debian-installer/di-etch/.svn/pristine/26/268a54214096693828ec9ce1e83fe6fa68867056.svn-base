<!-- retain these comments for translator revision tracking -->
<!-- original version: 28672 untranslated -->

 <sect1 id="official-cdrom">
 <title>Official &debian; CD-ROM Sets</title>
<para>

By far the easiest way to install &debian; is from an Official
Debian CD-ROM Set. You can buy a set from a vendor (see the
<ulink url="&url-debian-cd-vendors;">CD vendors page</ulink>).
You may also download the CD-ROM images from a Debian mirror and make
your own set, if you have a fast network connection and a CD burner
(see the <ulink url="&url-debian-cd;">Debian CD page</ulink> for
detailed instructions). If you have a Debian CD set and CDs are
bootable on your machine, you can skip right to
<xref linkend="boot-installer"/>; much effort has been expended to ensure
the files most people need are there on the CD. Although a full set of
binary packages requires several CDs, it is unlikely you will need
packages on the third CD and above. You may also consider using the
DVD version, which saves a lot of space on your shelf and you avoid
the CD shuffling marathon.

</para><para>

If your machine doesn't support CD booting, but you do have a CD set,
you can use an alternative strategy such as

<phrase condition="supports-floppy-boot">floppy disk,</phrase>

<phrase arch="s390">tape, emulated tape,</phrase>

<phrase condition="bootable-disk">hard disk,</phrase>

<phrase condition="bootable-usb">usb stick,</phrase>

<phrase condition="supports-tftp">net boot,</phrase>

or manually loading the kernel from the CD to initially boot the
system installer. The files you need for booting by another means are
also on the CD; the Debian network archive and CD folder organization
are identical. So when archive file paths are given below for
particular files you need for booting, look for those files in the
same directories and subdirectories on your CD.

</para><para>

Once the installer is booted, it will be able to obtain all the other
files it needs from the CD.

</para><para>

If you don't have a CD set, then you will need to download the
installer system files and place them on the

<phrase arch="s390">installation tape</phrase>

<phrase condition="supports-floppy-boot">floppy disk or</phrase>

<phrase condition="bootable-disk">hard disk or</phrase>

<phrase condition="bootable-usb">usb stick or</phrase>

<phrase condition="supports-tftp">a connected computer</phrase>

so they can be used to boot the installer.

</para>

 </sect1>
