<!-- $Id: m68k.xml 45650 2007-03-04 09:00:52Z mck-guest $ -->
<!-- original version: 45435 -->

  <sect2 arch="m68k"><title>Výběr instalace</title>
<para>

Některé &arch-title; podarchitektury mají k dispozici linuxová jádra
řady 2.4 i starší 2.2. Pokud si můžete vybrat, vždy nejprve zkuste
instalaci s jádrem 2.4.<replaceable>x</replaceable>. Instalace
s jádrem 2.4 by měla být méně náročná na paměť, protože používá
<firstterm>tmpfs</firstterm>, zatímco řada 2.2 vyžaduje ramdisk
s pevně danou velikostí.

</para><para>

Jestliže používáte linuxové jádro řady 2.2, musíte použít parametr
jádra &ramdisksize;.

</para><para>

Zajistěte, aby se mezi zaváděcími parametry jádra objevil i parametr
<userinput>root=/dev/ram</userinput>.

</para><para>

Pokud máte s instalací problémy, podívejte se na <ulink
url="&url-m68k-cts-faq;">cts's &arch-title; &d-i; FAQ</ulink>.

</para>

<itemizedlist>
  <listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
  <listitem><para><xref linkend="m68k-boot-atari"/></para></listitem>
  <listitem><para><xref linkend="m68k-boot-bvme6000"/></para></listitem>
  <listitem><para><xref linkend="m68k-boot-mac"/></para></listitem>
  <listitem><para><xref linkend="m68k-boot-mvme"/></para></listitem>
  <listitem><para><xref linkend="m68k-boot-q40"/></para></listitem>
</itemizedlist>

   <sect3 id="m68k-boot-amiga"><title>Amiga</title>
<para>

Jediný způsob instalace na Amigách je zavedením z pevného disku (viz
<xref linkend="m68k-boot-hd"/>). <emphasis>Jinými slovy: z CD se nedá
zavádět.</emphasis>

</para><para>

Amigy momentálně nepracují s <firstterm>bogl</firstterm>em, takže
pokud uvidíte chyby týkající se boglu, použijte zaváděcí parametr
<userinput>fb=false</userinput>.

</para>
    </sect3>

   <sect3 id="m68k-boot-atari"><title>Atari</title>
<para>

Instalační program pro Atari můžete spustit buď z pevného disku (viz
<xref linkend="m68k-boot-hd"/>) nebo z disket (viz <xref
linkend="boot-from-floppies"/>). <emphasis>Jinými slovy: z CD se nedá
zavádět.</emphasis>

</para><para>

Atari momentálně nepracují s <firstterm>bogl</firstterm>em, takže
pokud uvidíte chyby týkající se boglu, použijte zaváděcí parametr
<userinput>fb=false</userinput>.

</para>
   </sect3>

   <sect3 id="m68k-boot-bvme6000"><title>BVME6000</title>
<para>

Instalaci na BVME6000 můžete spustit z cdrom (viz <xref
linkend="m68k-boot-cdrom"/>), disket (viz <xref
linkend="boot-from-floppies"/>) nebo ze sítě (viz <xref
linkend="boot-tftp"/>).

</para>
   </sect3>

   <sect3 id="m68k-boot-mac"><title>Macintosh</title>
<para>

Jediný způsob instalace na Macintoshích je zavedením z pevného disku
(viz <xref linkend="m68k-boot-hd"/>). <emphasis>Jinými slovy: z CD se
nedá zavádět.</emphasis> Macintoshe nemají funkční jádro řady 2.4.

</para><para>

Pokud máte SCSI sběrnici založenou na 53c9x, pravděpodobně budete
muset použít zaváděcí parametr
<userinput>mac53c9x=1,0</userinput>. Počítače se dvěma takovými
sběrnicemi (např. Quadra 950) by měly použít
<userinput>mac53c9x=2,0</userinput>. Alternativně můžete též použít
parametr <userinput>mac53c9x=-1,0</userinput>, což ponechá zapnuté
automatické rozpoznávání, ale zakáže SCSI odpojení. Tento parametr je
nutný pouze v případě více disků. Bez něj totiž poběží systém rychleji.

</para>
   </sect3>

   <sect3 id="m68k-boot-mvme"><title>MVME147 a MVME16x</title>
<para>

Instalaci na MVME147 a MVME16x můžete spustit buď z disket (viz <xref
linkend="boot-from-floppies"/>) nebo ze sítě (viz <xref
linkend="boot-tftp"/>).  <emphasis>Jinými slovy: z CD se nedá
zavádět.</emphasis>

</para>
   </sect3>

   <sect3 id="m68k-boot-q40"><title>Q40/Q60</title>
<para>

Jediný způsob instalace na Q40/Q60 je zavedením z pevného disku (viz
<xref linkend="m68k-boot-hd"/>). <emphasis>Jinými slovy: z CD se nedá
zavádět.</emphasis>

</para>
   </sect3>
  </sect2>

  <sect2 arch="m68k" id="m68k-boot-hd">
  <title>Zavedení z pevného disku</title>

&boot-installer-intro-hd.xml;

<para>

Pro zavádění z pevného disku můžete použít alespoň šest různých
ramdisků (tři metody a každý ve variantě pro jádro řady 2.2 - viz
<ulink
url="&disturl;/main/installer-&architecture;/current/images/MANIFEST">MANIFEST</ulink>).

</para><para>

Tyto tři ramdisky jsou: <filename>cdrom</filename>,
<filename>hd-media</filename>
a <filename>nativehd</filename>. Ramdisky se liší pouze tím, kde
očekávají instalační balíčky. <filename>cdrom</filename> používá pro
získání zbytku instalačního programu CD mechaniku,
<filename>hd-media</filename> používá obraz CD uložený na pevném disku
a konečně <filename>nativehd</filename> využívá pro instalaci balíků
síť.

</para>

<itemizedlist>
  <listitem><para><xref linkend="m68k-boothd-amiga"/></para></listitem>
  <listitem><para><xref linkend="m68k-boothd-atari"/></para></listitem>
  <listitem><para><xref linkend="m68k-boothd-mac"/></para></listitem>
  <listitem><para><xref linkend="m68k-boothd-q40"/></para></listitem>
</itemizedlist>


   <sect3 id="m68k-boothd-amiga">
   <title>Zavedení z AmigaOS</title>
<para>

V programu <command>Workbench</command> zahájíte instalaci dvojitým
poklikáním na ikonu <guiicon>StartInstall</guiicon> v adresáři
<filename>debian</filename>.

</para><para>

Poté, co instalační program vypíše nějaká odlaďovací hlášení, byste
měli dvakrát stisknout klávesu &enterkey;. Poté vám obrazovka na
několik okamžiků zešedne, což může trvat i několik sekund.
Následně se objeví černá obrazovka s bílým textem, který roluje
rychleji než dokážete číst &mdash; to je v pořádku. Po několika
sekundách by se měl automaticky nastartovat instalační program, takže
můžete přeskočit na <xref linkend="d-i-intro"/>.

</para>
   </sect3>

   <sect3 id="m68k-boothd-atari">
   <title>Zavedení z Atari TOS</title>
<para>

V GEM desktopu spusťte instalační proces dvojitým kliknutím
na ikonu <guiicon>bootstra.prg</guiicon> v adresáři
<filename>debian</filename>. V dialogovém okně pro nastavení programu
klikněte na <guibutton>Ok</guibutton>.

</para><para>

Poté, co instalační program vypíše nějaká odlaďovací hlášení, byste
měli dvakrát stisknout klávesu &enterkey;. Poté vám obrazovka na
několik okamžiků zešedne, což může trvat i několik sekund.
Následně se objeví černá obrazovka s bílým textem, který roluje
rychleji než dokážete číst &mdash; to je v pořádku. Po několika
sekundách by se měl automaticky nastartovat instalační program, takže
můžete přeskočit na <xref linkend="d-i-intro"/>.

</para>
   </sect3>

   <sect3 id="m68k-boothd-mac">
   <title>Zavedení z 68k MacOS</title>
<para>

V případě Macintoshů musíte zachovat původní operační systém a zavádět
z něj. Je <emphasis>důležité</emphasis>, abyste při zavádění MacOS za
účelem spuštění zavaděče <command>Penguin</command> drželi stisknutou
klávesu <keycap>Shift</keycap>. Tím totiž zabráníte nahrání rozšíření,
která mohou způsobit běžícímu linuxovému jádru náhodné problémy.
Pokud používáte MacOS pouze pro zavedení Linuxu, můžete stejného
výsledku dosáhnout odstraněním všech rozšíření a ovládacích panelů ze
systémové složky MacOS.

</para><para>

Macy vyžadují zavaděč <command>Penguin</command>, který si můžete
stáhnout ze stránek projektu <ulink url="&url-m68k-mac;">Linux/mac68k
sourceforge.net</ulink>. Pokud nemáte nástroje pro práci se
<command>Stuffit</command> archivy, můžete jej přenést na disketu
formátovanou pro MacOS pomocí druhého počítače s GNU/Linuxem
(libovolné architektury) a nástrojů <command>hmount</command>,
<command>hcopy</command> a <command>humount</command> z balíku
<classname>hfsutils</classname>.

</para><para>

Na MacOS desktopu spusťte instalační proces dvojitým poklikáním na
ikonu <guiicon>Penguin Prefs</guiicon> v adresáři
<filename>Penguin</filename>. Tím se spustí zavaděč
<command>Penguin</command>. Jděte do nabídky
<guimenuitem>Settings</guimenuitem> v menu <guimenu>File</guimenu>
a klikněte na záložku <guilabel>Kernel</guilabel>. Vyberte obrazy
jádra (<filename>vmlinuz</filename>) a RAMdisku
(<filename>initrd.gz</filename>) tím, že kliknete na odpovídající
tlačítka v pravém horním rohu a pomocí následujícího dialogu si
soubory dohledáte.

</para><para>

Chcete-li nastavit zaváděcí parametry, vyberte menu
<guimenu>File</guimenu> -&gt; <guimenuitem>Settings...</guimenuitem>
a přepněte se do záložky <guilabel>Options</guilabel>. Zde můžete
zadat potřebné parametry do textového pole. Pokud chcete tyto
parametry použít i příště, vyberte <guimenu>File</guimenu> -&gt;
<guimenuitem>Save Settings as Default</guimenuitem>.

</para><para>

Zavřete dialog <userinput>Settings</userinput>, uložte nastavení
a spusťte zaváděcí proces kliknutím na <guimenuitem>Boot
Now</guimenuitem> v menu <guimenu>File</guimenu>.

</para><para>

Zavaděč <command>Penguin</command> vypíše nějaká odlaďovací hlášení.
Potom vám obrazovka na několik okamžiků zešedne, což může trvat
i několik sekund. Potom se objeví černá obrazovka s bílým textem,
který se roluje rychleji než dokážete číst &mdash; to je v pořádku.
Po několika sekundách by se měl automaticky spustit instalační
program, takže můžete přeskočit na <xref linkend="d-i-intro"/>.

</para>
   </sect3>

   <sect3 id="m68k-boothd-q40">
   <title>Zavedení na Q40/Q60</title>
<para>

Instalační program by se měl spustit automaticky, takže můžete
pokračovat rovnou kapitolou <xref linkend="d-i-intro"/>.

</para>
   </sect3>
  </sect2>


  <sect2 arch="m68k" id="m68k-boot-cdrom">
  <title>Zavedení z CD-ROM</title>
<para>

Momentálně jediná podarchitektura &arch-title; podporující zavádění
z CD-ROM je BVME6000.

</para>

&boot-installer-intro-cd.xml;

  </sect2>


  <sect2 arch="m68k" id="boot-tftp"><title>Zavedení z TFTP</title>

&boot-installer-intro-net.xml;

<para>

Po zavedení systému VMEbus se zobrazí LILO prompt
<prompt>Boot:</prompt>. Linux zavedete jednou z následujících řádek,
čímž zahájíte normální instalaci za použití emulace terminálu vt102:

<!-- Because the &enterkey; definition uses <keycap>,    -->
<!-- we use <screen> instead of <userinput> in this list -->

<itemizedlist>
<listitem><para>

pro instalaci na BVME4000/6000 napište <screen>i6000 &enterkey;</screen>

</para></listitem>
<listitem><para>

pro instalaci na MVME162 napište <screen>i162 &enterkey;</screen>

</para></listitem>
<listitem><para>

pro instalaci na MVME166/167 napište <screen>i167 &enterkey;</screen>

</para></listitem>
    </itemizedlist>

</para><para>

Jestliže chcete emulovat jiný terminál, třeba vt100, přidejte ještě
řetězec <screen>TERM=vt100</screen>. (Celá řádka pak vypadá třeba
takto: <screen>i6000 TERM=vt100 &enterkey;</screen>.)

</para>
  </sect2>


  <sect2 arch="m68k" id="boot-from-floppies">
  <title>Zavedení z disket</title>
<para>

Pro většinu počítačů založených na architektuře &arch-title; se
doporučuje zavést instalační systém z lokálního souborového systému.

</para><para>

Zavedení z diskety momentálně podporují jenom Atari a VME
(VME pouze se SCSI disketovou mechanikou).

</para>
 </sect2>
