<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43576 -->


  <sect2 arch="x86"><title>Partitionnement depuis DOS ou Windows</title>
<para>

Si vous manipulez des partitions FAT ou NTFS existantes, il est
recommand� de suivre la m�thode ci-dessous ou d'utiliser des produits
DOS ou Windows. Il n'est pas utile de partitionner � partir de
DOS ou Windows&nbsp;; les outils de partitionnement Linux sont g�n�ralement
meilleurs.

</para><para>

Si vous avez un grand disque IDE, et si vous n'utilisez ni l'adressage LBA,
ni des pilotes de recouvrement (parfois fournis par les constructeurs de
disques durs), ni un BIOS r�cent (apr�s 1998) qui accepte les extensions
d'acc�s pour disques de grande capacit�, vous devez faire attention
� la position de la partition de d�marrage de Debian&nbsp;: vous
devez la placer dans les 1024&nbsp;premiers cylindres de votre disque dur
(habituellement aux alentours de 524&nbsp;Mo, sans conversion par le
BIOS). Et il faudra peut-�tre d�placer une partition FAT ou NTFS
existante.

</para>

   <sect3 id="lossless">
   <title>Repartitionnement sans perte sous DOS, Windows-32 ou OS/2
   </title>

<para>

Il est tr�s courant de vouloir installer Debian sur un syst�me qui contient
d�j� DOS (y compris Windows 3.1), Win32 (Windows 95, 98, Me, NT, 2000, 
XP) ou OS/2, sans d�truire l'ancien syst�me. L'installateur Debian sait
redimensionner les syst�mes de fichiers FAT et NTFS utilis�s par DOS et Windows.
Lancez simplement l'installateur. Quand vous �tes � l'�tape <quote>partitionnement</quote>,
choisissez l'option <menuchoice> <guimenuitem>Manuel</guimenuitem> </menuchoice>,
puis choisissez la partition � redimensionner et indiquez la 
taille voulue. Dans la plupart des cas, vous n'aurez pas besoin de la m�thode 
d�crite plus bas. 

</para><para>

Avant d'aller plus loin, vous devez avoir d�cid� comment vous allez
diviser le disque. La m�thode pr�sent�e dans ce chapitre ne vise qu'�
d�couper une partition en deux parties. L'une contiendra l'OS original
et l'autre sera utilis�e pour Debian.
Pendant l'installation de Debian, vous aurez l'occasion de terminer la division
de la partie Debian du disque comme vous le souhaitez (partition d'�change ou
partition avec syst�me de fichiers).

</para><para>

L'id�e est de d�placer toutes les donn�es au d�but de la partition, avant de
changer les caract�ristiques de la partition&nbsp;: ainsi rien ne sera perdu.
Il est important de ne rien faire entre le d�placement des donn�es et le repartitionnement,
pour minimiser le risque qu'un fichier soit �crit pr�s de la fin de la partition,
puisque cela diminuerait la quantit� d'espace pouvant �tre pris sur la partition.

</para><para>

Il faut tout d'abord une copie de <command>fips</command> qui est disponible
dans le r�pertoire <filename>tools/</filename> du miroir Debian le plus
proche. D�compressez l'archive et copiez les fichiers 
<filename>RESTORRB.EXE</filename>,
<filename>FIPS.EXE</filename> et <filename>ERRORS.TXT</filename> sur une 
disquette amor�able. Une disquette amor�able peut �tre cr��e en utilisant la
commande <filename>sys a:</filename> sous DOS. <command>Fips</command> est 
fourni avec une tr�s bonne documentation qu'il est conseill� de lire. Vous devrez la 
lire si vous utilisez un pilote de compression de disque ou un gestionnaire 
de disque. Cr�ez la disquette et lisez la documentation 
<emphasis>avant</emphasis> de d�fragmenter le disque.

</para><para>

L'�tape suivante consiste � d�placer toutes les donn�es au d�but de
la partition. L'utilitaire <command>defrag</command>, qui est livr� en 
standard avec DOS 6.0 et les versions sup�rieures, est parfaitement appropri� � cette
t�che. Consultez la documentation de fips pour une liste des autres 
logiciels qui peuvent faire l'affaire. Notez que si vous avez Windows95, vous 
devez lancer <command>defrag</command> depuis celui-ci, puisque DOS ne 
comprend pas le type de syst�me de fichiers VFAT qui est utilis� pour g�rer 
les noms longs depuis Windows 95 et les versions sup�rieures

</para><para>

Apr�s la d�fragmentation (qui peut prendre du temps sur un grand
disque), r�amorcez avec la disquette <command>fips</command> que vous avez 
cr��e dans le lecteur de disquette. Saisissez simplement 
<filename>a:\fips</filename> et suivez les instructions.

</para><para>

Il existe beaucoup d'autres utilitaires de partitionnement
au cas o� <command>fips</command> ne conviendrait pas.

</para>
   </sect3>

   <sect3 id="partitioning-for-dos"><title>Partitionnement sous DOS</title>

<para>

Si vous partitionnez des disques DOS, ou si vous changez la taille de
partitions DOS, en utilisant des outils Linux, sachez que beaucoup de
probl�mes ont �t� constat�s avec
les partitions FAT obtenues. Par exemple, on a remarqu� des probl�mes de
performance ou des probl�mes de coh�rence avec <command>scandisk</command>, et
d'autres erreurs bizarres sous DOS ou Windows.

</para><para>

Apparemment, chaque fois que vous cr�ez ou que vous redimensionnez une
partition destin�e � �tre utilis�e sous DOS, c'est une bonne id�e de
remplir avec des z�ros quelques-uns des premiers secteurs. Vous pouvez ex�cuter
la commande suivante depuis Linux avant de lancer
la commande DOS <command>format</command>&nbsp;:

<informalexample><screen>
# dd if=/dev/zero of=/dev/hdXX bs=512 count=4
</screen></informalexample>

</para>
   </sect3>
  </sect2>
