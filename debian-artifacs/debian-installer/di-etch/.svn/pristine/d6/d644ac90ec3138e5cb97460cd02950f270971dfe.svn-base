<!-- retain these comments for translator revision tracking -->
<!-- original version: 25496 -->


 <sect1 id="what-is-debian-linux">
 <!-- <title>What is &debian;?</title> -->
 <title>Cosa &egrave; &debian;?</title>
<para>

<!--
The combination of Debian's philosophy and methodology and the GNU
tools, the Linux kernel, and other important free software, form a
unique software distribution called &debian;. This
distribution is made up of a large number of software
<emphasis>packages</emphasis>.  Each package in the distribution
contains executables, scripts, documentation, and configuration
information, and has a <emphasis>maintainer</emphasis> who is
primarily responsible for keeping the package up-to-date, tracking bug
reports, and communicating with the upstream author(s) of the packaged
software. Our extremely large user base, combined with our bug
tracking system ensures that problems are found and fixed quickly.
-->

La combinazione della filosofia e della metodologia di Debian con
gli strumenti di GNU, il kernel Linux e altri importati software liberi,
formano un distribuzione unica chiamata &debian;. Questa
distribuzione &egrave; composta da un grande numero di
<emphasis>pacchetti</emphasis> software. Ogni pacchetto nella
distribuzione contiene eseguibili, script, documentazione e informazioni
sulla configurazione e ha un <emphasis>responsabile</emphasis> che
deve mantenere il pacchetto aggiornato, controllare i bug report
e comunicare con l'autore/gli autori upstream del pacchetto software.
La larghissima base di utenti, combinata con il sistema di
tracciamento dei bug, assicura che i problemi trovati siano sistemati
velocemente.

</para><para>

<!--
Debian's attention to detail allows us to produce a high-quality,
stable, and scalable distribution.  Installations can be easily
configured to serve many roles, from stripped-down firewalls to
desktop scientific workstations to high-end network servers.
-->

L'attenzione di Debian ai dettagli permette di produrre una
distribuzione di alta qualit&agrave;, stabile e scalabile.
Le installazioni possono essere configurate per essere utili per
molti usi, dai firewall minimali alle workstation scientifiche
ai server di rete di alto livello.

</para><para>

<!--
Debian is especially popular among advanced users because of its
technical excellence and its deep commitment to the needs and
expectations of the Linux community. Debian also introduced many
features to Linux that are now commonplace.
-->

Debian &egrave; soprattutto popolare tra gli utenti esperti a causa
della sua eccellenza tecnica e il profondo impegno verso le
necessit&agrave; e le attese della comunit&agrave; Linux. Debian
ha introdotto molte funzionalit&agrave; in Linux che ormai sono
comuni.

</para><para>

<!--
For example, Debian was the first Linux distribution to include a
package management system for easy installation and removal of
software. It was also the first Linux distribution that could be
upgraded without requiring reinstallation.
-->

Per esempio, Debian &egrave; stata la prima distribuzione Linux a
includere un sistema di gestione dei pacchetti per l'installazione e
la rimozione facile del software. &Egrave; stata anche la prima
distribuzione Linux a poter essere aggiornata senza richiedere
la reinstallazione.

</para><para>

<!--
Debian continues to be a leader in Linux development. Its development
process is an example of just how well the Open Source development
model can work &mdash; even for very complex tasks such as building and
maintaining a complete operating system.
-->

Debian continua a essere un leader nello sviluppo di Linux. Il suo
processo di sviluppo &egrave; un esempio di come il modello di
sviluppo Open Source possa funzionare bene &mdash; anche per
lavori molto complessi come costruire e manutenere un sistema
operativo completo.

</para><para>

<!--
The feature that most distinguishes Debian from other Linux
distributions is its package management system.  These tools give the
administrator of a Debian system complete control over the packages
installed on that system, including the ability to install a single
package or automatically update the entire operating system.
Individual packages can also be protected from being updated.  You can
even tell the package management system about software you have
compiled yourself and what dependencies it fulfills.
-->

La funzionalit&agrave; che maggiormente distingue Debian dalle
altre distribuzioni &egrave; il sistema di gestione dei pacchetti.
Questi strumenti danno all'amministratore di un sistema Debian il
controllo completo sui pacchetti installati sul sistema,
inclusa la possibilit&agrave; di installare un singolo pacchetto
o aggiornare automaticamente l'intero sistema operativo.
Si pu&ograve; anche negare l'aggiornamento dei singoli pacchetti.
Si pu&ograve; anche istruire il sistema di gestione dei pacchetti
riguardo ai software che si sono compilati autonomamente e che
dipendenze devono soddisfare.

</para><para>

<!--
To protect your system against <quote>Trojan horses</quote> and other malevolent
software, Debian's servers verify that uploaded packages come from
their registered Debian maintainers.  Debian packagers also take great
care to configure their packages in a secure manner.  When security
problems in shipped packages do appear, fixes are usually available
very quickly.  With Debian's simple update options, security fixes can
be downloaded and installed automatically across the Internet.
-->

Per proteggere i sistemi da <quote>Trojan horse</quote> e altri software
maligni, i server di Debian verificano che i pacchetti siano inviati dai
loro manutentori Debian registrati. Coloro che impacchettano i programmi
Debian fanno molta attenzione nel configurare i pacchetti in
modalit&agrave; sicura. Quando appaiono problemi nei pacchetti
distribuiti le correzioni sono generalmente disponibili molto
rapidamente. Con la semplice opzione di aggiornamento di Debian, le
correzioni ai problemi di sicurezza possono essere scaricate e
installate da Internet.

</para><para>

<!--
The primary, and best, method of getting support for your &debian;
system and communicating with Debian Developers is through
the many mailing lists maintained by the Debian Project (there are
more than &num-of-debian-maillists; at this writing).  The easiest
way to subscribe to one or more of these lists is visit
<ulink url="&url-debian-lists-subscribe;">
Debian's mailing list subscription page</ulink> and fill out the form
you'll find there.
-->

Il metodo principale e migliore per ottenere supporto per
un sistema &debian; e comunicare con gli sviluppatori
Debian &egrave; usando le molte mailing list gestite dal
progetto Debian (ci sono pi&ugrave; di &num-of-debian-maillists;
al momento in cui questo documento &egrave; scritto). La maniera
pi&ugrave; facile per iscriversi a una o pi&ugrave; liste &egrave;
visitare <ulink url="&url-debian-lists-subscribe;">la pagina
di iscrizione alle mailing list di Debian</ulink> e riempire
la form che vi troverete.

</para>
 </sect1>
