<!-- original version: 33725 -->


   <sect3 id="localechooser">
   <title>Lokalisatie bepalen</title>

<para>

Over het algemeen zullen de eerste vragen die worden gesteld de bepaling van
de lokalisatie voor zowel de installatie als het geïnstalleerde systeem
betreffen. De lokalisatie-opties betreffen taal, land en
<quote>locale</quote>.

</para><para>

De taal die u kiest zal worden gebruikt tijdens het vervolg van de
installatie, tenminste als een vertaling van de verschillende dialogen
beschikbaar is. Als voor de geselecteerde taal geen geldige vertaling
beschikbaar is, zal het installatiesysteem terugvallen op Engels.

</para><para>

Het geselecteerde land zal later in het installatieproces worden gebruikt
bij de selectie van uw tijdzone en een voor uw locatie geschikte Debian
spiegelserver. Taal en land tezamen zullen worden gebruikt om de standaard
<quote>locale</quote> voor uw systeem in te stellen en om u te helpen bij
de selectie van uw toetsenboord.

</para><para>

Allereerst zal worden gevraagd welke taal uw voorkeur heeft. De talen
worden genoemd zowel in het Engels (links) als in de taal zelf (rechts);
de namen aan de rechter zijde worden tevens getoond in het juiste script
voor de taal. De lijst is gesorteerd op de Engelse namen.
Bovenaan de lijst vindt u een extra optie waarmee u, in plaats van een taal,
de <quote>C</quote>-locale kunt selecteren. Het kiezen van de
<quote>C</quote>-locale heeft tot gevolg dat de installatie in het Engels
zal plaatsvinden; daarnaast zal het geïnstalleerde systeem geen ondersteuning
voor lokalisatie hebben aangezien het pakket <classname>locales</classname>
niet zal worden geïnstalleerd.

</para><para>

Als u een taal heeft geselecteerd die wordt herkend als officiële taal voor
meer dan één land<footnote>

<para>

In technische termen: indien voor die taal meer dan één <quote>locale</quote>
bestaat met verschillende landcodes.

</para>

</footnote>, zal u vervolgens worden gevraagd een land te selecteren. Als u
kiest voor <guimenuitem>Andere</guimenuitem> onderaan de lijst, zal een
overzicht worden getoond van alle landen, gegroepeerd op continent. Als de
taal slechts bij één land voorkomt, zal dat land automatisch worden
geselecteerd.

</para><para>

Op basis van door u geselecteerde taal en land zal een standaardwaarde voor
locale worden bepaald. Als u de installatie uitvoert op een lagere dan de
standaard prioriteit, heeft u de mogelijkheid om een andere standaard locale te
kiezen en om aanvullende locales te kiezen die voor het geïnstalleerde systeem
moeten worden gegenereerd.

</para>
   </sect3>
