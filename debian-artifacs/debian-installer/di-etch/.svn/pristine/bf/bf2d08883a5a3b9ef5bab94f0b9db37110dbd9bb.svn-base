apt-setup (1:0.20etch1) stable; urgency=low

  * 50mirror: default suite to codename, similar to what we already do for
    network-based installs; choose-mirror will determine the actual suite
    from the codename. Closes: #422442.

 -- Frans Pop <fjp@debian.org>  Mon,  4 Jun 2007 21:15:12 +0200

apt-setup (1:0.20) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 16:37:56 +0100

apt-setup (1:0.19) unstable; urgency=low

  [ Joey Hess ]
  * Improve regexp in 90security to not misfire if the mirror's name or path
    contains "contrib" or "non-free".

  [ Updated translations ]
  * Hebrew (he.po) by Lior Kaplan

 -- Frans Pop <fjp@debian.org>  Tue, 13 Feb 2007 23:23:00 +0100

apt-setup (1:0.18) unstable; urgency=low

  [ Updated translations ]
  * Romanian (ro.po) by Eddy Petrișor

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:18:40 +0100

apt-setup (1:0.17) unstable; urgency=low

  [ Christian Perrier ]
  * Fix error in French translation. Closes: #400715.

  [ Frans Pop ]
  * Make wget respect proxy servers defined for local sources. Based on a
    patch suggested by John Morrissey, for which thanks. Closes: #408297.
  * Use log-output for commands that produce output if run in the installer.
  * Consistently use http protocol when setting up security archive (ftp
    reguires a different mirror directory and is thus not easy to support).
  * Reduce timeout for testing security mirrors to 30 seconds. As security.d.o
    currently has 3 IP addresses listed, and these are tried consecutively, the
    total timeout will be 1.5 minutes (was 6 minutes). Closes: #393033.
  * apt-setup-verify: reset IFS to avoid problems with option processing.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bosnian (bs.po) by Safir Secerovic
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş

 -- Frans Pop <fjp@debian.org>  Mon, 29 Jan 2007 21:43:32 +0100

apt-setup (1:0.16) unstable; urgency=low

  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 13:48:32 +0200

apt-setup (1:0.15) unstable; urgency=low

  * Fix broken proxy setting code in 90security. Closes: #378868
    Some systems installed before this fix will have Acquire::http::proxy "false"
    set in apt.conf, which leads to breakage in some situations. Also, if a
    proxy was set, it would not be written to the file.

 -- Joey Hess <joeyh@debian.org>  Thu, 17 Aug 2006 17:45:07 -0400

apt-setup (1:0.14) unstable; urgency=low

  * Make apt-setup idempotent by removing workfile on back up.
  * Allow back up from use network mirror question.
  * Make apt-setup-verify responsible for blank lines between sources lines
    from different generators.
  * Add Lintian override for standards-version.
  * Add missing dependencies on debconf.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Icelandic (is.po) by David Steinn Geirsson
  * Dutch (nl.po) by Bart Cornelis
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 15 Aug 2006 02:22:07 +0200

apt-setup (1:0.13) unstable; urgency=low

  [ Colin Watson ]
  * Only use RET in security generator for data returned from debconf, to
    avoid confusion.
  * Fix mirror generators not to break if apt-setup/use_mirror isn't asked.

  [ Updated translations ]
  * Finnish (fi.po) by Tapio Lehtonen
  * Panjabi (pa.po) by A S Alam
  * Tagalog (tl.po) by Eric Pareja

 -- Frans Pop <fjp@debian.org>  Tue, 25 Jul 2006 13:01:56 +0200

apt-setup (1:0.12) unstable; urgency=low

  [ Colin Watson ]
  * Port mirror generator changes from 1:0.11 to Ubuntu mirror generator.

  [ Updated translations ]
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Estonian (et.po) by Siim Põder
  * Hungarian (hu.po) by SZERVÑC Attila
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Frans Pop <fjp@debian.org>  Thu, 13 Jul 2006 17:11:34 +0200

apt-setup (1:0.11) unstable; urgency=low

  [ Frans Pop ]
  * Make the use of a network mirror optional if the base system is
    installable from CD.

  [ Colin Watson ]
  * Set $protocol in security generator so that proxy configuration works.

  [ Joey Hess ]
  * Move mirror use prompt code into mirror generator.
  * Run choose-mirror with new -n switch so it does not mess with the existing
    progress bar.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Progga
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Thu, 15 Jun 2006 13:54:54 -0400

apt-setup (1:0.10) unstable; urgency=low

  [ Frans Pop ]
  * When the security mirror cannot be reached, do not apt-setup-verify the
    source lines again. This saves having to wait for two DNS timeouts.

  [ Colin Watson ]
  * Add multiverse component support to Ubuntu mirror generator (thanks,
    Timo Aaltonen; closes: https://launchpad.net/bugs/29646).

  [ Joey Hess ]
  * Use cdrom/codename if mirror/codename is empty; part of the cdrom/codename
    split.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bosnian (bs.po) by Safir Secerovic
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Sonam Rinchen
  * Esperanto (eo.po) by Serge Leblanc
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Leang Chumsoben
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Wed, 19 Apr 2006 19:43:31 +0200

apt-setup (1:0.9) unstable; urgency=low

  * If no mirror hostname or directory are configured, avoid asking non-free
    question or trying to set up a mirror. Useful for cases such as full CD
    installs, where the user might choose to skip setting up a mirror.

 -- Joey Hess <joeyh@debian.org>  Fri,  7 Apr 2006 17:39:58 -0400

apt-setup (1:0.8) unstable; urgency=low

  * Add support for preseeding local archives to be appended to
    sources.list, based on a patch by Timo Aaltonen (closes: #348509).

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Irish (ga.po) by Kevin Patrick Scannell
  * Khmer (km.po) by hok kakada

 -- Colin Watson <cjwatson@debian.org>  Tue, 28 Mar 2006 17:14:50 +0100

apt-setup (1:0.7) unstable; urgency=low

  [ Joey Hess ]
  * Remove --ignore-time-conflict settings, set globally for the installer
    by base-installer 1.49.

  [ Frans Pop ]
  * Add isinstallable file to skip apt setup when installing Sarge.

  [ Updated translations ]
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Welsh (cy.po) by Dafydd Harries
  * Basque (eu.po) by Piarres Beobide
  * Hindi (hi.po) by Nishant Sharma
  * Khmer (km.po) by hok kakada
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Mon, 20 Mar 2006 18:30:54 +0100

apt-setup (1:0.6) unstable; urgency=low

  [ Joey Hess ]
  * Ignore time conflicts when running apt-get update, should fix later apt
    brokenness for systems with clock issues.

  [ Updated translations ]
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hungarian (hu.po) by SZERVÑC Attila
  * Polish (pl.po) by Bartosz Fenski
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 12 Feb 2006 18:18:48 +0000

apt-setup (1:0.5) unstable; urgency=low

  [ Colin Watson ]
  * Fix reversed logic for universe and backports in Ubuntu mirror
    generator.
  * Fix comment typo in Ubuntu mirror generator.

  [ Frans Pop ]
  * Change menu title to fit in better with rest of menu.

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Punjabi (India) (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Mon, 23 Jan 2006 21:23:37 +0100

apt-setup (1:0.4) unstable; urgency=low

  [ Frans Pop ]
  * Use the codename for the release in sources.list instead of the suite.
    Requires: cdrom-detect >=1.11; choose-mirror >=1.16; iso-scan >=1.10.
    Closes: #313235

  [ Colin Watson ]
  * Make apt-setup-verify preserve blank lines.
  * Avoid double slashes in sources.list mirror lines.

 -- Colin Watson <cjwatson@debian.org>  Fri,  9 Dec 2005 12:09:30 +0000

apt-setup (1:0.3) unstable; urgency=low

  * Yet another case of an upload needed to update code to match a value from
    a choices list that was changed by someone without checking for the code
    that dependned on that value.

  [ Updated translations ]
  * Danish (da.po) by Claus Hindsgaul
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Mon,  5 Dec 2005 19:40:39 -0500

apt-setup (1:0.2) unstable; urgency=low

  [ Colin Watson ]
  * Add strings for Ubuntu mirror generator to translation infrastructure,
    per http://lists.debian.org/debian-boot/2005/10/msg01407.html and
    followups.
  * Clarify apt-setup/universe description a bit.

  [ Joey Hess ]
  * If setting up a mirror fails, allow the user to retry or change their
    mirror.
  * If setting up a CD fails, let the user know there was a problem instead of
    quietly continuing without a main apt source.
  * Add a comment before commented out lines to make clear why they are
    commented out.

  [ Christian Perrier ]
  * Break Choices in single components in templates

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Russian (ru.po) by Yuri Kozlov
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Joey Hess <joeyh@debian.org>  Wed, 16 Nov 2005 17:31:02 -0500

apt-setup (1:0.1) unstable; urgency=low

  [ Joey Hess ]
  * First release as a udeb broken out from base-config and rewritten.

  * This means that apt mirror config uses d-i's mirror configs automatically.
    Closes: #220269, #252121, #235980, #251007
  * Issues with /dev/cdrom symlinks no longer apply, since that's taken
    care of by d-i. Closes: #198759, #264375, #274615
  * Issues with CD eject buttons no longer apply, since well, it currently
    only supports loading in one CD. Closes: #215639, #215639, #240331
    Closes: #333513
    (TODO item exists for multiple CDs tho).
  * Runs in the first stage so no issues with it running again and eating
    configured sources or wanting to clear proxy settings.
    Closes: #229048, #196625
  * Will deal nicely with failure to reach security.debian.org.
    Closes: #251413
  * You can enter a mirror manually in choose-mirror and entry proxy info too.
    Closes: #277481, #284794
  * CD stuff works ok in noninteractive/preseeded modes. Closes: #282356
  * Adds deb-src lines for everything, including security updates.
    Closes: #269154.

  [ Colin Watson ]
  * Various small fixes.
  * Call the main package apt-setup-udeb to allow for apt-setup.deb in the
    future.
  * Add translation infrastructure.
  * Set epoch to supersede the base-config-based apt-setup-udeb in Ubuntu.
  * Don't bother looking for security updates for unstable.
  * Strip everything after the first dot from generator names when
    constructing progress template names, to allow writing variants of a
    generator with the same basic purpose as the original.
  * Initial untested mirror generator for Ubuntu. Not added to translation
    infrastructure yet, although the templates are there.

 -- Joey Hess <joeyh@debian.org>  Thu, 27 Oct 2005 18:29:39 -0400
