<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43773 -->

 <sect1 id="installation-media">
<title>Les supports d'installation</title>

<para>
Cette section pr�cise les diff�rents supports utilisables pour installer
Debian. Il est possible par exemple d'installer Debian avec un lecteur de
disquettes. Un chapitre entier est consacr� aux supports, le 
<xref linkend="install-methods"/>&nbsp;; il indique les avantages et les
d�savantages de chaque support.
</para>

<sect2 condition="supports-floppy-boot">
<title>Les disquettes</title>

<para>
Dans certains cas, vous devrez effectuer votre premier amor�age depuis un 
lecteur de disquettes. En g�n�ral vous 
avez besoin d'une disquette haute densit� (1440 kilo-octets) de 3,5 pouces.
</para>

<para arch="powerpc">
Pour CHRP, l'utilisation de disquette est actuellement impossible.
</para>
</sect2>

<sect2><title>CD-ROM, DVD-ROM</title>

<note><para>
Chaque fois que vous lirez <quote>c�d�rom</quote> dans ce manuel, cela 
voudra dire aussi bien CD-ROM que DVD-ROM, car, du point de vue du syst�me
d'exploitation, les deux techniques sont les m�mes, mis � part quelques
vieux lecteurs non standard, ni SCSI ni IDE/ATAPI.
</para></note> 

<para>
L'installation � partir d'un c�d�rom est aussi possible avec certaines 
architectures. Sur les ordinateurs qui acceptent de d�marrer sur c�d�rom 
(c�d�rom amor�able), vous devriez pouvoir faire une installation enti�rement
<phrase arch="not-s390">sans disquette</phrase>
<phrase arch="s390">sans bande</phrase>.
M�me si votre syst�me ne peut pas d�marrer �
partir d'un c�d�rom, vous pouvez utiliser le c�d�rom en m�me
temps que les autres techniques pour installer votre
syst�me, une fois que vous avez d�marr� par d'autres
moyens&nbsp;; voyez le <xref linkend="boot-installer"/>.
</para>

<para arch="x86">
Les c�d�roms de type SCSI et IDE/ATAPI sont reconnus. 
De plus, toutes les interfaces de c�d�rom
non standard reconnues par Linux sont reconnues par les
disquettes de d�marrage (comme les lecteurs Mitsumi et
Matsushita). Cependant, ces mod�les peuvent demander des
param�tres d'amor�age sp�ciaux ou d'autres m�thodes pour
les faire fonctionner&nbsp;; et d�marrer � partir de ces interfaces
non standard risque de ne pas �tre possible. Le 
<ulink url="&url-cd-howto;">HOWTO Linux CD-ROM</ulink> contient des
informations d�taill�es sur l'utilisation des c�d�roms avec
Linux. 
</para>

<para arch="x86">
Les lecteurs USB sont reconnus, comme le sont les p�riph�riques FireWire
reconnus par les pilotes ohci1394 et sbp2.
</para>
<para arch="alpha">
L'architecture &arch-title; reconna�t les lecteurs de c�d�roms SCSI et
IDE/ATAPI si le contr�leur est reconnu par la console SRM. Cela exclut
beaucoup de cartes avec contr�leur int�gr� mais la plupart des cartes avec 
puces IDE et SCSI fournies par le constructeur devraient fonctionner. Pour
savoir si votre mat�riel est reconnu par la console SRM, voyez le
<ulink url="&url-srm-howto;">SRM HOWTO</ulink>.
</para>


<para arch="arm">
Les c�d�roms IDE/ATAPI sont reconnus par toutes les architectures ARM.
Sur les RiscPC, les c�d�roms SCSI sont aussi reconnus. 
</para>

<para arch="mips">
Sur les machines SGI, amorcer depuis un c�d�rom demande un lecteur SCSI qui
puisse fonctionner avec un bloc logique de 512 bytes. La plupart des lecteurs
SCSI vendus sur le march� ne le peuvent pas. Si votre lecteur poss�de un 
cavalier <quote>Unix/PC ou 512/2048</quote>, mettez-le en position Unix ou
512.
Pour commencer l'installation, choisissez l'entr�e 
<quote>System installation</quote> dans le firmware. La carte Broadcom 
BCM91250A reconna�t les p�riph�riques IDE standard, comme les lecteurs de 
c�d�rom, mais les images sur c�d�rom pour cette plateforme ne sont pas fournies car le
firmware ne reconna�t pas les lecteurs de c�d�rom. Pour installer Debian sur une carte
Broadcom BCM91480B, il faut une carte IDE, SATA ou SCSI.
</para>


<para arch="mipsel">
Sur les DECstations, l'amor�age depuis le
c�d�rom n�cessite un lecteur de c�d�roms SCSI capable de
travailler avec une taille de bloc logique de 512 octets.
Beaucoup de lecteurs de c�d�roms SCSI vendus sur le march�
des PC n'offrent pas cette possibilit�. Si votre lecteur de
c�d�roms poss�de un cavalier d�nomm� �&nbsp;Unix/PC&nbsp;� ou
�&nbsp;512/2048&nbsp;�, placez-le en position
�&nbsp;Unix&nbsp;� ou �&nbsp;512&nbsp;�.
</para>

<para arch="mipsel">
Le c�d�rom n�&nbsp;1 contient le programme d'installation
pour la sous-architecture r3k-kn02 (DECstations 5000/1xx
et 5000/240 bas�es sur un processeur R3000, ainsi que pour les 
mod�les Personal DECstation bas�s sur un processeur R3000), 
et le c�d�rom n�&nbsp;2 celui pour la sous-architecture r4k-kn04
(DECstations 5000/150 et 5000/260 bas�es sur un processeur
R4x00, ainsi que pour la Personal DECstation 5000/50).
</para>

<para arch="mipsel">
Pour amorcer depuis un c�d�rom, tapez la commande
<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable></userinput>
� l'invite du microprogramme (<emphasis>firmware</emphasis>), o�
<replaceable>#</replaceable> est le num�ro de p�riph�rique TurboChannel
depuis lequel se fait l'amor�age (3 sur la plupart des
DECstations) et <replaceable>id</replaceable> est l'identifiant SCSI du
lecteur de c�d�roms. Si vous avez besoin de fournir des
param�tres suppl�mentaires, ils peuvent �tre ajout�s en
suivant cette syntaxe&nbsp;:
</para>

<para arch="mipsel">
<userinput>boot
<replaceable>#</replaceable>/rz<replaceable>id</replaceable>
param1=valeur1 param2=valeur2 ...</userinput>
</para>

</sect2>

<sect2><title>Les disques durs</title>
<para>
L'installation � partir d'un disque local est possible sur beaucoup 
d'architectures. Cela demande qu'un autre syst�me d'exploitation charge
l'installateur sur le disque dur.
</para>

<para arch="m68k">
En fait, l'installation � partir d'un disque
local est la technique d'installation pr�f�r�e pour les
machines &architecture;. 
</para>

<para arch="sparc">
Malgr� que &arch-title; ne permette pas de
d�marrer � partir de SunOS (Solaris), l'installation �
partir d'une partition SunOS (par exemple, des tranches UFS)
est tout � fait possible. 
</para>
  </sect2>

<sect2 condition="bootable-usb"><title>Les cl�s USB</title>

<para>
Beaucoup de machines Debian n'ont besoin d'un lecteur de disquettes ou 
d'un lecteur de c�d�roms que pour l'installation du syst�me ou pour g�rer
des probl�mes. Si vous administrez des serveurs, vous avez sans doute d�j�
pens� � vous d�barrasser de ces lecteurs et � utiliser les cl�s USB. Elles
sont aussi utiles pour les syst�mes avec peu de place.
</para>
</sect2>

<sect2><title>Le r�seau</title>

<para>
Pendant l'installation, le t�l�chargement des fichiers n�cessaires peut se faire
par le r�seau. Selon le type d'installation que vous avez choisi et les r�ponses
que vous donnez � certaines questions, le r�seau est utilis� ou non.
Le syst�me d'installation accepte la plupart des modes de connexion (PPPoE, par
exemple, mais pas ISDN ou PPP), par HTTP ou FTP. Une fois l'installation termin�e,
vous pouvez configurer les modes ISDN ou PPP.
</para>

<para condition="supports-tftp">

Vous pouvez aussi <emphasis>amorcer</emphasis> votre syst�me sur le r�seau.
<phrase arch="mips">C'est la m�thode d'installation pr�f�r�e pour Mips.</phrase>
</para>

<para condition="supports-nfsroot">
L'installation sans disque, en utilisant le d�marrage par r�seau et le 
montage par NFS de tous les syst�mes de fichiers locaux, est une autre option.
</para>

</sect2>

<sect2><title>Un*x ou syst�me GNU</title>
<para>
Si vous utilisez un syst�me de type Unix, vous pouvez installer &debian; sans
utiliser l'installateur qui est d�crit dans la suite de ce manuel. Cette mani�re de
faire peut servir � des utilisateurs qui poss�dent des mat�riels non reconnus
ou qui sont sur des machines qui ne peuvent se permettre un temps d'arr�t.
Si cette technique vous int�resse, reportez-vous � la 
<xref linkend="linux-upgrade"/>.
  </para>
   </sect2>

<sect2><title>Syst�mes de stockage reconnus</title>
<para>
Les disquettes de d�marrage Debian contiennent un noyau
construit pour permettre de faire tourner un maximum de
syst�mes. Malheureusement, sa taille augmente en cons�quence, avec beaucoup 
de pilotes qui ne seront jamais utilis�s 
(voyez la <xref linkend="kernel-baking"/> pour apprendre �
construire le v�tre). Cependant, le support du plus grand
nombre de p�riph�riques possible est voulu afin de
s'assurer que l'on puisse installer Debian sur le plus de
mat�riel possible.
</para>

<para arch="x86">
En g�n�ral, le syst�me d'installation Debian reconna�t les disquettes, les 
disques IDE, les disquettes IDE, les p�riph�riques IDE sur le port parall�le, 
les contr�leurs et disques SCSI, les p�riph�riques USB et FireWire. Les 
syst�mes de fichiers reconnus comprennent FAT, les extensions FAT Win-32 
(VFAT), NTFS et d'autres encore.
</para>

<para arch="x86">
Les interfaces disque qui �mulent l'interface du disque
dur �&nbsp;AT&nbsp;�, qu'on appelle souvent MFM, RLL, IDE
ou ATA, sont reconnues. Les tr�s vieux contr�leurs de
disques 8 bits utilis�s dans l'ordinateur IBM XT ne sont
accept�s que par module. Les contr�leurs de disques SCSI
provenant de diff�rents constructeurs sont
reconnus. Voyez le 
<ulink url="&url-hardware-howto;">HOWTO sur la compatibilit� des mat�riels avec Linux</ulink> 
pour plus de pr�cisions.
</para>

<para arch="m68k">
� peu pr�s tous les syst�mes de stockage reconnus par le
noyau Linux sont reconnus par le syst�me d'installation
Debian. Notez que le noyau Linux courant n'accepte pas du tout les disquettes 
sur le Macintosh, et le syst�me d'installation Debian ne reconna�t pas les 
disquettes pour Amiga. Le syst�me HFS de Macintosh est aussi reconnu sur
les Atari, et l'AFFS en tant que module. Les Macs acceptent le syst�me de 
fichiers Atari (FAT). Les Amiga acceptent le syst�me de fichiers FAT et celui 
de HFS en module.
</para>

<para arch="sparc">
Tout syst�me de stockage reconnu par le noyau Linux est aussi
reconnu par le syst�me de d�marrage. Les pilotes SCSI
suivants sont reconnus dans le noyau par d�faut&nbsp;:
<itemizedlist>
<listitem><para>Sparc ESP</para></listitem>
<listitem><para>PTI Qlogic,ISP</para></listitem>
<listitem><para>Adaptec AIC7xxx</para></listitem>
<listitem><para>NCR and Symbios 53C8XX</para></listitem>
</itemizedlist>

Les syst�mes IDE (tels que UltraSPARC 5) sont aussi
reconnus. Voyez la <ulink url="&url-sparc-linux-faq;">FAQ Linux pour les processeurs SPARC</ulink> pour plus d'informations
sur le mat�riel SPARC reconnu par le noyau Linux. 
</para>

<para arch="alpha">
Tout syst�me de stockage reconnu par le noyau Linux est reconnu par le 
syst�me de d�marrage. Les disques IDE et SCSI sont reconnus.Il faut cependant
noter que sur de nombreux syst�mes, la console SRM est incapable de d�marrer 
� partir des lecteurs IDE, et la Jensen est incapable de d�marrer � partir des
disquettes, voyez
 <ulink url="&url-jensen-howto;">&url-jensen-howto;</ulink> pour plus
d'informations sur le d�marrage de Jensen. 
</para>

<para arch="powerpc">
Tout syst�me de stockage reconnu par le noyau Linux est
aussi reconnu par le syst�me de d�marrage. Il faut
remarquer que le noyau Linux actuel n'accepte pas du
tout les disquettes sur les syst�mes CHRP.
</para>

<para arch="hppa">
Tout syst�me de stockage reconnu par le noyau Linux est
aussi reconnu par le syst�me de d�marrage. Il faut
remarquer que le noyau Linux actuel n'accepte pas les
disquettes.
</para>

<para arch="mips">
Tout syst�me de stockage reconnu par le noyau Linux est
aussi reconnu par le syst�me de d�marrage.
</para>

<para arch="s390">
Tout syst�me de stockage reconnu par le noyau Linux est
aussi support� par le syst�me de d�marrage. Cela signifie
que les DASD FBA et ECKD sont reconnus pour les vieux
formats de disque Linux (ldl) et le nouveau format de
disque S/390 plus courant (cdl). 
</para>
  </sect2>
 </sect1>
