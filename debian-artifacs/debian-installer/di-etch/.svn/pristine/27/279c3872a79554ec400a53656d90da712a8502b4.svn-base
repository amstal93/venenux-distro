<!-- $Id: alpha.xml 40441 2006-09-02 17:50:36Z mck-guest $ -->
<!-- original version: 39920 -->

  <sect2 arch="alpha"><title>Dělení disku na &arch-title;</title>
<para>

Zavádění Debianu ze SRM konzoly vyžaduje, abyste na svém zaváděcím
disku neměli dosovou tabulku rozdělení disku, ale tzv. <quote>BSD
disklabel</quote>. (Zaváděcí blok SRM je nekompatibilní s tabulkou
oblastí systému MS-DOS &mdash; viz <xref linkend="alpha-firmware"/>).
Program na dělení disku <command>partman</command> vytváří na
architektuře &architecture; BSD disklabel automaticky, ale pokud již
máte na disku existující dosovou tabulku oblastí, budete ji muset
nejprve smazat.

</para><para>

Pokud pro dělení disku použijete program <command>fdisk</command>
a vybraný disk ještě neobsahuje BSD disklabel, musíte se přepnout do
režimu disklabel příkazem <userinput>b</userinput>.

</para><para>

Pokud nepotřebujete rozdělovaný disk používat z Tru64 Unixu nebo
některého z volných klonů systému 4.4BSD-Lite (FreeBSD, OpenBSD nebo
NetBSD), <emphasis>neměli byste</emphasis> nastavovat třetí oblast,
aby obsahovala celý disk (tj. aby počáteční a koncový sektor pokrýval
celý disk), protože nástroje používané pro nastavení zavaděče
<command>aboot</command> neumějí takovou oblast použít. To znamená, že
disk, který instalační systém nastaví jako zaváděcí, nebude dostupný
z výše zmíněných operačních systémů.

</para><para>

Na začátku disku <emphasis>musíte</emphasis> nechat dostatek volného
místa pro <command>aboot</command>, protože se zapisuje do několika
prvních sektorů na disku (v současnosti zabírá asi 70 kilobajtů nebo
150 sektorů). Dříve se doporučovalo pro tyto účely vytvořit na začátku
disku malou nenaformátovanou oblast, ale nyní si myslíme, že na
discích používaných jenom GNU/Linuxem to není nutné. (S ohledem na
důvody zmíněné výše.) Použijete-li <command>partman</command>, oblast
pro <command>aboot</command> se pro jistotu vytvoří automaticky.

</para><para condition="FIXME">

Pro instalaci z ARC konzoly byste měli udělat na začátku disku malou
FAT oblast, která bude obsahovat <command>MILO</command> a
<command>linload.exe</command>.
Podle <xref linkend="non-debian-partitioning"/> by mělo stačit
5 megabajtů. Vytvoření této oblasti z menu zatím není podporováno,
takže ji budete muset vytvořit ručně programem
<command>mkdosfs</command>. (Samozřejmě ještě před instalací zavaděče.)

</para>
  </sect2>
