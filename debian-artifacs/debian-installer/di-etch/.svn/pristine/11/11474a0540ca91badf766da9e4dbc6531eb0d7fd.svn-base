<!-- $Id: alpha.xml 43692 2006-12-30 15:59:22Z mck-guest $ -->
<!-- original version: 43576 -->

  <sect2 arch="alpha">
  <title>Dělení disku v Tru64 UNIX</title>
<para>

Tru64 UNIX, dříve známý jako Digital UNIX, což je vlastně jiné jméno
pro OSF/1, používá dělicí schéma podobné jako BSD <quote>disk
label</quote>.  To znamená, že povoluje mít až osm oblastí na každém
disku.  Oblasti jsou v Linuxu očíslovány od <quote>1</quote> do
<quote>8</quote> a v UNIXu opísmenkovány od <quote>a</quote> do
<quote>h</quote>.  V linuxových jádrech 2.2 a vyšších odpovídá vždy
číslo <quote>1</quote> písmenku <quote>a</quote>, číslo
<quote>2</quote> písmenku <quote>b</quote> a tak dále.  Například:
oblast <filename>rz0e</filename> v Tru64 UNIXu by se v Linuxu nazývala
<filename>sda5</filename>.

</para><para>

Oblasti v Tru64 disk label se mohou překrývat. Pokud budete tento disk
používat z Tru64, je vyžadováno, aby se oblast <quote>c</quote>
rozpínala přes celý disk, tudíž překrývala všechny ostatní neprázdné
oblasti. Pod Linuxem to znamená, že oblast <filename>sda3</filename>
je identická s <filename>sda</filename> (pokud je přítomen další disk,
tak <filename>sdb3</filename> s <filename>sdb</filename>, atd.). Kromě
splnění tohoto požadavku není žádný rozumný důvod, proč vytvářet
překrývající se oblasti. Současný dělicí program &d-i;u nepodporuje
překrývající se oblasti, což znamená, že nedoporučujeme sdílet disk
mezi Tru64 a Debianem. Po skončení instalace samozřejmě můžete Tru64
disky připojit a používat zcela bez problémů.

</para><para>

Další zažitá konvence je požadavek, aby oblast <quote>a</quote>
začínala na začátku disku, což zajistí, že disklabel bude vždy
obsahovat zaváděcí sektor. Jestliže chcete zavádět Debian z tohoto
disku, měla by mít velikost alespoň 2MB, aby se tam vešel aboot
a možná jádro. Tato oblast je požadovaná pouze z důvodu kompatibility;
v žádném případě na ní nevytvářejte souborový systém, protože byste si
zničili data.

</para><para>

Je možné a vlastně docela rozumné, sdílet mezi Linuxem a UNIXem
oblast pro virtuální paměť. V takovém případě však budete muset
na tuto oblast spouštět program <command>mkswap</command> pokaždé,
když budete spouštět Linux po předchozí práci v UNIXu.
Děje se to proto, protože UNIX na dané oblasti zničí příznak virtuální
paměti, a Linux si bez onoho příznaku nedovolí na oblast cokoliv
uložit. K vyřešení tohoto problému  stačí ve startovacích skriptech
Linuxu spustit program <command>mkswap</command> kdekoliv před
příkazem <command>swapon -a</command>.

</para><para>

Digital UNIX může nativně používat dva druhy oblastí: UFS a AdvFS.
Linux bohužel rozumí pouze té první. Chcete-li si tedy prohlížet
UNIXové oblasti z Linuxu, zkontrolujte, zda jsou typu UFS.

</para>
  </sect2>

  <sect2 arch="alpha">
  <title>Dělení disku ve Windows NT</title>
<para>

Windows NT používají tabulku rozdělení disku stejnou, jako na osobních
počítačích. Pokud měníte existující FAT nebo NTFS oblasti, je
doporučeno použít nástroje obsažené v instalaci Windows NT (nebo
pohodlněji přímo z AlphaBIOSu).
V ostatních případech je lepší použít linuxové nástroje, které
obvykle odvedou lepší práci.
Poznámka: pokud vám Disk Administrátor z Windows NT nabídne zapsat
<quote>harmless signature</quote> na ne-Windowsové disky (pokud nějaké
máte), <emphasis>nikdy</emphasis> je to nenechte udělat, protože tato
signatura zničí tabulku rozdělení disku!

</para><para>

Pokud plánujete zavádění Linuxu z konzoly ARC/AlphaBIOS/ARCSBIOS,
budete potřebovat malou FAT oblast pro zavaděč MILO.
(5 MB je plně dostačujících.) Pokud jsou nainstalovány Windows NT,
je možno pro tento účel využít jejich 6 MB velkou zaváděcí oblast.
Debian &releasename; nepodporuje instalaci MILa. Pokud již máte MILO
nainstalováno nebo je budete instalovat z jiného média, stále můžete
Debian zavést z ARC konzoly.

</para>
  </sect2>
