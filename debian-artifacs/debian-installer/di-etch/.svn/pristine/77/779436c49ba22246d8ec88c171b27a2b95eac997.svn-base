<!-- retain these comments for translator revision tracking -->
<!-- original version: 43734 -->

   <sect3 id="pkgsel">
   <title>Software auswählen und installieren (pkgsel)</title>

<para>

Während des Installationsprozesses wird Ihnen die Möglichkeit geboten,
zusätzliche Software zur Installation auszuwählen. Statt aber einzelne
Programme aus den &num-of-distrib-pkgs; verfügbaren Paketen
herauszusuchen, zielt dieser Punkt der Installation mehr darauf ab,
aus vorbereiteten Softwarezusammenstellungen die auszuwählen, die den
künftigen Aufgaben des Rechners am nächsten kommen.

</para><para>

<!-- TODO: Should explain popcon first -->

Sie können dazu zunächst Programmgruppen (<emphasis>tasks</emphasis>)
auswählen, die Sie installieren möchten, und anschließend noch einzelne
Pakete hinzufügen. Diese Programmgruppen repräsentieren eine lockere
Zusammenstellung von verschiedenen Aufgaben oder Dingen, die Sie mit
Ihrem Computer erledigen können, wie <quote>Desktop-Umgebung</quote>
(Arbeitsplatzrechner), <quote>Web-Server</quote> (Inhalte für das
Internet bereitstellen) oder <quote>Druck-Server</quote> (Drucken
und Druckmanagement) <footnote>

<para>

Sie sollten wissen, dass der Installer lediglich das Programm
<command>tasksel</command> aufruft, in dem dann diese Liste angezeigt
wird. <command>tasksel</command> kann auch nach der Installation jederzeit
aufgerufen werden, um
weitere Pakete zu installieren (oder sie zu entfernen), oder Sie benutzen
ein feinkörnigeres Werkzeug wie <command>aptitude</command>.
Wenn Sie ein bestimmtes einzelnes Paket suchen, nachdem die
Installation abgeschlossen ist, führen Sie einfach <userinput>aptitude install
<replaceable>package</replaceable></userinput> aus, wobei
<replaceable>package</replaceable> der Name des Paketes ist, das Sie
suchen.

</para>

</footnote>. <xref linkend="tasksel-size-list"/> gibt eine Übersicht über
den erforderlichen Speicherplatz für die verschiedenen Programmgruppen.

</para><para>

Einige der Programmgruppen sind unter Umständen vorgewählt, abhängig von
der Charakteristik des Rechners, den Sie installieren. Falls Sie mit der
Vorauswahl nicht zufrieden sind, können Sie sie ändern. Sie können sich
sogar entscheiden, an dieser Stelle gar keine Programmgruppen zu installieren.

</para>
<note><para>

<!-- TODO: Explain the "Standard system" task first -->

Die Programmgruppe <quote>Arbeitsplatzrechner</quote> installiert die
GNOME-Desktop-Umgebung. Die Optionen direkt im Installer erlauben es
derzeit nicht, eine andere Desktop-Umgebung (wie z.B. KDE) zu installieren.

</para><para>

Es ist trotzdem möglich, den Installer anzuweisen, KDE zu installieren,
und zwar mittels Voreinstellung (siehe <xref linkend="preseed-pkgsel"/>) oder
indem Sie beim Start des Installers am Boot-Prompt
<literal>tasks="standard, kde-desktop"</literal> angeben. Allerdings
wird dies nur funktionieren, wenn die Pakete, die für KDE benötigt werden,
auch verfügbar sind. Wenn Sie von einem Komplett-CD-Image installieren,
müssen diese von einem Spiegelserver heruntergeladen werden, da die
KDE-Pakete im ersten Komplett-CD-Image nicht enthalten sind; KDE auf diesem Weg
zu installieren, sollte aber funktionieren, wenn Sie von einem DVD-Image
oder mit irgendeiner anderen Installationsmethode installieren.

</para><para>

Die verschiedenen Server-Programmgruppen installieren grob gesagt folgende
Software.
DNS-Server: <classname>bind9</classname>;
Datei-Server: <classname>samba</classname>, <classname>nfs</classname>;
Mail-Server: <classname>exim4</classname>, <classname>spamassassin</classname>,
<classname>uw-imap</classname>;
Print-Server: <classname>cups</classname>;
SQL-Datenbank: <classname>postgresql</classname>;
Web-Server: <classname>apache</classname>.

</para></note>
<para>

Wenn Sie die gewünschten Programmgruppen ausgewählt haben, drücken Sie
<guibutton>OK</guibutton>. <command>aptitude</command> wird jetzt die
entsprechenden Pakete installieren.

</para>
<note><para>

In der Standard-Benutzeroberfläche des Installers (text-basierter Installer)
können Sie die Leertaste benutzen, um den Auswahlzustand der Programmgruppen
umzuschalten.

</para></note>
<para>

</para><para>

Sie sollten beachten, dass besonders die Programmgruppe Arbeitsplatzrechner
sehr gross ist. Speziell wenn Sie von einer normalen CD-ROM installieren und
zusätzlich Pakete, die dort nicht enthalten sind, von einem Spiegel-Server
beziehen, muss der Installer unter Umständen sehr viele Pakete über das
Netzwerk herunterladen. Falls Sie eine relativ langsame Internetverbindung
haben, kann dies recht lange dauern. Es gibt keine Möglichkeit, die
Installation der Pakete abzubrechen, wenn sie einmal gestartet ist.
 
 </para><para>

Sogar wenn Pakete auf der CD-ROM enthalten sind, könnte der Installer sie
trotzdem über das Internet herunterladen, wenn die auf dem Spiegel-Server
verfügbare Version neuer ist als die auf der CD-ROM. Falls Sie die
stable-Distribution installieren, kann dies vorkommen, wenn eine
Aktualisierung von stable stattgefunden hat (ein sogenanntes Point-Release,
hierbei werden Pakete nur aufgrund von Sicherheits-Updates oder kritischen
Fehlern aktualisiert); falls Sie testing installieren, kann dies passieren,
wenn Sie ein älteres CD-Image verwenden.

</para><para>

Jedes Paket, das Sie mit <command>tasksel</command> ausgewählt haben, wird
von den Programmen <command>apt-get</command> und <command>dpkg</command>
heruntergeladen, entpackt und dann installiert. Falls ein bestimmtes
Programm für die Installation zusätzliche Informationen von Ihnen
benötigt, werden Sie während dieses Prozesses danach gefragt.

</para>
   </sect3>
