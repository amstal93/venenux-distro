<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 43528 -->

  <sect2 arch="sparc" id="sparc-cpus">
<!--
  <title>CPU and Main Boards Support</title>
-->
  <title>CPU・マザーボードのサポート</title>
<para>

<!--
Sparc-based hardware is divided into a number of different subarchitectures,
identified by one of the following names: sun4, sun4c, sun4d, sun4m, sun4u
or sun4v. The following list describes what machines they include and what
level of support may be expected for each of them.
-->
Sparc ベースのハードウェアは、数種の異なるサブアーキテクチャに分かれており、
sun4, sun4c, sun4d, sun4m, sun4u, sun4v といった名前で認識されています。
以下に、各サブアーキテクチャにどのようなマシンが含まれ、
どの程度サポートされるだろうか、といったことを一覧します。

</para>

<variablelist>
<varlistentry>
<term>sun4, sun4c, sun4d</term>

<listitem><para>

<!--
These subarchitectures include some very old 32-bit machines, which are
no longer supported. For a complete list please consult the
<ulink url="http://en.wikipedia.org/wiki/SPARCstation">Wikipedia
SPARCstation page</ulink>.
-->
このサブアーキテクチャには、旧式の 32 ビットマシンが含まれており、
もうサポートされていません。
完全なリストは、
<ulink url="http://en.wikipedia.org/wiki/SPARCstation">Wikipedia の
SPARCstation ページ</ulink> をご覧ください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4m</term>

<listitem><para>

<!--
sun4m is the only 32-bit subarchitecture (sparc32) that is currently
supported. The most popular machines belonging to this class are
Sparcstation 4, 5, 10 and 20.
-->
sun4m は現在サポートしている、
唯一の 32 ビットサブアーキテクチャ (sparc32) です。
このクラスのもっともポピュラーなマシンは、Sparcstation 4, 5, 10, 20 です。

</para><para>

<!--
Note that symmetric multiprocessing (SMP) &mdash; the ability to run
processes on multiple processors &mdash; is not supported on this hardware,
due to stability problems with such configurations. The available
uniprocessor (UP) sparc32 kernel will boot fine on multiprocessor
machines, although it will activate and use only the first CPU.
-->
複数のプロセッサでプロセスを実行する対称型マルチプロセッシング (SMP) を、
安定性に問題があるため、このハードウェアでは、
サポートしていないことに注意してください。
単一プロセッサ (UP) sparc32 カーネルはマルチプロセッサマシンでも、
(最初の CPU しか有効になりませんが) 問題なく起動します。

</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4u</term>

<listitem><para>

<!--
This subarchitecture includes all 64-bit machines (sparc64) based on
the UltraSparc processor and its clones. Most of the machines are well
supported, even though for some you may experience problems booting from
CD due to firmware or bootloader bugs (this problem may be worked around
by using netbooting). Use the sparc64 or sparc64-smp kernel in UP and SMP
configurations respectively.
-->
このサブアーキテクチャには、UltraSparc プロセッサとその互換 CPU を搭載した、
全 64 ビットマシン (sparc64) が含まれます。
ファームウェアやブートローダのバグで、
CD から起動する際に問題が起きる可能性もありますが 
(netboot でこの問題に対処できます)、
ほとんどのマシンがきちんとサポートされています。
UP や SMP の構成にあわせて、
sparc64 か sparc64-smp のカーネルを使用してください。

</para></listitem>
</varlistentry>

<varlistentry>
<term>sun4v</term>

<listitem><para>

<!--
This is the newest addition to the Sparc family, which includes machines
based on the Niagara multi-core CPUs. At the moment such CPUs are only
available in T1000 and T2000 servers by Sun, and are well supported. Use
the sparc64-smp kernel.
-->
Sparc ファミリに新しく追加され、
Niagara マルチコア CPU を搭載したマシンを含んでいます。
現在、こういった CPU は Sun の T1000 サーバや T2000 サーバでのみ利用でき、
きちんとサポートされています。sparc64-smp カーネルを使用してください。

</para></listitem>
</varlistentry>
</variablelist>

<para>

<!--
Note that Fujitsu's SPARC64 CPUs used in PRIMEPOWER family of servers are not
supported due to lack of support in the Linux kernel.
-->
サーバの PRIMEPOWER ファミリで使用されている富士通の SPARC64 CPU は、
Linux カーネルのサポートが不充分なため、
Debian でもサポートしていないことにご注意ください。

</para>
  </sect2>
