<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43693 -->
<!-- Revisado por Rudy Godoy -->


  <sect2 arch="arm"><title>Soporte de CPU, placas base y v�deo</title>

<para>

Cada arquitectura de ARM diferente requiere su propio n�cleo. Esto
es debido a que la distribuci�n est�ndar de Debian solamente soporta
la instalaci�n de las plataformas m�s comunes. El espacio de usuario de
Debian puede utilizarse, sin embargo, en <emphasis>cualquier</emphasis>
CPU ARM incluyendo �xscale�.

</para>

<para>

Muchas CPUs ARM tambi�n pueden funcionar en cualquier modo endian
�little-endian� o �big-endian�).
Sin embargo, la mayor�a de las implementaciones de sistemas actuales usan
el modo �little-endian�.
Debian s�lo soporta actualmente sistemas ARM �little-endian�.

</para>

<para>

Las plataformas soportadas son:

<variablelist>
<varlistentry>
<term>Netwinder</term>
<listitem><para>

�ste es realmente el nombre para el grupo de m�quinas basadas en
el procesador StrongARM 110 e Intel 21285 Northbridge (tambi�n 
conocido como Footbridge). Incluye
las m�quinas: Netwinder (posiblemente una de las m�quinas ARM
m�s comunes), CATS (tambi�n conocida como EB110ATX), EBSA 285 y
Compaq personal server (cps, tambi�n conocido como skiff).

</para></listitem>
</varlistentry>

<varlistentry>
<term>IOP32x</term>
<listitem><para>

La l�nea de procesadores de Intel de Entrada/Salida (�Intel's I/O Processor� o
IOP) se puede encontrar en un conjunto de productos dedicados al almacenamiento
y procesado de datos. Debian soporta actualmente la plataforma IOP32x, que
incluye los chips IOP 80219 y 32x que se encuentran habitualmente en dispositivos
de almacenamiento en red (� Network Attached Storage�, NAS). Debian soporta de
forma expl�cita dos de estos dispositivos: el equipo <ulink url="&url-arm-cyrius-glantank;">GLAN Tank</ulink> de IO-Data y el
<ulink url="&url-arm-cyrius-n2100;">Thecus N2100</ulink>.
</para></listitem>
</varlistentry>

<varlistentry>
<term>IXP4xx</term>
<listitem><para>

La plataforma IXP4xx est� basada en el core ARM XScale de Intel. Actualmente
s�lo se dispone de soporte para un sistema basado en IXP4xx: el NSLU2 de
Linksys. El NSLU2 (�Network Storage Link for USB 2.0 Disk Drives�) de Linksys
es un peque�o dispositivo que permite la conexi�n a almacenamiento de forma
sencilla a trav�s de la red. Se provee con una conexi�n de 
red Ethernet y dos puertos USB a los que se pueden conectar unidades de
disco. Hay un sitio externo con <ulink
url="&url-arm-cyrius-nslu2;">instrucciones de instalaci�n</ulink>.


</para></listitem>
</varlistentry>

<varlistentry>
<term>RiscPC</term>
<listitem><para>

�sta m�quina es el hardware soportado m�s antiguo. Pero el soporte del
instalador para este es incompleto.  Tiene un RISC OS en la ROM y Linux puede
arrancarse desde este sistema operativo usando �linloader�. El sistema RiscPC
tiene una tarjeta de CPU modular y habitualmente tiene un procesador 30MHz 610,
40MHz 710 � 233MHz Strongarm 110 empotrada. La placa base tiene integrados
los dispositivos IDE, video SVGA,
el puerto paralelo, un �nico puerto de serie, el teclado PS/2 y
un puerto de rat�n propietario. El bus
de expansi�n propietario permite conectar de cuatro a ocho tarjetas de expansi�n
que se pueden incluir dependiendo de la configuraci�n, varios de estos
m�dulos tienen controladores para Linux.

</para></listitem>
</varlistentry>

</variablelist>

</para>
  </sect2>
