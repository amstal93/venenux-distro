#!/bin/sh

# This script removes the lvm flag for partitions whose method is not
# lvm and sets it for partition whose method is lvm

. /lib/partman/definitions.sh

dev=$1
num=$2
id=$3
size=$4
type=$5
fs=$6
path=$7

cd $dev

if [ $fs = free ]; then
	exit 0
fi

method=
if [ -f $id/method ]; then
	method=$(cat $id/method)
fi

# As setting flags on RAID devices does not work and causes errors from
# libparted, do not attempt to sync flags in case of LVM on RAID.
# This is in line with the hacks in init.d/lvm and undo.d/lvm.
if [ -f device ]; then
	case $(cat device) in
	    /dev/md/*)
		exit 0 ;;
	esac
fi

has_lvm=no
flags=''
open_dialog GET_FLAGS $id
while { read_line flag; [ "$flag" ]; }; do
	if [ "$flag" != lvm ]; then
		if [ "$flags" ]; then
			flags="$flags
$flag"
		else
			flags="$flag"
		fi
	else
		has_lvm=yes
	fi
done
close_dialog

# Some flags make no sense in combination with lvm
cleanflags=''
for flag in $flags; do
	if [ "$flag" = raid ]; then
		continue
	elif [ "$flag" = swap ]; then
		continue
	elif [ -n "$cleanflags" ]; then
		cleanflags="$cleanflags
$flag"
	else
		cleanflags="$flag"
	fi
done

if [ "$method" = '' ] && [ "$has_lvm" = yes ]; then
	echo lvm >$dev/$id/method
	rm -f $dev/$id/use_filesystem
	rm -f $dev/$id/format
elif [ "$method" = lvm ] && [ "$has_lvm" = no ]; then
	open_dialog SET_FLAGS $id
	write_line "$cleanflags"
	write_line lvm
	write_line NO_MORE
	close_dialog
elif [ "$method" != lvm ] && [ "$has_lvm" = yes ]; then
	open_dialog SET_FLAGS $id
	write_line "$flags"
	write_line NO_MORE
	close_dialog
fi
