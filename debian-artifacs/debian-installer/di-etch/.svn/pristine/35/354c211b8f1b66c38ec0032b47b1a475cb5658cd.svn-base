<!-- Start of file welcome/welcome.xml -->
<!-- $Id: welcome.xml 28672 2005-06-26 10:06:30Z fjp $ -->

<chapter id="welcome"
><title
>Chào mừng bạn dùng Debian</title>
<para
>Chương này cung cấp tổng quan của Dự Án Debian và &debian;. Nếu bạn quen với lịch sử của Dự Án Debian và bản phát hành &debian;, bạn cũng nhảy được tới chương kế tiếp. </para>


<!-- Start of file welcome/what-is-debian.xml -->
<!-- $Id: what-is-debian.xml 45347 2007-02-22 15:26:47Z fjp $ -->

 <sect1 id="what-is-debian">
 <title
>Debian là gì vậy?</title>
<para
>Debian là một tổ chức nguyên tình nguyện cống hiến để phát triển phần mềm tự do và đẩy mạnh những lý tưởng của Tổ Chức Phần Mềm Tự Do. Dự Án Debian mới tạo trong năm 1993, khi Ian Murdock gởi lời mời mở cho các nhà phát triển phần mềm để đóng góp cho một bản phát hành hoàn toàn và mạch lạc dựa vào hạt nhân Linux hơi mới. Nhóm người say mê cống hiến hơi nhỏ đó, đầu tiên do <ulink url="&url-fsf-intro;"
>Tổ Chức Phần Mềm Tự Do</ulink
> hỗ trợ, cũng theo triết lý của tổ chức <ulink url="&url-gnu-intro;"
>GNU</ulink
> , đã lớn lên qua một số năm để trở thành một tổ chức có khoảng &num-of-debian-developers; <firstterm
>Nhà Phát Triển Debian</firstterm
>. </para
><para
>Nhà Phát Triển Debian tham gia nhiều hoạt động khác nhau, gồm quản trị chỗ Mạng <ulink url="&url-debian-home;"
>HTTP</ulink
> và <ulink url="&url-debian-ftp;"
>FTP</ulink
>, thiết kế đồ họa, phân tích pháp luật các giấy phép phần mềm, tạo tài liệu hướng dẫn và, tất nhiên, bảo trì gói phần mềm. </para
><para
>Để truyền triết lý của chúng tôi, và hấp dẫn nhà phát triển theo những nguyên tắc do Debian hỗ trợ, Dự Án Debian đã xuất bản một số tài liệu phác họa các giá trị của chúng tôi, cũng chỉ dẫn người nào muốn trở thành nhà phát triển Debian. <itemizedlist>
<listitem
><para
><ulink url="&url-social-contract;"
>Hợp Động Xã Hội Debian</ulink
> phát biểu các lời cam kết Debian cho Cộng Đồng Phần Mềm Tự Do. Bất cứ ai hứa tuân theo Hợp Động Xã Hội thì có thể trở thành <ulink url="&url-new-maintainer;"
>nhà bảo trì</ulink
>. Bất cứ nhà bảo trì nào có khả năng giới thiệu phần mềm mới vào Debian &mdash; miễn là gói phần mềm thỏa tiêu chuẩn cả tự do lẫn chất lượng của chúng tôi. </para
></listitem>
<listitem
><para
><ulink url="&url-dfsg;"
>Chỉ Dẫn Phần Mềm Tự Do Debian</ulink
> (DFSG) là lời tuyên bố rõ ràng và ngắn gọn về tiêu chuẩn phần mềm tự do của Debian. DFSG là tài liệu có ảnh hưởng rất lớn trong Phong Trào Phần Mềm Tự Do, cũng đã đặt nền móng cho <ulink url="&url-osd;"
>Lời Định Nghĩa Nguồn Mở</ulink
>. </para
></listitem>
<listitem
><para
><ulink url="&url-debian-policy;"
>Sổ Tay Chính Sách Debian</ulink
> là đặc tả rộng rãi về những tiêu chuẩn chất lượng của Dự Án Debian. </para
></listitem>
</itemizedlist>
</para
><para
>Nhà phát triển Debian cũng tham gia một số dự án khác, đặc trưng cho Debian hay gồm một phần cộng đồng Linux. Chẳng hạn: <itemizedlist>
<listitem
><para
><ulink url="&url-lsb-org;"
>Cơ Bản Linux Chuẩn</ulink
> (LSB) là dự án nhắm mục đích là tiêu chuẩn hóa hệ thống GNU/Linux cơ bản, mục đích sẽ cho mỗi nhà phát triển phần mềm hay phần cứng có khả năng dễ dàng thiết kế chương trình và trình điều khiển thiết bị cho Linux chung, hơn là cho một bản phát hành GNU/Linux riêng. </para
></listitem>
<listitem
><para
><ulink url="&url-fhs-home;"
>Tiêu Chuẩn Phân Cấp Hệ Thống Tập Tin</ulink
> (FHS) là sự cố gắng tiêu chuẩn hóa bố trí của hệ thống tập tin Linux. FHS sẽ cho nhà phát triển phần mềm có khả năng tập trung sự cố gắng để thiết kế chương trình, không cần lo lăng về phương pháp cài đặt gói đó vào mỗi bản phát hành GNU/Linux riêng. </para
></listitem>
<listitem
><para
><ulink url="&url-debian-jr;"
>Debian Còn Trẻ</ulink
> là một dự án bên trong, nhắm mục đích là bảo đảm Debian hấp dẫn được người dùng trẻ nhất. </para
></listitem>
</itemizedlist>

</para
><para
>Để tìm thông tin chung thêm về Debian, xem <ulink url="&url-debian-faq;"
>Hỏi Đáp Debian</ulink
>. </para>

 </sect1>

<!--   End of file welcome/what-is-debian.xml -->

<!-- Start of file welcome/what-is-linux.xml -->
<!-- $Id: what-is-linux.xml 45186 2007-02-16 15:19:34Z fjp $ -->

 <sect1 id="what-is-linux">
 <title
>GNU/Linux là gì vậy?</title>
<para
>Linux là hệ điều hành: một dãy chương trình cho bạn khả năng tương tác với máy tính, cũng chạy chương trình khác. </para
><para
>Một hệ điều hành gồm có nhiều chương trình cơ bản khác nhau do máy tính cần thiết để liên lạc với và nhận lệnh từ người dùng; đọc từ và ghi vào đĩa cứng, băng và máy in; điều khiển cách sử dụng bộ nhớ; chạy phần mềm khác. Trong hệ điều hành, phần quan trọng nhất là hạt nhân. Trong hệ thống kiểu GNU/LInux, Linux là thành phần hạt nhân. Phần còn lại của hệ thống chứa chương trình khác nhau, gồm nhiều phần mềm do dự án GNU ghi hay hỗ trợ. Vì hạt nhân Linux đơn độc không làm toàn bộ hệ điều hành, chúng tôi chọn sử dụng tên <quote
>GNU/Linux</quote
> để diễn tả hệ thống cũng có tên cẩu thả <quote
>Linux</quote
>. </para
><para
>Hệ thống Linux làm theo hệ điều hành UNIX. Kể từ đầu, Linux đã được thiết kế như là hệ thống đa tác vụ, đa người dùng. Những sự thật này là đủ làm cho Linux khác với các hệ điều hành nổi tiếng khác. Tuy nhiên, Linux vẫn còn khác hơn. Trái ngược với hệ điều hành khác, không có ai sở hữu Linux. Phần lớn việc phát triển nó được làm bởi người tình nguyện không được tiền. </para
><para
>Tiến trình phát triển cái trở thành GNU/Linux đã bắt đầu trong năm 1984, khi <ulink url="http://www.gnu.org/"
>Tổ Chức Phần Mềm Tự Do</ulink
> bắt đầu phát triển một hệ điều hành miễn phí kiểu Unix được gọi là GNU. </para
><para
>Dự Án GNU đã phát triển một bộ gần hết các công cụ phần mềm tự do để sử dụng với UNIX&trade; và hệ điều hành kiểu UNIX như Linux. Những công cụ này cho người dùng có khả năng thực hiện công việc trong phạm vị từ việc thường (như sao chép hay gỡ bỏ tập tin ra hệ thống) đến việc phức tạp (như ghi hay biên dịch chương trình hoặc hiệu chỉnh cấp cao nhiều dạng thức tài liệu khác nhau). </para
><para
>Mặc dù nhiều nhóm và người đã đóng góp cho Linux, Tổ Chức Phần Mềm Tự Do vẫn còn đã đóng góp nhiều nhất: nó đã tạo phần lớn công cụ được dùng trong Linux, ngay cả triết lý và cộng đồng hỗ trợ nó. </para
><para
> <ulink url="&url-kernel-org;"
>Hạt nhân Linux</ulink
> mới xuất hiện trong năm 1991, khi một học sinh vi tính tên Linus Torvalds loan báo cho nhóm tin tức Usenet <userinput
>comp.os.minix</userinput
> một phiên bản sớm của hạt nhân thay thế điều của Minix. Xem trang lịch sử Linux <ulink url="&url-linux-history;"
>Linux History Page</ulink
> của Linux Quốc Tế. </para
><para
>Linux Torvalds tiếp tục điều hợp các công việc của vài trăm nhà phát triển, với sự giúp đỡ của vài người thay quyền đáng tin. Xem <ulink url="&url-kernel-traffic;"
>Kernel Traffic</ulink
>, một bản tóm tắt hàng tuần rất tốt của những cuộc thảo luận trong hộp thư chung hạt nhân Linux <userinput
>linux-kernel</userinput
>. Cũng xem Hỏi Đáp <ulink url="&url-linux-kernel-list-faq;"
>linux-kernel mailing list FAQ</ulink
> để tìm thông tin thêm về hộp thư chung này. </para
><para
>Người dùng Linux có khả năng chọn phần mềm một cách rất tự do. Chẳng hạn, người dùng Linux có thể chọn trong mười hai trình bao dòng lệnh, cũng trong vài môi trường đồ họa. Lựa chọn này có thể làm bối rối người dùng hệ điều hành khác, không quen với ý kiến có khả năng thay đổi dòng lệnh hay môi trường đồ họa. </para
><para
>Hơn nữa, Linux sụp đổ ít hơn, chạy dễ hơn nhiều chương trình đồng thời, cũng là bảo mật hơn nhiều hệ điều hành khác. Do những lợi ích này, Linux là hệ điều hành lớn lên nhanh nhất trong thị trường trình phục vụ. Gần đây hơn, Linux cũng mới ưa chuộng với người dùng kinh doanh và ở nhà. </para>

 </sect1>

<!--   End of file welcome/what-is-linux.xml -->

<!-- Start of file welcome/what-is-debian-linux.xml -->
<!-- $Id: what-is-debian-linux.xml 25496 2005-02-07 14:56:06Z fjpop-guest $ -->

 <sect1 id="what-is-debian-linux">
 <title
>&debian; là gì vậy?</title>
<para
>Kết hợp triết lý và phương pháp luận của Debian với những công cụ GNU, hạt nhân Linux, và phần mềm tự do quan trọng khác, các điều này thành lập một bản phát hành phần mềm duy nhất được gọi là &debian;. Bản phát hành này gồm có rất nhiều <emphasis
>gói</emphasis
> phần mềm. Trong bản phát hành này, mỗi gói chứa chương trình chạy được, tập lệnh, tài liệu hướng dẫn và thông tin cấu hình, cũng có một <emphasis
>nhà bảo trì</emphasis
> nhận trách nhiệm chính cập nhật gói đó, theo dõi thông báo lỗi, và liên lạc với tác giả gốc của phần mềm đã đóng gói. Cơ bản người dùng rất lớn của chúng tôi, cùng với hệ thống theo dõi lỗi, bảo đảm các sự khó được tìm và sửa nhanh. </para
><para
>Tập trung Debian với chi tiết có kết quả là một bản phát hành có chất lượng cao, ổn định, và có khả năng co giãn. Có thể cấu hình dễ dàng bản cài đặt để thực hiện nhiều nhiệm vụ, từ bức tường lửa độc lập, đến máy trăm khoá học để bàn, đến máy phục vụ mạng lớp cao. </para
><para
>Debian nối tiếng nhất trong những người dùng cấp cao vì nó có kỹ thuật rất tốt, cam kết năng nổ với các sự cần và sự mong đợi của cộng đồng Linux. Debian cũng đã giới thiệu vào Linux nhiều tính năng đã trở thành thường dụng. </para
><para
>Chẳng hạn, Debian là bản phát hành Linux thứ nhất có gồm hệ thống quản lý gói để cài đặt và gỡ bỏ phần mềm một cách dễ dàng. Nó cũng là bản phát hành Linux thứ nhất có khả năng nâng cấp không cần cài đặt lại. </para
><para
>Debian tiếp tục dẫn đường phát triển Linux. Tiến trình phát triển của nó là thông lệ xuất sắc của mẫu phát triển Nguồn Mở &mdash; ngay cả cho công việc rất phức tạp như xây dựng và bảo trì một hệ điều hành hoàn toàn. </para
><para
>Tính năng khác biệt Debian nhiều nhất với các bản phát hành Linux khác là hệ thống quản lý gói. Những công cụ này cho quản trị hệ thống Debian khả năng điều khiển hoàn toàn mọi gói được cài đặt vào hệ thống đó, gồm khả năng cài đặt một gói riêng hoặc cập nhật tự động toàn bộ hệ điều hành. Cũng có thể bảo vệ gói riêng chống cập nhật. Bạn ngay cả có khả năng báo hệ thống quản lý gói biết về phần mềm tự biên dịch và cách phụ thuộc nào nó thỏa. </para
><para
>Để bảo vệ hệ thống của bạn chống <quote
>vi rút Trojan</quote
> và phần mềm hiểm độc khác, máy phục vụ Debian kiểm tra mỗi gói được tải lên từ nhà bảo trì Debian đã đăng ký của nó. Người đóng gói Debian cũng rất cẩn thận để cấu hình gói một cách bảo mật. Khi lỗi bảo mật xuất hiện trong gói đã phát hành, thường cung cấp rất nhanh cách sửa. Với những tùy chọn cập nhật đơn giản của Debian, cách sửa bảo mật có thể được tải về và cài đặt tự động qua Mạng. </para
><para
>Phương pháp chính và tốt nhất để được sự hỗ trợ cho hệ thống &debian; của bạn, cũng để liên lạc với Nhà Phát Triển Debian là bằng những hộp thư chung do Dự Án Debian bảo trì (có hơn &num-of-debian-maillists; hộp thư vào lúc viết câu này). Cách dễ nhất để đăng ký tham gia một hay nhiều hộp thư chung này là thăm trang đăng ký hộp thư chung Debian <ulink url="&url-debian-lists-subscribe;"
>Debian's mailing list subscription page</ulink
> rồi điền vào đơn tại đó. </para>

 </sect1>
<!--   End of file welcome/what-is-debian-linux.xml -->

<!-- Start of file welcome/what-is-debian-hurd.xml -->
<!-- $Id: what-is-debian-hurd.xml 28672 2005-06-26 10:06:30Z fjp $ -->

<!-- conditionalised because the hurd port is not yet an official debian
release -->
 <sect1 id="what-is-debian-hurd" condition="unofficial-build">
 <title
>Debian GNU/Hurd là gì vậy?</title>

<para
>Debian GNU/Hurd là một hệ thống kiểu GNU thay thế hạt nhân lớn riêng Linux bằng điều Hurd của GNU &mdash; một bộ trình phục vụ có chạy bên trên vi hạt nhân Mach của GNU. Hurd vẫn chưa hoàn toàn, hiện thời không thích hợp để sử dụng hàng ngày, nhưng vẫn còn đang cấu tạo nó. Hurd hiện thời đang được phát triển chỉ cho kiến trúc i386, dù nó sẽ được chuyển sang những kiến trúc khác một khi hệ thống ổn định hơn. </para
><para
>Để tìm thông tin thêm, xem trang bản chuyển Debian GNU/Hurd <ulink url="http://www.debian.org/ports/hurd/"
>Debian GNU/Hurd ports page</ulink
> và hộp thư chung <email
>debian-hurd@lists.debian.org</email
>. </para>

 </sect1>
<!--   End of file welcome/what-is-debian-hurd.xml -->

<!-- Start of file welcome/getting-newest-inst.xml -->
<!-- $Id: getting-newest-inst.xml 28672 2005-06-26 10:06:30Z fjp $ -->

 <sect1 id="getting-newest-inst">
 <title
>Lấy Debian</title>

<para
>Để tìm thông tin về cách tải &debian; xuống Mạng hoặc từ họ có thể mua đĩa CD Debian chính thức, xem trang bản phát hành <ulink url="&url-debian-distrib;"
>distribution web page</ulink
>. Danh sách các máy nhân bản Debian <ulink url="&url-debian-mirrors;"
>list of Debian mirrors</ulink
> chứa bộ đầy đủ của máy nhân bản Debian chính thức, để cho bạn tìm máy gần nhất chỗ mình. </para
><para
>Rất dễ dàng nâng cấp được Debian sau khi cài đặt. Thủ tục cài đặt sẽ giúp đỡ bạn thiết lập hệ thống để cho bạn khả năng nâng cấp nó một khi cài đặt xong, nếu cần thiết. </para>

 </sect1>
<!--   End of file welcome/getting-newest-inst.xml -->

<!-- Start of file welcome/getting-newest-doc.xml -->
<!-- $Id: getting-newest-doc.xml 28672 2005-06-26 10:06:30Z fjp $ -->

 <sect1 id="getting-newest-doc">
 <title
>Lấy phiên bản mới nhất của tài liệu này</title>

<para
>Tài liệu này đang được sửa đổi liên miên. Bạn hãy kiểm tra xem trang bản phát hành &release; Debian <ulink url="&url-release-area;"
>Debian &release; pages</ulink
> tìm tin tức nào về bản phát hành &release; của hệ thống &debian;. Phiên bản đã cập nhật của sổ tay cài đặt này cũng sẵn sàng từ trang Sổ Tay Cài Đặt chính thức <ulink url="&url-install-manual;"
>official Install Manual pages</ulink
>. </para>

 </sect1>
<!--   End of file welcome/getting-newest-doc.xml -->

<!-- Start of file welcome/doc-organization.xml -->
<!-- $Id: doc-organization.xml 33772 2006-01-04 22:07:27Z fjp $ -->

 <sect1 id="doc-organization">
 <title
>Cấu trúc của tài liệu này</title>

<para
>Tài liệu này được thiết kế nhằm sổ tay cho người dùng bắt đầu chạy Debian. Nó cố gắng giả sử càng ít càng có thể về lớp kỹ năng của bạn. Tuy nhiên, chúng tôi có phải giả sử là bạn có kiến thức chung về hoạt động của các phần cứng của máy tính của mình. </para
><para
>Trong tài liệu này, người dùng thành thạo cũng tìm được thông tin tham chiếu hay, gồm kích cỡ cài đặt tối thiểu, và chi tiết về phần cứng được hỗ trợ bởi hệ thống cài đặt Debian. Chúng tôi khuyên người dùng thành thạo theo đường dẫn riêng trong tài liệu này. </para
><para
>Nói chung, sổ tay này được sắp xếp bằng thứ tự tuyến tính, dẫn bạn qua tiến trình cài đặt từ đầu đến cuối. Đây là những bước cài đặt &debian;, và tiết đoạn tài liệu tương ứng với mỗi bước: <orderedlist>
<listitem
><para
>Quyết định nếu phần cứng có thỏa tiêu chuẩn sử dụng hệ thống cài đặt chưa, trong <xref linkend="hardware-req"/>. </para
></listitem>
<listitem
><para
>Lưu trữ hệ thống, thực hiện việc định và cấu hình phần cứng nào cần thiết trước khi cài đặt Debian, trong <xref linkend="preparing"/>. Nếu bạn chuẩn bị một hệ thống đa khởi động, bạn có thể cần phải tạo sức chứa phân vùng được trên phần cứng cho Debian dùng. </para
></listitem>
<listitem
><para
>Trong <xref linkend="install-methods"/>, bạn sẽ giành những tập tin cài đặt cần thiết cho phương pháp cài đặt đã chọn. </para
></listitem>
<listitem
><para
><xref linkend="boot-installer"/> diễn tả tiến trình khởi động vào hệ thống cài đặt. Chương này cũng diễn tả thủ tục giải đáp thắc mắc trong trường hợp bạn gặp khó khăn thực hiện bước này. </para
></listitem>
<listitem
><para
>Thực hiện việc cài đặt thật tùy theo <xref linkend="d-i-intro"/>. Tiến trình này đòi hỏi cần phải chọn ngôn ngữ của bạn, cấu hình mô-đun điều khiển ngoại vi, cấu hình sự kết nối mạng để lấy được các tập tin cài đặt còn lại từ máy phục vụ Debian (nếu bạn không cài đặt từ đĩa CD), phân vùng đĩa cứng và cài đặt hệ thống cơ bản, rồi chọn và cài đặt công việc. (Một phần thông tin bối cảnh về cách thiết lập phân vùng cho hệ thống Debian được giải thích trong <xref linkend="partitioning"/>.) </para
></listitem>
<listitem
><para
>Khởi động vào hệ thống cơ bản mới cài đặt, từ <xref linkend="boot-new"/>. </para>
</listitem>
</orderedlist>

</para
><para
>Một khi cài đặt xong hệ thống, bạn đọc <xref linkend="post-install"/>. Chương này giải thích nơi cần tìm thông tin thêm về Unix và Debian, và cách thay thế hạt nhân. </para
><para
>Cuối cùng, thông tin về tài liệu này và cách đóng góp cho nó, nằm trong <xref linkend="administrivia"/>. </para>

 </sect1>

 <sect1 condition="FIXME">
 <title
>Mời bạn giúp đỡ tạo tài liệu hướng dẫn</title>

<para
>Chúng tôi rất cảm kích bạn giúp đỡ hay đề nghị gì, nhất là đóng góp đắp vá. Phiên bản hoạt động của tài liệu này nằm tại trang sổ tay cài đặt Debian <ulink url="&url-d-i-alioth-manual;"
></ulink
>. Trang đó hiển thị danh sách các kiến trúc và ngôn ngữ cho đó tài liệu này được công bố. </para
><para
>Mã nguồn cũng được công bố: xem trong <xref linkend="administrivia"/> để tìm thông tin thêm về cách đóng góp. Chúng tôi mời bạn đề nghị, chú ý, tạo đắp vá thông báo lỗi (thông báo lỗi đối với gói <classname
>installation-guide</classname
>, nhưng trước hết kiểm tra nếu lỗi đó đã được thông báo chưa). </para>
 </sect1>
<!--   End of file welcome/doc-organization.xml -->

<!-- Start of file welcome/about-copyright.xml -->
<!-- $Id: about-copyright.xml 29000 2005-07-07 21:37:38Z fjp $ -->

 <sect1>
<title
>Về tác quyền và giấy phép phần mềm </title>

<para
>Chúng tôi chắc là bạn đã đọc một số giấy phép được phát hành cùng với hậu hết phần mềm buôn bán &mdash; chúng thường nói là bạn có quyền dùng chỉ một bản sao của phần mềm đó trên một máy tính riêng lẻ. Giấy phép của hệ thống này là rất khác với đó. Chúng tôi mời bạn cài đặt một bản sao của hệ thống này vào mọi máy tính trong trường học hay chỗ làm của bạn. Cho các người bạn mượn vật chứa phần mềm cài đặt, cũng giúp đỡ họ cài đặt nó vào các máy tính nhé ! Bạn ngay cả có quyền tạo vài nghìn bản sao và <emphasis
>bán</emphasis
> chúng &mdash; dù với một số điều kiện. Quyền cài đặt và sử dụng hệ thống này dựa trực tiếp vào cơ bản <emphasis
>phần mềm tự do</emphasis
> của Debian. </para
><para
>Gọi phần mềm là <emphasis
>tự do</emphasis
> không có nghĩa là phần mềm không có tác quyền, cũng không có nghĩa là đĩa CD chứa phần mềm này phải được phát hành miễn phí. Phần mềm tự do, phần nào, có nghĩa là giấy phép của chương trình riêng không cần thiết bạn trả tiền cho quyền phát hành hay sử dụng chương trình đó. Phần mềm tự do cũng có nghĩa là bất cứ ai co thể mở rộng, thích ứng và sửa đổi phần mềm đó, cũng phát hành kết quả của sự cố gắng của họ. <note
><para
>Dự án Debian, để giúp đỡ người dùng, có phải làm cho công bố một số gói không thỏa tiêu chuẩn tự do của chúng tôi. Tuy nhiên, những gói này không phải thuộc về bản phát hành chính thức, cũng chỉ sẵn sàng từ phần <userinput
>đóng góp</userinput
> (contrib) hay <userinput
>khác tự do</userinput
> (non-free) của máy nhân bản Debian hay trên đĩa CD-ROM nhóm ba; xem Hỏi Đáp Debian  <ulink url="&url-debian-faq;"
>Debian FAQ</ulink
>, dưới Kho FTP Debian <quote
>The Debian FTP archives</quote
>, để tìm thông tin thêm về bố trí và nội dung của kho đó. </para
></note>

</para
><para
>Nhiều chương trình của hệ thống được phát hành với điều kiện của <emphasis
>Giấy Phép Công Cộng GNU</emphasis
>, thường được gọi đơn giản là <quote
>GPL</quote
>. Giấy phép GPL cần thiết bạn làm cho <emphasis
>mã nguồn</emphasis
> của chương trình sẵn sàng khi nào bạn phát hành một bản sao nhị phân của chương trình đó; điều khoản này trong giấy phép thì bảo đảm bất cứ người dùng nào có thể sửa đổi phần mềm đó. Do điều khoản này, mã nguồn <footnote
> <para
> Để tìm thông tin về phương pháp định vị, giải nén và xây dựng bộ nhị phân từ gói mã nguồn Debian, xem Hỏi Đáp Debian <ulink url="&url-debian-faq;"
>Debian FAQ</ulink
>, dưới Những điều cơ bản của Hệ Thống Quản Lý Gói Debian (<quote
>Basics of the Debian Package Management System</quote
>).</para
></footnote
> cho mọi chương trình như vậy có sẵn trong hệ thống Debian. </para
><para
>Có vài kiểu khác của lời tuyên bố tác quyền và giấy phép phần mềm được áp dụng cho chương trình của Debian. Bạn có thể tìm tác quyền và giấy phép dành cho mỗi gói được cài đặt vào hệ thống, bằng cách xem tập tin <filename
>/usr/share/doc/<replaceable
>tên_gói</replaceable
>/copyright </filename
> một khi gói đó được cài đặt vào hệ thống. </para
><para
>Để tìm thông tin thêm về giấy phép và cách Debian quyết định nếu phần mềm là đủ tự do để được bao gồm trong bản phát hành chính, xem Chỉ Dẫn Phần Mềm Tự Do Debian <ulink url="&url-dfsg;"
>Debian Free Software Guidelines</ulink
>. </para
><para
>Thông báo hợp pháp quan trọng nhất là: phần mềm này <emphasis
>không bảo hành gì cả</emphasis
>.  Những lập trình viên tạo phần mềm này đã làm như thế để giúp đỡ cộng đồng. Không bảo hành sự thích hợp của phần mềm cho mục đích riêng nào. Tuy nhiên, vì phần mềm là tự do, bạn có quyền sửa đổi nó để thích hợp với sự cần của mình &mdash; cũng để thích thú lợi ích của các sự sửa đổi được tạo bởi người khác đã mở rộng phần mềm đó bằng cách này. </para>
 </sect1>
<!--   End of file welcome/about-copyright.xml -->

</chapter>
<!--   End of file welcome/welcome.xml -->
