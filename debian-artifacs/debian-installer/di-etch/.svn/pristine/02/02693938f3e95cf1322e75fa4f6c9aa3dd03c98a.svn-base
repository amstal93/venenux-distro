<?xml version="1.0" encoding="EUC-JP"?>
<!-- retain these comments for translator revision tracking -->
<!-- original version: 36639 -->

  <sect2 arch="arm" id="boot-tftp"><title>TFTP からの起動</title>

&boot-installer-intro-net.xml;

   <sect3 arch="arm"><title>Netwinder での TFTP からの起動</title>
<para>

<!--
Netwinders have two network interfaces: A 10Mbps NE2000-compatible
card (which is generally referred to as <literal>eth0</literal>) and
a 100Mbps Tulip card.  There may be problems loading the image via TFTP
using the 100Mbps card so it is recommended that you use the 10Mbps
interfaced (the one labeled with <literal>10 Base-T</literal>).
-->
Netwinder には 2 つのネットワークインターフェースがあります。
10Mbps の NE2000 互換カード (通常 <literal>eth0</literal> となる) と、
100Mbps の Tulip カードです。
100Mbps カードを使用して TFTP でイメージをロードすると、
問題が発生するかもしれません。
そのため、10Mbps のインターフェース 
(<literal>10 Base-T</literal> と書かれている方) の使用をお奨めします。

</para>
<note><para>

<!--
You need NeTTrom 2.2.1 or later to boot the installation system, and
version 2.3.3 is recommended.  Unfortunately, firmware files are currently
not available for download because of license issues.  If this situation
changes, you may find new images at <ulink url="http//www.netwinder.org/"></ulin|ｖ|k>.
-->
インストールシステムを起動するには NeTTrom 2.2.1 以降が必要です。
NeTTrom 2.3.3 をお勧めします。
不幸にもライセンス上の理由により、
ファームウェアファイルは現在ダウンロードできません。
この状況を回避するには <ulink url="http//www.netwinder.org/"></ulink> で新しいイメージを探してください。

</para></note>
<para>

<!--
When you boot your Netwinder you have to interrupt the boot process during the
countdown.  This allows you to set a number of firmware settings needed in
order to boot the installer.  First of all, start by loading the default
settings:
-->
Netwinder を起動する際、
カウントダウン中に起動プロセスに割り込みをかけなければなりません。
これにより、インストーラを起動するのに必要な、多くの設定を行うことができます。
まず、デフォルト設定を読んで起動します。

<informalexample><screen>
    NeTTrom command-&gt; load-defaults
</screen></informalexample>

<!--
Furthermore, you must configure the network, either with a static address:
-->
その上で、ネットワークの設定を行わなければなりません。
静的アドレスにするのに、以下の両方を行ってください。

<informalexample><screen>
    NeTTrom command-&gt; setenv netconfig_eth0 flash
    NeTTrom command-&gt; setenv eth0_ip 192.168.0.10/24
</screen></informalexample>

<!--
where 24 is the number of set bits in the netmask, or a dynamic address:
-->
ここで、24 はネットマスクで立っているビットの数です。
動的アドレスの場合は、以下のようにしてください。

<informalexample><screen>
    NeTTrom command-&gt; boot diskless
    NeTTrom command-&gt; setenv netconfig_eth0 dhcp
</screen></informalexample>

<!--
You may also need to configure the <userinput>route1</userinput>
settings if the TFTP server is not on the local subnet.
-->
とします。TFTP サーバがローカルなサブネットにない場合は、
<userinput>route1</userinput> の設定も必要です。

<!--
Following these settings, you have to specify the TFTP server and the
location of the image.  You can then store your settings to flash.
-->
以下の設定で、TFTP サーバとイメージの場所を指定しなければなりません。
次いで、設定をフラッシュメモリに格納できます。

<informalexample><screen>
    NeTTrom command-&gt; setenv kerntftpserver 192.168.0.1
    NeTTrom command-&gt; setenv kerntftpfile boot.img
    NeTTrom command-&gt; save-all
</screen></informalexample>

<!--
Now you have to tell the firmware that the TFTP image should be booted:
-->
今度は以下のように、
TFTP イメージを起動させるファームウェアを指定する必要があります。

<informalexample><screen>
    NeTTrom command-&gt; setenv kernconfig tftp
    NeTTrom command-&gt; setenv rootdev /dev/ram
</screen></informalexample>

<!--
If you use a serial console to install your Netwinder, you need to add the
following setting:
-->
Netwinder にインストールするのに、シリアルコンソールを使用する場合は、
以下の設定も必要です。

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram console=ttyS0,115200
</screen></informalexample>

<!--
Alternatively, for installations using a keyboard and monitor you have to
set:
-->
その他にはインストールするのに、キーボードとモニタを使用して、
以下のように設定してください。

<informalexample><screen>
    NeTTrom command-&gt; setenv cmdappend root=/dev/ram
</screen></informalexample>

<!--
You can use the <command>printenv</command> command to review your
environment settings.  After you have verified that the settings are
correct, you can load the image:
-->
行った設定を見直すには、<command>printenv</command> コマンドを使います。
設定が正しいことを確認したら、以下のようにイメージをロードしてください。

<informalexample><screen>
    NeTTrom command-&gt; boot
</screen></informalexample>

<!--
In case you run into any problems, a <ulink
url="http://www.netwinder.org/howto/Firmware-HOWTO.html">detailed
HOWTO</ulink> is available.
-->
問題に遭遇した場合は、<ulink
url="http://www.netwinder.org/howto/Firmware-HOWTO.html">detailed
HOWTO</ulink> が利用できます。

</para>
   </sect3>

   <sect3 arch="arm"><title>CATS での TFTP からの起動</title>
<para>

<!--
On CATS machines, use <command>boot de0:</command> or similar at the
Cyclone prompt.
-->
CATS マシンでは、Cyclone プロンプトで 
<command>boot de0:</command> のように入力してください。

</para>
   </sect3>
  </sect2>


<!--
  <sect2 arch="arm"><title>Booting from CD-ROM</title>
-->
  <sect2 arch="arm"><title>CD-ROM からの起動</title>

&boot-installer-intro-cd.xml;

<para>

<!--
To boot a CD-ROM from the Cyclone console prompt, use the command
<command>boot cd0:cats.bin</command>
-->
Cyclone コンソールプロンプトから CD-ROM を起動するには、
<command>boot cd0:cats.bin</command> というコマンドを使ってください。

</para>
  </sect2>


<!--
  <sect2 arch="arm" id="boot-firmware"><title>Booting from Firmware</title>
-->
  <sect2 arch="arm" id="boot-firmware"><title>ファームウェアからの起動</title>

&boot-installer-intro-firmware.xml;

<!--
   <sect3 arch="arm" id="boot-firmware-nslu2"><title>Booting the NSLU2</title>
-->
   <sect3 arch="arm" id="boot-firmware-nslu2"><title>NSLU2 の起動</title>
<para>

<!--
There are three ways how to put the installer firmware into flash:
-->
インストーラファームウェアをフラッシュメモリに置くのに、
3 通りの方法があります。

</para>

<!--
    <sect4 arch="arm"><title>Using the NSLU2 web interface</title>
-->
    <sect4 arch="arm"><title>NSLU2 ウェブインターフェースの利用</title>
<para>

<!--
Go to the administration section and choose the menu item
<literal>Upgrade</literal>.  You can then browse your disk for the
installer image you have previously downloaded.  Then press the
<literal>Start Upgrade</literal> button, confirm, wait for a few minutes
and confirm again.  The system will then boot straight into the installer.
-->
administration セクションへ行き、
<literal>Upgrade</literal> メニュー項目を選択してください。それから、
以前ダウンロードしたインストーライメージを探してディスクを閲覧できます。
<literal>Start Upgrade</literal> ボタンを押したら、確認後、
しばらく置いてもう一度確認してください。
するとシステムがインストーラへと起動します。

</para>
    </sect4>

<!--
    <sect4 arch="arm"><title>Via the network using Linux/Unix</title>
-->
    <sect4 arch="arm"><title>Linux/Unix を利用したネットワーク経由</title>
<para>

<!--
You can use <command>upslug2</command> from any Linux or Unix machine to
upgrade the machine via the network.  This software is packaged for
Debian.
-->
Linux や Unix マシンからネットワーク経由でマシンをアップグレードするのに
<command>upslug2</command> を利用できます。
このソフトウェアは Debian でパッケージ化されています。

<!--
First, you have to put your NSLU2 in upgrade mode:
-->
まず、NSLU2 をアップグレードモードにする必要があります。

<orderedlist>
<listitem><para>

<!--
Disconnect any disks and/or devices from the USB ports.
-->
USB ポートから、ディスクやデバイスを外してください。

</para></listitem>
<listitem><para>

<!--
Power off the NSLU2
-->
NSLU2 の電源を切ってください。

</para></listitem>
<listitem><para>

<!--
Press and hold the reset button (accessible through the small hole on the
back just above the power input).
-->
リセットボタンを押したままにしてください。
(電源入力のすぐ上の小さな穴です)

</para></listitem>
<listitem><para>

<!--
Press and release the power button to power on the NSLU2.
-->
電源ボタンを押して放し、NSLU2 の電源を入れてください。

</para></listitem>
<listitem><para>

<!--
Wait for 10 seconds watching the ready/status LED. After 10 seconds it
will change from amber to red. Immediately release the reset button.
-->
ready/status LED を監視しながら 10 秒待ってください。
10 秒後、オレンジから赤に変わります。すぐにリセットボタンを放してください。

</para></listitem>
<listitem><para>

<!--
The NSLU2 ready/status LED will flash alternately red/green (there is a 1
second delay before the first green). The NSLU2 is now in upgrade mode.
-->
NSLU2 の ready/status LED が、
赤と緑で交互に点滅 (最初に緑になる前に 1 秒待つ) します。
これで NSLU2 はアップグレードモードになります。

</para></listitem>
</orderedlist>

<!--
See the <ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">NSLU2-Linux
pages</ulink> if you have problems with this.
-->
これについて問題が発生したら、<ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">NSLU2-Linux
pages</ulink> をご覧ください。

<!--
Once your NSLU2 is in upgrade mode, you can flash the new image:
-->
一度 NSLU2 がアップグレードモードになれば、
以下のように新しいイメージをフラッシュメモリに書き込めます。

<informalexample><screen>
sudo upslug2 -i di-nslu2.bin
</screen></informalexample>

<!--
Note that the tool also shows the MAC address of your NSLU2, which may come
in handy to configure your DHCP server.  After the whole image has been
written and verified, the system will automatically reboot.  Make sure you
connect your USB disk again now, otherwise the installer won't be able to
find it.
-->
このツールは NSLU2 の MAC アドレスも表示するのに注意してください。
DHCP サーバの設定を行うのに便利でしょう。
イメージをすべて書き込んで、確認が終わると自動的に再起動します。
USB ディスクの接続を再度確認してください。
そうでないとインストーラが見つけられません。

</para>
    </sect4>

<!--
    <sect4 arch="arm"><title>Via the network using Windows</title>
-->
    <sect4 arch="arm"><title>Windows を利用したネットワーク経由</title>
<para>

<!--
There is <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">a
tool</ulink> for Windows to upgrade the firmware via the network.
-->
ネットワーク経由でファームウェアのアップグレードする Windows 用 <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">ツール</ulink> があります。

</para>
    </sect4>
   </sect3>
  </sect2>
