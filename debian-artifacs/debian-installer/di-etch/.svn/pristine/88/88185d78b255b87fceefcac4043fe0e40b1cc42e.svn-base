aboot-installer (0.0.25) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A
  * Dutch (nl.po) by Bart Cornelis

 -- Steve Langasek <vorlon@debian.org>  Fri,  2 Mar 2007 16:06:53 -0800

aboot-installer (0.0.24) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Hebrew (he.po) by Lior Kaplan
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Steve Langasek <vorlon@debian.org>  Wed, 31 Jan 2007 10:06:16 -0800

aboot-installer (0.0.23) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Gujarati (gu.po) by Kartik Mistry
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Japanese (ja.po) by Kenshi Muto
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Panjabi (pa.po) by A S Alam
  * Romanian (ro.po) by Eddy Petrișor
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Steve Langasek <vorlon@debian.org>  Tue, 24 Oct 2006 16:43:36 -0700

aboot-installer (0.0.22) unstable; urgency=low

  [ Joey Hess ]
  * Update bsd partition table code to work with parted 1.7.1.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Estonian (et.po) by Siim Põder
  * Georgian (ka.po) by Aiet Kolkhi
  * Lithuanian (lt.po) by Kęstutis Biliūnas

 -- Steve Langasek <vorlon@debian.org>  Sun,  2 Jul 2006 10:53:55 -0700

aboot-installer (0.0.21) unstable; urgency=low

  [ Christian Perrier ]
  * Change Build-Depends-Indep: debhelper to Build-Depends:, since it's used
    in the clean rule (closes: #367194).

 -- Steve Langasek <vorlon@debian.org>  Mon, 22 May 2006 17:26:07 -0700

aboot-installer (0.0.20) unstable; urgency=low

  [ Matt Kraai ]
  * Fix the spelling of "file system" and "boot loader".

  [ Joey Hess ]
  * Remove unused $log variable.

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Kazakh (kk.po) by Talgat Daniyarov
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Punjabi (Gurmukhi) (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Xhosa (xh.po) by Canonical Ltd
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Steve Langasek <vorlon@debian.org>  Wed, 25 Jan 2006 23:30:34 -0800

aboot-installer (0.0.19) unstable; urgency=low

  * Note: includes variable substitution fix(es) in translations.
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Filippo Giunchedi
    - Japanese (ja.po) by Kenshi Muto
    - Norwegian (nb.po) by Hans Fredrik Nordhaug
    - Dutch (nl.po) by Bart Cornelis
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Albanian (sq.po) by Elian Myftiu

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 21:41:10 -0500

aboot-installer (0.0.18) unstable; urgency=low

  * Updated translations: 
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Debian Indonesia Team
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Steve Langasek <vorlon@debian.org>  Wed,  6 Oct 2004 16:26:45 -0700

aboot-installer (0.0.17) unstable; urgency=low

  * Update the partman "how to use this partition" string for aboot, to
    make it match the current UI standard.

 -- Steve Langasek <vorlon@debian.org>  Wed, 29 Sep 2004 23:55:01 -0700

aboot-installer (0.0.16) unstable; urgency=low

  * Fix default value for boolean debconf template.
  * Add support for RAID devices (closes: #265597).
  * Updated translations: 
    - Danish (da.po) by Claus Hindsgaul
    - French (fr.po) by Christian Perrier
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Turkish (tr.po) by Recai Oktaş

 -- Steve Langasek <vorlon@debian.org>  Fri, 20 Aug 2004 02:04:26 -0700

aboot-installer (0.0.15) unstable; urgency=low

  * Remove debian/isinstallable, which *never* returns true now that
    debootstrap doesn't install aboot for us.
  * Updated translations: 
    - Polish (pl.po) by Bartosz Fenski

 -- Steve Langasek <vorlon@debian.org>  Sat, 31 Jul 2004 00:45:12 -0700

aboot-installer (0.0.14) unstable; urgency=low

  * rename debian/templates to debian/aboot-installer.templates
  * Work-around for goofy busybox bug: when dealing with large disks,
    [ bignum -le 1024 ] returns true, but [ $((bignum/1024)) -le 1 ]
    returns false.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerovic
    - Norwegian Bokmål (nb.po) by Bjørn Steensrud

 -- Steve Langasek <vorlon@debian.org>  Tue, 27 Jul 2004 21:19:03 -0700

aboot-installer (0.0.13) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by George Papamichelakis
    - Finnish (fi.po) by Tapio Lehtonen
    - Croatian (hr.po) by Krunoslav Gernhard
    - Italian (it.po) by Filippo Giunchedi
    - Korean (ko.po) by Jung Seung-Cheol
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lope
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Steve Langasek <vorlon@debian.org>  Sun, 25 Jul 2004 20:09:48 -0700

aboot-installer (0.0.12) unstable; urgency=low

  * Add "aboot" as an option for partition usage.
  * Recognize pre-existing partitions that are candidates for aboot
    usage.
  * Let swriteboot know about partitions it's allowed to overlap with
  * Lower the priority of the question about the boot partition to use
    unless we can't guess a sane default
  * When /boot is a separate partition, install our own symlinks there
    for vmlinuz and initrd
  * Updated translations: 
    - Albanian (sq.po) by Elian Myftiu
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - French (fr.po) by Christian Perrier
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lope
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Steve Langasek <vorlon@debian.org>  Mon, 19 Jul 2004 01:25:05 -0700

aboot-installer (0.0.11) unstable; urgency=low

  * Joshua Kwan
    - Build in the binary-arch target, not binary-indep. Thanks
      Santiago Vila <sanvila@unex.es>. (Closes: #251969)
  * Steve Langasek
    - Actually give users the option to say "yes" to buggy combinations,
      instead of forcing them back into the partitioner
    - Fix partition table detection, so we only complain about a lack of
      BSD disklabels when we don't have them

 -- Steve Langasek <vorlon@debian.org>  Sun, 20 Jun 2004 23:01:41 -0500

aboot-installer (0.0.10) unstable; urgency=low

  * Add missing quotes needed to properly detect fs types;
    thanks to Martin Michlmayr.
  * Depend on parted-udeb, so we can really check the disklabels.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Gallegan (gl.po) by Héctor Fernández López
    - Italian (it.po) by Filippo Giunchedi
    - Slovenian (sl.po) by Jure Čuhalev
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Steve Langasek <vorlon@debian.org>  Fri, 28 May 2004 14:59:56 -0500

aboot-installer (0.0.9) unstable; urgency=low

  * Actually install our partman hook, so that it's actually useful for
    ensuring filesystem types (closes: #246900).
  * Updated translations: 
    - Korean (ko.po) by Changwoo Ryu
    - Norwegian (nn.po) by Håvard Korsvoll
    - Swedish (sv.po) by André Dahlqvist

 -- Steve Langasek <vorlon@debian.org>  Tue, 11 May 2004 13:41:23 -0500

aboot-installer (0.0.8) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Albanian (sq.po) by Elian Myftiu

 -- Steve Langasek <vorlon@debian.org>  Fri, 23 Apr 2004 15:08:34 -0500

aboot-installer (0.0.7) unstable; urgency=low

  * Fix template name for one of the partman questions
  * Re-add support for installing aboot from d-i, so it can be removed
    from debootstrap post-beta4; requires adding a new debconf template
    as well -- translations pulled in from yaboot.
  * Updated translations: 
    - Danish (da.po) by Claus Hindsgaul
    - Basque (eu.po) by Piarres Beobide
    - Finnish (fi.po) by Tapio Lehtonen
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Steve Langasek <vorlon@debian.org>  Sun, 19 Apr 2004 13:35:19 -0500

aboot-installer (0.0.6) unstable; urgency=low

  * Joshua Kwan
    - Use debhelper's new udeb support.
  * Colin Watson
    - Fix missing quote in bootdev != realbootdev case.
  * Steve Langasek
    - Integration with partman to ensure we get a valid ext2 /boot on a
      disklabel-using disk.
  * Updated translations: 
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Filippo Giunchedi
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Norwegian (nb.po) by Axel Bojer
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Marek Laska,,,
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lope
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by André Dahlqvist
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Steve Langasek <vorlon@debian.org>  Wed, 14 Apr 2004 23:03:10 -0500

aboot-installer (0.0.5) unstable; urgency=low

  * DO NOT update this file, translators. This package switch
    to the automated changelog-changes script. I repeat : DO NOT
    add yourself manually below. The script should be run just before
    releasing the package. See scripts/changelog-changes.README.
  * Updated translations: 
   - Bulgarian (bg.po) by Ognyan Kulev
   - Bosnian (bs.po) by Safir Šećerović
   - Catalan (ca.po) by Jordi Mallach
   - Czech (cs.po) by Miroslav Kure
   - Finnish (fi.po) by Tapio Lehtonen
   - Korean (ko.po) by Changwoo Ryu
   - Lithuanian (lt.po) by Kęstutis Biliūnas
   - Polish (pl.po) by Marek Laska,,,
   - Portuguese (pt.po) by Miguel Figueiredo
   - Romanian (ro.po) by Eddy Petrisor
   - Russian (ru.po) by Nikolai Prokoschenko
   - Slovak (sk.po) by Peter KLFMANiK Mann
   - Slovenian (sl.po) by Jure Čuhalev
   - Albanian (sq.po) by Elian Myftiu
   - Swedish (sv.po) by André Dahlqvist
   - Turkish (tr.po) by Osman Yüksel
   - Ukrainian (uk.po) by Eugeniy Meshcheryakov
   - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
   - zh_TW (zh_TW.po) by Tetralet
   - Bokmål (nb.po) by Axel Bojer. (Checked in by Håvard Korsvoll)
  * Joey Hess
    - Add a dependency on di-utils-mapdevfs.
  * Christian Perrier
    - Switch to changelogs-changes script for translations

 -- Steve Langasek <vorlon@debian.org>  Sat, 14 Feb 2004 18:40:55 +0200

aboot-installer (0.0.4) unstable; urgency=low

  * Konstantinos Margaritis
    - Updated Greek translation.
  * Dennis Stampfer
    - Update German translation de.po
  * Claus Hindsgaul
    - Update da (Danish) translation.
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * Steve Langasek
    - Fix the package priority to standard, since it's needed by default
      on this architecture.
    - Set a proper root device in /etc/aboot.conf, instead of "/dev/ram".
      Should now give us a genuinely bootable system (usually).

 -- Steve Langasek <vorlon@debian.org>  Thu, 15 Jan 2004 23:14:00 -0600

aboot-installer (0.0.3) unstable; urgency=low

  * Miguel Figueiredo
    - Updated Portuguese translation (pt.po)
  * Ming Hua
    - Initial Simplified Chinese translation (zh_CN.po)
  * Bartosz Fenski
    - Update Polish (pl) translation.
  * Claus Hindsgaul
    - Update da (Danish) translation.
  * Kenshi Muto
    - Added Japanese Translation (ja.po)
    - Update ja.po
  * Jordi Mallach
    - Added Catalan (ca) translation.
  * Miroslav Kure
    - Initial Czech translation.
  * Christian Perrier
    - Improve French translation.
  * Jure Cuhalev
    - Initial Slovenian translation (sl.po).  Closes: #215421
  * Safir Secerovic, Amila Akagic
    - Add translation to Bosnian (bs.po).
  * Peter Mann
    - Initial Slovak translation (sk.po).
  * Petter Reinholdtsen
    - Make menu item translatable.
  * André Luís Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
    - Fixed the translation of "Device for boot loader installation:" to
       be consistent between all the udebs it is being used within
       debian-installer.
  * Pierre Machard
    - Update French translation.
    - Run debconf-updatepo
  * Bart Cornelis
    - Update Dutch translation (nl.po)
    - incorporated debian-l10n-dutch review comments into nl.po
    - Merged Norwegian Bokmael (nb.po) from skolelinux-cvs
    - Merged Norwegian Nynorsk (nn.po) from skolelinux-cvs
  * Ilgiz Kalmetev
    - Initial Russian translation.
  * Konstantinos Margaritis
    - Initial Greek translation (el.po)
  * Steinar H. Gunderson
    - Template polishing from Christian Perrier. (Closes: #218208)
  * Verok Istvan
    - Initial Hungarian translation.
  * David Martínez Moreno
    - Updated Spanish translation (es.po) and converted to UTF-8.
  * Dennis Stampfer
    - Update German translation (de.po)
    - Merged some strings in German translation de.po
  * Konstantinos Margaritis
    - Updated Greek translation (el.po)
  * Giuseppe Sacco
    - first italian translation by Filippo Giunchedi
    - New update from Stefano Melchior
  * Steinar H. Gunderson
    - Updated Norwegian translation (nb.po).
  *Kęstutis Biliūnas
    - Added Lithuanian translation (lt.po).
  * Ognyan Kulev
    - Added/updated bulgarian translation (bg.po).
  * Marco Ferra
    - Added portuguese translation (pt.po).
  * Jure Cuhalev
    - Added/updated slovenian translation (sl.po).
  * André Dahlqvist
    - Added Swedish translation
  * Teófilo Ruiz Suárez
    - Fixed some inconsistencies in Spanish Translation (es.po)
  * Anmar Oueja
    - created and translated to Arabic (ar.po)
  * Nikolai Prokoschenko
    - Upated russian translation (ru.po)
  * Steve Langasek
    - Add myself to Uploaders.
    - Complete redesign (again based on grub-installer) to correctly
      detect the set of viable boot device options (ext2 only) and
      provide more automatic configuration.
    - Partially updated translations for Spanish (es.po),
      French (fr.po), Italian (it.po), Norwegian Bokmal (nb.po),
      Portuguese (pt.po), and Brazilian Portuguese (pt_BR.po).
    - Proposed (fuzzy) translation updates for Arabic (ar.po),
      Bulgarian (bg.po), Bosnian (bs.po), Czech (cs.po), Danish (da.po),
      German (de.po), Greek (el.po), Finnish (fi.po), Hungarian (hu.po),
      Japanese (ja.po), Lithuanian (lt.po), Dutch (nl.po), Norwegian
      Nynorsk (nn.po), Polish (pl.po), Slovak (sk.po),
      Slovenian (sl.po), Swedish (sv.po), Turkish (tr.po), and
      Simplified Chinese (zh_CN.po), based on existing translations
      in the grub-installer package.
   * Konstantinos Margaritis
     - Updated the Greek translation for the new design.
   * Christian Perrier
     - Added ellipses to progress texts. Sorry, cannot unfuzzy
       translations, so again work for translators.
     - French translation update

 -- Steve Langasek <vorlon@debian.org>  Thu, 15 Jan 2004 8:39:00 -0600

aboot-installer (0.0.2) unstable; urgency=low

  * Martin Sjögren
    - Replace XBC with XB so our special control fields don't confuse the
      changes files.
    - Add sv.po, thanks to David Weinehall.
  * Tollef Fog Heen
    - Add de.po, thanks to Helge Kreutzmann (closes: #173016)
  * Petter Reinholdtsen
    - Change maintainer to debian-boot@lists.debian.org, and make the old
      maintainer an uploader
    - Added Norwegian Bokmål (nb.po) translations recieved from
      Bjørn Steensrud.
    - Added Norwegian Nynorsk (nn.po) translations recieved from
      Gaute Hvoslef Kvalnes.
  * Thorsten Sauter
    - Update de.po
  * André Luís Lopes :
    - Improve pt_BR translation a bit.
  * Matt Kraai
    - Add nl.po, thanks to Bart Cornelis.
  * Alastair McKinstry
    - Convert changelog to UTF-8 for Standards-Version: 3.6.0

 -- Alastair McKinstry <mckinstry@computer.org>  Sun, 20 Jul 2003 10:18:01 +0100

aboot-installer (0.0.1) unstable; urgency=low

  * First release, I'm not even sure it works :)
  * THIS IS PRETTY UNTESTED!!!!!!!!!!!!
    DON'T use it unless you are interested in helping me debug.
    Thank you for your consideration.
  * Add Brazilian Portuguese debconf template.
  * Add Danish debconf template.
  * Convert to po-debconf, set Build-Depends-Indep: debhelper (>= 4.1.16)
    to ensure that generated templates are right, and set output encoding
    to UTF-8.
  * André Luís Lopes
    - Define pt_BR.po's control fields.
    - Rearrange translation layout a bit.

 -- Wartan Hachaturow <wart@debian.org>  Tue, 20 Aug 2002 23:27:19 +0400
