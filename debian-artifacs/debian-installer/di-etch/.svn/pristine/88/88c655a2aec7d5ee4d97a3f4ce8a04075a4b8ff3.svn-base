<!-- retain these comments for translator revision tracking -->
<!-- original version: 43576 -->


  <sect2 arch="m68k"><title>Partitionieren unter AmigaOS</title>
<para>

Wenn Sie AmigaOS benutzen, können Sie das Programm
<command>HDToolBox</command> benutzen, um Ihre ursprüngliche
Partitionierung vor der Installation anzupassen.

</para>
  </sect2>

  <sect2 arch="m68k"><title>Partitionieren unter Atari TOS</title>
<para>

Atari Partitionskennungen bestehen aus drei ASCII-Zeichen, dabei steht
<quote>LNX</quote> für Daten- und <quote>SWP</quote> für Swap-Partitionen. Wenn Sie die
Installationsmethode für wenig Arbeitsspeicher benutzen, benötigen Sie außerdem
noch eine kleine Minix-Partition (etwa 2MB), deren Partitionskennung
<quote>MNX</quote> ist. Fehler beim Setzen der korrekten Partitionskennung
verhindern nicht nur das Erkennen der Partitionen während der
Debian-Installation, sondern führen auch dazu, dass TOS versucht, die
Linux-Partitionen zu benutzen, was den Festplatten-Treiber verwirrt und
den Zugriff auf die gesamte Festplatte unmöglich macht.

</para><para>

Es gibt eine Vielzahl von Partitionierungswerkzeugen (Ataris
Hilfsprogramm <command>harddisk</command> erlaubt es nicht, die
Partitionskennung zu ändern). Diese Anleitung kann keine detaillierte
Anleitung von allen geben. Die folgende Anleitung beschreibt
<command>SCSITool</command> (von Hard+Soft GmbH).

<orderedlist>
<listitem><para>

Starten Sie <command>SCSITool</command> und wählen Sie die Festplatte
aus, die Sie partitionieren möchten (Menü <guimenu>Disk</guimenu>,
Eintrag <guimenuitem>select</guimenuitem>).

</para></listitem>
<listitem><para>

Wählen Sie im <guimenu>Partition</guimenu>-Menü entweder
<guimenuitem>New</guimenuitem>, um eine neue Partition zu erstellen oder
Partitionsgrößen bestehender Partitionen zu ändern, oder
<guimenuitem>Change</guimenuitem> um eine bestimmte Partition zu verändern.
<guimenuitem>New</guimenuitem> ist wahrscheinlich die beste Wahl, es sei denn
Sie haben bereits Partitionen mit den passenden Größen erstellt
und wollen lediglich eine Partitionskennung ändern.


</para></listitem>
<listitem><para>

Nach dem Menü-Eintrag <guimenuitem>New</guimenuitem> wählen Sie
in der Dialog-Box für die Starteinstellungen
<guilabel>existing</guilabel>. Das nächste Fenster zeigt eine Liste der
existierenden Partitionen, die Sie mit den Scroll-Knöpfen oder durch
Klicken in den Laufbalken ausrichten können. Die erste Spalte zeigt die
Partitionskennung. Klicken Sie einfach auf das Textfeld, um Sie zu
ändern. Wenn Sie alle Änderungen an den Partitionen vorgenommen haben,
speichern Sie Ihre Änderungen, indem Sie das Fenster mit dem
<guibutton>OK</guibutton>-Knopf verlassen.

</para></listitem>
<listitem><para>

Für die <guimenuitem>Change</guimenuitem>-Option wählen Sie aus der
Liste die Partition und in der Dialog-Box <guilabel>other
systems</guilabel>. Das nächste Fenster zeigt Ihnen detaillierte
Informationen über die Position dieser Partition und gibt Ihnen die Möglichkeit, die
Partitionskennung zu ändern. Speichern Sie Ihre Änderungen, indem Sie das
Fenster mit dem <guibutton>OK</guibutton>-Knopf verlassen.

</para></listitem>
<listitem><para>

Notieren Sie sich die Linux-Namen jeder Partition, die Sie zum Benutzen
unter Linux erstellt oder verändert haben &ndash; siehe hierzu auch <xref
linkend="device-names"/>.

</para></listitem>
<listitem><para>

Beenden Sie <command>SCSITool</command> mit dem Eintrag
<guimenuitem>Quit</guimenuitem> des <guimenu>File</guimenu>-Menüs. Ihr
Computer wird neu starten, um sicher zu gehen, dass die geänderte
Partitionstabelle von TOS benutzt wird. Wenn Sie irgendeine TOS/GEM-Partition
geändert haben, wird Sie ungültig und muss reinitialisiert
werden (wir sagten Ihnen, dass Sie eine Sicherheitskopie Ihrer
Festplatten erstellen sollen, oder?).

</para></listitem>
</orderedlist>

</para><para>

Im Installationssystem gibt es ein Partitionierungswerkzeug für
Linux/m68k namens <command>atari-fdisk</command>, aber derzeit empfehlen
wir Ihnen, Ihre Festplatten mit einem TOS-Partitions-Editor oder einem
anderen Festplatten-Programm zu partitionieren. Wenn Ihr
Partitionierungswerkzeug keine Option zum Ändern der Partitionskennung hat,
können Sie diesen kritischen Schritt auch in einem späteren Stadium (von
der gebooteten, temporären Installations-RAM-Disk) durchführen.
<command>SCSITool</command> ist lediglich ein Partitionierungswerkzeug, von
dem wir wissen, dass es die Auswahl beliebiger Partitionskennungen
erlaubt. Es gibt vermutlich noch andere. Benutzen Sie einfach das
Werkzeug, das Ihnen am besten gefällt.

</para>
</sect2>

  <sect2 arch="m68k"><title>Partitionieren unter MacOS</title>
<para>

Einige der getesteten Partitionierungswerkzeuge sind
<command>pdisk</command>, <command>HD SC Setup</command> 7.3.5 (Apple),
<command>HDT</command> 1.8 (FWB), <command>SilverLining</command>
(LaCie) und <command>DiskTool</command> (Tim Endres, GPL). Für
<command>HDT</command> und <command>SilverLining</command> benötigen Sie
Vollversionen. Das Programm von Apple benötigt einen Patch, um
fremde Festplatten zu erkennen (eine Beschreibung, wie man <command>HD SC
Setup</command> mit <command>ResEdit</command> patcht, finden Sie unter
<ulink url="http://www.euronet.nl/users/ernstoud/patch.html"></ulink>).

</para><para>

Für IDE-basierte Macs müssen Sie das Programm <command>Apple Drive
Setup</command> benutzen, um freien Platz für die Linux-Partitionen zu
schaffen; dann vollenden Sie die Partitionierung unter Linux oder Sie verwenden
die MacOS-Version von pdisk, welche bei
<ulink url="http://homepage.mac.com/alk/downloads/pdisk.sit.hqx">Alsoft</ulink>
zum Download bereitsteht.

</para>
</sect2>


