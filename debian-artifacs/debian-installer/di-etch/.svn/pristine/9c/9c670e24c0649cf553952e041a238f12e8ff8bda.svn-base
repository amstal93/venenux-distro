<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 45453 -->

 <sect1 condition="supports-floppy-boot" id="create-floppy">
 <title>Comment cr�er des disquettes depuis des images disque ?</title>
<para>

Les disquettes d'amor�age sont la plupart du temps utilis�es en dernier 
recours pour lancer l'installateur sur des machines qui ne peuvent �tre 
amorc�es � partir d'un c�d�rom ou d'un autre moyen.

</para><para arch="powerpc">

L'amor�age de l'installateur par disquette ne fonctionne pas avec
les lecteurs de disquettes USB.


</para><para arch="m68k">

L'amor�age par disquette n'est pas possible sur les Amiga et les Mac 68k.

</para><para>

Les images disque sont des fichiers contenant l'ensemble du
contenu d'une disquette en mode <emphasis>raw</emphasis> (copie brute des
secteurs physiques). Les images disque, 
comme <filename>rescue.bin</filename>, ne peuvent pas �tre simplement copi�es 
sur une disquette. Un programme sp�cial est utilis� pour copier les fichiers 
d'images sur des disquettes en mode brut <emphasis>raw</emphasis>. 
C'est obligatoire car ces images sont une repr�sentation brute du 
disque&nbsp;; il faut donc recopier les donn�es par 
<emphasis>copie des secteurs</emphasis> du fichier vers la disquette.

</para><para>

Il existe diff�rentes m�thodes pour cr�er des disquettes � partir
d'images disque. Cette partie d�crit comment le faire sur les diff�rentes plateformes.


</para>
<para>

Avant de cr�er les disquettes, vous devez d'abord t�l�charger les images
sur l'un des miroirs Debian, comme expliqu� dans <xref linkend="downloading-files"/>.
<phrase arch="i386"> Si vous poss�dez un CD ou un DVD d'installation, les images sont
incluses sur le CD/DVD./</phrase>
</para>
<para>

Quelle que soit la mani�re dont vous les obtiendrez, n'oubliez pas de 
verrouiller les disquettes cr��es de fa�on � vous assurer qu'elles ne seront 
pas endommag�es par m�garde.

</para>

  <sect2><title>�crire des images disque depuis Linux ou un syst�me Unix</title>
<para>

Pour �crire une image disque vers une disquette, vous aurez certainement 
besoin d'un acc�s avec les droits du superutilisateur. Placez une disquette 
vierge non d�fectueuse dans le lecteur. Ensuite, ex�cutez la commande 
suivante&nbsp;:

<informalexample><screen> 
$ dd if=<replaceable>fichier</replaceable> of=/dev/fd0 bs=1024 conv=sync ; sync 
</screen></informalexample>

o� <replaceable>fichier</replaceable> est un des fichiers image disque 
(voyez la <xref linkend="downloading-files"/> pour savoir quel fichier).
<filename>/dev/fd0</filename> est g�n�ralement le nom utilis� pour le lecteur 
de disquette. Cela peut �tre diff�rent sur une station de travail<phrase arch="sparc">
(sur Solaris, c'est <filename>/dev/fd/0</filename>)</phrase>. Il se peut que vous 
r�cup�riez la main avant que votre syst�me Unix n'ait fini d'�crire la 
disquette&nbsp;; aussi, assurez-vous que le signal lumineux, activ� lors du 
fonctionnement du lecteur, soit bien �teint, et que la disquette ne tourne 
plus avant de la sortir. Sur certains syst�mes, vous devez utiliser une 
commande pour �jecter la disquette du lecteur<phrase arch="sparc">Sur 
Solaris, utilisez <command>eject</command> et lisez la page de manuel 
associ�e.</phrase>.

</para><para>

Certains syst�mes tentent de monter automatiquement la disquette lorsque vous 
la placez dans le lecteur. Vous devrez d�sactiver cette option sinon la 
station de travail ne vous permettra pas d'�crire des donn�es 
<emphasis>brutes</emphasis> (�&nbsp;raw mode&nbsp;�) sur la disquette. 
Malheureusement, la fa�on de le faire varie consid�rablement suivant le syst�me
d'exploitation. <phrase arch="sparc">Sur Solaris, vous pouvez configurer le 
gestionnaire de volumes pour autoriser les acc�s directs sur les disquettes 
(utilisez <command>volcheck</command> ou une commande �quivalente dans le 
gestionnaire de fichiers). Ensuite, utilisez la commande <command>dd</command>
avec la forme donn�e au-dessus en rempla�ant simplement 
<filename>/dev/fd0</filename> par <filename>/vol/rdsk/<replaceable>floppy_name</replaceable></filename>, o�
<replaceable>floppy_name</replaceable> est le nom donn� au lecteur de 
disquettes lors de sa cr�ation (les lecteurs non nomm�s ont un nom g�n�rique 
<filename>unnamed_floppy</filename>). Sur les autres syst�mes, il faut demander
� l'administrateur.
</phrase>

</para><para arch="powerpc"> 

Si vous �crivez une disquette pour un PowerPC, vous
devrez pouvoir l'�jecter. Le programme <command>eject</command> s'en chargera
tr�s bien&nbsp;; vous devrez l'installer.

</para>

  </sect2>

&floppy-i386.xml; <!-- can be used for other arches -->
&floppy-m68k.xml;
&floppy-powerpc.xml;

 </sect1>
