<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 43939 -->

 <sect1 id="supported-peripherals">
<title>P�riph�riques et autres mat�riels</title>

<para arch="not-s390">
Linux reconna�t une large gamme de p�riph�riques comme les souris, les 
imprimantes, les scanners, les modems, les cartes r�seau, les p�riph�riques 
PCMCIA et USB, etc. Cependant aucun de ces p�riph�riques n'est requis lors de
l'installation du syst�me.
</para>

<para arch="x86">
Le mat�riel USB fonctionne correctement mais certains
claviers USB peuvent n�cessiter une configuration suppl�mentaire 
(voyez la see <xref linkend="hardware-issues"/>).
</para>

<para arch="x86">
Encore une fois, reportez-vous au 
<ulink url="&url-hardware-howto;">HOWTO sur la compatibilit� des mat�riels avec Linux</ulink> 
pour d�terminer si votre mat�riel est reconnu par Linux.
</para>

<para arch="s390">
Les installations de paquets � partir de XPRAM ou d'une bande ne sont pas 
reconnues par le syst�me. Tous les paquets que vous d�sirez installer doivent 
�tre disponibles sur un DASD ou bien sur le r�seau via NFS, HTTP ou FTP.
</para>

<para arch="mips">
La carte d'�valuation Broadcom BCM91250A fournit des emplacements PCI 3,3v 
32 et 64 bits ainsi que des connecteurs USB. La carte BCM91480B propose quatre
emplacements Pci 64 bits.

</para><para arch="mipsel">

La carte d'�valuation Broadcom BCM91250A fournit des emplacements PCI 3,3v 
32 et 64 bits ainsi que des connecteurs USB. La carte BCM91480B
propose quatre emplacements Pci 64 bits. Le Cobalt RaQ ne permet pas
l'ajout de p�riph�rique mais le Qube a un emplacement PCI libre.

</para>

 </sect1>

 <sect1 arch="not-s390">
<title>Acheter du mat�riel sp�cialement pour GNU/Linux</title>

<para>
Il existe des vendeurs qui livrent des syst�mes en 
<ulink url="&url-pre-installed;">pr�installant</ulink> Debian ou d'autres 
distributions de GNU/Linux. Vous paierez 
peut-�tre plus cher pour avoir ce privil�ge, mais vous achetez ainsi un peu 
de tranquillit� d'esprit, puisque vous serez certain que le mat�riel est bien 
reconnu par GNU/Linux.
</para>

<para arch="m68k">
Malheureusement, il est assez rare de trouver un vendeur qui livre des 
machines &arch-title; neuves.
</para>

<para arch="x86">  
Si vous devez acheter un ordinateur fourni avec Windows, lisez attentivement 
la licence logicielle accompagnant Windows&nbsp;; vous pourrez peut-�tre
rejeter la licence et obtenir un rabais de votre vendeur. Une recherche sur internet
avec la cha�ne <quote>rabais sur Windows</quote> peut vous donner des pistes.
</para>

<para>
Que vous achetiez ou non un syst�me livr� avec Linux, ou m�me un syst�me 
d'occasion, il est important de v�rifier que votre mat�riel est reconnu par 
le noyau Linux. V�rifiez si votre mat�riel est list� dans les r�f�rences
ci-dessus. Indiquez � votre revendeur que vous recherchez un 
syst�me Linux. Soutenez les revendeurs de mat�riel amis de Linux.
</para>

  <sect2>
<title>�viter les mat�riels propri�taires ou ferm�s</title>

<para>
Certains constructeurs refusent simplement de fournir les informations qui 
permettraient d'�crire des pilotes pour leurs mat�riels. D'autres 
n'autorisent pas l'acc�s � la documentation sans accord de confidentialit�, 
ce qui nous emp�che de distribuer le code source pour Linux. 
    </para>

<para arch="m68k">
Un exemple est le mat�riel propri�taire dans les anciennes gammes 
Macintosh. En fait, aucune sp�cification ni documentation n'a �t� donn�e 
sur le mat�riel Macintosh, surtout en ce qui concerne le contr�leur ADB 
(utilis� par la souris et le clavier), le contr�leur de disquettes, et toute
l'acc�l�ration et la manipulation des CLUT sur le mat�riel vid�o (bien que 
la manipulation des CLUT soit maintenant possible sur la plupart des 
composants vid�o internes). Cela explique en partie pourquoi le portage de 
Linux sur Macintosh est en retard par rapport aux autres portages Linux. 
</para>

<para>
Puisque nous n'avons pas �t� autoris�s � acc�der � la documentation de ces 
p�riph�riques, ils ne fonctionneront simplement pas sous Linux. Vous pouvez 
nous aider en demandant � ces constructeurs de distribuer la documentation 
de tels mat�riels. Si suffisamment de personnes font cette demande, ils 
r�aliseront que la communaut� du logiciel libre est un march� important.
</para>

  </sect2>

  <sect2 arch="x86">
<title>Mat�riels sp�cifiques � Windows</title>

<para>
La prolif�ration de modems et d'imprimantes sp�cifiques � Windows est une 
f�cheuse tendance. Ces p�riph�riques sont sp�cialement con�us pour �tre 
directement g�r�s par le syst�me d'exploitation Windows de Microsoft et 
portent le terme <quote>WinModem</quote> ou 
<quote>fabriqu� sp�cifiquement pour les ordinateurs utilisant 
Windows</quote>. Cela est g�n�ralement obtenu en enlevant les processeurs 
internes aux p�riph�riques et en confiant le travail qu'ils accomplissaient � 
un pilote Windows qui tourne en utilisant le processeur de votre ordinateur. 
Cette strat�gie permet la fabrication d'un mat�riel � moindre co�t mais les 
utilisateurs <emphasis>ne b�n�ficient pas</emphasis> souvent de ces �conomies 
et ces mat�riels peuvent �tre parfois plus chers que des p�riph�riques
�quivalents qui gardent leur �&nbsp;intelligence&nbsp;� interne.
</para>

<para>
Vous devriez �viter les p�riph�riques sp�cifiques � Windows pour deux 
raisons. La premi�re est que les constructeurs ne rendent g�n�ralement pas 
publics les moyens n�cessaires pour �crire un pilote Linux. Le mat�riel et 
l'interface logicielle du p�riph�rique sont propri�taires et la 
documentation, quand elle existe, n'est pas disponible sans un accord de 
confidentialit�. Cela emp�che toute utilisation dans un logiciel libre, 
puisque les auteurs de logiciel libre communiquent le code source de leurs 
programmes. La seconde raison est que, lorsqu'on retire de ces p�riph�riques 
leurs processeurs internes, le syst�me d'exploitation doit accomplir
le travail de ces processeurs, souvent dans une priorit� 
<emphasis>temps r�el</emphasis>&nbsp;; ainsi le processeur n'est plus 
disponible pour faire tourner vos programmes pendant qu'il g�re ces 
p�riph�riques. Puisque l'utilisateur moyen de Windows n'utilise pas aussi 
intensivement que celui de Linux le multit�che, les constructeurs esp�rent 
que l'utilisateur de Windows ne remarquera pas la charge que fait porter ce
mat�riel sur le processeur. Mais de toute fa�on, tout syst�me d'exploitation 
multit�che, m�me Windows 95 ou NT, est affaibli lorsque les constructeurs de 
p�riph�riques l�sinent sur la puissance de calcul interne de leurs mat�riels.
</para>

<para>
Vous pouvez changer cette situation en encourageant ces constructeurs � 
publier les documentations et tout autre moyen n�cessaire � la programmation 
de leurs mat�riels. Mais la meilleure strat�gie est simplement d'�viter ce 
genre de mat�riel tant qu'il n'est pas r�pertori� dans le 
<ulink url="&url-hardware-howto;">HOWTO sur la compatibilit� des mat�riels avec Linux</ulink>.
</para>

  </sect2>


 </sect1>
