<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version:22935  -->
<!-- Piarres Beobidek egina 2004-ko Azaroaren 25-eam -->

 <sect2 id="base-config-mta">
 <title>Posta transferentzi Agentea (MTA) konfiguratu</title>

<para>

Gaur egun eposta oso garrantzitsua da jende askoren bizitzarako, beraz
ez da batere arrigarri Debian-ek eposta transferentzi agentea
instalazioaren barnean konfiguratzea. Debian sistematan lehenetsiriko
posta transferentzi agentea <command>exim4</command> da, zein 
nahiko txikia, moldagarria eta ulertzeko erraza bait da.

</para><para>

Agian ordenagailua ez dagoenez sarean posta agentea beharrezko den 
galdetuko zara. Erantzun laburra Bai da. Argibide luzeagoak: zenbait
sistema osagaiek (<command>cron</command>,
<command>quota</command>, <command>aide</command>, &hellip;) mezu
garrantzitsuak eposta bidez bidaltzen dituztela.

</para><para>

Lehenengo pantailan zenbait espota ingurune erakusten dira.
Aukeratu horietako bat zure beharren arabera:

</para>

<variablelist>
<varlistentry>
<term>internet gunea</term>
<listitem><para>

Sistema zuzenean internetera konektaturik dago eta eposta
SMTP bidez jaso eta bidaliko da. Hurrengo pantailetan zenbait
oinarrizko galdera egingo ditu instalatzaileak; makinaren 
eposta izena edo jaso nahi diren dominioen zerrenda.

</para></listitem>
</varlistentry>

<varlistentry>
<term>ePosta smarthost-ak bidaliko du</term>
<listitem><para>

Ingurune honetan zure kaporako epostak  <quote>smarthost</quote>
deituriko beste makina bati bidali eta honek behar den lekura 
birbidaliko ditu. Smathost makinak normalean zuri doakizun eposta-k
jasoko ditu beraz zu ez zara beti sarean egon behar. Fetchmail edo 
antzerako programa bat (eposta bezeroa) erabili beharko da eposta 
jasotzeko. Aukera hau arruntean telefono bidezko saretzeek 
erabiltzen dute.

</para></listitem>
</varlistentry>

<varlistentry>
<term> bidalketa lokala bakarrik</term>
<listitem><para>

Sistema ez dago sarean eta eposta erabiltzaile lokalen artean
bidali eta jasotzen da bakrrik. Nahiz eta mezurik ez bidaltzea
pentsatu hau da aukera gomendagarria, zenbait programa
eta lanabesek eposta eabiltzen bait dute erabiltzaileari
mezu garrantzitsuak bidaltzeko (adib. <quote>Disko quota
muga gainditurik</quote>). Aukera hau da erabiltzaile berrientzat
gomendagarriena ere ez bait ditu galdera gehiago egiten.
</para></listitem>
</varlistentry>

<varlistentry>
<term>Ez konfiratu hune honetan</term>
<listitem><para>

Aukera hau zer egitera zoazen badakizula ziur bazaude bakarrik
erabili. Honek konfiguratu gabeko eposta sistema &mdash; utziko
du, zuk eskuz konfiguratu arte. Ezingo duzu eposta ez jaso 
ez bidali eta sistema lanabesen zenbait informazio garrantzitsu
gal dezakezu.
</para></listitem>
</varlistentry>
</variablelist>

<para>

Ingurune hauek ez badute zure eginbeharra betetzen edo zuk 
konfigurazio zehatzago bat behar izan ezkero 
<filename>/etc/exim4</filename> kapretako konfigurazio
fitxategiak editatu ahal izango dituzu bein instalazioa
amaitu denean.  <command>exim4</command>-eri buruzko argibide
gehiago jasotzeko ikusi <filename>/usr/share/doc/exim4</filename>.

</para>
 </sect2>
