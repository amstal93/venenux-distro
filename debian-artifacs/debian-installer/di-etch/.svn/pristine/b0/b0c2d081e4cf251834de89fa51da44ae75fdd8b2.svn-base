<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 44002 -->
<!-- actualizado por rudy, 5 diciembre 2004 -->
<!-- revisado Francisco Garc�a <franciscomanuel.garcia@hispalinux.es>, 27 enero 2005 -->
<!-- revisado rudy, 24 feb. 2005 -->

  <sect2 arch="x86"><title>Arranque desde un CD-ROM</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!--
El CD #1 del juego de CD-ROM oficial de Debian para &arch-title;
presentar� un cursor <prompt>boot:</prompt> en la mayor�a del hardware.
Pulse <keycap>F4</keycap> para ver la lista de opciones de n�cleo
disponibles desde las que puede arrancar. Simplemente escriba el nombre
de la <quote>variante</quote> (idepci, vanilla, compact, bf24) en el cursor
<prompt>boot:</prompt> seguido de &enterkey;.

</para><para>

Si su hardware no puede arrancar desde im�genes m�ltiples,
ponga uno de los otros CDs en la unidad. Aparentemente la mayor�a
de unidades de CD-ROM SCSI no soportan el arranque desde
m�ltiples im�genes de <command>isolinux</command>, por lo que los
usuarios con CD-ROMs SCSI deber�n intentarlo con el CD2 (vanilla),
CD3 (compact) o CD5 (bf2.4).

</para><para>

Cada uno de los CDs del 2 al 5 arrancar� una <quote>variante</quote> diferente dependiendo
en que tipo de CD-ROM est� insertado. Vea <xref linkend="kernel-choice"/>
para informaci�n sobre las distintas <quote>variantes</quote>. Esta es la disposici�n
de las variantes en los diferentes CD-ROMs:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

Permite una selecci�n de las im�genes de n�cleo desde el cual arrancar
(la variante <quote>idepci</quote> es la predeterminada, si no elige nada).

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Arranca la variante <quote>vanilla</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Arranca la variante <quote>compact</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Arranca la variante <quote>idepci</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Arranca la variante <quote>bf2.4</quote>.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

  </sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
     until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Arranque desde una partici�n de DOS</title>

&boot-installer-intro-hd.xml;

<para>

Arranque DOS (no Windows) sin ning�n controlador cargado. Para hacer
esto, presione <keycap>F8</keycap> en el momento exacto (y opcionalmente
elija la opci�n <quote>safe mode command prompt only</quote>). Ingrese en el subdirectorio
para la variante que elija, p. ej.:

<informalexample><screen>
cd c:\install
</screen></informalexample>.

Seguidamente, ejecute <command>install.bat</command>.
Se cargar� el n�cleo y despu�s el sistema de instalaci�n.

</para><para>

Por favor, tenga en cuenta que existe un problema con loadlin (#142421) que
impide que <filename>install.bat</filename> pueda usarse con
la variante bf2.4. El s�ntoma de este problema es el mensaje de error
<computeroutput>invalid compressed format</computeroutput>.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <title>Arranque desde Linux usando <command>LILO</command> o
  <command>GRUB</command></title>

<para>
Para arrancar el instalador desde el disco duro, primero deber�
descargar los ficheros necesarios como se describe en
<xref linkend="boot-drive-files"/>.
</para>

<para>
Si intenta usar el disco duro solamente para arrancar y descargar
todo a trav�s de la red, deber� descargar el fichero
<filename>netboot/debian-installer/&architecture;/initrd.gz</filename> y su
n�cleo correspondiente <filename>netboot/debian-installer/&architecture;/linux</filename>.
Esto le permite reparticionar el disco duro desde donde arranc� el
instalador, aunque debe hacerlo con cuidado.
</para>

<para>
Alternativamente, si desea mantener una partici�n existente en el
disco duro sin modificarla durante la instalaci�n, debe descargar el
fichero <filename>hd-media/initrd.gz</filename> y su n�cleo correspondiente,
as� como copiar una ISO de CD en el disco duro (aseg�rese de que el nombre
del fichero termine en <literal>.iso</literal>). Entonces el instalador puede arrancar
desde el disco duro e instalar desde la imagen de CD, sin necesitar la red.
</para>

<para>
Para <command>LILO</command>, deber� configurar dos cosas esenciales en
/etc/lilo.conf:
<itemizedlist>
<listitem><para>

para cargar <filename>initrd.gz</filename> del instalador al momento del
arranque;

</para></listitem>
<listitem><para>

y hacer que el n�cleo <filename>vmlinuz</filename> use este disco RAM como
su partici�n ra�z.

</para></listitem>
</itemizedlist>

Este es un ejemplo de <filename>/etc/lilo.conf</filename>:

</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

Para m�s detalles, vea las p�ginas de manual de
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> y
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>. Ahora ejecute
<userinput>lilo</userinput> y reinicie.

</para><para>

El procedimiento para <command>GRUB</command> es bastante similar. Localice su
<filename>menu.lst</filename> en el directorio <filename>/boot/grub/</filename>
(algunas veces est� en <filename>/boot/boot/grub/</filename>), y a�ada las
siguientes l�neas:

<informalexample><screen>
title  Nueva instalaci�n
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

y reinicie.

</para><para>
Tenga en cuenta que puede tener que ajustar el valor de
<userinput>ramdisksize</userinput>
en funci�n del tama�o de la imagen initrd.
Desde este momento en adelante, no habr� diferencia entre
<command>GRUB</command> o <command>LILO</command>.

</para>
  </sect2>

  <sect2 arch="x86" condition="bootable-usb" id="usb-boot">
  <title>Arranque desde un dispositivo de memoria USB</title>
<para>

Asumimos que ha preparado todo conforme se describe en <xref
linkend="boot-dev-select"/> y <xref linkend="boot-usb-files"/>. Ahora
simplemente conecte su dispositivo de memoria USB en alguno de los
conectores USB libres y reinicie el ordenador. El sistema deber� arrancar,
y deber�a presentar un cursor <prompt>boot:</prompt>. Aqu� puede
ingresar argumentos de arranque adicionales o simplemente presionar
&enterkey;.

</para>
  </sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <title>Arranque desde disquetes</title>
<para>

Deber�a haber descargado previamente las im�genes que necesita y creado los
disquetes desde �stas como se explica en <xref linkend="create-floppy"/>.
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see
<xref linkend="rescue-replace-kernel"/>. -->

</para><para>

Para arrancar el instalador desde el disquete, introd�zcalo en la unidad de
disquete primaria, apague el sistema como lo har�a normalmente, luego enci�ndalo
nuevamente.

</para><para>

Para instalar desde una unidad LS-120 (versi�n ATAPI) con un juego
de disquetes, necesitar� especificar la ubicaci�n virtual de la
unidad de disquete. Puede hacer esto con el argumento de arranque
<emphasis>root=</emphasis>, a�adiendo el dispositivo asignado por el
controlador de disquetes IDE. Por ejemplo, si su unidad LS-120 est�
conectada como el primer dispositivo IDE (maestro) en el segundo
cable, debe ingresar
<userinput>install root=/dev/hdc</userinput> en el cursor de arranque.

</para><para>

Note que en algunas m�quinas, <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Supr</keycap></keycombo> no reinicia
la m�quina apropiadamente, por lo que se recomienda reiniciar en
forma <quote>forzada</quote> o total. Si est� instalando desde un sistema operativo
existente (p. ej. desde un sistema DOS) no tiene opci�n. De otro modo,
por favor reinicie en forma <quote>forzada</quote> o total cuando arranque.

</para><para>

Se acceder� al disquete, deber�a ver una pantalla que presente
el disquete de arranque y finalice mostrando el cursor
<prompt>boot:</prompt>.

</para><para>

Cuando presione &enterkey;, deber�a ver el mensaje
<computeroutput>Loading...</computeroutput>, seguido de
<computeroutput>Uncompressing Linux...</computeroutput>, y
luego una pantalla llena de informaci�n sobre el hardware
de su sistema. Puede encontrar m�s informaci�n sobre esta fase
del proceso de arranque en <xref linkend="kernel-msgs"/>.

</para><para>

Despu�s de arrancar el disquete de arranque, se solicita el
disquete marcado como <quote>root</quote>. Ins�rtelo en la unidad y presione
&enterkey;, los contenidos se cargar�n en memoria. El programa
instalador <command>debian-installer</command> se cargar�
autom�ticamente.

</para>
  </sect2>

  <sect2 arch="x86" id="boot-tftp"><title>Arranque con TFTP</title>

&boot-installer-intro-net.xml;

<para>

Existen varias formas de realizar un arranque con TFTP en i386.

</para>

   <sect3><title>Tarjetas de red o placas base que soportan PXE</title>
<para>

Podr�a ser que su tarjeta de red o placa base provea la
funcionalidad de arranque PXE. Lo que es una reimplementaci�n
de <trademark class="trade">Intel</trademark> del arranque TFTP.
De ser su caso podr�a tener la posibilidad de configurar su BIOS
para arrancar desde la red.

</para>
   </sect3>

   <sect3><title>Tarjeta de red con bootROM</title>
<para>

Podr�a ser que su tarjeta de red (NIC) provea la funcionalidad
de arranque usando TFTP.

</para><para condition="FIXME">

D�jenos (<email>&email-debian-boot-list;</email>) saber como
lo efectu�. Por favor, haga referencia a este documento.

</para>
   </sect3>

   <sect3><title>Etherboot</title>
<para>

El <ulink url="http://www.etherboot.org">proyecto etherboot</ulink>
provee disquetes de arranque e incluso <quote>bootroms</quote> que efect�an un
arranque usando TFTP.

</para>
   </sect3>

  </sect2>

  <sect2 arch="x86"><title>El cursor de arranque</title>
<para>

Cuando arranca el instalador, se le presentar� una pantalla gr�fica amigable
con el logo de Debian y el cursor de arranque:

<informalexample><screen>
Press F1 for help, or ENTER to boot:
</screen></informalexample>

(<quote>Pulse F1 para ayuda o ENTER para arrancar:</quote>, n. del t.)
En el cursor de arranque puede pulsar simplemente  &enterkey; para arrancar
el instalador con las opciones por omisi�n o introducir un m�todo de arranque
espec�fico y, opcionalmente, par�metros de arranque.

</para><para>

Puede encontrar informaci�n, que le podr�a ser �til, sobre los par�metros
de arranque y las opciones de arranque presionando las teclas
<keycap>F2</keycap> a <keycap>F8</keycap>.  Si a�ade par�metros a la l�nea
de �rdenes de arranque, aseg�rese de escribir el m�todo de arranque (el
predeterminado es <userinput>install</userinput>) y un espacio despu�s del primer
par�metro (p. ej. <userinput>install fb=false</userinput>). 

<note><para>

Si est� instalando el sistema a trav�s de un sistema de gesti�n remoto que
dispone de un interfaz de texto a la consola VGA puede que no vea la pantalla
gr�fica inicial cuando arranque el instalador. De hecho, puede que ni siquiera
vea el cursor de arranque. Algunos ejemplos de estos sistemas son la consola de
texto de Compaq <quote>integrated Lights Out</quote> (iLO) y la consola de HP
<quote>Integrated Remote Assistant</quote> (IRA).
En estos casos puede simplemente presionar F1 a ciegas
<footnote>

<para>

En algunos casos tiene que utilizar una secuencia de escape en estos
dispositivos para poder ejecutar esa pulsaci�n de tecla. Por ejemplo, el
dispositivo IR utiliza <keycombo> <keycap>Ctrl</keycap> <keycap>F</keycap>
</keycombo>,&nbsp;<keycap>1</keycap>.

</para>

</footnote> para saltar esta pantalla y mostrar el texto de ayuda.  Una vez
salga de la pantalla de inicio y dentro del texto de ayuda se le mostrar�n sus
pulsaciones de teclado en el cursor como suele ser habitual. Si quiere evitar
que el instalador utilice el <quote>framebuffer</quote> durante la instalaci�n
puede a�adir la opci�n
<userinput>fb=false</userinput> en el cursor de
arranque tal y como se describe en el texto de ayuda.


</para></note>

</para>

  </sect2>
