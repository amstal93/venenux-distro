arcboot-installer (1.11lenny1) unstable; urgency=high

  * Rebuild with fixed Danish translation

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 10 Jan 2009 09:29:44 +0100

arcboot-installer (1.11) unstable; urgency=low

  [ Frans Pop ]
  * Remove Alastair McKinstry and Karsten Merker as Uploaders with many
    thanks for their past contributions. Add Martin Michlmayr.

  [ Martin Michlmayr ]
  * Remove myself as Uploader and add Thiemo Seufer.

  [ Updated translations ]
  * Belarusian (be.po) by Pavel Piatruk
  * Bosnian (bs.po) by Armin Besirovic
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * French (fr.po) by Christian Perrier
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Milo Casagrande
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Peteris Krisjanis
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Slovak (sk.po) by Ivan Masár
  * Serbian (sr.po) by Veselin Mijušković
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 26 Sep 2008 14:31:14 +0300

arcboot-installer (1.10) unstable; urgency=low

  [ Updated translations ]
  * Finnish (fi.po) by Esko Arajärvi
  * Indonesian (id.po) by Arief S Fitrianto
  * Polish (pl.po) by Bartosz Fenski
  * Turkish (tr.po) by Recai Oktaş
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 15 Feb 2008 19:46:24 +0100

arcboot-installer (1.9) unstable; urgency=low

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Belarusian (be.po) by Hleb Rubanau
  * Bengali (bn.po) by Jamil Ahmed
  * Danish (da.po) by Claus Hindsgaul
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Changwoo Ryu
  * Latvian (lv.po) by Viesturs Zarins
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Dutch (nl.po) by Frans Pop
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Ivan Masár
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 04 Feb 2008 23:29:34 +0100

arcboot-installer (1.8) unstable; urgency=low

  [ Joey Hess ]
  * Multiply menu-item-numbers by 100

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Basque (eu.po) by Piarres Beobide
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Apr 2007 14:32:43 -0700

arcboot-installer (1.7) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Feb 2007 19:59:42 +0000

arcboot-installer (1.6) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Bosnian (bs.po) by Safir Secerovic
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 01 Feb 2007 16:32:43 +0100

arcboot-installer (1.5) unstable; urgency=low

  [ Colin Watson ]
  * Mark firmware commands in arcboot/prom-variables as untranslatable.

  [ Frans Pop ]
  * Update arcboot/prom-variables to no longer refer to next installation
    stage. Closes: #389526.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan; Valencian (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish; Castilian (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmål; Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Dutch; Flemish (nl.po) by Bart Cornelis
  * Panjabi; Punjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Oct 2006 20:46:46 +0100

arcboot-installer (1.4) unstable; urgency=low

  * Thiemo Seufer
    - New upload for translation updates.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan; Valencian (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Irish (ga.po) by Kevin Patrick Scannell
  * Gujarati (gu.po) by Kartik Mistry
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Giuseppe Sacco
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk; Nynorsk, Norwegian (nn.po) by Håvard Korsvoll
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall

 -- Thiemo Seufer <ths@hattusa.textio>  Sun, 16 Jul 2006 14:35:01 +0100

arcboot-installer (1.3) unstable; urgency=low

  * Christian Perrier
    - Add a comment about untranslatable stuff in one template
  * Frans Pop
    - Improve branding using same string as yaboot-installer

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Norwegian Nynorsk (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Xhosa (xh.po) by Canonical Ltd
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Jan 2006 22:30:07 +0000

arcboot-installer (1.2) unstable; urgency=low

  * Thiemo Seufer
    - New upload for translation updates.
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Italian (it.po) by Stefano Melchior
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor

 -- Thiemo Seufer <ths@debian.org>  Thu, 03 Feb 2005 00:36:18 +0100

arcboot-installer (1.1) unstable; urgency=low

  * Martin Michlmayr
    - Fix subarchitecture detection for chrooted installation.
      Closes: #280372.
    - Obtain the correct information about the root device under 2.6.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek (el.po) by Greek Translation Team
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Stefano Melchior
    - Russian (ru.po) by Yuri Kozlov

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 09 Nov 2004 01:35:07 +0000

arcboot-installer (1.0) unstable; urgency=low

  * Martin Michlmayr
    - Gratuitous version number bump.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 07 Oct 2004 13:48:52 +0100

arcboot-installer (0.012) unstable; urgency=low

  * Martin Michlmayr
    - Print correct scsi PROM variables when running a 2.6 kernel.
      Closes: #261491
  * Updated translations:
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 29 Sep 2004 20:25:23 +0100

arcboot-installer (0.011) unstable; urgency=low

  * Martin Michlmayr
    - Add IP32 sub-architectures to the control file.
  * Updated translations:
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - German (de.po) by Dennis Stampfer
    - Hebrew (he.po) by Lior Kaplan
    - hr (hr.po) by Krunoslav Gernhard
    - Korean (ko.po) by Jung Seung-Cheol
    - Norwegian (nn.po) by Håvard Korsvoll
    - Portuguese (pt.po) by Miguel Figueiredo
    - Russian (ru.po) by Dmitry Beloglazov
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 25 Jul 2004 18:30:35 +0100

arcboot-installer (0.010) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by George Papamichelakis
    - Spanish (es.po) by Teófilo Ruiz Suárez
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Michel Grentzinger
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Stefano Melchior
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Norwegian (nb.po) by Knut Yrvin
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Albanian (sq.po) by Elian Myftiu

 -- Joshua Kwan <joshk@triplehelix.org>  Tue, 25 May 2004 10:43:39 -0700

arcboot-installer (0.009) unstable; urgency=low

  * Joshua Kwan
    - Convert changelog to UTF-8 as required by policy.
    - Use debhelper's new udeb support.
  * Thiemo Seufer
    - Add a reminder screen to set the correct firmware variables,
      thanks to Nicholas Breen.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Basque (eu.po) by Piarres Beobide Egaña
    - Gallegan (gl.po) by Héctor Fernández López
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Stefano Melchior
    - Norwegian (nb.po) by Bjørn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Nikolai Prokoschenko
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 20 May 2004 01:41:37 +0100

arcboot-installer (0.008) unstable; urgency=medium

  * Joshua Kwan
    - Add a menu entry for arcboot-installer.
  * Martin Michlmayr
    - Only use arcboot on the r4k-ip22 and r5k-ip22 sub-architectures
      (i.e. SGI machines).
  * Thiemo Seufer
    - Fix installation, remove the isinstallable check.
  * Colin Watson
    - Subarchitecture: is whitespace-separated; remove comma.
  * Updated translations:
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Konstantinos Margaritis
    - Spanish (es.po) by TeÃ³filo Ruiz SuÃ¡rez
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by Michel Grentzinger
    - Hungarian (hu.po) by VERÃK IstvÃ¡n
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by KÄstutis BiliÅ«nas
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by HÃ¥vard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by AndrÃ© LuÃ­s Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Äuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Tobias GrÃ¶nqvist
    - Turkish (tr.po) by Osman YÃ¼ksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 23 Mar 2004 20:20:01 +0000

arcboot-installer (0.007) unstable; urgency=low

  * Updated translations:
   - Arabic (ar.po) by Ossama Khayat
   - Catalan (ca.po) by Jordi Mallach
   - Finnish (fi.po) by Tapio Lehtonen
   - Korean (ko.po) by Changwoo Ryu
   - Lithuanian (lt.po) by KÄstutis BiliÅ«nas
   - Norwegian (nn.po) by HÃ¥vard Korsvoll
   - Romanian (ro.po) by Eddy Petrisor
   - Russian (ru.po) by Nikolai Prokoschenko
   - Slovak (sk.po) by Peter KLFMANiK Mann
   - Albanian (sq.po) by Elian Myftiu
   - Turkish (tr.po) by Osman YÃ¼ksel
   - Ukrainian (uk.po) by Eugeniy Meshcheryakov
   - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
   - Traditional Chinese (zh_TW.po) by Tetralet
   - Initial Swedish translation (sv.po)
   - Korean (ko.po) by Changwoo Ryu
   - Norwegian bokmÃ¥l (nb.po) by HÃ¥vard Korsvoll from Axel Bojer
   - German (de.po) by Dennis Stampfer

 -- Karsten Merker <merker@debian.org>  Sun, 14 Mar 2004 19:02:51 +0100

arcboot-installer (0.006) unstable; urgency=high

  * Ming Hua
    - Initial Simplified Chinese translation (zh_CN.po)
  * Bartosz Fenski
    - Updated Polish (pl) translation.
  * Ilgiz Kalmetev
    - Initial Russian translation.
  * Konstantinos Margaritis
    - Initial Greek translation (el.po)
  * Dennis Stampfer
    - Initial German translation (de.po)
  * Safir Å eÄeroviÄ
    - Updated Bosnian translation (bs.po)
  * Verok Istvan
    - Initial Hungarian translation (hu.po)
  * Konstantinos Margaritis
    - Updated Greek translation (el.po)
  * Steinar H. Gunderson
    - Updated Norwegian translation (nb.po).
  * Bart Cornelis
    - Updated Dutch translation (nl.po)
    - Merged Norwegian Bokmael (nb.po) from skolelinux-cvs
    - Merged Norwegian Nynorsk (nn.po) from skolelinux-cvs
  * Thiemo Seufer
    - Preliminary fix of postinst.
    - Fix serial console parameter parsing.
  * KÄstutis BiliÅ«nas
    - Added Lithuanian translation (lt.po).
  * Ognyan Kulev
    - Added/updated bulgarian translation (bg.po).
  * Giuseppe Sacco
    - Added italian translation by Stefano Melchior (it.po)
  * Michel Grentzinger
    - Updated french translation (fr.po)
  * Jure Cuhalev
    - Added/updated slovenian translation (sl.po).
  * Marco Ferra
    - Added portuguese translation (pt.po).
  * TeÃ³filo Ruiz SuÃ¡rez
    - Updated Spanish translation (es.po)
    - Switched to UTF-8
  * AndrÃ© LuÃ­s Lopes
    - Made translation of the template which reads
      "Disk for boot loader installation:" be consistent between
      delo-installer and arcboot-installer.
  * TeÃ³filo Ruiz SuÃ¡rez
    - Fixed some inconsistencies in Spanish Translation (es.po)
  * Peter Mann
    - Initial Slovak translation

 -- Karsten Merker <merker@debian.org>  Sun, 04 Jan 2004 21:06:45 +0100

arcboot-installer (0.005) unstable; urgency=low

  * Safir Secerovic
    - Add Bosnian translation (bs.po).
  * Christian Perrier:
    - Debconf template polishing. Closes: #218569.
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * Michel Grentzinger
    - Update French translation.
  * Miroslav Kure
    - Update Czech (cs.po) translation.
  * AndrÃ© LuÃ­s Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * Bart Cornelis
    - Update Dutch translation (nl.po)
  * Claus Hindsgaul
    - Added Danish translation (da.po).

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 11 Nov 2003 21:30:47 +0100

arcboot-installer (0.004) unstable; urgency=low

  * Kenshi Muto
    - Added Japanese translation (ja.po)
    - Update ja.po
  * Chris Tillman
    - Update English usage in message templates
  * Jordi Mallach
    - Added Catalan translation.
  * Pierre Machard
    - Update French po-debconf translation [Michel Grentzinger]
    - Update French po-debconf translation [Christian Perrier]
  * AndrÃ© LuÃ­s Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * Bart Cornelis
    - updated dutch translation
  * Miroslav Kure
    - Added Czech translation.
  * Bruno Rodrigues
    - Added Portuguese translation. Closes: 216393.
  * Claus Hindsgaul
    - Added Danish translation (da.po).

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 22 Oct 2003 21:31:24 +0100

arcboot-installer (0.003) unstable; urgency=low

  * Changed maintainer email to mkinstry@debian.org
  * Moved to Standards-Version: 3.6.0; Changed Changelog to UTF-8

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 31 Jul 2003 08:07:04 +0100

arcboot-installer (0.002) unstable; urgency=low

  * Carlos Valdivia YagÃ¼e :
      - Add Spanish (es) debconf template translation.
  * Petter Reinholdtsen
    - Add french translation by Michel Grentzinger. (Closes: #197310)

 -- Alastair McKinstry <mckinstry@computer.org>  Mon, 16 Jun 2003 13:32:37 +0100

arcboot-installer (0.001) unstable; urgency=low

  * First release
  * THIS IS PRETTY MUCH UNTESTED!!!!!!
    DON'T use this unless you're interested in helping me debug.
  * AndrÃ© LuÃ­s Lopes :
    - Add Brazilian Portuguese (pt_BR) debconf template translation.

 -- Alastair McKinstry <mckinstry@computer.org>  Sun, 11 May 2003 09:19:13 +0300
