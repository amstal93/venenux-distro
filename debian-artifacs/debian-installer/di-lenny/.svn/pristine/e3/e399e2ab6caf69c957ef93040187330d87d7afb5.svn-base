# This file is distributed under the same license as the win32-loader package.
# Copyright 2007, 2008 (C) Robert Millan <rmh@aybabtu.com>
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-03-22 13:39+0200\n"
"Last-Translator: Robert Millan <rmh@aybabtu.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_CATALAN"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp437"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Catalan"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Instal·lador de Debian"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Catalan.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Carregador de l'instal·lador de Debian"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "No s'ha pogut trobar win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini incomplet.  Contacteu amb el proveidor d'aquest medi."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Aquest programa ha detectat que el vostre tipus de teclat és \"$0\".  És "
"correcte?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Si us plau, envieu un informe d'error amb la següent informació:\n"
"\n"
" - Versió de Windows.\n"
" - Configuració regional.\n"
" - Tipus de teclat que teniu.\n"
" - Tipus de teclat que s'ha detectat.\n"
"\n"
"Gràcies."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Sembla que no hi ha prou espai lliure al disc $c.  Per a una instal·lació "
"completa d'escriptori, se'n recomanen com a mínim 3 GB.  Si ja hi ha un disc "
"o partició separats on instal·lar Debian, o si penseu reemplaçar Windows "
"completament, podeu ignorar aquest avís."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Error: no hi ha prou espai lliure en disc.  S'aborta la instal·lació."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Aquest programa encara no suporta Windows $windows_version."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"La versió de Debian que tracteu d'instal·lar està dissenyada per a "
"ordinadors moderns de 64 bits.  No obstant, el vostre ordinador no és capaç "
"d'executar programes de 64 bits.\n"
"\n"
"Empreu la versió de 32 bits de Debian (\"i386\"), o la versió de Multi-arch "
"que és capaç d'instal·lar qualssevol de les dues.\n"
"\n"
"S'abortarà la instal·lació."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"El vostre ordinador és capaç d'executar sistemes operatius moderns de 64 "
"bits.  No obstant, la versió de Debian que tracteu d'instal·lar està "
"dissenyada per a executar-se en maquinari antic, de 32 bits.\n"
"\n"
"Podeu prosseguir amb la instal·lació, però per tal d'obtindre el màxim "
"profit del vostre ordinador, us recomanem que empreu la versió de Debian de "
"64 bits (\"amd64\") en comptes d'aquesta, o la versió de Multi-arch que és "
"capaç d'instal·lar qualssevol de les dues.\n"
"\n"
"Desitgeu abortar ara?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Seleccioneu el mode d'instal·lació:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Mode normal.  Recomanat per a la majoria dels usuaris."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Mode expert.  Recomanat per a usuaris experts que volen tot el control del "
"procés d'instal·lació."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Sel·leccioneu una acció:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Instal·lar Debian GNU/Linux en aquest ordinador."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Reparar una instal·lació existent de Debian (mode de rescat)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Instal·lació gràfica"

#: win32-loader.c:89
msgid "Text install"
msgstr "Instal·lació en text"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Abaixant %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Connectant ..."

#: win32-loader.c:92
msgid "second"
msgstr "s"

#: win32-loader.c:93
msgid "minute"
msgstr "m"

#: win32-loader.c:94
msgid "hour"
msgstr "h"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr " "

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) de %dkB a %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s restant)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Seleccioneu la versió de l'instal·lador de Debian a emprar:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Llançament estable.  Això instal·larà Debian \"stable\"."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Compilació diaria.  Això és la versió de desenvolupament de l'instal·lador "
"de Debian.  Instal·larà Debian \"testing\" per defecte, i potser sigui capaç "
"d'instal·lar \"stable\" o \"unstable\" també."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Es recomana que comproveu els problemes coneguts (en anglès) abans d'emprar "
"una compilació diaria.  Voleu fer-ho ara?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Seleccioneu un entorn d'escriptori:"

#: win32-loader.c:114
msgid "None"
msgstr "Cap"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Es configurarà el carregador de l'instal·lador de Debian amb els parametres "
"següents.  NO en canvieu cap a no ser que sapigueu el que esteu fent."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Servidor intermediari (host:port):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Ubicació de boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "URL base per a les imatges \"netboot\" (linux i initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Error"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Error: no s'ha pogut copiar $0 a $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Generant $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Afegint informació de preseeding a $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Error: no s'ha pogut executar $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Desactivant la compressió NTFS als fitxers d'arrencada"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registrant l'instal·lador de Debian amb NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registrant l'instal·lador de Debian amb BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Error: no s'ha pogut processar l'eixida de bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr ""
"Error: no s'ha trobat boot.ini.  Això és realment Windows $windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "AVÍS MOLT IMPORTANT:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"Ara començarà la segona fase d'aquest procés d'instal·lació.  Quan ho "
"confirmeu, aquest programa reiniciarà Windows en mode DOS, i automàticament "
"carregarà l'instal·lador de Debian.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Cal que reinicieu per tal de procedir amb la instal·lació de Debian.  Durant "
"el proper inici, se us demanarà si voleu iniciar Windows o bé l'instal·lador "
"de Debian.  Trieu l'instal·lador de Debian per tal de continuar amb el "
"procés d'instal·lació.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Durant el procés d'instal·lació, se us oferirà la possibilitat de reduir la "
"partició de Windows per tal d'instal·lar-hi Debian o bé reemplaçar-la "
"completament.  En tots dos casos, es RECOMANA VEHEMENTMENT que abans feu una "
"còpia de seguretat de les vostres dades.  Ni els autors d'aquest carregador "
"ni el projecte Debian assumirà CAP RESPONSABILITAT en cas de pèrdua de dades."
"\\n\\nUn cop la instal·lació de Debian s'hagi completat (i si trieu "
"conservar Windows al disc), podeu desinstal·lar el carregador de "
"l'instal·lador de Debian a través del menú d'afegir/llevar programes al "
"Tauler de Control."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Voleu reiniciar ara?"
