# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005-2007.
# Felipe Castro <fefcas@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: eo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-21 22:51+0000\n"
"PO-Revision-Date: 2008-08-03 08:57-0300\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:1001
msgid "Continue installation remotely using SSH"
msgstr "Daŭrigi instaladon de malproksime per SSH"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer"
msgstr "Lanĉi la instalilon"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer (expert mode)"
msgstr "Lanĉi la instalilon (por spertuloj)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start shell"
msgstr "Lanĉi la ŝelon"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "Network console option:"
msgstr "Reta konzola alternativo:"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid ""
"This is the network console for the Debian installer. From here, you may "
"start the Debian installer, or execute an interactive shell."
msgstr ""
"Tio ĉi estas la reta konzolo por la Debiana instalilo. El tie ĉi, vi povas "
"lanĉi la Debianan instaladon, aŭ plenumi interagan ŝelon."

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "To return to this menu, you will need to log in again."
msgstr "Por reiri al tiu menuo, vi bezonos denove rekonekti vin."

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:3001
msgid "Generating SSH host key"
msgstr "Generi 'SSH'-an gastan ĉifroŝlosilon"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid "Remote installation password:"
msgstr "Demalproksima instalada pasvorto:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"You need to set a password for remote access to the Debian installer. A "
"malicious or unqualified user with access to the installer can have "
"disastrous results, so you should take care to choose a password that is not "
"easy to guess. It should not be a word found in the dictionary, or a word "
"that could be easily associated with you, like your middle name."
msgstr ""
"Vi bezonas difini pasvorton por demalproksime atingi Debianan instalilon. "
"Malica aŭ malsperta uzanto kiu atingus la instalilon povus kaŭzi "
"katastrofon, do vi ege zorge devos elekti nediveneblan pasvorton. Ĝi nepre "
"ne troviĝu en vortaro kaj ĝi ne estu vorto ligita al vi, kiel via dua "
"persona nomo."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"This password is used only by the Debian installer, and will be discarded "
"once you finish the installation."
msgstr ""
"Tiu pasvorto estas uzita nur per Debiana instalilo, kaj ĝi estos forlasita "
"kiam la instalado finiĝos."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid "Re-enter password to verify:"
msgstr "Retajpu la pasvorton por konfirmi:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid ""
"Please enter the same remote installation password again to verify that you "
"have typed it correctly."
msgstr ""
"Bonvolu retajpi la saman foran instaladan pasvorton por kontroli ĝian "
"tajpĝustecon."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid "Empty password"
msgstr "Malplena pasvorto"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Vi tajpis malpermesitan malplenan pasvorton. Bonvolu tajpi plenan pasvorton."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid "Password mismatch"
msgstr "Mistajpita pasvorto"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid ""
"The two passwords you entered were not the same. Please enter a password "
"again."
msgstr "La du pasvortoj kiujn vi tajpis malsamas. Bonvolu reenigi pasvorton."

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "Start SSH"
msgstr "Lanĉi 'SSH'-on"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"To continue the installation, please use an SSH client to connect to the IP "
"address ${ip} and log in as the \"installer\" user. For example:"
msgstr ""
"Por daŭrigi la instaladon, bonvolu uzi 'SSH'-klienton por konekti vin al la "
"'IP'-adreso ${ip} kaj salutu kiel uzanto \"installer\". Ekzemple:"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "The fingerprint of this SSH server's host key is: ${fingerprint}"
msgstr "La 'SSH'-gastservila ĉifroŝlosila stampaĵo estas: ${fingerprint}"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"Please check this carefully against the fingerprint reported by your SSH "
"client."
msgstr "Bonvolu zorge rekontroli la stampaĵon raportitan de via 'SSH'-kliento."
