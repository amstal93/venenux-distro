# translation of win32-loader to German
# This file is distributed under the same license as the win32-loader package.
# Copyright (C) Jan Keller <bananenscheibe@gmx.de>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2007-03-24 21:08+0200\n"
"Last-Translator: Jan Keller <bananenscheibe@gmx.de>\n"
"Language-Team: <debian-l10n-german@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_GERMAN"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp437"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "German"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian-Installer"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "German.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Ladeprogramm des Debian-Installers"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Kann win32-loader.ini nicht finden."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr ""
"Die Datei win32-loader.ini ist unvollständig. Kontaktieren Sie den Anbieter "
"dieses Programms."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Dieses Programm hat Ihr Tastaturlayout als »$0« erkannt. Ist dies korrekt?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Bitte schicken Sie uns einen Fehlerbericht mit den folgenden Informationen:\n"
"\n"
" - Windows-Version.\n"
" - Regionale Einstellungen.\n"
" - Ihr tatsächliches Tastaturlayout.\n"
" - Erkanntes Tastaturlayout.\n"
"\n"
"Danke."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Der Platz auf Laufwerk $c scheint nicht auszureichen. Für die Installation "
"eines kompletten Desktop-Systems wird mindestens 3 GB freier "
"Festplattenplatz empfohlen. Wenn bereits eine separate Festplatte oder "
"Partition existiert, um darauf Debian zu installieren oder falls Sie planen, "
"Windows komplett zu ersetzen, können Sie diese Warnung ignorieren."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr ""
"Fehler: Nicht genügend Platz auf der Festplatte. Breche Installation ab."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Dieses Programm unterstützt Windows $windows_version noch nicht."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"Die Version von Debian, die Sie versuchen zu installieren, wurde entwickelt, "
"um auf modernen 64-Bit-Rechnern zu laufen. Allerdings kann Ihr Rechner keine "
"64-Bit-Programme ausführen.\n"
"\n"
"Verwenden Sie die 32-Bit-Version von Debian (»i386«) oder die Multi-Arch-"
"Version (für mehrere Architekturen), die in der Lage ist, beides zu "
"installieren.\n"
"\n"
"Die Installation wird jetzt abgebrochen."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Ihr Rechner ist in der Lage, moderne 64-Bit-Betriebssysteme auszuführen, "
"aber die Version von Debian, die Sie gerade installieren wollen, wurde "
"entwickelt, um auf älterer 32-Bit-Hardware zu laufen.\n"
"\n"
"Sie können diese Installation fortsetzen, aber um die Fähigkeiten Ihres "
"Rechners voll ausnutzen zu können, empfehlen wir Ihnen, die 64-Bit-Version "
"von Debian (»amd64«) zu installieren oder die Multi-Arch-Version (für "
"mehrere Architekturen) zu verwenden, die in der Lage ist, beides zu "
"installieren.\n"
"\n"
"Möchten Sie jetzt abbrechen?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Wählen Sie die Art der Installation:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Normaler Modus. Für die meisten Benutzer empfohlen."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Expertenmodus. Empfohlen für fortgeschrittene Benutzer, die volle Kontrolle "
"über den Installationsprozess haben wollen."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Wählen Sie eine Aktion:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Debian GNU/Linux auf diesem Rechner installieren."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Ein bestehendes Debian-System reparieren (Rettungsmodus)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Grafische Installation"

#: win32-loader.c:89
msgid "Text install"
msgstr "Installation im Textmodus"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Lade %s herunter"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Verbinde ..."

#: win32-loader.c:92
msgid "second"
msgstr "Sekunde"

#: win32-loader.c:93
msgid "minute"
msgstr "Minute"

#: win32-loader.c:94
msgid "hour"
msgstr "Stunde"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "n"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%d kB (%d%%) von %d kB bei %d.%01d kB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s verbleibend)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr ""
"Wählen Sie die Version des Debian-Installers, die verwendet werden soll:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Stabile Veröffentlichung. Dies wird Debian »Stable« installieren."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Täglicher Build. Dies ist die Entwicklerversion des Debian-Installers. Sie "
"wird standardmäßig Debian »Testing« installieren, könnte aber auch für die "
"Installation von »Stable« oder »Unstable« verwendet werden."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Es wird empfohlen, dass Sie sich die bekannten Probleme (»known issues«) "
"ansehen, bevor Sie einen täglichen Build verwenden. Allerdings ist diese "
"Seite nur auf Englisch verfügbar. Möchten Sie dies jetzt tun?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Grafische Arbeitsoberfläche:"

#: win32-loader.c:114
msgid "None"
msgstr "Keine"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Das Ladeprogramm des Debian-Installers verwendet folgende Parameter. Ändern "
"Sie NICHTS, falls Sie nicht wissen, was Sie tun."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Proxy-Einstellungen (Rechner:Port):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Ort von boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "URL für die Netzinstallations-Images (linux und initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Fehler"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Fehler: Kopieren von $0 nach $1 ist fehlgeschlagen."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Erzeuge $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Füge Voreinstellungsinformationen zu $0 hinzu"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Fehler: kann $0 nicht ausführen."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Deaktiviere die NTFS-Kompression in den bootstrap-Dateien."

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registriere den Debian-Installer in NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registriere den Debian-Installer in BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Fehler: konnte die Ausgabe von bcdedit.exe nicht verarbeiten."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr ""
"Fehler: boot.ini wurde nicht gefunden. Ist das wirklich Windows "
"$windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "SEHR WICHTIGER HINWEIS:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"Die zweite Stufe des Installationsprozesses wird nun gestartet. Nach Ihrer "
"Bestätigung wird dieses Programm Windows im DOS-Modus neu starten und den "
"Debian-Installer automatisch laden.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Sie müssen den Rechner neu starten, um mit der Debian-Installation "
"fortzufahren. Beim nächsten Start können Sie auswählen, ob Sie Windows oder "
"den Debian-Installer starten wollen. Wählen Sie den Debian-Installer, um den "
"Installationsprozess fortzusetzen.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Während der Installation ist es möglich, entweder Ihre Windows-Partition zu "
"verkleinern, um Debian zu installieren, oder die Partition vollständig zu "
"ersetzen. Für beide Fälle wird DRINGEND EMPFOHLEN, vorher eine Sicherung "
"Ihrer Daten durchzuführen. Weder die Autoren dieses Ladeprogramms noch das "
"Debian-Projekt werden IRGENDEINE VERANTWORTUNG übernehmen, falls Sie Ihre "
"Daten verlieren.\\n\\nWenn die Debian-Installation abgeschlossen ist (und "
"Sie Windows auf der Festplatte behalten haben), können Sie dieses "
"Ladeprogramm mit dem »Programme Ändern/Entfernen«-Dialog in der Windows-"
"Systemsteuerung deinstallieren."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Wollen Sie jetzt neu starten?"

#~ msgid "Debconf preseed line:"
#~ msgstr "Zeile für Voreinstellungen von Debconf:"
