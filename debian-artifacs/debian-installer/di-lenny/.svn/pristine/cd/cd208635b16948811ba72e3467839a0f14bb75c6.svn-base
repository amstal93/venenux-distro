partman-ext2r0 (1.18) unstable; urgency=high

  * Rebuild with fixed Danish translation

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 09 Jan 2009 22:28:38 +0100

partman-ext2r0 (1.17) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Armin Besirovic
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Esperanto (eo.po) by Felipe Castro
  * French (fr.po) by Christian Perrier
  * Hebrew (he.po) by Omer Zak
  * Hindi (hi.po) by Kumar Appaiah
  * Croatian (hr.po) by Josip Rodin
  * Georgian (ka.po) by Aiet Kolkhi
  * Latvian (lv.po) by Peteris Krisjanis
  * Dutch (nl.po) by Frans Pop
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Serbian (sr.po) by Veselin Mijušković
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 23 Sep 2008 06:06:40 +0200

partman-ext2r0 (1.16) unstable; urgency=low

  [ Martin Michlmayr ]
  * Remove myself as an uploader.

  [ Jérémy Bobbio ]
  * Use cdebconf's new column alignment feature for active_partition.
    Requires partman-base (>= 124).

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * Hebrew (he.po) by Lior Kaplan
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Milo Casagrande
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 05 Aug 2008 22:08:28 +0300

partman-ext2r0 (1.15) unstable; urgency=low

  * Use common mount options template; requires partman-basicfilesystems (59).
  * Drop dependency on ancient version of di-utils.
  * Remove standards version fields and add lintian override for it.
  * Add missing debconf dependency.

 -- Frans Pop <fjp@debian.org>  Wed, 26 Mar 2008 15:48:07 +0100

partman-ext2r0 (1.14) unstable; urgency=low

  [ Frans Pop ]
  * Moved definitions.sh to ./lib/base.sh. Requires partman-base (>= 114).
  * Major whitespace cleanup and some coding style improvements.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 15 Feb 2008 19:48:46 +0100

partman-ext2r0 (1.13) unstable; urgency=low

  [ Colin Watson ]
  * Move sanity-checking scripts from finish.d to check.d. Requires
    partman-base 106.
  * Honour all supplied mount options. Requires partman-target 51 to filter
    out ro.
  * Use 'mkdir -p' rather than more awkward test-then-create constructions.
  * Add support for relatime mount option (see
    http://lkml.org/lkml/2006/8/25/380; requires util-linux(-ng) 2.13).
  * Remove redundant "defaults" if adding other mount options.

  [ Frans Pop ]
  * Move deletion of SVN directories to install-rc script.
  * Improve the way install-rc is called.

  [ Updated translations ]
  * Belarusian (be.po) by Hleb Rubanau
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bengali (bn.po) by Jamil Ahmed
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae Park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Colin Watson <cjwatson@debian.org>  Tue, 23 Oct 2007 16:13:42 +0100

partman-ext2r0 (1.12) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A
  * Swedish (sv.po) by Daniel Nylander

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Feb 2007 19:49:59 +0000

partman-ext2r0 (1.11) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivanov
  * Bengali (bn.po) by Jamil Ahmed
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hungarian (hu.po) by SZERVÁC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by drtvasudevan
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 01 Feb 2007 16:30:46 +0100

partman-ext2r0 (1.10) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Indonesian (id.po) by Arief S Fitrianto
  * Kurdish (ku.po) by Erdal Ronahi
  * Romanian (ro.po) by Eddy Petrișor
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Oct 2006 20:30:26 +0100

partman-ext2r0 (1.9) unstable; urgency=low

  [ Martin Michlmayr ]
  * Drop "-O none" from the mkfs.ext2 call because it's not needed and
    currently broken (see #392107).

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Northern Sami (se.po) by Børre Gaup
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 10 Oct 2006 14:34:21 +0100

partman-ext2r0 (1.8) unstable; urgency=low

  [ Colin Watson ]
  * Don't use the mountpoint as a default label (closes: #310754).

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Greek (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Pokharel
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Punjabi (Gurmukhi) (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 14 Jul 2006 02:10:53 +0200

partman-ext2r0 (1.7) unstable; urgency=low

  [ Colin Watson ]
  * Fix mount point sanity check to really reject mount points containing
    spaces.
  * Update GPL notices with the FSF's new address.
  * Use 'rm -f' rather than more awkward test-then-remove constructions.

  [ Joey Hess ]
  * Use log-output.

  [ Martin Michlmayr ]
  * Detect ext2 revision 0 filesystems as ext2r0.  Patch by Anton Zinoviev.
    Closes: #249755.  Requires partman-base 78.

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 16 Jan 2006 21:48:36 +0000

partman-ext2r0 (1.6) unstable; urgency=low

  * Christian Perrier
    - Add a comment about the Choices length for translators
      in the debconf templates file

 -- Christian Perrier <bubulle@debian.org>  Sat, 19 Mar 2005 12:39:15 +0100

partman-ext2r0 (1.5) unstable; urgency=low

  * Colin Watson
    - Move templates to partman-ext2r0.templates for clarity in .pot files.
  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Finnish (fi.po) by Tapio Lehtonen
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Indonesian (id.po) by Arief S Fitrianto
    - Italian (it.po) by Giuseppe Sacco
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov

 -- Thiemo Seufer <ths@debian.org>  Fri, 04 Feb 2005 16:43:26 +0100

partman-ext2r0 (1.4) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Greek (el.po) by Greek Translation Team
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 21 Oct 2004 11:58:00 -0300

partman-ext2r0 (1.3) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 07 Oct 2004 14:07:42 +0100

partman-ext2r0 (1.2) unstable; urgency=low

  * Joey Hess
    - Fixes for preseeding: Remove seen flag unsetting and reset questions
      only after they're first asked. (This is not complete, I see some places
      where question aswers are forced, which will also break preseeding.)
  * Updated translations:
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Italian (it.po) by Stefano Canepa
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Norwegian (nb.po) by Axel Bojer
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovenian (sl.po) by Jure Čuhalev
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 28 Sep 2004 14:38:54 +0100

partman-ext2r0 (1.1) unstable; urgency=low

  * Anton Zinoviev
    - use the scripts get_mountoptions and select_mountoptions
    - templates: remove one unused template for mount options - use
      capital letter in the long name of the file system ("old Ext2
      (revision 0) file system") to be consistent with the names of the
      other file systems. Thanks to Changwoo Ryu.
  * Updated translations:
    - Arabic (ar.po) by Christian Perrier
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Japanese (ja.po) by Kenshi Muto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjørn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 30 Aug 2004 19:27:21 +0100

partman-ext2r0 (1.0) unstable; urgency=low

  * Updated translations:
    - Greek (el.po) by George Papamichelakis
    - Norwegian (nb.po) by Knut Yrvin
    - Swedish (sv.po) by Per Olofsson

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Jul 2004 13:54:46 +0100

partman-ext2r0 (0.05) unstable; urgency=low

  * Martin Michlmayr
    - Fix a critical cut&paste error introduced in the latest mega partman
      change which results in a hang.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 23 Jul 2004 02:48:19 +0100

partman-ext2r0 (0.04) unstable; urgency=low

  * Anton Zinoviev
    - disable the "noexec" mount option for the file system mounted on
      /tmp; thanks to Mika Bostrom, Stephen Touset and Ken Schweigert
      (closes: #249322, #255135, #258117)
    - active_partition/ext2r0/*, commit.d/format_ext2r0: support for labels
  * Joey Hess
    - don't disable swap after formatting
  * Updated translations:
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Bosnian (bs.po) by Safir Šećerović
    - German (de.po) by Dennis Stampfer
    - fa (fa.po) by n
    - Finnish (fi.po) by Tapio Lehtonen
    - Hebrew (he.po) by Lior Kaplan
    - hr (hr.po) by Kruno
    - Korean (ko.po) by Changwoo Ryu
    - Norwegian (nn.po) by Håvard Korsvoll
    - Portuguese (pt.po) by Miguel Figueiredo
    - Albanian (sq.po) by Elian Myftiu
    - Turkish (tr.po) by Recai Oktaş

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 22 Jul 2004 12:04:17 +0100

partman-ext2r0 (0.03) unstable; urgency=low

  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by George Papamichelakis
    - Finnish (fi.po) by Tapio Lehtonen
    - Gallegan (gl.po) by Héctor Fernández López
    - Italian (it.po) by Stefano Canepa
    - Norwegian (nb.po) by Knut Yrvin
    - Norwegian (nn.po) by Håvard Korsvoll
    - Romanian (ro.po) by Eddy Petrisor
    - Slovenian (sl.po) by Jure Čuhalev
    - Traditional Chinese (zh_TW.po) by Tetralet
    - Albanian (sq.po) by Elian Myftiu

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 26 May 2004 21:40:24 -0300

partman-ext2r0 (0.02) unstable; urgency=low

  * Martin Michlmayr
    - Add the following checks:
        o The boot partition has to be old ext2 (revision 0)
        o The boot file system has to be the first primary partition
        o The root partition has to be a primary partition
      This addresses most of #244100.
    - Add missing partman-ext2r0/text/options debconf message.
  * Updated translations:
    - Czech (cs.po) by Miroslav Kure
    - Danish (da.po) by Claus Hindsgaul
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - French (fr.po) by Christian Perrier
    - Hungarian (hu.po) by VERÓK István
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Dutch (nl.po) by Bart Cornelis
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Martin Michlmayr <tbm@cyrius.com>  Wed, 19 May 2004 00:57:11 +0100

partman-ext2r0 (0.01) unstable; urgency=low

  * New package partman-ext2r0 based on partman-ext3 which adds support
    for the ext2 revision 0 file system needed by some old boot loaders
    to boot from (e.g. on MIPS based Cobalt and some ARM based Netwinder
    machine).
  * Martin Michlmayr
    - Put this file system after more important file systems in the list,
      but before FAT.
    - Use /boot as the default mount point.
    - Make the package arch: arm mipsel and only provide it for
      arm/netwinder and mipsel/cobalt.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 15 May 2004 02:49:51 +0100

