<!-- retain these comments for translator revision tracking -->
<!-- original version: 45435 -->


  <sect2 arch="m68k">
  <!-- <title>Choosing an Installation Method</title> -->
  <title>Scelta del metodo d'installazione</title>
<para>

<!--
Some &arch-title; subarchs have the option of booting using either a
2.4.x or 2.2.x linux kernel. When such a choice exists, try the 2.4.x
linux kernel. The installer should also require less memory when using
a 2.4.x linux kernel as 2.2.x support requires a fixed-sized ramdisk
and 2.4.x uses tmpfs.
-->

Alcune delle sottoarchitetture di &arch-title; hanno la possibilità di
usare un kernel Linux 2.4.x o 2.2.x. Nei casi in cui questa scelta è
possibile si provi prima il kernel 2.4.x, infatti l'installatore richiede
meno memoria con un kernel 2.4.x e inoltre un kernel 2.2.x richiede un
ramdisk di dimensione prefissata invece, il 2.4.x, supporta tmpfs.

</para><para>

<!--
If you are using a 2.2.x linux kernel, then you need to use the &ramdisksize;
kernel parameter.
-->

Se si utilizza un kernel Linux 2.2 allora è necessario usare il parametro
del kernel &ramdisksize;.

</para><para>

<!--
Make sure <userinput>root=/dev/ram</userinput> is one of your kernel
parameters.
-->

Assicurarsi che fra i parametri del kernel sia presente
<userinput>root=/dev/ram</userinput>.

</para><para>

<!--
If you're having trouble, check
<ulink url="&url-m68k-cts-faq;">cts's &arch-title; debian-installer FAQ</ulink>.
-->

Se si riscontrano dei problemi si consultino le
<ulink url="&url-m68k-cts-faq;">cts's &arch-title; debian-installer
FAQ</ulink>.

</para>

<itemizedlist>
	<listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-atari"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-bvme6000"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-mac"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-mvme"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-q40"/></para></listitem>
</itemizedlist>


   <sect3 id="m68k-boot-amiga">
   <!-- <title>Amiga</title> -->
   <title>Amiga</title>
<para>

<!--
The only method of installation available to amiga is the hard drive
(see <xref linkend="m68k-boot-hd"/>).
<emphasis>In other words the cdrom is not bootable.</emphasis>
-->

L'unico metodo d'installazione disponibile per amiga è dal disco fisso
(si consulti <xref linkend="m68k-boot-hd"/>). <emphasis>In altre parole
il cdrom non è avviabile.</emphasis>

</para><para>

<!--
Amiga does not currently work with bogl, so if
you are seeing bogl errors, you need to include the boot parameter
<userinput>fb=false</userinput>.
-->

Attualmente gli Amiga non funzionano con bogl quindi se si verificano
degli errori che riguardano bogl si deve inserire il parametro di avvio
<userinput>fb=false</userinput>.

</para>
   </sect3>

   <sect3 id="m68k-boot-atari">
   <!-- <title>Atari</title> -->
   <title>Atari</title>
<para>

<!--
The installer for atari may be started from either the hard
drive (see <xref linkend="m68k-boot-hd"/>) or from floppies
(see <xref linkend="boot-from-floppies"/>).
<emphasis>In other words the cdrom is not bootable.</emphasis>
-->

L'installatore per atari può essere avviato usando il disco fisso
(<xref linkend="m68k-boot-hd"/>) o i dischetti
(<xref linkend="boot-from-floppies"/>). <emphasis>In altre parole
il cdrom non è avviabile.</emphasis>

</para><para>

<!--
Atari does not currently work with bogl, so if
you are seeing bogl errors, you need to include the boot parameter
<userinput>fb=false</userinput>.
-->

Attualmente gli Atari non funzionano con bogl quindi se si verificano
degli errori che riguardano bogl si deve inserire il parametro di avvio
<userinput>fb=false</userinput>.

</para>
   </sect3>

   <sect3 id="m68k-boot-bvme6000">
   <!-- <title>BVME6000</title> -->
   <title>BVME6000</title>
<para>

<!--
The installer for BVME6000 may be started from a cdrom
(see <xref linkend="m68k-boot-cdrom"/>), floppies
(see <xref linkend="boot-from-floppies"/>), or the net
(see <xref linkend="boot-tftp"/>).
-->

L'installatore per BVME6000 può essere avviato da un CD-ROM
(<xref linkend="m68k-boot-cdrom"/>), dai dischetti
(<xref linkend="boot-from-floppies"/>) o dalla rete
(<xref linkend="boot-tftp"/>).

</para>
   </sect3>


   <sect3 id="m68k-boot-mac">
   <!-- <title>Macintosh</title> -->
   <title>Macintosh</title>
<para>

<!--
The only method of installation available to mac is from
the hard drive (see <xref linkend="m68k-boot-hd"/>).
<emphasis>In other words the cdrom is not bootable.</emphasis>
Macs do not have a working 2.4.x kernel.
-->

L'unico metodo d'installazione disponibile per mac è dal disco fisso
(si consulti <xref linkend="m68k-boot-hd"/>). <emphasis>In altre parole
il cdrom non è avviabile.</emphasis>

</para><para>

<!--
If your hardware uses a 53c9x-based scsi bus, then you may need to
include the kernel parameter <userinput>mac53c9x=1,0</userinput>.
Hardware with two such scsi buses, such as the Quadra 950, will need
<userinput>mac53c9x=2,0</userinput> instead. Alternatively, the
parameter can be specified as <userinput>mac53c9x=-1,0</userinput>
which will leave autodetection on, but which will disable SCSI
disconnects. Note that specifying this parameter is only necessary
if you have more than one hard disk; otherwise, the system will run
faster if you do not specify it.
-->

Se il proprio hardware dispone di un bus SCSI basato su 53c9x allora
potrebbe essere necessario includere il parametro del kernel
<userinput>mac53c9x=1,0</userinput>. Con l'hardware con due bus SCSI
di questo tipo, come il Quadra 950, invece si deve usare
<userinput>mac53c9x=2,0</userinput>. In alternativa si può inserire
<userinput>mac53c9x=-1,0</userinput> che attiva il riconoscimento
automatico ma che disattiva la disconnessioni SCSI. Si noti che la
specifica di questo parametro è necessaria solo se si ha più di un
disco fisso e che il sistema risulta più veloce se non lo si inserisce.

</para>
   </sect3>


   <sect3 id="m68k-boot-mvme">
   <!-- <title>MVME147 and MVME16x</title> -->
   <title>MVME147 e MVME16x</title>
<para>

<!--
The installer for MVME147 and MVME16x may be started from
either floppies (see <xref linkend="boot-from-floppies"/>)
or the net (see <xref linkend="boot-tftp"/>).
<emphasis>In other words the cdrom is not bootable.</emphasis>
-->

L'installatore per MVME147 e MVME16x può essere avviato usando i
dischetti (<xref linkend="boot-from-floppies"/>) o dalla rete
(<xref linkend="boot-tftp"/>). <emphasis>In altre parole il cdrom non
è avviabile.</emphasis>

</para>
   </sect3>


   <sect3 id="m68k-boot-q40">
   <!-- <title>Q40/Q60</title> -->
   <title>Q40/Q60</title>
<para>

<!--
The only method of installation available to Q40/Q60 is
from the hard drive (see <xref linkend="m68k-boot-hd"/>).
<emphasis>In other words the cdrom is not bootable.</emphasis>
-->

L'unico metodo d'installazione disponibile per Q40/Q60 è dal disco fisso
(si consulti <xref linkend="m68k-boot-hd"/>). <emphasis>In altre parole
il cdrom non è avviabile.</emphasis>

</para>
   </sect3>
  </sect2>


  <sect2 arch="m68k" id="m68k-boot-hd">
  <!-- <title>Booting from a Hard Disk</title> -->
  <title>Avvio da disco fisso</title>

&boot-installer-intro-hd.xml;

<para>

<!--
At least six different ramdisks may be used to boot from the hard
drive, three different types each with and without support for a
2.2.x linux kernel (see
<ulink url="&disturl;/main/installer-&architecture;/current/images/MANIFEST">MANIFEST</ulink>
for details).
-->

Per l'avvio da disco fisso si possono usare almeno sei ramdisk diversi,
tre tipi diversi ognuno dei quali con e senza il supporto per il kernel
Linux 2.2.x (si veda
<ulink url="&disturl;/main/installer-&architecture;/current/images/MANIFEST">MANIFEST</ulink>
per i dettagli).

</para><para>

<!--
The three different types of ramdisks are <filename>cdrom</filename>,
<filename>hd-media</filename>, and <filename>nativehd</filename>. These
ramdisks differ only in their source for installation packages.
The <filename>cdrom</filename> ramdisk uses a cdrom to get
debian-installer packages. The <filename>hd-media</filename> ramdisk
uses an iso image file of a cdrom currently residing on a hard disk.
Finally, the <filename>nativehd</filename> ramdisk uses the net to
install packages.
-->

I tre diversi tipi di ramdisk sono <filename>cdrom</filename>,
<filename>hd-media</filename> e <filename>nativehd</filename>. Questi
ramdisk differiscono solo nei sorgenti dei pacchetti per l'installazione.
Il ramdisk <filename>cdrom</filename> usa un CD-ROM per procurarsi i
pacchetti dell'Installatore Debian, il ramdisk <filename>hd-media</filename>
usa un file con l'immagine iso di un CD-ROM che risiede su un disco fisso
e infine il ramdisk <filename>nativehd</filename> usa la rete per
l'installazione dei pacchetti.

</para>

<itemizedlist>
	<listitem><para><xref linkend="m68k-boothd-amiga"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-atari"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-mac"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-q40"/></para></listitem>
</itemizedlist>


   <sect3 id="m68k-boothd-amiga">
   <!-- <title>Booting from AmigaOS</title> -->
   <title>Avvio da AmigaOS</title>
<para>

<!--
In the <command>Workbench</command>, start the Linux installation
process by double-clicking on the <guiicon>StartInstall</guiicon> icon
in the <filename>debian</filename> directory.
-->

Dal <command>Workbench</command>, avviare il processo d'installazione di
Linux facendo doppio clic sull'icona <guiicon>StartInstall</guiicon> nella
directory <filename>debian</filename>.

</para><para>

<!--
You may have to press the &enterkey; key twice after the Amiga
installer program has output some debugging information into a window.
After this, the screen will go grey, there will be a few seconds'
delay. Next, a black screen with white text should come up, displaying
all kinds of kernel debugging information.  These messages may scroll
by too fast for you to read, but that's OK.  After a couple of
seconds, the installation program should start automatically, so you
can continue down at <xref linkend="d-i-intro"/>.
-->

Potrebbe essere necessario premere &enterkey; due volte, dopo di che il
programma d'installazione per Amiga emetterà delle informazioni di
debug in una finestra. Dopo di ciò lo schermo si dovrebbe oscurare per
alcuni secondi, per poi mostrare informazioni varie di debug del kernel
come testo bianco su sfondo nero. Questi messaggi potrebbero scorrere
troppo velocemente per riuscire a leggerli, ma non ci si dove preoccupare.
Dopo un paio di secondi, il programma di installazione dovrebbe partire in
automatico. Si può proseguire a leggere <xref linkend="d-i-intro"/>.

</para>
   </sect3>


   <sect3 id="m68k-boothd-atari">
   <!-- <title>Booting from Atari TOS</title> -->
   <title>Avvio da Atari TOS</title>
<para>

<!--
At the GEM desktop, start the Linux installation process by
double-clicking on the <guiicon>bootstra.prg</guiicon> icon in the
<filename>debian</filename> directory and clicking
<guibutton>Ok</guibutton> at the program options dialog box.
-->

Dal desktop GEM avviare il processo d'installazione di Linux facendo
doppio clic sull'icona <guiicon>bootstra.prg</guiicon> contenuta nella
directory <filename>debian</filename> e poi nella finestra di dialogo
con le opzioni del programma fare clic su <guibutton>Ok</guibutton>.

</para><para>

<!--
You may have to press the &enterkey; key after the Atari
bootstrap program has output some debugging information into a
window. After this, the screen will go grey, there will be a few
seconds' delay.  Next, a black screen with white text should come up,
displaying all kinds of kernel debugging information. These messages
may scroll by too fast for you to read, but that's OK. After a couple
of seconds, the installation program should start automatically, so
you can continue below at <xref linkend="d-i-intro"/>.
-->

Potrebbe essere necessario premere &enterkey; una volta, dopo di che il
programma d'installazione per Atari emetterà delle informazioni di
debug in una finestra. Dopo di ciò lo schermo si dovrebbe oscurare per
alcuni secondi, per poi mostrare informazioni varie di debug del kernel
come testo bianco su sfondo nero. Questi messaggi potrebbero scorrere
troppo velocemente per riuscire a leggerli, ma non ci si dove preoccupare.
Dopo un paio di secondi, il programma di installazione dovrebbe partire in
automatico. Si può proseguire a leggere <xref linkend="d-i-intro"/>.

</para>
   </sect3>


   <sect3 id="m68k-boothd-mac">
   <!-- <title>Booting from MacOS</title> -->
   <title>Avvio da MacOS</title>
<para>

<!--
You must retain the original Mac system and
boot from it. It is <emphasis>essential</emphasis> that, when booting
MacOS in preparation for booting the Penguin linux loader, you
hold the <keycap>shift</keycap> key down to prevent extensions from
loading. If you don't use MacOS except for loading linux, you can
accomplish the same thing by removing all extensions and control
panels from the Mac's System Folder. Otherwise extensions may be left
running and cause random problems with the running linux kernel.
-->

Si deve necessario conservare il sistema Mac originale e usarlo per
l'avvio. È <emphasis>essenziale</emphasis> per preparare l'avvio di
MacOS per far partire bootloader Penguin, si deve terne premuto il tasto
<keycap>shift</keycap> per impedire il caricamento delle estensioni.
Se si usa MacOS solo per caricare Linux allora è possibile rimuovere
tutte le estensioni e i pannelli di controllo dal System Folder, infatti
le estensioni lasciate attive potrebbero causare dei problemi durante
l'esecuzione di Linux.

</para><para>

<!--
Macs require the <command>Penguin</command> bootloader, which can be
downloaded from <ulink url="&url-m68k-mac;">the Linux/mac68k
sourceforge.net project</ulink>. If you do not have the tools to handle
a <command>Stuffit</command> archive, you can put it on a
MacOS-formatted floppy using a second GNU/Linux machine of any
architecture and the <command>hmount</command>,
<command>hcopy</command>, and <command>humount</command> tools from the
<classname>hfsutils</classname> suite.
-->

I Mac richiedono l'uso del bootloader <command>Penguin</command>, questo
bootloader può essere scaricato dal <ulink url="&url-m68k-mac;">progetto
Linux/mac68k su sourceforge.net</ulink>. Se non si dispone di programmi
per gestire gli archivi <command>Stuffit</command> si può inserire un
dischetto formattato con MacOS in una qualsiasi altra macchina con
GNU/Linux e poi utilizzare i comandi <command>hmount</command>,
<command>hcopy</command> e <command>humount</command> contenuti nel
pacchetto <classname>hfsutils</classname>.

</para><para>

<!--
At the MacOS desktop, start the Linux installation process by
double-clicking on the <guiicon>Penguin Prefs</guiicon> icon in
the <filename>Penguin</filename> directory. The
<command>Penguin</command> booter will start up. Go to the
<guimenuitem>Settings</guimenuitem> item in the
<guimenu>File</guimenu> menu, click the
<guilabel>Kernel</guilabel> tab.  Select the kernel
(<filename>vmlinuz</filename>) and ramdisk
(<filename>initrd.gz</filename>) images in the
<filename>install</filename> directory by clicking on the corresponding
buttons in the upper right corner, and navigating the file select
dialogs to locate the files.
-->

Dal desktop di MacOS avviare il processo di installazione di Linux
facendo doppio clic sull'icona <guiicon>Penguin Prefs</guiicon> nella
directory <filename>Penguin</filename>, facendo così partire il
bootloader. Selezionare le voce <guimenuitem>Settings</guimenuitem> nel
menu <guimenuitem>File</guimenuitem> e poi fare clic sulla linguetta
<guilabel>Kernel</guilabel>. Scegliere le immagini del kernel
(<filename>vmlinuz</filename>) e del ramdisk (<filename>initrd.gz</filename>)
nella directory <filename>install</filename>, cliccando sui pulsanti
corrispondenti nell'angolo in alto a destra e navigando nei dialoghi di
selezione file per trovarli.

</para><para>

<!--
To set the boot parameters in Penguin, choose <guimenu>File</guimenu> -&gt;
<guimenuitem>Settings...</guimenuitem>, then switch to the
<guilabel>Options</guilabel> tab.  Boot parameters may be typed in to
the text entry area.  If you will always want to use these settings,
select <guimenu>File</guimenu> -&gt; <guimenuitem>Save Settings as
Default</guimenuitem>.
-->

Per impostare i parametri di avvio scegliere <guimenu>File</guimenu> -&gt;
<guimenuitem>Settings...</guimenuitem> e poi passare nella linguetta
<guilabel>Options</guilabel>, i parametri possono essere inseriti
nell'apposito spazio. Se si vogliono usare sempre questi parametri si
deve scegliere <guimenu>File</guimenu> -&gt; <guimenuitem>Save Settings as
Default</guimenuitem>.

</para><para>

<!--
Close the <guilabel>Settings</guilabel>
dialog, save the settings and start the bootstrap using the
<guimenuitem>Boot Now</guimenuitem> item in the
<guimenu>File</guimenu> menu.
-->

Chiudere la finestra di dialogo <guilabel>Settings</guilabel>, salvare
le impostazioni e avviare il bootstrap con la voce
<guimenuitem>Boot Now</guimenuitem> del menu <guimenu>File</guimenu>.

</para><para>

<!--
The <command>Penguin</command> booter will output some debugging
information into a window. After this, the screen will go grey, there
will be a few seconds' delay. Next, a black screen with white text
should come up, displaying all kinds of kernel debugging
information. These messages may scroll by too fast for you to read,
but that's OK. After a couple of seconds, the installation program
should start automatically, so you can continue below at
<xref linkend="d-i-intro"/>.
-->

Il boot loader <command>Penguin</command> emetterà delle informazioni di
debug in una finestra. Dopo di ciò lo schermo si dovrebbe oscurare per
alcuni secondi, per poi mostrare informazioni varie di debug del kernel
come testo bianco su sfondo nero. Questi messaggi potrebbero scorrere
troppo velocemente per riuscire a leggerli, ma non ci si dove preoccupare.
Dopo un paio di secondi, il programma di installazione dovrebbe partire in
automatico. Si può proseguire a leggere <xref linkend="d-i-intro"/>.

</para>
   </sect3>


   <sect3 id="m68k-boothd-q40">
   <!-- <title>Booting from Q40/Q60</title> -->
   <title>Avvio da Q40/Q60</title>
<para>

FIXME

</para><para>

<!--
The installation program should start automatically, so you can
continue below at <xref linkend="d-i-intro"/>.
-->

Il programma di installazione dovrebbe partire automaticamente e
quindi si può continuare con <xref linkend="d-i-intro"/>.

</para>
   </sect3>
  </sect2>


  <sect2 arch="m68k" id="m68k-boot-cdrom">
  <!-- <title>Booting from a CD-ROM</title> -->
  <title>Avvio da CD-ROM</title>
<para>

<!--
Currently, the only &arch-title; subarchitecture that
supports CD-ROM booting is the BVME6000.
-->

Attualmente l'unica sottoarchitettura di &arch-title; che supporta
l'avvio da CD-ROM è la BVME6000.

</para>

&boot-installer-intro-cd.xml;

  </sect2>


  <sect2 arch="m68k" id="boot-tftp">
  <!-- <title>Booting with TFTP</title> -->
  <title>Avvio con TFTP</title>

&boot-installer-intro-net.xml;

<para>

<!--
After booting the VMEbus systems you will be presented with the LILO
<prompt>Boot:</prompt> prompt.  At that prompt enter one of the
following to boot Linux and begin installation proper of the Debian
software using vt102 terminal emulation:
-->

Dopo l'avvio sui sistemi VMEbus viene presentato il prompt di LILO
<prompt>Boot:</prompt>. Adesso si deve inserire una delle seguenti
stringhe per avviare Linux e iniziare l'installazione di Debian usando
l'emulazione del terminale vt102 corretta:

<!-- Because the &enterkey; definition uses <keycap>,    -->
<!-- we use <screen> instead of <userinput> in this list -->

<itemizedlist>
<listitem><para>

<!--
type <screen>i6000 &enterkey;</screen> to install a BVME4000/6000
-->

inserire <screen>i6000 &enterkey;</screen> per installare su BVME4000/6000

</para></listitem>
<listitem><para>

<!--
type <screen>i162 &enterkey;</screen> to install an MVME162
-->

inserire <screen>i162 &enterkey;</screen> per installare su MVME162

</para></listitem>
<listitem><para>

<!--
type <screen>i167 &enterkey;</screen> to install an MVME166/167
-->

inserire <screen>i167 &enterkey;</screen> per installare su MVME166/167

</para></listitem>
</itemizedlist>

</para><para>

<!--
You may additionally append the string
<screen>TERM=vt100</screen> to use vt100 terminal emulation,
e.g., <screen>i6000 TERM=vt100 &enterkey;</screen>.
-->

Si può aggiungere la stringa <screen>TERM=vt100</screen> per usare
l'emulazione del terminale vt100, per esempio <screen>i6000 TERM=vt100
&enterkey;</screen>.

</para>
  </sect2>


  <sect2 arch="m68k" id="boot-from-floppies">
  <!-- <title>Booting from Floppies</title> -->
  <title>Avvio da dischetti</title>
<para>

<!--
For most &arch-title; architectures, booting from a local filesystem is the
recommended method.
-->

L'avvio da un file system locale è il metodo d'avvio raccomandato per
molte delle architetture &arch-title;.

</para><para>

<!--
Booting from the boot floppy is supported only for Atari and VME
(with a SCSI floppy drive on VME) at this time.
-->

Attualmente l'avvio tramite dischetti è supportato solo su Atari e VME
(purché con un lettore per dischetti SCSI).

</para>
  </sect2>
