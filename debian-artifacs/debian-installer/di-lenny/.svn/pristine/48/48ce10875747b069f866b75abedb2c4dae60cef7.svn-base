<!-- retain these comments for translator revision tracking -->
<!-- original version: 56425 -->

 <sect1 condition="supports-tftp" id="install-tftp">
 <title>Preparació dels fitxers per a l'arrencada en xarxa TFTP</title>
<para>

Si teniu una màquina connectada a una xarxa d'àrea local, podeu
arrencar-la des d'una altra màquina de la xarxa fent servir TFTP. Si
voleu arrencar el sistema d'instal·lació des d'una altra màquina, els
fitxers d'arrencada hauran de col·locar-se a llocs específics d'aquesta,
la qual ha de configurar-se a fi de permetre l'arrencada de la vostra.

</para><para>

Heu d'instal·lar un servidor TFTP, i en nombroses màquines, cal també un
servidor DHCP
<phrase condition="supports-rarp">, o un servidor RARP</phrase>
<phrase condition="supports-bootp">, o un servidor BOOTP</phrase>.

</para><para>

<phrase condition="supports-rarp">El protocol de resolució inversa
d'adreces (RARP) és una manera d'indicar al vostre client les adreces
IP que ha d'emprar. Una altra manera és fer servir el protocol BOOTP.</phrase>

<phrase condition="supports-bootp">BOOTP és un protocol IP que
indica a l'ordinador la seua pròpia adreça IP i el lloc de la xarxa
on obtenir la imatge d'arrencada.</phrase>

<phrase arch="m68k">Hi ha una altra alternativa en sistemes VMEbus:
l'adreça IP es pot configurar manualment en una ROM d'arrencada.</phrase>

El DHCP (Protocol de configuració
dinàmica d'ordinadors centrals) és una extensió més flexible, i
compatible, del BOOTP. Alguns sistemes sols es poden configurar fent
servir el DHCP.

</para><para arch="powerpc">

Per als PowerPC, si teniu una màquina NewWorld Power Macintosh, és bona
idea emprar el DHCP en lloc del BOOTP. Algunes de les màquines més noves
no poden arrencar si empreu el BOOTP.

</para><para arch="alpha">

A diferència de l'Open Firmware de màquines Sparc i PowerPC, la consola
SRM <emphasis>no</emphasis> fa servir el RARP per obtenir les adreces
IP, i per tant heu d'emprar el BOOTP per arrencar en xarxa l'Alpha<footnote>

<para>
Els sistemes Alpha també es poden arrencar en xarxa amb el DECNet MOP
(Protocol d'operacions de manteniment), però això no s'explica aquí.
Probablement el vostre operador OpenVMS local estarà disposat a
ajudar-vos en cas que realment tingueu necessitat d'emprar el MOP per
arrencar Linux al vostre Alpha.
</para>

</footnote>. També podeu introduir la configuració IP de les interfícies
de xarxa directament en la consola SRM.

</para><para arch="hppa">

Algunes màquines HPPA més antigues (p.ex. 715/75) usen l'RBOOTD en lloc
del BOOTP. Hi ha un paquet <classname>rbootd</classname> a l'abast en
Debian.

</para><para>

El protocol de transferència trivial de fitxers (TFTP) s'empra per posar
a disposició del client la imatge d'arrencada. En teoria, es pot usar
qualsevol servidor de qualsevol plataforma que implemente aquests
protocols. Els exemples d'aquesta secció ofereixen instruccions per al
SunOS 4.x, el SunOS 5.x (i.e. Solaris), i el GNU/Linux.

<note arch="x86"><para>

El mètode PXE d'arrencada TFTP, o entorn d'execució d'arrencada prèvia,
requereix un servidor TFTP que permeta l'ús de <userinput>tsize</userinput>.
En un servidor &debian;, els paquets <classname>atftpd</classname> i
<classname>tftpd-hpa</classname> són adients; és recomanable el
<classname>tftpd-hpa</classname>.

</para></note>

</para>

&tftp-rarp.xml;
&tftp-bootp.xml;
&tftp-dhcp.xml;

  <sect2 id="tftpd">
  <title>Habilitació del servidor TFTP</title>
<para>

Per a posar en marxa el servidor TFTP, assegureu-vos primer que teniu
activat el <command>tftpd</command>. Per fer-ho, el fitxer
<filename>/etc/inetd.conf</filename> ha de tenir quelcom semblant a la
línia següent:

<informalexample><screen>
tftp dgram udp wait nobody /usr/sbin/tcpd in.tftpd /tftpboot
</screen></informalexample>

Els paquets Debian solen configurar-ho per defecte correctament.

</para>
<note><para>

Històricament, els servidors TFTP utilitzen el directori
<filename>/tftpboot</filename> per servir les imatges. Per altra banda,
els paquets de &debian; poden utilitzar altres directoris per complir
amb l'<ulink url="&url-fhs-home;">Estàndard de la Jerarquia del Sistema
de Fitxers</ulink>. Per exemple, el <classname>tftpd-hpa</classname>
utilitza per defecte <filename>/var/lib/tftpboot</filename>. Es possible
que hagueu d'ajustar els exemples de configuració d'aquesta secció en
conseqüència.

</para></note>
<para>

Vegeu el fitxer <filename>/etc/inetd.conf</filename> i recordeu el directori
que s'utilitza com a paràmetre del <command>in.tftpd</command><footnote>

<para>
Totes les alternatives de l'<command>in.tftpd</command> que hi ha a Debian
per defecte haurien d'enregistrar les peticions TFTP al registre del
sistema. Algunes d'elles suporten un paràmetre <userinput>-v</userinput>
per mostrar una sortida més detallada.
Es recomana que comproveu aquests missatges registrats en el cas que patiu
problemes d'arrencada com a punt d'inici al diagnòstic de la causa dels 
problemes.
</para>

</footnote>; ho necessitareu després.
Si heu canviat <filename>/etc/inetd.conf</filename>, haureu de notificar-ho
al procés <command>inetd</command> que s'està executant. En màquines Debian,
executeu <userinput>/etc/init.d/inetd reload</userinput>; en d'altres,
esbrineu la identitat (ID) del procés <command>inetd</command> i executeu
<userinput>kill -HUP <replaceable>pid-d'inetd</replaceable></userinput>.

</para><para arch="mips">

Si aneu a instal·lar Debian en una màquina SGI i el servidor TFTP empra
GNU/Linux amb Linux 2.4, heu de posar això al servidor:

<informalexample><screen>
# echo 1 &gt; /proc/sys/net/ipv4/ip_no_pmtu_disc
</screen></informalexample>

per desactivar la funcionalitat Path MTU discovery, altrament la PROM
de SGI no podrà descarregar el nucli. A més, assegureu-vos que els
paquets TFTP s'envien des d'un port origen no major que el 32767, o la
descàrrega s'estancarà després del primer paquet. Per evitar aquest
error de la PROM amb el Linux 2.4.X, feu el següent,

<informalexample><screen>
# echo "2048 32767" &gt; /proc/sys/net/ipv4/ip_local_port_range
</screen></informalexample>

a fi d'ajustar el rang de ports origen que empra el servidor TFTP de Linux.

</para>
  </sect2>

  <sect2 id="tftp-images">
  <title>Posar les imatges del TFTP al seu lloc</title>
<para>

Després, col·loqueu la imatge d'arrencada TFTP que us calga, i que figura
a <xref linkend="where-files"/>, al directori d'imatges d'arrencada del
<command>tftpd</command>. Haureu de fer un enllaç des d'aquest
fitxer al fitxer que emprarà el <command>tftpd</command> per arrencar un
client determinat. Malauradament, el nom del fitxer el determina el client
TFTP, i no hi existeix cap norma ben establerta.

</para><para arch="powerpc">

En màquines NewWorld Power Macintosh, heu d'establir el carregador
d'arrencada <command>yaboot</command> com a imatge d'arrencada TFTP.
Amb això el <command>yaboot</command> enviarà les imatges del nucli i
el RAMdisk a través del TFTP mateix. Necessitareu descarregar els fitxers
següents des del directori <filename>netboot/</filename>:

<itemizedlist>
<listitem><para>

<filename>vmlinux</filename>

</para></listitem>
<listitem><para>

<filename>initrd.gz</filename>

</para></listitem>
<listitem><para>

<filename>yaboot</filename>

</para></listitem>
<listitem><para>

<filename>yaboot.conf</filename>

</para></listitem>
<listitem><para>

<filename>boot.msg</filename>

</para></listitem>
</itemizedlist>

</para><para arch="x86">

Per a arrencar amb PXE, sols cal el tarball
<filename>netboot/netboot.tar.gz</filename>. Simplement extraieu-lo
al directori d'imatge d'arrencada del <command>tftpd</command>.
Assegureu-vos que el servidor dhcp està configurat per passar
<filename>pxelinux.0</filename> al <command>tftpd</command> com a
nom del fitxer d'arrencada.

</para><para arch="ia64">

Per a arrencar amb PXE, sols cal el tarball
<filename>netboot/netboot.tar.gz</filename>. Simplement extraieu-lo al
directori d'imatge d'arrencada <command>tftpd</command>. Assegureu-vos
que el servidor dhcp està configurat per passar
<filename>/debian-installer/ia64/elilo.efi</filename> al
<command>tftpd</command> com a nom del fitxer d'arrencada.

</para>

   <sect3 arch="alpha">
   <title>Arrencada TFTP dels Alpha</title>
<para>

En els Alpha, heu d'especificar el nom del fitxer (com un camí relatiu al
directori de la imatge d'arrencada) amb l'argument
<userinput>-file</userinput> de l'ordre <userinput>boot</userinput> a l'SRM,
o bé ajustant la variable d'entorn <userinput>BOOT_FILE</userinput>. També
es pot proporcionar el nom del fitxer a través del BOOTP (en ISC
<command>dhcpd</command>, feu servir la directriu
<userinput>filename</userinput>). A diferència de l'Open Firmware, en SRM
<emphasis>no hi ha nom del fitxer per defecte</emphasis>, per tant
<emphasis>heu</emphasis> d'especificar un nom de fitxer amb algun d'aquests
mètodes.

</para>
   </sect3>

   <sect3 arch="sparc">
   <title>Arrencada TFTP de l'SPARC</title>
<para>

Algunes arquitectures SPARC afegeixen els noms de la subarquitectura,
com ara <quote>SUN4M</quote> o <quote>SUN4C</quote> al fitxer. Es a dir,
si la subarquitectura del vostre sistema és un SUN4C, i la seva IP
és 192.168.1.3, el fitxer serà <filename>C0A80103.SUN4C</filename>.
Però, hi ha també subarquitectures on el fitxer del client és
<filename>ip-del-client-en-hex</filename>. Per a determinar fàcilment el
codi hexadecimal de la adreça IP introduiu l'ordre següent a l'intèrpret
d'ordres (suposant que la IP de la màquina és 10.0.0.4).

<informalexample><screen>
$ printf '%.2x%.2x%.2x%.2x\n' 10 0 0 4
</screen></informalexample>

Per obtenir el nom de fitxer correcte, canvieu totes les lletres a
majúscules i afegiu-hi si cal el nom de la subarquitectura.

</para><para>

Si ho heu fet tot bé, posant l'ordre <userinput>boot net</userinput> a
l'OpenPROM hauria de carregar la imatge. Si la imatge no es troba, proveu
de comprovar els fitxers de registre del vostre servidor tftp per veure
quin és el nom de la imatge que s'ha demanat.

</para><para>

També podeu obligar alguns sistemes sparc a cercar un nom específic
afegint-lo al final de l'ordre d'arrencada de
l'OpenPROM, p.ex. <userinput>boot net my-sparc.image</userinput>. Aquest
ha de ser-hi al directori on cerca el servidor TFTP.

</para>
   </sect3>

   <sect3 arch="m68k">
   <title>Arrencada TFTP dels BVM/Motorola</title>
<para>

En els sistemes BVM i Motorola VMEbus copieu els fitxers
&bvme6000-tftp-files; a <filename>/tftpboot/</filename>.

</para><para>

Després, configureu les ROM d'arrencada o el servidor BOOTP per a carregar
inicialment el fitxers <filename>tftplilo.bvme</filename> o el
<filename>tftplilo.mvme</filename> des del servidor TFTP. Dirigiu-vos
al fitxer <filename>tftplilo.txt</filename> de la subarquitectura per
obtenir informació addicional específica del sistema.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Arrencada TFTP dels SGI</title>
<para>

En màquines SGI, el <command>bootpd</command> proporciona el nom del
fitxer TFTP. Es dóna o com a <userinput>bf=</userinput> en
<filename>/etc/bootptab</filename> o com a opció
<userinput>filename=</userinput> en <filename>/etc/dhcpd.conf</filename>.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>Arrencada TFTP dels Broadcom BCM91250A i BCM91480B</title>
<para>

No cal que configureu el DHCP de manera especial perquè passareu el
camí complet del fitxer per carregar-lo al CFE.

</para>
   </sect3>

  </sect2>

<!-- FIXME: commented out since it seems too old to be usable and a current
            way is not known

  <sect2 id="tftp-low-memory">
  <title>TFTP Installation for Low-Memory Systems</title>
<para>

On some systems, the standard installation RAMdisk, combined with the
memory requirements of the TFTP boot image, cannot fit in memory.  In
this case, you can still install using TFTP, you'll just have to go
through the additional step of NFS mounting your root directory over
the network as well.  This type of setup is also appropriate for
diskless or dataless clients.

</para><para>

First, follow all the steps above in <xref linkend="install-tftp"/>.

<orderedlist>
<listitem><para>

Copy the Linux kernel image on your TFTP server using the
<userinput>a.out</userinput> image for the architecture you are
booting.

</para></listitem>
<listitem><para>

Untar the root archive on your NFS server (can be the same system as
your TFTP server):

<informalexample><screen>
# cd /tftpboot
# tar xvzf root.tar.gz
</screen></informalexample>

Be sure to use the GNU <command>tar</command> (other tar programs, like the
SunOS one, badly handle devices as plain files).

</para></listitem>
<listitem><para>

Export your <filename>/tftpboot/debian-sparc-root</filename> directory
with root access to your client.  E.g., add the following line to
<filename>/etc/exports</filename> (GNU/Linux syntax, should be similar
for SunOS):

<informalexample><screen>
/tftpboot/debian-sparc-root <replaceable>client</replaceable>(rw,no_root_squash)
</screen></informalexample>

NOTE: <replaceable>client</replaceable> is the host name or IP address recognized
by the server for the system you are booting.

</para></listitem>
<listitem><para>

Create a symbolic link from your client IP address in dotted notation
to <filename>debian-sparc-root</filename> in the
<filename>/tftpboot</filename> directory.  For example, if the client
IP address is 192.168.1.3, do

<informalexample><screen>
# ln -s debian-sparc-root 192.168.1.3
</screen></informalexample>

</para></listitem>
</orderedlist>

</para>

  </sect2>

  <sect2 condition="supports-nfsroot">
  <title>Installing with TFTP and NFS Root</title>
<para>

Installing with TFTP and NFS Root is similar to
<xref linkend="tftp-low-memory"/> because you don't want to
load the RAMdisk anymore but boot from the newly created NFS-root file
system.  You then need to replace the symlink to the tftpboot image by
a symlink to the kernel image (for example,
<filename>linux-a.out</filename>).

</para><para>

RARP/TFTP requires all daemons to be running on the same server (the
workstation is sending a TFTP request back to the server that replied
to its previous RARP request).

</para>


  </sect2>
END FIXME -->
 </sect1>
