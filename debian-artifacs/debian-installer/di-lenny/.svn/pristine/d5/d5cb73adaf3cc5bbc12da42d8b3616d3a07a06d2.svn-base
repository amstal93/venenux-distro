# win32-loader po translation to Spanish
# Copyright (C) 2007 Software in the Public Interest, SPI Inc.
# This file is distributed under the same license as the win32-loader package.
#
# Changes:
# - Initial translation
#       Javier Fernández-Sanguino , 2007
# - Reviewers: Fernando Cerezal
#
#
#  Traductores, si no conoce el formato PO, merece la pena leer la 
#  documentación de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   http://www.debian.org/intl/spanish/
#   especialmente las notas y normas de traducción en
#   http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (<debian-l10n-spanish@lists.debian.org>)
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-05-03 00:59+0200\n"
"Last-Translator: Javier Fernández-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: debconf win testing gz loader initrd ini nlf cp\n"
"X-POFile-SpellExtra: bcdedit boot preconfiguracin Multi URL amd\n"
"X-POFile-SpellExtra: LANGSPANISH dkB proxy arch linux exe windows netboot\n"
"X-POFile-SpellExtra: Spanish bootini GB NTFS windowsversion NTLDR\n"
"X-POFile-SpellExtra: preconfiguraci�n BootMgr\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_SPANISH"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp850"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Spanish"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Instalador de Debian"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Spanish.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Cargador del instalador de Debian"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "No se pudo encontrar «win32-loader.ini»."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr ""
"«win32-loader.ini» está incompleto. Contacte con el proveedor de este medio."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Este programa ha detectado que su teclado es del tipo «$0». ¿Es esto "
"correcto?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Por favor, envíe un informe de error con la siguiente información:\n"
"\n"
" - Versión de Windows.\n"
" - Configuración regional.\n"
" - Tipo de teclado real.\n"
" - Tipo de teclado detectado.\n"
"\n"
"Gracias."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"No parece haber suficiente espacio en la unidad $c. Es necesario disponer de "
"al menos 3GB de espacio libre para una instalación de escritorio completa. "
"Puede ignorar este aviso si dispone de un disco o partición independiente "
"para la instalación de Debian o si desea reemplazar Windows completamente."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr ""
"Error: No hay suficiente espacio libre en el disco. Interrumpiendo la "
"instalación."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr ""
"Este programa no puede usarse aún con la versión de Windows $windows_version."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"La versión de Debian que está intentando instalar se ha diseñado para "
"ejecutarse en sistemas modernos de 64 bits. Sin embargo, su ordenador no "
"puede ejecutar programas de 64 bit.\n"
"\n"
"Debe utilizar la versión de 32 bits («i386») de Debian o la versión «Multi-"
"arch» que puede instalar cualquiera de ellas.\n"
"\n"
"El instalador se interrumpirá ahora."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Su ordenador es capaz de ejecutar sistemas operativos modernos de 64 bits. "
"Sin embargo, la versión de Debian que está intentando instalar se ha "
"diseñado para ejecutarse en equipos de 32 bits antiguos.\n"
"Puede continuar con la instalación pero le recomendamos que utilice la "
"versión de 64 bits («amd64») de Debian si desea sacarle el máximo provecho a "
"su sistema, o bien la versión «Multi-arch» que puede instalar cualquiera de "
"los dos.\n"
"\n"
"¿Desea interrumpir la instalación ahora?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Seleccione el modo de instalación:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Modo normal. Recomendado para la mayoría de los usuarios."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Modo experto. Recomendado para los usuarios experimentados que quieren un "
"control total del proceso de instalación."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Seleccione una acción:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Instalar Debian GNU/Linux en este ordenador."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Reparar un sistema de Debian existente (modo de rescate)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Instalador gráfico"

#: win32-loader.c:89
msgid "Text install"
msgstr "Instalador de texto"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Descargando %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Conectando..."

#: win32-loader.c:92
msgid "second"
msgstr "segundo"

#: win32-loader.c:93
msgid "minute"
msgstr "minuto"

#: win32-loader.c:94
msgid "hour"
msgstr "hora"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "s"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) de %dkB a %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (faltan %d %s%s)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Seleccione la versión del instalador de Debian a utilizar:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Publicación estable. Esta opción instalará Debian «estable»."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Compilación diaria. Esta es la versión de desarrollo del instalador de "
"Debian. Instalará Debian «testing» (en pruebas) por omisión pero puede "
"utilizarse para instalar también «estable» o «inestable»."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Se recomienda que consulte los problemas conocidos antes de utilizar la "
"compilación diaria. ¿Desea hacerlo ahora?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Entorno de escritorio:"

#: win32-loader.c:114
msgid "None"
msgstr "Ninguno"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"El cargador del Instalador de Debian se configurará con los parámetros "
"mostrados a configuración. NO cambie ninguno de estos valores a no ser que "
"sepa lo que está haciendo."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Configuración de proxy (sistema:puerto):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Ubicación de «boot.ini»:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "URL base para las imágenes de arranque por red (linux e initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Error"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Error: no se ha podido copiar $0 a $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Generando $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Añadiendo información de preconfiguración a $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Error: no se ha podido ejecutar $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Deshabilitando la compresión NTFS en los ficheros de arranque"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registrando el instalador de Debian en NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registrando el instalador de Debian en BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Error: no se ha podido tratar la salida de «bcdedit.exe»."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr ""
"Error: no se encontró «boot.ini». ¿Es esta la versión $windows_version de "
"Windows?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "AVISO MUY IMPORTANTE\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"Ahora se iniciará la segunda parte del proceso de instalación. El programa "
"reiniciará en modo DOS de Windows y cargará automáticamente el Instalador de "
"Debian después de su confirmación.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Deberá reiniciar para poder continuar con la instalación de Debian. En su "
"siguiente arranque se le preguntará si quiere arrancar Windows o el "
"Instalador de Debian. Elija «Instalador de Debian» si desea continuar con el "
"proceso de instalación.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Se le ofrecerá durante el proceso de instalación la posibilidad de o bien "
"reducir su partición de Windows para poder instalar Debian o reemplazarla "
"completamente. Se le RECOMIENDA ENCARECIDAMENTE, en ambos casos, que haga "
"una copia previa de todos sus datos. Ni los autores de este cargador ni el "
"proyecto Debian tendrá NINGUNA RESPONSABILIDAD si se produce una pérdida de "
"datos.\\n\\nUna vez haya completado su instalación de Debian (y si escoge "
"mantener Windows en su disco), podrá desinstalar el cargador del Instalador "
"de Debian a través de la opción de Windows «Añadir/Eliminar programas» en el "
"Panel de Control."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "¿Desea reiniciar ahora?"

#~ msgid "Debconf preseed line:"
#~ msgstr "Línea de preconfiguración de debconf:"
