# SOME DESCRIPTIVE TITLE.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-02-27 20:31+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: boot-new.xml:5
#, no-c-format
msgid "Booting Into Your New Debian System"
msgstr ""

#. Tag: title
#: boot-new.xml:7
#, no-c-format
msgid "The Moment of Truth"
msgstr ""

#. Tag: para
#: boot-new.xml:8
#, no-c-format
msgid "Your system's first boot on its own power is what electrical engineers call the <quote>smoke test</quote>."
msgstr ""

#. Tag: para
#: boot-new.xml:13
#, no-c-format
msgid "If you did a default installation, the first thing you should see when you boot the system is the menu of the <classname>grub</classname> or possibly the <classname>lilo</classname> bootloader. The first choices in the menu will be for your new Debian system. If you had any other operating systems on your computer (like Windows) that were detected by the installation system, those will be listed lower down in the menu."
msgstr ""

#. Tag: para
#: boot-new.xml:23
#, no-c-format
msgid "If the system fails to start up correctly, don't panic. If the installation was successful, chances are good that there is only a relatively minor problem that is preventing the system from booting Debian. In most cases such problems can be fixed without having to repeat the installation. One available option to fix boot problems is to use the installer's built-in rescue mode (see <xref linkend=\"rescue\"/>)."
msgstr ""

#. Tag: para
#: boot-new.xml:32
#, no-c-format
msgid "If you are new to Debian and Linux, you may need some help from more experienced users. <phrase arch=\"x86\">For direct on-line help you can try the IRC channels #debian or #debian-boot on the OFTC network. Alternatively you can contact the <ulink url=\"&url-list-subscribe;\">debian-user mailing list</ulink>.</phrase> <phrase arch=\"not-x86\">For less common architectures like &arch-title;, your best option is to ask on the <ulink url=\"&url-list-subscribe;\">debian-&arch-listname; mailing list</ulink>.</phrase> You can also file an installation report as described in <xref linkend=\"submit-bug\"/>. Please make sure that you describe your problem clearly and include any messages that are displayed and may help others to diagnose the issue."
msgstr ""

#. Tag: para
#: boot-new.xml:48
#, no-c-format
msgid "If you had any other operating systems on your computer that were not detected or not detected correctly, please file an installation report."
msgstr ""

#. Tag: title
#: boot-new.xml:55
#, no-c-format
msgid "BVME 6000 Booting"
msgstr ""

#. Tag: para
#: boot-new.xml:56
#, no-c-format
msgid "If you have just performed a diskless install on a BVM or Motorola VMEbus machine: once the system has loaded the <command>tftplilo</command> program from the TFTP server, from the <prompt>LILO Boot:</prompt> prompt enter one of:"
msgstr ""

#. Tag: para
#: boot-new.xml:64
#, no-c-format
msgid "<userinput>b6000</userinput> followed by &enterkey; to boot a BVME 4000/6000"
msgstr ""

#. Tag: para
#: boot-new.xml:69
#, no-c-format
msgid "<userinput>b162</userinput> followed by &enterkey; to boot an MVME162"
msgstr ""

#. Tag: para
#: boot-new.xml:74
#, no-c-format
msgid "<userinput>b167</userinput> followed by &enterkey; to boot an MVME166/167"
msgstr ""

#. Tag: title
#: boot-new.xml:86
#, no-c-format
msgid "Macintosh Booting"
msgstr ""

#. Tag: para
#: boot-new.xml:88
#, no-c-format
msgid "Go to the directory containing the installation files and start up the <command>Penguin</command> booter, holding down the <keycap>command</keycap> key. Go to the <userinput>Settings</userinput> dialogue (<keycombo> <keycap>command</keycap> <keycap>T</keycap> </keycombo>), and locate the kernel options line which should look like <userinput>root=/dev/ram ramdisk_size=15000</userinput> or similar."
msgstr ""

#. Tag: para
#: boot-new.xml:98
#, no-c-format
msgid "You need to change the entry to <userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>. Replace the <replaceable>yyyy</replaceable> with the Linux name of the partition onto which you installed the system (e.g. <filename>/dev/sda1</filename>); you wrote this down earlier. For users with tiny screens, adding <userinput>fbcon=font:VGA8x8</userinput> (or <userinput>video=font:VGA8x8</userinput> on pre-2.6 kernels) may help readability. You can change this at any time."
msgstr ""

#. Tag: para
#: boot-new.xml:109
#, no-c-format
msgid "If you don't want to start GNU/Linux immediately each time you start, uncheck the <userinput>Auto Boot</userinput> option. Save your settings in the <filename>Prefs</filename> file using the <userinput>Save Settings As Default</userinput> option."
msgstr ""

#. Tag: para
#: boot-new.xml:116
#, no-c-format
msgid "Now select <userinput>Boot Now</userinput> (<keycombo> <keycap>command</keycap> <keycap>B</keycap> </keycombo>) to start your freshly installed GNU/Linux instead of the RAMdisk installer system."
msgstr ""

#. Tag: para
#: boot-new.xml:122
#, no-c-format
msgid "Debian should boot, and you should see the same messages as when you first booted the installation system, followed by some new messages."
msgstr ""

#. Tag: title
#: boot-new.xml:132
#, no-c-format
msgid "OldWorld PowerMacs"
msgstr ""

#. Tag: para
#: boot-new.xml:133
#, no-c-format
msgid "If the machine fails to boot after completing the installation, and stops with a <prompt>boot:</prompt> prompt, try typing <userinput>Linux</userinput> followed by &enterkey;. (The default boot configuration in <filename>quik.conf</filename> is labeled Linux). The labels defined in <filename>quik.conf</filename> will be displayed if you press the <keycap>Tab</keycap> key at the <prompt>boot:</prompt> prompt. You can also try booting back into the installer, and editing the <filename>/target/etc/quik.conf</filename> placed there by the <guimenuitem>Install Quik on a Hard Disk</guimenuitem> step. Clues for dealing with <command>quik</command> are available at <ulink url=\"&url-powerpc-quik-faq;\"></ulink>."
msgstr ""

#. Tag: para
#: boot-new.xml:147
#, no-c-format
msgid "To boot back into MacOS without resetting the nvram, type <userinput>bye</userinput> at the OpenFirmware prompt (assuming MacOS has not been removed from the machine). To obtain an OpenFirmware prompt, hold down the <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>o</keycap> <keycap>f</keycap> </keycombo> keys while cold booting the machine. If you need to reset the OpenFirmware nvram changes to the MacOS default in order to boot back to MacOS, hold down the <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </keycombo> keys while cold booting the machine."
msgstr ""

#. Tag: para
#: boot-new.xml:160
#, no-c-format
msgid "If you use <command>BootX</command> to boot into the installed system, just select your desired kernel in the <filename>Linux Kernels</filename> folder, un-choose the ramdisk option, and add a root device corresponding to your installation; e.g. <userinput>/dev/hda8</userinput>."
msgstr ""

#. Tag: title
#: boot-new.xml:172
#, no-c-format
msgid "NewWorld PowerMacs"
msgstr ""

#. Tag: para
#: boot-new.xml:173
#, no-c-format
msgid "On G4 machines and iBooks, you can hold down the <keycap>option</keycap> key and get a graphical screen with a button for each bootable OS, &debian; will be a button with a small penguin icon."
msgstr ""

#. Tag: para
#: boot-new.xml:180
#, no-c-format
msgid "If you kept MacOS and at some point it changes the OpenFirmware <envar>boot-device</envar> variable you should reset OpenFirmware to its default configuration. To do this hold down the <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </keycombo> keys while cold booting the machine."
msgstr ""

#. Tag: para
#: boot-new.xml:188
#, no-c-format
msgid "The labels defined in <filename>yaboot.conf</filename> will be displayed if you press the <keycap>Tab</keycap> key at the <prompt>boot:</prompt> prompt."
msgstr ""

#. Tag: para
#: boot-new.xml:194
#, no-c-format
msgid "Resetting OpenFirmware on G3 or G4 hardware will cause it to boot &debian; by default (if you correctly partitioned and placed the Apple_Bootstrap partition first). If you have &debian; on a SCSI disk and MacOS on an IDE disk this may not work and you will have to enter OpenFirmware and set the <envar>boot-device</envar> variable, <command>ybin</command> normally does this automatically."
msgstr ""

#. Tag: para
#: boot-new.xml:203
#, no-c-format
msgid "After you boot &debian; for the first time you can add any additional options you desire (such as dual boot options) to <filename>/etc/yaboot.conf</filename> and run <command>ybin</command> to update your boot partition with the changed configuration. Please read the <ulink url=\"&url-powerpc-yaboot-faq;\">yaboot HOWTO</ulink> for more information."
msgstr ""

#. Tag: title
#: boot-new.xml:221
#, no-c-format
msgid "Mounting encrypted volumes"
msgstr ""

#. Tag: para
#: boot-new.xml:223
#, no-c-format
msgid "If you created encrypted volumes during the installation and assigned them mount points, you will be asked to enter the passphrase for each of these volumes during the boot. The actual procedure differs slightly between dm-crypt and loop-AES."
msgstr ""

#. Tag: title
#: boot-new.xml:233
#, no-c-format
msgid "dm-crypt"
msgstr ""

#. Tag: para
#: boot-new.xml:235
#, no-c-format
msgid ""
      "For partitions encrypted using dm-crypt you will be shown the following prompt during the boot: <informalexample><screen>\n"
      "Starting early crypto disks... <replaceable>part</replaceable>_crypt(starting)\n"
      "Enter LUKS passphrase:\n"
      "</screen></informalexample> In the first line of the prompt, <replaceable>part</replaceable> is the name of the underlying partition, e.g. sda2 or md0. You are now probably wondering <emphasis>for which volume</emphasis> you are actually entering the passphrase. Does it relate to your <filename>/home</filename>? Or to <filename>/var</filename>? Of course, if you have just one encrypted volume, this is easy and you can just enter the passphrase you used when setting up this volume. If you set up more than one encrypted volume during the installation, the notes you wrote down as the last step in <xref linkend=\"partman-crypto\"/> come in handy. If you did not make a note of the mapping between <filename><replaceable>part</replaceable>_crypt</filename> and the mount points before, you can still find it in <filename>/etc/crypttab</filename> and <filename>/etc/fstab</filename> of your new system."
msgstr ""

#. Tag: para
#: boot-new.xml:258
#, no-c-format
msgid "The prompt may look somewhat different when an encrypted root file system is mounted. This depends on which initramfs generator was used to generate the initrd used to boot the system. The example below is for an initrd generated using <classname>initramfs-tools</classname>:"
msgstr ""

#. Tag: screen
#: boot-new.xml:265
#, no-c-format
msgid ""
      "Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
      "Begin: Running /scripts/local-top ...\n"
      "Enter LUKS passphrase:"
msgstr ""

#. Tag: para
#: boot-new.xml:267 boot-new.xml:292
#, no-c-format
msgid "No characters (even asterisks) will be shown while entering the passphrase. If you enter the wrong passphrase, you have two more tries to correct it. After the third try the boot process will skip this volume and continue to mount the next filesystem. Please see <xref linkend=\"crypto-troubleshooting\"/> for further information."
msgstr ""

#. Tag: para
#: boot-new.xml:275 boot-new.xml:300
#, no-c-format
msgid "After entering all passphrases the boot should continue as usual."
msgstr ""

#. Tag: title
#: boot-new.xml:283
#, no-c-format
msgid "loop-AES"
msgstr ""

#. Tag: para
#: boot-new.xml:285
#, no-c-format
msgid "For partitions encrypted using loop-AES you will be shown the following prompt during the boot:"
msgstr ""

#. Tag: screen
#: boot-new.xml:290
#, no-c-format
msgid ""
      "Checking loop-encrypted file systems.\n"
      "Setting up /dev/loop<replaceable>X</replaceable> (/<replaceable>mountpoint</replaceable>)\n"
      "Password:"
msgstr ""

#. Tag: title
#: boot-new.xml:308
#, no-c-format
msgid "Troubleshooting"
msgstr ""

#. Tag: para
#: boot-new.xml:310
#, no-c-format
msgid "If some of the encrypted volumes could not be mounted because a wrong passphrase was entered, you will have to mount them manually after the boot. There are several cases."
msgstr ""

#. Tag: para
#: boot-new.xml:319
#, no-c-format
msgid "The first case concerns the root partition. When it is not mounted correctly, the boot process will halt and you will have to reboot the computer to try again."
msgstr ""

#. Tag: para
#: boot-new.xml:326
#, no-c-format
msgid ""
      "The easiest case is for encrypted volumes holding data like <filename>/home</filename> or <filename>/srv</filename>. You can simply mount them manually after the boot. For loop-AES this is a one-step operation: <informalexample><screen>\n"
      "<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></userinput>\n"
      "<prompt>Password:</prompt>\n"
      "</screen></informalexample> where <replaceable>/mount_point</replaceable> should be replaced by the particular directory (e.g. <filename>/home</filename>). The only difference from an ordinary mount is that you will be asked to enter the passphrase for this volume."
msgstr ""

#. Tag: para
#: boot-new.xml:340
#, no-c-format
msgid ""
      "For dm-crypt this is a bit trickier. First you need to register the volumes with <application>device mapper</application> by running: <informalexample><screen>\n"
      "<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
      "</screen></informalexample> This will scan all volumes mentioned in <filename>/etc/crypttab</filename> and will create appropriate devices under the <filename>/dev</filename> directory after entering the correct passphrases. (Already registered volumes will be skipped, so you can repeat this command several times without worrying.) After successful registration you can simply mount the volumes the usual way:"
msgstr ""

#. Tag: screen
#: boot-new.xml:355
#, no-c-format
msgid "<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></userinput>"
msgstr ""

#. Tag: para
#: boot-new.xml:358
#, no-c-format
msgid ""
      "If any volume holding noncritical system files could not be mounted (<filename>/usr</filename> or <filename>/var</filename>), the system should still boot and you should be able to mount the volumes manually like in the previous case. However, you will also need to (re)start any services usually running in your default runlevel because it is very likely that they were not started. The easiest way to achieve this is by switching to the first runlevel and back by entering <informalexample><screen>\n"
      "<prompt>#</prompt> <userinput>init 1</userinput>\n"
      "</screen></informalexample> at the shell prompt and pressing <keycombo> <keycap>Control</keycap> <keycap>D</keycap> </keycombo> when asked for the root password."
msgstr ""

#. Tag: title
#: boot-new.xml:381
#, no-c-format
msgid "Log In"
msgstr ""

#. Tag: para
#: boot-new.xml:383
#, no-c-format
msgid "Once your system boots, you'll be presented with the login prompt. Log in using the personal login and password you selected during the installation process. Your system is now ready for use."
msgstr ""

#. Tag: para
#: boot-new.xml:389
#, no-c-format
msgid "If you are a new user, you may want to explore the documentation which is already installed on your system as you start to use it. There are currently several documentation systems, work is proceeding on integrating the different types of documentation. Here are a few starting points."
msgstr ""

#. Tag: para
#: boot-new.xml:397
#, no-c-format
msgid "Documentation accompanying programs you have installed can be found in <filename>/usr/share/doc/</filename>, under a subdirectory named after the program (or, more precise, the Debian package that contains the program). However, more extensive documentation is often packaged separately in special documentation packages that are mostly not installed by default. For example, documentation about the package management tool <command>apt</command> can be found in the packages <classname>apt-doc</classname> or <classname>apt-howto</classname>."
msgstr ""

#. Tag: para
#: boot-new.xml:408
#, no-c-format
msgid "In addition, there are some special folders within the <filename>/usr/share/doc/</filename> hierarchy. Linux HOWTOs are installed in <emphasis>.gz</emphasis> (compressed) format, in <filename>/usr/share/doc/HOWTO/en-txt/</filename>. After installing <classname>dhelp</classname>, you will find a browsable index of documentation in <filename>/usr/share/doc/HTML/index.html</filename>."
msgstr ""

#. Tag: para
#: boot-new.xml:417
#, no-c-format
msgid ""
      "One easy way to view these documents using a text based browser is to enter the following commands: <informalexample><screen>\n"
      "$ cd /usr/share/doc/\n"
      "$ w3m .\n"
      "</screen></informalexample> The dot after the <command>w3m</command> command tells it to show the contents of the current directory."
msgstr ""

#. Tag: para
#: boot-new.xml:427
#, no-c-format
msgid "If you have a graphical desktop environment installed, you can also use its web browser. Start the web browser from the application menu and enter <userinput>/usr/share/doc/</userinput> in the address bar."
msgstr ""

#. Tag: para
#: boot-new.xml:433
#, no-c-format
msgid "You can also type <userinput>info <replaceable>command</replaceable></userinput> or <userinput>man <replaceable>command</replaceable></userinput> to see documentation on most commands available at the command prompt. Typing <userinput>help</userinput> will display help on shell commands. And typing a command followed by <userinput>--help</userinput> will usually display a short summary of the command's usage. If a command's results scroll past the top of the screen, type <userinput>|&nbsp;more</userinput> after the command to cause the results to pause before scrolling past the top of the screen. To see a list of all commands available which begin with a certain letter, type the letter and then two tabs."
msgstr ""

