<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56141 -->
<!-- revisado por Igor Tamara, 16 nov 2008 -->
<!-- revisado por ender, 31 dic 2005 -->
<!-- traducido por jfs, 30 dic 2005 -->

   <sect3 id="network-console">
   <title>Instalaci�n a trav�s de la red</title>

<para arch="not-s390">

Uno de los componentes m�s interesantes es 
<firstterm>network-console</firstterm>.  �ste le permite hacer una
gran parte de la instalaci�n a trav�s de la red mediante SSH.
El uso de la red implica que tiene que llevar a cabo los primeros
pasos de la instalaci�n a trav�s de la consola al menos hasta 
llegar al punto en el que se configura la red (aunque puede 
automatizar esta parte con 
<xref linkend="automatic-install"/>).

</para><para arch="not-s390">

Este componente no aparece en el men� de la instalaci�n por omisi�n, por
lo que tiene que pedirlo expl�citamente.

En el caso de que est� instalando desde CD debe arrancar fijando la prioridad a
media o llamar al men� de instalaci�n y seleccionar <guimenuitem>Cargar
componentes del instalador desde CD</guimenuitem> y seleccionar de la lista de
componentes <guimenuitem>network-console: Continuar la instalaci�n de forma
remota utilizando SSH</guimenuitem>.  Si el componente se carga correctamente
ver� una nueva entrada de men� llamada <guimenuitem>Continuar la instalaci�n de forma remota utilizando SSH</guimenuitem>.

</para><para arch="s390">

Este es el m�todo de instalaci�n por omisi�n para las instalaciones en
&arch-title; una vez configurada la red.

</para><para>

<phrase arch="not-s390">Despu�s de seleccionar esta nueva entrada se</phrase>
<phrase arch="s390">Se</phrase> le preguntar� la contrase�a a utilizar
para conectarse con el sistema de instalaci�n, y se confirmar� esta nueva
contrase�a.  Eso es todo lo que necesita.  Ahora deber�a poder ver una pantalla
que le indica que debe conectarse de forma remota con el identificador de
usuario <emphasis>installer</emphasis> y la contrase�a que introdujo. Un detalle
importante a destacar es que se le indicar� tambi�n la huella digital del
sistema que est� instalando. Tiene que transferir esta huella de forma
segura a la <quote>persona que continuar� con la instalaci�n remota</quote>.

</para><para>

Siempre puede pulsar &enterkey; para continuar con la instalaci�n local si
lo desea. Si lo hace se le mostrar� el men� principal y podr� elegir otro
componente.

</para><para>

En el otro extremo de la comunicaci�n, como prerequisito, deber�
configuar su terminal para que utilice codificaci�n UTF-8, porque es 
la que utiliza el sistema de instalaci�n. Si no lo hace podr� 
hacer la instalaci�n pero puede que vea caracteres extra�os en la
pantalla, como puedan ser bordes de cuadro de di�logo rotos o caracteres
no americanos ilegibles. Para conectarse al sistema de instalaci�n remoto
s�lo tiene que escribir:

<informalexample><screen>
<prompt>$</prompt> <userinput>ssh -l installer <replaceable>sistema_a_instalar</replaceable></userinput>
</screen></informalexample>

donde <replaceable>sistema_a_instalar</replaceable> es o bien el nombre
o bien la direcci�n IP del equipo que est� instalando. Antes de conectarse
se le mostrar� la huella digital del sistema remoto y deber� confirmar que
es la correcta.

</para><note><para>

La orden <command>ssh</command> en el instalador usa una configuraci�n
predeteminada que no env�a paquetes de keep-alive. En principio, una
conexi�n al sistema que est� instal�ndose deber�a mantener la conexi�n
abierta de forma indefinida. Pero, en ciertas situaciones &mdash;
dependiendo de su configuraci�n local de red &mdash; la conexi�n puede
interrumpirse despu�s de un per�odo de inactividad.  Un caso com�n 
puede ocurrir cuando hay alguna modalidad de Traducci�n de Direcci�nes
de Red (NAT) en alg�n sitio entre el cliente y el sistema que est�
instal�ndose.  Dependiendo del punto en el cual la conexi�n se perdi�,
podr�a resultar imposible dar continuidad a la instalaci�n despu�s
de reconectarse.

</para><para>

Podr�a evitar que caiga la conexi�n a�adiendo la opci�n
<userinput>-o&nbsp;ServerAliveInterval=<replaceable>value</replaceable></userinput>
cuando se hace la conexi�n <command>ssh</command>, o adicionar tal
opci�n en en su archivo de configuraci�n de <command>ssh</command>.
Pero tenga en cuenta que en algunos casos adicionar tal orden
podr�a tambi�n <emphasis>causar</emphasis> la ca�da de la conexi�n (por
ejemplo si los paquetes keep-alive se env�an mientras haya una ca�da
corta de la red, en la cu�l la orden <command>ssh</command> se ha
recuperado),  por lo tanto debe usarse �nicamente cuando sea necesario.

</para></note>

<note><para>
Si instala muchos sistemas de forma consecutiva y, por casualidad, comparten la
direcci�n IP o nombre de equipo, puede tener problemas para conectarse a �stos
porque <command>ssh</command> se negar� a conectarse a ellos, ya
que cada sistema tiene una huella digital distinta, lo que para <command>ssh</command>
es indicativo de un posible ataque de suplantaci�n.  Si est� seguro de que no
se trata de ning�n ataque deber� eliminar la l�nea del equipo en cuesti�n del fichero
<filename>~/.ssh/known_hosts</filename><footnote>

<para>
El comando a continuaci�n eliminar� cualquier l�nea que existiera para un
servidor:
<command>ssh-keygen -R &lt;<replaceable>hostname</replaceable>|<replaceable>IP&nbsp;address</replaceable>&gt;</command>.
</para>

</footnote> and try again.

</para></note><para>

Despu�s de acceder al sistema se le mostrar� una pantalla de instalaci�n inicial
donde tendr� dos posibilidades: <guimenuitem>Arrancar men�</guimenuitem> y
<guimenuitem>Arrancar consola</guimenuitem>.  La primera de estas opciones
le llevar� al men� de instalaci�n, donde podr� seguir con la instalaci�n
como lo hace habitualmente.  La segunda de estas opciones ejecuta un
int�rprete de l�nea de �rdenes desde el que puede examinar, y quiz�s arreglar,
el sistema remoto.  S�lo deber�a arrancar una sesi�n de SSH para el men� de
instalaci�n, aunque puede tener tantas sesiones como quiera con consolas
remotas.

</para><warning><para>

Una vez ha arrancado la instalaci�n por SSH de forma remota no deber�a
volver a la sesi�n de instalaci�n que se est� ejecutando en la consola local.
Si lo hace, podr�a corromper la base de datos que guarda la configuraci�n
del nuevo sistema, al realizar accesos simult�neos a ella.  Esto podr�a llevar
a que la instalaci�n fallara o a que tuviera problemas con el sistema que ha instalado.

</para></warning>

   </sect3>
