partman-auto-crypto (11) unstable; urgency=low

  [ Updated translations ]
  * Bosnian (bs.po) by Armin Besirovic
  * Danish (da.po)
  * French (fr.po)
  * Croatian (hr.po) by Josip Rodin
  * Serbian (sr.po) by Veselin Mijušković

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 21:00:17 -0300

partman-auto-crypto (10) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Use $pv_devices that auto_lvm_prepare() now fills for us instead of
    iterating through every known devices.
    Depends: partman-auto (>= 81)

  [ Updated translations ]
  * French (fr.po)
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 25 Aug 2008 21:05:07 +0200

partman-auto-crypto (9) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Propagate auto_lvm_prepare() error code in autopartition-crypto.
  * Correctly handle GOBACK in
    automatically_partition/some_device_crypto/do_option.

  [ Updated translations ]
  * Belarusian (be.po) by Pavel Piatruk
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
	
 -- Otavio Salvador <otavio@debian.org>  Tue, 05 Aug 2008 13:43:16 -0300

partman-auto-crypto (8) unstable; urgency=low

  [ Updated translations ]
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 00:49:58 -0300

partman-auto-crypto (7) unstable; urgency=low

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Changwoo Ryu

 -- Otavio Salvador <otavio@debian.org>  Fri, 15 Feb 2008 08:40:26 -0200

partman-auto-crypto (6) unstable; urgency=low

  * Move deletion of SVN directories to install-rc script.
  * Improve the way install-rc is called.
  * Moved definitions.sh to ./lib/base.sh.
  * Moved recipes.sh and auto-shared.sh to ./lib/.
  * Moved auto-lvm_tools.sh to ./lib/auto-lvm.sh.
  * Moved crypto_tools.sh to ./lib/crypto-base.sh.
  * Change priority to optional to allow dynamic loading by partman-base.
  * Requires: partman-base (>= 114); partman-crypto (>= 25);
              partman-auto (>= 73); partman-auto-lvm (>= 24).

  [ Updated translations ]
  * Belarusian (be.po) by Hleb Rubanau
  * Danish (da.po) by Claus Hindsgaul
  * Basque (eu.po) by Piarres Beobide
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Tue, 11 Dec 2007 14:06:22 +0100

partman-auto-crypto (5) unstable; urgency=low

  [ Updated translations ]
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 18:15:15 +0100

partman-auto-crypto (4) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Bosnian (bs.po) by Safir Secerovic
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Estonian (et.po) by Siim Põder
  * Galician (gl.po) by Jacobo Tarrio
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 12:19:43 +0100

partman-auto-crypto (3) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Bengali (bn.po) by Jamil Ahmed
  * Catalan (ca.po) by Jordi Mallach
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hindi (hi.po) by Nishant Sharma
  * Indonesian (id.po) by Arief S Fitrianto
  * Japanese (ja.po) by Kenshi Muto
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 15:43:51 +0200

partman-auto-crypto (2) unstable; urgency=low

  [ Max Vozeler ]
  * Run debconf-updatepo to generate debian/po/templates.pot

  [ Frans Pop ]
  * Change description for method
  * Avoid double sourcing of common functions
  * Add missing dependency on debconf
  * Add Lintian override for standards-version

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Greek (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Wed, 11 Oct 2006 04:46:03 +0200

partman-auto-crypto (1) unstable; urgency=low

  [ David Härdeman ]
  * Initial version

 -- Max Vozeler <xam@debian.org>  Sat, 23 Sep 2006 13:02:37 +0200
