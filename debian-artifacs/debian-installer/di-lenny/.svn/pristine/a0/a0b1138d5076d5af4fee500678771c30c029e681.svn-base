# Slovak translation of win32-loader.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the win32-loader package.
# Peter Mann <Peter.Mann@tuke.sk>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-05-01 20:39+0200\n"
"Last-Translator: Peter Mann <Peter.Mann@tuke.sk>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_SLOVAK"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1250"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp852"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Slovak"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Inštalácia Debianu"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Slovak.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Zavádzač inštalácie Debianu"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Nedá sa nájsť win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini je neúplný.  Kontaktujte poskytovateľa tohto média."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Program zistil, že typ vašej klávesnici je \"$0\".  Je to správne?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Zašlite prosím chybové hlásenie s nasledovnými informáciami:\n"
"\n"
" - Verzia Windows.\n"
" - Regionálne nastavenia.\n"
" - Skutočný typ klávesnice.\n"
" - Zistený typ klávesnice.\n"
"\n"
"Ďakujeme."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Zdá sa, že na disku $c nemáte dostatok miesta. Na inštaláciu kompletného "
"desktopu sa odporúča aspoň 3 GB voľného miesta na disku. Ak už máte na "
"inštaláciu Debianu vyhradený samostatný disk alebo diskovú oblasť, prípadne "
"plánujete úplné nahradenie systému Windows, toto upozornenie môžete "
"ignorovať."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Chyba: nedostatok voľného miesta.  Inštalácia sa prerušuje."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Tento program zatiaľ nepodporuje verziu Windows $windows_version."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"Verzia Debianu, ktorú sa pokúšate inštalovať, je určená pre moderné 64 bitové "
"počítače.  Váš počítač však neumožňuje spúšťať 64 bitové programy.\n"
"\n"
"Musíte použiť 32 bitovú (\"i386\") verziu Debianu alebo verziu podporujúcu "
"inštaláciu viac architektúr (tzv. multi-arch verziu).\n"
"\n"
"Inštalácia sa teraz preruší."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Váš počítač umožňuje používať moderné 64 bitové operačné systémy.  Verzia "
"Debianu, ktorú sa pokúšate inštalovať, je určená pre staršie 32 bitové systémy.\n"
"\n"
"Môžete pokračovať v inštalácii, ale na využitie všetkých schopností vášho počítača "
"sa odporúča použitie 64 bitovej (\"amd64\") verzie Debianu alebo verzie podporujúcej "
"inštaláciu viac architektúr (tzv. multi-arch verziu).\n"
"\n"
"Chcete inštaláciu prerušiť?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Zvoľte spôsob inštalácie:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Štandartný spôsob.  Odporúča sa pre väčšinu používateľov."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Pokročilý spôsob.  Odporúča sa pre skúsených používateľov, ktorí chcú mať "
"úplnú kontrolu nad inštaláciou."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Zvoľte akciu:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Inštalácia Debian GNU/Linux na tento počítač."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Obnova existujúceho systému Debian (záchranný režim)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Grafická inštalácia"

#: win32-loader.c:89
msgid "Text install"
msgstr "Textová inštalácia"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Sťahuje sa %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Pripája sa ..."

#: win32-loader.c:92
msgid "second"
msgstr "s"

#: win32-loader.c:93
msgid "minute"
msgstr "m"

#: win32-loader.c:94
msgid "hour"
msgstr "h"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr " "

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) z %dkB pri %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (ostáva %d %s%s)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Zvoľte si verziu inštalátora Debianu, ktorú chcete použiť:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Stabilné vydanie.  Nainštaluje sa Debian \"stable\"."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Denné vydanie.  Toto je vývojová verzia inštalátora Debianu.  Zvyčajne nainštaluje "
"Debian  \"testing\" verziu, ale je možné nainštalovať aj verzie \"stable\" "
"alebo \"unstable\"."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Pred použitím denného vydania sa odporúča prezrieť známe problémy.  Chcete "
"si ich prezrieť teraz?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Desktopové prostredie:"

#: win32-loader.c:114
msgid "None"
msgstr "Žiadne"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Zavádzač inštalácie Debianu sa nastaví s nasledovnými parametrami.  Ak "
"neviete, čo robíte, NEMEŇTE žiaden z parametrov."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Nastavenie proxy (počítač:port)"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Umiestnenie súboru boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "Základná URL adresa pre netboot súbory (linux a initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Chyba"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Chyba: zlyhalo kopírovanie $0 do $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Generuje sa $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Do $0 sa pripájajú informácie o predvolenom nastavení."

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Chyba: nedá sa spustiť $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Zakazuje sa NTFS kompresia v zavádzacích súboroch"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Zavádzač inštalácie Debianu sa registruje do NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Zavádzač inštalácie Debianu sa registruje do BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Chyba: nepodarilo sa spracovať výstup bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "Chyba: súbor boot.ini sa nenašiel.  Je to naozaj Windows $windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "VEĽMI DÔLEŽITÁ POZNÁMKA:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"Teraz sa spustí druhá časť inštalácie.  Tento program po vašom potvrdení "
"reštartuje Windows do DOS režimu a automaticky načíta inštalátor Debianu.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Na pokračovanie inštalácie Debianu je potrebný reštart počítača. Počas "
"ďalšieho nábehu počítača dostanete na výber štart Windows alebo inštalácie "
"Debianu. Zvoľte si teda inštaláciu Debianu, ak chcete naozaj pokračovať "
"v inštalácii.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Počas inštalácie dostanete možnosť na zmenšenie diskovej oblasti pre Windows "
"a inštaláciu Debianu na voľné miesto alebo na úplné prepísanie diskovej oblasti. "
"V oboch prípadoch sa DÔRAZNE ODPORÚČA záloha dôležitých údajov na disku. "
"Ani autori tohto zavádzača ani projekt Debian NENESÚ ŽIADNU ZODPOVEDNOSŤ "
"za prípadnú stratu údajov. \\n\\nAk sa rozhodnete ponechať na svojom disku "
"Windows aj Debian, môžete po ukončení inštalácie odstrániť tento zavádzač "
"pomocou dialógu Pridať/Odobrať programy v Ovládacom paneli."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Chcete teraz reštartovať počítač?"

