<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 51744 -->
<!-- traducido jfs, 21 abril 2006 -->
<!-- Revisado por Igor Tamara, enero 2007 -->
<!-- Revisado Igor Tamara, nov 21 2008 -->

   <sect3 id="apt-setup">
   <title>Configurar apt</title>

<para>

Una de las herramientas utilizadas para instalar paquetes en un sistema &debian;
es el programa <command>apt-get</command>, que est� dentro
del paquete <classname>apt</classname>.<footnote>

<para>

En realidad el programa que instala los paquetes se llama
<command>dpkg</command>.  Este programa es, sin embargo, una herramienta
de bajo nivel. <command>apt-get</command> es una herramienta de
m�s alto nivel que llama a <command>dpkg</command> cuando sea necesario.
Sabe c�mo obtener los paquetes: de un CD, de la red o de cualquier otra
ubicaci�n. Tambi�n es capaz de instalar otros paquetes indispensables
para que los paquetes que usted intenta instalar funcionen adecuadamente.

</para>

</footnote>
Existen otras interfaces a la gesti�n de paquetes, como 
<command>aptitude</command> y <command>synaptic</command>.  Se recomienda
a los usuarios n�veles que utilicen estas interfaces ya que integran otras
funciones adicionales (como la b�squeda de paquetes y comprobaciones de
estado) en un interfaz de usuario m�s amigable.  De hecho, la herramienta
recomendada para la gesti�n de paquetes es <command>aptitude</command>.

</para><para>

Debe configurar <command>apt</command> para que sepa de d�nde descargar
paquetes. Los resultados de la configuraci�n realizada se guardan en el
fichero <filename>/etc/apt/sources.list</filename>. Podr� examinar y
editar este fichero a su gusto una vez haya terminado la instalaci�n.

</para><para>

Si est� instalando con la prioridad predeterminada,  basado en el
m�todo de instalaci�n que est� usando y posiblemente usando las
elecciones previas en la instalaci�n, el instalador se encargar� en gran
medida de la configuraci�n de forma autom�tica.  La mayor parte de
ocasiones el instalador a�adir� autom�ticamente una r�plica de seguridad,
y si est� instalando una distribuci�n estable, una r�plica para el
servicio de actualizaci�n <quote>volatile</quote>.

</para><para>

Tendr� oportunidad de tomar muchas m�s decisiones por su cuenta si est� 
usando en la instalaci�n una prioridad menor (p.e. modo experto).  Puede
elegir usar o no los servicios securidad y/o volatile, y puede elegir
a�adir paquetes de las secciones del archivo <quote>contrib</quote> y
<quote>non-free</quote>.

</para>

    <sect4 id="apt-setup-cdset">
    <title>Instalar desde uno o m�s CDs o DVDs</title>

<para>

Si instala desde un CD o un DVD que hace parte de un conjunto m�s grande,
el instalador le preguntar� si desea explorar CDs o DVDs adicionales.
Si tiene CDs o DVDs adicionales, probablemente desear� hacerlo, de forma
que el instalador pueda usar los paquetes inclu�dos en ellos.

</para><para>

Si no tiene CDs o DVDs adicionales, no hay problema: no son indispensables.
Si tampoco usa una r�plica de red(como se explica en la secci�n siguiente),
podr�a darse que no todos los paquetes pertenecientes a las tareas que
seleccione en el siguiente paso de la instalaci�n puedan instalarse.

</para>
<note><para>

Los paquetes se incluyen en los CDs (y DVDs) de acuerdo con su popularidad.
Esto significa que en la mayor�a de casos, solamente se necesita el primer
CD de un conjunto y que muy poca gente realmente usa cualesquiera de los
paquetes incluidos en el �ltimo CD de un conjunto.

</para><para>

Tambi�n significa que comprar o descargar y quemar el conjunto completo de
CDs es un desperdicio de dinero, debido a que usted no usar� la mayor�a de
ellos.  En general, es mejor obtener solamente entre los primeros 3 y 8 CDs
y usar una r�plica de red por Internet para instalar cualquier paquete
adicional.  De igual forma sucede con los conjuntos de DVDs: el primer DVD,
o el primer y segundo DVD suplir�an la mayor�a de necesidades.

</para><para>

Por norma general, para una instalaci�n de escritorio habitual(usando el
ambiente de escritorio GNOME), basta con los primeros tres CDs.  Para los
ambientes de escritorio alternativos(KDE o Xfce), se requieren CDs
adicionales.  El primer DVD cubre sin problema los tres ambientes de
escritorio.

</para></note>
<para>

Si usted explora varios CDs o DVDs, el instalador le solicitar� insertarlos
uno a uno cuando necesite paquetes de otro CD/DVD que no est�n en el que
est� en la unidad. Tenga en cuenta que deber�a explorar �nicamente CDs o
DVDs que pertenezcan al mismo conjunto.  El orden en el que se exploren no
interesa, pero explorarlos en orden ascendente reducir� posibles
equivocaciones.

</para>
    </sect4>

    <sect4 id="apt-setup-mirror">
    <title>Usar una r�plica en red</title>

<para>

Una pregunta que se har� durante la mayor�a de instalaciones es si si se
desea o no usar una r�plica en red como fuente de paquetes.  En la mayor�a
de oportunidades, la respuesta predeterminada es la adecuada, pero hay
excepciones.

</para><para>

Si <emphasis>no</emphasis> est� instalando desde un CD o DVD commpleto o
usando una imagen completa de CD/DVD, deber�a querer usar una r�plica en
red, puesto que de otra manera terminar�a con un sistema muy m�nimo. Pero,
si tiene una conexi�n a Internet limitada, es mejor
<emphasis>not</emphasis> seleccionar la tarea <literal>desktop</literal>
en el paso siguiente de la instalaci�n.

</para><para>

Si instala desde un solo CD completo o con una imagen completa de CD, no
es necesaria una r�plica en red, pero es muy recomendado emplearla porque un
solo CD contiene solamente una cantidad limitada de paquetes.  Si tiene una
conexi�n a Internet limitada, podr�a ser mejor <emphasis>no</emphasis>
seleccionar una r�plica en red en este momento y m�s bien culminar la
instalaci�n usando �nicamente lo que est� en el CD e instalar selectivamente
paquetes adicionales despu�s de la instalaci�n (esto es, despu�s de haber
reiniciado en el nuevo sistema).

</para><para>

Si instala desde un DVD o una imagen de DVD, cualquier paquete que necesite
druante la instalaci�n deber�a estar presente en el primer DVD.  De la misma
forma si ha explorado varios CDs como se explic� en la secci�n anterior.  El
uso de una r�plica en red es opcional.

</para><para>

Una ventaja de a�adir una r�plica en red es que las actualizaciones que hayan
ocurrido desde el momento en que el conjunto de CDs/DVDs fue creado y hecho
p�blico, estar�n disponibles para instalarse, extendiendo de esta manera la
vida de su conjunto de CDs/DVDs sin comprometer la seguridad o estabilidad del
sistema instalado.

</para><para>

En Resumen: el seleccionar una r�plica en red en general es una buena idea,
excepto si no tiene una conexi�n a Internet buena.  Si la versi�n actual de
un paquete est� disponible en un CD/DVD, el instalador siempre lo usar�.
La cantidad de datos que se descargar� si usted ha seleccionado una r�plica
en red depender� de:

<orderedlist>
<listitem><para>

las tareas que seleccione en el paso siguiente de esta instalaci�n,

</para></listitem>
<listitem><para>

los paquetes que se requieran para esas tareas,

</para></listitem>
<listitem><para>
 
cuales de esos paquetes est�n presentes en los CDs o DVDs que haya explorado, y

</para></listitem>
<listitem><para>

si hay versiones actualizadas en las r�plicas en red de los paquetes inclu�dos
en los CDs o DVDs(bien sea para una r�plica en red de paquetes o una r�plica
de seguridad o actualizaciones vol�tiles).


</para></listitem>
</orderedlist>

</para><para>

Observe que el �ltimo punto significa que, incluso si usted no ha seleccionado
una r�plica en red, algunos paquetes pueden descargarse de Internet si hay
actualizaciones de seguridad o vol�tiles para aquellos servicios que se hayan
configurado.

</para>
</sect4>
   </sect3>
