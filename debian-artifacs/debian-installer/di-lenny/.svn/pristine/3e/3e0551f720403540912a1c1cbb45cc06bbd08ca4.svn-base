# Esperanto translation.
# Copyright (C) 2008 Free Software Foundation Inc.
# This file is distributed under the same license as the win32_loader package.
# Felipe Castro <fefcas@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: win32_loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-09-07 00:21+0200\n"
"PO-Revision-Date: 2008-10-22 22:14-003\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_ENGLISH"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp437"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Esperanto"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debiana Instalilo"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "English.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Debian-Instalila Ekshargilo"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Ne eblis trovi 'win32-loader.ini'."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "'win32-loader.ini estas malkompleta. Kontaktu la kreinton de tiu chi datumportilo."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr ""
"Tiu chi programo detektis ke via klavar-tipo estas \"$0\". Chu tio estas "
"korekte?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Bonvolu sendi eraro-raporton kun la jena informaro:\n"
"\n"
" - Versio de Windows.\n"
" - Landaj agordoj.\n"
" - Reala klavar-tipo.\n"
" - Detektita klavar-tipo.\n"
"\n"
"Dankon."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Shajne ne ekzistas suficha libera disk-spaco en la aparato $c. Por kompleta "
"labortabla instalo, oni rekomendas minimume 3 GB. Se jam ekzistas "
"aparta disko au diskparto por instali 'Debian', au vi planas tute "
"anstatauigi Windows, vi povas sekure ignori tiun chi atentigon."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Eraro: ne-suficha libera disk-spaco. Oni chesigas la instaladon."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Tiu chi programo ankorau ne subtenas Windows '$windows_version'."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"La versio de Debian vi provas instali estas projektita por funkcii en modernaj, "
"64-bitaj komputiloj. Tamen, via komputilo ne kapablas lanchi 64-bitajn programojn.\n"
"\n"
"Uzu la 32-bitan (\"i386\") version de Debian, au la version 'Multi-arch', "
"kiu kapablas instali iun ajn el ili.\n"
"\n"
"Tiu chi instalilo chesighos nun."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Via komputilo kapablas lanchi modernajn, 64-bitajn operaciumojn. "
"Tamen, la versio de Debian kiun vi provas instali estas projektita por "
"funkcii en pli malmoderna, 32-bita aparataro.\n"
"\n"
"Ech tiel vi povas daurigi tiun chi instaladon, sed por plejeble profiti "
"vian komputilon, ni rekomendas ke vi uzu la 64-bitan (\"amd64\") version "
"de Debian anstataue, au la version 'Multi-arch', kiu kapablas instali "
"iun ajn el ili.\n"
"\n"
"Chu vi deziras chesigi nun?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Elektu instal-reghimon:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Normala reghimo. Rekomendinde por plejparto el la uzantoj."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Porspertula reghimo. Rekomendinde por spertaj uzantoj kiuj volas kompletan "
"mastrumadon por la instalprocezo."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Elektu agon:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Instali Debian GNU/Linux en tiu chi komputilo."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Ripari ekzistantan sistemon Debian (restariga reghimo)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Grafika instalado"

#: win32-loader.c:89
msgid "Text install"
msgstr "Teksta instalado"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Elshutado de %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Konektado ..."

#: win32-loader.c:92
msgid "second"
msgstr "sekundo"

#: win32-loader.c:93
msgid "minute"
msgstr "minuto"

#: win32-loader.c:94
msgid "hour"
msgstr "horo"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "j"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) de %dkB je %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s restas)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Elektu la version de Debian-Instalilo por uzi:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Stabila eldono. Tio chi instalos Debian kun nivelo \"stabila\" (stable)."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Chiutaga kompilado. Tio chi estas la disvolvighanta versio de Debian-Instalilo. "
"Ghi implicite instalos Debian-nivelon \"testing\" (testado), kaj ghi povas instali "
"ankau \"stable\" (stabilan) au \"unstable\" (malstabilan)."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Oni rekomendas ke vi kontrolu konatajn problemojn antau ol uzi chiutagan "
"kompiladon. Chu vi deziras fari tion nun? (nur angle)"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Labortabla medio:"

#: win32-loader.c:114
msgid "None"
msgstr "Nenio"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"La Debian-Instalila Ekshargilo estos agordita per la jenaj parametroj. NE "
"shanghu iun ajn el ili krom se vi bone scias kion vi faras."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Prokurilaj agordoj (gastiganto:pordo)"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Loko de 'boot.ini':"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "Baza URL por disk-kopiajhoj 'netboot' ('linux' kaj 'initrd.gz'):"

#: win32-loader.c:119
msgid "Error"
msgstr "Eraro"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Eraro: malsukceso por kopii $0 al $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Kreado de $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Postmetado de \"antausema\" (preseeding) informaro al $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Eraro: ne eblas lanchi $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Malebligado de kompaktigo NTFS en ekshargaj dosieroj"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registrado de Debian-Instalilo en NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registrado de Debian-Instalilo en BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Eraro: malsukceso por analizi la eligon de 'bcdedit.exe'."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "Eraro: 'boot.ini' ne estis trovita. Chu vere tio estas Windows $windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "TRE GRAVA RIMARKIGO:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"La dua etapo de tiu chi instalada procezo estas jhus ekigota. Post via "
"konfirmo, tiu chi programo reshargos je Windows per reghimo DOS, kaj automate "
"lanchos la Debian-Instalilon.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Vi bezonas reshargi por daurigi vian Debian-instaladon. Dum la sekvonta "
"ekshargo, vi estos pridemandata chu vi volas lanchi Windows au la Debian-"
"Instalilon. Elektu Debian-Instalilon por daurigi la instaladan procezon.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Dum la instalada procezo, vi havos oportunon por malpligrandigi la diskparton de "
"Windows por instali Debian au anstatauigi entute. Ambauokaze, oni FORTE REKOMENDAS "
"ke vi jam sekurkopiis vian tutan datumaron. Nek la autoroj de tiu chi ekshargilo nek "
"la projekto Debian prenos IUN AJN RESPONDECON okaze de perdo de datumaro.\\n\\nTujpost "
"kiam via instalado de Debian estos plenumita (kaj se vi elektis pluteni Windows en via "
"disko), vi povas malinstali la Debian-Instalilan Ekshargilon per la dialogo de Windows "
"'Aldoni/Forigi Programojn', en la Stir-Panelo (Control Panel)."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Chu vi volas reshargi nun?"
