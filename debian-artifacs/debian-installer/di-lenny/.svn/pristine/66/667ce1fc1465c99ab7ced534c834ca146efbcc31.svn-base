<!-- original version: 56326 -->

<sect3 id="partman-lvm">
   <title>Configuration du <quote>Logical Volume Manager</quote> (LVM)</title>
<para>

Si vous travaillez sur les ordinateurs comme administrateur système ou
si vous êtes un utilisateur <quote>avancé</quote>, vous avez sûrement
rencontré le cas suivant&nbsp;: une partition qui manque d'espace libre 
(c'est habituellement la plus importante du système) et une partition
grossièrement sous-utilisée, la gestion de cette situation ayant consisté
en déplacement de fichiers, création de liens symboliques, etc.
</para>
<para>
Pour éviter cette situation, on peut utiliser un gestionnaire de volumes
logiques, <quote>Logical Volume Manager</quote> (LVM). Dit simplement,
avec LVM vous pouvez combiner vos partitions 
(<firstterm>volumes physiques</firstterm> dans le lexique LVM) pour former
un disque virtuel (<firstterm>groupe de volumes</firstterm>) qui peut être
divisé en partitions virtuelles (<firstterm>volumes logiques</firstterm>).
L'idée est que les volumes logiques s'étendent sur plusieurs disques physiques.
</para>
<para>
Dès lors, quand vous constatez que votre vieille partition 
<filename>/home</filename> de 160&nbsp;Go a besoin d'espace, vous pouvez
simplement ajouter un disque de 300&nbsp;Go à votre machine, l'intégrer au
groupe de volumes existant et redimensionner le volume logique qui comprend
la partition <filename>/home</filename> et vos utilisateurs ont
de nouveau de la place sur la partition de 460&nbsp;Go&nbsp;!
Cet exemple est bien sûr un peu simplifié. Si vous ne l'avez pas encore lu,
vous devriez consulter le <ulink url="&url-lvm-howto;">HOWTO LVM</ulink>.

</para>
<para>

La configuration de LVM dans l'installateur Debian est très simple. D'abord,
vous devez marquer les partitions à utiliser comme volumes physiques par
LVM. Cela se fait avec <command>partman</command> dans le menu 
<guimenu>Configuration des partitions</guimenu>, où vous choisissez
<menuchoice><guimenu>Utiliser comme :</guimenu> 
<guimenuitem>volume physique pour LVM</guimenuitem></menuchoice>.

</para>
<para>

Quand vous revenez à l'écran principal de <command>partman</command>, vous voyez
une nouvelle option 
<guimenuitem>Configurer LVM, le gestionnaire des volumes logiques</guimenuitem>.
Quand vous la sélectionnez, on vous demande d'abord de confirmer les modifications en
attente pour la table des partitions (s'il y en a) et le menu de configuration de LVM sera
affiché. Le menu n'affiche que les actions valables selon le contexte.
Les actions possibles sont&nbsp;:

<itemizedlist>
  <listitem><para>
    <guimenuitem>Afficher les détails de la configuration</guimenuitem> :
    montre la structure des volumes LVM, le nom et la taille des volumes, etc.
  </para></listitem>
  <listitem><para>
    <guimenuitem>Créer un groupe de volumes</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Créer un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Supprimer un groupe de volumes</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Supprimer un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Etendre un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Réduire un volume logique</guimenuitem>
  </para></listitem>
  <listitem><para>
    <guimenuitem>Terminer</guimenuitem>:
    retourne à l'écran principal de <command>partman</command>.
  </para></listitem>
</itemizedlist>

</para>
<para>
Utilisez les options de ce menu pour créer d'abord un groupe de volumes et pour
créer ensuite les volumes logiques.
</para>

<para>  
Quand vous revenez à l'écran principal de <command>partman</command>,
tous les volumes logiques créés sont affichés comme si c'étaient
de simples partitions, et vous devez les traiter ainsi.
</para>
</sect3>


