# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005-2007.
# Felipe Castro <fefcas@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: eo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-14 18:32-0300\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Sava modo"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Eniri restarigan modon"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Neniu trovita diskparto"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"La instalilo ne povis trovi iun ajn diskparton, tiel ke vi ne povos munti "
"radikan dosiersistemon. Tio povas esti kaŭzita de malsukcesinta kerno dum "
"detektado de pelilo por via fiksita disko aŭ dum legado de la diskparta "
"tabelo, aŭ la disko povas esti nepartigita. Se vi deziras, vi povas "
"priesplori tion el ŝelo en la instalila medio."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Surmetota adaptilo kiel radika dosiersistemo (/):"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Elektu adaptilon, kiun vi volas uzi kiel radika dosiersistemo. Vi eble "
"elktos inter pluraj farotaj savadoj sur tiu dosiersistemo."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Neniu tia aparato"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"La aparato kiun vi indikis por via radika dosiersistemo (${DEVICE}) ne "
"ekzistas. Bonvolu reprovi."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Surmetado malsukcesis"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Eraro okazis dum muntado de la aparato, kiun vi elektis tiel radika "
"dosiersistemo (${DEVICE}) sur '/target'."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Bonvolu kontroli la 'syslog'-dosieron por plia informo."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Restarigaj operacioj"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Savado malsukcesis"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "La savado '${OPERATION}' malsukcesis kun erara kodo ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Lanĉu ŝelon en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Lanĉu ŝelon en la instalota kunteksto"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Elektu malsaman radikan dosiersistemon"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Reŝargu sistemon"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Ŝelo estas plenumata"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Post tiu ĉi mesaĝo, vi atingos ŝelon kun ${DEVICE}-o muntita sur '/' (radiko-"
"dosierujo). Se vi bezonas aliajn dosiersistemojn, ekzemple apartan '/usr'-"
"dosierujon, vi mem devos munti ilin."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Interpretila funkciada eraro en '/target'"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Interpretilo (${SHELL}) estas trovita en via radika dosiersistemo "
"(${DEVICE}), sed eraro okazis dum ĝia rultempo."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Neniu interpretilo trovitas en '/target'"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Nenio uzebla interpretilo estas trovita sur via radika dosiersistemo "
"(${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Interaga ŝelo en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Post tiu ĉi mesaĝo, vi atingos ŝelon kun ${DEVICE}-o muntita sur la "
"dosierujo '/target'. Vi povos labori sur ĝi per disponeblaj iloj el la "
"instalila medio. Se vi volas igi ĝin dumtempa radika dosiersistemo, komandu "
"tion: \"chroot /target\". Se vi bezonas aliajn dosiersistemojn, ekzemple "
"apartan '/usr'-dosierujon, vi mem devos munti ilin."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Post tiu ĉi mesaĝo, vi atingos ŝelon en la instalila medio. Ĉar la instalilo "
"trovis neniun diskpartigon, neniu dosiersistemo estis muntita por vi."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Interaga ŝelo en la instalila medio"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "'${DEVICE}'-pasfrazo:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Bonvolu tajpi la pasfrazon por ĉifri la datumarujon '${DEVICE}'."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Se vi nenion indikas, la datumarujon estos maldisponebla dum la restarigaj "
"procezoj."
