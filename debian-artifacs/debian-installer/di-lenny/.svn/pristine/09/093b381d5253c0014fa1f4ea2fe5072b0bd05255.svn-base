# Debian GNU/Linux Installation Guide
msgid ""
msgstr ""
"Project-Id-Version: d-i-manual\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-05-30 00:09+0000\n"
"PO-Revision-Date: 2005-06-10 01:57+0800\n"
"Last-Translator: Jhang, Jia-Wei<dreamcryer@gmail.com>\n"
"Language-Team: debian-chinese-big5 <debian-chinese-big5@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: bookinfo.xml:5
#, no-c-format
msgid "&debian; Installation Guide"
msgstr "&debian; 安裝手冊"

#. Tag: para
#: bookinfo.xml:8
#, fuzzy, no-c-format
msgid ""
"This document contains installation instructions for the &debian; &release; "
"system (codename <quote>&releasename;</quote>), for the &arch-title; "
"(<quote>&architecture;</quote>) architecture. It also contains pointers to "
"more information and information on how to make the most of your new Debian "
"system."
msgstr ""
"本文件包含了在 &arch-title; (<quote>&architecture;</quote>) 硬體架構下 "
"&debian; &release; 系統的安裝指引。文章內容還包括建立最適合您的 Debian 系統的"
"資訊，以及其他相關資料的超鏈結。"

#. Tag: para
#: bookinfo.xml:17
#, no-c-format
msgid ""
"Because the &arch-title; port is not a release architecture for "
"&releasename;, there is no official version of this manual for &arch-title; "
"for &releasename;. However, because the port is still active and there is "
"hope that &arch-title; may be included again in future official releases, "
"this development version of the Installation Guide is still available."
msgstr ""

#. Tag: para
#: bookinfo.xml:25
#, no-c-format
msgid ""
"Because &arch-title; is not an official architecture, some of the "
"information, and especially some links, in this manual may be incorrect. For "
"additional information, please check the <ulink url=\"&url-ports;"
"\">webpages</ulink> of the port or contact the <ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; mailing list</ulink>."
msgstr ""

#. Tag: para
#: bookinfo.xml:36
#, no-c-format
msgid ""
"This installation guide is based on an earlier manual written for the old "
"Debian installation system (the <quote>boot-floppies</quote>), and has been "
"updated to document the new Debian installer. However, for &architecture;, "
"the manual has not been fully updated and fact checked for the new "
"installer. There may remain parts of the manual that are incomplete or "
"outdated or that still document the boot-floppies installer. A newer version "
"of this manual, possibly better documenting this architecture, may be found "
"on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</ulink>. You "
"may also be able to find additional translations there."
msgstr ""
"本安裝指南是基於一份舊版 Debian 安裝系統 (\"boot-floppies\") 手冊而撰寫的，並"
"且針對新的 Debian 安裝系統進行了增補和修訂。但是，在 &architecture; 架構下，"
"本手冊尚未對新版的安裝程式進行完整的更新和實際檢測。這使得本手冊中可能會存在"
"少量不完整、過時的、或者仍然在介紹 boot-floppies 安裝程式的內容。本手冊的新版"
"本也許會針對本硬體架構做更好的說明指引，可在 <ulink url=\"&url-d-i;\">&d-i; "
"首頁</ulink>找到它。您還可以在那裡找到一些其他語言的翻譯版本。"

#. Tag: para
#: bookinfo.xml:49
#, no-c-format
msgid ""
"Although this installation guide for &architecture; is mostly up-to-date, we "
"plan to make some changes and reorganize parts of the manual after the "
"official release of &releasename;. A newer version of this manual may be "
"found on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>. You may also be able to find additional translations there."
msgstr ""
"雖然這份 &architecture; 架構的安裝指南是最新的版本，但我們計畫在官方發佈 "
"&releasename; 之後繼續對手冊進行編修的動作。更新的手冊可在 <ulink url=\"&url-"
"d-i;\">&d-i; 首頁</ulink>上找到。您還可以在那裡找到一些其他語言的翻譯版本。"

#. Tag: para
#: bookinfo.xml:58
#, no-c-format
msgid ""
"Translators can use this paragraph to provide some information about the "
"status of the translation, for example if the translation is still being "
"worked on or if review is wanted (don't forget to mention where comments "
"should be sent!). See build/lang-options/README on how to enable this "
"paragraph. Its condition is \"translation-status\"."
msgstr ""

#. Tag: holder
#: bookinfo.xml:75
#, no-c-format
msgid "the Debian Installer team"
msgstr "Debian Installer 團隊"

#. Tag: para
#: bookinfo.xml:79
#, no-c-format
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License. Please refer to the license in "
"<xref linkend=\"appendix-gpl\"/>."
msgstr ""
"本手冊屬於自由軟體，您可以在 GNU 通用公共許可證 (GPL) 的條約下重新發佈和 "
"(或) 修改它。該許可證的內容請參考 <xref linkend=\"appendix-gpl\"/>。"
