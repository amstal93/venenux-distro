# Simplified Chinese translation for win32-loader.
# Copyright (C) 2007-2008 The win32-loader project.
# This file is distributed under the same license as the win32-loader package.
# Deng Xiyue <manphiz@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-03-22 12:30+0000\n"
"PO-Revision-Date: 2008-05-02 11:29+0800\n"
"Last-Translator: Deng Xiyue <manphiz@gmail.com>\n"
"Language-Team: Simplified Chinese <debian-chinese-gb@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_SIMPCHINESE"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "gbk"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp936"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Chinese (Simplified)"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian 安装程序"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "SimpChinese.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Debian 安装程序加载器"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "无法找到 win32-loader.ini"

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini 不完整。请联系此文件的提供者。"

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr "本程序检测到您的键盘类型是“$0”。这是正确的吗？"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"请发送错误报告并包含以下信息：\n"
"\n"
" - Windows 的版本。\n"
" - 国家设置。\n"
" - 真实键盘类型。\n"
" - 检测到的键盘类型。\n"
"\n"
"谢谢您。"

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"驱动器 $c 似乎没有足够的空闲空间。为了完整安装桌面系统，建议至少有 3 GB 的剩"
"余空间。如果已经有独立的磁盘或分区来安装 Debian，或者您计划完全覆盖 Windows，"
"您可以安全的忽略此警告。"

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "错误：没有足够的空闲磁盘空间。终止安装。"

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "本程序尚未支持 Windows $windows_version。"

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"您正在试图安装的 Debian 版本是为运行在现代64位计算机系统上设计的。但是，您的"
"计算机不能运行64位程序。\n"
"\n"
"请使用32位(“i386”)版本的 Debian，或者多架构版本以安装任意版本。\n"
"\n"
"本安装程序现在将终止。"

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"您的计算机可以运行现代64位操作系统。但您正在试图安装的 Debian 版本是为旧的32"
"位硬件设计的。\n"
"\n"
"您仍然可以进行此安装，但为了能最大限度发挥您计算机的性能，我们建议您使用64位"
"(“amd64”)版本的 Debian，或者多架构版本以安装任意版本。\n"
"\n"
"现在您希望终止安装吗？"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "选择安装模式："

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "标准模式。建议大多数用户使用。"

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr "专家模式。建议想完全控制安装过程的专家用户使用。"

#: win32-loader.c:84
msgid "Select action:"
msgstr "选择动作："

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "在这台计算机上安装 Debian GNU/Linux"

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "修复已有的 Debian 系统(维修模式)"

#: win32-loader.c:88
msgid "Graphical install"
msgstr "图形化安装"

#: win32-loader.c:89
msgid "Text install"
msgstr "文本化安装"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "正在下载 %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "正在连接 ..."

#: win32-loader.c:92
msgid "second"
msgstr "秒"

#: win32-loader.c:93
msgid "minute"
msgstr "分钟"

#: win32-loader.c:94
msgid "hour"
msgstr "小时"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr " "

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) 共 %dkB 速度 %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (还剩 %d %s%s)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "请选择使用哪个版本的 Debian 安装程序："

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "稳定版。这将安装 Debian “稳定版”。"

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"每日编译版。这是开发版本的 Debian 安装程序。它将默认安装 Debian “测试版”，而"
"且也能够安装“稳定版”或“非稳定版”。"

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr "我们建议您查看在使用每日编译版之前先查看其已知问题。您现在想查看吗？"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "桌面环境："

#: win32-loader.c:114
msgid "None"
msgstr "无"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Debian 安装程序加载器将会设置使用以下参数。请“不要”做任何修改除非您知道您在做"
"什么。"

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "代理设置(主机:端口)："

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "boot.ini 的位置："

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "网络启动映像(linux 和 initrd.gz)的路径："

#: win32-loader.c:119
msgid "Error"
msgstr "错误"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "错误：复制 $0 到 $1 失败。"

#: win32-loader.c:121
msgid "Generating $0"
msgstr "正在创建 $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "正在将预播种信息加到 $0 之后"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "错误：无法运行 $0。"

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "正在禁用引导文件中的 NTFS 压缩"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "正在 NT 加载器中注册 Debian 安装程序"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "正在启动管理器中注册 Debian 安装程序"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "错误：无法解析 bcdedit.exe 的输出。"

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "错误：无法找到 boot.ini< 。这确实是 Windows $windows_version 吗？"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "“非常重要的提示”：\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"第二阶段的安装过程现在即将开始。在您确认以后，本程序将会重启 Windows 到 DOS "
"模式，并自动加载 Debian 安装程序。\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"您需要重启来继续进行您的 Debian 安装。在您下次启动过程中，您将会被询问是否想"
"要启动 Windows 或者 Debian 安装程序。选择 Debian 安装程序来继续安装进程。\\n"
"\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"在安装过程中，您将可以选择减少您的 Windows 分区来安装 Debian 或者完全替"
"换。“强烈建议”您事先备份您的数据。本加载器的作者和 Debian 项目对数据的丢失“不"
"会负任何责任”。\\n\\n一旦您的 Debian 安装完成(而且您选择在您的磁盘上保留 "
"Windows)，您可以通过 Windows 控制面板中的添加/删除程序对话框来卸载 Debian 安"
"装程序。"

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "您想现在重新启动吗？"

#~ msgid "Debconf preseed line:"
#~ msgstr "Debconf 预置行："
