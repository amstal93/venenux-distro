<!-- retain these comments for translator revision tracking -->
<!-- original version: 43789 -->

 <sect2 arch="alpha" id="alpha-firmware">
 <title>Alpha Console-Firmware</title>
<para>

Die Konsolen-Firmware ist in einem Flash-ROM gespeichert und wird gestartet,
wenn ein Alpha-System eingeschaltet oder resettet wird. Es gibt zwei
verschiedene Konsolen-Spezifikationen auf Alpha-Systemen und deswegen
sind auch zwei Arten von Konsolen-Firmware verfügbar:

</para>

<itemizedlist>
<listitem><para>

  <emphasis>SRM Console</emphasis> basiert auf der Alpha Console
  Subsystem-Spezifikation, die eine Betriebsumgebung für OpenVMS, Tru64 UNIX
  und Linux-Betriebssysteme zur Verfügung stellt.

</para></listitem>
<listitem><para>

  <emphasis>ARC, AlphaBIOS oder ARCSBIOS Console</emphasis> basiert auf der
  Advanced RISC Computing-(ARC)Spezifikation, die eine Betriebsumgebung für
  Windows NT zur Verfügung stellt.

</para></listitem>
</itemizedlist>

<para>

Aus der Perspektive des Benutzers gesehen ist der wichtigste Unterschied
zwischen SRM und ARC, dass die Wahl der Konsole die möglichen
Partitionsschemata der Festplatte einschränkt, von der Sie booten möchten.

</para><para>

ARC erfordert, dass Sie eine MS-DOS-Partitionstabelle für die
Boot-Platte verwenden (wie sie durch <command>cfdisk</command>
erstellt wird). Deswegen ist die MS-DOS-Partitionstabelle das
<quote>native</quote> Partitionsformat, wenn man von ARC bootet. Da
AlphaBIOS allerdings ein Festplatten-Partitionierungswerkzeug enthält,
sollten Sie es vorziehen, Ihre Festplatten über das Firmware-Menü
zu partitionieren, bevor Sie Linux installieren.

</para><para>

Auf der anderen Seite ist SRM <emphasis>inkompatibel</emphasis><footnote>

<para>
Genauer gesagt, kollidiert das Format des Boot-Sektors, das
die Console-Subsystem-Spezifikation verlangt, mit der Platzierung der
DOS-Partitionstabelle.
</para>

</footnote> zu MS-DOS-Partitionstabellen. Da Tru64 Unix das
BSD-Disklabel-Format benutzt, ist dies das <quote>native</quote>
Partitionsformat für SRM-Installationen.

</para><para>

GNU/Linux ist das einzige Betriebssystem, das auf Alpha-Systemen von
beiden Konsolentypen gebootet werden kann, aber &debian; &release; unterstützt
nur das Booten von SRM-basierten Systemen. Wenn Sie eine Alpha-Maschine
haben, für die keine SRM-Version verfügbar ist, wenn Sie ein Dual-Boot-System
mit Windows-NT aufbauen möchten oder wenn Ihr Boot-Laufwerk für die
BIOS-Initialisierung ARC-Konsolen-Support benötigt, können Sie nicht den
&debian; &release;-Installer verwenden. Sie können aber trotzdem
&debian; &release; auf dieser Maschine laufen lassen, indem Sie andere
Installationsmedien benutzen; z.B. können Sie zunächst Debian Woody mit
MILO installieren und dann auf ein aktuelles Debian aktualisieren.

</para><para>

Da <command>MILO</command> für keines der Alpha-Systeme der laufenden
Produktion (seit Februar 2000) verfügbar ist und weil es nicht mehr
nötig ist, eine OpenVMS- oder Tru64 Unix-Lizenz zu kaufen, um
SRM-Firmware für ältere Alpha-Systeme zu bekommen, wird empfohlen,
dass Sie wenn möglich SRM benutzen.

</para><para>

Die folgende Tabelle fasst verfügbare und unterstützte Systemtypen
bzw. Konsolenkombinationen zusammen (<xref linkend="alpha-cpus"/>
enthält Infos über die Systemtypen-Bezeichnungen). Das Wort <quote>ARC</quote>
in der Tabelle kennzeichnet alle ARC-konformen Konsolen.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry>Systemtyp</entry>
  <entry>Unterstützter Konsolentyp</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry>ARC (siehe das Handbuch des Motherboards) oder SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>nur ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>nur SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC oder SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>nur ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>nur ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

Generell kann keine der Konsolen Linux direkt booten; es ist immer
die Hilfe eines zwischengeschalteten Bootloaders erforderlich. Für die
SRM-Konsole gibt es <command>aboot</command>, einen kleinen,
plattform-unabhängigen Bootloader. Sehen Sie sich auch das (leider veraltete)
<ulink url="&url-srm-howto;">SRM-Firmware-HowTo</ulink> an.

</para><para condition="FIXME">

<!--obsolet - MILO -->
Die folgenden 5 Abschnitte stammen aus dem Woody-Installationshandbuch und
sind hier nur als Referenz eingefügt; vielleicht sind Sie zu einem späteren
Zeitpunkt mal nützlich, wenn Debian eventuell wieder MILO-basierte Installationen
unterstützt. Sie sind aktuell für &releasename-cap; nicht anwendbar.

</para><para condition="FIXME">

Generell kann keine der Konsolen Linux direkt booten; es ist immer
die Hilfe eines zwischengeschalteten Bootloaders erforderlich. Es
gibt derer zwei Stück: <command>MILO</command> und <command>aboot</command>.

</para><para condition="FIXME">

<command>MILO</command> ist selbst auch eine Konsole, die im Speicher
ARC oder SRM ersetzt. <command>MILO</command> kann sowohl von ARC wie von
SRM gestartet werden und ist der einzige Weg, um Linux von der ARC-Konsole
zu booten. <command>MILO</command> ist plattform-spezifisch (unterschiedliche
Versionen von <command>MILO</command> für die verschiedenen Systemtypen sind
nötig) und existiert nur für solche Systeme, die in der Tabelle mit ARC-Support
aufgelistet sind. Lesen Sie auch das (leider überholte)
<ulink url="&url-milo-howto;">MILO HowTo</ulink>.

</para><para condition="FIXME">

<command>aboot</command> ist ein kleiner, plattform-unabhängiger Bootloader,
der nur auf SRM läuft. Siehe auch das (leider ebenfalls veraltete)
<ulink url="&url-srm-howto;">SRM HowTo</ulink>, wenn Sie mehr Infos über
<command>aboot</command> wünschen.

</para><para condition="FIXME">

Es gibt also drei mögliche Szenarios, abhängig von der Konsolen-Firmware
des Systems und ob <command>MILO</command> verfügbar ist oder nicht:

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

</para><para condition="FIXME">

Da <command>MILO</command> für keines der Alpha-Systeme der laufenden
Produktion (seit Februar 2000) verfügbar ist und weil es nicht mehr
nötig ist, eine OpenVMS- oder Tru64 Unix-Lizenz zu kaufen, um
SRM-Firmware für ältere Alpha-Systeme zu bekommen, wird empfohlen,
dass Sie SRM und <command>aboot</command> für GNU/Linux-Installationen
benutzen, außer Sie haben ein Dual-Boot-System mit Windows NT.

</para><para condition="FIXME">

Ende der (überholten) Kapitel aus Woody.

</para><para>

Die Mehrzahl von AlphaServer-Systemen und aller aktuellen Server und
Workstations enthalten sowohl SRM wie auch AlphaBIOS in ihrer Firmware.
Bei <quote>half-flash</quote>-Maschinen wie den verschiedenen Evaluation-Boards
ist es möglich, zwischen den beiden Versionen umzuschalten, indem man die
Firmware neu flasht. Auch ist es möglich, ARC/AlphaBIOS von einer
Floppy zu starten, wenn SRM einmal installiert ist (benutzen Sie dazu den
<command>arc</command>-Befehl). Aus den bereits erwähnten Gründen
empfehlen wir, auf SRM umzusteigen, bevor &debian; installiert wird.

</para><para>

Wie auf anderen Architekturen auch sollten Sie die neueste verfügbare
Firmware-Version installieren,<footnote>

<para>Außer auf Jensen, wo
Linux auf Firmware-Versionen neuer als 1.7 nicht unterstützt
wird &ndash; siehe <ulink url="&url-jensen-howto;"></ulink> für mehr
Informationen.</para>

</footnote> bevor Sie &debian; installieren.
Für Alpha-Systeme können Sie Firmware-Updates auf
<ulink url="&url-alpha-firmware;">Alpha Firmware Updates</ulink>
erhalten.

</para>
 </sect2>


  <sect2 arch="alpha"><title>Per TFTP booten</title>
<para>

Unter SRM sind Ethernet-Schnittstellen mit <userinput>ewa</userinput>
benannt und können mit dem <userinput>show dev</userinput>-Befehl aufgelistet
werden, wie hier (leicht verändert):

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

Als erstes müssen Sie das Boot-Protokoll einstellen:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

Stellen Sie jetzt sicher, dass der richtige Medientyp eingestellt ist:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>

Mit <userinput>&gt;&gt;&gt;set ewa0_mode</userinput> bekommen Sie eine Liste
der möglichen Modi.

</para><para>

Um jetzt von der ersten Ethernet-Schnittstelle zu booten, geben Sie ein:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags ""
</screen></informalexample>

So wird mit den Standard-Kernel-Parametern gebootet, wie Sie im netboot-Image
enthalten sind.

</para><para>

Wollen Sie eine serielle Konsole benutzen, <emphasis>müssen</emphasis>
Sie dem Kernel den Parameter <userinput>console=</userinput> übergeben.
Dies wird mit dem <userinput>-flags</userinput>-Argument gemacht, das zum
SRM-<userinput>boot</userinput>-Kommando hinzugefügt wird. Die seriellen Ports
sind wie die entsprechenden Dateien in <userinput>/dev</userinput> benannt.
Wenn Sie zusätzliche Kernelparameter verwenden, müssen Sie bestimmte
Standardoptionen ebenfalls mit angeben, die von den &d-i;-Images
benötigt werden. Ein Beispiel: um von <userinput>ewa0</userinput>
zu starten und die Konsole auf dem ersten seriellen Port zu nutzen,
müssen Sie eintippen:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags &quot;root=/dev/ram ramdisk_size=16384 console=ttyS0&quot;
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha"><title>Booten von CD-ROM mit der SRM-Konsole</title>
<para>

Die &debian;-Installations-CDs enthalten verschiedene vorkonfigurierte
Boot-Optionen für VGA und serielle Konsole. Tippen Sie

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

ein, um mittels VGA-Konsole zu booten, wobei <replaceable>xxxx</replaceable>
die SRM-Bezeichnung Ihres CD-ROM-Laufwerk ist. Um die serielle Konsole auf
dem ersten seriellen Port zu nutzen, tippen Sie folgendes ein:

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

Und für die Konsole auf dem zweiten seriellen Port dies:

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <title>Booten von CD-ROM mit der ARC- oder AlphaBIOS-Konsole</title>
<para>

Um per ARC-Konsole von CD-ROM zu booten, finden Sie den Codenamen Ihrer
Unterarchitektur heraus (lesen Sie dazu <xref linkend="alpha-cpus"/>);
geben Sie dann <filename>\milo\linload.exe</filename> als Bootloader
und <filename>\milo\<replaceable>subarch</replaceable></filename>
als Betriebssystem-Pfad (<quote>OS Path</quote>) im <quote>OS Selection
Setup</quote>-Menü ein (wobei <replaceable>subarch</replaceable> der korrekte
Name Ihrer Unterarchitektur ist). Ruffian-Maschinen bilden eine Ausnahme:
hier müssen Sie <filename>\milo\ldmilo.exe</filename> als Bootloader
eintragen.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Booten von Floppy mit der SRM-Konsole</title>
<para>

Geben Sie am SRM-Prompt (<prompt>&gt;&gt;&gt;</prompt>) Folgendes ein:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

Möglicherweise müssen Sie <filename>dva0</filename> durch die korrekte
Gerätebezeichnung ersetzen. Normalerweise ist <filename>dva0</filename>
das Floppy-Laufwerk; tippen Sie

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

ein, um eine Liste der Geräte zu bekommen (z.B. auch, wenn Sie von CD booten
möchten). Beachten Sie: wenn Sie via MILO booten, wird das
<command>-flags</command>-Argument ignoriert; Sie können also nur
<command>boot dva0</command> benutzen.
Wenn alles richtig läuft, werden Sie eventuell sehen, wie der Linux-Kernel
startet.

</para><para>

Wenn Sie via <command>aboot</command> starten und Kernelparameter
angeben wollen, nutzen Sie folgenden Befehl:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 arguments"
</screen></informalexample>

(alles in einer Zeile); ersetzen Sie dabei bei Bedarf
<filename>dva0</filename> durch die korrekte SRM-Bezeichnung des Boot-Laufwerks,
<filename>fd0</filename> durch die Linux-Bezeichnung des Bootlaufwerks und
<filename>arguments</filename> durch die gewünschten Kernelparameter.

</para><para>

Wenn Sie via <command>MILO</command> booten und dabei Kernelparameter
angeben wollen, müssen Sie den Boot-Prozess unterbrechen, sobald Sie MILO
erreichen. Lesen Sie dazu <xref linkend="booting-from-milo"/>.

</para>
  </sect2>


  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Booten von Floppy mit der ARC- oder AlphaBIOS-Konsole</title>

<para>

Im Betriebssystem-Auswahlmenü (<quote>OS Selection menu</quote>) stellen Sie
<command>linload.exe</command> als Bootloader ein und <command>milo</command>
als Betriebssystem-Pfad (<quote>OS Path</quote>). Bootstrap benutzt dann den
neu erstellten Eintrag.

</para>
  </sect2>

 <sect2 arch="alpha" condition="FIXME" id="booting-from-milo"><title>Booten mit MILO</title>
<para>

<!-- obsolet - MILO -->
Dieser Abschnitt ist überholt und stammt aus dem Installationshandbuch
für Woody. Er ist derzeit für &releasename-cap; nicht anwendbar.

</para><para>

MILO (auf dem Boot-Medium enthalten) ist konfiguriert, direkt nach
Linux durchzustarten. Wenn Sie hier eingreifen wollen, müssen Sie
während des MILO-Countdowns nur die Leertaste drücken.

</para><para>

Wenn Sie alles explizit angeben möchten (z.B. um zusätzliche Parameter
hinzuzufügen), nutzen Sie einen Befehl wie diesen:

<informalexample><screen>
MILO&gt; boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1 <!-- arguments -->
</screen></informalexample>

Wenn Sie von einem anderen Medium als von Floppy starten, ersetzen Sie
in dem obigen Beispiel <filename>fd0</filename> durch die entsprechende
Gerätebezeichnung (im Linux-Format). Das Kommando <command>help</command>
gibt eine kurze Einführung in die MILO-Kommandos.

</para>
 </sect2>
