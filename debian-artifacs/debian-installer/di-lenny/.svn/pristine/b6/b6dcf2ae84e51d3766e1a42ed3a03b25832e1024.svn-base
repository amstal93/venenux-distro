<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 23660 -->
<!-- Piarres Beobidek egina 2004-ko Azaroaren 25-eam -->

 <sect2 arch="not-s390" id="PPP">
 <title>PPP konfiguratzen</title>

<para>

Isntalazioaren lehen atalean ez bada saretzerik konfiguratu instalazioaren 
falta dena ppp bidez konfiguratu nahi den galdetuko du. PPP modem bidez
egindako konexioak sortzen dituen protokolo bat da, instalazio sistemak
seguridade eguneraketa edo pakete gehigarria behar bait ditzake 
instalazioaren hurrengo pausuetan.
Ordenagailuan ez badago modemik edo konfigurazio hori beranduago egoin 
nahi izan ezkero atal hau utz dezakzu.

</para><para>

PPP konexioa zuzen konfiguratzeko, zure internet hornitzailearen datu
batzuzuek beharrezkoak dira, telefono zenbakia, erabiltzaile izena, 
pasahitza eta DNS zerbitzariak (aukerakoa). Zenbait hornitzailek
Linuxerako instalazio gidaliburuak dituzte. Zuk informazio hau erabili
dezakzu nahiz beriek ez debianeraok denik esan konfiguraizo aldagai 
(eta software) gehiena berdinak bait dira Linux banaketa gehienetean.

</para><para>

PPP momentu honetan konfiguratzea aukeratu ezkero,
<command>pppconfig</command> izeneko programa bat abiaraziko da,
programa honek PPP kofgurazio ezarpenak egiten lagunuduko du.
<emphasis>ziurtatu zaitez, programak markatze konexiorako izen
eskatzen dizunean <userinput>provider</userinput> erabiltzen 
duzula.</emphasis>

</para><para>

<command>pppconfig</command> programak arazo gabeko PPP konexio
ezarpen bat egiteakoan gidatuko zaitu. Hala ere honek zure kasuan
ez funtzionatu ezkero begiratu argibide zehatzagoak aurrerago.

</para><para>

PPP konfiguratu ahal izateko GNU/Linux-pean fitxategia ikusi eta 
editatzeari buruzko oinarrizko gauza batzuk jakin behar dira.
Fitxaegiak ikusteko <command>more</command> erabil dezakezu,
<command>zmore</command> <userinput>.gz</userinput> extensio
erabiliaz konprimitutarikoentzat. Adibidez, 
<filename>README.debian.gz</filename> fitxategia ikusteko,
<userinput>zmore README.debian.gz</userinput> idatzi. Sistema
oinarriak <command>nano</command> nano izeneko editore 
oso simplea baina aukea askotako dakar. Ziurrenik beranduago
editore ahaltsuago batenbat instalatuko duzu, 
<command>jed</command>, <command>nvi</command>,
<command>less</command>, edo <command>emacs</command> belakoa.

</para><para>


<filename>/etc/ppp/peers/provider</filename> fitxategia editatu etae
<userinput>/dev/modem</userinput> lerroaz
<userinput>/dev/ttyS<replaceable>&num;</replaceable></userinput> 
aldatu non <replaceable>&num;</replaceable> erabili nahi den 
serial atakaren zenbakia da. Linuxpean serial atakak 0-tik 
kontatzen hasten dira.

<phrase arch="powerpc;m68k">OSeria taka dituen Macintosh-etan modem
ataka <filename>/dev/ttyS0</filename> da eta inprimagailuaren 
<filename>/dev/ttyS1</filename>.</phrase>

Hurrengo pausua 
<filename>/etc/chatscripts/provider</filename> editatu eta 
hornitzailearen telefono zenbakia, erabiltzaile izena eta
pasahitza  ipintzea da. Mesedez ez ezabatu pasahitzak aurretik
duen <quote>\q</quote> honek sistema erregistrotan zure pasahitza
ager ez dadin balio du eta.


</para><para>

Zenbait honitzailek testu lauaren ordezPAP edo CHAP sarrera sekuentziak 
erabiltzen dituzte. Beste batzuek biak. Zure hornitzaileak PAP edo
CHAP erabili ezkero, hurrengo pausuak egin beharko dituzu.
Iradokitu ezazu (aurretik '#' ipniaz) markatze kateeen ondoren
dagoen guztia (<quote>ATDT</quote> hasten dena)
<filename>/etc/chatscripts/provider</filename> fitxategian, aldatu
<filename>/etc/ppp/peers/provider</filename> fitxategia beherago 
esplikatzen den bezala eta gehitu 
<userinput>user <replaceable>izena</replaceable></userinput> non
<replaceable>izena</replaceable> konekatu nahi den hornitzailearen 
izen da. Gero editatu <filename>/etc/ppp/pap-secrets</filename> edo
<filename>/etc/ppp/chap-secrets</filename> fitxategia eta pasahitza
ezarri.


</para><para>

Gero <filename>/etc/resolv.conf</filename> fitxategia ediatu
eta gehitu zure hornitzailearen izen zerbitzarien (DNS) IP 
helbideak. <filename>/etc/resolv.conf</filename> fitxategia
formatu honetan da:
<userinput>nameserver
<replaceable>xxx.xxx.xxx.xxx</replaceable></userinput> non
<replaceable>x</replaceable> IP helbideko zenbakiak diren.
NAhi izan ezkero <userinput>usepeerdns</userinput> aukera 
gehitu dezakezu <filename>/etc/ppp/peers/provider</filename>
fitxategian normalean urrutiko ekipoak ematen dituen DNS 
zerbitzariak erabiltzeko.

</para><para>

Hornitzaileak ez badu gehiengoarendik ezberdindutako sarrera
sekuentziarik erabiltzen,dena prest dago! PPP konexioa abiarazi
<command>pon</command> root bezala, eta erabilitako prozesua
<command>plog</command> komandoa erabiliaz begiratu. Deskonektatzeko
<command>poff</command>, erabili, berriz root bezala

</para><para>

Irakurri <filename>/usr/share/doc/ppp/README.Debian.gz</filename> 
fitxategia Debianen PPP erabilerari buruz gehiago jakiteko.



</para><para>

SLIP konexio estatikoentzat, 
<userinput>slattach</userinput> koomandoa (
<classname>net-tools</classname> paketekoa) erabili beharko duzu
<filename>/etc/init.d/network</filename> fitxategian. SLIP dinamikoak
<classname>gnudip</classname> paketea behar du.

</para>

  <sect3 id="PPPOE">
  <title>PPP Ethernet gainetik (PPPOE) konfiguratzen </title>

<para>


PPPOE zenbait banda zabaleko hornitzailek erabiltzen duten  PPP 
protokoloa. Oraindik ez dago onarpenik oinarri konfigurazioan
hau konfiguratzen laguntzeko. Hala behar den softwarea gehitu
egin da beraz zuk PPPOE eskuz momentu honetan konfigura dezakezu
VT2-ra aldatzen (Alt+F2) eta <command>pppoeconf</command> abiarazten.

</para>

  </sect3>
 </sect2>
