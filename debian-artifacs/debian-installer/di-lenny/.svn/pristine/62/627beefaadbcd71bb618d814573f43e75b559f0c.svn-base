# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Spanish messages for debian-installer.
# Copyright (C) 2003-2007 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Contributors to the translation of debian-installer:
# Teófilo Ruiz Suárez <teo@debian.org>, 2003.
# David Martínez Moreno <ender@debian.org>, 2003, 2005.
# Carlos Alberto Martín Edo <carlos@dat.etsit.upm.es>, 2003
# Carlos Valdivia Yagüe <valyag@dat.etsit.upm.es>, 2003
# Rudy Godoy <rudy@kernel-panik.org>, 2003-2006
# Steve Langasek <vorlon@debian.org>, 2004
# Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>, 2005
# Rubén Porras Campo <nahoo@inicia.es>, 2005
# Javier Fernández-Sanguino <jfs@debian.org>, 2003-2007
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   http://www.debian.org/intl/spanish/
#   especialmente las notas de traducción en
#   http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (debian-l10n-spanish@lists.debian.org)
#
# NOTAS: 
#
# - Se ha traducido en este fichero 'boot loader' de forma homogénea por
# 'cargador de arranque' aunque en el manual se utiliza éste término y
# también 'gestor de arranque'
#
# - 'array' no está traducido aún. La traducción como 'arreglo' suena
# fatal (y es poco conocida)
#
#  
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-06 22:55+0200\n"
"Last-Translator: Javier Fernández-Sanguino Peña <jfs@debian.org>\n"
"Language-Team:  Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Modo rescate"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Acceder al modo rescate"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "No se encontraron particiones"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"El instalador no pudo encontrar ninguna partición por lo que no podrá montar "
"un sistema de ficheros raíz. Esto puede deberse a que el núcleo no haya "
"podido detectar su disco duro, no ha podido leer la tabla de particiones o "
"el disco está sin particionar. Si lo desea, puede investigar ésto utilizando "
"el intérprete de órdenes en el entorno del instalador."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Dispositivo a utilizar como sistema de ficheros raíz:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Introduzca un dispositivo que desea utilizar como sistema de ficheros raíz. "
"Podrá elegir entre una serie de operaciones de rescate a realizar en este "
"sistema de ficheros."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "No existe ese dispositivo"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"No existe el dispositivo (${DEVICE}) que introdujo para el sistema de "
"ficheros raíz. Intente de nuevo."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Fallo en el montaje"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Se produjo un error al montar el dispositivo que introdujo como el sistema "
"de ficheros raíz (${DEVICE}) en «/target»."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Por favor, consulte el «syslog» para más información."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Operación de rescate"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Se produjo un error en la operación de rescate"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"Se produjo un error en la operación de rescate «${OPERATION}», indicó el "
"código de error ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Ejecutar un intérprete de órdenes en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Ejecutar un intérprete de órdenes en el entorno del instalador"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Escoger un sistema de ficheros raíz distinto"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Reiniciar el sistema"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Ejecutando un intérprete de órdenes"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Tras este mensaje, se le ofrecerá un intérprete de órdenes en ${DEVICE} con "
"dicho sistema montado en «/». Deberá montar otros sistemas de ficheros usted "
"mismo si los necesita (como, por ejemplo, si tiene un «/usr» separado)"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr ""
"Se ha producido un error al ejecutar el intérprete de órdenes en «/target»"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Se encontró un intérprete (${SHELL}) en el sistema de ficheros raíz "
"(${DEVICE}) pero se produjo un error al intentar ejecutarlo."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "No se ha encontrado ningún intérprete de órdenes en «/target»"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"No se encontró ningún intérprete de órdenes que pudiera utilizarse en el "
"sistema de ficheros raíz (${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Intérprete de órdenes interactivo en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Tras este mensaje, se le ofrecerá un intérprete de órdenes en ${DEVICE} con "
"dicho sistema en «/target». Puede trabajar en él con las herramientas "
"disponibles en el entorno de instalación. Ejecute «chroot /target» si quiere "
"hacer que éste sea su sistema de ficheros raíz temporalmente. Deberá montar "
"otros sistemas de ficheros usted mismo si los necesita (como, por ejemplo, "
"si tiene un «/usr» separado)."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Tras este mensaje, se le ofrecerá un intérprete de órdenes en el entorno del "
"instalador. No se ha montado ningún sistema de ficheros ya que el instalador "
"no encontró ninguna partición."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Intérprete de órdenes interactivo en el entorno del instalador"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Frase de contraseña para ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Debe elegir una frase de contraseña para el volumen cifrado ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"El volumen no podrá utilizarse durante las operaciones de rescate si no "
"introduce aquí ningún valor."
