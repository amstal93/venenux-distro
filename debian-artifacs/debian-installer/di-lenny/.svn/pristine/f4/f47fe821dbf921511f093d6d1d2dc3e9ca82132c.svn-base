<!-- retain these comments for translator revision tracking -->
<!-- original version: 56425 -->

<para>

Wenn Sie sich mit Partitionierung nicht auskennen oder einfach mehr Details
benötigen, lesen Sie <xref linkend="partitioning"/>.

</para>
<warning arch="sparc"><para>
<!-- BTS: #384653 -->

Wenn eine Festplatte früher bereits unter Solaris genutzt wurde, könnte es sein,
dass der Partitionierer die Größe der Platte nicht korrekt erkennt. Auch das
Erzeugen einer neuen Partitionstabelle löst dieses Problem nicht. Was aber hilft
ist, die ersten paar Sektoren der Festplatte mit Nullen zu überschreiben:

<informalexample><screen>
# dd if=/dev/zero of=/dev/hd<replaceable>X</replaceable> bs=512 count=2; sync
</screen></informalexample>

Beachten Sie, dass Sie durch diese Aktion auf alle vorhandenen Daten auf der
Platte nicht mehr zugreifen können!

</para></warning>
<para>

Zuerst wird Ihnen die Auswahl geboten, automatisch entweder eine gesamte Platte
zu partitionieren oder nur den freien Speicherplatz auf einer Platte, falls
solcher vorhanden ist. Dies wird auch als <quote>Geführte
Partitionierung</quote> bezeichnet. Wenn Sie keine automatische Partitionierung
wollen, wählen Sie <guimenuitem>Manuell</guimenuitem>.

</para>
 
   <sect3 id="partman-auto">
   <title>Geführte Partitionierung</title>
<para>

Wenn Sie Geführte Partitionierung wählen, haben Sie drei Möglichkeiten:
Partitionen direkt auf der Festplatte erzeugen (die klassische Methode),
LVM (Logical Volume Management) benutzen oder verschlüsseltes LVM
benutzen<footnote>

<para>
Der Installer verschlüsselt die LVM-Volumegruppe mittels eines
256 Bit-AES-Schlüssels und nutzt dazu die
<quote>dm-crypt</quote>-Unterstützung des Kernels.
</para>

</footnote>.

</para>
<note><para>

Die Option zur Nutzung von LVM (Standard oder verschlüsselt) ist
möglicherweise nicht auf allen Architekturen verfügbar.

</para></note>
<para>

Bei der Verwendung von LVM oder verschlüsseltem LVM erzeugt der Installer
die meisten Partitionen innerhalb einer großen Partition; der Vorteil dabei
ist, dass die Partitionen innerhalb der großen Partition sehr einfach
nachträglich in der Größe verändert werden können. Im Fall von verschlüsseltem
LVM ist die große Partition ohne Kenntnis einer speziellen Passphrase nicht
lesbar und bietet deshalb zusätzliche Sicherheit Ihrer (persönlichen) Daten.

</para><para>

Wenn Sie verschlüsseltes LVM verwenden, wird der Installer auch automatisch
die Festplatte löschen, indem Zufallsdaten darauf geschrieben werden. Dies
verbessert zusätzlich die Sicherheit (da es so unmöglich ist, zu erkennen,
welche Teile der Platte genutzt werden und außerdem alle Spuren von
früheren Installationen gelöscht werden), könnte aber einiges an Zeit
benötigen, abhängig von der Größe der Festplatte.

</para>
<note><para>

Wenn Sie Geführte Partitionierung mittels LVM oder verschlüsseltem LVM wählen,
müssen einige Änderungen in der Partitionstabelle auf die ausgewählten
Festplatten geschrieben werden, während LVM eingerichtet wird. Diese Änderungen
löschen effektiv alle Daten, die zu dieser Zeit auf den entsprechenden
Festplatten gespeichert sind und sind nicht rückgängig zu machen! Der
Installer fordert Sie aber auf, diese Änderungen zu bestätigen, bevor sie auf
die Platten geschrieben werden.

</para></note>
<para>

Nachdem Sie Geführte Partitionierung (entweder klassisch oder mittels
(verschlüsseltem) LVM) für eine ganze Festplatte ausgewählt haben, müssen
Sie die Platte angeben, die Sie nutzen möchten. Überprüfen Sie, ob alle
Ihre Festplatten aufgelistet sind und, falls Sie mehrere haben, achten Sie
darauf, dass Sie die richtige wählen. Die Reihenfolge, in der sie aufgelistet
sind, könnte anders sein als sie es gewohnt sind. Die Größe der Festplatten
kann Ihnen helfen, sie zu identifizieren.

</para><para>

Alle Daten auf den ausgewählten Festplatten könnten verloren gehen,
aber Sie werden immer aufgefordert, jegliche Änderungen zu bestätigen, bevor
Sie auf die Festplatte geschrieben werden. Wenn Sie die klassische
Partitionierungsmethode gewählt haben, können Sie bis zuletzt alle Änderungen
rückgängig machen; bei der Verwendung von LVM (Standard oder LVM) ist dies
nicht möglich.

</para><para>

Als nächstes können Sie aus der Liste ein Partitionsschema auswählen.
Alle Schemata haben ihre Vor- und
Nachteile, von denen einige im <xref linkend="partitioning"/> erörtert
sind. Wenn Sie sich nicht sicher sind, wählen Sie die erste Möglichkeit.
Beachten Sie, dass die automatische Partitionierung ein gewisses Minimum
an freiem Speicher benötigt. Wenn Sie nicht mindestens 1GB Platz
(abhängig vom gewählten Schema) zur Verfügung stellen,
wird die automatische Partitionierung fehlschlagen.

</para>

<informaltable>
<tgroup cols="3">
<thead>
<row>
  <entry>Partitionierungsschema</entry>
  <entry>Mindestens erforderlicher Festplattenplatz</entry>
  <entry>Erstellte Partitionen</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Alle Dateien in eine Partition</entry>
  <entry>600MB</entry>
  <entry><filename>/</filename>, swap
  </entry>
</row><row>
  <entry>Separate /home-Partition</entry>
  <entry>500MB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>, swap
  </entry>
</row><row>
  <entry>Separate /home-, /usr-, /var- und /tmp-Partitionen</entry>
  <entry>1GB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>,
    <filename>/usr</filename>, <filename>/var</filename>,
    <filename>/tmp</filename>, swap
  </entry>
</row>

</tbody></tgroup></informaltable>

<para>

Wenn Sie Geführte Partitionierung mit LVM (Standard oder LVM) wählen,
wird der Installer zusätzlich eine separate
<filename>/boot</filename>-Partition erzeugen.
Alle anderen Partitionen (inklusive Swap) werden innerhalb der LVM-Partition
erstellt.

</para><para arch="ia64">

Wählen Sie Geführte Partitionierung auf Ihrem IA-64-System, wird eine
zusätzliche Partition, formatiert als bootfähiges FAT16-Dateisystem, für
den EFI-Bootloader erstellt. Es gibt ebenfalls einen zusätzlichen Menüpunkt
im Format-Menü, um eine Partition manuell als EFI-Boot-Partition einzurichten.

</para><para arch="alpha">

Wenn Sie Geführte Partitionierung für Ihr Alpha-System wählen, wird am
Anfang der Festplatte eine zusätzliche, unformatierte Partition erstellt,
um Platz für den aboot-Bootloader zu reservieren.

</para><para>

Nachdem Sie ein Schema ausgewählt haben, erscheint im nächsten Bild Ihre
neue Partitionstabelle mit Informationen darüber, ob und wie die Partitionen
formatiert werden und wo sie ins Dateisystem eingebunden werden.

</para><para>

Die Liste der Partitionen sieht in ihrer Struktur ähnlich aus wie diese:

<!-- TODO: show some flags here (lightning, skull, smiley) -->
<informalexample><screen>
  IDE1 master (hda) - 6.4 GB WDC AC36400L
        #1 primär    16.4 MB  B f ext2       /boot
        #2 primär   551.0 MB      swap       swap
        #3 primär     5.8 GB      ntfs
           pri/log    8.2 MB      FREIER SPEICHER

  IDE1 slave (hdb) - 80.0 GB ST380021A
        #1 primär    15.9 MB      ext3
        #2 primär   996.0 MB      fat16
        #3 primär     3.9 GB      xfs        /home
        #5 logisch    6.0 GB    f ext3       /
        #6 logisch    1.0 GB    f ext3       /var
        #7 logisch  498.8 MB      ext3
        #8 logisch  551.5 MB      swap       swap
        #9 logisch   65.8 GB      ext2

</screen></informalexample>

Dieses Beispiel zeigt, wie zwei IDE-Festplatten in mehrere Partitionen
aufgeteilt wurden; die erste Platte enthält noch etwas freien Speicher. Jede
dieser Zeilen mit jeweils einer Partition besteht aus der Partitionsnummer,
dem Typ (primär, erweitert, logisch), der Größe, optionalen Markierungen,
dem Dateisystemtyp und dem Einhängepunkt im Dateisystem (falls vorhanden).
Hinweis: dieses spezielle Setup kann nicht mittels Geführter Partitionierung
erstellt werden, aber es zeigt mögliche Variationen, die mit manueller
Partitionierung erreicht werden können.

</para><para>

Hiermit ist die Vorauswahl der automatischen Partitionierung beendet. Wenn Sie
mit der erstellten Partitionstabelle zufrieden sind, wählen Sie
<guimenuitem>Partitionierung beenden und Änderungen übernehmen</guimenuitem>,
um die neue Partitionstabelle zu aktivieren (wie am Ende dieses Kapitels
beschrieben). Sind Sie nicht zufrieden, können Sie <guimenuitem>Änderungen an
den Partitionen rückgängig machen</guimenuitem> wählen, um die automatische
Partitionierung erneut zu starten, oder Sie nehmen die Vorschläge als Basis und
führen daran noch manuelle Änderungen durch, wie weiter unten für das manuelle
Partitionieren erläutert.

</para>
   </sect3>

   <sect3 id="partman-manual">
   <title>Manuelle Partitionierung</title>
<para>

Ein ähnliches Bild wie oben wird Ihnen angezeigt, wenn Sie
<quote>Manuell</quote> wählen, mit dem Unterschied, dass Ihre derzeit
existierende Partitionstabelle angezeigt wird und die Einhängepunkte fehlen.
Wie Sie die Partitionstabelle manuell einrichten (sowie einiges über die
Nutzung der Partitionen durch Ihr neues Debian-System) wird im Rest dieses
Kapitels beschrieben.

</para><para>

Wenn Sie eine neue, unberührte Platte auswählen, die weder Partitionen noch
freien Speicher enthält, werden Sie gefragt, ob eine neue Partitionstabelle
erstellt werden soll (dies ist nötig, um neue Partitionen anlegen zu können).
Danach sollte eine neue Zeile <quote>FREIER SPEICHER</quote> in der Tabelle
unter den ausgewählten Festplatten erscheinen.

</para><para>

Wenn Sie eine Zeile mit freiem Speicher auswählen, haben Sie die Gelegenheit,
eine neue Partition zu erstellen. Sie müssen einige Fragen über ihre Größe, den
Typ (primär oder logisch) und die Lage auf der Platte (am Anfang oder am Ende
des freien Speicherbereichs) beantworten. Danach wird eine detaillierte
Übersicht der neuen Partition angezeigt. Die wichtigste Option hier ist
<guimenuitem>Benutzen als:</guimenuitem>, welche festlegt, ob die Partition
ein Dateisystem enthalten soll oder als Swap, Software-RAID, LVM,
verschlüsseltem LVM oder überhaupt nicht verwendet werden soll. Andere Einträge
enthalten den Einhängepunkt im zukünftigen Dateisystem, Optionen zum Einbinden
und die <quote>Boot-Flag</quote>-Markierung (ob die Partition bootfähig ist
oder nicht). Welche Einträge angezeigt werden, hängt von der Art der Nutzung
der Partition ab. Falls Ihnen die gewählten Standardwerte nicht
zusagen, scheuen Sie sich nicht, sie Ihren Bedürfnissen entsprechend zu ändern.
Z.B. können Sie mittels der <guimenuitem>Benutzen als:</guimenuitem>-Option
ein anderes Dateisystem für die Partition auswählen sowie haben die Möglichkeit,
die Partition als Swap (um Arbeitsspeicher auf die Platte auszulagern),
Software-RAID, LVM oder überhaupt nicht zu nutzen. Eine andere tolle Funktion
ist, Daten von einer bestehenden Partition auf die neue zu kopieren. Wenn Sie
mit der neu erstellten Partition zufrieden sind, wählen Sie
<guimenuitem>Anlegen der Partition beenden</guimenuitem>, um zurück zum
<command>partman</command>-Hauptbildschirm zu gelangen.

</para><para>

Wenn Sie meinen, noch etwas an Ihren Partitionen ändern zu müssen, wählen Sie
einfach eine aus und Sie kommen zum Partitions-Konfigurationsmenü. Dies ist das
gleiche Bild wie beim Erstellen von Partitionen; deshalb stehen auch die
gleichen Optionen zur Auswahl. Eine Sache, die vielleicht auf den ersten Blick
nicht ganz einleuchtend sein könnte, ist die Möglichkeit, eine Partition in der
Größe zu verändern, indem Sie das Element auswählen, das die Größe anzeigt.
Dateisysteme, bei denen das bekanntermaßen funktioniert, sind FAT16, FAT32,
EXT2, EXT3 und Swap. In diesem Menü ist es ebenfalls möglich, eine Partition
zu löschen.

</para><para>

Sie müssen mindestens zwei Partitionen erzeugen: eine für das
<emphasis>root</emphasis>-Dateisystem (das als <filename>/</filename>
eingebunden werden muss) und eine für <emphasis>Swap</emphasis>. Falls Sie
vergessen, die Root-Partition einzubinden, gestattet <command>partman</command>
es nicht, weiterzumachen, bis diese Angelegenheit behoben ist.

</para><para arch="ia64">

Falls Sie außerdem keine EFI-Boot-Partition erstellt und formatiert haben, wird
<command>partman</command> dies ebenfalls erkennen und Sie können nicht
fortfahren, ehe Sie eine zugewiesen haben.

</para><para>

Die Fähigkeiten von <command>partman</command> können mittels zusätzlicher
Installer-Module noch ausgebaut werden, dies ist allerdings abhängig von Ihrer
System-Architektur. Falls also nicht alle angekündigten Optionen sichtbar sind,
stellen Sie sicher, dass die erforderlichen Module geladen sind (wie z.B.
<filename>partman-ext3</filename>, <filename>partman-xfs</filename>
oder <filename>partman-lvm</filename>).

</para><para>

Wenn Sie mit der Partitionierung zufrieden sind, wählen Sie
<guimenuitem>Partitionierung beenden und Änderungen übernehmen</guimenuitem>
aus dem Partitionierungsmenü. Es wird eine Zusammenfassung aller Änderungen,
die gemacht wurden, angezeigt und Sie werden aufgefordert, die Erstellung der
Dateisysteme zu bestätigen.

</para>
   </sect3>
