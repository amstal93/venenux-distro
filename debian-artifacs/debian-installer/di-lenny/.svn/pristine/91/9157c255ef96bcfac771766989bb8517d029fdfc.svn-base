# Tagalog translation of debian-installer manual.
# Copyright (C) 2005 Software in the Public Interest, Inc.
# Eric Pareja <xenos@upm.edu.ph>, 2005.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: debian-installer manual Tagalog\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-05-30 00:09+0000\n"
"PO-Revision-Date: 2005-11-30 04:11+0800\n"
"Last-Translator: Eric Pareja <xenos@upm.edu.ph>\n"
"Language-Team: Tagalog <debian-tl@banwa.upm.edu.ph>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

#. Tag: title
#: bookinfo.xml:5
#, no-c-format
msgid "&debian; Installation Guide"
msgstr "Patnubay sa Pagluklok ng &debian;"

#. Tag: para
#: bookinfo.xml:8
#, no-c-format
msgid ""
"This document contains installation instructions for the &debian; &release; "
"system (codename <quote>&releasename;</quote>), for the &arch-title; "
"(<quote>&architecture;</quote>) architecture. It also contains pointers to "
"more information and information on how to make the most of your new Debian "
"system."
msgstr ""
"Nilalaman nito ang mga bilin sa pagluklok ng sistemang &debian; &release;, "
"para sa arkitekturang &arch-title; (<quote>&architecture;</quote>). "
"Nilalaman din nito ang ilang mga pahiwatig tungo sa karagdagang impormasyon "
"at impormasyon kung paano magamit ng husto ang inyong bagong sistemang "
"Debian."

#. Tag: para
#: bookinfo.xml:17
#, no-c-format
msgid ""
"Because the &arch-title; port is not a release architecture for "
"&releasename;, there is no official version of this manual for &arch-title; "
"for &releasename;. However, because the port is still active and there is "
"hope that &arch-title; may be included again in future official releases, "
"this development version of the Installation Guide is still available."
msgstr ""

#. Tag: para
#: bookinfo.xml:25
#, no-c-format
msgid ""
"Because &arch-title; is not an official architecture, some of the "
"information, and especially some links, in this manual may be incorrect. For "
"additional information, please check the <ulink url=\"&url-ports;"
"\">webpages</ulink> of the port or contact the <ulink url=\"&url-list-"
"subscribe;\">debian-&arch-listname; mailing list</ulink>."
msgstr ""

#. Tag: para
#: bookinfo.xml:36
#, no-c-format
msgid ""
"This installation guide is based on an earlier manual written for the old "
"Debian installation system (the <quote>boot-floppies</quote>), and has been "
"updated to document the new Debian installer. However, for &architecture;, "
"the manual has not been fully updated and fact checked for the new "
"installer. There may remain parts of the manual that are incomplete or "
"outdated or that still document the boot-floppies installer. A newer version "
"of this manual, possibly better documenting this architecture, may be found "
"on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</ulink>. You "
"may also be able to find additional translations there."
msgstr ""
"Itong gabay sa pagluklok ay batay sa mas-naunang manwal na sinulat para sa "
"lumang sistema ng pagluklok ng Debian (ang mga \"boot floppy\"), at binago "
"upang maipaliwanag ang bagong tagaluklok (installer) ng Debian. Gayunman, "
"para sa &architecture;, hindi pa buo ang pagbabago at pagsuri nito para sa "
"bagong tagaluklok. May mga bahagi nitong manwal na kulang o hindi na "
"kasalukuyan o tumutukoy pa rin sa tagaluklok na luma (gumagamit ng boot "
"floppy). Ang mas-bagong bersyon nitong manwal, na maaaring mas-angkop ang "
"paliwanag sa arkitekturang ito, ay mahahanap sa Internet sa <ulink url="
"\"&url-d-i;\">&d-i; home page</ulink>. Maari ding makahanap ng karagdagang "
"mga salin doon."

#. Tag: para
#: bookinfo.xml:49
#, no-c-format
msgid ""
"Although this installation guide for &architecture; is mostly up-to-date, we "
"plan to make some changes and reorganize parts of the manual after the "
"official release of &releasename;. A newer version of this manual may be "
"found on the Internet at the <ulink url=\"&url-d-i;\">&d-i; home page</"
"ulink>. You may also be able to find additional translations there."
msgstr ""
"Bagama't itong gabay sa pagluklok para sa &architecture; ay malamang na "
"kasalukuyan, balak namin itong baguhin at palitan ang pagkakaayos ng mga "
"bahagi nitong manwal matapos ang opisyal na paglaya ng &releasename;. "
"Maaaring mahanap ang mas-bagong bersyon nitong manwal sa Internet sa <ulink "
"url=\"&url-d-i;\">&d-i; home page</ulink>. Maari din na makahanap ng "
"karagdagang mga salin doon."

#. Tag: para
#: bookinfo.xml:58
#, no-c-format
msgid ""
"Translators can use this paragraph to provide some information about the "
"status of the translation, for example if the translation is still being "
"worked on or if review is wanted (don't forget to mention where comments "
"should be sent!). See build/lang-options/README on how to enable this "
"paragraph. Its condition is \"translation-status\"."
msgstr ""

#. Tag: holder
#: bookinfo.xml:75
#, no-c-format
msgid "the Debian Installer team"
msgstr "ang pangkat na bumuo ng Tagaluklok ng Debian"

#. Tag: para
#: bookinfo.xml:79
#, no-c-format
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License. Please refer to the license in "
"<xref linkend=\"appendix-gpl\"/>."
msgstr ""
"Ang manwal na ito ay malayang software; maaari niyo itong ipamigay at/o "
"baguhin ito sa ilalim ng GNU General Public License. Mangyari lamang na "
"basahin ang lisensiya sa <xref linkend=\"appendix-gpl\"/>."
