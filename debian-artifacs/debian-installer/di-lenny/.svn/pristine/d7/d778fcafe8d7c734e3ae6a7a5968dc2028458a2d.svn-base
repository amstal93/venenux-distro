<!-- retain these comments for translator revision tracking -->
<!-- original version: 55471 untranslated -->


  <sect2 arch="mipsel"><title>CPU, Main Boards, and Video Support</title>
<para>

Debian on &arch-title; supports the following platforms:

<itemizedlist>
<listitem><para>

Cobalt Microserver: only MIPS based Cobalt machines are covered here.  This
includes the Cobalt Qube 2700 (Qube1), RaQ, Qube2 and RaQ2, and the Gateway
Microserver.

</para></listitem>
<listitem><para>

MIPS Malta: this platform is emulated by QEMU and is therefore a nice way
to test and run Debian on MIPS if you don't have the hardware.

</para></listitem>
<listitem><para>

Broadcom BCM91250A (SWARM): this is an ATX form factor evaluation board
from Broadcom based on the dual-core SB1 1250 CPU.

</para></listitem>
<listitem><para>

Broadcom BCM91480B (BigSur): this is an ATX form factor evaluation board
from Broadcom based on the quad-core SB1A 1480 CPU.

</para></listitem>
</itemizedlist>

Complete information regarding supported mips/mipsel machines can be found
at the <ulink url="&url-linux-mips;">Linux-MIPS homepage</ulink>.  In the
following, only the systems supported by the Debian installer will be
covered.  If you are looking for support for other subarchitectures, please
contact the <ulink url="&url-list-subscribe;">
debian-&arch-listname; mailing list</ulink>.

</para>

   <sect3><title>CPU/Machine types</title>

<para>

All MIPS based Cobalt machines are supported.

</para><para>

The Broadcom BCM91250A evaluation board comes with an SB1 1250 chip with
two cores which are supported in SMP mode by this installer.  Similarly,
the BCM91480B evaluation board contains an SB1A 1480 chip with four cores
which are supported in SMP mode.

</para>
   </sect3>

   <sect3><title>Supported console options</title>
<para>

Both Cobalt and Broadcom BCM91250A/BCM91480B use 115200 bps.

</para>
   </sect3>
  </sect2>
