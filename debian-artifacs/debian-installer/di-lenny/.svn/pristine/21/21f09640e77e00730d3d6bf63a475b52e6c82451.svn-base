<!-- original version: 53521 -->

<bookinfo id="debian_installation_guide">
<title>Manuel d'installation pour la distribution &debian; </title>

<abstract>
<para>
Ce document contient les instructions d'installation du système &debian; 
(nommé <quote>&releasename;</quote>), 
sur l'architecture &arch-title; (<quote>&architecture;</quote>).
Des pointeurs vers d'autres sources d'informations vous permettront de tirer
le meilleur parti de votre nouveau système Debian.
</para>

<para>
<note arch="m68k"><para>
Le portage de l'architecture &arch-title; n'est pas publié avec &releasename;.
Et il n'y a pas de manuel officiel de &arch-title; pour &releasename;.
Cependant, nous espérons que l'architecture &arch-title; sera de
nouveau incluse dans de prochaines publications officielles et
nous donnons cette version de développement du manuel d'installation.

</para><para>

L'architecture &arch-title; n'est pas une architecture officielle. Certaines
informations et particulièrement certains liens peuvent ne pas être corrects.
Pour d'autres informations, veuillez consulter les <ulink url="&url-ports;">pages</ulink>
de l'architecture ou contacter la liste de diffusion
<ulink url="&url-list-subscribe;">debian-&arch-listname;</ulink>.
</para></note>

<warning condition="not-checked"><para>
Ce manuel d'installation est basé sur un ancien manuel, le
<quote>boot-floppies</quote>, qui a été mis à jour pour servir de guide au
nouvel installateur Debian. Cependant, pour &architecture;, le manuel n'a 
pas été complètement mis à jour ni vérifié. Certaines parties du manuel
peuvent être incomplètes ou dépassées, ou bien elles documentent encore 
l'ancien installateur. Une nouvelle version de ce manuel, avec de meilleures
informations sur cette architecture, se trouve sur la <ulink url="&url-d-i;">
page de l'installateur Debian</ulink>. De nouvelles traductions y sont 
disponibles.
</para></warning>
<note condition="checked"><para>
Ce guide d'installation pour &architecture; est, pour l'essentiel, à jour.
Nous avons néanmoins l'intention d'en réorganiser certaines parties après
la publication de la version officielle, &releasename;. Une nouvelle version
de ce manuel se trouvera sur la <ulink url="&url-d-i;">page de l'installateur</ulink>.
Vous y trouverez aussi de nouvelles traductions.
</para></note>
</para>
<para condition="translation-status">
Toutes les remarques sur cette traduction,
sont à envoyer à l'adresse <email>debian-l10n-french@lists.debian.org</email>.
La traduction de la <quote>GNU GPL</quote> est celle qu'on trouve sur le site de la
<ulink url="http://fsffrance.org/gpl/gpl-fr.fr.html">FSF France</ulink>.
</para>
</abstract>

<copyright>
 <year>2004</year>
 <year>2005</year>
 <year>2006</year>
 <year>2007</year>
 <year>2008</year>
<holder>L'équipe de l'installateur Debian</holder>
</copyright>

<legalnotice>
<para>
Ce manuel est un logiciel libre&nbsp;; vous pouvez le distribuer ou le
modifier selon les termes de la Licence publique générale GNU. Veuillez
consulter la licence à l'<xref linkend="appendix-gpl"/>.
</para>

</legalnotice>
</bookinfo>
