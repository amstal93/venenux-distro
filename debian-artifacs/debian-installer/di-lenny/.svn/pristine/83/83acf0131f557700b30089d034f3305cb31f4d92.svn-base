<!-- retain these comments for translator revision tracking -->
<!-- original version: 52960 untranslated -->

 <sect1 id="memory-disk-requirements">
 <title>Memory and Disk Space Requirements</title>

<para>

You must have at least &minimum-memory; of memory and &minimum-fs-size;
of hard disk space to perform a normal installation. Note that these are
fairly minimal numbers. For more realistic figures, see
<xref linkend="minimum-hardware-reqts"/>.

</para><para>

Installation on systems with less memory<footnote condition="gtk">

<para>

Installation images that support the graphical installer require more
memory than images that support only the textual installer and should
not be used on systems with less than &minimum-memory; of memory. If
there is a choice between booting the regular and the graphical installer,
the former should be selected.

</para>

</footnote> or disk space available may be possible but is only advised for
experienced users.

</para><para arch="m68k">

On the Amiga the size of FastRAM is relevant towards the total memory
requirements.  Also, using Zorro cards with 16-bit RAM is not
supported; you'll need 32-bit RAM.  The <command>amiboot</command>
program can be used to disable 16-bit RAM; see the
<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>.  Recent kernels should
disable 16-bit RAM automatically.

</para><para arch="m68k">

On the Atari, both ST-RAM and Fast RAM (TT-RAM) are used by Linux.
Many users have reported problems running the kernel itself in Fast
RAM, so the Atari bootstrap will place the kernel in ST-RAM. The
minimum requirement for ST-RAM is 2 MB. You will need an additional
12 MB or more of TT-RAM.

</para><para arch="m68k">

On the Macintosh, care should be taken on machines with RAM-based
video (RBV). The RAM segment at physical address 0 is used as screen
memory, making the default load position for the kernel unavailable.
The alternate RAM segment used for kernel and RAMdisk must be at least
4 MB.

</para><para arch="m68k">

<emphasis condition="FIXME">FIXME: is this still true?</emphasis>

</para>

 </sect1>
