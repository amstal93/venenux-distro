#!/bin/sh

# Copyright (C) 2005, 2006, 2008  Martin Michlmayr <tbm@cyrius.com>

# This code is covered by the GNU General Public License (version 2
# or higher)

set -e

. /usr/lib/oldsys-preseed/functions

log() {
	logger -t oldsys-preseed "$@"
}

exit_unknown() {
	log "Unknown system - not writing preseed file"
	exit 0
}

# If this is set to "yes", the user *absolutely* cannot input anything
# before network-console comes up.  Therefore, preseed some info which
# is not optimal but which will ensure that network-console is reached
# without prompting for user input.
NONINTERACTIVE="yes"
FILE=/preseed.cfg

case "`archdetect`" in
	arm*/ixp4xx)
		machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
		if echo "$machine" | grep -q "^Linksys NSLU2"; then
			check_file /proc/mtd
			sysconf=$(get_mtdblock "SysConf")
			if [ -z "$sysconf" ]; then
				log "Can't find SysConf MTD partition"
				exit 0
			fi
			parse_sysconf "/dev/$sysconf"
			# The original NSLU2 uses a different name for the network interface
			if [ "$INTERFACE" = "ixp0" ]; then
				# IXP4xx NPE ethernet interface seems to come
				# up as eth0 (#407460) therefore only use eth0
				# for the installer if the NPE microcode is
				# present
				if [ -e /lib/firmware/NPE-B ]; then
					INTERFACE=eth0
				else
					INTERFACE=eth1
					if [ "$NONINTERACTIVE" = "yes" ]; then
						add "$FILE" "hw-detect/load_firmware" "boolean" "false"
					fi
				fi
			fi
			sanity_check_static_config
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.77
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			if [ "$NONINTERACTIVE" = "yes" ]; then
				add "$FILE" "ethdetect/use_firewire_ethernet" "boolean" "false"
			fi
		fi
	;;
	arm*/iop32x)
		machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
		if echo "$machine" | grep -q "^Thecus"; then
			check_file /proc/mtd
			config=$(get_mtdblock "user")
			if [ -z "$config" ]; then
				log "Can't find MTD partition holding config data"
				exit 0
			fi
			path=/tmp/oldsys-preseed
			mkdir -p $path/mnt $path/defaults
			mount -t jffs2 -o ro /dev/$config $path/mnt
			if [ -e $path/mnt/default.tar.gz ]; then
				(cd $path/defaults && tar -xzf $path/mnt/default.tar.gz)
				if [ -e $path/defaults/app/etc/HOSTNAME ]; then
					DEFAULT_HOSTNAME=$(cut -d . -f 1 $path/defaults/app/etc/HOSTNAME)
					DEFAULT_DOMAIN=$(cut -d . -f 2- $path/defaults/app/etc/HOSTNAME)
				fi
			fi
			parse_unix_tree $path/mnt
			INTERFACE=eth0
			if grep -q udhcpc $path/mnt/cfg/cfg_nic0; then
				NET_CONFIG=dhcp
			else
				NET_CONFIG=static
				parse_ifconfig $path/mnt/cfg/cfg_nic0
			fi
			sanity_check_static_config
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.100
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			HOSTNAME=$(cut -d . -f 1 $path/mnt/etc/HOSTNAME)
			# work around a bug in busybox's cut
			DOMAIN=$( (cat $path/mnt/etc/HOSTNAME ; echo) | cut -d . -f 2-)
			unset_matching_var "HOSTNAME" "$DEFAULT_HOSTNAME"
			# The default says Thecus_N2100 but in fact it is set to N2100....
			unset_matching_var "HOSTNAME" "N2100"
			unset_matching_var "HOSTNAME" "N4100"
			unset_matching_var "DOMAIN" "$DEFAULT_DOMAIN"
			unset_matching_var "DOMAIN" "thecus.com"
			umount $path/mnt
			rm -rf $path/defaults
			rmdir $path/mnt $path || true
		else
			exit_unknown
		fi
	;;
	arm*/orion5x)
		machine=$(grep "^Hardware" /proc/cpuinfo | sed 's/Hardware\s*:\s*//')
		if echo "$machine" | grep -q "^Buffalo/Revogear Kurobox Pro"; then
			check_file /proc/mtd
			rootfs=$(get_mtdblock "rootfs")
			if [ -z "$rootfs" ]; then
				log "Can't find rootfs MTD partition"
				exit 0
			fi
			path=/tmp/oldsys-preseed
			mkdir -p $path/rootfs
			mount -t jffs2 -o ro /dev/$rootfs $path/rootfs || true
			INTERFACE=eth0
			parse_unix_tree $path/rootfs
			netinfo=$path/rootfs/etc/netinfo
			if [ -e "$netinfo" ]; then
				usage=$(sed -n '/^my_ipaddress/ {s/.*=//; p}' "$netinfo")
				if [ "$usage" = "dhcp" ]; then
					NET_CONFIG=dhcp
				else
					NET_CONFIG=static
					IP_ADDRESS=$usage
					NETMASK=$(sed -n '/^my_subnetmask/ {s/.*=//; p}' "$netinfo")
					GATEWAY=$(sed -n '/^my_dgw/ {s/.*=//; p}' "$netinfo")
				fi
			fi
			hostinfo=$path/rootfs/etc/host.info
			if [ -e "$hostinfo" ]; then
				HOSTNAME=$(sed -n '/^hostname/ {s/.*=//; p}' "$hostinfo")
			fi
			unset_matching_var "HOSTNAME" "KUROBOX-PRO"
			sanity_check_static_config
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.11.150
				NETMASK=255.255.255.0
				GATEWAY=192.168.11.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.11.1
				dhcp_fallback $FILE
			fi
			umount $path/rootfs || true
			rmdir $path/rootfs $path || true
		elif echo "$machine" | grep -q "^HP Media Vault mv2120"; then
			path=/tmp/oldsys-preseed
			mkdir -p $path/sda5
			mount -t ext3 -o ro /dev/sda5 $path/sda5 || true
			INTERFACE=eth0
			if [ -e $path/sda5/etc/nas_conf_db.xml ]; then
				dhcp=$(remove_xml $(grep "<dhcpenabled>" $path/sda5/etc/nas_conf_db.xml))
				if [ "$dhcp" == "true" ]; then
					NET_CONFIG=dhcp
				else
					NET_CONFIG=static
					IPADDRESS=$(remove_xml $(grep "<ipaddr>" $path/sda5/etc/nas_conf_db.xml))
					NETMASK=$(remove_xml $(grep "<netmask>" $path/sda5/etc/nas_conf_db.xml))
					GATEWAY=$(remove_xml $(grep "<gateway>" $path/sda5/etc/nas_conf_db.xml))
					for i in $(grep "<dnsserver>" $path/sda5/etc/nas_conf_db.xml); do
							var_add NAMESERVERS $(remove_xml $i)
					done
				fi
				HOSTNAME=$(remove_xml $(grep "<hostname>" $path/sda5/etc/nas_conf_db.xml | head -n 1))
				unset_matching_var "HOSTNAME" "HPMediaVault"
				DOMAIN=$(remove_xml $(grep "<domain>" $path/sda5/etc/nas_conf_db.xml))
				unset_matching_var "DOMAIN" "local"
			else
				NET_CONFIG=dhcp
			fi
			sanity_check_static_config
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.100
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			umount $path/sda5 || true
			rmdir $path/sda5 $path || true
		elif echo "$machine" | grep -q "^QNAP TS-109/TS-209" ||
		   echo "$machine" | grep -q "^QNAP TS-409"; then
			path=/tmp/oldsys-preseed
			mkdir -p $path/sda1
			mount -t ext2 -o ro /dev/sda1 $path/sda1 || true
			INTERFACE=eth0
			if [ -e $path/sda1/.config/uLinux.conf ]; then
				capture=0
				eth=$(cat $path/sda1/.config/uLinux.conf | while read line; do
					if [ $capture -gt 0 ]; then echo $line ; fi
					if [ "$line" = "[eth0]" ]; then capture=1 ; fi
					if [ "$line" = "" ]; then capture=0 ; fi
				done)
				usage=$(echo "$eth" | grep "^Usage" | sed 's/^.* //')
				if [ "$usage" = "DHCP" ]; then
					NET_CONFIG=dhcp
				else
					NET_CONFIG=static
					IPADDRESS=$(echo "$eth" | grep "^IP Address" | sed 's/^.* //')
					NETMASK=$(echo "$eth" | grep "^Subnet Mask" | sed 's/^.* //')
					GATEWAY=$(echo "$eth" | grep "^Gateway" | sed 's/^.* //')
				fi
				for i in $(grep "Domain Name Server" $path/sda1/.config/uLinux.conf | sed 's/^.* //' | grep -v 0.0.0.0); do
						var_add NAMESERVERS "$i"
				done
				HOSTNAME=$(grep "Server Name" $path/sda1/.config/uLinux.conf | sed 's/^.* //')
				unset_matching_var "HOSTNAME" NAS$(echo "$MAC" | sed 's/^..:..:..://' | sed 's/://g')
			else
				NET_CONFIG=dhcp
			fi
			sanity_check_static_config
			if [ "$NET_CONFIG" != "static" ]; then
				IPADDRESS=192.168.1.100
				NETMASK=255.255.255.0
				GATEWAY=192.168.1.1
				[ -z "$NAMESERVERS" ] && NAMESERVERS=192.168.1.1
				dhcp_fallback $FILE
			fi
			umount $path/sda1 || true
			rmdir $path/sda1 $path || true
		else
			exit_unknown
		fi
	;;
	*)
		exit_unknown
	;;
esac

if [ "$NONINTERACTIVE" = "yes" ]; then
	# Just continue if d-i enters lowmem mode
	add "$FILE" "lowmem/low" "note"
	# Any hostname and domain names assigned from DHCP take precedence
	# over values set here.  However, setting the values still prevents
	# the questions from being shown, even if values come from dhcp.
	add "$FILE" "netcfg/get_hostname" "string" "debian"
	add "$FILE" "netcfg/get_domain" "string" "example.org"
	# I'm not terribly happy to preseed a generic password but I guess
	# there's no other way on some machines.
	add "$FILE" "network-console/password" "password" "install"
	add "$FILE" "network-console/password-again" "password" "install"
fi

# Workaround for broken partconf
add "$FILE" "partconf/already-mounted" "boolean" "false"

generate_preseed_file $FILE

