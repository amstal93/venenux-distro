<!-- retain these comments for translator revision tracking -->
<!-- original version: 28672 -->

 <sect1 id="official-cdrom">
 <title>Offizielle &debian;-CD-ROMs</title>
<para>

Die bei weitem einfachste Methode, &debian; zu installieren, ist
von einem Satz offizieller Debian-CD-ROMs. Sie können die CDs
bei einem Händler kaufen (siehe die
<ulink url="&url-debian-cd-vendors;">Verkäufer von Debian CDs-Seite</ulink>).
Sie können sich auch die CD-ROM-Images von einem Debian-Spiegelserver
herunterladen und Ihren eigenen Satz CDs erstellen, wenn
Sie eine schnelle Netzwerkverbindung und einen CD-Brenner haben
(lesen Sie die <ulink url="&url-debian-cd;">Debian GNU/Linux auf CD-Seite</ulink>
bezüglich weiterer Details). Wenn Sie einen Satz Debian-CDs haben
und Ihr Rechner kann auch von CD booten, können Sie direkt
zum <xref linkend="boot-installer"/> springen. Es wurde
ein großer Aufwand betrieben, sicherzustellen, dass die Dateien,
die die meisten Leute benötigen werden, auf den CDs enthalten sind.
Obwohl ein kompletter Satz Binärpakete mehrere CDs erfordert,
ist es unwahrscheinlich, dass Sie Pakete von der dritten
CD oder darüber benötigen. Sie können sich auch für die DVD-Version
entscheiden, was viel Platz in Ihrem Regal spart; außerdem vermeiden
Sie dadurch den CD-Wechsel-Marathon.

</para><para>

Wenn Ihre Maschine nicht von CD booten kann, Sie aber trotzdem einen
Satz Debian-CDs haben, können Sie eine alternative Strategie verfolgen,
wie zum Beispiel

<phrase condition="supports-floppy-boot">Floppy-Disk,</phrase>

<phrase arch="s390">Bandlaufwerk oder emuliertes Bandlaufwerk,</phrase>

<phrase condition="bootable-disk">Festplatte,</phrase>

<phrase condition="bootable-usb">USB-Stick,</phrase>

<phrase condition="supports-tftp">Netzwerk-Boot,</phrase>

oder Sie laden den Kernel manuell von der CD, um den Installer zu
starten. Die Dateien, die Sie benötigen, um auf alternative Art zu
booten, sind ebenfalls auf der CD. Das Debian-Netzwerk-Archiv und
die Verzeichnisstruktur der CD sind identisch. Wenn also weiter unten
Pfade zu Dateien im Archiv angegeben werden, können Sie diese Dateien
auch im gleichen Verzeichnis/Unterverzeichnis auf der CD finden.

</para><para>

Sobald der Installer gestartet ist, kann er alle anderen Dateien, die
er benötigt, von der CD beziehen.

</para><para>

Wenn Sie keine CDs haben, müssen Sie die Installer-Systemdateien
herunterladen und speichern, z.B. auf

<phrase arch="s390">einem Installations-Tape</phrase>

<phrase condition="supports-floppy-boot">Floppy-Disk,</phrase>

<phrase condition="bootable-disk">Festplatte,</phrase>

<phrase condition="bootable-usb">USB-Stick,</phrase>

<phrase condition="supports-tftp">einem per Netzwerk angebundenen Computer,</phrase>

so dass der Installer sie nutzen kann.

</para>

 </sect1>
