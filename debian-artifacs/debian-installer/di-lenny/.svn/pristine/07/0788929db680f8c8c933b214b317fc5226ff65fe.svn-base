# translation of debian-installer_packages_po_sublevel4_da.po to
# Danish messages for debian-installer.
# This file is distributed under the same license as debian-installer.
#
# Henrik Christian Grove <debian@3001.dk>, 2008
# Jesper Dahl Nyerup <debian@jespernyerup.dk>, 2008.
# Jacob Sparre Andersen <sparre@nbi.dk>, 2008.
# Claus Hindsgaul <claus.hindsgaul@gmail.com>, 2004-2007.
# Reviewed 2007 by Niels Rasmussen
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel3_da\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-26 13:16+0000\n"
"PO-Revision-Date: 2008-09-18 22:41+0200\n"
"Last-Translator: Jesper Dahl Nyerup <debian@jespernyerup.dk>\n"
"Language-Team: Danish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:1001
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr "Tjekker ext2-filsystemet på partition ${PARTITION} på ${DEVICE}..."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "Gå tilbage til menuen for at rette fejlene?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""
"Testen af filsystemet med typen ext2 (revision 0) på partition ${PARTITION} "
"på ${DEVICE} fandt fejl, der ikke var rettet."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"Hvis du ikke går tilbage til partitioneringsmenuen og retter disse fejl, vil "
"partitionen slet ikke blive benyttet."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid "Failed to create a file system"
msgstr "Kunne ikke oprette et filsystem"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""
"Oprettelsen af ext2-filsystem på partition ${PARTITION} på ${DEVICE} "
"mislykkedes."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "Vil du tilbage til partitioneringsmenuen?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""
"Der er ikke angivet noget monteringspunkt for ext2-filsystemet på partition "
"${PARTITION} på ${DEVICE}."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"Hvis du ikke går tilbage til partitioneringsmenuen og angiver et "
"monteringspunkt derfra, vil denne partition slet ikke blive benyttet."

#. Type: select
#. Choices
#. :sl4:
#. what's to be entered is a mount point
#: ../partman-ext2r0.templates:5001
msgid "Enter manually"
msgstr "Angiv manuelt"

#. Type: select
#. Choices
#. :sl4:
#. "it" is a partition
#: ../partman-ext2r0.templates:5001
msgid "Do not mount it"
msgstr "Montér den ikke"

#. Type: select
#. Description
#. Type: string
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:5002 ../partman-ext2r0.templates:6001
msgid "Mount point for this partition:"
msgstr "Monteringspunkt for denne partition:"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Invalid mount point"
msgstr "Ugyldigt monteringspunkt"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "The mount point you entered is invalid."
msgstr "Det monteringspunkt, du angav, er ugyldigt."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Mount points must start with \"/\". They cannot contain spaces."
msgstr ""
"Monteringspunkter skal starte med \"/\". De må ikke indeholde mellemrum."

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:8001 ../partman-ext2r0.templates:10001
msgid "ext2r0"
msgstr "ext2r0"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-ext2r0.templates:9001
msgid "old Ext2 (revision 0) file system"
msgstr "gammelt ext2-filsystem (revision 0)"

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid "Go back to the menu and correct this problem?"
msgstr "Gå tilbage til menuen for at ordne dette problem?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""
"Din opstartspartition er ikke blevet sat op med det gamle ext2-filsystem "
"(revision 0). Dette er nødvendigt for at din maskine skal kunne startes op. "
"Gå tilbage og brug det gamle ext2-filsystem (revision 0)."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"Hvis du ikke går tilbage til partitioneringsmenuen og ordner disse "
"problemer, vil partitionen blive brugt som den er. Det vil betyde at du "
"muligvis ikke vil kunne starte op fra din harddisk."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:12001
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""
"Din opstartspartition ligger ikke på den første primære partition på din "
"harddisk. Dette er nødvendigt for at din maskine skal kunne startes op. Gå "
"tilbage og brug den første primære partition som opstartspartition."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:13001
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""
"Din rodpartition er ikke en primær partition på din harddisk. Dette er "
"nødvendigt for at din maskine skal kunne startes op. Gå tilbage og brug en "
"primær partition som rodpartition."
