# welcome.po
# ByungHyun Choi <byunghyun.choi@gmail.com>, 2005.
# Jang Seok-moon <drssay97@gmail.com>, 2005.
# Changwoo Ryu <cwryu@debian.org>, 2005, 2007.
#
# 이 번역은 완성 단계로 품질에 많은 신경을 쓰고 있습니다. 반드시 메일링 리스트에
# 번역 사실을 알리고 토의를 하십시오.
#
msgid ""
msgstr ""
"Project-Id-Version: welcome.xml\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-10-17 00:09+0000\n"
"PO-Revision-Date: 2008-11-11 22:40+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: Korean <debian-i10n-korean@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: welcome.xml:4
#, no-c-format
msgid "Welcome to Debian"
msgstr "데비안을 만나신 것을 환영합니다"

#. Tag: para
#: welcome.xml:5
#, no-c-format
msgid ""
"This chapter provides an overview of the Debian Project and &debian;. If you "
"already know about the Debian Project's history and the &debian; "
"distribution, feel free to skip to the next chapter."
msgstr ""
"이 장에서는 데비안 프로젝트와 &debian;에 대해 간단히 설명합니다. 데비안 프로"
"젝트의 역사와 &debian; 배포판에 대해 이미 알고 있다면 다음 장으로 넘어가셔도 "
"됩니다."

#. Tag: title
#: welcome.xml:19
#, no-c-format
msgid "What is Debian?"
msgstr "데비안이란?"

#. Tag: para
#: welcome.xml:20
#, no-c-format
msgid ""
"Debian is an all-volunteer organization dedicated to developing free "
"software and promoting the ideals of the Free Software community. The Debian "
"Project began in 1993, when Ian Murdock issued an open invitation to "
"software developers to contribute to a complete and coherent software "
"distribution based on the relatively new Linux kernel. That relatively small "
"band of dedicated enthusiasts, originally funded by the <ulink url=\"&url-"
"fsf;\">Free Software Foundation</ulink> and influenced by the <ulink url="
"\"&url-gnu-intro;\">GNU</ulink> philosophy, has grown over the years into an "
"organization of around &num-of-debian-developers; <firstterm>Debian "
"Developers</firstterm>."
msgstr ""
"데비안은 자유 소프트웨어를 개발하고 자유 소프트웨어 커뮤니티의 이상을 널리 알"
"리는 일을 위해 조직한, 자원자로만 구성된 조직입니다. 데비안 프로젝트는 1993년"
"에 이안 머독이 새로운 (당시에는 비교적 새로운 버전의) 커널을 사용하면서, 완전"
"하고 일관된 소프트웨어 배포판을 만드는 데 참여할 소프트웨어 개발자를 공개적으"
"로 모집하면서부터 시작되었습니다. 처음에는 <ulink url=\"&url-fsf;\">자유 소프"
"트웨어 재단(Free Software Foundation)</ulink>의 자금 지원을 받았고, <ulink "
"url=\"&url-gnu-intro;\">GNU</ulink> 철학에 영향을 받은 비교적 작은 규모의 이 "
"열성적인 집단은, 해를 거듭하면서 &num-of-debian-developers;여 명의 "
"<firstterm>데비안 개발자</firstterm>가 참여하는 조직으로 성장했습니다."

#. Tag: para
#: welcome.xml:35
#, no-c-format
msgid ""
"Debian Developers are involved in a variety of activities, including <ulink "
"url=\"&url-debian-home;\">Web</ulink> and <ulink url=\"&url-debian-ftp;"
"\">FTP</ulink> site administration, graphic design, legal analysis of "
"software licenses, writing documentation, and, of course, maintaining "
"software packages."
msgstr ""
"데비안 개발자는 <ulink url=\"&url-debian-home;\">홈페이지</ulink> 및 <ulink "
"url=\"&url-debian-ftp;\">FTP</ulink> 사이트 관리, 그래픽 디자인, 소프트웨어 "
"라이선스의 법률적 해석, 문서 작성, (말할 필요도 없이) 소프트웨어 패키지 관리 "
"등 다양한 활동에 참여합니다."

#. Tag: para
#: welcome.xml:44
#, no-c-format
msgid ""
"In the interest of communicating our philosophy and attracting developers "
"who believe in the principles that Debian stands for, the Debian Project has "
"published a number of documents that outline our values and serve as guides "
"to what it means to be a Debian Developer:"
msgstr ""
"데비안의 철학을 전달하기 위해, 또 데비안에 동의하는 개발자를 끌어 모으기 위"
"해 데비안 프로젝트는 여러가지 문서를 발표했습니다. 이 문서에서 데비안의 가치"
"를 간략히 설명하고, 또 이 문서는 데비안 개발자가 되는 게 무엇을 뜻하는 지에 "
"대한 지침 역할을 합니다:"

#. Tag: para
#: welcome.xml:52
#, no-c-format
msgid ""
"The <ulink url=\"&url-social-contract;\">Debian Social Contract</ulink> is a "
"statement of Debian's commitments to the Free Software Community. Anyone who "
"agrees to abide to the Social Contract may become a <ulink url=\"&url-new-"
"maintainer;\">maintainer</ulink>. Any maintainer can introduce new software "
"into Debian &mdash; provided that the software meets our criteria for being "
"free, and the package follows our quality standards."
msgstr ""
"<ulink url=\"&url-social-contract;\">데비안 우리의 약속(Debian Social "
"Contract)</ulink>은 자유소프트웨어 공동체에 대한 데비안의 약속입니다. 우리의 "
"약속에 따르기로 동의한 사람은 누구나 <ulink url=\"&url-new-maintainer;\">메인"
"테이너</ulink>가 될 수 있습니다. 어떤 메인테이너든지 새로운 소프트웨어를 데비"
"안에 추가할 수 있습니다. 단 그 소프트웨어가 우리의 자유소프트웨어 기준에 맞아"
"야 하고, 패키지가 우리의 품질 기준에 맞아야 합니다."

#. Tag: para
#: welcome.xml:64
#, no-c-format
msgid ""
"The <ulink url=\"&url-dfsg;\">Debian Free Software Guidelines</ulink> are a "
"clear and concise statement of Debian's criteria for free software. The DFSG "
"is a very influential document in the Free Software Movement, and was the "
"foundation of the <ulink url=\"&url-osd;\">The Open Source Definition</"
"ulink>."
msgstr ""
"<ulink url=\"&url-dfsg;\">데비안 자유 소프트웨어 기준(Debian Free Software "
"Guidelines)</ulink>은 자유소프트웨어에 대한 데비안의 기준을 단순명료하게 설명"
"한 것입니다. DFSG는 자유소프트웨어 운동에 있어서 매우 영향력 있는 문서로, 이 "
"문서를 기초로 <ulink url=\"&url-osd;\">오픈소스 정의(Open Source Definition)"
"</ulink>가 작성되었습니다."

#. Tag: para
#: welcome.xml:74
#, no-c-format
msgid ""
"The <ulink url=\"&url-debian-policy;\">Debian Policy Manual</ulink> is an "
"extensive specification of the Debian Project's standards of quality."
msgstr ""
"<ulink url=\"&url-debian-policy;\">데비안 정책 안내서(Debian Policy Manual)</"
"ulink>는 데비안 프로젝트의 품질 기준을 자세히 적은 명세서입니다."

#. Tag: para
#: welcome.xml:82
#, no-c-format
msgid ""
"Debian developers are also involved in a number of other projects; some "
"specific to Debian, others involving some or all of the Linux community. "
"Some examples include:"
msgstr ""
"데비안 개발자는 여러가지 다른 프로젝트에 참가하기도 합니다. 데비안과 관계된 "
"프로젝트도 있고, 리눅스 공동체의 일부 혹은 전부와 관련되어 있는 프로젝트도 합"
"니다. 예를 들면 다음과 같습니다:"

#. Tag: para
#: welcome.xml:89
#, no-c-format
msgid ""
"The <ulink url=\"&url-lsb-org;\">Linux Standard Base</ulink> (LSB) is a "
"project aimed at standardizing the basic GNU/Linux system, which will enable "
"third-party software and hardware developers to easily design programs and "
"device drivers for Linux-in-general, rather than for a specific GNU/Linux "
"distribution."
msgstr ""
"<ulink url=\"&url-lsb-org;\">Linux Standard Base</ulink> (LSB) 프로젝트는 기"
"본적인 GNU/리눅스 시스템을 표준화하는 프로젝트로, 써드파티 소프트웨어와 하드"
"웨어 개발자가 특정 GNU/리눅스 배포판용이 아니라 일반적인 리눅스에 대한 프로그"
"램과 디바이스 드라이버를 쉽게 설계할 수 있도록 정한 것입니다."

#. Tag: para
#: welcome.xml:99
#, no-c-format
msgid ""
"The <ulink url=\"&url-fhs-home;\">Filesystem Hierarchy Standard</ulink> "
"(FHS) is an effort to standardize the layout of the Linux file system. The "
"FHS will allow software developers to concentrate their efforts on designing "
"programs, without having to worry about how the package will be installed in "
"different GNU/Linux distributions."
msgstr ""
"<ulink url=\"&url-fhs-home;\">Filesystem Hierarchy Standard</ulink> (FHS) 프"
"로젝트는 리눅스 파일 시스템의 구성을 표준화하는 프로젝트입니다. FHS는 개발한 "
"패키지가 여러가지 GNU/리눅스 배포판에 어떻게 설치되는 지 고민할 필요 없이, 소"
"프트웨어 개발자가 프로그램 설계에만 전념할 수 있도록 만드는 프로젝트입니다."

#. Tag: para
#: welcome.xml:109
#, no-c-format
msgid ""
"<ulink url=\"&url-debian-jr;\">Debian Jr.</ulink> is an internal project, "
"aimed at making sure Debian has something to offer to our youngest users."
msgstr ""
"<ulink url=\"&url-debian-jr;\">데비안 쥬니어 (Debian Junior)</ulink> 프로젝트"
"는 데비안 내부 프로젝트로 어린이 사용자가 사용할 만한 데비안을 만드는 프로젝"
"트입니다."

#. Tag: para
#: welcome.xml:118
#, no-c-format
msgid ""
"For more general information about Debian, see the <ulink url=\"&url-debian-"
"faq;\">Debian FAQ</ulink>."
msgstr ""
"데비안에 관해 더 일반적인 정보는 <ulink url=\"&url-debian-faq;\">데비안 FAQ</"
"ulink>를 참고하십시오."

#. Tag: title
#: welcome.xml:133
#, no-c-format
msgid "What is GNU/Linux?"
msgstr "GNU/리눅스란?"

#. Tag: para
#: welcome.xml:134
#, no-c-format
msgid ""
"Linux is an operating system: a series of programs that let you interact "
"with your computer and run other programs."
msgstr ""
"리눅스는 운영체제입니다. 운영체제는 여러가지 프로그램의 모음으로, 이 프로그램"
"을 이용해 컴퓨터를 사용하고 다른 프로그램을 실행하기도 합니다."

#. Tag: para
#: welcome.xml:139
#, no-c-format
msgid ""
"An operating system consists of various fundamental programs which are "
"needed by your computer so that it can communicate and receive instructions "
"from users; read and write data to hard disks, tapes, and printers; control "
"the use of memory; and run other software. The most important part of an "
"operating system is the kernel. In a GNU/Linux system, Linux is the kernel "
"component. The rest of the system consists of other programs, many of which "
"were written by or for the GNU Project. Because the Linux kernel alone does "
"not form a working operating system, we prefer to use the term <quote>GNU/"
"Linux</quote> to refer to systems that many people casually refer to as "
"<quote>Linux</quote>."
msgstr ""
"운영체제는 컴퓨터에 필요한 여러 가지 기초적인 프로그램으로 구성되어 있고, 이 "
"프로그램을 이용해 사용자는 컴퓨터와 의사소통을 하고 컴퓨터에 지시를 내립니"
"다. 예를 들어 하드 디스크, 테이프, 프린터로 데이터를 보내거나 여기에서 데이터"
"를 읽어들이며, 메모리 사용을 제어하고, 다른 프로그램을 실행합니다. 운영체제"
"의 가장 중요한 부분이 바로 커널입니다, GNU/리눅스 시스템에서 리눅스는 커널 부"
"분을 말합니다. 시스템의 나머지 부분은 기타 프로그램으로 구성되며, GNU 프로젝"
"트가 많은 부분을 개발했습니다. 리눅스 커널 그 자체만으로는 시스템을 구성할 "
"수 없기 때문에, 우리는 흔히 리눅스라고 호칭하는 시스템을 GNU/리눅스라는 이름"
"으로 사용합니다."

#. Tag: para
#: welcome.xml:153
#, no-c-format
msgid ""
"Linux is modelled on the Unix operating system. From the start, Linux was "
"designed to be a multi-tasking, multi-user system. These facts are enough to "
"make Linux different from other well-known operating systems. However, Linux "
"is even more different than you might imagine. In contrast to other "
"operating systems, nobody owns Linux. Much of its development is done by "
"unpaid volunteers."
msgstr ""
"리눅스는 유닉스 (Unix) 운영체제를 모델로 만든 것입니다. 애초부터 리눅스는 다"
"중 작업, 다중 사용자 시스템으로 설계되었습니다. 다중 작업, 다중 사용자 시스템"
"이라는 것만으로도 리눅스는 여타의 잘 알려진 운영체제와 차별됩니다. 그러나 리"
"눅스는 여러분이 생각하는 것 이상으로 다른 점이 아주 많습니다. 다른 운영체제와"
"는 달리 어느 누구도 리눅스를 소유하지 않습니다. 자원자의 힘으로 리눅스의 상"
"당 부분을 개발했습니다."

#. Tag: para
#: welcome.xml:162
#, no-c-format
msgid ""
"Development of what later became GNU/Linux began in 1984, when the <ulink "
"url=\"&url-fsf;\">Free Software Foundation</ulink> began development of a "
"free Unix-like operating system called GNU."
msgstr ""
"후에 GNU/리눅스라고 불리게 된 시스템의 개발은 1984년에 시작되었으며, 이 때 "
"<ulink url=\"&url-fsf;\">자유 소프트웨어 재단(FSF)</ulink>은 유닉스와 유사한 "
"운영체제의 개발을 시작하면서 그 이름을 GNU라고 했습니다."

#. Tag: para
#: welcome.xml:168
#, no-c-format
msgid ""
"The <ulink url=\"&url-gnu;\">GNU Project</ulink> has developed a "
"comprehensive set of free software tools for use with Unix&trade; and Unix-"
"like operating systems such as Linux. These tools enable users to perform "
"tasks ranging from the mundane (such as copying or removing files from the "
"system) to the arcane (such as writing and compiling programs or doing "
"sophisticated editing in a variety of document formats)."
msgstr ""
"<ulink url=\"&url-gnu;\">GNU 프로젝트</ulink>는 유닉스 (Unix&trade;) 및 리눅"
"스처럼 유닉스와 유사한 운영체제에서 사용할 수 있는 일련의 자유 소프트웨어 도"
"구를 개발해 왔습니다. 이러한 도구를 이용해 사용자는 파일을 복사하거나 지우는 "
"아주 일상적인 작업부터, 프로그램 작성과 컴파일, 여러가지 종류의 문서 편집에 "
"이르기까지 다양한 작업을 할 수 있습니다."

#. Tag: para
#: welcome.xml:178
#, no-c-format
msgid ""
"While many groups and individuals have contributed to Linux, the largest "
"single contributor is still the Free Software Foundation, which created not "
"only most of the tools used in Linux, but also the philosophy and the "
"community that made Linux possible."
msgstr ""
"수많은 단체와 개인이 리눅스의 개발에 기여했지만, 단독으로 가장 크게 기여한 곳"
"은 자유 소프트웨어 재단입니다. 자유 소프트웨어 재단은 리눅스에서 사용하는 도"
"구의 대부분을 개발했을 뿐 아니라, 리눅스가 생겨나게 했던 철학과 공동체를 만들"
"어 냈습니다."

#. Tag: para
#: welcome.xml:185
#, no-c-format
msgid ""
"The <ulink url=\"&url-kernel-org;\">Linux kernel</ulink> first appeared in "
"1991, when a Finnish computing science student named Linus Torvalds "
"announced an early version of a replacement kernel for Minix to the Usenet "
"newsgroup <userinput>comp.os.minix</userinput>. See Linux International's "
"<ulink url=\"&url-linux-history;\">Linux History Page</ulink>."
msgstr ""
"<ulink url=\"&url-kernel-org;\">리눅스 커널</ulink>은 리누스 토발즈라는 "
"(Linus Torvalds) 핀란드의 한 컴퓨터 과학 대학생이 1991년 유즈넷 뉴스그룹 "
"<userinput>comp.os.minix</userinput>에 미닉스(Minix)를 대체하는 커널의 초기 "
"버전을 발표하면서 처음으로 그 모습을 세상에 드러냈습니다. 자세한 사항은 리눅"
"스 인터네셔널의 <ulink url=\"&url-linux-history;\">리눅스 역사 페이지 (Linux "
"History Page)</ulink>를 참고하십시오."

#. Tag: para
#: welcome.xml:194
#, no-c-format
msgid ""
"Linus Torvalds continues to coordinate the work of several hundred "
"developers with the help of a few trusty deputies. An excellent weekly "
"summary of discussions on the <userinput>linux-kernel</userinput> mailing "
"list is <ulink url=\"&url-kernel-traffic;\">Kernel Traffic</ulink>. More "
"information about the <userinput>linux-kernel</userinput> mailing list can "
"be found on the <ulink url=\"&url-linux-kernel-list-faq;\">linux-kernel "
"mailing list FAQ</ulink>."
msgstr ""
"리누스 토발즈는 믿음직한 대표자 몇 명의 도움을 받아 수백명에 달하는 개발자의 "
"작업을 조율하고 있습니다. <ulink url=\"&url-kernel-traffic;\">커널 트래픽"
"(Kernel Traffic)</ulink>은 <userinput>linux-kernel</userinput> 메일링 리스트"
"의 토론을 주 단위로 훌륭하게 요약해 놓았습니다. <userinput>linux-kernel</"
"userinput> 메일링 리스트에 대한 자세한 정보는 <ulink url=\"&url-linux-kernel-"
"list-faq;\">linux-kernel 메일링 리스트 FAQ</ulink>에서 찾아 보십시오."

#. Tag: para
#: welcome.xml:205
#, no-c-format
msgid ""
"Linux users have immense freedom of choice in their software. For example, "
"Linux users can choose from a dozen different command line shells and "
"several graphical desktops. This selection is often bewildering to users of "
"other operating systems, who are not used to thinking of the command line or "
"desktop as something that they can change."
msgstr ""
"리눅스 사용자는 소프트웨어 선택에 많은 자유를 갖고 있습니다. 예를 들어 리눅"
"스 사용자는 12개의 다른 커맨드 라인 쉘, 여러가지 그래픽 데스크탑 중에서 원하"
"는 소프트웨어를 선택할 수 있습니다. 이와 같이 선택의 폭이 넓어서 쉘이나 데스"
"크탑을 바꿀 수 있다는 걸 전혀 생각치 못했던 사용자가 당황스러워 하기도 합니"
"다."

#. Tag: para
#: welcome.xml:214
#, no-c-format
msgid ""
"Linux is also less likely to crash, better able to run more than one program "
"at the same time, and more secure than many operating systems. With these "
"advantages, Linux is the fastest growing operating system in the server "
"market. More recently, Linux has begun to be popular among home and business "
"users as well."
msgstr ""
"또한 리눅스는 여타 운영체제에 비해 시스템이 멈추는 경우가 적고, 동시에 둘 이"
"상의 프로그램을 실행하는 성능이 월등하고, 보안에 강합니다. 리눅스는 서버 시장"
"에서 가장 성장이 빠른 운영체제입니다. 최근에 리눅스는 가정과 업무용 사용자에"
"게도 퍼져 나가고 있습니다."

# &debian; => 데비안 GNU/리눅스 (조사 사용 주의)
#. Tag: title
#: welcome.xml:232
#, no-c-format
msgid "What is &debian;?"
msgstr "&debian;란?"

# &debian; => 데비안 GNU/리눅스, 조사 구분에 주의
#. Tag: para
#: welcome.xml:233
#, no-c-format
msgid ""
"The combination of Debian's philosophy and methodology and the GNU tools, "
"the Linux kernel, and other important free software, form a unique software "
"distribution called &debian;. This distribution is made up of a large number "
"of software <emphasis>packages</emphasis>. Each package in the distribution "
"contains executables, scripts, documentation, and configuration information, "
"and has a <emphasis>maintainer</emphasis> who is primarily responsible for "
"keeping the package up-to-date, tracking bug reports, and communicating with "
"the upstream author(s) of the packaged software. Our extremely large user "
"base, combined with our bug tracking system ensures that problems are found "
"and fixed quickly."
msgstr ""
"데비안의 철학 및 방법론과 GNU 도구, 리눅스 커널, 그리고 기타 중요한 자유소프"
"트웨어가 모여 &debian;라는 독특한 배포판을 만듭니다. 이 배포판은 수많은 소프"
"트웨어 <emphasis>패키지</emphasis>로 구성됩니다. 배포판의 각 패키지는 실행파"
"일, 스크립트, 문서, 설정 정보가 들어 있으며 <emphasis>메인테이너</emphasis>"
"가 관리합니다. 메인테이너는 각 패키지를 항상 최신으로 유지하고, 버그 리포트"
"를 추적하고, 패키지로 만든 소프트웨어의 원 개발자와 연락을 하고 있습니다. 데"
"비안의 거대한 사용자 기반이 버그 추적 시스템과 결합해 문제점을 빠르게 찾아내"
"고 수정할 수 있습니다."

#. Tag: para
#: welcome.xml:247
#, no-c-format
msgid ""
"Debian's attention to detail allows us to produce a high-quality, stable, "
"and scalable distribution. Installations can be easily configured to serve "
"many roles, from stripped-down firewalls to desktop scientific workstations "
"to high-end network servers."
msgstr ""
"데비안이 세세한 신경을 쓰는 덕분에 높은 품질의, 안정적이고, 확장성 좋은 배포"
"판을 만들어 냅니다. 간단한 설치 설정에 따라 방화벽부터 데스크탑 공학용 워크스"
"테이션과 고성능 네트워크 서버까지 다양한 역할을 할 수 있습니다."

#. Tag: para
#: welcome.xml:254
#, no-c-format
msgid ""
"Debian is especially popular among advanced users because of its technical "
"excellence and its deep commitment to the needs and expectations of the "
"Linux community. Debian also introduced many features to Linux that are now "
"commonplace."
msgstr ""
"기술적으로 우수하면서 리눅스 공동체의 필요와 기대에 대해 적극적으로 참여하기 "
"때문에, 데비안은 고급 사용자에게 특히 인기가 있습니다. 또한 데비안은 지금은 "
"리눅스에 일반적인 기능이 된 많은 기능을 새로 도입하는 데 앞장 서 왔습니다."

#. Tag: para
#: welcome.xml:261
#, no-c-format
msgid ""
"For example, Debian was the first Linux distribution to include a package "
"management system for easy installation and removal of software. It was also "
"the first Linux distribution that could be upgraded without requiring "
"reinstallation."
msgstr ""
"예를 들어보면, 소프트웨어의 설치와 제거를 손쉽게 할 수 있는 패키지 관리 시스"
"템이 들어간 최초의 리눅스 배포판이 데비안입니다. 또 다시 설치하지 않고도 업그"
"레이드할 수 있는 최초의 배포판입니다."

#. Tag: para
#: welcome.xml:268
#, no-c-format
msgid ""
"Debian continues to be a leader in Linux development. Its development "
"process is an example of just how well the Open Source development model can "
"work &mdash; even for very complex tasks such as building and maintaining a "
"complete operating system."
msgstr ""
"데비안은 리눅스 개발에 있어 여전히 앞서 나가고 있습니다. 데비안의 개발 과정"
"은 오픈 소스 개발 모델이 (전체 운영체제의 개발 및 관리같은 복잡한 일이라고 해"
"도) 얼마나 잘 진행되는 지를 보여주는 한 예입니다."

#. Tag: para
#: welcome.xml:275
#, no-c-format
msgid ""
"The feature that most distinguishes Debian from other Linux distributions is "
"its package management system. These tools give the administrator of a "
"Debian system complete control over the packages installed on that system, "
"including the ability to install a single package or automatically update "
"the entire operating system. Individual packages can also be protected from "
"being updated. You can even tell the package management system about "
"software you have compiled yourself and what dependencies it fulfills."
msgstr ""
"데비안이 다른 리눅스 배포판과 다른 가장 큰 특징은 데비안의 패키지 관리 시스템"
"입니다. 데비안 시스템은 패키지 관리 도구를 이용해 시스템에 설치된 패키지를 완"
"벽하게 제어합니다. 패키지 하나하나를 설치하는 일에서부터 자동으로 전체 운영체"
"제를 업데이트하는 것까지 할 수 있습니다. 또한 개별 패키지에 대해 업데이트를 "
"하지 않도록 조정할 수도 있습니다. 직접 컴파일한 소프트웨어을 패키지 관리 시스"
"템에 등록해 놓고 어떤 의존성이 필요한지 지정할 수도 있습니다."

#. Tag: para
#: welcome.xml:286
#, no-c-format
msgid ""
"To protect your system against <quote>Trojan horses</quote> and other "
"malevolent software, Debian's servers verify that uploaded packages come "
"from their registered Debian maintainers. Debian packagers also take great "
"care to configure their packages in a secure manner. When security problems "
"in shipped packages do appear, fixes are usually available very quickly. "
"With Debian's simple update options, security fixes can be downloaded and "
"installed automatically across the Internet."
msgstr ""
"여러분의 시스템을 <quote>트로이 목마</quote> 및 기타 악의적인 소프트웨어로부"
"터 보호하기 위해, 각 패키지마다 등록된 데비안 메인테이너가 패키지를 업로드하"
"면 그 패키지가 올바른지 검사합니다. 데비안 패키지를 만드는 사람은 안전한 방법"
"으로 자신의 패키지를 설정하려고 상당한 주의를 기울입니다. 배포한 패키지에 보"
"안 문제가 발생하면, 상당히 빨리 수정 버전이 나옵니다. 데비안의 간단한 업데이"
"트 옵션을 이용하면, 인터넷을 통해 보안 업데이트 버전을 자동으로 다운로드하고 "
"설치할 수 있습니다."

#. Tag: para
#: welcome.xml:296
#, no-c-format
msgid ""
"The primary, and best, method of getting support for your &debian; system "
"and communicating with Debian Developers is through the many mailing lists "
"maintained by the Debian Project (there are more than &num-of-debian-"
"maillists; at this writing). The easiest way to subscribe to one or more of "
"these lists is visit <ulink url=\"&url-debian-lists-subscribe;\"> Debian's "
"mailing list subscription page</ulink> and fill out the form you'll find "
"there."
msgstr ""
"여러분의 &debian; 시스템에 대한 지원을 받고 데비안 개발자와 연락을 할 수 있"
"는 많이 사용하는 방법이면서 가장 좋은 방법은, 데비안 프로젝트에서 관리하는 여"
"러가지 메일링 리스트(이 글이 작성된 시점에 약 &num-of-debian-maillists;개 이"
"상)를 이용하는 것입니다. 메일링 리스트에 가입하려면, <ulink url=\"&url-"
"debian-lists-subscribe;\">데비안 메일링 리스트 가입 페이지</ulink>에서 그 페"
"이지에 있는 양식을 채우면 됩니다."

#. Tag: title
#: welcome.xml:318
#, no-c-format
msgid "What is Debian GNU/Hurd?"
msgstr "데비안 GNU/Hurd란?"

#. Tag: para
#: welcome.xml:320
#, no-c-format
msgid ""
"Debian GNU/Hurd is a Debian GNU system that replaces the Linux monolithic "
"kernel with the GNU Hurd &mdash; a set of servers running on top of the GNU "
"Mach microkernel. The Hurd is still unfinished, and is unsuitable for day-to-"
"day use, but work is continuing. The Hurd is currently only being developed "
"for the i386 architecture, although ports to other architectures will be "
"made once the system becomes more stable."
msgstr ""
"데비안 GNU/Hurd는 리눅스 모노리딕 (Monolithic) 커널을 GNU Hurd로 바꾼 데비안 "
"GNU 시스템으로, GNU 마크 (Mach) 마이크로 커널에서 동작하는 서버의 모음입니"
"다. Hurd는 아직까지 개발이 끝나지 않았고, 일상적인 용도로 사용하기에는 적합하"
"지 않지만, 개발은 계속 진행 중입니다. 현재 Hurd는 i386 아키텍처용으로만 개발"
"중이지만, 일단 시스템이 더 안정되면 다른 아키텍처로 포팅될 것입니다."

#. Tag: para
#: welcome.xml:330
#, no-c-format
msgid ""
"For more information, see the <ulink url=\"http://www.debian.org/ports/hurd/"
"\"> Debian GNU/Hurd ports page</ulink> and the <email>debian-hurd@lists."
"debian.org</email> mailing list."
msgstr ""
"보다 자세한 정보는 <ulink url=\"http://www.debian.org/ports/hurd/\">데비안 "
"GNU/Hurd 포팅 페이지</ulink>와 <email>debian-hurd@lists.debian.org</email> 메"
"일링 리스트를 참고하시기 바랍니다."

#. Tag: title
#: welcome.xml:347
#, no-c-format
msgid "Getting Debian"
msgstr "데비안 구하기"

#. Tag: para
#: welcome.xml:349
#, no-c-format
msgid ""
"For information on how to download &debian; from the Internet or from whom "
"official Debian CDs can be purchased, see the <ulink url=\"&url-debian-"
"distrib;\">distribution web page</ulink>. The <ulink url=\"&url-debian-"
"mirrors;\">list of Debian mirrors</ulink> contains a full set of official "
"Debian mirrors, so you can easily find the nearest one."
msgstr ""
"공식 데비안 CD를 인터넷에서 다운로드할 수 있는 곳이나 구입할 수 있는 곳에 대"
"해서는 <ulink url=\"&url-debian-distrib;\">배포판 웹 페이지</ulink>를 참고하"
"십시오. <ulink url=\"&url-debian-mirrors;\">데비안 미러 목록</ulink>에 공식적"
"인 데비안 미러 사이트가 모두 정리되어 있으니, 여러분에게 가장 가까운 미러 사"
"이트를 쉽게 찾을 수 있을 것입니다."

#. Tag: para
#: welcome.xml:358
#, no-c-format
msgid ""
"Debian can be upgraded after installation very easily. The installation "
"procedure will help set up the system so that you can make those upgrades "
"once installation is complete, if need be."
msgstr ""
"데비안은 설치한 뒤에 쉽게 업그레이드 할 수 있습니다. 일단 설치 과정을 마친 시"
"스템은 필요할 때 언제든지 업그레이드할 수 있습니다."

#. Tag: title
#: welcome.xml:373
#, no-c-format
msgid "Getting the Newest Version of This Document"
msgstr "이 문서의 최신 버전 구하는 법"

#. Tag: para
#: welcome.xml:375
#, no-c-format
msgid ""
"This document is constantly being revised. Be sure to check the <ulink url="
"\"&url-release-area;\"> Debian &release; pages</ulink> for any last-minute "
"information about the &release; release of the &debian; system. Updated "
"versions of this installation manual are also available from the <ulink url="
"\"&url-install-manual;\">official Install Manual pages</ulink>."
msgstr ""
"이 문서는 계속해서 바뀝니다. &debian; 시스템의 &release; 릴리스에 대한 최신 "
"정보는 <ulink url=\"&url-release-area;\">데비안 &release; 페이지</ulink>를 확"
"인하기 바랍니다. 이 설치문서의 최신 버전은 <ulink url=\"&url-install-manual;"
"\">공식 데비안 설치 안내서 페이지</ulink>에서도 구할 수 있습니다."

#. Tag: title
#: welcome.xml:393
#, no-c-format
msgid "Organization of This Document"
msgstr "이 문서의 구성"

#. Tag: para
#: welcome.xml:395
#, no-c-format
msgid ""
"This document is meant to serve as a manual for first-time Debian users. It "
"tries to make as few assumptions as possible about your level of expertise. "
"However, we do assume that you have a general understanding of how the "
"hardware in your computer works."
msgstr ""
"이 문서는 데비안을 처음 사용하는 분을 위한 설명서입니다. 가능하면 읽는 사람"
"이 전문지식을 갖추지 않았다고 생각하고 이 문서를 작성했습니다. 하지만 컴퓨터"
"에 장착된 하드웨어가 동작하는 방식은 어느정도 이해하고 있다고 가정했습니다."

#. Tag: para
#: welcome.xml:402
#, no-c-format
msgid ""
"Expert users may also find interesting reference information in this "
"document, including minimum installation sizes, details about the hardware "
"supported by the Debian installation system, and so on. We encourage expert "
"users to jump around in the document."
msgstr ""
"전문적인 사용자도 이 문서를 읽으면 여러 가지 관심있는 정보를 알 수 있습니다. "
"예를 들면 최소 설치 크기, 데비안 설치 시스템이 지원하는 하드웨어 등과 같은 내"
"용이 있습니다. 전문적인 사용자는 이 문서를 처음부터 읽지 말고 필요한 부분만 "
"읽어 나가기를 권합니다."

# &debian; => 데비안 GNU/리눅스, 조사 구분에 주의
#. Tag: para
#: welcome.xml:409
#, no-c-format
msgid ""
"In general, this manual is arranged in a linear fashion, walking you through "
"the installation process from start to finish. Here are the steps in "
"installing &debian;, and the sections of this document which correlate with "
"each step:"
msgstr ""
"이 안내서는 설치 과정의 처음부터 끝까지, 각 과정의 순서대로 쓰여 있습니다. 아"
"래는 &debian;를 설치하는 과정으로, 각 과정에 관련된 문서 부분을 안내해 놓았습"
"니다:"

#. Tag: para
#: welcome.xml:417
#, no-c-format
msgid ""
"Determine whether your hardware meets the requirements for using the "
"installation system, in <xref linkend=\"hardware-req\"/>."
msgstr ""
"<xref linkend=\"hardware-req\"/>에서, 하드웨어가 설치 시스템에 필요한 요구사"
"항을 만족하는 지 판단합니다."

#. Tag: para
#: welcome.xml:423
#, no-c-format
msgid ""
"Backup your system, perform any necessary planning and hardware "
"configuration prior to installing Debian, in <xref linkend=\"preparing\"/>. "
"If you are preparing a multi-boot system, you may need to create "
"partitionable space on your hard disk for Debian to use."
msgstr ""
"<xref linkend=\"preparing\"/>에서, 데비안을 설치하기에 앞서 여러분의 시스템"
"을 백업하고, 필요한 계획을 세우고 하드웨어를 설정합니다. 멀티 부팅을 생각하"
"고 있다면, 데비안을 설치하려는 하드디스크에 파티션 가능한 공간을 만들어야 할 "
"수도 있습니다."

#. Tag: para
#: welcome.xml:431
#, no-c-format
msgid ""
"In <xref linkend=\"install-methods\"/>, you will obtain the necessary "
"installation files for your method of installation."
msgstr ""
"<xref linkend=\"install-methods\"/>에서, 설치하는 방법에 따라 필요한 설치 파"
"일을 구합니다."

# FIXME: bug #344693 참고, xml파일이 (아니면 xmlpo 설정) 고쳐져야 함.  원문은 "<xref ... > describes ..."
#. Tag: para
#: welcome.xml:437
#, no-c-format
msgid ""
"describes booting into the installation system. This chapter also discusses "
"troubleshooting procedures in case you have problems with this step."
msgstr ""
"에서, 설치 시스템으로 부팅하는 방법을 설명합니다. 이 과정에서 문제가 발생한 "
"경우 문제를 해결하는 방법도 이 장에서 설명합니다."

#. Tag: para
#: welcome.xml:444
#, no-c-format
msgid ""
"Perform the actual installation according to <xref linkend=\"d-i-intro\"/>. "
"This involves choosing your language, configuring peripheral driver modules, "
"configuring your network connection, so that remaining installation files "
"can be obtained directly from a Debian server (if you are not installing "
"from a CD), partitioning your hard drives and installation of a base system, "
"then selection and installation of tasks. (Some background about setting up "
"the partitions for your Debian system is explained in <xref linkend="
"\"partitioning\"/>.)"
msgstr ""
"<xref linkend=\"d-i-intro\"/>에 따라 실제 설치를 진행합니다. 이 단계에서 여러"
"분이 사용하는 언어의 선택, 주변장치 드라이버 모듈 설정, 네트워크 설정, 하드 "
"디스크 파티션 작업과 베이스 시스템의 설치를 진행합니다. 특히 (CD로 설치하는 "
"경우가 아니라면) 네트워크를 설정한 다음 나머지 설치 파일을 모두 데비안 서버로"
"부터 직접 받아 올 수 있습니다. (데비안 시스템에서 파티션을 설정하는 방법은 "
"<xref linkend=\"partitioning\"/>에서 설명합니다.)"

#. Tag: para
#: welcome.xml:457
#, no-c-format
msgid ""
"Boot into your newly installed base system, from <xref linkend=\"boot-new\"/"
">."
msgstr ""
"<xref linkend=\"boot-new\"/>에서, 새로 설치한 베이스 시스템으로 부팅합니다."

#. Tag: para
#: welcome.xml:465
#, no-c-format
msgid ""
"Once you've got your system installed, you can read <xref linkend=\"post-"
"install\"/>. That chapter explains where to look to find more information "
"about Unix and Debian, and how to replace your kernel."
msgstr ""
"일단 시스템을 설치했으면, <xref linkend=\"post-install\"/> 부분을 보십시오. "
"여기에서는 유닉스와 데비안에 대해 더 많은 정보을 볼 수 있는 다른 읽을 거리를 "
"소개하고, 커널을 교체하는 방법을 설명합니다."

#. Tag: para
#: welcome.xml:475
#, no-c-format
msgid ""
"Finally, information about this document and how to contribute to it may be "
"found in <xref linkend=\"administrivia\"/>."
msgstr ""
"마지막으로, 이 문서에 대한 정보와 이 문서에 참여하는 방법은 <xref linkend="
"\"administrivia\"/>에 있습니다."

#. Tag: title
#: welcome.xml:485
#, no-c-format
msgid "Your Documentation Help is Welcome"
msgstr "문서에 도움을 주실 분은 언제든지 환영합니다"

#. Tag: para
#: welcome.xml:487
#, no-c-format
msgid ""
"Any help, suggestions, and especially, patches, are greatly appreciated. "
"Working versions of this document can be found at <ulink url=\"&url-d-i-"
"alioth-manual;\"></ulink>. There you will find a list of all the different "
"architectures and languages for which this document is available."
msgstr ""
"어떤 형태든지 이 문서에 대한 도움, 제안, 특히 패치에 대해 깊이 감사드립니다. "
"현재 작업중인 버전은 <ulink url=\"&url-d-i-alioth-manual;\"></ulink>에 있습니"
"다. 이 홈페이지에는 다른 아키텍처용 문서와 다른 여러 언어의 번역판도 더 있습"
"니다."

#. Tag: para
#: welcome.xml:494
#, no-c-format
msgid ""
"Source is also available publicly; look in <xref linkend=\"administrivia\"/> "
"for more information concerning how to contribute. We welcome suggestions, "
"comments, patches, and bug reports (use the package <classname>installation-"
"guide</classname> for bugs, but check first to see if the problem is already "
"reported)."
msgstr ""
"문서의 소스도 공개되어 있습니다. <xref linkend=\"administrivia\"/>를 보시면 "
"문서에 기여하는 방법에 관하여 더 자세히 쓰여 있습니다. 제안, 조언, 패치 그리"
"고 버그 리포트는 언제나 환영합니다. (버그를 리포트할 때는 "
"<classname>installation-guide</classname> 패키지를 이용하시고, 해당 문제가 이"
"미 보고한 문제인지 먼저 확인하시기 바랍니다.)"

#. Tag: title
#: welcome.xml:510
#, no-c-format
msgid "About Copyrights and Software Licenses"
msgstr "저작권 및 소프트웨어 라이선스 정보"

#. Tag: para
#: welcome.xml:513
#, no-c-format
msgid ""
"We're sure that you've read some of the licenses that come with most "
"commercial software &mdash; they usually say that you can only use one copy "
"of the software on a single computer. This system's license isn't like that "
"at all. We encourage you to put a copy of &debian; on every computer in your "
"school or place of business. Lend your installation media to your friends "
"and help them install it on their computers! You can even make thousands of "
"copies and <emphasis>sell</emphasis> them &mdash; albeit with a few "
"restrictions. Your freedom to install and use the system comes directly from "
"Debian being based on <emphasis>free software</emphasis>."
msgstr ""
"대부분의 상업용 소프트웨어에는 라이선스가 따라 나오는데, 아마도 이 라이선스"
"를 조금이라도 읽어 본 적이 있을 것입니다. 이러한 라이선스는 보통 하나의 컴퓨"
"터에 한 복사본만 사용하도록 정하고 있습니다. 데비안 시스템의 라이선스는 이 라"
"이선스와 전혀 다릅니다. &debian; 복사본 하나로 학교나 회사의 모든 컴퓨터에 얼"
"마든지 설치하십시오. 설치 미디어를 여러분의 친구에게 빌려주고 컴퓨터에 설치하"
"는 것을 도와주십시오! 수천장을 복사하고, <emphasis>판매</emphasis>하십시오. "
"(약간 제한이 있습니다.) 데비안은 <emphasis>자유 소프트웨어</emphasis>를 기반"
"으로 하기 때문에 여러분은 자유롭게 시스템을 설치하고 사용할 수 있습니다."

#. Tag: para
#: welcome.xml:526
#, no-c-format
msgid ""
"Calling software <emphasis>free</emphasis> doesn't mean that the software "
"isn't copyrighted, and it doesn't mean that CDs containing that software "
"must be distributed at no charge. Free software, in part, means that the "
"licenses of individual programs do not require you to pay for the privilege "
"of distributing or using those programs. Free software also means that not "
"only may anyone extend, adapt, and modify the software, but that they may "
"distribute the results of their work as well."
msgstr ""
"소프트웨어를 <emphasis>자유(free)</emphasis>라고 부르는 것은 소프트웨어의 저"
"작권이 없다는 뜻은 아니고, 자유 소프트웨어를 배포하는 CD가 무료라는 뜻도 아닙"
"니다. 자유소프트웨어는, 부분적인 의미로는 프로그램의 라이선스에서 프로그램을 "
"배포하고 사용할 권리에 대해 비용을 지불할 필요가 없다는 의미입니다. 또 자유소"
"프트웨어는 누구라도 소프트웨어를 확장하고 개작하고 수정할 수 있으면서, 그렇"
"게 바꾼 결과물도 배포할 수 있습니다."

#. Tag: para
#: welcome.xml:537
#, no-c-format
msgid ""
"The Debian project, as a pragmatic concession to its users, does make some "
"packages available that do not meet our criteria for being free. These "
"packages are not part of the official distribution, however, and are only "
"available from the <userinput>contrib</userinput> or <userinput>non-free</"
"userinput> areas of Debian mirrors or on third-party CD-ROMs; see the <ulink "
"url=\"&url-debian-faq;\">Debian FAQ</ulink>, under <quote>The Debian FTP "
"archives</quote>, for more information about the layout and contents of the "
"archives."
msgstr ""
"데비안 프로젝트는 자유소프트웨어의 기준에 맞지 않는 패키지도 (사용자의 실용"
"적 필요때문에) 일부 배포하고 있습니다. 이 패키지는 공식 배포판에 포함되지 않"
"으며, 데비안 미러 사이트나 써드파티 CD-ROM에 들어 있는 <userinput>contrib</"
"userinput>이나 <userinput>non-free</userinput>에서 구할 수 있습니다. 데비안 "
"아카이브의 구조나 내용에 대해서는 <quote>데비안 FTP 아카이브</quote>의 "
"<ulink url=\"&url-debian-faq;\">데비안 FAQ</ulink>를 참고하기 바랍니다."

#. Tag: para
#: welcome.xml:551
#, no-c-format
msgid ""
"Many of the programs in the system are licensed under the <emphasis>GNU</"
"emphasis> <emphasis>General Public License</emphasis>, often simply referred "
"to as <quote>the GPL</quote>. The GPL requires you to make the "
"<emphasis>source code</emphasis> of the programs available whenever you "
"distribute a binary copy of the program; that provision of the license "
"ensures that any user will be able to modify the software. Because of this "
"provision, the source code<footnote> <para> For information on how to "
"locate, unpack, and build binaries from Debian source packages, see the "
"<ulink url=\"&url-debian-faq;\">Debian FAQ</ulink>, under <quote>Basics of "
"the Debian Package Management System</quote>. </para> </footnote> for all "
"such programs is available in the Debian system."
msgstr ""
"시스템의 많은 프로그램이 사용하고 있는 라이선스는 <quote>GPL</quote>로 알려"
"진 <emphasis>GNU</emphasis> <emphasis> General Public License</emphasis>입니"
"다. GPL 라이선스의 프로그램은 바이너리를 배포할 때 <quote>소스 코드</quote>"
"를 같이 배포해야 합니다. 즉 누구라도 소프트웨어를 수정할 수 있도록 하는 것이 "
"이 라이선스의 규정입니다. 그러한 규정때문에 그러한 프로그램의 모든 소스코드"
"<footnote> <para> 데비안 소스 패키지를 찾고, 압축을 풀고, 바이너리를 빌드하"
"는 방법에 대해서는 <ulink url=\"&url-debian-faq;\">데비안 FAQ</ulink>의 "
"<quote>Basics of the Debian Package Management System</quote> 부분을 보십시"
"오. </para> </footnote>가 데비안 시스템에 들어 있습니다."

#. Tag: para
#: welcome.xml:570
#, no-c-format
msgid ""
"There are several other forms of copyright statements and software licenses "
"used on the programs in Debian. You can find the copyrights and licenses for "
"every package installed on your system by looking in the file <filename>/usr/"
"share/doc/<replaceable>package-name</replaceable>/copyright </filename> once "
"you've installed a package on your system."
msgstr ""
"데비안 프로그램에 사용한 저작권과 소프트웨어 라이선스는 GPL 외에도 여러가지"
"가 있습니다. 패키지를 설치하면 <filename>/usr/share/doc/<replaceable>패키지이"
"름</replaceable>/copyright</filename> 파일을 보면 해당 패키지의 저작권과 라이"
"선스를 볼 수 있습니다."

#. Tag: para
#: welcome.xml:580
#, no-c-format
msgid ""
"For more information about licenses and how Debian determines whether "
"software is free enough to be included in the main distribution, see the "
"<ulink url=\"&url-dfsg;\">Debian Free Software Guidelines</ulink>."
msgstr ""
"라이선스에 관한 정보, 그리고 데비안에서 소프트웨어가 메인 배포판에 포함될 만"
"큼 자유로운지 결정하는 방법에 대해 알고 싶으면 <ulink url=\"&url-dfsg;\">데비"
"안 자유소프트웨어 기준(Debian Free Software Guidelines)</ulink>을 참고하십시"
"오."

#. Tag: para
#: welcome.xml:586
#, no-c-format
msgid ""
"The most important legal notice is that this software comes with "
"<emphasis>no warranties</emphasis>. The programmers who have created this "
"software have done so for the benefit of the community. No guarantee is made "
"as to the suitability of the software for any given purpose. However, since "
"the software is free, you are empowered to modify that software to suit your "
"needs &mdash; and to enjoy the benefits of the changes made by others who "
"have extended the software in this way."
msgstr ""
"가장 중요한 법적인 고지는, 이 소프트웨어는 <emphasis>어떠한 보증도 하지 않는"
"다</emphasis>는 것입니다. 이런 소프트웨어를 만든 프로그래머는 공동체의 이익"
"을 위해 만든 것입니다. 어떤 목적에 대해서도 소프트웨어의 적합성을 보장하지 않"
"습니다. 하지만 소프트웨어가 자유소프트웨어이므로, 여러분에 목적에 맞게 소프트"
"웨어를 수정하는 권리는 여러분에 있습니다. 또한 이런 방법으로 다른 사람이 소프"
"트웨어를 확장하여 생긴 변화로 얻은 이익을 누릴 권리도 여러분에게 있습니다."
