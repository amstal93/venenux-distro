# Korean translations for win32-loader package
# win32-loader 패키지에 대한 한국어 번역문.
# Copyright (C) 2007 THE win32-loader'S COPYRIGHT HOLDER
# This file is distributed under the same license as the win32-loader package.
# Sunjae Park <darehanl@gmail.com>, 2007 - 2008
# Changwoo Ryu <cwryu@debian.org>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-05-04 13:59-0400\n"
"Last-Translator: Sunjae Park <darehanl@gmail.com>\n"
"Language-Team: Korean <debian-l10n-korean@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_KOREAN"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "cp949"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp949"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Korean"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "데비안 설치 프로그램"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Korean.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "데비안 설치 프로그램 로더"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "win32-loader.ini를 찾을 수 없습니다."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini가 완전하지 않습니다. 미디어 제공자에 문의하십시오."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr "키보드 종류를 \"$0\"(으)로 감지했습니다. 맞습니까?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"아래 정보를 첨부해 버그를 보고하십시오.\n"
"\n"
" - 윈도우의 버전.\n"
" - 국가 설정.\n"
" - 키보드의 실제 종류.\n"
" - 감지된 키보드 종류.\n"
"\n"
"감사합니다."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"$c 드라이브에 빈 공간이 충분하지 않은 것 같습니다. 데스크탑 설치를 완전히 하"
"기 위해서는 디스크의 빈 공간이 최소한 3 GB 이상 될 것을 권장합니다. 데비안을 "
"설치할 디스크나 파티션이 따로 있다든나 윈도우를 완전히 대체하고자 한다면 이 "
"경고를 무시하셔도 됩니다."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "오류: 빈 공간이 충분하지 않습니다. 설치 중단하는 중."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "이 프로그램은 아직 윈도우 $windows_version 지원하지 않습니다."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"설치하려는 데비안 버전은 최신의 64비트 컴퓨터에서 동작하도록 설계되어 있습니"
"다. 하지만 이 컴퓨터는 64비트 프로그램을 실행하지 못합니다.\n"
"\n"
"데비안 32비트 (\"i386\") 버전을 사용하시거나, 두가지 운영 체제를 모두 설치할 "
"수 있는 멀티-아키텍처 버전을 사용하십시오.\n"
"\n"
"지금 설치 프로그램을 중단합니다."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"이 컴퓨터는 최신의 64비트 운영 체제가 동작할 수 있습니다. 하지만 설치하려는 "
"데비안 버전은 구식의 32비트 운영 체제에서 동작하도록 설계되어 있습니다.\n"
"\n"
"설치를 계속하실 수 있지만, 이 컴퓨터의 기능을 모두 활용하려면 데비안 64비트 "
"버전을 (\"amd64\") 사용하시거나 두 가지 운영 체제를 다 설치할 수 있는 멀티-아"
"키텍처 버전을 사용하십시오.\n"
"\n"
"지금 종료하시겠습니까?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "설치 방식을 선택하십시오:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "일반 방식. 대부분의 사용자에게 권장합니다."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"전문가 방식. 설치 과정을 완전히 제어하고자 하는 고급 사용자들에게 권장합니다."

#: win32-loader.c:84
msgid "Select action:"
msgstr "작업 선택:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "이 컴퓨터에 데비안 GNU/리눅스를 설치합니다."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "기존 데비안 시스템을 복구합니다(복구 모드)"

#: win32-loader.c:88
msgid "Graphical install"
msgstr "그래픽 환경 설치"

#: win32-loader.c:89
msgid "Text install"
msgstr "텍스트 설치"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "%s 내려받는 중"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "연결하는 중..."

#: win32-loader.c:92
msgid "second"
msgstr "초"

#: win32-loader.c:93
msgid "minute"
msgstr "분"

#: win32-loader.c:94
msgid "hour"
msgstr "시간"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr " "

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%3$d kB 중 %1$d kB (%2$d%%). 속도 %4$d.%05$1dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s 남았음)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "사용하려는 데비안 설치 프로그램 버전을 선택하십시오:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "안정 릴리스. 데비안 \"stable\"를 설치합니다."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"일일 빌드. 데비안 설치 프로그램의 개발 버전입니다. 기본적으로 데비안 "
"\"testing\"을 설치하고, \"stable\"나 \"unstable\"도 설치할 수 있습니다."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"일일 빌드를 사용하시기 전에 알려진 문제점이 있는지 확인하실 것을 권장합니다. "
"지금 확인해보시겠습니까?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "데스크탑 환경:"

#: win32-loader.c:114
msgid "None"
msgstr "없음"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"데비안 설치 프로그램 로더가 다음 매개변수를 사용하도록 설정합니다. 정확하게 "
"알지 못한다면 이 변수들을 절대 변경하지 마십시오."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "프록시 설정 (호스트:포트번호):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "boot.ini의 위치:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "네트워크 부팅 이미지의 기본 URL(linux와 initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "오류"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "오류: $0을(를) $1(으)로 복사하는데 실패했습니다."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "$0 만드는 중"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "선시드 정보를 $0에 추가하는 중"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "오류: $0 실행하지 못합니다."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "부트스트랩 파일의 NTFS 압축을 사용하지 않도록 하는 중"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "데비안 설치 프로그램을 NTLDR에 등록하는 중"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "데비안 설치 프로그램을 BootMgr에 등록하는 중"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "오류: bcdedit.exe의 출력결과를 처리하는데 실패했습니다."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr ""
"오류: boot.ini 찾을 수 없습니다.  실제로 윈도우 $windows_version가 맞습니까?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "매우 중요한 주의사항:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"설치 과정의 두번째 단계를 시작합니다. 확인을 하면 윈도우즈를 DOS 모드에서 다"
"시 시작하고, 데비안 설치 프로그램을 시작합니다."

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"데비안 설치를 계속하시려면 다시 시작해야 합니다. 다음 부팅할 때 윈도우를 시작"
"하실 것인지 데비안 설치 프로그램을 시작하실 것인지를 질문을 받게 됩니다. 설"
"치 과정을 계속하시기 위해서는 데비안 설치 프로그램을 선택하십시오."

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"설치 과정 중에 데비안을 설치하려고 윈도우 파티션을 줄이는 방법과 윈도우 파티"
"션을 완전히 없애는 방법 중에서 선택하실 수 있습니다. 어느 경우이든 데이터를 "
"백업해두시길 강력히 권장합니다. 이 로더 프로그램이나 데비안 프로젝트 어느 쪽"
"도 데이터를 잃어버려도 책임을 지지 않습니다.\\n\\n일단 데비안 설치가 끝나고 "
"(원도우를 디스크에 그대로 두기로 선택하셨다면) 데비안 설치 프로그램 로더 프로"
"그램을 제어판의 프로그램 추가/제거 대화상자에서 삭제하실 수 있습니다."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "지금 다시 시작하시겠습니까?"

#~ msgid "Debconf preseed line:"
#~ msgstr "debconf 미리 설정 파일:"
