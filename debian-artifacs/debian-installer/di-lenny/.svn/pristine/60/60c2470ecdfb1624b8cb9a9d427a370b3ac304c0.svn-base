<!-- retain these comments for translator revision tracking -->
<!-- $Id: installation-media.xml 56150 2008-09-26 10:48:26Z lunar $ -->

 <sect1 id="installation-media">
 <title>Installation Media</title>

<para>

This section will help you determine which different media types you can use to
install Debian. For example, if you have a floppy disk drive on your machine,
it can be used to install Debian. There is a whole chapter devoted to media,
<xref linkend="install-methods"/>, which lists the advantages and
disadvantages of each media type. You may want to refer back to this page once
you reach that section.

</para>

  <sect2 condition="supports-floppy-boot"><title>Floppies</title>
<para>

In some cases, you'll have to do your first boot from floppy disks.
Generally, all you will need is a
high-density (1440 kilobytes) 3.5 inch floppy drive.

</para><para arch="powerpc">

For CHRP, floppy support is currently broken.

</para>
  </sect2>

  <sect2><title>CD-ROM/DVD-ROM</title>

<note><para>

Whenever you see <quote>CD-ROM</quote> in this manual, it applies to both
CD-ROMs and DVD-ROMs, because both technologies are really
the same from the operating system's point of view, except for some very
old nonstandard CD-ROM drives which are neither SCSI nor IDE/ATAPI.

</para></note><para>

CD-ROM based installation is supported for some architectures.
On machines which support bootable CD-ROMs, you should be able to do a
completely
<phrase arch="not-s390">floppy-less</phrase>
<phrase arch="s390">tape-less</phrase>
installation.  Even if your system doesn't
support booting from a CD-ROM, you can use the CD-ROM in conjunction
with the other techniques to install your system, once you've booted
up by other means; see <xref linkend="boot-installer"/>.

</para><para arch="x86">

SCSI, SATA and IDE/ATAPI CD-ROMs are supported.  The <ulink
url="&url-cd-howto;">Linux CD-ROM HOWTO</ulink> contains in-depth information
on using CD-ROMs with Linux.

</para><para arch="x86">

USB CD-ROM drives are also supported, as are FireWire devices that
are supported by the ohci1394 and sbp2 drivers.

</para><para arch="alpha">

Both SCSI and IDE/ATAPI CD-ROMs are supported on &arch-title;, as long
as the controller is supported by the SRM console.  This rules out many
add-on controller cards, but most integrated IDE and SCSI chips and
controller cards that were provided by the manufacturer can be expected
to work.  To find out whether your device is supported from the SRM
console, see the <ulink url="&url-srm-howto;">SRM HOWTO</ulink>.

</para><para arch="arm">

IDE/ATAPI CD-ROMs are supported on all ARM machines.

</para><para arch="mips">

On SGI machines, booting from CD-ROM requires a SCSI CD-ROM drive
capable of working with a logical blocksize of 512 bytes. Many of the
SCSI CD-ROM drives sold on the PC market do not have this
capability. If your CD-ROM drive has a jumper labeled
<quote>Unix/PC</quote> or <quote>512/2048</quote>, place it in the
<quote>Unix</quote> or <quote>512</quote> position.
To start the install, simply choose the <quote>System installation</quote>
entry in the firmware.  The Broadcom BCM91250A supports standard IDE devices,
including CD-ROM drives, but CD images for this platform are currently not
provided because the firmware doesn't recognize CD drives.  In order to
install Debian on an Broadcom BCM91480B evaluation board, you need an PCI
IDE, SATA or SCSI card.

</para>
  </sect2>

  <sect2><title>Hard Disk</title>

<para>

Booting the installation system directly from a hard disk is another option
for many architectures. This will require some other operating system
to load the installer onto the hard disk.

</para><para arch="m68k">

In fact, installation from your local disk is the preferred
installation technique for most &architecture; machines.

</para><para arch="sparc">

Although the &arch-title; does not allow booting from SunOS
(Solaris), you can install from a SunOS partition (UFS slices).

</para>
  </sect2>

  <sect2 condition="bootable-usb"><title>USB Memory Stick</title>

<para>

Many Debian boxes need their floppy and/or CD-ROM drives only for
setting up the system and for rescue purposes. If you operate some
servers, you will probably already have thought about omitting those
drives and using an USB memory stick for installing and (when
necessary) for recovering the system. This is also useful for small
systems which have no room for unnecessary drives.

</para>
  </sect2>

  <sect2><title>Network</title>

<para>

The network can be used during the installation to retrieve files needed
for the installation. Whether the network is used or not depends on the
installation method you choose and your answers to certain questions that
will be asked during the installation. The installation system supports
most types of network connections (including PPPoE, but not ISDN or PPP),
via either HTTP or FTP. After the installation is completed, you can also
configure your system to use ISDN and PPP.

</para><para condition="supports-tftp">

You can also <emphasis>boot</emphasis> the installation system over the
network. <phrase arch="mips">This is the preferred installation technique
for &arch-title;.</phrase>

</para><para condition="supports-nfsroot">

Diskless installation, using network booting from a local area network
and NFS-mounting of all local filesystems, is another option.

</para>
  </sect2>

  <sect2><title>Un*x or GNU system</title>

<para>

If you are running another Unix-like system, you could use it to install
&debian; without using the &d-i; described in the rest of this
manual. This kind of install may be useful for users with otherwise
unsupported hardware or on hosts which can't afford downtime.  If you
are interested in this technique, skip to the <xref
linkend="linux-upgrade"/>.

</para>
  </sect2>

  <sect2><title>Supported Storage Systems</title>

<para>

The Debian boot disks contain a kernel which is built to maximize the
number of systems it runs on.  Unfortunately, this makes for a larger
kernel, which includes many drivers that won't be used for your
machine (see <xref linkend="kernel-baking"/> to learn how to
build your own kernel).  Support for the widest possible range of
devices is desirable in general, to ensure that Debian can be
installed on the widest array of hardware.

</para><para arch="x86">

Generally, the Debian installation system includes support for floppies,
IDE (also known as PATA) drives, IDE floppies, parallel port IDE devices, SATA
and SCSI controllers and drives, USB, and FireWire.  The supported file systems
include FAT, Win-32 FAT extensions (VFAT) and NTFS.

</para><para arch="i386">

Disk interfaces that emulate the <quote>AT</quote> hard disk interface
&mdash; often called MFM, RLL, IDE, or PATA &mdash; are supported.  SATA and
SCSI disk controllers from many different manufacturers are supported. See the
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>
for more details.

</para><para arch="m68k">

Pretty much all storage systems supported by the Linux kernel are
supported by the Debian installation system.  Note that the current
Linux kernel does not support floppies on the Macintosh at all, and
the Debian installation system doesn't support floppies for Amigas.
Also supported on the Atari is the Macintosh HFS system, and AFFS as a
module.  Macs support the Atari (FAT) file system.  Amigas support the
FAT file system, and HFS as a module.

</para><para arch="sparc">

Any storage system supported by the Linux kernel is also supported by
the boot system. The following SCSI drivers are supported in the default
kernel:

<itemizedlist>
<listitem><para>

Sparc ESP

</para></listitem>
<listitem><para>

PTI Qlogic,ISP

</para></listitem>
<listitem><para>

Adaptec AIC7xxx

</para></listitem>
<listitem><para>

NCR and Symbios 53C8XX

</para></listitem>
</itemizedlist>

IDE systems (such as the UltraSPARC 5) are also supported. See
<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>
for more information on SPARC hardware supported by the Linux kernel.

</para><para arch="alpha">

Any storage system supported by the Linux kernel is also supported by
the boot system.  This includes both SCSI and IDE disks.  Note, however,
that on many systems, the SRM console is unable to boot from IDE drives,
and the Jensen is unable to boot from floppies.  (see
<ulink url="&url-jensen-howto;"></ulink>
for more information on booting the Jensen)

</para><para arch="powerpc">

Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
floppies on CHRP systems at all.

</para><para arch="hppa">

Any storage system supported by the Linux kernel is also supported by
the boot system.  Note that the current Linux kernel does not support
the floppy drive.

</para><para arch="mips">

Any storage system supported by the Linux kernel is also supported by
the boot system.

</para><para arch="s390">

Any storage system supported by the Linux kernel is also supported by
the boot system.  This means that FBA and ECKD DASDs are supported with
the old Linux disk layout (ldl) and the new common S/390 disk layout (cdl).

</para>

  </sect2>

 </sect1>
