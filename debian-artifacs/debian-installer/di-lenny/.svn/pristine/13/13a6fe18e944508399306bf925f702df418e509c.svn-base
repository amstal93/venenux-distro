#di-netboot-assistant v0.3 sources file
#
#Format: Each must contain four fields, separated by single tabulation.
# 1st field: An arbitraty repository name (must match "[[:alnum:]_\.-]+")
# 2nd field: The architecture (debian's architecture name)
# 3rd field: The base URL to download files from (it should contain 
#            a debian-installer folder)
#            (If you want to use a mirror, have a look at MIRROR_REGEXPS in
#            the file di-netboot-assistant.conf to rewrite the URL)
# 4th field: That path to the actual netboot archive (or .img) file.
#            (relative to the "base URL" above).
# Lines _starting_ with a "#" are considered as comments and are ignored.
# Empty lines are ignored too.

#Debian/Stable (( http://www.debian.org/distrib/netinst#netboot ))
stable	alpha	http://ftp.debian.org/dists/stable/main/installer-alpha/current/images/	netboot/boot.img
stable	amd64	http://ftp.debian.org/dists/stable/main/installer-amd64/current/images/	netboot/netboot.tar.gz
stable	hppa	http://ftp.debian.org/dists/stable/main/installer-hppa/current/images/	netboot/2.6/netboot.tar.gz
stable	i386	http://ftp.debian.org/dists/stable/main/installer-i386/current/images/	netboot/netboot.tar.gz
stable	ia64	http://ftp.debian.org/dists/stable/main/installer-ia64/current/images/	netboot/netboot.tar.gz
#Debian/Stable:Etch has two sparc images 
stable	sparc32	http://ftp.debian.org/dists/stable/main/installer-sparc/current/images/	sparc32/netboot/2.6/boot.img
stable	sparc64	http://ftp.debian.org/dists/stable/main/installer-sparc/current/images/	sparc64/netboot/2.6/boot.img
#Debian/Stable:(for Lenny+) only supports sparc64 images 
stable	sparc	http://ftp.debian.org/dists/stable/main/installer-sparc/current/images/	sparc/netboot/boot.img

stable-gtk	amd64	http://ftp.debian.org/dists/stable/main/installer-amd64/current/images/	netboot/gtk/netboot.tar.gz
stable-gtk	i386	http://ftp.debian.org/dists/stable/main/installer-i386/current/images/	netboot/gtk/netboot.tar.gz

#debian/Etch
etch	alpha	http://ftp.debian.org/dists/etch/main/installer-alpha/current/images/	netboot/boot.img
etch	amd64	http://ftp.debian.org/dists/etch/main/installer-amd64/current/images/	netboot/netboot.tar.gz
etch	hppa	http://ftp.debian.org/dists/etch/main/installer-hppa/current/images/	netboot/2.6/boot.img
etch	i386	http://ftp.debian.org/dists/etch/main/installer-i386/current/images/	netboot/netboot.tar.gz
etch	ia64	http://ftp.debian.org/dists/etch/main/installer-ia64/current/images/	netboot/netboot.tar.gz
etch	sparc32	http://ftp.debian.org/dists/etch/main/installer-sparc/current/images/	sparc32/netboot/2.6/boot.img
etch	sparc64	http://ftp.debian.org/dists/etch/main/installer-sparc/current/images/	sparc64/netboot/2.6/boot.img

etch-gtk	amd64	http://ftp.debian.org/dists/etch/main/installer-amd64/current/images/	netboot/gtk/netboot.tar.gz
etch-gtk	i386	http://ftp.debian.org/dists/etch/main/installer-i386/current/images/	netboot/gtk/netboot.tar.gz

#Debian/Lenny
lenny	alpha	http://ftp.debian.org/dists/lenny/main/installer-alpha/current/images/	netboot/boot.img
lenny	amd64	http://ftp.debian.org/dists/lenny/main/installer-amd64/current/images/	netboot/netboot.tar.gz
lenny	hppa	http://ftp.debian.org/dists/lenny/main/installer-hppa/current/images/	netboot/2.6/boot.img
lenny	i386	http://ftp.debian.org/dists/lenny/main/installer-i386/current/images/	netboot/netboot.tar.gz
lenny	ia64	http://ftp.debian.org/dists/lenny/main/installer-ia64/current/images/	netboot/netboot.tar.gz
lenny	sparc	http://ftp.debian.org/dists/lenny/main/installer-sparc/current/images/	netboot/boot.img

lenny-gtk	amd64	http://ftp.debian.org/dists/lenny/main/installer-amd64/current/images/	netboot/gtk/netboot.tar.gz
lenny-gtk	i386	http://ftp.debian.org/dists/lenny/main/installer-i386/current/images/	netboot/gtk/netboot.tar.gz

# DEVELOPMENT (( http://www.debian.org/devel/debian-installer/ ))

#beta1 or beta2 or beta3 or rc1 or rc2
testing	alpha	http://ftp.debian.org/dists/testing/main/installer-alpha/current/images/	netboot/boot.img
testing	amd64	http://ftp.debian.org/dists/testing/main/installer-amd64/current/images/	netboot/netboot.tar.gz
testing	hppa	http://ftp.debian.org/dists/testing/main/installer-hppa/current/images/	netboot/2.6/boot.img
testing	i386	http://ftp.debian.org/dists/testing/main/installer-i386/current/images/	netboot/netboot.tar.gz
testing	ia64	http://ftp.debian.org/dists/testing/main/installer-ia64/current/images/	netboot/netboot.tar.gz
testing	sparc	http://ftp.debian.org/dists/testing/main/installer-sparc/current/images/	netboot/boot.img

testing-gtk	amd64	http://ftp.debian.org/dists/testing/main/installer-amd64/current/images/	netboot/gtk/netboot.tar.gz
testing-gtk	i386	http://ftp.debian.org/dists/testing/main/installer-i386/current/images/	netboot/gtk/netboot.tar.gz


#Daily netboot DI images. Read :
#     *   http://people.debian.org/~joeyh/d-i/build-logs.html
#     *   http://wiki.debian.org/DebianInstaller/Today
daily	alpha	http://people.debian.org/~vorlon/d-i/alpha/daily/	netboot/boot.img
daily	amd64	http://people.debian.org/~aba/d-i/images/daily/	netboot/netboot.tar.gz
daily	hppa	http://people.debian.org/~kyle/d-i/hppa/daily/	netboot/2.6/boot.img
daily	i386	http://people.debian.org/~joeyh/d-i/images/daily/	netboot/netboot.tar.gz
daily	ia64	http://people.debian.org/~dannf/d-i/images/daily/	netboot/netboot.tar.gz
daily	sparc	http://people.debian.org/~stappers/d-i/sparc/daily/	netboot/boot.img

daily-gtk	amd64	http://people.debian.org/~aba/d-i/images/daily/	netboot/gtk/netboot.tar.gz
daily-gtk	i386	http://people.debian.org/~joeyh/d-i/images/daily/	netboot/gtk/netboot.tar.gz



# ############ Some Debian Derivatives ################ #

#Ubuntu Dapper 6.06
dapper	i386	http://archive.ubuntu.com/ubuntu/dists/dapper/main/installer-i386/current/images/	netboot/netboot.tar.gz
dapper	amd64	http://archive.ubuntu.com/ubuntu/dists/dapper/main/installer-amd64/current/images/	netboot/netboot.tar.gz

#Ubuntu Hardy 8.04
hardy	i386	http://archive.ubuntu.com/ubuntu/dists/hardy/main/installer-i386/current/images/	netboot/netboot.tar.gz
hardy	amd64	http://archive.ubuntu.com/ubuntu/dists/hardy/main/installer-amd64/current/images/	netboot/netboot.tar.gz


# vim: ft=disources
