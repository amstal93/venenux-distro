<!-- original version: 56150 -->

 <sect1 id="needed-info">
 <title>Benodigde informatie</title>

  <sect2>
  <title>Documentatie</title>

   <sect3>
   <title>Installatiehandleiding</title>

<para condition="for_cd">

Het document dat u nu aan het lezen bent, als gewoon ASCII tekstbestand
of in HTML- of PDF-formaat.

</para>

<itemizedlist condition="for_cd">
&list-install-manual-files;
</itemizedlist>

<para condition="for_wdo">

Het document dat u nu aan het lezen bent. Dit is de officiële versie van
de installatiehandleiding voor release &releasename; van Debian en is
beschikbaar in <ulink url="&url-release-area;/installmanual">diverse
bestandsformaten en vertalingen</ulink>.

</para>

<para condition="for_alioth">

Het document dat u nu aan het lezen bent. Dit is de ontwikkelingsversie van
de installatiehandleiding voor de volgende release van Debian en is
beschikbaar in <ulink url="&url-d-i-alioth-manual;">diverse
bestandsformaten en vertalingen</ulink>.

</para>
</sect3>

   <sect3><title>Hardware documentatie</title>
<para>

Bevat vaak bruikbare informatie over de configuratie of het gebruik
van uw hardware.
</para>

 <itemizedlist arch="x86;m68k;alpha;sparc;mips;mipsel">
<listitem arch="x86"><para>

<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>

</para></listitem>

<listitem arch="m68k"><para>

<ulink url="&url-m68k-faq;">Linux/m68k FAQ</ulink>

</para></listitem>

<listitem arch="alpha"><para>

<ulink url="&url-alpha-faq;">Linux/Alpha FAQ</ulink>

</para></listitem>

<listitem arch="sparc"><para>

<ulink url="&url-sparc-linux-faq;">Linux for SPARC Processors FAQ</ulink>

</para></listitem>

<listitem arch="mips;mipsel"><para>

<ulink url="&url-linux-mips;">Linux/Mips website</ulink>

</para></listitem>

</itemizedlist>
   </sect3>

   <sect3 arch="s390">
   <title>Naslag voor &arch-title;-hardware</title>
<para>

Installatie-instructies en stuurprogramma's ((DASD, XPRAM, Console,
tape, z90 crypto, chandev, netwerk) voor Linux op &arch-title; gebruik
makend van kernel 2.4.

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://oss.software.ibm.com/developerworks/opensource/linux390/docu/l390dd08.pdf">Device Drivers and Installation Commands</ulink>

</para></listitem>
</itemizedlist>

<para>

Redbook van IBM waarin is beschreven hoe Linux kan worden gecombineerd
met z/VM op zSeries en &arch-title; hardware.

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg244987.pdf">
Linux for &arch-title;</ulink>

</para></listitem>
</itemizedlist>

<para>

Redbook van IBM waarin de Linux distributies die beschikbaar zijn voor
het mainframe zijn beschreven.

</para>

<itemizedlist>
<listitem><para>

<ulink url="http://www.redbooks.ibm.com/pubs/pdfs/redbooks/sg246264.pdf">
Linux for IBM eServer zSeries and &arch-title;: Distributions</ulink>

</para></listitem>
</itemizedlist>
   </sect3>
  </sect2>


  <sect2 id="fsohi">
  <title>Bronnen voor informatie over apparatuur</title>
<para>

In veel gevallen zal het installatiesysteem in staat zijn om uw hardware
automatisch te herkennen. Wij adviseren u echter, om voorbereid te zijn,
om uzelf voor de installatie vertrouwd te maken met uw apparatuur.

</para><para>

Informatie over apparatuur kan worden verzameld op basis van:

</para>

<itemizedlist>
<listitem><para>

De handleidingen die bij uw hardware behoren.

</para></listitem>
<listitem><para>

De configurartieschermen voor het BIOS van uw computer. U kunt deze schermen
bekijken door bepaalde toetsen in te drukken tijdens het opstarten van uw
computer. Welke toets(en) u moet indrukken vindt u in uw handleiding.
Vaak is dit de <keycap>Delete</keycap> toets of één van de functietoetsen.
<!-- FJP: op al mijn computers is het één van de functietoetsen //-->

</para></listitem>
<listitem><para>

De verpakkingen van uw hardware.

</para></listitem>

<listitem arch="x86"><para>

Het Systeem-venster in het Configuratiescherm (Control Panel) van Windows.

</para></listitem>
<listitem><para>

Systeemopdrachten of hulpprogramma's in andere besturingssystemen, waaronder
programma's voor bestandsbeheer. Deze bron is met name geschikt om informatie
over het interne geheugen en harde schijf capaciteit te verkrijgen.
<!-- FJP Bedoelen ze niet 'RAM memory and hard drive capacity'? //-->

</para></listitem>
<listitem><para>

Uw systeembeheerder of Internetprovider (ISP). Deze bronnen kunnen u
vertellen welke instellingen u nodig heeft voor de configuratie van
uw netwerk en e-mail.

</para></listitem>
</itemizedlist>

<para>

<table>
<title>Voor een installatie benodigde informatie over hardware</title>
<tgroup cols="2">
<thead>
<row>
  <entry>Hardware</entry><entry>Mogelijk benodigde informatie</entry>
</row>
</thead>

<tbody>
<row arch="not-s390">
  <entry morerows="5">Harde schijven</entry>
  <entry>Hoeveel heeft u er.</entry>
</row>
<row arch="not-s390"><entry>Hun volgorde in het systeem.</entry></row>
<!-- "not-m68k;not-s390" would really turn out to be everything... -->
<row arch="alpha;arm;hppa;x86;ia64;mips;mipsel;powerpc;sparc">
  <entry>Zijn ze IDE (ook wel aangeduid met PATA), SATA of SCSI.</entry>
</row>
<row arch="m68k">
  <entry>Zijn ze IDE of SCSI (de meeste m68k computers hebben SCSI).</entry>
</row>
<row arch="not-s390"><entry>Beschikbare vrije ruimte.</entry></row>
<row arch="not-s390"><entry>Partities.</entry></row>
<row arch="not-s390">
  <entry>Op welke partities andere besturingssystemen geïnstalleerd zijn.</entry>
</row>

<row arch="not-s390">
  <entry morerows="5">Beeldscherm</entry>
  <entry>Model en fabrikant.</entry>
</row>
<row arch="not-s390"><entry>Ondersteunde resoluties.</entry></row>
<row arch="not-s390"><entry>Horizontale frequentie.</entry></row>
<row arch="not-s390"><entry>Verticale frequentie.</entry></row>
<row arch="not-s390">
  <entry>Ondersteunde kleurdieptes (aantal kleuren).</entry>
</row>
<row arch="not-s390"><entry>Grootte van het scherm.</entry></row>

<row arch="not-s390">
  <entry morerows="3">Muis</entry>
  <entry>Type: serieel, PS/2 of USB.</entry>
</row>
<row arch="not-s390"><entry>Poort.</entry></row>
<row arch="not-s390"><entry>Fabrikant.</entry></row>
<row arch="not-s390"><entry>Aantal knoppen.</entry></row>

<row arch="not-s390">
  <entry morerows="1">Netwerk</entry>
  <entry>Model en fabrikant.</entry>
</row>
<row arch="not-s390"><entry>Type van de adapter.</entry></row>

<row arch="not-s390">
  <entry morerows="1">Printer</entry>
  <entry>Model en fabrikant.</entry>
</row>
<row arch="not-s390"><entry>Ondersteunde afdrukresolities.</entry></row>

<row arch="not-s390">
  <entry morerows="2">Videokaart</entry>
  <entry>Model en fabrikant.</entry>
</row>
<row arch="not-s390"><entry>Beschikbaar videogeheugen.</entry></row>
<row arch="not-s390">
  <entry>Ondersteunde resoluties en kleurdieptes (deze dient u te controleren
  ten opzichte van de mogelijkheden van uw beeldscherm).</entry>
</row>

<row arch="s390">
  <entry morerows="1">DASD</entry>
  <entry>Apparaatnummers.</entry>
</row>
<row arch="s390"><entry>Beschikbare vrije ruimte.</entry></row>

<row arch="s390">
  <entry morerows="2">Netwerk</entry>
  <entry>Type van de adapter.</entry>
</row>
<row arch="s390"><entry>Apparaatnummers.</entry></row>
<row arch="s390"><entry>Het relatieve apparaatnummer voor OSA kaarten.</entry></row>

</tbody></tgroup></table>

</para>
  </sect2>


  <sect2>
  <title>Hardware compatibiliteit</title>

<para>

Veel merkproducten werken zonder problemen onder Linux. Sterker nog,
de ondersteuning van apparatuur binnen Linux verbetert met de dag.
Toch ondersteunt Linux nog niet dezelfde variëteit aan apparatuur als
sommige andere besturingssystemen.
<!-- s/hardware for/hardware support for/ ? //-->

</para><para arch="x86">

In het bijzonder kan Linux geen hardware aansturen die vereist dat
een versie van Microsoft Windows actief is.

</para><para arch="x86">

Hoewel het mogelijk is om sommige Windows-specifieke apparatuur onder
Linux aan de praat te krijgen, vereist dit vaak extra inspanning. Daarnaast
horen de stuurprogramma's voor Windows-specifieke apparatuur vaak bij één
bepaalde versie van de Linux kernel waardoor ze snel verouderd kunnen raken.

</para><para arch="x86">

De meest voorkomende apparatuur van dit type zijn de zogenaamde win-modems.
Maar ook printers en andere apparatuur kunnen Windows-specifiek zijn.

</para><para>

U kunt de compabibiliteit van apparatuur vaststellen door:

<itemizedlist>
<listitem><para>

De websites van fabrikanten te raadplegen voor nieuwe stuurprogramma's.

</para></listitem>
<listitem><para>

Op websites of in handleidingen te zoeken naar informatie over emulatie.
Minder bekende merken kunnen soms gebruik maken van de stuurprogramma's
of instellingen van merkapparatuur.

</para></listitem>
<listitem><para>

De overzichten van hardware compatibiliteit voor Linux te raadplegen op
websites gericht op uw platform.

</para></listitem>
<listitem><para>

Op het Internet te zoeken naar de ervaringen van andere gebruikers.

</para></listitem>
</itemizedlist>

</para>
  </sect2>


  <sect2>
  <title>Netwerkinstellingen</title>

<para>

Als uw computer 24 uur per dag is aangesloten op een netwerk (dus een
Ethernet- of vergelijkbare verbinding &mdash; niet een PPP-verbinding)
kunt u deze informatie opvragen bij uw netwerkbeheerder.

<itemizedlist>
<listitem><para>

Uw computernaam (mogelijk mag u die zelf bepalen).

</para></listitem>
<listitem><para>

De domeinnaam van uw netwerk.

</para></listitem>
<listitem><para>

Het IP-adres van uw computer.

</para></listitem>
<listitem><para>

Het voor uw netwerk te gebruiken netwerkmasker.

</para></listitem>
<listitem><para>

Het IP-adres van het systeem (de 'gateway') waarlangs u toegang kunt krijgen tot
andere netwerken, waaronder het Internet (uiteraard <emphasis>alleen</emphasis>
als uw netwerk over een dergelijke gateway beschikt).
<!-- FJP Vertaling 'gateway'? //-->

</para></listitem>
<listitem><para>

Het systeem op uw netwerk dat u als DNS-server (Domain Name Service)
kunt gebruiken.

</para></listitem>
</itemizedlist>

</para><para>

Als uw netwerkbeheerder echter aangeeft dat er een DHCP-server beschikbaar
is en dat het gebruik daarvan de voorkeur verdient, dan heeft u deze
informatie niet nodig omdat de DHCP-server die tijdens het installatieproces
automatisch beschikbaar zal stellen.

</para><para>

Als u gebruik maakt van een draadloos netwerk, zou u ook moeten uitzoeken:

<itemizedlist>
<listitem><para>

ESSID van uw draadloze netwerk.
</para></listitem>
<listitem><para>

WEP-beveiligingssleutel (indien van toepassing).

</para></listitem>
</itemizedlist>

</para>
  </sect2>

 </sect1>
