#! /bin/sh
set -e

. /usr/share/debconf/confmodule
db_capb backup

. /usr/lib/base-installer/library.sh

NEWLINE="
"

db_input low live-installer/mode || true
db_go || exit 10 # back to menu
db_get live-installer/mode
mode="$RET"

install_live_system () {
	# Look at
	PLACES=""
	PLACE_FOUND=0

	# Load filesystem support
	for script in $(ls /lib/live-installer/*); do
		. $script
	done

	for place in $PLACES; do
		[ ! -e $place ] && continue

		SUPPORT=$(echo $place | sed 's,.*\.\(.*\)$,\1,g')
		info "Using $SUPPORT support for $place"

		PLACE_FOUND=1

		eval ${SUPPORT}_prepare
		STEPS=$(eval ${SUPPORT}_count)

		db_progress INFO live-installer/progress/copying

		COUNT=0
		OLD_IFS=$IFS
		mkdir -p /target
		# use tar from inside the live filesystem to create
		# the tarball, because busybox tar in d-i does not 
		# support creating tarballs.
		# 
		# The --exclude is a paranoia measure, in case this program
		# is running from the toplevel of a live filesystem,
		# which is not normally the case.
		exec 4>&0
		chroot . tar c . --exclude=target | \
		(chdir /target && tar xv) | \
		(
			while read line; do
				COUNT=$(($COUNT + 1))
				CURRENT=$(($COUNT * 100 / $STEPS))

				[ x$CURRENT = x$LAST_UPDATE ] && continue

				LAST_UPDATE=$CURRENT
				db_progress STEP 1 <&4
			done
		)
		exec 0>&4
		IFS=$OLD_IFS
	done

	if [ ${PLACE_FOUND} -eq 0 ]; then
		error "Could not find any live images"
		exit 1
	fi

	# if we're dumping it, we need to set the boot mode
	if [ "$mode" = live ]; then
		# which init script to use
		if [ -d /cdrom/casper ]; then
			bootmode=casper
		else
			bootmode=live
		fi

		kopts=
		if db_get debian-installer/add-kernel-opts && [ "$RET" ]; then
			kopts="$RET"
			# remove any existing boot= option
			kopts="$(echo "$kopts" | sed -r "s/(^| )boot=[^ ]*//")"
		fi
		db_set debian-installer/add-kernel-opts \
			"${kopts:+$kopts }boot=$bootmode"

		# skip the hooks
		return
	fi

	# run the scripts found in hook directory after copying the system
	partsdir="/usr/lib/live-installer.d"
	if [ -d "$partsdir" ]; then
		for script in $(ls "$partsdir"/*); do
			base=$(basename $script | sed 's/[0-9]*//')
			if ! db_progress INFO live-installer/progress/$base; then
				db_subst live-installer/progress/fallback SCRIPT "$base"
				db_progress INFO live-installer/progress/fallback
			fi

			if [ -x "$script" ] ; then
				# be careful to preserve exit code
				if log-output -t live-installer "$script"; then
					:
				else
					warning "$script returned error code $?"
				fi
			else
				error "Unable to execute $script"
			fi
		done
	fi
}

waypoint 1	check_target
waypoint 1	get_mirror_info
waypoint 100	install_live_system
waypoint 1	pre_install_hooks
#waypoint 1	setup_dev
waypoint 1	configure_apt_preferences
waypoint 1	configure_apt
waypoint 3	apt_update
#waypoint 2	install_filesystems
waypoint 5	post_install_hooks
#waypoint 1	pick_kernel
#waypoint 20	install_linux
#waypoint 10	install_extra
waypoint 0	cleanup

run_waypoints live-installer/progress/installing

# mount /dev and /proc on target otherwise grub-installer will fail
mount -o bind /dev /target/dev

if [ ! -e /target/proc/version ]; then
	mount -o bind /proc /target/proc
fi

exit 0
