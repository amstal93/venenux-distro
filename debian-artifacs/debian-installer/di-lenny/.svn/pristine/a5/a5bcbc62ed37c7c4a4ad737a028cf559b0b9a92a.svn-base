<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 39465 -->
<!-- revisado por jfs, 30 dic. 2005 -->
<!-- revisado por rudy, 24 feb. 2005 -->

 <sect1 id="directory-tree">
 <title>�rbol de directorios</title>
<para>

&debian; se basa en el
<ulink url="&url-fhs-home;">Est�ndar de jerarqu�a de los sistemas
de ficheros</ulink> (FHS) en lo que se refiere a directorios y nombres.
Este est�ndar permite predecir a los usuarios y programas la
localizaci�n de ficheros y directorios. El
directorio ra�z se representa por una barra vertical
<filename>/</filename>. Todos los sistemas Debian incluyen los siguientes directorios
partiendo del directorio ra�z,

<informaltable>
<tgroup cols="2">
<thead>
<row>
  <entry>Directorio</entry><entry>Contenido</entry>
</row>
</thead>

<tbody>
<row>
  <entry><filename>bin</filename></entry>
  <entry>Binarios esenciales del sistema</entry>
</row><row>
  <entry><filename>boot</filename></entry>
  <entry>Ficheros est�ticos utilizados por el cargador de arranque</entry>
</row><row>
  <entry><filename>dev</filename></entry>
  <entry>Ficheros de dispositivos</entry>
</row><row>
  <entry><filename>etc</filename></entry>
  <entry>Ficheros de configuraci�n espec�ficos del equipo</entry>
</row><row>
  <entry><filename>home</filename></entry>
  <entry>Directorios de los usuarios</entry>
</row><row>
  <entry><filename>lib</filename></entry>
  <entry>Bibliotecas compartidas esenciales y m�dulos del n�cleo</entry>
</row><row>
  <entry><filename>media</filename></entry>
  <entry>Puntos de montaje para medios extra�bles</entry>
</row><row>
  <entry><filename>mnt</filename></entry>
  <entry>Punto de montaje temporal para un sistema de ficheros</entry>
</row><row>
  <entry><filename>proc</filename></entry>
  <entry>Directorio virtual que contiene informaci�n del sistema (n�cleos de la serie 2.4 y 2.6)</entry>
</row><row>
  <entry><filename>root</filename></entry>
  <entry>Directorio del usuario administrador del equipo</entry>
</row><row>
  <entry><filename>sbin</filename></entry>
  <entry>Binarios esenciales del sistema</entry>
</row><row>
  <entry><filename>sys</filename></entry>
  <entry>Directorio virtual que contiene la informaci�n del sistema (n�cleos de la serie 2.6)</entry>
</row><row>
  <entry><filename>tmp</filename></entry>
  <entry>Ficheros temporales</entry>
</row><row>
  <entry><filename>usr</filename></entry>
  <entry>Jerarqu�a secundaria</entry>
</row><row>
  <entry><filename>var</filename></entry>
  <entry>Datos variables</entry>
</row><row>
  <entry><filename>srv</filename></entry>
  <entry>Datos de los servicios ofrecidos por el sistema</entry>
</row><row>
  <entry><filename>opt</filename></entry>
  <entry>Paquetes de programas y aplicaciones opcionales instalados manualmente</entry>
</row>
</tbody></tgroup></informaltable>
</para>

<para>

A continuaci�n, se presenta una lista de consideraciones importantes con
respecto a los directorios y a las particiones. Tenga en cuenta que el uso
de disco var�a mucho para algunas configuraciones espec�ficas de sistemas y
para algunos patrones de uso espec�ficos. Las recomendaciones indicadas a continuaci�n
son gu�as generales y dan un punto de partida para el particionado.

</para>
<itemizedlist>
<listitem><para>

La partici�n ra�z <filename>/</filename> siempre debe contener
f�sicamente las particiones <filename>/etc</filename>, <filename>/bin</filename>,
<filename>/sbin</filename>, <filename>/lib</filename> y
<filename>/dev</filename>, sino el sistema no podr� arrancar.
Habitualmente es suficiente tener de 150 a 200 MB para una partici�n ra�z.

</para></listitem>
<listitem><para>

<filename>/usr</filename>: contiene todos los programas para usuarios
(<filename>/usr/bin</filename>), bibliotecas
(<filename>/usr/lib</filename>), documentaci�n
(<filename>/usr/share/doc</filename>), etc.
�sta es la parte del sistema de ficheros que requiere mayor espacio. Debe
asignar al menos 500 MB de espacio de su disco. Debe incrementar el
tama�o de esta partici�n en funci�n del n�mero y tipo de paquetes que vaya
a instalar. Una instalaci�n generosa de una estaci�n de trabajo o servidor
deber�a tener de 4 a 6 GB.

</para></listitem>
<listitem><para>

<filename>/var</filename>: aqu� se almacenan todos los datos variables tales
como los art�culos de noticias, correo electr�nico, sitios web, el
cache del sistema de empaquetado, etc.
El tama�o de este directorio depende directamente del uso que haga del
sistema, pero para los usuarios normales el valor estar� fijado por
la sobrecarga de la herramienta de gesti�n de paquetes.
Si planea hacer una instalaci�n completa de todos los programas que
le ofrece el sistema Debian, en una sola sesi�n, dejar 2 � 3 GB
de espacio para <filename>/var</filename> deber�a ser suficiente.
Si va a instalar el sistema por partes (esto implica, instalar los
servicios y utilidades, seguidos por herramientas de texto, luego
el entorno gr�fico, ...), deber�a asignar alrededor de 300 � 500
MB. Si va a ser un sistema donde no har� actualizaciones importantes
y tiene problemas de espacio en disco puede llegar a funcionar
con 30 � 40 MB.

</para></listitem>
<listitem><para>

<filename>/tmp</filename>: lo m�s probable es que los datos temporales de los
programas se almacenen en este directorio. Asignar 40 � 100 MB
a esta partici�n deber�a ser suficiente. Algunas aplicaciones hacen uso
de esta ubicaci�n para almacenar ficheros de im�genes de gran tama�o temporalmente,
�ste es el caso de las herramientas manipulaci�n de archivos, de creaci�n de CD � DVD,
y programas multimedia. Si va a utilizar este tipo de programas deber�a ajustar el
tama�o disponible en <filename>/tmp</filename>.

</para></listitem>
<listitem><para>

<filename>/home</filename>: todos los usuarios guardar�n sus datos en un
subdirectorio de este directorio. El tama�o de �ste depende del n�mero de
usuarios que tendr� en el sistema y los ficheros que guardar�n en sus
directorios. Dependiendo de lo que pretenda hacer, deber�a reservar alrededor
de 100 MB para cada usuario pero deber� adaptar este valor a sus necesidades.
Reserve mucho m�s espacio si planea guardar muchos archivos multimedia
(pel�culas, MP3, fotograf�as, etc.) en su directorio de usuario.

</para></listitem>

</itemizedlist>

 </sect1>
