# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Catalan messages for debian-installer.
# Copyright 2002, 2003, 2004, 2005, 2006, 2007, 2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Jordi Mallach <jordi@debian.org>, 2002, 2003, 2004, 2006, 2007, 2008.
# Guillem Jover <guillem@debian.org>, 2005, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer lenny\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-04 14:50+0200\n"
"Last-Translator: Jordi Mallach <jordi@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Mode de rescat"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Entra al mode de rescat"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "No s'ha trobat cap partició"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"L'instal·lador no ha trobat cap partició, així que no podreu muntar un "
"sistema de fitxers arrel. Això pot ser degut a que el nucli no ha detectat "
"el vostre disc dur o no ha pogut llegir la taula de particions, o el disc "
"pot not estar partit. Si voleu, podeu investigar això des d'un intèrpret "
"d'ordres a l'entorn de l'instal·lador."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Dispositiu a utilitzar com a sistema de fitxers arrel:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Introduïu el dispositiu que voleu utilitzar com el vostre sistema de fitxers "
"arrel. Podreu triar entre diverses operacions de rescat a aplicar en aquest "
"sistema de fitxers."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "El dispositiu no existeix"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"El dispositiu que heu introduït com a sistema de fitxers arrel (${DEVICE}) "
"no existeix. Si us plau, proveu-ho de nou."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Ha fallat el muntatge"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"S'ha produït un error en muntar el dispositiu que heu introduït per al "
"vostre sistema de fitxers arrel (${DEVICE}) a /target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr ""
"Si us plau, comproveu el registre del sistema per a obtindre'n més "
"informació."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Operacions de rescat"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Ha fallat l'operació de rescat"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"L'operació de rescat «${OPERATION}» ha fallat amb codi d'eixida ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Executa un intèrpret d'ordres en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Executa un intèrpret d'ordres a l'entorn de l'instal·lador"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Seleccioneu un sistema de fitxers arrel diferent"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Torna a arrencar el sistema"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "S'està executant un intèrpret d'ordres"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Després d'aquest missatge, se us donarà un intèrpret d'ordres amb ${DEVICE} "
"muntat a «/». Si necessiteu cap altre sistema de fitxers (com un «/usr» "
"separat), els haureu de muntar vosaltres."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "S'ha produït un error en executar l'intèrpret d'ordres a /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"S'ha trobat un intèrpret d'ordres (${SHELL}) al vostre sistema de fitxers "
"arrel (${DEVICE}), però s'ha produït un error en executar-lo."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "No hi ha cap intèrpret d'ordres a /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"No s'ha trobat cap intèrpret d'ordres utilitzable al vostre sistema de "
"fitxers arrel (${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Intèrpret d'ordres interactiu en ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Després d'aquest missatge, se us donarà un intèrpret d'ordres amb ${DEVICE} "
"muntat a «/». Podeu treballar en ell utilitzant les eines disponibles a "
"l'entorn de l'instal·lador. Si necessiteu que siga el vostre sistema de "
"fitxers arrel temporalment, executeu «chroot /target». Si necessiteu cap "
"altre sistema de fitxers (com un «/usr» separat), els haureu de muntar "
"vosaltres."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Després d'aquest missatge, se us donarà un intèrpret d'ordres a l'entorn de "
"l'instal·lador. Com l'instal·lador no ha pogut trobar cap partició, no s'ha "
"muntat cap sistema de fitxers."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Intèrpret d'ordres a l'entorn de l'instal·lador"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Frase de pas per a ${DEVICE}"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Introduïu la frase de pas per al volum xifrat ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Si no introduïu res, el volum no estarà disponible durant les operacions de "
"rescat."
