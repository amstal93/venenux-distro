<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56442 -->
<!-- revisado por nahoo, 7 noviembre 2004 -->
<!-- revisado por Rudy Godoy, 22 feb. 2005 -->
<!-- revisado por Igor T�mara, 16 nov. 2008 -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Preparaci�n de ficheros para el arranque desde un dispositivo de memoria USB</title>

<para>

Se provee dos m�todos para arrancar desde una memoria USB. El primero es
instalar completamente desde la red. El segundo consiste en copiar una imagen
de CD en la memoria USB y usarla como fuente de paquetes, posiblemente en
combinaci�n con un sitio de r�plica. El segundo es m�s usado.

</para><para>

El primer m�todo de instalaci�n requiere descargar una imagen de instalador
del directorio <filename>netboot</filename>(en  el sitio mencionado en
<xref linkend="where-files"/>) y usar la <quote>forma flexible</quote>
que se explica m�s adelante para copiar los ficheros en la memoria USB.

</para><para>

Las im�genes de instalaci�n para el segundo m�todo se encuentran en el
directorio <filename>hd-media</filename> y se puede usar tanto la
<quote>forma f�cil</quote> o la <quote>forma flexible</quote> para copiar
la imagen en la memoria USB. Este m�todo de instalaci�n tambi�n requerir�
descargar la imagen del CD. La imagen de instalaci�n y la imagen del CD
deben coincidir con la version del &d-i;. Si no coinciden es muy probable
que surjan errores<footnote>

<para>
El mensaje que se presenta m�s comunmente es que no se pudo encontrar los
m�dulos del n�cleo. Esto significa que la versi�n del m�dulo del n�cleo
udebs inclu�do en la imagen del CD difiere de la versi�n del n�cleo que
est� en ejecuci�n.
</para>

</footnote> durante la instalaci�n.

</para><para>

Para preparar una memoria USB, necesitar� un sistema en el cual ya est�
corriendo GNU/Linux y con soporte para USB. En los sistemas GNU/Linux
actuales la memoria USB deber�a reconocerse autom�ticamente cuando la
inserta. Si no es as�, revise que el m�dulo del n�cleo usb-storage est�
cargado. Cuando se inserta una memoria USB, se mapear� a un dispositivo
llamado <filename>/dev/sdX</filename>, en el que <quote>X</quote> es una
letra entre la a y la z. Al ejecutar la orden <command>dmesg</command>
deber�a poder visualizar a qu� dispositivo se mapea la memoria USB al
insertarla. Para escribir en la memoria, es posible que deba quitar el
seguro de protecci�n contra escritura.

</para>
<warning><para>

�Los procedimientos descritos en esta secci�n destruir�n lo que sea que haya
en el dispositivo! Aseg�rese de elegir correctamente el nombre del dispositivo
para su memoria USB. Si usa el dispositivo incorrecto podr�a terminar
perdiendo toda la informaci�n de, por ejemplo, un disco duro.

</para></warning>
<para>

Note que el dispositivo USB, deber� tener por lo menos una capacidad de 256 MB
(se pueden realizar configuraciones con menos capacidad, tal como se indica en
<xref linkend="usb-copy-flexible"/>).

</para>

  <sect2 id="usb-copy-easy">
  <title>Copiado de los ficheros &mdash; la forma f�cil</title>
<para>

Existe un fichero �todo en uno� <filename>hd-media/boot.img.gz</filename>
que contiene todos los ficheros del instalador(incluyendo el n�cleo)
<phrase arch="x86">al igual que <classname>syslinux</classname> y su fichero
de configuraci�n.</phrase>
<phrase arch="powerpc">al igual que <classname>yaboot</classname> y su
fichero de configuraci�n.</phrase>

</para><para>

Tenga en cuenta que aunque es conveniente, este m�todo tiene una gran
desventaja: el tama�o l�gico del dispositivo se limitar� a 256 MB,
incluso si la capacidad de la memoria USB es mayor. Si quiere contar
con la capacidad  completa de la memoria posteriormente para un uso
diferente, tendr� que reparticionarla y crearle un nuevo sistema de
archivos. Otra desventaja es que no puede copiar una imagen completa
de CD en la memoria USB, solamente im�genes netinst o de tarjeta de
presentaci�n.

</para><para arch="x86">

Para usar esta imagen solamente hace falta extraer directamente en la
memoria USB:

<informalexample><screen>
# zcat boot.img.gz &gt;/dev/<replaceable>sdX</replaceable>
</screen></informalexample>

</para><para arch="powerpc">

Cree una partici�n del tipo �Apple_Bootstrap� en su dispositivo
USB utilizando la orden <userinput>C</userinput> de
<command>mac-fdisk</command> y extraiga la imagen directamente
a �ste:

<informalexample><screen>
# zcat boot.img.gz &gt;/dev/<replaceable>sdX2</replaceable>
</screen></informalexample>

</para><para>

Despu�s de esto, monte la memoria USB
<phrase arch="x86">(<userinput>mount
/dev/<replaceable>sdX</replaceable> /mnt</userinput>),</phrase>
<phrase arch="powerpc">(<userinput>mount
/dev/<replaceable>sdX2</replaceable> /mnt</userinput>),</phrase>
que ahora tendr� un sistema de ficheros
<phrase arch="x86">FAT</phrase>
<phrase arch="powerpc">HFS</phrase>
en ella, y copie una imagen ISO �netinst� o �bussinesscard� de Debian
sobre la memoria USB.
Desmonte el dispositivo USB (<userinput>umount /mnt</userinput>) y habr�
terminado.

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>Copiado de los ficheros &mdash; la forma flexible</title>
<para>

Si desea m�s flexibilidad o simplemente le gusta saber que ocurre,
podr�a usar el siguiente m�todo para colocar los ficheros en su
dispositivo USB. Una ventaja de este m�todo es que &mdash; si la 
capacidad de su memoria USB es lo suficientemente grande &mdash; 
tendr� la opci�n de copiar una imagen completa de CD en ella.

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

   </sect2>

   <!-- TODO: doesn't this section belong later? -->
   <sect2 arch="x86">
   <title>Arranque con la memoria USB</title>

<warning><para>

Si su sistema reh�sa arrancar desde el dispositivo USB, �ste podr�a
contener un registro de arranque maestro (MBR) inv�lido. Para
corregirlo, use la orden <command>install-mbr</command> del
paquete <classname>mbr</classname>:

<informalexample><screen>
# install-mbr /dev/<replaceable>sdX</replaceable>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
