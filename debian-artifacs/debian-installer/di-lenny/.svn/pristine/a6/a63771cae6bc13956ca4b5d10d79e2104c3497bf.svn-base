<!-- original version: 35613 -->

  <sect2 arch="mips" id="boot-tftp"><title>Amorcer avec TFTP</title>

<sect3><title>Amorcer avec TFTP les SGI</title>
<para>
Une fois entré dans le moniteur de commandes, utilisez

<informalexample><screen>
bootp():
</screen></informalexample>

sur les machines SGI pour amorcer Linux et commencer l'installation de 
Debian. Pour ce faire, il sera peut-être nécessaire de déconfigurer la 
variable d'environnement <envar>netaddr</envar>. Tapez&nbsp;:

<informalexample><screen>
unsetenv netaddr
</screen></informalexample>
dans le moniteur de commandes. 

</para>
</sect3>  

<sect3>
   <title>Amorcer avec TFTP les cartes Broadcom BCM91250A et BCM91480B</title>
<para>

Avec les cartes d'évaluation Broadcom BCM91250A et  BCM91480B, vous devez charger via TFTP 
le programme d'amorçage SiByl, lequel chargera et lancera l'installateur
Debian. Dans la plupart des cas vous obtiendrez une adresse IP par DHCP, mais 
il est aussi possible d'indiquer une adresse fixe. Pour utiliser DHCP, il
suffit d'entrer la commande suivante à l'invite CFE&nbsp;:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Une fois l'adresse obtenue, vous pouvez charger SiByl avec la commande 
suivante&nbsp;:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

Vous devez changer l'adresse donnée dans l'exemple et indiquer soit le nom soit
l'adresse de votre serveur TFTP. Une fois cette commande exécutée, 
l'installateur sera automatiquement chargé.
</para>
</sect3>
</sect2>

  <sect2 arch="mips"><title>Paramètres d'amorçage</title>

<sect3>
<title>Amorcer avec TFTP les SGI</title>
<para>
Sur les SGI, vous pouvez ajouter des paramètres d'amorçage après la 
commande <command>bootp():</command> dans le moniteur de commandes.

</para><para>

Après la commande <command>bootp():</command>, vous pouvez ajouter le chemin 
et le nom du fichier à amorcer si vous ne donnez pas un nom explicite via 
votre serveur BOOTP/DHCP. Par exemple&nbsp;:

<informalexample><screen>
bootp():/boot/tftpboot.img
</screen></informalexample>

Vous pouvez passer d'autres paramètres au noyau avec la commande
<command>append</command>&nbsp;:

<informalexample><screen>
bootp(): append="root=/dev/sda1"
</screen></informalexample>
</para>
 </sect3>

<sect3>
   <title>Amorcer avec TFTP les cartes Broadcom BCM91250A et BCM91480B</title>
<para>
Vous ne pouvez pas indiquer des paramètres d'amorçage à l'invite CFE. 
Vous devez modifier le fichier <filename>/boot/sibyl.conf</filename> sur le
serveur TFTP et mettre vos paramètres dans la variable 
<replaceable>extra_args</replaceable>.
</para>
</sect3>

</sect2>
