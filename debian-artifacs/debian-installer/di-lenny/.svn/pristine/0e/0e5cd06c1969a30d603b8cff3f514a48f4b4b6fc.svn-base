<!-- retain these comments for translator revision tracking -->
<!-- original version: 18674 untranslated -->

  <sect2 arch="powerpc" id="boot-cd"><title>Booting from a CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

Currently, the only &arch-title; subarchitectures that support CD-ROM
booting are PReP and New World PowerMacs. On PowerMacs, hold the
<keycap>c</keycap> key, or else the combination of
<keycap>Command</keycap>, <keycap>Option</keycap>,
<keycap>Shift</keycap>, and <keycap>Delete</keycap>
keys together while booting to boot from the CD-ROM.

</para><para>

OldWorld Powermacs will not boot a Debian CD, because OldWorld
computers relied on a MacOSROM CD boot driver to be present on the CD,
and a free-software version of this driver is not available. All
OldWorld systems have floppy drives, so use the floppy drive to launch
the installer, and then point the installer to the CD for the needed
files.

</para><para>

If your system doesn't boot directly from CD-ROM, you can still use
the CD-ROM to install the system. On NewWorlds, you can also use an
OpenFirmware command to boot from the CD-ROM manually. Follow the
instructions in <xref linkend="boot-newworld"/> for booting from
the hard disk, except use the path to <command>yaboot</command> on the
CD at the OF prompt, such as

<informalexample><screen>

0 > boot cd:,\install\yaboot

</screen></informalexample>
</para>
  </sect2>

  <sect2 arch="powerpc" id="install-drive">
  <title>Booting from Hard Disk</title>

&boot-installer-intro-hd.xml;

  <sect3><title>Booting CHRP from OpenFirmware</title>

<para>

  <emphasis>Not yet written.</emphasis>

</para>
   </sect3>

   <sect3><title>Booting OldWorld PowerMacs from MacOS</title>
<para>

If you set up BootX in <xref linkend="files-oldworld"/>, you can
use it to boot into the installation system. Double click the
<guiicon>BootX</guiicon> application icon. Click on the
<guibutton>Options</guibutton> button and select <guilabel>Use
Specified RAM Disk</guilabel>. This will give you the 
chance to select the <filename>ramdisk.image.gz</filename> file. You
may need to select the <guilabel>No Video Driver</guilabel> checkbox,
depending on your hardware. Then click the
<guibutton>Linux</guibutton> button to shut down MacOS and launch the
installer.

</para>
   </sect3>


  <sect3 id="boot-newworld">
  <title>Booting NewWorld Macs from OpenFirmware</title>
<para>

You will have already placed the <filename>linux.bin, yaboot,
yaboot.conf</filename>, and <filename>root.bin</filename> files at the
root level of your HFS partition in <xref linkend="files-newworld"/>. 
Restart the computer, and immediately (during the chime) hold down the
<keycap>Option</keycap>, <keycap>Command (cloverleaf/Apple)</keycap>,
<keycap>o</keycap>, and <keycap>f</keycap> keys all together. After 
a few seconds you will be presented with the Open Firmware prompt.
At the prompt, type

<informalexample><screen>

0 > boot hd:<replaceable>x</replaceable>,yaboot

</screen></informalexample>

replacing <replaceable>x</replaceable> with the partition number of
the HFS partition where the
kernel and yaboot files were placed, followed by a &enterkey;. On some
machines, you may need to use <userinput>ide0:</userinput> instead of
<userinput>hd:</userinput>. In a few more seconds you will see a
yaboot prompt

<informalexample><screen>

boot:

</screen></informalexample>

At yaboot's <prompt>boot:</prompt> prompt, type either
<userinput>install</userinput> or <userinput>install video=ofonly</userinput>
followed by a &enterkey;.  The
<userinput>video=ofonly</userinput> argument is for maximum
compatibility; you can try it if <userinput>install</userinput>
doesn't work. The Debian installation program should start.

</para>
   </sect3>
  </sect2>

  <sect2 arch="powerpc" id="boot-tftp"><title>Booting with TFTP</title>

&boot-installer-intro-net.xml;

<para>

Currently, PReP and New World PowerMac systems support netbooting. 

</para><para>

On machines with Open Firmware, such as NewWorld Power Macs, enter the
boot monitor (see <xref linkend="invoking-openfirmware"/>) and
use the command <command>boot enet:0</command>.  PReP and CHRP boxes
may have different ways of addressing the network.  On a PReP machine,
you should try 
<userinput>boot <replaceable>server_ipaddr</replaceable>,<replaceable>file</replaceable>,<replaceable>client_ipaddr</replaceable></userinput>.

</para>
  </sect2>


  <sect2 arch="powerpc">
  <title>Booting from Floppies</title>
<para>

Booting from floppies is supported for &arch-title;, although it is
generally only applicable for OldWorld systems. NewWorld systems are
not equipped with floppy drives, and attached USB floppy drives are 
not supported for booting.

</para><para>

You will have already downloaded the floppy images you needed and
created floppies from the images in <xref linkend="create-floppy"/>.

</para><para>

To boot from the <filename>boot-floppy-hfs.img</filename> floppy,
place it in floppy drive after shutting the system down, and before
pressing the power-on button.

</para><note><para>
For those not familiar with Macintosh
floppy operations: a floppy placed in the machine prior to boot will
be the first priority for the system to boot from. A floppy without a
valid boot system will be ejected, and the machine will then check for
bootable hard disk partitions.

</para></note><para>

After booting, the <filename>root.bin</filename> floppy is
requested. Insert the root floppy and press &enterkey;. The installer
program is automatically launched after the root system has been
loaded into memory.

</para>
  </sect2>


  <sect2 arch="powerpc"><title>PowerPC Boot Parameters</title>
<para>

Many older Apple monitors used a 640x480 67Hz mode. If your video
appears skewed on an older Apple monitor, try appending the boot
argument <userinput>video=atyfb:vmode:6</userinput> , which will
select that mode for most Mach64 and Rage video hardware. For Rage 128
hardware, this changes to
<userinput>video=aty128fb:vmode:6</userinput> .

</para>
  </sect2>
