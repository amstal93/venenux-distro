<!-- retain these comments for translator revision tracking -->
<!-- original version: 19287 untranslated -->

 <sect1 id="minimum-hardware-reqts">
<title>Meeting Minimum Hardware Requirements</title>
<para>

Once you have gathered information about your computer's hardware,
check that your hardware will let you do the type of installation
that you want to do.

</para><para arch="not-s390">

Depending on your needs, you might manage with less than some of the
recommended hardware listed in the table below. However, most users
risk being frustrated if they ignore these suggestions. 

</para><para arch="x86">

A Pentium 100 is the minimum recommended for desktop 
systems, and a Pentium II-300 for a Server.

</para><para arch="m68k">

A 68030 or better processor is recommended for m68k
installs. You may get by with a little less drive space than shown.

</para><para arch="powerpc">

Any OldWorld or NewWorld PowerPC can serve well 
as a Desktop System. For servers, a minimum 132-Mhz machine is 
recommended.

</para>

<table>
<title>Recommended Minimum System Requirements</title>
<tgroup cols="3">
<thead>
<row>
  <entry>Install Type</entry><entry>RAM</entry><entry>Hard Drive</entry>
</row>
</thead>

<tbody>
<row>
  <entry>No desktop</entry>
  <entry>24 megabytes</entry>
  <entry>450 megabytes</entry>
</row><row>
  <entry>With Desktop</entry>
  <entry>64 megabytes</entry>
  <entry>1 gigabyte</entry>
</row><row>
  <entry>Server</entry>
  <entry>128 megabytes</entry>
  <entry>4 gigabytes</entry>
</row>

</tbody></tgroup></table>

<para>

Here is a sampling of some common Debian system configurations.
You can also get an idea of the disk space used by related groups
of programs by referring to <xref linkend="tasksel-size-list"/>.

</para>
<variablelist>

<varlistentry>
 <term>Standard Server</term>
 <listitem><para>

This is a small server profile, useful for a stripped down server
which does not have a lot of niceties for shell users.  It includes an
FTP server, a web server, DNS, NIS, and POP. For these 100MB of disk
space would suffice, and then you would need to add space
for any data you serve up.

</para></listitem>
</varlistentry>

<varlistentry arch="not-s390">
 <term>Desktop</term>
 <listitem><para>

A standard desktop box, including the X window system, full desktop
environments, sound, editors, etc.  You'll need about 2GB using the
standard desktop task, though it can be done in far less.

</para></listitem>
</varlistentry>

<varlistentry arch="not-s390">
 <term>Work Console</term>
 <listitem><para>

A more stripped-down user machine, without the X window system or X
applications.  Possibly suitable for a laptop or mobile computer.  The
size is around 140MB. 

</para></listitem>
</varlistentry>

<varlistentry>
 <term>Developer</term>
 <listitem><para>

A desktop setup with all the development packages, such as Perl, C,
C++, etc.  Size is around 475MB.  Assuming you are adding X11 and some
additional packages for other uses, you should plan around 800MB for
this type of machine.

</para></listitem>
</varlistentry>

</variablelist><para>

Remember that these sizes don't include all the other materials which
are usually to be found, such as user files, mail, and data.  It is
always best to be generous when considering the space for your own
files and data.  Notably, the <filename>/var</filename> partition contains
a lot of state information specific to Debian in addition to its regular
contents like logfiles.  The
<command>dpkg</command> files (with information on all installed
packages) can easily consume 20MB. Also,
<command>apt-get</command> puts downloaded packages here before they are
installed. You should
usually allocate at least 100MB for <filename>/var</filename>.

</para>

 </sect1>

