<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56450 -->
<!-- revisado por Steve Langasek -->

 <sect2 id="network-cards">
 <title>Hardware de conectividad de red</title>

<para>

Casi cualquier tarjeta de interfaz de red (tambi�n llamadas �network
interface card� o NIC, n. del t.)  que el n�cleo de Linux soporte
estar� tambi�n soportada en el sistema de instalaci�n. Los
controladores modulares se cargar�n autom�ticamente por regla general.

<phrase arch="x86">Esto incluye la mayor�a de las tarjetas PCI y PCMCIA.</phrase>
<phrase arch="i386">Tambi�n se soporta muchas tarjetas ISA antiguas.</phrase>

<phrase arch="m68k">De nuevo, consulte <ulink url="&url-m68k-faq;"></ulink>
para m�s detalles.</phrase>

</para><para arch="sparc">

Esta lista incluye muchas de las tarjetas PCI gen�ricas (en aquellos
sistemas que tienen PCI) y las siguientes NICs de Sun:

<itemizedlist>
<listitem><para>

Sun LANCE

</para></listitem>
<listitem><para>

Sun Happy Meal

</para></listitem>
<listitem><para>

Sun BigMAC

</para></listitem>
<listitem><para>

Sun QuadEthernet

</para></listitem>
<listitem><para>

MyriCOM Gigabit Ethernet

</para></listitem>
</itemizedlist>

</para><para arch="s390">

La lista de dispositivos de red soportados es:

<itemizedlist>
 <listitem><para>

Conexi�n canal a canal (CTC) y ESCON (real o emulado)

</para></listitem>
 <listitem><para>

OSA-2 Token Ring/Ethernet y OSA-Express Fast Ethernet (non-QDIO)

</para></listitem>
<listitem><para>

OSA-Express en modo QDIO, HiperSockets y Guest-LANs

</para></listitem>
</itemizedlist>

</para><para arch="x86">

Se soporta RDSI, pero no durante la instalaci�n.

</para>

  <sect3 condition="supports-wireless" id="nics-wireless">
   <title>Tarjetas de red inal�mbrica</title>
<para>

En general tambi�n se soportan las tarjetas de red inal�mbricas. El n�cleo
oficial de Linux soporta un n�mero creciente de adaptadores inal�mbricos,
aunque muchos necesitan que se cargue primero su �firmware�. Aunque puede hacer
funcionar en &debian; muchos adaptadores de red inal�mbricos que no se soportan
en el n�cleo oficial de Linux, no est�n soportados durante el proceso de
instalaci�n.
 
  </para><para>
   
A�n se est� desarrollando en el programa de instalaci�n la posibilidad de
utilizar una red inal�mbrica durante el proceso. El hecho de que funcione (o
no) depender� de su tipo de adaptador y de la configuraci�n de su punto de
acceso.
En cualquier caso, puede instalar &debian; utilizando una imagen completa de CD-ROM o
DVD si no puede utilizar ninguna otra tarjeta de red. Elija la opci�n
que no configure la red e instale el sistema s�lo con los paquetes
disponibles en el CD/DVD. Una vez haya hecho esto podr� instalar el
controlador y el �firmware� que necesite despu�s de terminar la
instalaci�n (despu�s del reinicio del sistema) y configurar la red
manualmente.

</para><para>

En algunos casos el controlador que necesita puede no estar disponible
como paquete Debian. Deber� comprobar si existe c�digo fuente
disponible en Internet y compilar el controlador vd. mismo. C�mo hacer
esto est� fuera del �mbito de este manual.
<phrase arch="x86">Si no hay ning�n controlador de Linux disponible
puede utilizar como �ltimo recurso el paquete
<classname>ndiswrapper</classname> que le permite utilizar un
controlador de Windows.</phrase>

</para>
  </sect3>

  <sect3 arch="sparc" id="nics-sparc-trouble">
  <title>Problemas conocidos para &arch-title;</title>
<para>

Existen algunos problemas conocidos con tarjetas de red espec�ficas
que merece la pena tratar aqu�.

</para>

   <sect4><title>Conflicto entre los controladores tulip y dfme</title>
<!-- BTS: #334104; may also affect other arches, but most common on sparc -->
<para>

<!-- BTS: #334104; may also affect other arches, but most common on
sparc --> Existen algunas tarjetas PCI que tienen la misma
identificaci�n PCI pero est�n soportados por controladores
relacionados, pero distintos. Algunas tarjetas funcionan con el
controlador <literal>tulip</literal> y otras con el controlador
<literal>dfme</literal>. El n�cleo no puede distinguir entre ellas ya
que tienen la misma identificaci�n, con lo que no est� seguro de qu�
controlador se debe cargar. Si se instala el controlador incorrecto
puede que no funcione la tarjeta de red o que funcione mal.

</para><para>

Este es un problema habitual en sistemas Netra con tarjetas Davicom
(compatible DEC-Tulip). En estos equipos el controlador
<literal>tulip</literal> es seguramente el que deba utilizar.  Puede prevenir
este problema poniendo el m�dulo del controlador incorrecto 
en una lista negra, como se describe en <xref linkend="module-blacklist"/>.
 
</para><para>

Una soluci�n alternativa durante la instalaci�n es cambiar a un int�rprete de
�rdenes, quitar el m�dulo del controlador incorrecto utilizando
<userinput>modprobe -r <replaceable>modulo</replaceable></userinput>
(o quitar ambos, si est�n los dos). Y despu�s cargar el m�dulo
correcto con <userinput>modprobe
<replaceable>modulo</replaceable></userinput>.

</para>
   </sect4>

   <sect4><title>Sun B100 blade</title>
<!-- BTS: #384549; should be checked for kernels >2.6.18 -->
<para>

El controlador de red <literal>cassini</literal> no funciona en
sistemas blade Sun B100.

</para>
  </sect4>
 </sect3>
</sect2>
