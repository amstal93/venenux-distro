# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of nb.po to Norwegian Bokmål
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Knut Yrvin <knuty@skolelinux.no>, 2004.
# Klaus Ade Johnstad <klaus@skolelinux.no>, 2004.
# Axel Bojer <axelb@skolelinux.no>, 2004.
# Bjørn Steensrud <bjornst@powertech.no>, 2004, 2005, 2006, 2007.
# Hans Fredrik Nordhaug <hans@nordhaug.priv.no>, 2005, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: nb\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-08-06 20:14+0000\n"
"PO-Revision-Date: 2008-08-06 13:06+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../mdcfg-utils.templates:1001
msgid "Configure MD devices"
msgstr "Sett opp MD-enheter"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid "Multidisk (MD) not available"
msgstr "Multidisk (MD) ikke tilgjengelig"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid ""
"The current kernel doesn't seem to support multidisk devices. This should be "
"solved by loading the needed modules."
msgstr ""
"Nåværende kjerne ser ikke ut til å støtte multidisk-enheter. Dette kan løses "
"ved å laste modulene som mangler."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Create MD device"
msgstr "Lag MD-enhet"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Delete MD device"
msgstr "Slett MD-enhet "

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Finish"
msgstr "Ferdig"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "Multidisk configuration actions"
msgstr "Oppsettshandlinger for multidisk"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "This is the Multidisk (MD) and software RAID configuration menu."
msgstr "Dette er oppsettsmenyen for multidisk (MD) og programvarestyrt RAID."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid ""
"Please select one of the proposed actions to configure multidisk devices."
msgstr "Velg en av de følgende handlingene for å sette opp multidisk-enheter."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid "No RAID partitions available"
msgstr "Ingen RAID-partisjoner tilgjengelig"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"No unused partitions of the type \"Linux RAID Autodetect\" are available. "
"Please create such a partition, or delete an already used multidisk device "
"to free its partitions."
msgstr ""
"Ingen ubrukte partisjoner av typen «Linux RAID Autodetect» er tilgjengelige. "
"Opprett en slik partisjon, eller slett en allerede brukt multidisk-enhet for "
"å frigjøre partisjonene."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"If you have such partitions, they might contain actual file systems, and are "
"therefore not available for use by this configuration utility."
msgstr ""
"Hvis du har slike partisjoner, så kan de inneholde virkelige filsystemer, og "
"derfor er de ikke tilgjengelige for dette oppsettsprogrammet."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid "Not enough RAID partitions available"
msgstr "Ikke tilstrekkelig mange RAID-partisjoner tilgjengelig"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid ""
"There are not enough RAID partitions available for your selected "
"configuration.  You have ${NUM_PART} RAID partitions available but your "
"configuration requires ${REQUIRED} partitions."
msgstr ""
"Det er ikke nok RAID-partisjoner tilgjengelig for oppsettet du har valgt. En "
"må ha ${NUM_PART} RAID-partisjoner tilgjengelig. Oppsettet nå krever "
"${REQUIRED} partisjoner."

#. Type: select
#. Choices
#. :sl3:
#. Type: select
#. Choices
#. :sl3:
#: ../mdcfg-utils.templates:6001 ../mdcfg-utils.templates:13001
msgid "Cancel"
msgstr "Avbryt"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Multidisk device type:"
msgstr "Enhetstype for multidisk:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Please choose the type of the multidisk device to be created."
msgstr "Velg hva slags type multidisk som skal opprettes."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid "Active devices for the RAID0 multidisk device:"
msgstr "Aktive enheter for RAID1 multidiskenheter:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid ""
"You have chosen to create a RAID0 array. Please choose the active devices in "
"this array."
msgstr ""
"Du har valgt å opprette en RAID0-rekke. Velg den aktive enheten i denne "
"rekka."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid "Number of active devices for the RAID${LEVEL} array:"
msgstr "Antall aktive enheter for RAID${LEVEL}-rekka:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid ""
"The RAID${LEVEL} array will consist of both active and spare partitions. The "
"active partitions are those used, while the spare devices will only be used "
"if one or more of the active devices fail. A minimum of ${MINIMUM} active "
"devices is required."
msgstr ""
"RAID${LEVEL}-rekka vil bestå av både aktive og reservepartisjoner. De aktive "
"partisjonene er de som er i bruk, mens reserveenhetene bare tas i bruk hvis "
"en eller flere av de aktive enhetene svikter. Det kreves minst ${MINIMUM} "
"aktive enheter."

#. Type: string
#. Description
#. :sl3:
#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001 ../mdcfg-utils.templates:12001
msgid "NOTE: this setting cannot be changed later."
msgstr "MERK: Det er ikke mulig å endre denne innstillingen senere."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid "Active devices for the RAID${LEVEL} multidisk device:"
msgstr "Aktive enheter for RAID${LEVEL} multidiskenheter:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} active devices."
msgstr ""
"Du har valgt å opprette en RAID${LEVEL}-rekke med ${COUNT} aktive enheter."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"Please choose which partitions are active devices. You must select exactly "
"${COUNT} partitions."
msgstr ""
"Velg hvilke partisjoner som er aktive enheter. Velg eksakt ${COUNT} "
"partisjoner."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:10001
msgid "Number of spare devices for the RAID${LEVEL} array:"
msgstr "Antall reserveenheter for RAID${LEVEL}-rekka:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid "Spare devices for the RAID${LEVEL} multidisk device:"
msgstr "Reserveenheter for RAID${LEVEL} multidiskenheten:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} spare devices."
msgstr ""
"Du har valgt å opprette en RAID${LEVEL}-rekke med ${COUNT} reserveenheter."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"Please choose which partitions will be used as spare devices. You may choose "
"up to ${COUNT} partitions. If you choose less than ${COUNT} devices, the "
"remaining partitions will be added to the array as \"missing\". You will be "
"able to add them later to the array."
msgstr ""
"Velg hvilke partisjoner som skal være reserveenheter. Opptil ${COUNT} "
"reserveenheter kan velges. Velges færre enn ${COUNT} enheter, vil resten bli "
"lagt inn i rekka som «mangler». Disse kan legges til senere."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid "Layout of the RAID10 multidisk device:"
msgstr "Utformingen av RAID10 multidiskenheten:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The layout must be n, o, or f (arrangement of the copies) followed by a "
"number (number of copies of each chunk). The number must be smaller or equal "
"to the number of active devices."
msgstr ""
"Utformingen må være n, o eller f (plassering av kopiene) fulgt av et nummer"
"(antall kopier av hver bit). Nummeret må være mindre eller lik antallet "
"aktive enheter."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The letter is the arrangement of the copies:\n"
" n - near copies: Multiple copies of one data block are at similar\n"
"     offsets in different devices.\n"
" f - far copies: Multiple copies have very different offsets\n"
" o - offset copies: Rather than the chunks being duplicated within a\n"
"     stripe, whole stripes are duplicated but are rotated by one\n"
"     device so duplicate blocks are on different devices."
msgstr ""
"Bokstaven angir plasseringen av kopiene:\n"
" n - nære kopier: Flere kopier av en datablokk har samme\n"
"     forskyvning i forskjellige enheter.\n"
" f - fjerne kopier: Flere kopier har veldig forskjellig \n"
"     forskyvning\n"
" o - forskjøvne (offset) kopier: Istedenfor at bitene er \n"
"     duplisert på en stripe, er hele striper duplisert men \n"
"     rotert med en enhet slik at duplikat blokker er på \n"
"     forskjellige enheter."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid "Multidisk device to be deleted:"
msgstr "Multidisk-enhet som skal slettes:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Deleting a multidisk device will stop it and clear the superblock of all its "
"components."
msgstr ""
"Å slette en multidiskenhet vil stoppe den, og slette alle delene fra "
"superblokka."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Please note this will not immediately allow you to reuse the partitions or "
"devices in a new multidisk device. The array will however be unusable after "
"the deletion."
msgstr ""
"Merk at dette ikke vil tillate umiddelbar gjenbruk av partisjoner eller "
"enheter i en ny multidiskenhet. Enhetsrekka vil være ubrukelig etter "
"sletting."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"If you select a device for deletion, you will get some information about it "
"and you will be given the option of aborting this operation."
msgstr ""
"Velges en enhet for sletting, vil du få informasjon om denne, og sletting "
"kan avbrytes etter eget ønske."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices available"
msgstr "Ingen multidiskenhet er tilgjengelig"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices are available for deletion."
msgstr "Ingen multidiskenhet er tilgjengelig for sletting."

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid "Really delete this multidisk device?"
msgstr "Skal denne multidiskenheten virkelig slettes?"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
"Please confirm whether you really want to delete the following multidisk "
"device:"
msgstr "Bekreft at du virkelig vil slette denne multidiskenheten:"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
" Device:            ${DEVICE}\n"
" Type:              ${TYPE}\n"
" Component devices:"
msgstr ""
" Enhet:            ${DEVICE}\n"
" Type:              ${TYPE}\n"
" Komponentenheter:"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "Failed to delete the multidisk device"
msgstr "Klarte ikke slette multidiskenheten"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "There was an error deleting the multidisk device. It may be in use."
msgstr ""
"Det oppsto en feil ved sletting av multidisk-enheten. Den kan være i bruk."
