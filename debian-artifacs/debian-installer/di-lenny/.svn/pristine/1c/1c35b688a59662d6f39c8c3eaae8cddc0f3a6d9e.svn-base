<!-- retain these comments for translator revision tracking -->
<!-- original version: 39920 -->


  <sect2 arch="alpha"><title>Partitionierung auf &arch-title;-Systemen</title>
<para>

Zum Booten von Debian von der SRM-Konsole (das ist die einzige Möglichkeit,
von der Festplatte zu starten, die von &releasename-cap; unterstützt wird)
benötigen Sie ein BSD-Disklabel anstatt einer DOS-Partitionstabelle auf
Ihrer Boot-Festplatte (zur Erinnerung: Der SRM-Bootblock ist mit
MS-DOS-Partitionstabellen nicht kompatibel &ndash; siehe <xref
linkend="alpha-firmware"/>.) Folglich erstellt <command>partman</command>
BSD-Disklabels, wenn es unter &architecture; läuft. Wenn aber Ihre
Festplatte eine DOS-Partitionstabelle enthält, muss diese gelöscht werden, bevor
partman ein BSD-Disklabel schreiben kann.

</para><para>

Wenn Sie <command>fdisk</command> zur Partitionierung Ihrer Festplatte
benutzen und die ausgewählte Festplatte noch kein BSD-Disklabel enthält,
müssen Sie das Befehlskürzel <quote>b</quote> benutzen, um den
Disklabel-Modus einzuschalten.

</para><para>

Es wird <emphasis>nicht</emphasis> empfohlen, die dritte Partition als
Komplett-Festplatten-Partition einzurichten (so dass sie die <emphasis>ganze</emphasis>
Festplatte enthält; Start- und Endsektoren der Festplatte sind auch Start-
und Endsektoren der Partition).
Einzige Ausnahme: Sie haben vor, die Festplatte, die Sie partitionieren, mit
Tru64 Unix oder einem der drei von 4.4BSD-Lite abgeleiteten Betriebssysteme
(FreeBSD, OpenBSD oder NetBSD) zu benutzen.
Durch diese Art der Partitionierung würde die Festplatte inkompatibel zu den
Werkzeugen, die die Boot-Fähigkeit mit aboot herstellen. Dies bedeutet, dass
die Platte, die vom Installer als Debian-Boot-Festplatte konfiguriert wird,
von den vorher genannten Betriebssystemen nicht erreichbar wäre.

</para><para>

Weil <command>aboot</command> in die ersten paar Sektoren der Festplatte
geschrieben wird (zurzeit benötigt es ungefähr 70 Kilobytes bzw. 150
Sektoren), <emphasis>müssen</emphasis> Sie außerdem dafür genug freien
Platz am Anfang der Festplatte freilassen. Früher wurde vorgeschlagen, dass Sie
eine kleine unformatierte Partition am Anfang der Festplatte erstellen. Aus
dem oben erwähnten Grund schlagen wir jetzt vor, dass Sie das nicht auf
Festplatten machen, die nur unter GNU/Linux benutzt werden. Bei der
Verwendung von <command>partman</command> wird der Bequemlichkeit halber
immer noch eine kleine Partition für <command>aboot</command> erzeugt.

</para><para condition="FIXME">

Bei ARC-Installationen sollten Sie eine kleine FAT-Partition am Anfang der
Festplatte für <command>MILO</command> und <command>linload.exe</command>
erstellen &ndash; 5 Megabytes sollten ausreichen, siehe <xref
linkend="non-debian-partitioning"/>. Leider wird das Erstellen von
FAT-Dateisystemen vom Menü noch nicht unterstützt, deswegen müssen Sie das
per Hand auf der Kommandozeile mit <command>mkdosfs</command> machen,
bevor Sie die Installation des Bootloaders in Angriff nehmen.

</para>
  </sect2>
