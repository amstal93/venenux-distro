iso-scan (1.27) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bosnian (bs.po) by Armin Besirovic
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * French (fr.po) by Christian Perrier
  * Hebrew (he.po) by Omer Zak
  * Croatian (hr.po) by Josip Rodin
  * Italian (it.po) by Milo Casagrande
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Peteris Krisjanis
  * Macedonian (mk.po) by Arangel Angov
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Serbian (sr.po) by Veselin Mijušković
  * Turkish (tr.po) by Mert Dirik
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 19:06:56 -0300

iso-scan (1.26) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Marathi (mr.po) by Sampada
  * Punjabi (Gurmukhi) (pa.po) by Amanpreet Singh Alam

 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 00:16:39 -0300

iso-scan (1.25) unstable; urgency=low

  * Scan partitions before scanning the whole disk to avoid blocking partition
    changes in partman. Thanks to Sam Powers for tracking the issue down and
    providing the patch. Closes: #472595.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Marathi (mr.po)
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Frans Pop <fjp@debian.org>  Sun, 20 Apr 2008 20:16:44 +0200

iso-scan (1.24) unstable; urgency=low

  [ Updated translations ]
  * Finnish (fi.po) by Esko Arajärvi
  * Hindi (hi.po) by Kumar Appaiah
  * Indonesian (id.po) by Arief S Fitrianto
  * Turkish (tr.po) by Recai Oktaş
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Otavio Salvador <otavio@debian.org>  Fri, 15 Feb 2008 08:16:39 -0200

iso-scan (1.23) unstable; urgency=low

  * postinst: add quotes when listing devices the second time to make the
    comparison with the initial list work.
  * Look for Release file in whatever dists/* directories are on the CD,
    rather than relying on the stable/unstable/etc symlinks. Prefer the name
    listed in /etc/default-release. (See also cdrom-detect 1.25.)
  * If the base-system is installable from CD, queue <codename>-support udeb
    for installation (choose-mirror will do the same for businesscard).

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Belarusian (be.po) by Hleb Rubanau
  * Bengali (bn.po) by Jamil Ahmed
  * Danish (da.po) by Claus Hindsgaul
  * French (fr.po) by Christian Perrier
  * Italian (it.po) by Stefano Canepa
  * Korean (ko.po) by Changwoo Ryu
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Viesturs Zarins
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Ivan Masár
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Wed, 06 Feb 2008 17:30:05 +0100

iso-scan (1.22) unstable; urgency=low

  * Fix broken regexp in architecture determination code.
    (Fixes problem paulcager reported.)

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc
  * Romanian (ro.po) by Eddy Petrișor

 -- Joey Hess <joeyh@debian.org>  Thu, 24 May 2007 21:29:45 -0400

iso-scan (1.21) unstable; urgency=low

  [ Joey Hess ]
  * Ensure that the Release file of an iso says that it supports
    the installation architecture. If not, skip that iso. This will allow
    putting multiple isos on a usb stick for different architectures.
    Closes: #419141
  * Use maybe-usb-floppy from di-utils 1.48 in the hack that deals with usb
    sticks that show up as floppies, as this is probably better than scanning
    all floppies.

 -- Frans Pop <fjp@debian.org>  Thu, 26 Apr 2007 08:10:15 +0200

iso-scan (1.20) unstable; urgency=low

  * Multiply menu-item-numbers by 100

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Apr 2007 14:36:16 -0400

iso-scan (1.19) unstable; urgency=low

  [ Updated translations ]
  * Hebrew (he.po) by Lior Kaplan
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 16:52:38 +0100

iso-scan (1.18) unstable; urgency=low

  [ Frans Pop ]
  * Suppress Lintian warning about missing standards version.
  * Fix template for internal use without description.
  * Add missing debconf dependencies.
  * Add myself to uploaders.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Panjabi (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:38:36 +0100

iso-scan (1.17) unstable; urgency=low

  * Sometimes udev mis-detects usb sticks as floppies. For example, this
    has been reported to happen with a corsair 2GB stick. This didn't matter,
    but now that list-devices is used, it does. Add a hack to use list-devices
    floppy and check those devices last.
  * Fix code that re-scans for usb devices after a failure; this was still
    using the (removed) block_devices function!

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Norwegian Bokmål (nb.po) by Bjørn Steensrud
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Tamil (ta.po) by Damodharan Rajalingam
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Oct 2006 18:00:24 -0400

iso-scan (1.16) unstable; urgency=low

  * Put debhelper in Build-Depends rather than in Build-Depends-Indep.
  * Use list-devices to look for block devices. Requires di-utils 1.34.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Gujarati (gu.po) by Kartik Mistry
  * Panjabi (pa.po) by A S Alam
  * Northern Sami (se.po) by Børre Gaup
  * Swedish (sv.po) by Daniel Nylander
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Colin Watson <cjwatson@debian.org>  Thu, 31 Aug 2006 17:32:37 +0100

iso-scan (1.15) unstable; urgency=low

  * Deal with the case where the machine is so fast that iso-scan
    scans for new devices before the USB disks become ready. Add a possible
    second pass in the quick scan that sleepds, looks for new devices and,
    if any new devices are found, rescans them all. Closes: #335093
    (Note that rather than complex code to keep track of what devices are new,
    I just rescan them all because this is supposed to happen only on very
    fast machines anyway. KISS)
  * Load hfsplus before hfs, since hfsplus filesystems show as an empty hfs
    filesystem, which hides any iso on them. Loading hdsplus first should make
    mount try it first. Closes: #337316

 -- Joey Hess <joeyh@debian.org>  Thu, 15 Jun 2006 18:52:52 -0400

iso-scan (1.14) unstable; urgency=low

  * Change load-iso menu item number from 13 to 16. cdrom-detect, load-iso,
    load-cdrom, ethdetect, and s390-netdevice are all changed due to this and
    should transition together.
    Closes: #373097

 -- Joey Hess <joeyh@debian.org>  Tue, 13 Jun 2006 20:30:33 -0400

iso-scan (1.13) unstable; urgency=low

  * Fix name in log function.
  * Remove unncecessary logging.
  * Queue apt-cdrom-setup for install if the iso can install base, so that
    the iso will be used after apt-setup.

 -- Joey Hess <joeyh@debian.org>  Fri, 12 May 2006 17:32:16 -0500

iso-scan (1.12) unstable; urgency=low

  [ Joey Hess ]
  * Rename debconf templates to cdrom/suite and cdrom/codename. These do not
    need to be translatable, they are internal use.
    Needs apt-setup 1:0.10, base-installer 1.56.

  [ Frans Pop ]
  * Schedule installation of choose-mirror for images that do not contain the
    base system packages. Needed to support installation without using network
    mirrors again.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Sonam Rinchen
  * Greek, Modern (1453-) (el.po) by quad-nrg.net
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Irish (ga.po) by Kevin Patrick Scannell
  * Galician (gl.po) by Jacobo Tarrio
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÑC Attila
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Leang Chumsoben
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Punjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Wed, 19 Apr 2006 19:38:05 +0200

iso-scan (1.11) unstable; urgency=low

  [ Updated translations ]
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Italian (it.po) by Giuseppe Sacco
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Romanian (ro.po) by Eddy Petrişor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Turkish (tr.po) by Recai Oktaş
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Mon, 23 Jan 2006 20:23:17 +0100

iso-scan (1.10) unstable; urgency=low

  * Also determine the codename of the release. This can be used later to
    set apt sources by codename.

  [ Updated translations ]
  * Bengali (bn.po) by Baishampayan Ghose
  * Hindi (hi.po) by Nishant Sharma
  * Icelandic (is.po) by David Steinn Geirsson
  * Norwegian Nynorsk (nn.po)
  * Romanian (ro.po) by Eddy Petrişor
  * Swedish (sv.po) by Daniel Nylander

 -- Frans Pop <fjp@debian.org>  Tue, 15 Nov 2005 21:03:46 +0100

iso-scan (1.09) unstable; urgency=low

  * Queue apt-mirror-setup for install, this is part of the apt-setup
    replacement that I've not quite gotten around to finishing yet. In the
    meantime, queuing it should be harmless.

  * Updated translations: 
    - German (de.po) by Holger Wansing
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Kurdish (ku.po) by Erdal Ronahi
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Sep 2005 17:00:42 +0200

iso-scan (1.08) unstable; urgency=low

  * Exit 1 on failure.
  * Deal with a race with usb-storage, which can take a while to "settle"
    and work on some hardware. If a mount fails, sleep and retry it.
  
  * Updated translations: 
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Joey Hess <joeyh@debian.org>  Fri, 15 Jul 2005 16:54:09 +0300

iso-scan (1.07) unstable; urgency=low

  [ Joey Hess ]
  * Don't bother with messing with mirror/suite's seen flag; this was
    instead fixed in base-config.
  
  * Updated translations: 
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Basque (eu.po) by Piarres Beobide
    - Portuguese (pt.po) by Miguel Figueiredo
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Mon, 13 Jun 2005 12:31:19 -0400

iso-scan (1.06) unstable; urgency=low

  * Joey Hess
    - Don't just set mirror/suite; properly preseed it by marking it as seen.
      This will allow apt-setup to ask the question at high priority as it
      should be, w/o making the question be displayed during initial installs.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernández-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Giuseppe Sacco
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Russian (ru.po) by Yuri Kozlov

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Jun 2005 13:45:33 -0400

iso-scan (1.05) unstable; urgency=low

  * Post-sarge; needs new anna.
  * Colin Watson
    - Try loading hfsplus module.
  * Joey Hess
    - Pass parameter to tell anna to use the iso as the default media.
    - Add a versioned dep on anna.
  * Matt Kraai
    - Fix the spelling of "file system".
  * Christian Perrier
    - Rename the templates file to help out translators working
      on a single file
  * Updated translations: 
    - Catalan (ca.po) by Guillem Jover
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Stefano Canepa
    - Portuguese (pt.po) by Miguel Figueiredo
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Yuri Kozlov
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Sun,  1 May 2005 11:04:24 -0400

iso-scan (1.04) unstable; urgency=low

  * Includes fixes for substitutions in translated templates.
  * Updated translations:
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Italian (it.po) by Cristian Rigamonti
    - Dutch (nl.po) by Bart Cornelis
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team

 -- Joey Hess <joeyh@debian.org>  Wed,  2 Feb 2005 18:27:48 -0500

iso-scan (1.03) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjorn Steensrud
    - Dutch (nl.po) by Bart Cornelis
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Oct 2004 14:28:38 -0400

iso-scan (1.02) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - French (fr.po) by French Team
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Bøkmal, Norwegian (nb.po) by Axel Bojer
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Russian (ru.po) by Russian L10N Team
    - Swedish (sv.po) by Per Olofsson

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Sep 2004 21:16:14 -0400

iso-scan (1.01) unstable; urgency=low

  * Joey Hess
    - Remove unnecessary seen flag unsetting.
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Latvian (lv.po) by Aigars Mahinovs
    - Bøkmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Russian (ru.po) by Yuri Kozlov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Matjaz Horvat
    - Turkish (tr.po) by Recai Oktaş
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Joey Hess <joeyh@debian.org>  Mon,  6 Sep 2004 20:34:01 -0400

iso-scan (1.00) unstable; urgency=low

  * Updated translations: 
    - Arabic (ar.po) by Abdulaziz Al-Arfaj
    - Danish (da.po) by Frederik Dannemare
    - German (de.po) by Dennis Stampfer
    - Persian (fa.po) by Arash Bijanzadeh
    - Croatian (hr.po) by Krunoslav Gernhard
    - Korean (ko.po) by Changwoo Ryu
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Joey Hess <joeyh@debian.org>  Sun, 25 Jul 2004 19:15:35 -0400

iso-scan (0.28) unstable; urgency=low

  * Updated translations: 
    - Polish (pl.po) by Bartosz Fenski
    - Russian (ru.po) by Yuri Kozlov

 -- Joey Hess <joeyh@debian.org>  Tue, 25 May 2004 12:25:15 -0300

iso-scan (0.27) unstable; urgency=low

  * Christian Perrier
    - Ellipsis typography fix
  * Joey Hess
    - Modprobe ntfs if it is available to scan such partitions for isos too.
  * Updated translations: 
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll

 -- Joey Hess <joeyh@debian.org>  Fri, 14 May 2004 10:24:43 -0300

iso-scan (0.26) unstable; urgency=low

  * Updated translations: 
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bokmal, Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll

 -- Joey Hess <joeyh@debian.org>  Fri, 23 Apr 2004 13:04:05 -0400

iso-scan (0.25) unstable; urgency=low

  * Updated translations: 
    - German (de.po) by Dennis Stampfer
    - Finnish (fi.po) by Tapio Lehtonen
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Setyo Nugroho
    - Italian (it.po) by Cristian Rigamonti
    - Bokmal, Norwegian (nb.po) by Axel Bojer
    - Norwegian Nynorsk (nn.po) by Håvard Korsvoll
    - Turkish (tr.po) by Osman Yüksel
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Tue, 20 Apr 2004 10:56:03 -0400

iso-scan (0.24) unstable; urgency=low

  * Updated translations: 
    - Basque (eu.po) by Piarres Beobide Egaña
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Setyo Nugroho
    - Dutch (nl.po) by Bart Cornelis

 -- Joey Hess <joeyh@debian.org>  Sat, 10 Apr 2004 00:43:17 -0400

iso-scan (0.23) unstable; urgency=low

  * Joey Hess
    - hw-detect no longer clobbers titles.
    - Pass a custom progress bar title to hw-detect.
  * Updated translations: 
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
    - Spanish (Castilian) (es.po) by Javier Fernández-Sanguino
    - French (fr.po) by Christian Perrier
    - Gallegan (gl.po) by Héctor Fernández López
    - Hungarian (hu.po) by VERÓK István
    - Indonesian (id.po) by Setyo Nugroho
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Nikolai Prokoschenko
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by André Dahlqvist
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by bbbush

 -- Joey Hess <joeyh@debian.org>  Sun,  4 Apr 2004 15:58:23 -0400

iso-scan (0.22) unstable; urgency=low

  * Christian Perrier : run debconf-updatepo
  * Updated translations: 
    - Bosnian (bs.po) by Safir Šećerović
    - Hungarian (hu.po) by VERÓK István
    - Polish (pl.po) by Bartosz Fenski
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Osman Yüksel
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Tue, 30 Mar 2004 14:49:51 -0500

iso-scan (0.21) unstable; urgency=low

  * Joey Hess
    - Build in binary-indep, not binary-arch. Closes: #223090
  * Translations
    - Håvard Korsvoll: Updated Norwegian, nynorsk translation (nn.po).

 -- Joey Hess <joeyh@debian.org>  Mon, 15 Mar 2004 23:35:57 -0500

iso-scan (0.20) unstable; urgency=low

  * Updated translations
    - Russian by Nikolai Prokoschenko
    - Norwegian Bokmål, by Steinar H. Gunderson.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 14 Mar 2004 19:39:28 +0100

iso-scan (0.19) unstable; urgency=low

  * Translations:
    - Dafydd Harries : Added Welsh translation (cy.po)
    - Kęstutis Biliūnas : Updated Lithuanian translation (lt.po)
    - Jordi Mallach: Updated Catalan translation (ca.po)
  * Christian Perrier
    -s/is may be corrupt/may be corrupt/ in templates. Unfuzzied translations
    
 -- Joey Hess <joeyh@debian.org>  Tue,  9 Mar 2004 13:04:30 +0100

iso-scan (0.18) unstable; urgency=low

  * Joey Hess
    - Commented out possibly broken Greek menu item (same as in load-iso),
      which caused infinite loop in Greek install.

  * Translations:
    - Giuseppe Sacco
      - Updated italian translation by Cristian Rigamonti (it.po)
    - Ognyan Kulev
      - Updated Bulgarian translation (bg.po).
    
 -- Joey Hess <joeyh@debian.org>  Fri,  5 Mar 2004 10:04:30 -0900

iso-scan (0.17) unstable; urgency=low

  * Translations 
    - Kęstutis Biliūnas: Updated Lithuanian translation (lt.po)
    - Ming Hua: Initial and updated Traditional Chinese translation
                (zh_TW.po), by Tetralet
    - Jure Cuhalev : Updated Slovenian translation (sl.po)
    - Håvard Korsvoll
      - Updated Norwegian, bokmål translation, (nb.po). From Axel Bojer

 -- Joey Hess <joeyh@debian.org>  Tue,  2 Mar 2004 13:34:06 -0500

iso-scan (0.16) unstable; urgency=low

  * Translations 
    - Bartosz Fenski
      - Updated Polish translation (pl.po)
    - Javier Fernandez-Sanguino : Updated Spanish translation (es.po)
    - Changwoo Ryu              : Added Korean translation (ko.po)
    - Håvard Korsvoll
      - Updated Norwegian, nynorsk (nn.po) translation.

 -- Joey Hess <joeyh@debian.org>  Sun, 22 Feb 2004 02:08:42 +0100

iso-scan (0.15) unstable; urgency=low

  * Translations
    - Andre Dahlqvist     : Update Swedish translation (sv.po)
    - Eugeniy Meshcheryakov : Updated Ukrainian translation (uk.po)
    - Carlos Z.F. Liu     : fix a typo in Simplified Chinese translation
  * Stephen R. Marenka
    - Added iso-scan/filename to support kernel 2.2.x.

 -- Joey Hess <joeyh@debian.org>  Fri, 20 Feb 2004 17:16:39 -0500

iso-scan (0.14) unstable; urgency=low

  * Translations
    - Kenshi Muto         : updated Japanese translation (ja.po)
    - Peter Mann          : updated Slovak translation (sk.po)
    - Jordi Mallach       : updated Catalan translation (ca.po).
    - Christian Perrier   : updated French translation (fr.po).
    - Bart Cornelis       : - updated Dutch translation (nl.po)
                            - added corrections send by Bastiaan Eeckhoudt
    - Kęstutis Biliūnas   : updated Lithuanian translation (lt.po).
    - Miroslav Kure       : updated Czech translation (cs.po)
    - Claus Hindsgaul     : Update Danish translation (da.po)
    - K. Margaritis       : Updated Greek translation (el.po)
    - Miguel Figueiredo   : Update Portuguese translation (pt.po)
    - André Luís Lopes  : Updated Brazilian Portuguese translation (pt_BR.po)
    - Dennis Stampfer     : Update German translation (de.po)
    - Carlos Z.F. Liu     : Updated Simplified Chinese translation (zh_CN.po)
  * Eugen Meshcheryakov : added Ukrainian translation (uk.po)
  * Stephen R. Marenka
    - Added hfs and support for kernel 2.2.x device names via
      /proc/partitions.
  * Joey Hess
    - Use debhelper's new udeb support.

 -- Joey Hess <joeyh@debian.org>  Fri, 13 Feb 2004 13:37:25 -0500

iso-scan (0.13) unstable; urgency=low

  * Joey Hess
    - Rename mirror/distribution to the more accurate mirror/suite.
    - Improved on the CD suite detection code, now it should
      set mirror/suite properly for later base-config use.

  * Translations
    - Giuseppe Sacco
      - applied patch for normalizing menus and some italian translation (it.po)
    - h3lios
      - Added Albanian translation (sq.po)
    - Jordi Mallach
      - Update Catalan translation (ca.po).
    - Carlos Z.F. Liu
      - fix a fatal error in Simplified Chinese translation (zh_CN.po)

 -- Joey Hess <joeyh@debian.org>  Thu,  5 Feb 2004 14:38:47 -0500

iso-scan (0.12) unstable; urgency=low

  * Bartosz Fenski
    - Updated Polish (pl) translation.
  * Joey Hess
    - Keep going if modprobe loop fails; maybe it was built in or already
      loaded.
  * Christian Perrier
    - Fixed mistake (not enough choices) and inconsistency in finnish
      translation.
  * Anmar Oueja
    - created and translated to Arabic (ar.po)
  * Nikolai Prokoschenko
    - updated russian translation (ru.po)
  * André Luís Lopes
    - Fixed inconsistencies among various pt_BR translations.
      Thanks to Christian Perrier for noticing these.
    - Updated Brazilian Portuguese (pt_BR) translation.
  * Joey Hess
    - Removed the problimatic number subtitutions in the templates, as
      there is no way to do proper ngettext as needed by some languages like
      Russian.
  * Christian Perrier
    - Updated french translation (fr.po)
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * Kęstutis Biliūnas
     - Updated Lithuanian translation (lt.po).
  * Bart Cornelis
     - Updated Dutch (nl.po) translation.
  * Claus Hindsgaul
    - Update Danish translation.
  * Miroslav Kure
    - Updated Czech translation
  * Dennis Stampfer
    - Update German translation de.po
  * Carlos Z.F. Liu
    - Update Chinese translation zh_CN.po
  * Andre Dahlqvist
    - Update Swedish translation (sv.po)
  * Peter Mann
    - Update Slovak translation
  * Safir Secerovic
    - Update Bosnian translation (bs.po).
  * Miguel Figueiredo
    - Updated Portuguese translation (pt.po)

 -- Christian Perrier <bubulle@debian.org>  Tue, 27 Jan 2004 07:46:14 +0100

iso-scan (0.11) unstable; urgency=low

  * Miguel Figueiredo
    - Updated Portuguese translation (pt.po)
  * Ming Hua
    - Initial Simplified Chinese translation (zh_CN.po)
  * Kęstutis Biliūnas
     - Updated Lithuanian translation (lt.po).
  * Bart Cornelis
    - Merged Norwegian Nynorsk (nn.po) translation from skolelinux-cvs
  * André Dahlqvist
    - Update Swedish translation (sv.po)
  * Teófilo Ruiz Suárez
    - Fixed some inconsistencies in Spanish Translation (es.po)
  * Ognyan Kulev
    - Updated bulgarian translation (bg.po).

 -- Joey Hess <joeyh@debian.org>  Thu,  1 Jan 2004 14:45:16 -0500

iso-scan (0.10) unstable; urgency=low

  * Bart Cornelis
    - incorporated debian-l10n-dutch review comments into nl.po
    - Merged Norwegian Bokmael (nb.po) from skolelinux-cvs
  * Dennis Stampfer
    - Merged some strings in German translation de.po
  * Jure Cuhalev
    - Added/updated slovenian translation (sl.po).
  * Teófilo Ruiz Suárez
    - Updated Spanish translation es.po
  * Christian Perrier
    - Consistency modification for brazilian
      (was obvious enough)
  * Marco Ferra
    - Added portuguese translation (pt.po)
  * Peter Mann
    - Updated Slovak translation
  * André Dahlqvist
    - Added Swedish translation
  * Claus Hindsgaul
    - Update Danish translation.

 -- Joey Hess <joeyh@debian.org>  Thu, 25 Dec 2003 19:56:21 -0500

iso-scan (0.09) unstable; urgency=low

  * Bartosz Fenski
    - Updated Polish (pl) translation.
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * André Luís Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * Christian Perrier
    - Update French translation.
  * Joey Hess
    - Make iso-scan retry if re-run, in case bad media was replaced.
  * Dennis Stampfer
    - Update German translation (de.po)
  * David Martínez Moreno
    - Started Spanish translation (es.po).
  * Claus Hindsgaul
    - Update Danish translation.
  * Joey Hess
    - Make the load-iso menu item translatable.
  * Kęstutis Biliūnas
    - Initial Lithuanian translation (lt.po)
  * Konstantinos Margaritis
    - Updated Greek translation (el.po)
  * Teófilo Ruiz Suárez
    - Updated Spanish translation (es.po)
    - Switched to UTF-8
  * Christian Perrier
    - Updated French translation (fr.po)
  * Kęstutis Biliūnas
    - Updated Lithuanian translation (lt.po)
  * Miroslav Kure
    - Updated Czech translation (cs.po)
  * Petter Reinholdtsen
    - Added Norwegian Bokmål translation (nb.po).
    - Added Norwegian Nynorsk (nn.po), thanks to Gaute Hvoslef Kvalnes.
  * Bart Cornelis
    - Updated Dutch translation (nl.po)
  * Ognyan Kulev
    - Added/updated bulgarian translation (bg.po).
  * Giuseppe Sacco
    - First italian translation by Cristian Rigamonti (it.po)
  * Teófilo Ruiz Suárez - Fixed es.po compilation error
  
 -- Joey Hess <joeyh@debian.org>  Mon, 22 Dec 2003 14:19:41 -0500

iso-scan (0.08) unstable; urgency=low

  * Verok Istvan
    - Initial Hungarian translation.
  * Joey Hess
    - Per discussion on debian-boot, use symbolic distribution names in
      mirror/distribution.
    - Remove release code name stuff.
    - Look for a testing distro on the iso before stable, as our current isos
      have a (bogus) stable symlink, which would end up with debootstrap being
      acked to bootstrap woody..

 -- Joey Hess <joeyh@debian.org>  Fri, 12 Dec 2003 16:37:52 -0500

iso-scan (0.07) unstable; urgency=low

  * Ilgiz Kalmetev
    - Initial Russian translation
  * Initial German translation de.po
  * Konstantinos Margaritis
    - Initial Greek translation (el.po)
  * Christian Perrier
    - Refining and standardizing templates. Closes: #219472
    - Update French translation.
  * Claus Hindsgaul
    - initial Danish translation (da.po)
  * Miroslav Kure
    - Update Czech translation.
  * Tommi Vainikainen
    - Update Finnish translation.
  * Jordi Mallach
    - Add Catalan translation (ca.po).
  * Kenshi Muto
    - Update Japanese translation (ja.po)
  * Safir Šećerović
    - Update Bosnian translation.
  * André Luís Lopes
    - Update pt_BR (Brazilian Portuguese) translation.

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Dec 2003 15:42:18 -0500

iso-scan (0.06) unstable; urgency=low

  * Peter Mann
    - Initial Slovak translation (sk.po).
  * Joey Hess
    - Remove the bogus modutils deps, add a dep on loop-modules.

 -- Denis Barbier <barbier@debian.org>  Sat,  8 Nov 2003 21:28:04 +0100

iso-scan (0.05) unstable; urgency=low

  * Tommi Vainikainen
    - Add Finnish (fi.po) translation.

 -- Joey Hess <joeyh@debian.org>  Fri,  7 Nov 2003 13:09:10 -0500

iso-scan (0.04) unstable; urgency=low

  * Safir Secerovic, Amila Akagic
    - Add Bosnian translation (bs.po).
  * Joey Hess
    - Menu-Item-Number drops to 13 so it is before network setup.
    - Add load-iso which provides a menu item to do just that, before
      the download-installer menu item.
    - Make iso-scan provide cdrom-detect.

 -- Joey Hess <joeyh@debian.org>  Sun,  2 Nov 2003 21:11:45 -0500

iso-scan (0.03) unstable; urgency=low

  * Christian Perrier
    - Add French translation.
  * André Luís Lopes
    - Updated pt_BR (Brazilian Portuguese) translation.
  * Miroslav Kure
    - Initial Czech translation (cs.po).
  * Kenshi Muto
    - Add Japanese translation (ja.po)
  * Bart Cornelis
    - initial dutch translation (nl.po)
  * Claus Hindsgaul
    - initial Danish translation (da.po)
  * Joey Hess
    - Dropped priority of success message to medium.

 -- Joey Hess <joeyh@debian.org>  Fri, 31 Oct 2003 10:21:37 -0500

iso-scan (0.02) unstable; urgency=low

  * Shut up modprobe.
  * Fix cdrom loopback mounting, yes it really works now.
  * Display a note on success.
  * Add yet another copy of mirror/distribution to the tree. :-(
  * Fix detection of already mounted cdrom.

 -- Joey Hess <joeyh@debian.org>  Mon, 13 Oct 2003 12:52:22 -0400

iso-scan (0.01) unstable; urgency=low
 
  * Initial release.
  * André Luís Lopes
    - Added pt_BR (Brazilian Portuguese) translation.

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Oct 2003 23:10:38 -0400
