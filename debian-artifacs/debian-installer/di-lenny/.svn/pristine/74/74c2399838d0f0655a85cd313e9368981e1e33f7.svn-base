palo-installer (0.0.12) unstable; urgency=low

  * Remove Tollef Fog Heen as Uploader with many thanks for his past
    contributions.

 -- Frans Pop <fjp@debian.org>  Wed, 24 Sep 2008 22:04:08 +0200

palo-installer (0.0.11) unstable; urgency=low

  * Replace 'base-installer' dependency by 'installed-base' virtual
    package. Needs base-installer 1.81.

 -- Otavio Salvador <otavio@ossystems.com.br>  Sun, 17 Jun 2007 09:57:05 -0300

palo-installer (0.0.10) unstable; urgency=low

  * Merge from Ubuntu (LaMont Jones):
    - Deal with the disk path being /dev/sd*, rather than /dev/scsi/...

 -- Colin Watson <cjwatson@debian.org>  Thu, 24 May 2007 09:24:11 +0100

palo-installer (0.0.9) unstable; urgency=low

  [ Joey Hess ]
  * Multiply menu-item-numbers by 100

 -- Frans Pop <fjp@debian.org>  Wed, 11 Apr 2007 00:12:17 +0200

palo-installer (0.0.8) unstable; urgency=medium

  [ Joey Hess ]
  * Remove unused debconf template. Closes: #283907
  * Remove useless extended package description. Closes: #308606
  * Simplify rules file.
  * Remove standards-version field (not applicable to a udeb).

  [ Colin Watson ]
  * Remove isinstallable file which checked for the presence of palo, so
    that we can remove palo from the base system and (as we do already)
    apt-install it later.

 -- Colin Watson <cjwatson@debian.org>  Thu, 20 Oct 2005 18:25:52 +0100

palo-installer (0.0.7) unstable; urgency=low

  * Colin Watson
    - Use busybox echo and sed instead of /target/bin/echo and
      /target/bin/sed; the former seem to be enough (closes: #283908).
    - palo-installer is Architecture: any, so build it in binary-arch, not
      binary-indep.
  * Updated translations:
    - Belarusian (be.po) by Andrei Darashenka
    - Bulgarian (bg.po) by Ognyan Kulev
    - Welsh (cy.po) by Dafydd Harries
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Esperanto (eo.po) by Serge Leblanc
    - Estonian (et.po) by Siim Põder
    - Basque (eu.po) by Piarres Beobide
    - Gallegan (gl.po) by Jacobo Tarrio
    - Hebrew (he.po) by Lior Kaplan
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Malagasy (mg.po) by Jaonary Rabarisoa
    - Macedonian (mk.po) by Georgi Stanojevski
    - Macedonian (pa_IN.po) by Amanpreet Singh Alam
    - Portuguese (pt.po) by Miguel Figueiredo
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrişor
    - Tagalog (tl.po) by Eric Pareja
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Vietnamese (vi.po) by Clytie Siddall
    - Wolof (wo.po) by Mouhamadou Mamoune Mbacke
    - Xhosa (xh.po) by Canonical Ltd
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu

 -- Colin Watson <cjwatson@debian.org>  Fri, 15 Jul 2005 17:15:29 +0100

palo-installer (0.0.6) unstable; urgency=low

  * Joey Hess
    - Add initrd code for 2.6 kernels. Closes: #282851

 -- Joey Hess <joeyh@debian.org>  Thu,  2 Dec 2004 01:09:54 -0500

palo-installer (0.0.5) unstable; urgency=low

  * Call "apt-install palo" in the postinst (closes: #262185).
  * Updated translations: 
    - Arabic (ar.po) by Ossama M. Khayat
    - Bulgarian (bg.po) by Ognyan Kulev
    - Bosnian (bs.po) by Safir Šećerović
    - Catalan (ca.po) by Jordi Mallach
    - Czech (cs.po) by Miroslav Kure
    - Welsh (cy.po) by Dafydd Harries
    - Danish (da.po) by Claus Hindsgaul
    - German (de.po) by Dennis Stampfer
    - Greek (el.po) by Greek Translation Team
    - Spanish (es.po) by Javier Fernandez-Sanguino Peña
    - Basque (eu.po) by Piarres Beobide Egaña
    - Persian (fa.po) by Arash Bijanzadeh
    - Finnish (fi.po) by Tapio Lehtonen
    - French (fr.po) by French Team
    - Hebrew (he.po) by Lior Kaplan
    - Croatian (hr.po) by Krunoslav Gernhard
    - Hungarian (hu.po) by VEROK Istvan
    - Indonesian (id.po) by Debian Indonesia Team
    - Japanese (ja.po) by Kenshi Muto
    - Korean (ko.po) by Changwoo Ryu
    - Lithuanian (lt.po) by Kęstutis Biliūnasn
    - Latvian (lv.po) by Aigars Mahinovs
    - Norwegian (nb.po) by Bjorn Steensrud
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Russian L10N Team
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Slovenian (sl.po) by Jure Čuhalev
    - Albanian (sq.po) by Elian Myftiu
    - Swedish (sv.po) by Per Olofsson
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov
    - Simplified Chinese (zh_CN.po) by Carlos Z.F. Liu
    - Traditional Chinese (zh_TW.po) by Tetralet

 -- Matthias Klose <doko@debian.org>  Sun, 10 Oct 2004 23:15:49 +0200

palo-installer (0.0.4) unstable; urgency=low

  * Bdale Garbee
    - need to compare filesystems, not devices, to find out if /boot is on /
      (closes: #249536)
  * Updated translations:
    - Hebrew (he.po) by Lior Kaplan
    - Indonesian (id.po) by Parlin Imanuel Toh
    - Norwegian (nb.po) by Bjørn Steensrud
    - Norwegian (nn.po) by Håvard Korsvoll
    - Polish (pl.po) by Bartosz Fenski
    - Romanian (ro.po) by Eddy Petrisor
    - Turkish (tr.po) by Osman Yüksel

 -- Bdale Garbee <bdale@gag.com>  Fri, 23 Apr 2004 17:34:59 -0600

palo-installer (0.0.3) unstable; urgency=low

  * Joshua Kwan
    - Use debhelper's new udeb support.
    - Add myself to Uploaders, this package hasn't been uploaded in a long
      time.
    - Change Build-Depends-Indep to Build-Depends.
  * Miguel Figueiredo
    - Added Portuguese (pt.po) translation.
  * Ming Hua
    - Initial Simplified Chinese translation (zh_CN.po)
    - Initial Traditional Chinese translation (zh_TW.po), by Tetralet
    - Updated Traditional Chinese translation (zh_TW.po), by Tetralet
  * Bartosz Fenski
    - Updated Polish (pl) translation.
  * Claus Hindsgaul
    - Update da (Danish) translation. Closes: #235124
  * Kenshi Muto
    - Added Japanese translation (ja.po)
    - Update ja.po
  * Teófilo Ruiz Suárez
    - Revised Spanish templates (es.po)
  * Alastair McKinstry
    - Converted changelog to UTF-8, as per policy.
    - Moved to Standards-Version: 3.6.1
  * Miroslav Kure
    - Initial Czech translation.
  * Christian Perrier
    - Improve French translation.
  * Safir Secerovic, Amila Akagic
    - Add Bosnian translation (bs.po)
  * Peter Mann
    - Initial Slovak translation (sk.po).
  * Ilgiz Kalmetev
    - Initial Russian translation.
  * Konstantinos Margaritis
    - Initial Greek translation (el.po).
  * Steinar H. Gunderson
    - Template polishing from Christian Perrier. (Closes: #218342)
    - Update Norwegian translation (nb.po).
  * Pierre Machard
    - Run debconf-updatepo.
    - Update French translation.
  * André Luís Lopes
    - Update pt_BR (Brazilian Portuguese) translation.
  * Verok Istvan
    - Initial Hungarian translation.
  * Konstantinos Margaritis
    - Updated Greek translation (el.po)
  * Giuseppe Sacco
    - First italian translation from Stefano Melchior
    - Applied patch for normalizing menus and some italian translation (it.po)
  * Teófilo Ruiz Suárez
    - Updated Spanish translation (es.po)
    - Switched to UTF-8
  * Alwin Meschede
    - Updated German translation (de.po)
  * Bart Cornelis
    - Updated Dutch translation (nl.po)
    - Incorporated debian-l10n-dutch review comments into nl.po
    - Merged Norwegian Bokmael (nb.po) translations from skolelinux-cvs
    - Merged Norwegian Nynorsk (nn.po) translations from skolelinux-cvs
  * Kęstutis Biliūnas
    - Added Lithuanian translation (lt.po);
    - Updated Lithuanian translation (lt.po).
  * Ognyan Kulev
    - Added/updated bulgarian translation (bg.po).
  * Dennis Stampfer
    - Merged some strings in German translation de.po
  * Jure Cuhalev
    - Added/updated slovenian translation (sl.po).
  * André Dahlqvist
    - Added Swedish translation
    - Merged Swedish translation (sv.po)
  * Teófilo Ruiz Suárez
    - Fixed some inconsistencies in Spanish Translation (es.po)
  * Anmar Oueja
    - created and translated to Arabic (ar.po)
  * Nikolai Prokoschenko
    - Upated russian translation (ru.po)
  * Safir Secerovic
    - Update Bosnian translation (bs.po).
  * h3li0s
    - added albanian translation (sq.po)
  * Jordi Mallach
    - Add Catalan translation (ca.po).
  * Eugen Meshcheryakov : added Ukrainian translation (uk.po)
  * Joey Hess
    - Add a dependency on di-utils-mapdevfs.
  * Håvard Korsvoll
    - Updated Norwegian, nynorsk (nn.po) translation.
  * Changwoo Ryu
    - Added Korean translation (ko.po)
  * Håvard Korsvoll
    - Updated Norwegian, bokmål translation, (nb.po). From Axel Bojer
  * Dafydd Harries
    - Added Welsh translation (cy.po)

 -- Joshua Kwan <joshk@triplehelix.org>  Tue,  6 Apr 2004 09:36:35 -0700

palo-installer (0.0.2) unstable; urgency=low

  * Petter Reinholdtsen
    - Update nn.po, thanks to Gaute Hvoslef Kvalnes.
  * Teófilo Ruiz Suárez
    - Updated es.po
  * Thorsten Sauter
    - Include german translation (de.po)
  * Matt Kraai
    - Add nl.po, thanks to Bart Cornelis.

 -- Alastair McKinstry <mckinstry@computer.org>  Sun, 23 Mar 2003 22:02:56 +0100

palo-installer (0.0.1) unstable; urgency=low

  * Richard Hirst
    - Initial attempt
    - change menu item number to match lilo
  * Petter Reinholdtsen
    - Added Norwegian Bokmål (nb.po) translations recieved from
      Bjørn Steensrud.
    - Added Norwegian Nynorsk (nn.po) translations recieved from
      Gaute Hvoslef Kvalnes.

 -- dann frazier <dannf@debian.org>  Sun, 16 Mar 2003 13:57:36 -0700
