<!-- retain these comments for translator revision tracking -->
<!-- original version: 35612 -->

  <sect2 arch="mipsel" id="boot-tftp"><title>Arrencada amb TFTP</title>

   <sect3>
   <title>Arrencada amb TFTP en Cobalt</title>
<para>

Estricament parlant, Cobalt utilitza NFS per arrencar en comptes de TFTP.
Necessitareu instal·lar un servidor NFS i posar els fitxers de l'instal·lador
a <filename>/nfsroot</filename>.  Quan arrenqueu el vostre Cobalt, heu de
pitjar els botons del cursor de la dreta i l'esquerre al mateix temps i la
màquina arrencarà via xarxa des de NFS. Aleshores mostrarà algunes opcions
a la pantalla.  Hi ha dos mètodes d'instal·lació:

<itemizedlist>
<listitem><para>

Via SSH (per defecte): En aquest cas, l'instal·lador configurarà la xarxa
fent servir DHCP i arrencant un servidor SSH. Mostrarà una contrasenya
aleatòria i altra informació de l'entrada (com la adreça IP) al LCD Cobalt.
Aleshores podeu connectar a la màquina amb un client SSH i podeu començar
amb la instal·lació.

</para></listitem>
<listitem><para>

Via la consola sèrie: Utilitzant un cable de mòdem nul, podeu connectar
al port sèrie de la vostra màquina Cobalt (utilitzant 115200 bps) i fent
la instal·lació d'aquesta forma. Aquesta opció no està disponible amb les
màquines Qube 2700 (Qube1) ja que no disposen de port sèrie.

</para></listitem>
</itemizedlist>

</para>
  </sect3>

   <sect3>
<!-- Note to translators: this is the same section as in mips.xml -->
   <title>Arrencada amb TFTP en Broadcom BCM91250A i BCM91480B</title>
<para>

En plaques d'avaluació Broadcom BCM91250A i BCM91480B, haureu de
carregar el carregador d'arrencada SiByl, i aquest s'encarregarà
aleshores d'iniciar l'instal·lador de Debian. En la majoria de casos,
primer obtindreu l'adreça IP mitjançant DHCP, però també és possible
configurar una adreça estàtica. Per usar DHCP, podeu introduir l'ordre
següent a l'indicador CFE:

<informalexample><screen>
ifconfig eth0 -auto
</screen></informalexample>

Una vegada heu obtingut una adreça IP, podeu carregar SiByl amb l'ordre
següent:

<informalexample><screen>
boot 192.168.1.1:/boot/sibyl
</screen></informalexample>

Haureu de substituir l'adreça IP de l'exemple pel nom o l'adreça del
vostre servidor TFTP. Tan bon punt s'executi l'ordre, l'instal·lador
es carregarà automàticament.

</para>
</sect3>
  </sect2>

  <sect2 arch="mipsel"><title>Paràmetres d'arrencada</title>

   <sect3>
   <title>Arrencada amb TFTP en Cobalt</title>
<para>

No podeu passar cap paràmetre d'arrencada directament. En comptes d'això,
heu d'editar el fitxer <filename>/nfsroot/default.colo</filename> al
servidor NFS i afegir els vostres paràmetres a la variable
<replaceable>args</replaceable>.

</para>
  </sect3>

   <sect3>
<!-- Note to translators: this is the same section as in mips.xml -->
   <title>Arrencada amb TFTP en Broadcom BCM91250A i BCM91480B</title>
<para>

No podeu passar cap paràmetre d'arrencada directament des de
l'indicador CFE. En comptes d'això, haureu d'editar el fitxer
<filename>/boot/sibyl.conf</filename> al servidor TFTP i afegir els
paràmetres desitjats a la variable <replaceable>extra_args</replaceable>.

</para>
  </sect3>

  </sect2>
