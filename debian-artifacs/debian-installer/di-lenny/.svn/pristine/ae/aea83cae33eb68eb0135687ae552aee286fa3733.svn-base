<!-- retain these comments for translator revision tracking -->
<!-- original version: 56244 -->

 <sect1 arch="not-s390"><title>Compra de maquinari específic per a GNU/Linux</title>

<para>

Hi ha diversos fabricants que venen sistemes amb Debian o altres distribucions
de GNU/Linux <ulink url="&url-pre-installed;">preinstal·lades</ulink>.
Potser heu de pagar més per a aquest privilegi, però així compreu pau
espiritual, ja que podeu estar segurs que el maquinari funciona bé a
GNU/Linux.

</para><para arch="m68k">

Malauradament, és força rar trobar cap fabricant que vengui màquines
&arch-title; noves.

</para><para arch="x86">

Si heu de comprar una màquina amb Windows inclòs, llegiu amb cura la
llicència de programari que ve amb el Windows; potser podeu rebutjar la
llicència i obtenir un descompte del fabricant. Cercar <quote>devolució de
windows</quote> a Internet us pot donar informació útil per ajudar-vos en
això.

</para><para>

Tant si compreu un sistema amb Linux preinstal·lat com si no, o fins i tot
si compreu un sistema de segona mà, és important que comproveu que el
vostre maquinari funciona amb el nucli Linux. Comproveu si el vostre
maquinari està llistat a les referències que es troben a continuació. Feu
saber al vostre venedor que voleu comprar per a un sistema Linux. Recolzeu
els fabricants de maquinari que funciona amb Linux.

</para>

  <sect2><title>Eviteu el maquinari propietari o tancat</title>
<para>

Alguns fabricants de maquinari simplement no ens diuen com escriure
controladors per al seu maquinari. Altres no ens permeten l'accés a la
documentació sense un acord de no divulgació que ens impediria alliberar
el codi font de Linux.

</para><para arch="m68k">

Un altre exemple és el maquinari propietari de l'antiga línia de Macintosh.
De fet, mai no s'ha alliberat especificacions o documentació per a maquinari
Macintosh, notablement per al controlador ADB (utilitzat pel teclat i el
ratolí), el controlador de disquetera, i tota l'acceleració i manipulació
CLUT del maquinari de vídeo (tot i que ara la manipulació CLUT funciona a
gairebé tots els xips de vídeo interns). Amb poques paraules, això explica 
per què el port de Lnux per a Macintosh no avança tan ràpidament com els
altres.

</para><para>

Com que no ens han donat permís per a accedir a la documentació d'aquests
dispositius, simplement no funcionen amb Linux. Podeu ajudar demanant als
fabricants d'aquest maquinari que alliberin la documentació. Si ho demana prou
gent, llavors s'adonaran que la comunitat del programari lliure és un mercat
important.

</para>
</sect2>


  <sect2 arch="x86"><title>Maquinari específic de Windows</title>
<para>

Una tendència molesta és la proliferació de mòdems i impressores específiques
de Windows. En alguns casos són dissenyats especialment per a serr utilitzats
amb el sistema operatiu Microsoft Windows i duen la llegenda
<quote>WinModem</quote> o <quote>Elaborat especialment per a ordinadors basats
en Windows</quote>. Això generalment es fa traient el processador incrustat
del maquinari i traslladant la feina d'aquest processador a un controlador de
Windows que s'executa a la UCP del vostre ordinador. Aquesta estratègia fa el
maquinari menys car, però l'estalvi sovint <emphasis>no</emphasis> arriba a
l'usuari, i aquest maquinari pot ser inclús més car que els dispositius 
equivalents que mantenen la seva intel·ligència incrustada.

</para><para>

És recomanable que eviteu maquinari específic de Windows per dues raons.
La primera és que els fabricants generalment no fan disponibles els recursos
necessaris per a escriure controladors per a Linux. Generalment, les
interfícies de programari i maquinari del dispositiu són propietàries, i la
documentació no està disponible sense un acord de no divulgació, si és que
està disponible. Això impossibilita que pugui ser utilitzat amb programari
lliure, ja que els desenvolupadors de programari lliure divulguen el codi
font dels seus programes. La segona raó és que quan es treu el processador
incrustat d'un d'aquests dispositius, el sistema operatiu ha de dur a terme
la feina d'aquest processador, sovint a prioritat de <emphasis>temps
real</emphasis>, provocant així que la UCP no estigui disponible per a
executar els vostres programes mentre està controlant el dispositiu. Com que
l'usuari típic de Windows no realitza multi-tasca tan intensivament com un
usuari de Linux, els fabricants esperen que els usuaris de Windows no
notaran la càrrega que aquest maquinari posa a la seva UPC. De tota manera,
qualsevol sistema operatiu multi-procés, fins i tot el Windows 2000 o XP,
pateixen degradacions en l'execució quan els fabricants de perifèrics
escatimen en la potència de processament del seu maquinari.

</para><para>

Podeu ajudar a millorar aquesta situació encoratjant aquests fabricants
a alliberar la documentació i altres recursos necessaris per a programar
el seu maquinari, però la millor estratègia és simplement evitar aquest
tipus de maquinari fins que estigui llistat com a funcionant al
<ulink url="&url-hardware-howto;">Linux Hardware Compatibility HOWTO</ulink>.

</para>
</sect2>
 </sect1>
