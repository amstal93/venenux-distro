<!-- $Id: arm.xml 37321 2006-05-14 16:17:19Z mck-guest $ -->
<!-- original version: 36639 -->

  <sect2 arch="arm" id="boot-tftp"><title>Zavedení z TFTP</title>

&boot-installer-intro-net.xml;

  <sect3 arch="arm"><title>Zavedení z TFTP na počítačích Netwinder</title>

<para>

Netwindery mají dvě síťová rozhraní: NE2000-kompatibilní kartu na
10Mbps (obvykle <filename>eth0</filename>) a 100Mbps kartu Tulip
(<filename>eth1</filename>). 100Mbps karta může mít problémy
s nahráváním obrazu přes TFTP, takže raději doporučujeme použít
pomalejší rozhraní (označené <literal>10 Base-T</literal>).

</para><note><para>

K zavedení instalačního systému potřebujete NeTTrom verze
2.2.1 a pozdější, doporučená verze je 2.3.3. Z licenčních důvodů se
však soubory s firmwarem nedají stáhnout. Pokud se situace změní,
naleznete nové obrazy na <ulink url="http//www.netwinder.org/"></ulink>.

</para></note><para>

Při zavádění Netwinderu musíte během odpočítávání přerušit zaváděcí
proces, což vám dovolí nastavit různé proměnné firmwaru, které
potřebujete pro zavedení instalačního systému. Nejprve nahrajte
výchozí nastavení:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>load-defaults</userinput>
</screen></informalexample>

Dále musíte nastavit síťovou adresu buď staticky:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv netconfig_eth0 flash</userinput>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv eth0_ip 192.168.0.10/24</userinput>
</screen></informalexample>

kde 24 je počet nastavených bitů v síťové masce, nebo dynamicky:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv netconfig_eth0 dhcp</userinput>
</screen></informalexample>

Pokud TFTP server není na lokální podsíti, budete možná muset upravit
nastavení <userinput>route1</userinput>. Dále byste měli nastavit
adresu TFTP serveru a umístění obrazu. Poté můžete uložit nastavení do
flash paměti.

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv kerntftpserver 192.168.0.1</userinput>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv kerntftpfile boot.img</userinput>
<prompt>NeTTrom command-&gt;</prompt> <userinput>save-all</userinput>
</screen></informalexample>

Nyní můžete firmwaru sdělit, že má z TFTP obrazu zavést systém:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv kernconfig tftp</userinput>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv rootdev /dev/ram</userinput>
</screen></informalexample>

Používáte-li pro instalaci Netwinderu sériovou konzoli, budete také
potřebovat následující nastavení:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv cmdappend root=/dev/ram console=ttyS0,115200</userinput>
</screen></informalexample>

Naopak pro instalace používající klávesnici a monitor musíte nastavit:

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>setenv cmdappend root=/dev/ram</userinput>
</screen></informalexample>

Nastavení proměnných můžete zkontrolovat příkazem
<command>printenv</command>. Je-li vše v pořádku, můžete obraz zavést
příkazem

<informalexample><screen>
<prompt>NeTTrom command-&gt;</prompt> <userinput>boot</userinput>
</screen></informalexample>

V případě jakýchkoliv problémů je k dispozici <ulink
url="http://www.netwinder.org/howto/Firmware-HOWTO.html">podrobné
HOWTO</ulink>.

</para>
  </sect3>

  <sect3 arch="arm"><title>Zavedení z TFTP na počítačích CATS</title>

<para>

Na strojích CATS použijte v Cyclone promptu příkaz
<command>boot de0:</command> (nebo podobný).

</para>
   </sect3>
  </sect2>

  <sect2 arch="arm"><title>Zavedení z CD-ROM</title>

&boot-installer-intro-cd.xml;

<para>

Pro zavedení z CD-ROM zadejte v promptu Cyclone konzoly příkaz
<command>boot cd0:cats.bin</command>

</para>
  </sect2>

  <sect2 arch="arm" id="boot-firmware">
  <title>Zavedení z firmwaru</title>

&boot-installer-intro-firmware.xml;

   <sect3 arch="arm" id="boot-firmware-nslu2">
   <title>Zavedení NSLU2</title>
<para>

Firmware instalačního systému lze do flash paměti nahrát třemi
způsoby.

</para>

    <sect4 arch="arm">
    <title>Pomocí webového rozhraní NSLU2</title>
<para>

Přejděte do administrátorské sekce a vyberte položku
<literal>Upgrade</literal>. Na disku vyhledejte obraz instalátoru,
který jste si tam již dříve stáhli. Poté stiskněte tlačítko
<literal>Start Upgrade</literal>, potvrďte, počkejte několik minut
a znovu potvrďte. Systém se pak zavede rovnou do instalačního
systému.

</para>
    </sect4>

    <sect4 arch="arm">
    <title>Přes síť pomocí Linuxu/Unixu</title>
<para>

Na libovolném počítači s Linuxem nebo Unixem můžete použít pro nahrání
instalačního systému do počítače program <command>upslug2</command>.
Tento software je dokonce zabalen přímo v Debianu.

</para><para>

Nejprve musíte přepnout NSLU2 do aktualizačního režimu:

<orderedlist>
<listitem><para>

Od USB portů odpojte všechny disky a jiná zařízení.

</para></listitem>
<listitem><para>

Vypněte NSLU2.

</para></listitem>
<listitem><para>

Stiskněte a držte tlačítko reset (přístupné skrze malou dírku na zadní
straně nad napájením).

</para></listitem>
<listitem><para>

Zapněte NSLU2.

</para></listitem>
<listitem><para>

Sledujte diodu ready/status a počkejte 10 sekund. Po 10 sekundách se
změní barva ze žluté na červenou. V ten okamžik uvolněte tlačítko
reset.

</para></listitem>
<listitem><para>

Dioda ready/status bude blikat střídavě červenou/zelenou barvou (před
první zelenou je sekundová pauza). NSLU2 je nyní v režimu aktualizace.

</para></listitem>
</orderedlist>

Pokud máte s touto částí problémy, podívejte se na stránky <ulink
url="http://www.nslu2-linux.org/wiki/OpenSlug/UsingTheBinary">NSLU2-Linux</ulink>.

Až bude NSLU2 v aktualizačním režimu můžete do něj nahrát nový obraz:

<informalexample><screen>
<userinput>sudo upslug2 -i di-nslu2.bin</userinput>
</screen></informalexample>

Všimněte si, že nástroj také zobrazí MAC adresu vašeho NSLU2, což se
může hodit pro nastavení DHCP serveru. Po zapsání a ověření celého
obrazu se systém automaticky restartuje. Nyní musíte znovu připojit
USB disk, protože jinak by jej instalační systém nenašel.

</para>
    </sect4>

    <sect4 arch="arm">
    <title>Přes síť pomocí Windows</title>
<para>

Pro Windows existuje <ulink
url="http://www.everbesthk.com/8-download/sercomm/firmware/all_router_utility.zip">nástroj</ulink>,
kterým se dá nahrát firmware přes síť.

</para>
    </sect4>
   </sect3>
  </sect2>
