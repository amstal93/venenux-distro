# Norwegian Bokmal translation for DI win32-loader.
# Copyright (C) 2007  Free Software Foundation, Inc.
# This file is distributed under the same license as the win32-loader package.
#
# Hans Fredrik Nordhaug <hans@nordhaug.priv.no>, 2007-2008.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-04-30 15:17+0200\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Emacs po-mode\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_NORWEGIAN"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp850"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Norwegian Bokmal"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian installasjonsprogram"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Norwegian.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Innlaster for Debian installasjonsprogram"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Klarte ikke finne win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr ""
"win32-loader.ini er ufullstendig. Kontakt leverandøren av dette mediet."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr "Programmet har oppdaget at din tastaturtype er «$0». Er det riktig?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Send en feilmelding med følgende informasjon:\n"
"\n"
" - Versjon av Windows.\n"
" - Landinnstillinger.\n"
" - Reelle tastaturtype.\n"
" - Oppdaget tastaturtype.\n"
"\n"
"Takk."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"Det ser ikke ut til å være nok ledig diskplass på lagringsenhet $c. For en "
"komplett skrivebordsinstallasjon er det anbefalt med minst 3GB. Hvis det "
"allerede er en separat disk eller partisjon for installasjonen av Debian, "
"eller hvis du planlegger å bytte ut Windows fullstendig, kan du trygt "
"ignorere denne advarselen."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Feil: Ikke nok ledig diskplass. Avbrøt installeringen."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Dette programmet støtter ikke Windows $windows_version enda."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"Versjonen av debian som du prøver å installere er lagd for å kjøre på "
"moderne 64-bit datamaskiner. Din datamaskin klarer ikke å kjøre 64-bit "
"program.\n"
"\n"
"Bruk 32-bit («i386») versjonen av Debian eller Multi-arch versjonen som kan "
"installere både 32- og 64-bit.\n"
"\n"
"Installasjonen avbrytes nå."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Din datamaskin kan kjøre moderne, 64-bit operativsystem. Versjonen av Debian "
"som du prøver å installere er lagd for å kjøre på eldre, 32-bit maskinvare.\n"
"\n"
"Du kan fortsette installasjonen, men for å få fullt utbytte av datamaskinen "
"anbefaler vi at du bruker 64-bit («amd64») versjon av Debian istedenfor. "
"Multi-arch versjonen kan installere både 32- og 64-bit.\n"
"\n"
"Vil du avbryte installasjonen nå?"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "Velg installeringsmåte:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Normal. Anbefalt for de fleste brukere."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr ""
"Ekspert. Anbefalte for ekspertbrukere som vil ha full kontroll med "
"installasjonsprosessen."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Velg handling:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Installer Debian GNU/Linux på denne datamaskinen."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Reparer et eksisterende Debiansystem (redningsmodus)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Grafisk installasjon"

#: win32-loader.c:89
msgid "Text install"
msgstr "Tekst installasjon"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Laster ned %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Opprette forbindelse .."

#: win32-loader.c:92
msgid "second"
msgstr "sekund"

#: win32-loader.c:93
msgid "minute"
msgstr "minutt"

#: win32-loader.c:94
msgid "hour"
msgstr "time"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "er"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) av %dkB ved %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s gjenstår)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Velg hvilken versjon av Debian installasjonsprogrammet du vil bruke:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Stabil utgave. Dette vil installere Debian «stable»."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"Daglig utgave. Dette er utviklingsversjonen av Debian "
"installasjonsprogrammet. Det vil installere Debian «testing» som standard og "
"kan være i stand til å installere «stable» eller «unstable» i tillegg."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr ""
"Det er anbefalt at du sjekker for kjente feil før du bruker en daglig "
"utgave. Vil du gjøre det nå?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Skrivebordsmiljø:"

#: win32-loader.c:114
msgid "None"
msgstr "Ingen"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Innlasteren av Debian installasjonsprogrammet er satt opp med følgende "
"parametre. IKKE endre noen av disse uten at du er helt sikker på hva du gjør."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Proxyinnstillinger (tjener:port):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Plassering av boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "Basis URL-en for nettoppstartsfiler (linux og initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Feil"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Feil: Klarte ikke kopiere $0 til $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Genererer $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Legger til informasjon om preseeding til $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Feil: Klarte ikke kjøre $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Slår av NTFS-komprimering i oppstartsfiler"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Registrerer Debian installasjonsprogram i NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Registrerer Debian installasjonsprogram i BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Feil: Klarte ikke tolke resultat fra bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "Feil: Fant ikke boot.ini. Er dette virkelig Windows $windows_version?"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "VELDIG VIKTIG MELDING"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"Andre del av installasjonsprosessen starter nå. Etter at du har bekreftet, "
"så vil dette programmet starte opp igjen Windows i DOS-modus og automatisk "
"laste inn Debian installasjonsprogrammet.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"Du må ta en omstart for å fortsette din installasjon av Debian. Under din "
"neste start, vil du bli spurt om du vil start Windows eller Debian "
"installasjonsprogrammet. Velg Debian for å fortsette med installasjonen."

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Under installasjonsprosessen vil du bli tilbudt muligheten til enten å "
"redusere din Windows-partisjon eller fullstendig bytte den ut for å "
"installere Debian. I begge tilfeller er det STERKT ANBEFALT at du tidligere "
"har tatt en sikkerhetskopi av dine data. Hverken forfatterne av denne "
"innlasteren eller Debian-prosjektet tar NOE ANSVAR for eventuelle tap av "
"data.\\n\\nNår installasjonen av Debian er ferdig (og hvis du har valgt å "
"beholde Windows på disken din), kan du avinstallere denne innlasteren "
"gjennom Windows sin «Legg til/fjern programvare»-dialog i kontrollpanelet."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Vil du starte på nytt nå?"

#~ msgid "Debconf preseed line:"
#~ msgstr "Førinnstillingslinje for Debconf:"
