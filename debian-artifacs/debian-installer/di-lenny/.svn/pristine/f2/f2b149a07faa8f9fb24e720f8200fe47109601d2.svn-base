<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 40980 -->
<!-- Revisado por Steve Langasek -->
<!-- Revisado Rudy Godoy, 23 feb. 2005 -->
<!-- revisado por Igor Tamara, enero 2007 -->

 <sect1 id="kernel-baking"><title>Compilar un nuevo n�cleo</title>
<para>

�Por qu� querr�a alguien compilar un nuevo n�cleo? La mayor�a de las
veces no ser� necesario puesto que el n�cleo est�ndar entregado
con Debian gestiona la mayor�a de configuraciones. Adem�s, Debian
ofrece habitualmente varios n�cleos alternativos. As�, deber�a comprobar
si hay un paquete de imagen de n�cleo alternativa que se ajuste mejor
a su hardware. En cualquier caso
es �til compilar un nuevo n�cleo para:

<itemizedlist>
<listitem><para>

tratar necesidades especiales de hardware, o conflictos de los mismos con
n�cleos predeterminados.

</para></listitem>
<listitem><para>

utilizar opciones del n�cleo que no est�n soportadas en los n�cleos preparados,
como puede ser el caso del soporte de memoria elevada (m�s de 4GB).

</para></listitem>

<listitem><para>

optimizar el n�cleo eliminando controladores no usados para acelerar
el tiempo de arranque.

</para></listitem>
<listitem><para>

crear un n�cleo monol�tico en lugar de uno modular.

</para></listitem>
<listitem><para>

ejecutar un n�cleo actualizado o de desarrollo.

</para></listitem>
<listitem><para>

aprender m�s de los n�cleos de Linux.

</para></listitem>
</itemizedlist>

</para>

  <sect2><title>Gesti�n de la imagen del n�cleo</title>
<para>

No tenga temor de intentar compilar el n�cleo. Es divertido y fruct�fero.

</para><para>

Para compilar un n�cleo al <quote>estilo Debian</quote>,
necesita algunos paquetes:
<classname>fakeroot</classname>,
<classname>kernel-package</classname>,
<classname>linux-source-2.6</classname>
y algunos otros que probablemente ya est�n instalados (vea
<filename>/usr/share/doc/kernel-package/README.gz</filename> si desea una
lista completa.

</para><para>

Este m�todo crear� un �.deb� para su n�cleo fuente y, si tiene m�dulos
no est�ndares, construir� tambi�n un �.deb� con dependencia sincronizada de
los mismos. Es la mejor forma de gestionar im�genes del n�cleo;
<filename>/boot</filename> albergar� el n�cleo, el fichero �System.map� y
el fichero de registro del fichero �config� activo para la construcci�n.

</para><para>

Observe que no <emphasis>tiene</emphasis> que compilar su n�cleo
<quote>al estilo Debian</quote>; pero creemos que usar el sistema de paquetes para
gestionar su n�cleo es realmente m�s seguro y sencillo. De hecho, puede
obtener las fuentes de su n�cleo directamente de Linus en lugar de
<classname>linux-source-2.6</classname>,
y a�n as� usar el m�todo de compilaci�n <classname>kernel-package</classname>.

</para><para>

Note que puede encontrar documentaci�n completa sobre el uso de
<classname>kernel-package</classname> en
<filename>/usr/doc/kernel-package</filename>. Esta secci�n s�lo contiene un
breve tutorial.

</para><para>

A partir de ahora, asumiremos que tiene acceso completo al sistema
y que va a extraer las fuentes del n�cleo en alg�n lugar de su
directorio personal<footnote>

<para>

Hay otras ubicaciones donde puede extraer las fuentes del n�cleo y
construir su n�cleo a medida, pero �sta es la forma m�s sencilla ya
que no requiere de permisos adicionales.

</para>

</footnote>. Tambi�n daremos por supuesto que la versi�n de su
n�cleo es &kernelversion;. Ub�quese en el directorio
donde desee desempaquetar las fuentes del
n�cleo, extraiga las fuentes del
mismo usando
<userinput>tar xjf /usr/src/linux-source-&kernelversion;.tar.bz2</userinput>
y c�mbiese al directorio
<filename>linux-source-&kernelversion;</filename>
que se habr� creado.

</para><para>

Ahora puede configurar su n�cleo. Ejecute
<userinput>make xconfig</userinput> si tiene X11 instalado,
configurado y ejecut�ndose, <userinput>make menuconfig</userinput> en
otro caso (necesitar� instalar <classname>libncurses5-dev</classname>). T�mese
su tiempo para leer la ayuda en l�nea y escoja cuidadosamente. Cuando
tenga duda, a menudo es mejor incluir el controlador de dispositivo
(el software que maneja el hardware perif�rico, como tarjetas Ethernet,
controladoras SCSI, etc.) del que no est� seguro. Tenga cuidado: Otras
opciones no relacionadas con un hardware espec�fico deber�an dejarse en
su valor predeterminado si no las comprende. No olvide seleccionar
<quote>Kernel module loader</quote> (para la carga autom�tica de los m�dulos) en
<quote>Loadable module support</quote> (�ste no es elegido por omisi�n).
Su instalaci�n Debian experimentar� problemas si no lo incluye.

</para><para>

Limpie el �rbol de fuentes y reinicie los par�metros de
<classname>kernel-package</classname>. Para hacerlo, ejecute
<userinput>make-kpkg clean</userinput>.

</para><para>

Ahora, compile el n�cleo: <userinput>fakeroot /usr/sbin/make-kpkg
--initrd --revision=custom.1.0 kernel_image</userinput>. Por supuesto, puede
cambiar el n�mero de la versi�n <quote>1.0</quote>
por cualquier otro valor; �ste s�lo es el n�mero
de versi�n que usar� para seguir la pista a los n�cleos que haya
construido. Igualmente puede sustituir <quote>custom</quote> por
cualquier otra palabra (por ejemplo, un nombre de m�quina).
La compilaci�n del n�cleo puede tomar un buen
tiempo, dependiendo de la potencia de su m�quina.

</para><para>

Una vez finalizada la compilaci�n, puede instalar su n�cleo
personalizado como cualquier paquete. Como superusuario, ejecute
<userinput>dpkg -i
../linux-image-&kernelversion;-<replaceable>subarchitectura</replaceable>_custom.1.0_&architecture;.deb</userinput>.
La parte <replaceable>subarquitectura</replaceable> es una subarquitectura
opcional,
<phrase arch="i386"> como <quote>i586</quote>, </phrase> dependiendo de las opciones
que haya establecido en el n�cleo.
<userinput>dpkg -i</userinput>
instalar� el n�cleo, junto con otros ficheros de soporte. Por ejemplo,
se instalar� <filename>System.map</filename> adecuadamente (de gran ayuda
para depurar problemas con el n�cleo), y tambi�n se instalar�
<filename>/boot/config-&kernelversion;</filename>, con su configuraci�n actual.
Su nuevo paquete
es tambi�n lo suficientemente listo como para actualizar autom�ticamente el
gestor de de arranque de su plataforma para que utilice la informaci�n de la
imagen del n�cleo, la cual le permita arrancar sin necesidad de ejecutarlo
nuevamente. Deber� instalar tambi�n el paquete de m�dulos si lo ha
construido.

</para><para>

Es el momento de reiniciar el sistema: lea cuidadosamente cualquier advertencia
que pueda haberse producido, luego ejecute <userinput>shutdown -r now</userinput>.

</para><para>

Si desea m�s informaci�n sobre los n�cleos de Debian y la compilaci�n del
n�cleo consulte el <ulink url="&url-kernel-handbook;">Debian Linux Kernel
Handbook</ulink> (�Gu�a del n�cleo de Linux para Debian�).
Consulte la excelente documentaci�n disponible en
<filename>/usr/share/doc/kernel-package</filename>
si desea m�s informaci�n sobre <classname>kernel-package</classname>.

</para>

</sect2>
</sect1>
