<!-- original version: 55471 -->

  <sect2 arch="mips">
<title>Microprocesseurs, cartes mère et cartes vidéo</title>

<para>

Debian sur &arch-title; gère les plateformes suivantes&nbsp;:

<itemizedlist>
<listitem><para>
SGI IP22&nbsp;: Cette plateforme englobe les machines SGI Indy, Indigo 2 et 
Challenge S. Puisque ces machines sont très semblables, chaque fois que ce 
document se rapporte aux machines SGI Indy, cela concerne aussi les machines 
Indigo 2 et Challenge S.

</para></listitem>
<listitem><para>

SGI IP32&nbsp;: cette plateforme est connue comme SGI O2.

</para></listitem>
<listitem><para>

MIPS Malta&nbsp;: cette plateforme est simulée par QEMU. C'est un bon moyen de tester Debian sur ARM
quand on ne possède pas le matériel.

</para></listitem>
<listitem><para>

Broadcom BCM1250 (SWARM)&nbsp;: c'est une carte d'évaluation au format ATX de
Broadcom basé sur la famille de leur processeur SB1 1250.

</para></listitem>
<listitem><para>
Broadcom BCM91480B (BigSur): c'est une carte d'évaluation au format ATX de
Broadcom basé sur la famille de leur processeur SB1A 1480.


</para></listitem>
</itemizedlist>

Une information complète à propos des machines mips/mipsel se trouve dans la 
<ulink url="&url-linux-mips;">page Linux-MIPS</ulink>. Dans la suite, seuls les
systèmes gérés par l'installateur Debian seront traités. Si vous recherchez de 
l'aide pour d'autres sous-architectures, vous pouvez contacter la 
<ulink url="&url-list-subscribe;">liste de diffusion debian-&arch-listname;</ulink>.

</para>

   <sect3>
<title>Microprocesseurs</title>

<para>
	
Sur SGI IP22, les plateformes SGI Indy, Indigo 2 et Challenge S utilisant 
les processeurs R4000, R4400, R4600 et R5000 sont reconnues par le système 
d'installation de Debian pour MIPS grand-boutien. Sur SGI IP32, seuls les systèmes
basés sur le R5000 sont reconnus. La carte BCM91250A de Broadcom qui possède une puce 
SB1 1250 avec deux processeurs est gérée en mode SMP par l'installateur.
De même, la carte BCM91480B avec une puce SB1A 1480 est gérée en mode SMP.

</para><para>

Certaines machines MIPS peuvent fonctionner dans les modes grand-boutien 
ou petit-boutien. Pour les MIPS petit-boutien, veuillez-vous reporter à 
la documentation sur l'architecture «&nbsp;mipsel&nbsp;».

</para>
   </sect3>
  </sect2>
