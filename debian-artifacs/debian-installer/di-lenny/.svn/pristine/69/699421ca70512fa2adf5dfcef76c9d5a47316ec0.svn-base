# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005-2007.
# Felipe Castro <fefcas@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: eo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-08-03 08:57-0300\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. MUST be kept below 55 characters/columns
#. :sl1:
#: ../apt-setup-udeb.templates:1001
msgid "Configure the package manager"
msgstr "Akomodi la pak-administrilon"

#. Type: text
#. Description
#. Translators, "apt" is the program name
#. so please do NOT translate it
#. :sl1:
#: ../apt-setup-udeb.templates:2001
msgid "Configuring apt"
msgstr "Akomodado de 'apt'"

#. Type: text
#. Description
#. :sl1:
#: ../apt-setup-udeb.templates:3001
msgid "Running ${SCRIPT}..."
msgstr "Interpretado de '${SCRIPT}'..."

#. Type: text
#. Description
#. :sl1:
#: ../apt-setup-udeb.templates:4001
msgid "Scanning local repositories..."
msgstr "Skanado de lokaj deponejoj..."

#. Type: text
#. Description
#. :sl1:
#: ../apt-setup-udeb.templates:5001
msgid "Scanning the security updates repository..."
msgstr "Skanado de la deponejo de sekurecaj ĝisdatigoj..."

#. Type: text
#. Description
#. :sl1:
#: ../apt-setup-udeb.templates:6001
msgid "Scanning the volatile updates repository..."
msgstr "Skanado de la deponejo de volatilaj ĝisdatigoj..."

#. Type: error
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:9001
msgid "Cannot access repository"
msgstr "Neeblas atingi la pak-deponejon"

#. Type: error
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:9001
msgid ""
"The repository on ${HOST} couldn't be accessed, so its updates will not be "
"made available to you at this time. You should investigate this later."
msgstr ""
"La deponejo '${HOST}' ne estas atingebla, tiel la ĝisdatigoj ne estos "
"disponeblaj al vi ĉi foje. Vi devus poste esplori tion."

#. Type: error
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:9001
msgid ""
"Commented out entries for ${HOST} have been added to the /etc/apt/sources."
"list file."
msgstr ""
"Malaktivaj enirejoj por ${HOST} estis aldonitaj al la '/etc/apt/sources."
"list'-dosiero."

#. Type: multiselect
#. Choices
#. SEC_HOST and VOL_HOST are host names (e.g. security.debian.org)
#. Translators: the *entire* string should be under 55 columns
#. including host name. In short, KEEP THIS SHORT and, yes, that's tricky
#. :sl2:
#: ../apt-setup-udeb.templates:10001
msgid "security updates (from ${SEC_HOST})"
msgstr "sekuraj ĝisdatigoj (el ${SEC_HOST})"

#. Type: multiselect
#. Choices
#. SEC_HOST and VOL_HOST are host names (e.g. security.debian.org)
#. Translators: the *entire* string should be under 55 columns
#. including host name. In short, KEEP THIS SHORT and, yes, that's tricky
#. :sl2:
#: ../apt-setup-udeb.templates:10001
msgid "volatile updates (from ${VOL_HOST})"
msgstr "volatilaj ĝisdatigoj (el ${VOL_HOST})"

#. Type: multiselect
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:10002
msgid "Services to use:"
msgstr "Uzotaj servoj:"

#. Type: multiselect
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:10002
msgid ""
"Debian has two services that provide updates to releases: security and "
"volatile."
msgstr ""
"Debiano posedas du servojn provizantajn freŝdatajn eldonojn: sekura kaj "
"volatila."

#. Type: multiselect
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:10002
msgid ""
"Security updates help to keep your system secured against attacks. Enabling "
"this service is strongly recommended."
msgstr ""
"Sekura ĝisdatigo permesas gardi vian sistemon sekuritan kontraŭ atakoj. Oni "
"nepre konsilas aktivigi ĉi tiun servon."

#. Type: multiselect
#. Description
#. :sl2:
#: ../apt-setup-udeb.templates:10002
msgid ""
"Volatile updates provide more current versions for software that changes "
"relatively frequently and where not having the latest version could reduce "
"the usability of the software. An example is the virus signatures for a "
"virus scanner. This service is only available for stable and oldstable "
"releases."
msgstr ""
"Volatila ĝisdatigo provizas freŝdatajn eldonojn de ofte ŝanĝitaj programoj, "
"kiuj perdus iliajn interesojn se oni ne uzas la lastajn versiojn. Ekzemple, "
"la signumoj por la kantraŭvirusaj programoj. Tiu servo ne estas disponebla "
"en la 'stable' kaj 'oldstable'-distribuo."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:1001
msgid "Scanning the CD-ROM..."
msgstr "Skanado de la lumdisko..."

#. Type: error
#. Description
#. :sl2:
#: ../apt-cdrom-setup.templates:2001
msgid "apt configuration problem"
msgstr "'apt'-akomodada problemo"

#. Type: error
#. Description
#. :sl2:
#: ../apt-cdrom-setup.templates:2001
msgid ""
"An attempt to configure apt to install additional packages from the CD "
"failed."
msgstr ""
"'apt'-akomoda klopodo por instali pluajn pakojn el lumdisko malsukcesis."

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:3001 ../apt-cdrom-setup.templates:4001
#: ../apt-cdrom-setup.templates:5001 ../apt-cdrom-setup.templates:6001
msgid "Scan another CD or DVD?"
msgstr "Ĉu oni skanu alian lumdiskon?"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:3001
msgid "Your installation CD or DVD has been scanned; its label is:"
msgstr "Via instalada lumdisko estis skanita; ĝia etikedo estas:"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:3001
msgid ""
"You now have the option to scan additional CDs or DVDs for use by the "
"package manager (apt). Normally these should be from the same set as the "
"installation CD/DVD. If you do not have any additional CDs or DVDs "
"available, this step can just be skipped."
msgstr ""
"Nun, vi povas skani aldonajn lumdiskojn aŭ lumdiskegojn uzotajn de la pak-"
"administrilo (apt). Ordinare ili devas aparteni al la sama aro de la "
"instalada lumdisko. Se vi ne posedas aliajn disponeblajn lumdiskojn, vi "
"povas preterpasi ĉi tiun etapon."

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:3001 ../apt-cdrom-setup.templates:4001
msgid "If you wish to scan another CD or DVD, please insert it now."
msgstr "Se vi deziras skani alian lumdiskon, bonvolu enmeti ĝin nun."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:4001
msgid "The CD or DVD with the following label has been scanned:"
msgstr "La lumdisko kun la jena etikedo estis skanita:"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:5001
msgid "The CD or DVD with the following label has already been scanned:"
msgstr "La lumdisko kun la jena etikedo jam estis skanita:"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:5001
msgid "Please replace it now if you wish to scan another CD or DVD."
msgstr "Bonvolu nun anstataŭigi ĝin, se vi deziras skani alian lumdisk(eg)on."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:6001
msgid ""
"An attempt to configure apt to install additional packages from the CD/DVD "
"failed."
msgstr ""
"Provo agordi 'APT' por instali aldonajn pakojn el lumdisko malsukcesis."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:6001
msgid "Please check that the CD/DVD has been inserted correctly."
msgstr "Bonvolu kontroli ĉu la lumdisko estis enmetita ĝuste."

#. Type: text
#. Description
#. :sl1:
#. This template uses the same text as used in the package apt for apt-cdrom
#. Do not translate "/cdrom/" (the mount point)
#: ../apt-cdrom-setup.templates:7001
msgid "Media change"
msgstr "Ŝanĝo de datumarujo"

#. Type: text
#. Description
#. :sl1:
#. This template uses the same text as used in the package apt for apt-cdrom
#. Do not translate "/cdrom/" (the mount point)
#: ../apt-cdrom-setup.templates:7001
msgid ""
"/cdrom/:Please insert the disc labeled: '${LABEL}' in the drive '/cdrom/' "
"and press enter."
msgstr ""
"/cdrom/:Bonvolu enmeti la diskon etikeditan: '${LABEL}' en la diskturnilo '/"
"cdrom/' kaj premu la klavon 'Enter'."

#. Type: text
#. Description
#. :sl1:
#. finish-install progress bar item
#: ../apt-cdrom-setup.templates:8001
msgid "Disabling netinst CD in sources.list..."
msgstr "Malaktivado de la lumdisko 'netinst' en la dosiero 'sources.list'..."

#. Type: text
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. :sl2:
#: ../apt-cdrom-setup.templates:9001 ../apt-mirror-setup.templates:6001
msgid ""
"If you are installing from a netinst CD and choose not to use a mirror, you "
"will end up with only a very minimal base system."
msgstr ""
"Se vi instalas la sistemon per lumdisko 'netinst' kaj vi ne uzas la retan "
"spegularkivon, tiam la instalota sistemo estos nur baza kaj mininuma."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:10001
msgid ""
"You are installing from a netinst CD, which by itself only allows "
"installation of a very minimal base system. Use a mirror to install a more "
"complete system."
msgstr ""
"Vi plenumas instaladon per lumdisko 'netinst' permesanta nur instaladon de "
"mininuma baza sistemo. Uzu spegularkivon por instali pli kompletan sistemon."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:11001
msgid ""
"You are installing from a CD, which contains a limited selection of packages."
msgstr ""
"Vi plenumas instaladon per lumdisko enhavanta malampleksan kolekton de pakoj."

#. Type: text
#. Description
#. :sl1:
#. The value of %i can be 2 or 3
#: ../apt-cdrom-setup.templates:12001
#, no-c-format
msgid ""
"You have scanned %i CDs. Even though these contain a fair selection of "
"packages, some may be missing (notably some packages needed to support "
"languages other than English)."
msgstr ""
"Vi skanis %i lumdiskojn. Kvankam multaj pakoj ĉeestas, kelkaj povas manki "
"(ĉefe, pakoj ebligantaj uzadon de diversaj lingvoj)."

#. Type: text
#. Description
#. :sl1:
#. The value of %i can be from 4 to 8
#: ../apt-cdrom-setup.templates:13001
#, no-c-format
msgid ""
"You have scanned %i CDs. Even though these contain a large selection of "
"packages, some may be missing."
msgstr ""
"Vi skanis %i lumdiskojn. Kvankam multaj pakoj ĉeestas, kelkaj povas manki."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:14001
msgid ""
"Note that using a mirror can result in a large amount of data being "
"downloaded during the next step of the installation."
msgstr ""
"Rimarku ke uzado de spegularkivo povas kaŭzi elŝutadon de granda kvanto da "
"datumoj dum la sekvonta instaletapo."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:15001
msgid ""
"You are installing from a DVD. Even though the DVD contains a large "
"selection of packages, some may be missing."
msgstr ""
"Vi plenumas instaladon per lumdiskego. Eĉ se ĝi enhavas ampleksan kolekton "
"de pakoj, kelkaj povas manki."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:16001
msgid ""
"Unless you don't have a good Internet connection, use of a mirror is "
"recommended, especially if you plan to install a graphical desktop "
"environment."
msgstr ""
"Krom se vi ne havas bonan interretan konekton, oni rekomendas uzon de "
"spegularkivo, precipe se vi intencas instali grafikan labortablon."

#. Type: text
#. Description
#. :sl1:
#: ../apt-cdrom-setup.templates:17001
msgid ""
"If you have a reasonably good Internet connection, use of a mirror is "
"suggested if you plan to install a graphical desktop environment."
msgstr ""
"Se vi havas rapidan kaj altkvalitan interretan konekton, vi devus uzi "
"spegularkivon. Precipe se vi intencas instali grafikan labortablon."

#. Type: text
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:1001
msgid "Scanning the mirror..."
msgstr "Skanado de la spegularkivo..."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:2001
msgid "Use non-free software?"
msgstr "Ĉu uzi neliberan programaron?"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:2001
msgid ""
"Some non-free software has been made to work with Debian. Though this "
"software is not at all a part of Debian, standard Debian tools can be used "
"to install it. This software has varying licenses which may prevent you from "
"using, modifying, or sharing it."
msgstr ""
"Kelkaj neliberaj programaroj funkcias kun Debiano. Kvankam tiu programaro "
"tute ne apartenas al Debiano, ordinaran Debianan ilaron eblas uzi por "
"instali ĝin. Tiu programaro trudas diversajn uzrajtojn kiuj povas malebligi "
"ke vi uzu, modifu, aŭ distribuu ĝin."

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates:2001
#: ../apt-mirror-setup.templates-ubuntu:1001
msgid "Please choose whether you want to have it available anyway."
msgstr "Bonvolu elekti ĉu vi deziras disponi ĝin ĉiel ajn."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:3001
msgid "Use contrib software?"
msgstr "Ĉu uzi kontribuan programaron?"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:3001
msgid ""
"Some additional software has been made to work with Debian. Though this "
"software is free, it depends on non-free software for its operation. This "
"software is not a part of Debian, but standard Debian tools can be used to "
"install it."
msgstr ""
"Kelkaj kromaj programoj funkcias kun Debiano. Kvankam tiu programaro estas "
"libera, ĝi dependas de malliberaj programaroj por funkcii. Tiu programaro ne "
"apartenas al Debiano, sed ordinaran Debianan ilaron povas esti uzata por "
"instali ĝin."

#. Type: boolean
#. Description
#. :sl1:
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates:3001
#: ../apt-mirror-setup.templates-ubuntu:2001
#: ../apt-mirror-setup.templates-ubuntu:3001
#: ../apt-mirror-setup.templates-ubuntu:4001
msgid ""
"Please choose whether you want this software to be made available to you."
msgstr "Bonvolu informi tamen ĉu vi intencas disponi tiun ĉi programaron."

#. Type: select
#. Choices
#. :sl2:
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:4001
msgid "Retry"
msgstr "Reprovu"

#. Type: select
#. Choices
#. :sl2:
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:4001
msgid "Change mirror"
msgstr "Ŝanĝu spegularkivon"

#. Type: select
#. Choices
#. :sl2:
#. These are choices of actions so this is, at least in English,
#. an infinitive form
#: ../apt-mirror-setup.templates:4001
msgid "Ignore"
msgstr "Ignori"

#. Type: select
#. Description
#: ../apt-mirror-setup.templates:4002
msgid "Downloading a file failed:"
msgstr "Elŝutado de la dosiero malsukcesas:"

#. Type: select
#. Description
#: ../apt-mirror-setup.templates:4002
msgid ""
"The installer failed to access the mirror. This may be a problem with your "
"network, or with the mirror. You can choose to retry the download, select a "
"different mirror, or ignore the problem and continue without all the "
"packages from this mirror."
msgstr ""
"La instalado ne sukcesis aliri la spegularkivon. Eble estas reta problemo, "
"aŭ ĉe la spegularkivo. Vi reprovu elŝuton, elektu alian spegularkivon, aŭ "
"preterpasu la problemon elektante daŭrigi sen ĉiuj pakoj el tiu spegularkivo."

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:5001
msgid "Use a network mirror?"
msgstr "Ĉu uzi retan spegularkivon?"

#. Type: boolean
#. Description
#. :sl1:
#: ../apt-mirror-setup.templates:5001
msgid ""
"A network mirror can be used to supplement the software that is included on "
"the CD-ROM. This may also make newer versions of software available."
msgstr ""
"Reta spegularkivo povas suplementi la programaron, kiu estas inkluzivitan "
"en  la lumdisko. Tio povas ankaŭ disponigi pli novajn versiojn de "
"programaroj."

#. Type: boolean
#. Description
#. :sl2:
#: ../apt-mirror-setup.templates:6001
msgid "Continue without a network mirror?"
msgstr "Daŭrigu sen retan spegularkivon?"

#. Type: boolean
#. Description
#. :sl2:
#: ../apt-mirror-setup.templates:6001
msgid "No network mirror was selected."
msgstr "Neniu elektita reta spegularkivo."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:1001
msgid "Use restricted software?"
msgstr "Ĉu uzi limigitan programaron?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:1001
msgid ""
"Some non-free software is available in packaged form. Though this software "
"is not a part of the main distribution, standard package management tools "
"can be used to install it. This software has varying licenses which may "
"prevent you from using, modifying, or sharing it."
msgstr ""
"Kelkaj malliberaj programoj pretas en pakformo. Kvankam tio programaro ne "
"apartenas al la ĉefa eldono, kutimaj pakaj administriloj uzeblus por instali "
"ĝin. Tio programaro posedas diversajn uzrajtojn kiuj malebligas vin uzi, "
"modifi, aŭ disdoni ĝin."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:2001
msgid "Use software from the \"universe\" component?"
msgstr "Ĉu uzi programaron el 'universe' komponento?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:2001
msgid ""
"Some additional software is available in packaged form. This software is "
"free and, though it is not a part of the main distribution, standard package "
"management tools can be used to install it."
msgstr ""
"Kelkaj pluaj programoj pretas en pakformo. Kvankam tiu programaro estas "
"libera, ne apartenas al ĉefa eldono, kutimaj pakaj administriloj uzeblus por "
"instali ĝin."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:3001
msgid "Use software from the \"multiverse\" component?"
msgstr "Ĉu uzi programaro el 'multiverse' komponento?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:3001
msgid ""
"Some non-free software is available in packaged form. Though this software "
"is not a part of the main distribution, standard package management tools "
"can be used to install it. This software has varying licenses and (in some "
"cases) patent restrictions which may prevent you from using, modifying, or "
"sharing it."
msgstr ""
"Kelkaj malliberaj programoj pretas en pakformoj. Kvankam tio programaro ne "
"apartenas al la ĉefa eldono, kutimaj pakaj administriloj uzeblus por instali "
"ĝin. Tio programaro posedas diversajn uzrajtojn kiuj malebligas vin uzi, "
"modifi, aŭ disdoni ĝin."

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:4001
msgid "Use backported software?"
msgstr "Ĉu uzi retroportan programaron?"

#. Type: boolean
#. Description
#. This template is used by the Ubuntu version of d-i.
#. :sl2:
#: ../apt-mirror-setup.templates-ubuntu:4001
msgid ""
"Some software has been backported from the development tree to work with "
"this release. Although this software has not gone through such complete "
"testing as that contained in the release, it includes newer versions of some "
"applications which may provide useful features."
msgstr ""
"Kelkaj programoj estas retroportitaj el la programada arbo por labori kun "
"tiu distribuo. Kvankam tia programaro ne estis testita samzorge kiel, tiu de "
"la distribuo ĝi povas enteni novajn pragramajn versiojn enhavantajn utilajn "
"apartaĵojn."
