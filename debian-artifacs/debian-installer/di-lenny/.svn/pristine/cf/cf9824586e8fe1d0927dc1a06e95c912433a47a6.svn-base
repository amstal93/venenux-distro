<!-- retain these comments for translator revision tracking -->
<!-- original version: 56145 untranslated -->

 <sect1 condition="gtk" id="graphical">
 <title>The Graphical Installer</title>
<para>

The graphical version of the installer is only available for a limited
number of architectures, including &arch-title;. The functionality of
the graphical installer is essentially the same as that of the regular
installer as it basically uses the same programs, but with a different
frontend.

</para><para>

Although the functionality is identical, the graphical installer still has
a few significant advantages. The main advantage is that it supports more
languages, namely those that use a character set that cannot be displayed
with the regular <quote>newt</quote> frontend. It also has a few usability
advantages such as the option to use a mouse, and in some cases several
questions can be displayed on a single screen.

</para><para arch="x86">

The graphical installer is available with all CD images and with the
hd-media installation method. To boot the graphical installer simply select
the relevant option from the boot menu. Expert and rescue mode for the
graphical installer can be selected from the <quote>Advanced options</quote>
menu. The previously used boot methods <userinput>installgui</userinput>,
<userinput>expertgui</userinput> and <userinput>rescuegui</userinput> can
still be used from the boot prompt which is shown after selecting the
<quote>Help</quote> option in the boot menu.

</para><para arch="x86">

There is also a graphical installer image that can be netbooted. And there
is a special <quote>mini</quote> ISO image<footnote id="gtk-miniiso">

<para>
The mini ISO image can be downloaded from a Debian mirror as described
in <xref linkend="downloading-files"/>.
Look for <filename>netboot/gtk/mini.iso</filename>.
</para>

</footnote>, which is mainly useful for testing.

</para><para arch="powerpc">

For &arch-title;, currently only an experimental <quote>mini</quote> ISO
image is available<footnote id="gtk-miniiso">

<para>
The mini ISO image can be downloaded from a Debian mirror as described
in <xref linkend="downloading-files"/>.
Look for <filename>netboot/gtk/mini.iso</filename>.
</para>

</footnote>. It should work on almost all PowerPC systems that have
an ATI graphical card, but is unlikely to work on other systems.

</para><para>

Just as with the regular installer it is possible to add boot parameters
when starting the graphical installer. One of those parameters allows to
configure the mouse for left-handed use. Others allow to select the mouse
device (e.g. for a serial mouse) and the mouse protocol. See
<xref linkend="boot-parms"/> for valid parameters<phrase arch="x86"> and
<xref linkend="boot-screen"/> for information on how to pass them</phrase>.

</para>
<note><para>

The graphical installer requires significantly more memory to run than
the regular installer: &minimum-memory-gtk;. If insufficient memory is
available, it will automatically fall back to the regular
<quote>newt</quote> frontend.

</para><para>

If the amount of memory in your system is below &minimum-memory;,
the graphical installer may fail to boot at all while booting the
regular installer would still work. Using the regular installer is
recommended for systems with little available memory.

</para></note>

  <sect2 id="gtk-using">
  <title>Using the graphical installer</title>
<para>

As already mentioned, the graphical installer basically works the same as
the regular installer and thus the rest of this manual can be used to guide
you through the installation process.

</para><para>

If you prefer using the keyboard over the mouse, there are two things you
need to know. To expand a collapsed list (used for example for the selection
of countries within continents), you can use the <keycap>+</keycap> and
<keycap>-</keycap> keys. For questions where more than one item can be
selected (e.g. task selection), you first need to tab to the
&BTN-CONT; button after making your selections; hitting
enter will toggle a selection, not activate &BTN-CONT;.

</para><para>

To switch to another console, you will also need to use the
<keycap>Ctrl</keycap> key, just as with the X Window System. For example,
to switch to VT2 (the first debug shell) you would use: <keycombo>
<keycap>Ctrl</keycap> <keycap>Left Alt</keycap> <keycap>F2</keycap>
</keycombo>. The graphical installer itself runs on VT5, so you can use
<keycombo> <keycap>Left Alt</keycap> <keycap>F5</keycap> </keycombo>
to switch back.

</para>
  </sect2>

  <sect2 id="gtk-issues">
  <title>Known issues</title>
<para>

The graphical frontend to the installer is relatively new and because of
that there are some known issues. We continue to work on resolving these.

</para>

<itemizedlist>
<listitem><para>

Information on some screens is not yet nicely formatted into columns as it
should be.

</para></listitem>
<listitem><para>

Support for touchpads is not yet optimal.

</para></listitem>
</itemizedlist>

  </sect2>
 </sect1>
