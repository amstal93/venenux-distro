<!-- retain these comments for translator revision tracking -->
<!-- original version: 43789 -->

 <sect2 arch="alpha" id="alpha-firmware">
 <title>Consola del microprogramari per Alpha</title>

<para>

El microprogramari de la consola s'emmagatzema a la memòria flaix ROM i
s'inicia quan el sistema Alpha arrenca o es reinicia. Hi ha dues
especificacions distintes a les consoles dels sistemes Alpha, d'aquí
que que hi hagin dues classes de microprogramari:

</para>

<itemizedlist>
<listitem><para>

  La <emphasis>consola SRM</emphasis>, que es basa en l'especificació Alpha
  Console Subsystem, permet tenir entorns operatius pels sistemes OpenVMS,
  Tru64 Unix i Linux.

</para></listitem>
<listitem><para>

  Les <emphasis>consoles ARC, AlphaBIOS o ARCBIOS</emphasis>, que es basen
  en l'especificació Advanced RISC Computing (ARC), permeten entorns
  operatius per Windows NT.

</para></listitem>
</itemizedlist>

<para>

Des del punt de vista de l'usuari la diferència més important entre SRM i
ARC és que l'elecció de consola limita les possibilitats de l'esquema de
particions del disc dur des de on voleu arrencar.

</para><para>

Amb ARC haureu d'usar una taula de particions MS-DOS (creada amb l'ordre
<command>cfdisk</command>). Això és perquè les taules de particions de
MS-DOS són el format <quote>natiu</quote> per arrencar des d'ARC. En
realitat, com que AlphaBIOS té una utilitat per fer particions de disc,
potser preferiu fer les particions des del menú del microprogramari abans
d'instal·lar Linux.

</para><para>

En canvi SRM és <emphasis>incompatible</emphasis><footnote>

<para>
Concretament, el format pel sector d'inici que és necessari pel «Console
Subsystem Specification» entra en conflicte amb la posició de les
particions DOS.
</para>

</footnote> amb les taules de partició de MS-DOS. Com que Tru64 Unix usa
el format de particions de BSD aquest és el format <quote>natiu</quote>
per instal·lacions de SRM.

</para><para>

GNU/Linux és l'únic sistema operatiu sobre Alpha que es pot arrencar des
dels dos tipus de consoles, però &debian; &release; sols suporta els
sistemes basats en SRM. Si teniu un Alpha que no té versió per SRM haureu
de deixar una arrencada dual amb Windows NT, o si el vostre dispositiu
d'arrencada demana una consola ARC per l'inici de la BIOS no podreu usar
l'instal·lador de &debian; &release;. Encara podeu usar &debian; &release;
a aquests sistemes usant altres mitjans per la instal·lació; per exemple,
podeu instal·lar Debian Woody amb MILO i actualitzar.

</para><para>

Donat que <command>MILO</command> no està disponible per cap dels
sistemes Alpha actualment en producció (a febrer del 2000) i donat que ja
no és necessari que compreu una llicència OpenVMS o Tru64 Unix per a tenir
el microprogramari SRM en el vostre antic Alpha, es recomana que useu SRM
sempre que sigui possible.

</para><para>

La taula següent resumeix els sistemes disponibles i suportats segons la
combinació de tipus/consola (consulteu <xref linkend="alpha-cpus"/> per
veure els noms dels tipus de sistema). La paraula <quote>ARC</quote>
assenyala les consoles compatibles amb ARC.

</para><para>

<informaltable><tgroup cols="2">
<thead>
<row>
  <entry>Tipus de sistema</entry>
  <entry>Tipus de consola suportada</entry>
</row>
</thead>

<tbody>
<row>
  <entry>alcor</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>avanti</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>book1</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>cabriolet</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>dp264</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>eb164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb64p</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb66</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>eb66p</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>jensen</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>lx164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>miata</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>mikasa</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>mikasa-p</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>nautilus</entry>
  <entry>ARC (vegeu el manual de la placa mare) o SRM</entry>
</row><row>
  <entry>noname</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>noritake</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>noritake-p</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>pc164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>rawhide</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>ruffian</entry>
  <entry>només ARC</entry>
</row><row>
  <entry>sable</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>sable-g</entry>
  <entry>només SRM</entry>
</row><row>
  <entry>sx164</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>takara</entry>
  <entry>ARC o SRM</entry>
</row><row>
  <entry>xl</entry>
  <entry>només ARC</entry>
</row><row>
  <entry>xlt</entry>
  <entry>només ARC</entry>
</row>

</tbody>
</tgroup>
</informaltable>

</para><para>

Per regla general cap d'aquestes consoles pot arrencar Linux directament
per la qual cosa es fa necessari un carregadora que faci d'intermediari. Per
les consoles SRM s'usa <command>aboot</command>, un petit carregador amb
independència de plataforma. Consulteu el (malauradament desactualitzat)
<ulink url="&url-srm-howto;">SRM COM ES FA</ulink> per més informació
sobre <command>aboot</command>.

</para><para condition="FIXME">

Els següents paràgrafs corresponen al manual d'instal·lació de Woody i
els incloem aquí per servir de referència; potser seran útils per algú
més endavant quan Debian suporti de nou instal·lacions basades en MILO.

</para><para condition="FIXME">

Generalment cap d'aquestes consoles poden arrencar directament des de
Linux, doncs necessiten l'ajuda d'un carregador d'arrencada com a
intermediari. Aquests són els dos carregadors disponibles en Linux:
<command>MILO</command> i <command>aboot</command>.

</para><para condition="FIXME">

<command>MILO</command> és una consola en si mateix que substitueix a ARC
o SRM en memòria. <command>MILO</command> pot carregar-se tant des de
ARC com SRM i és l'únic mètode per a arrencar des d'una consola ARC.
<command>MILO</command> és específic de la plataforma (es necessari un
<command>MILO</command> diferent per a cadascun dels tipus de sistema
suportats) i n'hi ha només un per a cada sistema, el suport ARC es mostra
en la taula de més a dalt. Mireu també el (malauradament
desactualitzat) <ulink url="&url-milo-howto;">MILO COM ES FA</ulink>.

</para><para condition="FIXME">

<command>aboot</command> és un carregador petit independent de la
plataforma, que s'executa només des de SRM. Mireu el (malauradament
desactualitzat) <ulink url="&url-srm-howto;">SRM COM ES FA</ulink> per a
més informació de <command>aboot</command>.

</para><para condition="FIXME">

Tan és així que hi han tres escenaris possibles depenent del
microprogramari de la consola i de si <command>MILO</command> està o
no disponible:

<informalexample><screen>
SRM -&gt; aboot
SRM -&gt; MILO
ARC -&gt; MILO
</screen></informalexample>

Donat que <command>MILO</command> no està disponible per cap dels
sistemes Alpha actualment en producció (a febrer del 2000) i donat que ja
no és necessari que compreu una llicència OpenVMS o Tru64 Unix per a tenir
el microprogramari SRM en el vostre antic Alpha, es recomana que useu SRM
i <command>aboot</command> en les noves instal·lacions de GNU/Linux a
menys que desitgeu tenir una arrencada dual amb Windows NT.

</para><para>

La majoria dels productes AlphaServers i tots els servidors corrents tal
com les estacions de treball contenen tant SRM com AlphaBIOS en el seu
microprogramari. Per a màquines <quote>half-flash</quote> com les diverses
plaques d'avaluació, és possible intercanviar entre una versió i l'altre
reflaixejant el microprogramari. També és possible, una vegada instal·lat
SRM, executar ARC/AlphaBIOS des d'un disquet (usant l'ordre
<command>arc</command>). Pels motius mencionats a dalt, es recomana que
commuteu a SRM abans d'instal·lar Debian.

</para><para>

Com en d'altres arquitectures, hauríeu d'instal·lar la revisió més nova
disponible en el microprogramari<footnote>

<para>
A excepció de Jensen, on Linux no té suport a les versions de microprogramari
majors que la 1.7 &mdash; consulteu <ulink url="&url-jensen-howto;"></ulink>
per més informació.
</para>

</footnote> abans d'instal·lar &debian;. Podeu trobar actualitzacions de
microprogramari per Alpha des de <ulink
url="&url-alpha-firmware;">Actualitzacions de microprogramari per Alpha</ulink>.

</para>
 </sect2>


  <sect2 arch="alpha"><title>Arrencada amb el TFTP</title>
<para>

A SRM les interfícies Ethernet s'anomenen amb el prefixe
<userinput>ewa</userinput> i es mostren com a sortida de l'ordre
<userinput>show dev</userinput>, com per exemple:

<informalexample><screen>
&gt;&gt;&gt; show dev
ewa0.0.0.9.0               EWA0              08-00-2B-86-98-65
ewb0.0.0.11.0              EWB0              08-00-2B-86-98-54
ewc0.0.0.2002.0            EWC0              00-06-2B-01-32-B0
</screen></informalexample>

Primerament necessitareu determinar el protocol d'arrencada:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_protocols bootp
</screen></informalexample>

Llavors comprovar que el tipus de medi és correcte:

<informalexample><screen>
&gt;&gt;&gt; set ewa0_mode <replaceable>mode</replaceable>
</screen></informalexample>

Podreu aconseguir un llistat dels modes vàlids amb
<userinput>&gt;&gt;&gt;set ewa0_mode</userinput>.

</para><para>

Llavors, per a arrencar des de la primera interfície Ethernet, escriviu:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags ""
</screen></informalexample>

Això arrencarà usant els paràmetres per defecte del nucli tal i com
s'inclou a la imatge de xarxa.

</para><para>

Si desitgeu usar la consola sèrie, <emphasis>haureu</emphasis> de passar
el paràmetre <userinput>console=</userinput> al nucli. Podeu fer-ho usant
l'argument <userinput>-flags</userinput> per a l'ordre SRM
<userinput>boot</userinput>. Els ports sèrie s'anomenaran com als seus
fitxers corresponents en <userinput>/dev</userinput>. Per exemple, per a
arrencar des de <userinput>ewa0</userinput> i usar la consola en el
primer port sèrie, hauríeu d'escriure:

<informalexample><screen>
&gt;&gt;&gt; boot ewa0 -flags &quot;root=/dev/ram ramdisk_size=16384 console=ttyS0&quot;
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha"><title>Arrencada des del CD-ROM des d'una consola SRM</title>

<para>

Els CD d'instal·lació de &debian; inclouen algunes opcions d'arrencada
preconfigurades pels terminals sèrie i VGA. Escriviu

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 0
</screen></informalexample>

per arrencar utilitzant la consola VGA, on <replaceable>xxxx</replaceable>
és la vostra unitat de CD-ROM en la notació SRM. Per utilitzar una consola
sèrie al primer dispositiu sèrie, escriviu

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 1
</screen></informalexample>

i per la consola en el segon port sèrie, escriviu

<informalexample><screen>
&gt;&gt;&gt; boot xxxx -flags 2
</screen></informalexample>

</para>
  </sect2>

  <sect2 arch="alpha" condition="FIXME">
  <title>Arrencada des d'un CD-ROM des de la consola ARC o AlphaBIOS</title>
<para>

Per a arrencar des d'un CD-ROM des d'una consola ARC console, cerqueu el
codi de la subarquitectura (mireu <xref linkend="alpha-cpus"/>), i escriviu
<filename>\milo\linload.exe</filename> pel carregador d'arrencada i
<filename>\milo\<replaceable>subarqui</replaceable></filename> (a on
<replaceable>subarqui</replaceable> serà el nom de la subarquitectura) com
la ruta del SO en el menú «OS Selection Setup». Una excepció per Ruffians:
Necessitareu usar <filename>\milo\ldmilo.exe</filename> com a carregador
d'arrencada.

</para>
  </sect2>

  <sect2 arch="alpha" condition="supports-floppy-boot">5A
  <title>Arrencada des de disquets en la consola SRM</title>
<para>

En l'indicatiu de SRM (<prompt>&gt;&gt;&gt;</prompt>), useu la següent
ordre:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -flags 0
</screen></informalexample>

possiblement canviant <filename>dva0</filename> per l'actual nom de
dispositiu. Normalment, <filename>dva0</filename> serà el disquet;
escriviu

<informalexample><screen>
&gt;&gt;&gt; show dev
</screen></informalexample>

per a veure una llista dels dispositius (p.ex. si voleu arrencar des del CD).
Noteu que si arrenqueu des de Milo l'argument <command>-flags</command>
s'ignorarà, així que podreu escriure <command>boot dva0</command>. Si tot
funciona correctament, podreu veure eventualment arrencar el nucli Linux.

</para><para>

Si voleu especificar els paràmetres del nucli mitjançant
<command>aboot</command>, useu l'ordre següent:

<informalexample><screen>
&gt;&gt;&gt; boot dva0 -file linux.bin.gz -flags "root=/dev/fd0 load_ramdisk=1 arguments"
</screen></informalexample>

(escrit en una sola línia), substituint si fos necessari el nom de
l'actual dispositiu d'arrencada SRM per <filename>dva0</filename>, el nom
del dispositiu d'arrencada Linux per <filename>fd0</filename> i els
paràmetres desitjats per al nucli en <filename>arguments</filename>.

</para><para>

Si voleu especificar paràmetres al nucli quan arrenqueu mitjançant
<command>MILO</command>, segurament haureu d'interrompre al carregador
un cop aquest arribi a MILO. Consulteu <xref linkend="booting-from-milo"/>.

</para>
  </sect2>

  <sect2 arch="alpha" condition="supports-floppy-boot">
  <title>Arrencada des de disquets des de la consola ARC o AlphaBIOS</title>

<para>

En el menú de selecció del sistema operatiu, escolliu
<command>linload.exe</command> com el carregador d'arrencada i
<command>milo</command> com a ruta del sistema operatiu. El carregador
usarà la nova entrada creada.

</para>
  </sect2>

 <sect2 arch="alpha" condition="FIXME" id="booting-from-milo"><title>
   Arrencada amb MILO</title>
<para>

El MILO que es troba en el medi carregador d'arrencada està configurat
per a procedir directament cap a Linux. Si desitgeu intervenir, tot el
que heu de fer es prémer espai durant el compte enredere de MILO.

</para><para>

Si voleu especificar tots els bits (per exemple, amb paràmetres
addicionals), podeu usar una ordre com aquesta:

<informalexample><screen>
MILO> boot fd0:linux.bin.gz root=/dev/fd0 load_ramdisk=1 <!-- arguments -->
</screen></informalexample>

Si esteu arrencant des de quelcom diferent a disquets, substituïu
<filename>fd0</filename> en l'exemple anterior pel nom del dispositiu
apropiat en la notació Linux. L'ordre <command>help</command> us donarà
un resum de referència sobre les ordres a MILO.

</para>
 </sect2>
