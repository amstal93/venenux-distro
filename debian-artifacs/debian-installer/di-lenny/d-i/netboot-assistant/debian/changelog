di-netboot-assistant (0.36b) unreleased; urgency=low

  * New SVN repository.

  * Upload sponsored by Petter Reinholdtsen.

 -- Frank Lin PIAT <fpiat@klabs.be>  Sun, 05 Oct 2008 20:21:42 +0200

di-netboot-assistant (0.36a) unreleased; urgency=low

  * fix incorrect location of elilo.conf top menu, when
    an alternate extract location is specified.
  * Invoke curl with "--fail" to actually detect errors. Closes: #493664,
    Thanks to Chris Lamb for the patch.
  * Upgrade pxelinux.0 when the installed image has a newer one
    (Fix bug reported by Frans Pop).

  * fix lintian errors (debian-watch-file-in-native-package and
    debian-copyright-line-too-long).
  * Remove duplicate "MENU TITLE" in pxelinux.HEAD. Closes: #493549,
    Thanks to Chris Lamb for the patch.
  * Correct strange "( release , arch )" spacing in menu. Closes: #493544,
    Thanks to Chris Lamb for the patch.
  * Correct typo in "missing checksum" output message. Closes: #493428,
    Thanks to Chris Lamb.
  * Correct title of manpage. Closes: #493412,
    Thanks to Chris Lamb.
  * Document --rebuild-menu option, implemented in 0.36.
  * Change maintainer email fpiat@bigfoot.com -> fpiat@klabs.be

  * Upload sponsored by Petter Reinholdtsen.

 -- Frank Lin PIAT <fpiat@klabs.be>  Wed, 23 Jul 2008 08:20:13 +0200

di-netboot-assistant (0.36) unstable; urgency=low

  * Implement the command rebuild-menu.
  * Add dnsmasq as alternate dhcp and tftpd dependency (as suggested
    by Pietro Abate).
  * Document how to use dnsmasq.
  * Document that pxelinux.pathprefix="/" and that passing some
    DHCP options are mandatory (bug reported by Frans Pop).
  * Clarify error message when syslinux/elilo aren't installed.
  * Add IA64 overview menu in examples/dhcpd.conf.multiarch
  * Add example dhcpd.conf.simple.

  * Upload sponsored by Petter Reinholdtsen.

 -- Franklin Piat <fpiat@bigfoot.com>  Sun, 13 Jul 2008 19:32:50 +0200

di-netboot-assistant (0.35) unstable; urgency=low

  * Implement MD5SUMS checkum checking (requested by Joey Hess).
  * Change the way unreachable mirrors are handled.
  * Fix path in pxelinux.HEAD

  * Upload sponsored by Petter Reinholdtsen.

 -- Franklin Piat <fpiat@bigfoot.com>  Fri, 11 Jul 2008 00:26:10 +0200

di-netboot-assistant (0.34) unstable; urgency=low

  * Add an option to choose the mirror (actually, rewrite canonical URL).
  * Add etch-gtk, lenny-gtk entries, etc..
  * Use the mirror ftp.de.debian.org by default (it has all archs !).
  * Fix location of images in di-sources.list (drop /debian prefix)
  * Re-add curl as alternate dependency to wget (was removed by mistake)
  * Add elilo as recommend dependency.

 -- Franklin Piat <fpiat@bigfoot.com>  Tue, 08 Jul 2008 09:51:35 +0200

di-netboot-assistant (0.33) unstable; urgency=low

  * Fix location of sparc images in di-sources.list
  * Multiple improvements in the documenations, manpage, 
    examples (thanks to Joey Hess).
  * Don't load french keymap (thanks to Joey Hess).
  * Don't use french mirrors in di-sources.list by default.
  * Add some wishlist from Joey Hess to the TODO list.

 -- Franklin Piat <fpiat@bigfoot.com>  Tue, 08 Jul 2008 08:10:42 +0200

di-netboot-assistant (0.32) unstable; urgency=low

  * Policy version 3.8.0 (bumped in previous release): no changes required.
  * ITP (closes: #489812).

 -- Franklin Piat <fpiat@bigfoot.com>  Tue, 08 Jul 2008 00:22:45 +0200

di-netboot-assistant (0.31) unstable; urgency=low

  * Policy version 3.7.3: no changes required.
  * Add Homepage: header to source package.
  * Update manpage and README.Debian.

 -- Franklin Piat <fpiat@bigfoot.com>  Mon, 09 Jun 2008 23:47:59 +0200

di-netboot-assistant (0.30) unstable; urgency=low

  * No upgrade from pre 0.30 : purge, then install.
  * Support downloading/unpacking alpha, ia64, hppa and sparc.
  * Support IA64's elilo (update file path in elilo.conf).
  * Improve dhcpd.conf example file.
  * Add vim syntax file for di-sources.list (disources.vim).
  * Dependency changed to [wget|curl].
  * Invoking "purge" without repository lists currently installed.
  * Don't build empty pxelinux.menu file (for ia64...)
  * Renamed pxelinux.cfg files renamed pxelinux.cfg/default
  * Renamed configuration file pxelinux.cfg.HEAD as pxelinux.HEAD

 -- Franklin Piat <fpiat@bigfoot.com>  Tue, 03 Jun 2008 01:18:00 +0200

di-netboot-assistant (0.21) unstable; urgency=low

  * Add package description.
  * Remove dummy prerm.
  * Fix debian/control's Maintainer name case.
  * Improve README.
  * cleanup /etc/di-netboot-assistant/pxelinux.cfg.HEAD

 -- Franklin Piat <fpiat@bigfoot.com>  Mon, 02 Jun 2008 23:51:56 +0200

di-netboot-assistant (0.20) unstable; urgency=low

  * Save information about downloaded repositories.

 -- Franklin Piat <fpiat@bigfoot.com>  Fri, 16 May 2008 08:04:22 +0200

di-netboot-assistant (0.19) unstable; urgency=low

  * Package rename.
  * Print warning if no argument is passed.
  * Add dhcpd.conf example (untested!)

 -- Franklin PIAT <fpiat@bigfoot.com>  Mon, 05 May 2008 02:14:20 +0200

dibas (0.18) unstable; urgency=low

  * Initial Public Release.

 -- Franklin PIAT <fpiat@bigfoot.com>  Sun, 04 May 2008 12:42:15 +0200
