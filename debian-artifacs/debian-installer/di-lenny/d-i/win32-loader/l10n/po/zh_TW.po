# Traditional Chinese translation for win32-loader
# Copyright (C) 2008 Tetralet 
# This file is distributed under the same license as the win32-loader package.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader 0.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-08-09 02:28+0800\n"
"Last-Translator: Tetralet <tetralet@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_TRADCHINESE"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "big5"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp950"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Chinese (Traditional)"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian 安裝程式"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "TradChinese.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Debian 安裝程式載入器"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "找不到 win32-loader.ini"

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini 不完整。請聯繫此檔案的提供者。"

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr "本程式偵測到您的鍵盤類型是「$0」。這是否正確？"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"請發送包含了以下資訊的錯誤回報：\n"
"\n"
" - Windows 的版本。\n"
" - 國家設定。\n"
" - 鍵盤的實際類型。\n"
" - 偵測到的鍵盤類型。\n"
"\n"
"謝謝您。"

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"在裝置 $c 中似乎沒有足夠的未使用磁碟空間。為了要安裝完整的桌面環境，建議至少"
"要有 3 GB 的空間。如果您已準備了個別的磁碟或分割區以用來安裝 Debian，或者您是"
"打算要完全取代 Windows，您可以放心得忽略此警告訊息。"

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "錯誤：沒有足夠的未使用磁碟空間。安裝中止。"

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "本程式尚未支援 Windows $windows_version。"

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"您正試圖安裝的 Debian 版本是為了能在新一代 64 位元的電腦上運作而設計的。但"
"是，您的電腦並無法執行 64 位元的程式。\n"
"\n"
"請使用 32 位元 (i386) 版本的 Debian，或者是可以安裝任意版本的多架構版本。\n"
"\n"
"本安裝程式將立即中止。"

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"您的電腦可以運行新一代的 64 位元作業系統。但是，您正試圖安裝的 Debian 版本是"
"為較舊的 32 位元硬體而設計的。\n"
"\n"
"您依然可以繼續安裝，但為了能讓您的電腦發揮最大的效益，我們建議您替而使用 64 "
"位元(amd64) 版本的 Debian，或者是可以安裝任意版本的多架構版本。\n"
"\n"
"您是否想要立即中止安裝？"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "選擇安裝模式："

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "標準模式。建議一般使用者採用。"

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr "進階模式。建議給那些想完全掌控安裝程序的進階使用者採用。"

#: win32-loader.c:84
msgid "Select action:"
msgstr "採取動作："

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "在這台電腦上安裝 Debian GNU/Linux。"

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "修復現有的 Debian 系統 (救援模式)。"

#: win32-loader.c:88
msgid "Graphical install"
msgstr "以圖形模式安裝"

#: win32-loader.c:89
msgid "Text install"
msgstr "以文字模式安裝"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "正在下載 %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "正在連線 ..."

#: win32-loader.c:92
msgid "second"
msgstr "秒"

#: win32-loader.c:93
msgid "minute"
msgstr "分鐘"

#: win32-loader.c:94
msgid "hour"
msgstr "小時"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr " "

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dkB (%d%%) 共 %dkB 速度 %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (還剩 %d %s%s)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "請選擇要使用哪個版本的 Debian 安裝程式："

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "穩定版。這將安裝 Debian「stable」。"

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr ""
"每日編譯版。這是 Debian 安裝程式的開發版。它預設是安裝 Debian「testing」，但"
"也能夠用來安裝「stable」或「unstable」。"

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr "在此建議您，請在使用每日編譯版之前先查看其已知問題。是否立即查看？"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "桌面環境："

#: win32-loader.c:114
msgid "None"
msgstr "無"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr ""
"Debian 安裝程式載入器將會設定為使用以下參數。除非有絕對把握，請[不要]對他們進"
"行任何修改。"

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Proxy 設定 (主機:埠)："

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "boot.ini 的位置："

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "網路開機影像檔 (linux 及 initrd.gz) 的基本網址："

#: win32-loader.c:119
msgid "Error"
msgstr "錯誤"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "錯誤：無法將 $0 複製到 $1。"

#: win32-loader.c:121
msgid "Generating $0"
msgstr "正在產生 $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "將預先設定資訊附加到 $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "錯誤：無法執行 $0。"

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "正在關閉開機檔案裡的 NTFS 壓縮設定"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "正在將 Debian 安裝程式註冊到 NTLDR 裡"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "正在將 Debian 安裝程式註冊到 BootMgr 裡"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "錯誤：無法分析 bcdedit.exe 的輸出資訊。"

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "錯誤：無法找到 boot.ini。這真的是 Windows $windows_version 嗎？"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "【非常重要的提示】：\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr ""
"第二階段的安裝程序現在即將開始。在您確認之後，本程式將會重新啟動 Windows 至 "
"DOS 模式，並自動載入 Debian 安裝程式。\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr ""
"您需要重新開機來繼續進行 Debian 的安裝作業。在您下次開機時，您將會被詢問要啟"
"動的是 Windows 或者 Debian 安裝程式。請選擇 Debian 安裝程式來繼續安裝程序。"
"\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"在安裝過程中，您將可以選擇是要縮減您的 Windows 分割區來安裝 Debian 或是將它完"
"全替換掉。不管您的選擇如何，[強烈建議]您應當事先備份好您的資料。本載入器的作"
"者和 Debian 專案對於資料遺失事件[不提供任何擔保]。\\n\\n等到您的 Debian 安裝"
"完成（而同時您也選擇了在您的磁碟裡保留 Windows），您可以利用 Windows 控制台中"
"的 新增/移除程式 對話盒來移除 Debian 安裝程式載入器。"

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "是否要立即重新開機？"

#~ msgid "Debconf preseed line:"
#~ msgstr "Debconf 預先設定列："
