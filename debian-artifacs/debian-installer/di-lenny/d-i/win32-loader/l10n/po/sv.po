# Copyright (C) 2007  Daniel Nylander <po@danielnylander.se>
# Copyright (C) 2008  Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the win32-loader package.
#
msgid ""
msgstr ""
"Project-Id-Version: win32-loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-01 10:40+0200\n"
"PO-Revision-Date: 2008-07-19 01:45+0100\n"
"Last-Translator: Martin Bagge <brother@bsnet.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36
#: win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_SWEDISH"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1252"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp850"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Swedish"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian-installerare"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Swedish.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "Inläsare för Debian-installerare"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "Kan inte hitta win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "win32-loader.ini är inte fullständig.  Kontakta leverantören av detta media."

#: win32-loader.c:74
msgid "This program has detected that your keyboard type is \"$0\".  Is this correct?"
msgstr "Det här programmet har identifierat din tangentbordstyp som \"$0\".  Är det korrekt?"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"Skicka in en felrapport med följande information:\n"
"\n"
" - Version av Windows.\n"
" - Landsinställningar.\n"
" - Verklig tangentbordstyp.\n"
" - Identifierad tangentbordstyp.\n"
"\n"
"Tack."

#: win32-loader.c:76
msgid "There doesn't seem to be enough free disk space in drive $c.  For a complete desktop install, it is recommended to have at least 3 GB.  If there is already a separate disk or partition to install Debian, or if you plan to replace Windows completely, you can safely ignore this warning."
msgstr "Det verkar inte som om du har tillräckligt ledigt diskutrymme på enheten $c.  Det rekommenderas att du har åtminstone 3 GB ledigt diskutrymme för en komplett skrivbordsinstallation.  Om du redan har en separat disk eller partition för att installera Debian på, eller så kan du ignorera den här varningen om du planerar att helt ersätta Windows."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "Fel: inte tillräckligt med ledigt diskutrymme.  Avbryter installationen."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "Det här programmet har inte stöd för Windows $windows_version än."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, 64-bit computers.  However, your computer is incapable of running 64-bit programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"Versionen av Debian som du försöker att installera är designad att köra på moderna 64-bitars datorer.  Tyvärr kan din dator inte köra 64-bitars program.\n"
"\n"
"Använd 32-bitars (\"i386\") versionen av Debian, eller \"Multi-arch\"-versionen som kan installera någon av de båda.\n"
"\n"
"Installationen kommer att avbrytas nu."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  However, the version of Debian you're trying to install is designed to run on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most advantage of your computer, we recommend that you use the 64-bit (\"amd64\") version of Debian instead, or the Multi-arch version which is able to install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"Din dator är kapabel att köra moderna 64-bitars operativsystem.  Dock är den version av Debian som du försöker att installera designad att köra på äldre 32-bitars maskinvara.\n"
"\n"
"Du kan dock fortsätta med installationen men vi rekommenderar att du använder 64-bitars (\"amd64\") versionen av Debian för att dra nytta av maskinvaran i din dator, eller \"Multi-arch\"-versionen som kan installera någon av de båda.\n"
"\n"
"Vill du avbryta nu?"

#: win32-loader.c:81
#: win32-loader.c:87
msgid "Select install mode:"
msgstr "Välj installationsläge:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "Normalläge.  Rekommenderat för de flesta användare."

#: win32-loader.c:83
msgid "Expert mode.  Recommended for expert users who want full control of the install process."
msgstr "Expertläge.  Rekommenderat för erfarna användare som vill ha full kontroll över installationsprocessen."

#: win32-loader.c:84
msgid "Select action:"
msgstr "Välj åtgärd:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "Installera Debian GNU/Linux på denna dator."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "Reparera ett befintligt Debian-system (räddningsläge)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "Grafisk installerare"

#: win32-loader.c:89
msgid "Text install"
msgstr "Textbaserad installerare"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "Hämtar %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "Ansluter ..."

#: win32-loader.c:92
msgid "second"
msgstr "sekund"

#: win32-loader.c:93
msgid "minute"
msgstr "minut"

#: win32-loader.c:94
msgid "hour"
msgstr "timme"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "s"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%d kB (%d%%) av %d kB i %d.%01dkB/s"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s återstår)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "Välj vilken version av Debian-Installer som ska användas:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "Stabil utgåva.  Det här kommer att installera Debian \"stable\"."

#: win32-loader.c:107
msgid "Daily build.  This is the development version of Debian-Installer.  It will install Debian \"testing\" by default, and may be capable of installing \"stable\" or \"unstable\" as well."
msgstr "Dagligt bygge.  Det här är utvecklingsversionen av Debian-Installer.  Det kommer att installera Debian \"testing\" som standard och kan även installera \"stable\" eller \"unstable\"."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid "It is recommended that you check for known issues before using a daily build.  Would you like to do that now?"
msgstr "Det rekommenderas att du letar upp information om kända problem innan du använder ett dagligt bygge.  Vill du göra det nu?"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "Skrivbordsmiljö:"

#: win32-loader.c:114
msgid "None"
msgstr "Ingen"

#: win32-loader.c:115
msgid "Debian-Installer Loader will be setup with the following parameters.  Do NOT change any of these unless you know what you're doing."
msgstr "Inläsare för Debian-installerare kommer att konfigureras med följande parametrar.  Ändra INTE någon av dessa såvida du inte vet vad du gör."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "Proxyinställningar (värdnamn:port):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "Plats för boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "Bas-URL för nätuppstartsavbilder (linux och initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "Fel"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "Fel: misslyckades med att kopiera $0 till $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "Skapar $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "Applicerar information från förinställningar för $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "Fel: kunde inte köra $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "Stänger av NTFS-komprimering i bootstrap-filer."

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "Lägger till Debian-installerare i NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "Lägger till Debian-installerare i BootMgr."

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "Fel: misslyckades med att tolka utdata från bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "Fel: boot.ini hittades inte.  Är det här verkligen Windows $windows_version?"

#: win32-loader.c:129
msgid ""
"VERY IMPORTANT NOTICE:\\n"
"\\n"
msgstr ""
"MYCKET VIKTIG NOTERING:\\n"
"\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your confirmation, this program will restart Windows in DOS mode, and automaticaly load Debian Installer.\\n"
"\\n"
msgstr ""
"Det andra steget av den här installationsprocessen kommer nu att startas.  Efter att du har bekräftat kommer programmet att starta om Windows till DOS-läget och automatiskt läsa in Debian Installer.\\n"
"\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During your next boot, you will be asked whether you want to start Windows or Debian Installer.  Choose Debian Installer to continue with the install process.\\n"
"\\n"
msgstr ""
"Du behöver starta om för att fortsätta med din installation av Debian.  Under nästa uppstart kommer du att bli frågad huruvida du vill starta Windows eller Debian Installer.  Välj Debian Installer för att fortsätta med installationsprocessen.\\n"
"\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either reducing your Windows partition to install Debian or completely replacing it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made a backup of your data.  Nor the authors of this loader neither the Debian project will take ANY RESPONSIBILITY in the event of data loss.\\n"
"\\n"
"Once your Debian install is complete (and if you have chosen to keep Windows in your disk), you can uninstall the Debian-Installer Loader through the Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"Under installationsprocessen kommer du att få möjligheten att antingen minska din Windows-partition för att installera Debian eller ersätta den totalt.  I båda fallen är det STARKT REKOMMENDERAT att du tidigare har gjort en säkerhetskopia av ditt data.  Varken upphovsmannen av den här inläsaren eller Debian-projektet har något ANSVAR om du förlorar data.\\n"
"\\n"
"När din Debian-installation är färdig (och om du har valt att behålla Windows på din disk) kan du avinstallera Inläsare för Debian-installerare genom Windows-dialogrutan Lägg till/Ta bort program under Kontrollpanelen."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "Vill du starta om nu?"

#~ msgid "Debconf preseed line:"
#~ msgstr "Förinställningsrad för Debconf:"
#~ msgid "GNOME.  Emphasizes ease of use."
#~ msgstr "GNOME.  Enkel att använda."
#~ msgid "KDE.  Emphasizes customizability."
#~ msgstr "KDE.  Anpassningsbar."
#~ msgid "XFCE.  Emphasizes speed and low use of resources."
#~ msgstr "XFCE.  Snabb och resurssnål."

