# translation of win32-loader_l10n_po.po to Arabic
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Ossama M. Khayat <okhayat@yahoo.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: win32-loader_l10n_po\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-05-05 15:20+0200\n"
"PO-Revision-Date: 2008-09-19 17:08+0300\n"
"Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. translate:
#. This must be a valid string recognised by Nsis.  If your
#. language is not yet supported by Nsis, please translate the
#. missing Nsis part first.
#.
#: win32-loader.sh:36 win32-loader.c:39
msgid "LANG_ENGLISH"
msgstr "LANG_ARABIC"

#. translate:
#. This must be the string used by GNU iconv to represent the charset used
#. by Windows for your language.  If you don't know, check
#. [wine]/tools/wmc/lang.c, or http://www.microsoft.com/globaldev/reference/WinCP.mspx
#.
#. IMPORTANT: In the rest of this file, only the subset of UTF-8 that can be
#. converted to this charset should be used.
#: win32-loader.sh:52
msgid "windows-1252"
msgstr "windows-1256"

#. translate:
#. Charset used by NTLDR in your localised version of Windows XP.  If you
#. don't know, maybe http://en.wikipedia.org/wiki/Code_page helps.
#: win32-loader.sh:57
msgid "cp437"
msgstr "cp437"

#. translate:
#. The name of your language _in English_ (must be restricted to ascii)
#: win32-loader.sh:67
msgid "English"
msgstr "Arabic"

#. translate:
#. IMPORTANT: only the subset of UTF-8 that can be converted to NTLDR charset
#. (e.g. cp437) should be used in this string.  If you don't know which charset
#. applies, limit yourself to ascii.
#: win32-loader.sh:81
msgid "Debian Installer"
msgstr "Debian Installer"

#. translate:
#. The nlf file for your language should be found in
#. /usr/share/nsis/Contrib/Language files/
#.
#: win32-loader.c:68
msgid "English.nlf"
msgstr "Arabic.nlf"

#: win32-loader.c:71
msgid "Debian-Installer Loader"
msgstr "مُحمّل برنامج تثبيت دبيان"

#: win32-loader.c:72
msgid "Cannot find win32-loader.ini."
msgstr "تعذّر العثور على الملف win32-loader.ini."

#: win32-loader.c:73
msgid "win32-loader.ini is incomplete.  Contact the provider of this medium."
msgstr "الملف win32-loader.ini غير مكتمل. اتصل بمُزوّدك بهذا القرص."

#: win32-loader.c:74
msgid ""
"This program has detected that your keyboard type is \"$0\".  Is this "
"correct?"
msgstr "اكتشف البرنامج أن نوع لوحة المفاتيح الخاصة بك هي \"$0\". هل هذا صحيح؟"

#: win32-loader.c:75
msgid ""
"Please send a bug report with the following information:\n"
"\n"
" - Version of Windows.\n"
" - Country settings.\n"
" - Real keyboard type.\n"
" - Detected keyboard type.\n"
"\n"
"Thank you."
msgstr ""
"رجاء أرسل تقرير الخلل بالمعلومات التالية:\n"
"\n"
" - نسخة ويندوز.\n"
" - إعدادات الدولة.\n"
" - نوع لوحة المفاتيح الفعلي.\n"
" - نوع لوحة المفاتيح الذي اكتشف.\n"
"\n"
"شكراً لك."

#: win32-loader.c:76
msgid ""
"There doesn't seem to be enough free disk space in drive $c.  For a complete "
"desktop install, it is recommended to have at least 3 GB.  If there is "
"already a separate disk or partition to install Debian, or if you plan to "
"replace Windows completely, you can safely ignore this warning."
msgstr ""
"يبدو أنه ليس هناك مساحة كافية في القرص $c. "
"لكي تقوم بتثبيت كامل، يُستحسن أن يكون لديك 3 جيجا بايت على الأقل. إن كان هناك قرص أو جزء منفصل لتثبيت دبيان، أو إن كنت تنوي استبدال ويندوز بالكامل، يمكنك تجاهل هذا التحذير."

#: win32-loader.c:77
msgid "Error: not enough free disk space.  Aborting install."
msgstr "خطأ: ليست هناك مساحة كافية. جاري إلغاء التثبيت."

#: win32-loader.c:78
msgid "This program doesn't support Windows $windows_version yet."
msgstr "لا يدعم هذا البرنامج ويندوز $windows_version بعد."

#: win32-loader.c:79
msgid ""
"The version of Debian you're trying to install is designed to run on modern, "
"64-bit computers.  However, your computer is incapable of running 64-bit "
"programs.\n"
"\n"
"Use the 32-bit (\"i386\") version of Debian, or the Multi-arch version which "
"is able to install either of them.\n"
"\n"
"This installer will abort now."
msgstr ""
"نسخة دبيان التي تحاول تثبيتها مُعدّة لتعمل على حاسبات 64-بت الحديثة. غير أن حاسبك غير قادر على تشغيل برامج 64-بت.\n"
"\n"
"استخدم نسخة 32-بت (\"i386\") من دبيان، أو نسخة Multi-arch التي يمكن بها تثبيت أي من النسختين.\n"
"\n"
"سيتم إلغاء التثبيت الآن."

#: win32-loader.c:80
msgid ""
"Your computer is capable of running modern, 64-bit operating systems.  "
"However, the version of Debian you're trying to install is designed to run "
"on older, 32-bit hardware.\n"
"\n"
"You may still proceed with this install, but in order to take the most "
"advantage of your computer, we recommend that you use the 64-bit (\"amd64\") "
"version of Debian instead, or the Multi-arch version which is able to "
"install either of them.\n"
"\n"
"Would you like to abort now?"
msgstr ""
"حاسبك قادر على تشغيل أنظمة التشغيل الحديثة من طراز 64-بت. إلا أن نسخة دبيان التي تقوم بتثبيتها مُعدّة للعمل على عتاد 32-بت الأقدم.\n"
"\n"
"يمكنك الاستمرار بالتثبيت، ولكن كي للاستفادة القصوى من حاسبك، يُستحسن أن تستخدم نسخة 64-بت (\"amd64\") من دبيان، أو نسخة Multi-arch بدلاً من ذلك والتي يمكن من خلالها تثبيت أي منها.\n"
"\n"
"هل تودّ إلغاء التثبيت الآن؟"

#: win32-loader.c:81 win32-loader.c:87
msgid "Select install mode:"
msgstr "اختر وضع التثبيت:"

#: win32-loader.c:82
msgid "Normal mode.  Recommended for most users."
msgstr "الوضع العادي.  مُستحسن لمعظم المستخدمين."

#: win32-loader.c:83
msgid ""
"Expert mode.  Recommended for expert users who want full control of the "
"install process."
msgstr "وضع الخبير. مُستحسن للمستخدمين الخُبراء الذين يريدون التحكّم الكامل بعمليّة التثبيت."

#: win32-loader.c:84
msgid "Select action:"
msgstr "اختر الإجراء:"

#: win32-loader.c:85
msgid "Install Debian GNU/Linux on this computer."
msgstr "تثبيت دبيان جنو/لينكس على هذا الحاسب."

#: win32-loader.c:86
msgid "Repair an existing Debian system (rescue mode)."
msgstr "أصلح نظام دبيان موجود (وضع الإنقاذ)."

#: win32-loader.c:88
msgid "Graphical install"
msgstr "التثبيت الرسومي"

#: win32-loader.c:89
msgid "Text install"
msgstr "التثبيت النصّي"

#: win32-loader.c:90
#, c-format
msgid "Downloading %s"
msgstr "تنزيل %s"

#: win32-loader.c:91
msgid "Connecting ..."
msgstr "إجراء الاتصال ..."

#: win32-loader.c:92
msgid "second"
msgstr "ثانية"

#: win32-loader.c:93
msgid "minute"
msgstr "دقيقة"

#: win32-loader.c:94
msgid "hour"
msgstr "ساعة"

#. translate:
#. This string is appended to "second", "minute" or "hour" to make plurals.
#. I know it's quite unfortunate.  An alternate method for translating NSISdl
#. has been proposed [1] but in the meantime we'll have to cope with this.
#. [1] http://sourceforge.net/tracker/index.php?func=detail&aid=1656076&group_id=22049&atid=373087
#.
#: win32-loader.c:102
msgid "s"
msgstr "ث"

#: win32-loader.c:103
#, c-format
msgid "%dkB (%d%%) of %dkB at %d.%01dkB/s"
msgstr "%dك.ب (%d%%) of %dك.ب بسرعة %d.%01dك.ب/ث"

#: win32-loader.c:104
#, c-format
msgid " (%d %s%s remaining)"
msgstr " (%d %s%s متبقية)"

#: win32-loader.c:105
msgid "Select which version of Debian-Installer to use:"
msgstr "اختر نسخة برنامج تثبيت دبيان التي تريد استخدامها:"

#: win32-loader.c:106
msgid "Stable release.  This will install Debian \"stable\"."
msgstr "الإصدار المستقرّ. يقوم بتثبيت نسخة \"stable\" من دبيان."

#: win32-loader.c:107
msgid ""
"Daily build.  This is the development version of Debian-Installer.  It will "
"install Debian \"testing\" by default, and may be capable of installing "
"\"stable\" or \"unstable\" as well."
msgstr "البُنية اليوميّة. النسخة التطويرية من برنامج تثبيت دبيان. ستقوم بتثبيت نسخة دبيان \"testing\" التجريبية بشكل افتراضي، وقد تكون قادراً على تثبيت النسخة المستقرّة \"stable\" أو  \"unstable\" الغير مستقرّة أيضاً."

#. translate:
#. You might want to mention that so-called "known issues" page is only available in English.
#.
#: win32-loader.c:112
msgid ""
"It is recommended that you check for known issues before using a daily "
"build.  Would you like to do that now?"
msgstr "من المُستحسن أن تتحقّق من المشاكل المعلومة قبل استخدام البُنية اليوميّة. هل تودّ القيام بذلك الآن؟"

#: win32-loader.c:113
msgid "Desktop environment:"
msgstr "بية سطح المكتب:"

#: win32-loader.c:114
msgid "None"
msgstr "لاشيء"

#: win32-loader.c:115
msgid ""
"Debian-Installer Loader will be setup with the following parameters.  Do NOT "
"change any of these unless you know what you're doing."
msgstr "سيتم إعداد مُحمّل برنامج تثبيت دبيان بالمُعطيات التالية. لا تقم بتغيير أي منها مالم تكن تعلم ما تقوم به."

#: win32-loader.c:116
msgid "Proxy settings (host:port):"
msgstr "إعدادات البروكسي (مضيف:منفذ):"

#: win32-loader.c:117
msgid "Location of boot.ini:"
msgstr "موضع boot.ini:"

#: win32-loader.c:118
msgid "Base URL for netboot images (linux and initrd.gz):"
msgstr "عنوان URL الأساسي لصُور netboot (linux و initrd.gz):"

#: win32-loader.c:119
msgid "Error"
msgstr "خطأ"

#: win32-loader.c:120
msgid "Error: failed to copy $0 to $1."
msgstr "خطأ: فشل نسخ $0 إلى $1."

#: win32-loader.c:121
msgid "Generating $0"
msgstr "جاري توليد $0"

#: win32-loader.c:122
msgid "Appending preseeding information to $0"
msgstr "جاري إضافة معلومات preseeding إلى $0"

#: win32-loader.c:123
msgid "Error: unable to run $0."
msgstr "خطأ: تعذّر تشغيل $0."

#: win32-loader.c:124
msgid "Disabling NTFS compression in bootstrap files"
msgstr "تعطيل ضغط NTFS في ملفات bootstrap"

#: win32-loader.c:125
msgid "Registering Debian-Installer in NTLDR"
msgstr "تسجيل برنامج تثبيت دبيان في NTLDR"

#: win32-loader.c:126
msgid "Registering Debian-Installer in BootMgr"
msgstr "تسجيل برنامج تثبيت دبيان في BootMgr"

#: win32-loader.c:127
msgid "Error: failed to parse bcdedit.exe output."
msgstr "خطأ: فشلت قراءة مُخرجات bcdedit.exe."

#: win32-loader.c:128
msgid "Error: boot.ini not found.  Is this really Windows $windows_version?"
msgstr "خطأ: تعذر العثور على boot.ini.  هل نسخة ويندوز هذه هي $windows_version؟"

#: win32-loader.c:129
msgid "VERY IMPORTANT NOTICE:\\n\\n"
msgstr "تنبيه مهم جداً:\\n\\n"

#. translate:
#. The following two strings are mutualy exclusive.  win32-loader
#. will display one or the other depending on version of Windows.
#. Take into account that either option has to make sense in our
#. current context (i.e. be careful when using pronouns, etc).
#.
#: win32-loader.c:137
msgid ""
"The second stage of this install process will now be started.  After your "
"confirmation, this program will restart Windows in DOS mode, and "
"automaticaly load Debian Installer.\\n\\n"
msgstr "المرحلة الثانية من عملية التثبيت ستبدأ الآن. بعد توكيدك، سيقوم البرنامج بإعادة تشغيل ويندوز إلى وضع DOS، وتحميل برنامج تثبيت دبيان تلقائياً.\\n\\n"

#: win32-loader.c:138
msgid ""
"You need to reboot in order to proceed with your Debian install.  During "
"your next boot, you will be asked whether you want to start Windows or "
"Debian Installer.  Choose Debian Installer to continue with the install "
"process.\\n\\n"
msgstr "يجب أن تقوم بإعادة التشغيل كي تستمر بتثبيت دبيان. أثناء الإقلاع التالي، سوف تُسأل عما إذا كتب تريد تشغيل ويندوز أو برنامج تثبيت دبيان. اختر Debian Installer كي تستمر في عملية التثبيت.\\n\\n"

#: win32-loader.c:139
msgid ""
"During the install process, you will be offered the possibility of either "
"reducing your Windows partition to install Debian or completely replacing "
"it.  In both cases, it is STRONGLY RECOMMENDED that you have previously made "
"a backup of your data.  Nor the authors of this loader neither the Debian "
"project will take ANY RESPONSIBILITY in the event of data loss.\\n\\nOnce "
"your Debian install is complete (and if you have chosen to keep Windows in "
"your disk), you can uninstall the Debian-Installer Loader through the "
"Windows Add/Remove Programs dialog in Control Panel."
msgstr ""
"أثناء عملية التثبيت، ستتمكّن من تصغير حجم جزء ويندوز لتثبيت دبيان أو استبدالها بالكامل. "
"في كلتا الحالتين من المُستحسن جداً أن تقوم بعمل نسخة احتياطية من بياناتك. لن يكون كاتبو هذا البرنامج ولا مشروع دبيان مسؤولون بأي شكل عن فقدان بياناتك.\\n\\nحالما يكتمل تثبيت دبيان (وإن اخترت إبقاء ويندوز على حاسبك)، يمكنك إزالة برنامج تثبيت دبيان عبر إضافة/إزالة البرامج من لوحة التحكّم."

#: win32-loader.c:140
msgid "Do you want to reboot now?"
msgstr "هل تريد إعادة التشغيل الآن؟"

