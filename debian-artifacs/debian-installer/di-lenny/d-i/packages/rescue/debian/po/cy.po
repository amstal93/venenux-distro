# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Welsh
# Copyright (C) 2004-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Jonathan Price <mynamesnotclive@notclive.co.uk>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-08-20 21:58+0100\n"
"Last-Translator: Jonathan Price <mynamesnotclive@notclive.co.uk>\n"
"Language-Team: Welsh <>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Defnyddio'r modd arbed"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Defnyddio'r modd arbed"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Ni chanfodwyd rhaniadau"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Y ddyfais i'w glymu fel y system ffeiliau gwraidd:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Mewnosodwch ddyfais yr ydych am ddefnyddio fel ei system ffeil gwraidd. "
"Byddwch yn medru dewis rhwng gwahanol gwasanaethau achub i weithredu ar y "
"system ffeil yma."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Nid yw'r ddyfais yn bodoli"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Nid yw'r ddyfais fe roddoch ar gyfer eich system ffeiliau gwraidd "
"(${DEVICE}) yn bodoli. Ceisiwch eto."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Methwyd clymu"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Roedd gwall wrth glymu'r dyfais fe roddoch ar gyfer eich system ffeiliau "
"gwraidd (${DEVICE}) ar /target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
#, fuzzy
msgid "Please check the syslog for more information."
msgstr ""
"Os gwelwch yn dda, gwiriwch y cofnod gwall ar y drydydd consol neu /var/log/"
"messages ar gyfer mwy o wybodaeth."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Gweithredoedd achub"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
#, fuzzy
msgid "Rescue operation failed"
msgstr "Methiad gweithred ailfeintio"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
#, fuzzy
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr ""
"Method gweithrediad y gorchymyn cynhedynnedig \"${COMMAND} gyda'r côd "
"gorffen ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Gweithredu cragen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Gweithredu cragen yn yr amgylchedd gosodiad"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "/ - system ffeiliau gwraidd"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Dim system ffeiliau gwraidd"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Gweithredu cragen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Yn dilyn y neges hwn, os clymwyd y system ffeiliau yn llwyddiannus, cewch "
"blisgyn gyda'r system ffeiliau hwnnw ar \"/\". Os ydych angen unrhyw "
"systemau ffeiliau eraill (fel \"/usr\" ar wahan), bydd rhaid i chi eu clymu "
"nhw eich hun."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
#, fuzzy
msgid "Error running shell in /target"
msgstr "Dim plisgyn wedi ei ganfod yn /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Canfuwyd plisgyn (${SHELL}) ar eich system ffeiliau gwraidd (${DEVICE}), ond "
"disgwyddodd gwall wrth ei rhedeg."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "Dim plisgyn wedi ei ganfod yn /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Ni chanfuwyd plisgyn defnyddiadwy ar eich system ffeiliau gwraidd "
"(${DEVICE})."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Gweithredu cragen"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"yn dilyn y neges hwn, os clymwyd y system ffeiliau yn llwyddiannus, cewch "
"blisgyn gyda'r system ffeiliau hwnnw ar \"/\". Os ydych angen unrhyw "
"systemau ffeiliau eraill (fel \"/usr\" ar wahan), bydd rhaid i chi eu clymu "
"nhw eich hun."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Yn dilyn y neges hwn, os clymwyd y system ffeiliau yn llwyddiannus, cewch "
"blisgyn gyda'r system ffeiliau hwnnw ar \"/\". Os ydych angen unrhyw "
"systemau ffeiliau eraill (fel \"/usr\" ar wahan), bydd rhaid i chi eu clymu "
"nhw eich hun."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Cragen rhyngweithiol yn yr amgylchedd gosodiad"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Cyfrinair ar gyfer ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Mewnosodwch y cyfrinair ar gyfer ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr ""
"Os na mewonosodwch unrhyw beth, ni fydd ar gael yn ystod yr proses achub"
