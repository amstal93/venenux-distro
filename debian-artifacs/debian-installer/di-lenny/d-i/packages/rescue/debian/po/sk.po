# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Slovak messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Peter Mann <peter.mann@tuke.sk>
# Ivan Masár <helix84@centrum.sk>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-08-05 11:20+0100\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "Záchranný režim"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "Vstup do záchranného režimu"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "Neboli nájdené žiadne oblasti"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"Inštalátor nenašiel žiadne diskové oddiely, preto nebudete môcť pripojiť "
"koreňový súborový systém.  Mohlo to by spôsobené tým, že jadro nenašlo váš "
"pevný disk alebo zlyhalo čítanie tabuľky oddielov alebo disk nie je "
"rozdelený do oddielov. Ak chcete, môžete to preskúmať zo shellu v prostredí "
"inštalátora."

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid "Device to use as root file system:"
msgstr "Zariadenie na použitie koreňového súborového systému:"

#. Type: select
#. Description
#. :sl2:
#: ../rescue-mode.templates:3001
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"Zadajte zariadenie, ktoré chcete použiť ako váš koreňový súborový systém. Na "
"tomto súborovom systéme budete môcť vykonávať rôzne záchranné operácie."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "Nejestvuje takéto zariadenie"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"Zariadenie, ktoré ste zadali ako svoj koreňový súborový systém (${DEVICE}), "
"nejestvuje. Skúste to znova."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "Pripojenie zlyhalo"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"Pri pripájaní zariadenia, ktoré ste zadali ako svoj koreňový súborový systém "
"(${DEVICE}) pre /target, došlo k chybe."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "Viac informácií nájdete v súbore /var/log/syslog."

#. Type: select
#. Description
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "Záchranné operácie"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "Záchranné operácie zlyhali"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "Záchranná operácia „${OPERATION}“ zlyhala s chybou ${CODE}."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "Spustenie shellu na ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "Spustenie shellu v prostredí inštalátora"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "Zvoliť iný koreňový súborový systém"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "Reštart systému"

#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "Spustenie shellu"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"Po tejto správe dostanete shell s ${DEVICE} pripojeným na „/“. Ak "
"potrebujete hocijaké ďalšie súborové systémy (ako napr. samostatné „/usr“), "
"musíte si ich pripojiť manuálne."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "Chyba pri spúšťaní shellu v /target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"Na vašom koreňovom súborovom systéme (${DEVICE}) bol nájdený shell "
"(${SHELL}), ale pri jeho spustení nastala chyba."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "V /target nebol nájdený žiaden shell"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"Na vašom koreňovom súborovom systéme (${DEVICE}) nebol nájdený žiaden shell."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "Interaktívny shell na ${DEVICE}"

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"Po tejto správe dostanete shell s ${DEVICE} pripojeným do „/target“. Na "
"prácu môžete použiť nástroje dostupné v prostredí inštalátora. Ak si "
"potrebujete dočasne vytvoriť koreňový súborový systém, spustite „chroot /"
"target“. Ak potrebujete hocijaké ďalšie súborové systémy (ako napr. "
"samostatné „/usr“), musíte si ich pripojiť manuálne."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"Since the installer could not find any partitions, no file systems have been "
"mounted for you."
msgstr ""
"Po tejto správe dostanete shell v prostredí inštalátora. Neboli automaticky "
"pripojené žiadne súborové systémy, pretože inštalátor nenašiel žiadne "
"diskové oddiely."

#. Type: text
#. Description
#. :sl2:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "Interaktívny shell v prostredí inštalátora"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "Heslo pre ${DEVICE}:"

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "Prosím, zadajte heslo pre šifrovaný zväzok ${DEVICE}."

#. Type: password
#. Description
#. :sl2:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr "Ak nič nezadáte, zväzok nebude dostupný pri záchranných operáciách."
