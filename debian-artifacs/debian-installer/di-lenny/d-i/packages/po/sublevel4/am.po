# Amharic translation for debian-installer
# This file is distributed under the same license as the debian-installer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-08-26 12:13+0000\n"
"PO-Revision-Date: 2008-03-02 15:57+0100\n"
"Last-Translator: Tegegne Tefera <tefera@mekuria.com>\n"
"Language-Team: Amharic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: n>1\n"
"X-Poedit-Language: Amharic\n"
"X-Poedit-Country: ETHIOPIA\n"

#. Type: text
#. Description
#. eg. Virtual disk 1 (xvda)
#. :sl4:
#: ../partman-base.templates:56001
#, no-c-format
msgid "Virtual disk %s (%s)"
msgstr ""

#. Type: text
#. Description
#. eg. Virtual disk 1, partition #1 (xvda1)
#. :sl4:
#: ../partman-base.templates:57001
#, fuzzy, no-c-format
msgid "Virtual disk %s, partition #%s (%s)"
msgstr "IDE%s ተከታይ, ክፋይ #%s (%s)"

#. Type: text
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:1001
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:8001 ../partman-ext2r0.templates:10001
msgid "ext2r0"
msgstr "Ext2r0"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-ext2r0.templates:9001
msgid "old Ext2 (revision 0) file system"
msgstr "አሮጌ Ext2 (ዝርያ 0) የፋይል ስርዓት"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""
"ማስነሻ ክፋይ በext2 (revision 0) ፋይል ስርዓት አልተስተካከለም፡፡ አስሊ መኪናዎ ለመነሳት ይህንን የፋይል "
"ስርዓት ይፈልጋል፡፡ እባክዎ ወደኋላ ይመለሱና ext2 (revision 0)ን ይጠቀሙ፡፡"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:13001
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""
"የሩት ክፋዩ የዲስኩ ዋና ክፋይ አይደለም፡፡ ለመነሳት በአስሊ መኪናዎ ይህ መሆን አስፈላጊ ነው፡፡ እባክዎ ይመለሱና ለሩት "
"ክፋይ ዋና ክፋይን ይጠቀሙ፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../partman-target.templates:3001
msgid ""
"In order to start your new system, a so called boot loader is used.  It is "
"installed in a boot partition.  You must set the bootable flag for the "
"partition.  Such a partition will be marked with \"${BOOTABLE}\" in the main "
"partitioning menu."
msgstr ""
"ስርዓትዎን ለማስነሳት ቡት አስነሺ የሚባል እንጠቀማለን።  ይህም በቡት ክፋዩ ላይ ይተከላል። ለዚህ ክፋይ የቡት ሰንደቅ "
"መትከል ግዴታ ነው። የዚህ ዓይነ ክፋይ በዋናው ክፋይ ምናሌ \"${BOOTABLE}\" የሚል ምልክት ይኖረዋል።"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:1001
#, no-c-format
msgid "!! ERROR: %s"
msgstr "!! ስህተት: %s"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:2001
msgid "KEYSTROKES:"
msgstr "KEYSTROKES:"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:3001
msgid "Display this help message"
msgstr "ይህንን የመመሪያ መልዕክት አሳይ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:4001
msgid "Go back to previous question"
msgstr "ወደቅድሙ ጥያቄ ተመለስ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:5001
msgid "Select an empty entry"
msgstr "ባዶ ገቢን ምረጥ"

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:8001
#, no-c-format
msgid "Prompt: '%c' for help, default=%d> "
msgstr "ስጥ: '%c' ለመመሪያ, ቀዳሚ=%d> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:9001
#, no-c-format
msgid "Prompt: '%c' for help> "
msgstr "ስጥ: '%c' ለመመሪያ> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:10001
#, no-c-format
msgid "Prompt: '%c' for help, default=%s> "
msgstr "ስጥ: '%c' ለመመሪያ, ቀዳሚ=%s> "

#. Type: text
#. Description
#. :sl4:
#: ../cdebconf-text-udeb.templates:11001
msgid "[Press enter to continue]"
msgstr "[ለመቀጠል ሂድ ቁልፍን ይጫኑ]"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001 ../quik-installer.templates:21001
#: ../yaboot-installer.templates:15001
msgid "Failed to mount /target/proc"
msgstr "/target/proc ን መጫን አልተሳካም"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001 ../quik-installer.templates:21001
#: ../yaboot-installer.templates:15001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "የproc ፋይል ስርዓትን በ /target/proc መጫን አልተሳካም።"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#: ../nobootloader.templates:2001 ../quik-installer.templates:12001
#: ../quik-installer.templates:21001 ../yaboot-installer.templates:11001
#: ../yaboot-installer.templates:13001 ../yaboot-installer.templates:15001
#: ../vmelilo-installer.templates:7001 ../vmelilo-installer.templates:9001
msgid "Warning: Your system may be unbootable!"
msgstr "ማስጠንቀቂያ፦ ስርዓትዎን ቡት ማድረግ አይቻል ይሆናል!"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
#: ../nobootloader.templates:5001 ../yaboot-installer.templates:16001
msgid "Setting firmware variables for automatic boot"
msgstr "በራስ እንዲነሳ የfirmware ተለዋጭ እሴቶችን በመሰየም ላይ"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../yaboot-installer.templates:16001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../nobootloader.templates:3001 ../nobootloader.templates:5001
#: ../yaboot-installer.templates:16001 ../arcboot-installer.templates:5001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"ይህንን ማደግ የሚያስፈልግዎት አንድ ጊዜ ብቻ ነው፡፡ ከዚያ በኋላ የ\"boot\" ትዕዛዝን በመስጠት ወይም እንደገና "
"በማስነሳት አዲስ ወደ ተተከለው ስርዓትዎ ይግቡ፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../yaboot-installer.templates:16001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr "በfirmware ማስገቢያ ላይ ከርነሉን በማስገባት የማስነሳት ምርጫ ይኖሮታል፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
#, fuzzy
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"ስርዓትዎ ወደ ሊኒክስ በራስ እንዲገባ አንዳንድ ተለዋጮች በNetwinder NeTTrom firmware መሰየም አለባቸው፡፡"
"በዚህ ተከላ መጨረሻ ስራዓቱ እንደገና ሲነሳ በራስ ለመነሳት ይሞክራል፡፡ማንኛውንም ቁልፍ በመጫን ይህንን ማስቆም "
"ይችላሉ፡፡ ይህ ሲሆን ወደ NeTTrom የትዕዛዝ ማዕከል ይወርዱና የሚቀጥሉትን ትዕዛዞች ይፈጽማሉ፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:5001
msgid ""
"Some variables need to be set in the Netwinder NeTTrom firmware in order for "
"your system to boot linux automatically.  At the end of this installation "
"stage, the system will reboot, and the firmware will attempt to autoboot.  "
"You can abort this by pressing any key. You will then be dropped into the "
"NeTTrom command system where you have to execute the following commands:"
msgstr ""
"ስርዓትዎ ወደ ሊኒክስ በራስ እንዲገባ አንዳንድ ተለዋጮች በNetwinder NeTTrom firmware መሰየም አለባቸው፡፡"
"በዚህ ተከላ መጨረሻ ስራዓቱ እንደገና ሲነሳ በራስ ለመነሳት ይሞክራል፡፡ማንኛውንም ቁልፍ በመጫን ይህንን ማስቆም "
"ይችላሉ፡፡ ይህ ሲሆን ወደ NeTTrom የትዕዛዝ ማዕከል ይወርዱና የሚቀጥሉትን ትዕዛዞች ይፈጽማሉ፡፡"

#. Type: text
#. Description
#. :sl4:
#. Type: text
#. Description
#. :sl4:
#: ../partman-newworld.templates:1001 ../partman-newworld.templates:5001
msgid "NewWorld boot partition"
msgstr "የአዲስዓለም ስርዓት ማስነሻ ክፋይ፦"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: boolean
#. Description
#. :sl5:
#: ../partman-newworld.templates:2001 ../partman-newworld.templates:3001
#: ../partman-prep.templates:3001 ../partman-prep.templates:4001
#: ../partman-palo.templates:2001 ../partman-palo.templates:3001
#: ../partman-efi.templates:3001
msgid "Go back to the menu and resume partitioning?"
msgstr "ወደ ምናሌው ልመለስና ማካፈሉ ይቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-newworld.templates:2001
msgid ""
"No NewWorld boot partition was found. The yaboot boot loader requires an "
"Apple_Bootstrap partition at least 819200 bytes in size, using the HFS "
"Macintosh file system."
msgstr ""
"ምንም የNewWorld ቡት ክፋይ አልተገኘም፡፡ የyaboot ቡት ጫኚ የApple_Bootstrap የሆነ ቢያንስ 819200 "
"ባይት መጠን ያለው የHFS Macintosh ፋይል ስርዓትን የሚጠቀም ክፋይ ያስፈልገዋል፡፡"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-newworld.templates:3001
msgid "The NewWorld boot partition must be at least 819200 bytes in size."
msgstr "የNewWorld ቡት ክፋይ ቢያንስ መጠኑ 819200 ባይት መሆን አለበት።"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: text
#. Description
#. short variant of 'NewWorld boot partition'
#. Up to 10 character positions
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: text
#. Description
#. :sl5:
#. short variant of 'PALO boot partition'
#. Up to 10 character positions
#: ../partman-newworld.templates:6001 ../partman-palo.templates:5001
msgid "boot"
msgstr "ስርዓት ማስነሻ"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-newworld.templates:7001
msgid "HFS Macintosh file system"
msgstr "HFS የማኪንቶሽ ፋይል ስርዓት"

#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-newworld.templates:8001
msgid "hfs"
msgstr "hfs"

#. Type: text
#. Description
#. :sl4:
#. A bit of context for translators :
#. PReP stands for PowerPC Reference Platform, is an acronym, and should not be
#. translated. The PReP boot partition is a partition of type 0x41, which is
#. used by PReP boxes and IBM CHRP boxes to boot yaboot or the kernel from.
#: ../partman-prep.templates:1001
msgid "Use the partition as a PowerPC PReP boot partition"
msgstr "ክፋዩን እንደ የፓወርፒሲ PReP ቡት ክፋይ ተጠቀም"

#. Type: text
#. Description
#. :sl4:
#: ../partman-prep.templates:2001
msgid "PowerPC PReP boot partition"
msgstr "የፓወርፒሲ PReP ገዢ ስልት ማስነሻ ክፋይ"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-prep.templates:3001
msgid "No PowerPC PReP boot partition is found."
msgstr "ምንም የፓወርፒሲ PReP ገዢ ስልት ማስነሻ ክፋይ አልተገኘም"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-prep.templates:4001
msgid "The PowerPC PReP boot partition must be in the first 8MB."
msgstr "የPowerPC PReP የመነሻ ፕሮግራም ክፋይ የግድ የመጀመሪያው 8ሜባ ውስጥ መሆን አለበት፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:1001
msgid "Installing quik"
msgstr "ክዊክን በመትከል ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:2001
msgid "Installing quik boot loader"
msgstr "የክዊክ የገዢ ስልት ማስነሻን በመትከል ላይ"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:3001
msgid "quik installation failed.  Continue anyway?"
msgstr "የክዊክ ተከላ አልተሳካም፡፡ ቢሆንም ልቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:3001
msgid ""
"The quik package failed to install into /target/.  Installing quik as a boot "
"loader is a required step.  The install problem might however be unrelated "
"to quik, so continuing the installation may be possible."
msgstr ""
"/target/ ላይ ክዊክቡትን መትከል ሳይሳካ ቀርቷል፡፡ ክዊክቡትን እንደ ቡት ማስነሻ መትከል አስፈላጊ የሆነ ደረጃ "
"ነው፡፡  ነገርግን የመትከሉ ችግር ከክዊክቡት ጋር ያልተገናኘና ተከላውንም መቀጠል ይቻል ይሆናል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:4001
msgid "Checking partitions"
msgstr "ክፋዩን በመቆጣጠር ላይ"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl5:
#: ../quik-installer.templates:5001 ../yaboot-installer.templates:7001
#: ../prep-installer.templates:6001 ../vmelilo-installer.templates:5001
msgid "No root partition found"
msgstr "ምንም የስር ክፋይ አልተገኘም።"

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl5:
#: ../quik-installer.templates:5001 ../yaboot-installer.templates:7001
#: ../prep-installer.templates:6001 ../vmelilo-installer.templates:5001
msgid ""
"No partition is mounted as your new root partition. You must mount a root "
"partition first."
msgstr "የስር ክፋይ ሆኖ የተጫነ ምንም ክፋይ የለም። በቅድሚያ የስር ክፋይን መጫን አለቦት።"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:6001
msgid "Root partition not on first disk"
msgstr "ስር ክፋይ በመጀመሪያው ዲስክ ላይ አይደለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:6001
msgid ""
"The quik boot loader requires the root partition to be on the first disk. "
"Please return to the partitioning step."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:7001
msgid "Boot partition not on first disk"
msgstr "Boot ክፋይ በመጀመሪያው ዲስክ ላይ አይደለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:7001
msgid ""
"The quik boot loader requires the boot partition to be on the first disk. "
"Please return to the partitioning step."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:8001
msgid "Boot partition must be on ext2"
msgstr "Boot ክፋይ በext2 ላይ መሆን አለበት"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:8001
msgid ""
"The quik boot loader requires the partition that holds /boot to be formatted "
"using the ext2 file system. Please return to the partitioning step."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001 ../quik-installer.templates:10001
msgid "Do you really want to install the quik boot loader?"
msgstr "በርግጥ የክዊክ የማስነሻ ፕሮግራምን መትከል ይፈልጋሉ?"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001
msgid ""
"You have chosen to install the quik boot loader. You will not be able to "
"boot any other operating system from this disk. Furthermore, your machine "
"may not be bootable in any manner after this process completes. If you are "
"left with a blank screen, you may need to try a cold boot and hold down "
"Command-Option-P-R."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001 ../quik-installer.templates:10001
msgid "Be aware that this code has not been thoroughly tested."
msgstr "ይህ ኮድ በቅጡ የተፈተሸ መሆኑን ይገንዘቡ፡፡"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:10001
msgid ""
"You have chosen to install the quik boot loader. You will not be able to "
"boot any other operating system from this disk. Furthermore, your machine "
"may not be bootable in any manner after this process completes."
msgstr ""
"የኩዊክ ማስነሻ ስልትን ለመትከል መርጠዋል፡፡ ከዚህ ዲስክ ምንም ሌላ ገዢስልት ማስነሳት አይችሉም፡፡ በተጨማሪ አስሊ "
"መኪናዎ ይህ ተግባር ሲያልቅ በጭራሽ መነሳት የማይችል ሊሆን ይችላል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:11001
msgid "Creating quik configuration"
msgstr "የኩዊክ ማስተካከያ ፋይል በመፍጠር ላይ"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001
msgid "Failed to create quik configuration"
msgstr "ኩዊክ አስተካካይን ምፍጠር አልተቻለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001
msgid "The creation of the main quik configuration file failed."
msgstr "ዋናውን የኩዊክ ማዘጋጃ ፈጠራ አልተሳካም፡፡"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:13001
msgid "Failed to resolve kernel symlink"
msgstr "የከርነል symlinkን resolve ማድረግ አልተቻለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:13001
msgid ""
"/vmlinux or /boot/vmlinux in the installed system appears not to be a "
"symlink to a kernel image. This is probably a bug."
msgstr ""

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:14001
msgid "Failed to resolve initrd symlink"
msgstr "የinitrd symlinkን resolve ማድረግ አልተቻለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:14001
msgid ""
"/initrd.img or /boot/initrd.img in the installed system appears not to be a "
"symlink to an initial RAM disk image. This is probably a bug."
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:15001
msgid "Installing quik into bootstrap partition"
msgstr "ክዊክቡትን በመነሻ ከፋይ ላይ በመትከል ላይ..."

#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl4:
#. #-#-#-#-#  templates.pot (PACKAGE VERSION)  #-#-#-#-#
#. Type: error
#. Description
#. :sl5:
#: ../quik-installer.templates:16001 ../yaboot-installer.templates:13001
#: ../vmelilo-installer.templates:9001
msgid "Failed to install boot loader"
msgstr "ማስነሻ ፕሮግራም ተሰናክሏል"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:16001
msgid "The installation of the quik boot loader failed."
msgstr "የክዊክቡት ማስነሻ ፕሮግራም ተሰናክሏል"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:16001
msgid "Warning: your system may be unbootable!"
msgstr "ማስጠንቀቂያ፦ ስርዓትዎን ቡት ማድረግ አይቻል ይሆናል!"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:17001
msgid "Setting up OpenFirmware"
msgstr "OpenFirmware በመትከል ላይ"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:18001
msgid "Unable to configure OpenFirmware"
msgstr "OpenFirmwareን ማስተካከል አልተቻለም"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:18001
msgid ""
"Setting the OpenFirmware boot-device variable failed. You will have to "
"configure OpenFirmware yourself to boot."
msgstr ""
"የOpenFirmware ማስነሻ አካል ተለዋጫ ስየማ አልተሳካም፡፡ OpenFirmwareን በእጅ ማዘጋጀት ይኖርቦታል፡፡"

#. Type: error
#. Description
#. :sl4:
#. This error may not be fatal, depending on the exact type of Mac.
#: ../quik-installer.templates:19001
msgid "Problem configuring OpenFirmware"
msgstr "OpenFirmware የማስተካከል ስህተት"

#. Type: error
#. Description
#. :sl4:
#. This error may not be fatal, depending on the exact type of Mac.
#: ../quik-installer.templates:19001
msgid ""
"Setting the OpenFirmware boot-command variable failed. You may have "
"intermittent boot failures."
msgstr ""
"የOpenFirmware ማስነሻ አካል ተለዋጫ ስየማ አልተሳካም፡፡ ምናልባት አልፎ አልፎ የቡት አለመሳካት ሊያስከትል "
"ይችላል።"

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001
msgid "Successfully installed quik"
msgstr "የኩዊክ ተከላ ተሳክቷል"

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001
msgid "The quik boot loader was successfully installed."
msgstr "ክዊክ ቡት ጫኝ በተሳካ ሁኔታ ተተክሏል"

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001 ../yaboot-installer.templates:14001
#: ../prep-installer.templates:8001
msgid "The new system is now ready to boot."
msgstr "አዲሱን ስርዓት ቡት ለማድረግ አሁን ዝግጁ ነው።"

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../quik-installer.templates:22001
msgid "Install quik on a hard disk"
msgstr "ክዊክ ዲስኩ ላይ ይትከሉ፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:1001
msgid "Installing Yaboot"
msgstr "ያቡት በመትከል ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:2001
msgid "Installing Yaboot boot loader"
msgstr "ያቡት የገዢ ስልት ማስነሻን በመትከል ላይ"

#. Type: boolean
#. Description
#. :sl4:
#: ../yaboot-installer.templates:3001
msgid "Yaboot installation failed.  Continue anyway?"
msgstr "Yaboot ተከላ አልተሳካም። ቢሆንም ልቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../yaboot-installer.templates:3001
msgid ""
"The yaboot package failed to install into /target/.  Installing Yaboot as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to Yaboot, so continuing the installation may be possible."
msgstr ""
"/target/ ላይ ያቡትን መትከል ሳይሳካ ቀርቷል፡፡ ያቡትን እንደ ቡት ማስነሻ መትከል አስፈላጊ የሆነ ደረጃ ነው፡፡  "
"ነገርግን የመትከሉ ችግር ከያቡት ጋር ያልተገናኘና ተከላውንም መቀጠል ይቻል ይሆናል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:4001
msgid "Looking for bootstrap partitions"
msgstr "የbootstrap ክፋይን በመፈለግ ላይ"

#. Type: error
#. Description
#. :sl4:
#: ../yaboot-installer.templates:5001
msgid "No bootstrap partition found"
msgstr "ምንም የbootstrap ፋይል አልተገኘም"

#. Type: error
#. Description
#. :sl4:
#: ../yaboot-installer.templates:5001
msgid ""
"No hard disks were found which have an \"Apple_Bootstrap\" partition.  You "
"must create an 819200-byte partition with type \"Apple_Bootstrap\"."
msgstr ""
" \"Apple_Bootstrap\" ክፋይ ያለበት ምንም ዲስክ አልተገኘም፡፡ የ\"Apple_Bootstrap\" ዓይነት "
"ያለበት ባለ 819200-byte ክፋይ የግድ መፍጠር ይኖርቦታል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:6001
msgid "Looking for root partition"
msgstr "የስር ክፋይን በመፈለግ ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:8001
msgid "Looking for other operating systems"
msgstr "ሌላ ገዢ ስልቶችን በመፈለግ ላይ"

#. Type: select
#. Description
#. :sl4:
#: ../yaboot-installer.templates:9001
msgid ""
"Yaboot (the Linux boot loader) needs to be installed on a hard disk "
"partition in order for your system to be bootable.  Please choose the "
"destination partition from among these partitions that have the bootable "
"flag set."
msgstr ""

#. Type: select
#. Description
#. :sl4:
#: ../yaboot-installer.templates:9001
msgid "Warning: this will erase all data on the selected partition!"
msgstr "ማስጠንቀቂያ፦ ይህ በተመረጠው ክፋይ ላይ ያለውን ሁሉንም መረጃ ይሰርዛል!"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:10001
msgid "Creating yaboot configuration"
msgstr "የያቡት ማስተካከያ ፋይል በመፍጠር ላይ"

#. Type: error
#. Description
#. :sl4:
#: ../yaboot-installer.templates:11001
msgid "Failed to create yaboot configuration"
msgstr "ያቡት አስተካካይን ምፍጠር አልተቻለም"

#. Type: error
#. Description
#. :sl4:
#: ../yaboot-installer.templates:11001
msgid "The creation of the main yaboot configuration file failed."
msgstr "ዋናውን የያቡት ማዘጋጃ ፋይል ፈጠራ አልተሳካም፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../yaboot-installer.templates:12001
msgid "Installing yaboot into bootstrap partition"
msgstr "ቪመሊሎን በመነሻ ከፋይ ላይ በመትከል ላይ..."

#. Type: error
#. Description
#. :sl4:
#: ../yaboot-installer.templates:13001
msgid "The installation of the yaboot boot loader failed."
msgstr "የቪሜሊሎ ማስነሻ ፕሮግራም ተሰናክሏል"

#. Type: note
#. Description
#. :sl4:
#: ../yaboot-installer.templates:14001
msgid "Successfully installed yaboot"
msgstr "የያቡት ተከላ ተስተካክሏል"

#. Type: note
#. Description
#. :sl4:
#: ../yaboot-installer.templates:14001
msgid "The yaboot boot loader was successfully installed."
msgstr "Yaboot ቡት ጫኝ በተሳካ ሁኔታ ተተክሏል"

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../yaboot-installer.templates:17001
msgid "Install yaboot on a hard disk"
msgstr "የአቡት ስርዓት አስነሺ ፕሮግራምን ዲስኩ ላይ ይትከሉ፡፡"

#. Type: text
#. Description
#. Rescue menu item
#. :sl4:
#: ../yaboot-installer.templates:18001
msgid "Reinstall yaboot boot loader"
msgstr "ያቡት የገዢ ስልት ማስነሻ እንደገና ይተከል"

#. Type: text
#. Description
#. :sl4:
#. Main menu item
#. Keep translations below 55 ccolumns (1 character=1 column
#. except for wide character languages such as Chinese/Japanese/Korean)
#. A bit of context for translators :
#. PReP stands for PowerPC Reference Platform, is an acronym, and should not be
#. translated. The PReP boot partition is a partition of type 0x41, which is
#. used by PReP boxes and IBM CHRP boxes to boot yaboot or the kernel from.
#: ../prep-installer.templates:1001
msgid "Install the kernel on a PReP boot partition"
msgstr "ከርነሉን PReP ማስነሻ ክፋይ ላይ ትከል"

#. Type: text
#. Description
#. :sl4:
#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:2001 ../prep-installer.templates:7001
msgid "Copying the kernel to the PReP boot partition"
msgstr "ከርነሉን ወደ PReP ቡት ክፋይ በመገልበጥ ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:3001
msgid "Looking for PReP boot partitions"
msgstr "PReP የማስነሻ ክፋይን በመፈለግ ላይ"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:4001
msgid "No PReP boot partitions"
msgstr "ምንም የPReP ማስነሻ ክፋይ አልተገኘም"

#. Type: error
#. Description
#. :sl4:
#: ../prep-installer.templates:4001
msgid ""
"No PReP boot partitions were found. You must create a PReP boot partition "
"within the first 8MB of your hard disk."
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../prep-installer.templates:5001
msgid "Looking for the root partition"
msgstr "የስር ክፋይን በመፈለግ ላይ"

#. Type: note
#. Description
#. :sl4:
#: ../prep-installer.templates:8001
msgid "Successfully installed PReP"
msgstr "የPReP ተከላእሳክቷል"

#. Type: note
#. Description
#. :sl4:
#: ../prep-installer.templates:8001
msgid "The kernel was properly copied to the PReP boot partition."
msgstr "ከርነሉ ወደ PReP ቡት ክፋይ በትክክል ኮፒ ተደርጓል;"

#. Type: boolean
#. Description
#. :sl4:
#: ../silo-installer.templates:1001
msgid "SILO installation failed.  Continue anyway?"
msgstr "የሲሎ ተከላ አልተሳካም፡፡ ቢሆንም ልቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../silo-installer.templates:1001
msgid ""
"The 'silo' package failed to install into /target/.  Installing SILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to SILO, so continuing the installation may be possible."
msgstr ""
"/target/ ላይ ሲሎን መትከል ሳይሳካ ቀርቷል፡፡ ሲሎን እንደ ቡት ማስነሻ መትከል አስፈላጊ የሆነ ደረጃ ነው፡፡  "
"ነገርግን የመትከሉ ችግር ከሲሎ ጋር ያልተገናኘና ተከላውንም መቀጠል ይቻል ይሆናል፡፡"

#. Type: error
#. Description
#. :sl4:
#: ../silo-installer.templates:2001
msgid "SILO installation failed"
msgstr "የሲሎ ተከላ አልተሳካም"

#. Type: error
#. Description
#. :sl4:
#: ../silo-installer.templates:2001
msgid "Running \"/sbin/silo\" failed with error code \"${ERRCODE}\"."
msgstr "\"/sbin/silo\" ማስኬድ \"${ERRCODE}\" የስህተት ኮድ ተፈጥሮበት አልተሳካም፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:3001
msgid "SILO installation successful"
msgstr "የሲሎ ተከላ ተሳክቷል"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:3001
msgid ""
"The SILO boot loader has been successfully installed on your new boot "
"partition, and your system should now be able to boot itself."
msgstr ""

#. Type: text
#. Description
#. Main menu text
#. :sl4:
#: ../silo-installer.templates:4001
msgid "Install the SILO boot loader on a hard disk"
msgstr "የSILO ቡት አስነሺን ዲስኩ ላይ ይትከሉ፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:5001
msgid "Unsupported boot file system type"
msgstr "ያልተደገፈ የገዢ ስልት ማስነሻ ፋይል ስርዓት"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:5001
msgid ""
"In order to be successfully loaded by SILO (Sparc Linux Loader), the kernel "
"must reside on an ext2, ext3 or ufs formatted partition. In the current "
"partitioning scheme the kernel is going to be installed on the ${SILO_PART} "
"partition of type ${SILO_TYPE}."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:5001 ../silo-installer.templates:7001
msgid ""
"It is strongly recommended that you go back to partitioning and correct this "
"problem. Keeping the current configuration may result in an unbootable "
"system."
msgstr ""
"ወደ መክፈሉ ተመልሰው ይህንን ስህተት ቢያርሙት ይሻላል፡፡ አሁን ባለው አዘገጃጀት እንዲቀጥል ከተደረገ ወጤቱ የማይነሳ "
"ስርዓት ሊሆን ይችላል፡፡"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:6001
msgid "Boot partition may cause problems with SILO"
msgstr "የቡት ክፋይ ከሲሎ ጋር ችግር ሊፈጥር ይችላል"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:6001
msgid ""
"This machine is likely to suffer from a firmware bug, which makes it "
"impossible for SILO (Sparc Linux Loader) to boot a kernel placed farther "
"than 1 GB from the beginning of the disk."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:6001
msgid ""
"To avoid problems it is strongly recommended to install the kernel into a "
"partition which fits entirely within the first GB of the disk (by creating a "
"small /boot partition in the beginning of the disk, for example). In the "
"current partitioning scheme it is going to be installed onto the "
"${SILO_PART} partition which extends up to ${SILO_OFF} mark. Keeping this "
"configuration may result in an unbootable system."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:7001
msgid "Unsupported partition table on the boot disk"
msgstr "በማስነሻው ዲስክ ዲስክ ላይ ያልተደገፈ ክፋይ አለ"

#. Type: note
#. Description
#. :sl4:
#: ../silo-installer.templates:7001
msgid ""
"SILO (Sparc Linux Loader) can only boot the kernel off a disk with the \"sun"
"\" partition table. In the current partitioning scheme the kernel is going "
"to be installed on the ${SILO_PART} partition located on a disk with the "
"partition table of type \"${SILO_DISK}\"."
msgstr ""
"SILO (Sparc Linux Loader) ከርነሉን መጫን የሚችለው የ\"sun\" ክፋይ ሠንጠረዥ ካለው ዲስክ ብቻ ነው፡፡ "
"በአሁኑ የክፋይ መርሃግብር ከርነሉ የሚተከለው የ\"${SILO_DISK}\" የክፋይ ሠንጠረዥ ዓይነት ባለበት ዲስክ ላይ "
"ባለው በ${SILO_PART} ክፋይ ላይ ይሆናል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../arcboot-installer.templates:1001
msgid "Install the Arcboot boot loader on a hard disk"
msgstr "አርክቡት አስነሽ ፕሮግራምን ዲስኩ ላይ ትከል"

#. Type: string
#. Description
#. :sl4:
#: ../arcboot-installer.templates:2001
msgid "Disk for boot loader installation:"
msgstr "የገዢ ስልት ማስነሻ መትከያ ዲስክ፦"

#. Type: string
#. Description
#. :sl4:
#: ../arcboot-installer.templates:2001
msgid ""
"Arcboot must be installed into the volume header of a disk with a SGI "
"disklabel. Usually the volume header of /dev/sda is used. Please give the "
"device name of the disk on which to put arcboot."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#: ../arcboot-installer.templates:3001
msgid "Arcboot configured to use a serial console"
msgstr "Arcboot serial consoleን እንዲጠቀም ተዘጋጅቷል"

#. Type: note
#. Description
#. :sl4:
#: ../arcboot-installer.templates:3001
msgid ""
"Arcboot is configured to use the serial port ${PORT} as the console. The "
"serial port speed is set to ${SPEED}."
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../arcboot-installer.templates:4001
msgid "Arcboot installation failed.  Continue anyway?"
msgstr "የአርክቡት ተከላ አልተሳካም፡፡ ቢሆንም ልቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../arcboot-installer.templates:4001
msgid ""
"The arcboot package failed to install into /target/.  Installing Arcboot as "
"a boot loader is a required step.  The install problem might however be "
"unrelated to Arcboot, so continuing the installation may be possible."
msgstr ""
"/target/ ላይ አርክቡትን መትከል ሳይሳካ ቀርቷል፡፡ አርክቡትን እንደ ቡት ማስነሻ መትከል አስፈላጊ የሆነ ደረጃ "
"ነው፡፡  ነገርግን የመትከሉ ችግር ከአርክቡት ጋር ያልተገናኘና ተከላውንም መቀጠል ይቻል ይሆናል፡፡"

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../arcboot-installer.templates:5001
msgid "Setting PROM variables for Arcboot"
msgstr "ለአርክቡት የPROM  ተለዋጫን በመሰየም ላይ"

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#: ../arcboot-installer.templates:5001
msgid ""
"If this is the first Linux installation on this machine, or if the hard "
"drives have been repartitioned, some variables need to be set in the PROM "
"before the system is able to boot normally."
msgstr ""

#. Type: note
#. Description
#. :sl4:
#. Translators, the 4th string of this description has been dropped from
#. PO files. It contains firmware commands and should not be translated.
#. "Stop for Maintenance" should be left in English
#: ../arcboot-installer.templates:5001
msgid ""
"At the end of this installation stage, the system will reboot.  After this, "
"enter the command monitor from the \"Stop for Maintenance\" option, and "
"enter the following commands:"
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid "SiByl boot loader installation failed.  Continue anyway?"
msgstr "የሲቢል ቡት ጫኚ ተከላ አልተሳካም። ቢሆንም ይቀጥል?"

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid ""
"The SiByl package failed to install into /target/.  Installing SiByl as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to SiByl, so continuing the installation may be possible."
msgstr ""
"/target/ ላይ ሲቢል መትከል ሳይሳካ ቀርቷል፡፡ ሲቢል እንደ ቡት ማስነሻ መትከል አስፈላጊ የሆነ ደረጃ ነው፡፡  "
"ነገርግን የመትከሉ ችግር ከሲቢል ጋር ያልተገናኘና ተከላውንም መቀጠል ይቻል ይሆናል፡፡"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:2001
msgid "Installing the SiByl boot loader"
msgstr "ሲቢል የገዢ ስልት ማስነሻን በመትከል ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:3001
msgid "Installing the SiByl package"
msgstr "የSiByl ጥቅልን በመትከል ላይ"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:4001
msgid "Creating SiByl configuration"
msgstr "የSiByl ማስተካከያ ፋይል በመፍጠር ላይ"

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../sibyl-installer.templates:5001
msgid "Install the SiByl boot loader on a hard disk"
msgstr "የSiByl ማስነሻ ፕሮራምን ዲስኩ ላይ ትከል"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:6001
msgid "SiByl boot partition"
msgstr "የSiByl ስርዓት ማስነሻ ክፋይ፦"

#. Type: text
#. Description
#. This item is a progress bar heading when the system configures
#. some flashable memory used by many embedded devices
#. :sl4:
#: ../flash-kernel-installer.templates:1001
msgid "Configuring flash memory to boot the system"
msgstr "ገዢ ስርዓትን ለማስነሳት ተሰኪ ማኅደርን በማስተካከል ላይ"

#. Type: text
#. Description
#. This item is a progress bar heading when an embedded device is
#. configured so it will boot from disk
#. :sl4:
#: ../flash-kernel-installer.templates:2001
#, fuzzy
msgid "Making the system bootable"
msgstr "ገዢ ስልትን በማሰናዳት ላይ "

#. Type: text
#. Description
#. This is "preparing the system" to flash the kernel and initrd
#. on a flashable memory
#. :sl4:
#: ../flash-kernel-installer.templates:3001
#, fuzzy
msgid "Preparing the system..."
msgstr "ገዢ ስልትን በማሰናዳት ላይ "

#. Type: text
#. Description
#. This is a progress bar showing up when the system
#. write the kernel to the flashable memory of the embedded device
#. :sl4:
#: ../flash-kernel-installer.templates:4001
#, fuzzy
msgid "Writing the kernel to flash memory..."
msgstr "ከርነልን በተሰኪ ማህደር ላይ በመጻፍ ላይ"

#. Type: text
#. Description
#. This is a progress bar showing up when the system generates a
#. special boot image on disk for some embedded device so they
#. can boot.
#. :sl4:
#: ../flash-kernel-installer.templates:5001
msgid "Generating boot image on disk..."
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. This item is a menu entry for a step where the system configures
#. the flashable memory used by many embedded devices
#. (writing the kernel and initrd to it)
#. :sl4:
#: ../flash-kernel-installer.templates:6001
#, fuzzy
msgid "Make the system bootable"
msgstr "GLAN Tankን ቡት ተደራጊ ይሁን"
