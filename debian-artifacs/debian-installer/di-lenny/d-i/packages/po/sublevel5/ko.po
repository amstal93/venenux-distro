# Korean messages for debian-installer.
# Copyright (C) 2003,2004,2005 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-08-23 22:49+0000\n"
"PO-Revision-Date: 2008-03-03 20:48+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team:  Korean <debian-l10n-korean@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:54001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:55001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s), 파티션 #%s"

#. Type: text
#. Description
#. :sl5:
#. Type: text
#. Description
#. :sl5:
#: ../partman-palo.templates:1001 ../partman-palo.templates:4001
msgid "PALO boot partition"
msgstr "PALO 부팅 파티션"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-palo.templates:2001
msgid "No PALO partition was found."
msgstr "PALO 파티션이 없습니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-palo.templates:3001
msgid "The PALO partition must be in the first 2GB."
msgstr "PALO 파티션은 처음 2GB에 들어 있어야 합니다."

#. Type: text
#. Description
#. :sl5:
#: ../partman-efi.templates:1001
msgid ""
"In order to start your new system, the firmware on your Itanium system loads "
"the boot loader from its private EFI partition on the hard disk.  The boot "
"loader then loads the operating system from that same partition.  An EFI "
"partition has a FAT16 file system formatted on it and the bootable flag set. "
"Most installations place the EFI partition on the first primary partition of "
"the same hard disk that holds the root file system."
msgstr ""
"새 데비안 시스템을 시작할 때, 이 이터늄 시스템의 펌웨어가 하드 디스크의 EFI "
"파티션에서 부트로더를 읽어들입니다.  그 다음에 부트로더가 같은 파티션에서 운"
"영 체제를 읽어들입니다.  EFI 파티션은 FAT16 파티션으로 포맷되어 있고 부팅 가"
"능 플래그가 설정되어 있습니다.  보통 루트 파일 시스템이 들어 있는 같은 하드 "
"디스크의 첫 번째 주 파티션을 EFI 파티션으로 합니다."

#. Type: text
#. Description
#. :sl5:
#. Type: text
#. Description
#. :sl5:
#: ../partman-efi.templates:2001 ../partman-efi.templates:4001
msgid "EFI boot partition"
msgstr "EFI 부팅 파티션"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-efi.templates:3001
msgid "No EFI partition was found."
msgstr "EFI 파티션이 없습니다."

#. Type: text
#. Description
#. :sl5:
#. short variant of 'EFI boot partition'
#. Up to 10 character positions
#: ../partman-efi.templates:5001
msgid "EFIboot"
msgstr "EFIboot"

#. Type: text
#. Description
#. :sl5:
#: ../partman-efi.templates:7001
msgid "EFI-fat16"
msgstr "EFI-fat16"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:1001
msgid "Aboot installation failed.  Continue anyway?"
msgstr "Aboot 설치가 실패했습니다.  그래도 계속하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:1001
msgid ""
"The aboot package failed to install into /target/.  Installing aboot as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to aboot, so continuing the installation may be possible."
msgstr ""
"aboot 패키지를 /target/에 설치하는 데 실패했습니다. aboot를 부트로더로 설치하"
"는 일은 꼭 필요한 단계입니다.  하지만 이 문제가 aboot와는 관계없는 문제일 수"
"도 있으므로, 설치를 계속할 수도 있습니다."

#. Type: select
#. Description
#. :sl5:
#: ../aboot-installer.templates:2001
msgid ""
"Aboot needs to install the boot loader on a bootable device containing an "
"ext2 partition.  Please select the ext2 partition that you wish aboot to "
"use.  If this is not the root file system, your kernel image and the "
"configuration file /etc/aboot.conf will be copied to that partition."
msgstr ""
"Aboot는 부트로더를 EXT2 파티션이 들어 있는 부팅 가능한 장치에 설치해야 합니"
"다.  aboot가 사용할 EXT2 파티션을 선택하십시오.  루트 파티션이 아니라면, 커"
"널 이미지와 설정 파일 /etc/aboot.conf를 그 파티션으로 복사합니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:3001
msgid "Install /boot on a disk with an unsupported partition table?"
msgstr ""
"지원하지 않는 파티션 테이블이 들어 있는 디스크에 /boot를 설치하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:3001
msgid ""
"To be bootable from the SRM console, aboot and the kernel it loads must be "
"installed on a disk which uses BSD disklabels for its partition table.  "
"Your /boot directory is not located on such a disk.  If you proceed, you "
"will not be able to boot your system using aboot, and will need to boot it "
"some other way."
msgstr ""
"SRM 콘솔로 부팅하려면, aboot와 aboot가 읽어들일 커널을 파티션 테이블에 BSD 디"
"스크 레이블을 사용한 디스크에 설치해야 합니다.  /boot 디렉토리가 그러한 디스"
"크에 들어 있지 않습니다.  계속 하신다면 aboot로 시스템을 부팅하지 못할 것이"
"고, 다른 방법으로 부팅해야 할 것입니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid "Use unsupported file system type for /boot?"
msgstr "/boot 파티션에 지원하지 않는 파일 시스템을 사용하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid ""
"Aboot requires /boot to be located on an ext2 partition on a bootable "
"device.  This means that either the root partition must be an ext2 file "
"system, or you must have a separate ext2 partition mounted at /boot."
msgstr ""
"aboot를 설치하려면, 부팅 가능한 장치의 EXT2 파티션에 /boot를 집어 넣어야 합니"
"다.  즉 루트 파티션이 EXT2 파일 시스템이거나, /boot에 마운트한 EXT2 파티션이 "
"있어야 합니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:4001
msgid ""
"Currently, /boot is located on a partition of type ${PARTTYPE}.  If you keep "
"this setting, you will not be able to boot your Debian system using aboot."
msgstr ""
"현재 /boot는 ${PARTTYPE} 파티션에 들어 있습니다.  지금 설정을 유지하면 aboot"
"로 데비안 시스템을 부팅할 수 없습니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid "Install without aboot?"
msgstr "aboot 없이 설치하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid ""
"Your /boot directory is located on a disk that has no space for the aboot "
"boot loader.  To install aboot, you must have a special aboot partition at "
"the beginning of your disk."
msgstr ""
"aboot 부트로더가 들어갈 만한 공간이 없는 디스크에 /aboot 디렉토리가 있습니"
"다.  yaboot를 설치하려면, 디스크의 첫 부분에 aboot 전용 파티션이 있어야 합니"
"다."

#. Type: boolean
#. Description
#. :sl5:
#: ../aboot-installer.templates:5001
msgid ""
"If you continue without correcting this problem, aboot will not be installed "
"and you will not be able to boot your new system from the SRM console."
msgstr ""
"이 문제를 바로잡지 않고 계속 하신다면, aboot를 설치하지 않을 것이고 SRM 콘솔"
"에서 새 시스템으로 부팅하지 못할 것입니다."

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../aboot-installer.templates:6001
msgid "Install aboot on a hard disk"
msgstr "하드 디스크에 ABOOT 설치"

#. Type: error
#. Description
#. :sl5:
#: ../aboot-installer.templates:7001
msgid "No ext2 partitions found"
msgstr "EXT2 파티션이 없습니다"

#. Type: error
#. Description
#. :sl5:
#: ../aboot-installer.templates:7001
msgid ""
"No ext2 file systems were found on your computer.  To load the Linux kernel "
"from the SRM console, aboot needs an ext2 partition on a disk that has been "
"partitioned with BSD disklabels.  You will need to configure at least one "
"ext2 partition on your system before continuing."
msgstr ""
"컴퓨터에 EXT2 파일 시스템이 없습니다.  SRM 콘솔에서 리눅스 커널을 읽어들이려"
"면 BSD 디스크레이블로 파티션한 EXT2 파티션이 필요합니다.  계속하기 전에 시스"
"템에 최소한 EXT2 파티션 한 개가 필요합니다."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:8001
msgid "Installing the aboot boot loader"
msgstr "aboot 부트로더를 설치하는 중입니다"

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:9001
msgid "Installing the 'aboot' package..."
msgstr "'aboot' 패키지를 설치하는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:10001
msgid "Determining aboot boot device..."
msgstr "aboot 부팅 장치를 찾는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:11001
msgid "Installing aboot on ${BOOTDISK}..."
msgstr "aboot를 ${BOOTDISK}에 설치하는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:12001
msgid "Configuring aboot to use partition ${PARTNUM}..."
msgstr "aboot가 ${PARTNUM} 파티션을 사용하도록 설정하는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:13001
msgid "Copying kernel images to ${BOOTDEV}..."
msgstr "커널 이미지를 ${BOOTDEV}에 복사하는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:14001 ../aboot-installer.templates:15001
msgid "Aboot boot partition"
msgstr "Aboot 부팅 파티션"

#. Type: text
#. Description
#. :sl5:
#: ../aboot-installer.templates:16001
msgid "aboot"
msgstr "aboot"

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:1001
msgid "Installing vmelilo"
msgstr "vmelilo를 설치하는 중입니다"

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:2001
msgid "Installing vmelilo boot loader"
msgstr "vmelilo 부트로더를 설치하는 중입니다"

#. Type: boolean
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:3001
msgid "vmelilo installation failed.  Continue anyway?"
msgstr "vmelilo 설치가 실패했습니다.  그래도 계속하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:3001
msgid ""
"The vmelilo package failed to install into /target/.  Installing vmelilo as "
"a boot loader is a required step.  The install problem might however be "
"unrelated to vmelilo, so continuing the installation may be possible."
msgstr ""
"vmelilo 패키지를 /target/에 설치하는 데 실패했습니다. vmelilo를 부트로더로 설"
"치하는 일은 꼭 필요한 단계입니다.  하지만 이 문제가 vmelilo와는 관계없는 문제"
"일 수도 있으므로, 설치를 계속할 수도 있습니다."

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:4001
msgid "Looking for root partition..."
msgstr "루트 파티션을 찾는 중입니다..."

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:6001
msgid "Creating vmelilo configuration..."
msgstr "vmelilo 설정을 만드는 중입니다..."

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001
msgid "Failed to create vmelilo configuration"
msgstr "vmelilo 설정을 만드는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001
msgid "The creation of the vmelilo configuration file failed."
msgstr "vmelilo 설정 파일을 만드는 데 실패했습니다."

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:8001
msgid "Installing vmelilo into bootstrap partition..."
msgstr "vmelilo를 부트스트랩 파티션에 설치하는 중입니다..."

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:9001
msgid "The installation of the vmelilo boot loader failed."
msgstr "vmelilo 부트로더를 설치하는 데 실패했습니다."

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../vmelilo-installer.templates:10001
msgid "Install the vmelilo boot loader on a hard disk"
msgstr "하드 디스크에 vmelilo 부트로더 설치"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:1001
msgid ""
"No partitions were found in your system. You may need to partition your hard "
"drives or load additional kernel modules."
msgstr ""
"시스템에 파티션이 없습니다. 하드 드라이브의 파티션을 나눠야 하거나, 커널 모듈"
"을 추가로 읽어들여야 할 수 있습니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:2001
msgid "No file systems found"
msgstr "파일 시스템이 없습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:2001
msgid ""
"No usable file systems were found. You may need to load additional kernel "
"modules."
msgstr ""
"사용할 수 있는 파일 시스템이 없습니다. 커널 모듈을 추가로 읽어들여야 할 수 있"
"습니다."

#. Type: select
#. Choices
#. :sl5:
#: ../partconf.templates:3001
msgid "Abort"
msgstr "중지"

#. Type: select
#. Choices
#. :sl5:
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partconf.templates:4001
msgid "Leave the file system intact"
msgstr "파일 시스템 그대로 유지"

#. Type: select
#. Choices
#. :sl5:
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. Type: select
#. Choices
#. :sl5:
#. Note to translators : Please keep your translations of each choice
#. (separated by commas)
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#: ../partconf.templates:4001 ../partconf.templates:5001
msgid "Create swap space"
msgstr "스왑 공간 만들기"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../partconf.templates:4002 ../partconf.templates:5002
msgid "Action on ${PARTITION}:"
msgstr "${PARTITION}에 할 작업:"

#. Type: select
#. Description
#. :sl5:
#: ../partconf.templates:4002
msgid ""
"This partition seems to already have a file system (${FSTYPE}). You can "
"choose to leave this file system intact, create a new file system, or create "
"swap space."
msgstr ""
"이 파티션에는 이미 파일 시스템이 들어 있습니다 (${FSTYPE}). 이 파일 시스템을 "
"그대로 유지할 수도 있고, 파일 시스템을 새로 만들거나 스왑 공간을 만들 수 있습"
"니다."

#. Type: select
#. Description
#. :sl5:
#: ../partconf.templates:5002
msgid ""
"This partition does not seem to have a file system. You can create a file "
"system or swap space on it."
msgstr ""
"이 파티션에는 파일 시스템이 없습니다. 파일 시스템을 새로 만들거나 스왑 공간"
"을 만들 수 있습니다."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of each choice
#. (separated by commas)
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl5:
#. "it" is a partition
#: ../partconf.templates:6001
msgid "Don't mount it"
msgstr "마운트하지 않음"

#. Type: select
#. Description
#. :sl5:
#. Type: string
#. Description
#. :sl5:
#: ../partconf.templates:6002 ../partconf.templates:7001
msgid "Mount point for ${PARTITION}:"
msgstr "${PARTITION}의 마운트 위치:"

#. Type: select
#. Description
#. :sl5:
#: ../partconf.templates:6002
msgid ""
"When a partition is mounted, it is available to the users of the system. You "
"always need a root (\"/\") partition, and it's often good to have a separate "
"partition for /home."
msgstr ""
"파티션을 마운트해야 시스템에서 사용할 수 있습니다. 루트 파티션은 (\"/\") 꼭 "
"필요하며, /home은 별도 파티션으로 만드는 편이 좋습니다."

#. Type: string
#. Description
#. :sl5:
#: ../partconf.templates:7001
msgid "Please enter where the partition should be mounted."
msgstr "파티션을 마운트할 위치를 입력하십시오."

#. Type: boolean
#. Description
#. :sl5:
#: ../partconf.templates:8001
msgid "Do you want to unmount the partitions so you can make changes?"
msgstr "파티션을 수정할 수 있도록 파티션의 마운트를 해제하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../partconf.templates:8001
msgid ""
"Since the partitions have already been mounted, you cannot do any changes."
msgstr "파티션이 마운트되어 있으므로, 수정할 수 없습니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:9001
msgid "Failed to unmount partitions"
msgstr "파티션을 마운트 해제하는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:9001
msgid "An unexpected error occurred while unmounting the partitions."
msgstr "파티션의 마운트를 해제하는 데 알 수 없는 오류가 발생했습니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:9001
msgid "The partition configuration process has been aborted."
msgstr "파티션 설정 과정을 중단했습니다."

#. Type: text
#. Description
#. :sl5:
#: ../partconf.templates:10001
#, no-c-format
msgid "Create %s file system"
msgstr "%s 파일 시스템을 만듭니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:11001
msgid "No root partition (/)"
msgstr "루트 파일 시스템 (/) 없음"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:11001
msgid ""
"You need a root partition. Please assign a partition to the root mount point "
"before continuing."
msgstr ""
"루트 파일 시스템이 필요합니다. 계속 하시기 전에 파티션에 루트 마운트 위치를 "
"지정하십시오."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:12001
msgid "Partition assigned to ${MOUNT}"
msgstr "${MOUNT}에 마운트할 파티션"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:12001
msgid "Mounting a partition on ${MOUNT} makes no sense. Please change this."
msgstr "파티션을 ${MOUNT}에 마운트하는 건 맞지 않습니다. 바로잡으십시오."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:13001
msgid "Several partitions assigned to ${MOUNT}"
msgstr "${MOUNT}에 파티션 여러 개를 마운트했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:13001
msgid ""
"You cannot assign several partitions to the same mount point. Please change "
"all but one of them."
msgstr ""
"파티션 여러 개를 같은 마운트 위치에 마운트할 수 없습니다. 한 개만 남겨두고 마"
"운트 위치를 바꾸십시오."

#. Type: boolean
#. Description
#. :sl5:
#: ../partconf.templates:14001
msgid "Ready to create file systems and mount partitions?"
msgstr "파일 시스템을 만들고 파티션을 마운트할 준비가 되었습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../partconf.templates:14001
msgid "File systems will be created and partitions mounted."
msgstr "파일 시스템을 만들고 파티션을 마운트합니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../partconf.templates:14001
msgid ""
"WARNING: This will destroy all data on the partitions you have assigned file "
"systems to."
msgstr ""
"경고: 파일 시스템을 지정한 파티션에 있는 모든 데이터를 잃게 될 것입니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:15001
msgid "Failed to create swap space on ${PARTITION}"
msgstr "${PARTITION} 파티션에 스왑 공간을 만드는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:15001
msgid "An error occurred when the swap space was created on ${PARTITION}."
msgstr "${PARTITION} 파티션에 스왑 공간을 만드는 데 오류가 발생했습니다."

#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:15001 ../partconf.templates:16001
#: ../partconf.templates:17001 ../partconf.templates:18001
msgid ""
"Please check the error log on the third console or /var/log/messages for "
"more information."
msgstr ""
"자세한 정보는 세 번째 콘솔에서 오류 로그를 살펴 보시거나 /var/log/messages를 "
"살펴보십시오."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:16001
msgid "Failed to activate the swap space on ${PARTITION}"
msgstr "${PARTITION} 파티션에 스왑 공간을 활성화하는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:16001
msgid "An error occurred when the swap space on ${PARTITION} was activated."
msgstr "${PARTITION} 파티션의 스왑 공간을 활성화하는 데 오류가 발생했습니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:17001
msgid "Failed to create ${FS} file system on ${PARTITION}"
msgstr "${PARTITION} 파티션에 ${FS} 파일 시스템을 만드는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:17001
msgid ""
"An error occurred when the ${FS} file system was created on ${PARTITION}."
msgstr ""
"${PARTITION} 파티션에 ${FS} 파일 시스템을 만드는 데 오류가 발생했습니다."

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:18001
msgid "Failed to mount ${PARTITION} on ${MOUNT}"
msgstr "${MOUNT}에 ${PARTITION} 파티션을 마운트하는 데 실패했습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partconf.templates:18001
msgid "An error occurred when ${PARTITION} was mounted on ${MOUNT}."
msgstr "${MOUNT}에 ${PARTITION} 파티션을 마운트하는 데 오류가 발생했습니다."

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../partconf.templates:19001
msgid "Configure and mount partitions"
msgstr "파티션 설정 및 마운트"

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002
msgid "Disk to partition:"
msgstr "파티션할 디스크:"

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002
msgid "Please choose one of the listed disks to create partitions on it."
msgstr "디스크 목록에서 파티션을 만들 디스크를 하나 선택하십시오."

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002 ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "모두 마쳤으면 목록 아래에 있는 \"마치기\"를 선택하십시오."

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:2001
msgid "No disk found"
msgstr "디스크를 찾을 수 없습니다"

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:2001
msgid ""
"Unable to find any disk in your system. Maybe some kernel modules need to be "
"loaded."
msgstr ""
"시스템에 디스크를 하나도 찾을 수 없습니다. 커널 모듈을 읽어들여야 할 수 있습"
"니다."

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:3001
msgid "Partitioning error"
msgstr "파티션 오류"

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:3001
msgid "Failed to partition the disk ${DISC}."
msgstr "${DISC} 디스크를 파티션하는 데 실패했습니다."

#. Type: text
#. Description
#. :sl5:
#. Main menu item
#: ../partitioner.templates:4001
msgid "Partition a hard drive"
msgstr "하드 드라이브 파티션"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "ctc: Channel to Channel (CTC) or ESCON connection"
msgstr "ctc: 채널에서 채널 (CTC) 혹은 ESCON 연결"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "qeth: OSA-Express in QDIO mode / HiperSockets"
msgstr "qeth: OSA-Express (QDIO 모드) / HiperSockets"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "iucv: Inter-User Communication Vehicle - available for VM guests only"
msgstr "iucv: 사용자간 통신 장치 - VM 게스트만 사용 가능"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid "Network device type:"
msgstr "네트워크 장치 종류:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid ""
"Please choose the type of your primary network interface that you will need "
"for installing the Debian system (via NFS or HTTP). Only the listed devices "
"are supported."
msgstr ""
"데비안을 설치할 때 (NFS 혹은 HTTP를 통해) 사용할 주 네트워크 인터페이스의 종"
"류를 선택하십시오. 이 목록에 있는 장치만 지원합니다."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001
msgid "CTC read device:"
msgstr "CTC 읽기 장치:"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001 ../s390-netdevice.templates:3001
msgid "The following device numbers might belong to CTC or ESCON connections."
msgstr "다음 장치 번호는 CTC 혹은 ESCON 연결에 해당할 지도 모릅니다."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:3001
msgid "CTC write device:"
msgstr "CTC 쓰기 장치:"

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001 ../s390-netdevice.templates:8001
#: ../s390-netdevice.templates:13001
msgid "Do you accept this configuration?"
msgstr "이 설정대로 사용하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001
msgid ""
"The configured parameters are:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"
msgstr ""
"설정한 인수는 다음과 같습니다:\n"
" 읽기 채널     = ${device_read}\n"
" 쓰기 채널     = ${device_write}\n"
" 프로토콜      = ${protocol}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "No CTC or ESCON connections"
msgstr "CTC나 ESCON 연결이 없습니다"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "Please make sure that you have set them up correctly."
msgstr "올바르게 설정했는지 확인하십시오."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:6001
msgid "Protocol for this connection:"
msgstr "이 연결에 사용할 프로토콜:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Device:"
msgstr "장치:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Please select the OSA-Express QDIO / HiperSockets device."
msgstr "OSA-Express QDIO / HiperSockets 장치를 선택하십시오."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:8001
msgid ""
"The configured parameters are:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" portname = ${portname}\n"
" layer2   = ${layer2}"
msgstr ""
"설정한 인수는 다음과 같습니다:\n"
" 채널     = ${device0}, ${device1}, ${device2}\n"
" 포트     = ${port}\n"
" 포트이름 = ${portname}\n"
" 레이어2  = ${layer2}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid "No OSA-Express QDIO cards / HiperSockets"
msgstr "OSA-Express QDIO 카드 혹은 HiperSocket이 없습니다"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid ""
"No OSA-Express QDIO cards / HiperSockets were detected. If you are running "
"VM please make sure that your card is attached to this guest."
msgstr ""
"OSA-Express QDIO 카드 혹은 HiperSockets를 찾지 못했습니다. VM을 실행중이라면 "
"카드가 게스트에 붙어 있는 지 확인하십시오."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Portname:"
msgstr "포트이름:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid ""
"Please enter the portname of your OSA-Express card. This name must be 1 to 8 "
"characters long and must be equal on all systems accessing the same card."
msgstr ""
"OSA-Express 카드의 포트이름을 입력하십시오. 이 이름은 한 글자에서 여덟 글자 "
"사이여야 하고 같은 카드를 접근하는 모든 시스템에 대해 동일해야 합니다."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid ""
"Leave it empty if you want to use HiperSockets. This parameter is required "
"for cards with microcode level 2.10 or later or when you want to share a "
"card."
msgstr ""
"HiperSockets를 사용하려면 비워두십시오. 이 인수는 마이크로코드 레벨 2.10 혹"
"은 그 이후의 카드 아니면 카드를 공유할 때 필요합니다."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "The name will automatically be converted to uppercase."
msgstr "이름은 자동으로 대문자로 변환합니다."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Port:"
msgstr "포트:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Please enter a relative port for this connection."
msgstr "이 연결의 상대 포트를 입력하십시오."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid "Use this device in layer2 mode?"
msgstr "이 장치를 레이어2 모드로 사용하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid ""
"By default OSA-Express cards use layer3 mode. In that mode LLC headers are "
"removed from incoming IPv4 packets. Using the card in layer2 mode will make "
"it keep the MAC addresses of IPv4 packets."
msgstr ""
"OSA-Express 카드는 기본적으로 레이어3 모드를 사용합니다. 레이어3 모드에서는 "
"들어오는 IPv4 패킷에서 LLC 헤더를 지웁니다. 레이어2 모드를 사용하면 IPv4에 있"
"는 MAC 주소를 그대로 둡니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"The configured parameter is:\n"
" peer  = ${peer}"
msgstr ""
"설정한 사항은 다음과 같습니다:\n"
" 피어  = ${peer}"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "VM peer:"
msgstr "VM 피어:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "Please enter the name of the VM peer you want to connect to."
msgstr "연결하려는 VM 피어의 이름을 입력하십시오."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid ""
"If you want to connect to multiple peers, separate the names by colons, e."
"g.  tcpip:linux1."
msgstr ""
"피어 여러개에 연결하려면 각각의 이름을 콜론으로 구분하십시오. 예를 들어, "
"tcpip:linux1."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid ""
"The standard TCP/IP server name on VM is TCPIP; on VIF it's $TCPIP. Note: "
"IUCV must be enabled in the VM user directory for this driver to work and it "
"must be set up on both ends of the communication."
msgstr ""
"VM의 표준 TCP/IP 서버 이름은 TCPIP입니다. VIF에서는 $TCPIP입니다. 주의: VM 사"
"용자 디렉토리에서 IUCV를 켜야 이 드라이버가 동작합니다. 그리고 통신하는 양쪽 "
"모두에서 설정해야 합니다."

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../s390-netdevice.templates:15001
msgid "Configure the network device"
msgstr "네트워크 장치 설정"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available disks:"
msgstr "사용할 수 있는 디스크:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following disk access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"다음 disk access storage device(DASD)를 사용할 수 있습니다.  사용하려는 장치"
"를 하나씩 선택하십시오."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose disk:"
msgstr "디스크를 선택하십시오:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a disk. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"디스크를 선택하십시오. 앞에 붙어 있는 0까지도 다 포함해서, 완전한 장치 번호"
"를 지정해야 합니다."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid disk"
msgstr "디스크가 잘못되었습니다"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "잘못된 장치 번호를 선택했습니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "Format the disk?"
msgstr "디스크를 포맷하시겠습니까?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"The installer is unable to detect if the device ${device} has already been "
"formatted or not. Disks need to be formatted before you can create "
"partitions."
msgstr ""
"${device} 장치에 이미 포맷을 했는지 알아낼 수 없습니다. 파티션을 만들기 전에 "
"디스크를 포맷해야 합니다."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"If you are sure the disk has already been correctly formatted, you don't "
"need to do so again."
msgstr ""
"디스크가 올바르게 포맷한 상태라고 확신한다면, 다시 포맷할 필요는 없습니다."

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid "Formatting ${device}..."
msgstr "${device} 장치를 포맷하는 중입니다..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "Configure disk access storage devices (DASD)"
msgstr "DASD (disk access storage device) 설정"

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../zipl-installer.templates:1001
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "하드 디스크에 ZIPL 부트로더 설치"
