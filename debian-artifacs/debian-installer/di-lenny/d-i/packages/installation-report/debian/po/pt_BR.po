# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-08-07 19:53-0300\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../save-logs.templates:1001
msgid "Save debug logs"
msgstr "Gravar logs de depuração"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "floppy"
msgstr "disquete"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "web"
msgstr "web"

#. Type: select
#. Choices
#. Possible locations for debug logs to be saved
#. :sl2:
#: ../save-logs.templates:2001
msgid "mounted file system"
msgstr "sistema de arquivos montado"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid "How should the debug logs be saved or transferred?"
msgstr "Como os log de depuração devem ser gravados ou transferidos?"

#. Type: select
#. Description
#. :sl2:
#: ../save-logs.templates:2002
msgid ""
"Debugging log files for the installer can be saved to floppy, served up over "
"the web, or saved to a mounted file system."
msgstr ""
"Arquivos de log de depuração do instalador podem ser gravados em um "
"disquete, servidor através da Web ou gravados em um sistema de arquivos "
"montado."

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid "Directory in which to save debug logs:"
msgstr "Diretório no qual gravar logs de depuração:"

#. Type: string
#. Description
#. :sl2:
#: ../save-logs.templates:3001
msgid ""
"Please make sure the file system you want to save debug logs on is mounted "
"before you continue."
msgstr ""
"Por favor, certifique-se de que o sistema de arquivos no qual você deseja "
"gravar os logs de depuração esteja montado antes de prosseguir."

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "Cannot save logs"
msgstr "Não foi possível gravar os logs"

#. Type: error
#. Description
#. :sl2:
#: ../save-logs.templates:4001
msgid "The directory \"${DIR}\" does not exist."
msgstr "O diretório \"${DIR}\" não existe."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid "Web server started, but network not running"
msgstr "Servidor Web iniciado, mas rede não está em funcionamento."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:5001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. However, the network is not set up yet. The web server will be "
"left running, and will be accessible once the network is configured."
msgstr ""
"Um servidor Web simples foi iniciado neste computador para servir os "
"arquivos de log e informações de depuração. Porém, a rede ainda não foi "
"configurada. O servidor Web será mantido em execução e será acessível uma "
"vez que a rede esteja configurada."

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid "Web server started"
msgstr "Servidor Web iniciado"

#. Type: note
#. Description
#. :sl3:
#: ../save-logs.templates:6001
msgid ""
"A simple web server has been started on this computer to serve log files and "
"debug info. An index of all the available log files can be found at http://"
"${ADDRESS}/"
msgstr ""
"Um servidor Web simples foi iniciado neste computador para servir os "
"arquivos de log e informações de depuração. Um índice de todos os arquivos "
"de log disponíveis pode ser encontrado em http://${ADDRESS}/"

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Insert formatted floppy in drive"
msgstr "Insira um disquete formatado no leitor de disquetes"

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid "Log files and debug info will be copied into this floppy."
msgstr ""
"Arquivos de log e informações de depuração serão copiados para esse disquete."

#. Type: note
#. Description
#. :sl2:
#: ../save-logs.templates:7001
msgid ""
"The information will also be stored in /var/log/installer/ on the installed "
"system."
msgstr ""
"As informações também serão armazenadas em /var/log/installer/ no sistema "
"instalado."

#. Type: error
#. Description
#: ../save-logs.templates:8001
msgid "Failed to mount the floppy"
msgstr "Falha ao montar o disquete"

#. Type: error
#. Description
#: ../save-logs.templates:8001
msgid ""
"Either the floppy device cannot be found, or a formatted floppy is not in "
"the drive."
msgstr ""
"O dispositivo de disquete não pôde ser encontrado ou não existe um disquete "
"formatado na unidade."

#. Type: text
#. Description
#. :sl1:
#. finish-install progress bar item
#: ../save-logs.templates:9001
msgid "Gathering information for installation report..."
msgstr "Obtendo informação para relatório de instalação..."
