# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of ro.po to Romanian
# Romanian translation
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
# Eddy Petrișor <eddy.petrisor@gmail.com>, 2004, 2005, 2006, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: ro\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-08-06 20:14+0000\n"
"PO-Revision-Date: 2008-08-07 01:13+0300\n"
"Last-Translator: Eddy Petrișor <eddy.petrisor@gmail.com>\n"
"Language-Team: Romanian <debian-l10n-romanian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../mdcfg-utils.templates:1001
msgid "Configure MD devices"
msgstr "Configurează dispozitivele MD"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid "Multidisk (MD) not available"
msgstr "Multidisc (MD) indisponibil"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:2001
msgid ""
"The current kernel doesn't seem to support multidisk devices. This should be "
"solved by loading the needed modules."
msgstr ""
"Nucleul curent nu suportă dispozitive multidisc. Acest lucru ar trebui să se "
"rezolve, dacă încărcați modulele necesare."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Create MD device"
msgstr "Creează dispozitiv MD"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Delete MD device"
msgstr "Șterge dispozitiv MD"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../mdcfg-utils.templates:3001
msgid "Finish"
msgstr "Finalizează"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "Multidisk configuration actions"
msgstr "Acțiuni de configurare multidisc"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid "This is the Multidisk (MD) and software RAID configuration menu."
msgstr ""
"Acesta este meniul de configurare al Multidisc-ului (MD) și al RAID-ului "
"software."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:3002
msgid ""
"Please select one of the proposed actions to configure multidisk devices."
msgstr ""
"Vă rugăm să selectați una din acțiunile propuse pentru a configura "
"dispozitivele multidisc."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid "No RAID partitions available"
msgstr "Nu s-au găsit partiții RAID disponibile"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"No unused partitions of the type \"Linux RAID Autodetect\" are available. "
"Please create such a partition, or delete an already used multidisk device "
"to free its partitions."
msgstr ""
"Nu este disponibilă nici o partiție nefolosită de tipul „Linux RAID "
"Autodetect”. Vă rugăm să creați o astfel de partiție, sau să să ștergeți un "
"dispozitiv multidisc deja folosit pentru a-i elibera partițiile."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:4001
msgid ""
"If you have such partitions, they might contain actual file systems, and are "
"therefore not available for use by this configuration utility."
msgstr ""
"Dacă aveți asemenea partiții, este posibil să conțină sisteme de fișiere, "
"deci nu sunt disponibile pentru utilizare în această unealtă de configurare."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid "Not enough RAID partitions available"
msgstr "Nu s-au găsit destule partiții RAID disponibile"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:5001
msgid ""
"There are not enough RAID partitions available for your selected "
"configuration.  You have ${NUM_PART} RAID partitions available but your "
"configuration requires ${REQUIRED} partitions."
msgstr ""
"Nu există destule partiții RAID disponibile pentru configurația selectată de "
"dumneavoastră. Aveți ${NUM_PART} partiții RAID disponibile, dar configurația "
"dumneavoastră are nevoie de ${REQUIRED} partiții."

#. Type: select
#. Choices
#. :sl3:
#. Type: select
#. Choices
#. :sl3:
#: ../mdcfg-utils.templates:6001 ../mdcfg-utils.templates:13001
msgid "Cancel"
msgstr "Anulează"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Multidisk device type:"
msgstr "Tipul dispozitivului multidisc:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:6002
msgid "Please choose the type of the multidisk device to be created."
msgstr "Vă rugăm alegeți tipul de dispozitiv multidisc care va fi creat."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid "Active devices for the RAID0 multidisk device:"
msgstr "Dispozitivele active pentru dispozitivul multidisc RAID0:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:7001
msgid ""
"You have chosen to create a RAID0 array. Please choose the active devices in "
"this array."
msgstr ""
"Ați ales să creați o matrice RAID0. Vă rugăm să alegeți dispozitivele active "
"din această matrice."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid "Number of active devices for the RAID${LEVEL} array:"
msgstr "Numărul de dispozitive active pentru matricea RAID${LEVEL}:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001
msgid ""
"The RAID${LEVEL} array will consist of both active and spare partitions. The "
"active partitions are those used, while the spare devices will only be used "
"if one or more of the active devices fail. A minimum of ${MINIMUM} active "
"devices is required."
msgstr ""
"Matricea RAID${LEVEL} va fi compusă atât din partiții active cât și din "
"partiții de rezervă. Cele active sunt cele utilizate, în timp ce partițiile "
"de rezervă vor fi folosite doar dacă una sau mai multe partiții active "
"eșuează. Este necesar un minim de ${MINIMUM} dispozitive active."

#. Type: string
#. Description
#. :sl3:
#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:8001 ../mdcfg-utils.templates:12001
msgid "NOTE: this setting cannot be changed later."
msgstr "NOTĂ: această configurație nu poate fi schimbată mai târziu."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid "Active devices for the RAID${LEVEL} multidisk device:"
msgstr "Dispozitivele active pentru dispozitivul multidisc RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} active devices."
msgstr "Ați ales să creați o matrice RAID${LEVEL} cu ${COUNT} partiții active."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:9001
msgid ""
"Please choose which partitions are active devices. You must select exactly "
"${COUNT} partitions."
msgstr ""
"Vă rugăm să alegeți care partiții sunt dispozitive active. Trebuie să "
"selectați exact ${COUNT} partiții."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:10001
msgid "Number of spare devices for the RAID${LEVEL} array:"
msgstr "Numărul de dispozitive de rezervă pentru matricea RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid "Spare devices for the RAID${LEVEL} multidisk device:"
msgstr "Dispozitive de rezervă pentru dispozitivul multidisc RAID${LEVEL}:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"You have chosen to create a RAID${LEVEL} array with ${COUNT} spare devices."
msgstr ""
"Ați ales să creați o matrice RAID${LEVEL} cu ${COUNT} partiții de rezervă."

#. Type: multiselect
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:11001
msgid ""
"Please choose which partitions will be used as spare devices. You may choose "
"up to ${COUNT} partitions. If you choose less than ${COUNT} devices, the "
"remaining partitions will be added to the array as \"missing\". You will be "
"able to add them later to the array."
msgstr ""
"Vă rugăm să alegeți care partiții vor fi folosite ca dispozitive de rezervă. "
"Puteți alege până la ${COUNT} dispozitive. Dacă alegeți mai puțin de "
"${COUNT} dispozitive, partițiile rămase vor fi adăugate la matrice ca "
"„lipsă”. Veți putea să le adăugați mai târziu la matrice."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid "Layout of the RAID10 multidisk device:"
msgstr "Aranjamentul dispozitivului multidisc RAID10:"

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The layout must be n, o, or f (arrangement of the copies) followed by a "
"number (number of copies of each chunk). The number must be smaller or equal "
"to the number of active devices."
msgstr ""
"Aranjamentul trebuie n, o sau f (aranjament pentru copii) urmat de un număr "
"(numărul de copii ai fiecărei părți). Numărul trebuie să fie mai mic sau "
"egal cu numărul de dispozitive active."

#. Type: string
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:12001
msgid ""
"The letter is the arrangement of the copies:\n"
" n - near copies: Multiple copies of one data block are at similar\n"
"     offsets in different devices.\n"
" f - far copies: Multiple copies have very different offsets\n"
" o - offset copies: Rather than the chunks being duplicated within a\n"
"     stripe, whole stripes are duplicated but are rotated by one\n"
"     device so duplicate blocks are on different devices."
msgstr ""
"Litera reprezintă aranjamentul copiilor:\n"
" n - copii apropiate: Multiple copii ale unui bloc de date se află\n"
"     la deplasamente similare în diversele dispozitive.\n"
" f - copii îndepărtate: Multiple copii au deplasamente foarte\n"
"     diferite\n"
" o - copii deplasate: În loc ca bucățile să fie duplicate într-o \n"
"     „fâșie”, toate „fâșiile” sunt duplicate, dar sunt rotite cu\n"
"     un dispozitiv astfel încât blocurile duplicate să se afle pe\n"
"     dispozitive diferite."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid "Multidisk device to be deleted:"
msgstr "Dispozitivul multidisc ce va fi șters:"

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Deleting a multidisk device will stop it and clear the superblock of all its "
"components."
msgstr ""
"Ștergerea unui dispozitiv multidisc îl va opri pe acesta și va șterge "
"superblocurile tuturor componentelor sale."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"Please note this will not immediately allow you to reuse the partitions or "
"devices in a new multidisk device. The array will however be unusable after "
"the deletion."
msgstr ""
"Această operație nu vă va permite imediat să reutilizați partițiile sau "
"dispozitivele într-un nou dispozitiv multidisc. Matricea însă va fi "
"inutilizabilă după ștergere."

#. Type: select
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:13002
msgid ""
"If you select a device for deletion, you will get some information about it "
"and you will be given the option of aborting this operation."
msgstr ""
"Dacă selectați un dispozitiv pentru ștergere, veți obține câteva informații "
"despre el și vi se va oferi posibilitatea de a abandona acestă operație."

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices available"
msgstr "Nu există dispozitive multidisc disponibile"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:14001
msgid "No multidisk devices are available for deletion."
msgstr "Nu există dispozitive multidisc disponibile pentru ștergere."

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid "Really delete this multidisk device?"
msgstr "Șterg într-adevăr acest dispozitiv multidisc?"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
"Please confirm whether you really want to delete the following multidisk "
"device:"
msgstr ""
"Vă rugăm să confirmați dacă doriți într-adevăr să ștergeți următoarele "
"dispozitive multidisc:"

#. Type: boolean
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:15001
msgid ""
" Device:            ${DEVICE}\n"
" Type:              ${TYPE}\n"
" Component devices:"
msgstr ""
" Dispozitiv:             ${DEVICE}\n"
" Tip:                    ${TYPE}\n"
" Dispozitive componente:"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "Failed to delete the multidisk device"
msgstr "Eșec la ștergera dispozitivului multidisc"

#. Type: error
#. Description
#. :sl3:
#: ../mdcfg-utils.templates:16001
msgid "There was an error deleting the multidisk device. It may be in use."
msgstr ""
"A apărut o eroare la ștergerea dispozitivului multidisc. Acesta ar putea fi "
"folosit în momentul de față."
