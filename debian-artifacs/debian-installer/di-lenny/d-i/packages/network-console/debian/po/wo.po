# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of wo.po to Wolof
#
#
# Mouhamadou Mamoune Mbacke <mouhamadoumamoune@gmail.com>, 2005, 2006, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: wo\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-21 22:51+0000\n"
"PO-Revision-Date: 2008-09-12 23:29+0200\n"
"Last-Translator: Mouhamadou Mamoune Mbacke <mouhamadoumamoune@gmail.com>\n"
"Language-Team: Wolof <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:1001
msgid "Continue installation remotely using SSH"
msgstr "Eggale istalaasioŋ bi ci ak soree jëfandikoo SSH"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer"
msgstr "Tambule prograamu istalaasioŋ bi"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer (expert mode)"
msgstr "Tambule prograamu istalaasioŋ bi (aw waane)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start shell"
msgstr "Tambule shell"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "Network console option:"
msgstr "Tanniiti konsol u resóo:"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid ""
"This is the network console for the Debian installer. From here, you may "
"start the Debian installer, or execute an interactive shell."
msgstr ""
"Lii mooy konsol bu resóo bu prograamu istalaasioŋ bu Debian. Man ngaa tannee "
"fii tambule prograamu istalaasioŋ bu Debian walla doxal ab shell bu "
"interaktiif."

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "To return to this menu, you will need to log in again."
msgstr "Ngir dellusi ci bii menu, da nga soxlawaata login."

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:3001
msgid "Generating SSH host key"
msgstr "Mi ngi amal ab caabi masin SSH "

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid "Remote installation password:"
msgstr "Baatujall bu istalaasioŋ ci ak sori:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"You need to set a password for remote access to the Debian installer. A "
"malicious or unqualified user with access to the installer can have "
"disastrous results, so you should take care to choose a password that is not "
"easy to guess. It should not be a word found in the dictionary, or a word "
"that could be easily associated with you, like your middle name."
msgstr ""
"Boo nekkee fu sori bëgg a aksi ci Debian installer, da ngaa wara joxe ab "
"baatujall. Ndax ab saaysaay walla ku amul ndigel bu aksii ci installer bi "
"man naa and toloftolf yu mag, moo tax nanga teeylu daldi tann ab baatujall "
"bu jafee jabbutu. Warul a doon ab baat bu am ci diksoneer bi, walla ab baat "
"bu yomb a lëŋkale ak yew, lu mel ne saw tur."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"This password is used only by the Debian installer, and will be discarded "
"once you finish the installation."
msgstr ""
"Baatujall bii Debian installer rekk moo koy jëfandikoo, deesnako wacci saa "
"bu istalaasioŋ bi jeexee."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid "Re-enter password to verify:"
msgstr "Dugëlaatal baatujall bi ngir nu wóorlu:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid ""
"Please enter the same remote installation password again to verify that you "
"have typed it correctly."
msgstr "Defaatal benn baatujall bu istalaasioŋ bi ngir wóorlu ne juumóoci."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid "Empty password"
msgstr "Baatujall bu deful dara"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Da nga dugël baatujall bu deful dara, ta loolu deesuko naŋgu. Kon tann "
"laneen ludul baatujall bu deful dara."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid "Password mismatch"
msgstr "Baatujall yi dëppóowuñu"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid ""
"The two passwords you entered were not the same. Please enter a password "
"again."
msgstr "Ñaari baatujall yi nga def bokkuñu. Joxewaatal ab baatujall."

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "Start SSH"
msgstr "Tambuli SSH"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"To continue the installation, please use an SSH client to connect to the IP "
"address ${ip} and log in as the \"installer\" user. For example:"
msgstr ""
"Ngir eggale istalaasioŋ bi, jëfandikool ab kiliyaŋ SSH ngir jokkoo ak adrees "
"IP bu ${ip} ta ubbi jataay ci turu \"installer\". Ci misaal:"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "The fingerprint of this SSH server's host key is: ${fingerprint}"
msgstr ""
"Taampebaaraam bu caabi bu masin u serwór u  SSH bi mi ngi doon: "
"${fingerprint}"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"Please check this carefully against the fingerprint reported by your SSH "
"client."
msgstr "Meŋgaleel loolu ci ak teey ak taampebaaram bi sa kiliyaŋ bu SSH joxe."
