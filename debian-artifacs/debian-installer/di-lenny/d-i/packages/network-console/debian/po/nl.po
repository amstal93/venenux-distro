# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of nl.po to Dutch
# Dutch messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Frans Pop <aragorn@tiscali.nl>, 2005.
# Frans Pop <elendil@planet.nl>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: nl\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-21 22:51+0000\n"
"PO-Revision-Date: 2008-07-16 18:33+0200\n"
"Last-Translator: Frans Pop <elendil@planet.nl>\n"
"Language-Team: Dutch <debian-l10n-dutch@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:1001
msgid "Continue installation remotely using SSH"
msgstr "Installatie verder van op afstand doorlopen via SSH"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer"
msgstr "Installatieprogramma opstarten"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer (expert mode)"
msgstr "Installatieprogramma opstarten (expert-modus)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start shell"
msgstr "Shell opstarten"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "Network console option:"
msgstr "Netwerkconsoleoptie:"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid ""
"This is the network console for the Debian installer. From here, you may "
"start the Debian installer, or execute an interactive shell."
msgstr ""
"Dit is de netwerkconsole van het Debian-installatieprogramma. Van hieruit "
"kunt u of het Debian-installatieprogramma, of een interactieve shell "
"opstarten."

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "To return to this menu, you will need to log in again."
msgstr "Om naar dit menu terug te keren dient u opnieuw aan te melden."

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:3001
msgid "Generating SSH host key"
msgstr "SSH-computersleutel wordt gegenereerd"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid "Remote installation password:"
msgstr "Wat is het wachtwoord voor de installatie op afstand?"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"You need to set a password for remote access to the Debian installer. A "
"malicious or unqualified user with access to the installer can have "
"disastrous results, so you should take care to choose a password that is not "
"easy to guess. It should not be a word found in the dictionary, or a word "
"that could be easily associated with you, like your middle name."
msgstr ""
"U dient een wachtwoord in te stellen waarmee u toegang kunt krijgen tot het "
"Debian-installatieprogramma van op afstand. Als een kwaadaardige of niet-"
"gekwalificeerde gebruiker toegang verkrijgt tot het installatieprogramma kan "
"dit desastreuze gevolgen hebben. Het is daarom belangrijk dat u een goed "
"wachtwoord kiest dat moeilijk te raden is (dus geen woord uit het "
"woordenboek, of iets dat makkelijk met u te associëren valt zoals "
"bijvoorbeeld uw tweede voornaam). "

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"This password is used only by the Debian installer, and will be discarded "
"once you finish the installation."
msgstr ""
"Dit wachtwoord wordt enkel gebruikt door het Debian-installatieprogramma en "
"is na de installatie niet meer geldig. "

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid "Re-enter password to verify:"
msgstr "Wachtwoord nogmaals (ter bevestiging):"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid ""
"Please enter the same remote installation password again to verify that you "
"have typed it correctly."
msgstr ""
"Ter controle op eventuele typfouten dient u het wachtwoord voor de "
"installatie op afstand nogmaals in te voeren."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid "Empty password"
msgstr "Leeg wachtwoord"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"U heeft een leeg wachtwoord ingevoerd, dit is niet toegestaan. U dient een "
"niet-leeg wachtwoord te kiezen."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid "Password mismatch"
msgstr "Wachtwoorden komen niet overeen"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid ""
"The two passwords you entered were not the same. Please enter a password "
"again."
msgstr ""
"De twee door u ingevoerde wachtwoorden zijn komen niet overeen. Gelieve het "
"wachtwoord opnieuw in te voeren."

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "Start SSH"
msgstr "SSH opstarten"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"To continue the installation, please use an SSH client to connect to the IP "
"address ${ip} and log in as the \"installer\" user. For example:"
msgstr ""
"Maak, om de installatie te vervolgen, met een SSH-client verbinding met IP-"
"adres ${ip} en meld u aan als de gebruiker 'installer'. Bijvoorbeeld:"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "The fingerprint of this SSH server's host key is: ${fingerprint}"
msgstr ""
"De vingerafdruk van de computersleutel van deze SSH-server is: ${fingerprint}"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"Please check this carefully against the fingerprint reported by your SSH "
"client."
msgstr ""
"Deze dient overeen te komen met de vingerafdruk die aangegeven wordt door uw "
"SSH-client. U kunt dit best nauwkeurig controleren."
