# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Lithuanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004...2008.
# Marius Gedminas <mgedmin@b4net.lt>, 2004.
# Darius Skilinskas <darius10@takas.lt>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-21 22:51+0000\n"
"PO-Revision-Date: 2008-07-05 10:40+0300\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:1001
msgid "Continue installation remotely using SSH"
msgstr "Įdiegimą tęsti nutolus, naudojant SSH protokolą"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer"
msgstr "Paleisti įdiegiklį"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start installer (expert mode)"
msgstr "Paleisti įdiegiklį (veiksena ekspertams)"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl2:
#: ../network-console.templates:2001
msgid "Start shell"
msgstr "Vykdyti apvalkalą (shell)"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "Network console option:"
msgstr "Tinklo konsolės parinktys:"

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid ""
"This is the network console for the Debian installer. From here, you may "
"start the Debian installer, or execute an interactive shell."
msgstr ""
"Tai yra Debian'o įdiegiklio tinklo konsolė. Iš čia galite paleisti Debian'o "
"įdiegiklį, arba paleisti vykdyti interaktyvų apvalkalą (shell)."

#. Type: select
#. Description
#. :sl2:
#: ../network-console.templates:2002
msgid "To return to this menu, you will need to log in again."
msgstr "Norėdami grįžti į šį meniu, turėsite vėl prisijungti (log in)."

#. Type: text
#. Description
#. :sl2:
#: ../network-console.templates:3001
msgid "Generating SSH host key"
msgstr "SSH kompiuterio rakto (host key) generavimas"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid "Remote installation password:"
msgstr "Nutolusio įdiegimo slaptažodis:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"You need to set a password for remote access to the Debian installer. A "
"malicious or unqualified user with access to the installer can have "
"disastrous results, so you should take care to choose a password that is not "
"easy to guess. It should not be a word found in the dictionary, or a word "
"that could be easily associated with you, like your middle name."
msgstr ""
"Nutolusiam priėjimui prie Debian'o įdiegiklio turite nurodyti slaptažodį. "
"Piktavalis arba nekvalifikuotas naudotojas, turintis priėjimą prie "
"įdiegiklio, gali sukelti pražūtingas pasekmes, taigi turite stengtis "
"pasirinkti tokį slaptažodį, kurį sunku būtų atspėti. Tai neturėtų būti "
"randamas žodyne, arba lengvai asocijuojamas su jumis, žodis."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:4001
msgid ""
"This password is used only by the Debian installer, and will be discarded "
"once you finish the installation."
msgstr ""
"Šis slaptažodis naudojamas tik Debian'o įdiegiklyje, ir jis bus pašalintas "
"kai tik baigsite įdiegimą."

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid "Re-enter password to verify:"
msgstr "Dar kartą įveskite slaptažodį patikrinimui:"

#. Type: password
#. Description
#. :sl2:
#: ../network-console.templates:5001
msgid ""
"Please enter the same remote installation password again to verify that you "
"have typed it correctly."
msgstr ""
"Įvedimo teisingumui patikrinti pakartotinai įveskite nutolusio įdiegimo "
"slaptažodį."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid "Empty password"
msgstr "Tuščias slaptažodis"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:6001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Įvedėte tuščią slaptažodį, tačiau tai neleistina. Pasirinkite netuščią "
"slaptažodį."

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid "Password mismatch"
msgstr "Slaptažodis nesutampa"

#. Type: error
#. Description
#. :sl2:
#: ../network-console.templates:7001
msgid ""
"The two passwords you entered were not the same. Please enter a password "
"again."
msgstr "Du slaptažodžiai, kuriuos įvedėte, nevienodi. Įveskite slaptažodį vėl."

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "Start SSH"
msgstr "Paleisti vykdyti SSH"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"To continue the installation, please use an SSH client to connect to the IP "
"address ${ip} and log in as the \"installer\" user. For example:"
msgstr ""
"Įdiegimui tęsti, naudokite SSH klientinę programą prisijungimui prie "
"kompiuterio IP adresu ${ip} ir prisijunkite (log in) naudotoju \"installer\"."
"Pavyzdžiui:"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid "The fingerprint of this SSH server's host key is: ${fingerprint}"
msgstr ""
"Šio SSH serverio rakto (host key) atspaudas (fingerprint): ${fingerprint}"

#. Type: note
#. Description
#. :sl2:
#: ../network-console.templates:8001
msgid ""
"Please check this carefully against the fingerprint reported by your SSH "
"client."
msgstr ""
"Įdėmiai sulyginkite šį atspaudą su atspaudu, kurį parodo Jūsų SSH klientinė "
"programa."
