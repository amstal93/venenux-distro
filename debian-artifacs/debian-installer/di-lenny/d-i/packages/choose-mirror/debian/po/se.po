# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of se.po to Northern Saami
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Børre Gaup <boerre@skolelinux.no>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: se\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2006-11-25 21:21+0100\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Saami <i18n-sme@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl2:
#: ../choose-mirror-bin.templates-in:3001
#, fuzzy
msgid "stable"
msgstr "ii geavahahtti"

#. Type: select
#. Choices
#. :sl2:
#: ../choose-mirror-bin.templates-in:3001
msgid "testing"
msgstr ""

#. Type: select
#. Choices
#. :sl2:
#: ../choose-mirror-bin.templates-in:3001
#, fuzzy
msgid "unstable"
msgstr "ii geavahahtti"

#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates-in:3002
#, fuzzy
msgid "Debian version to install:"
msgstr "Čoahkku maid sajáiduhttit:"

#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates-in:3002
msgid ""
"Debian comes in several flavors. Stable is well-tested and rarely changes. "
"Unstable is untested and frequently changing. Testing is a middle ground, "
"that receives many of the new versions from unstable if they are not too "
"buggy."
msgstr ""

#. Type: text
#. Description
#. :sl1:
#: ../choose-mirror-bin.templates-in:5001
msgid "Checking the Debian archive mirror"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: text
#. Description
#. :sl1:
#: ../choose-mirror-bin.templates-in:6001
msgid "Downloading the Release file..."
msgstr "Viežžamin Release-fiilla …"

#. Type: error
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates-in:7001
#, fuzzy
msgid "Bad archive mirror"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: error
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates-in:7001
#, fuzzy
msgid ""
"The specified Debian archive mirror is either not available, or does not "
"have a valid Release file on it. Please try a different mirror."
msgstr ""
"Válljejuvvon Debian-arkiivaspeajal ii oro doarjumin du arkitektuvrra. "
"Geahččal eará speadjala."

#. Type: error
#. Description
#. :sl3:
#: ../choose-mirror-bin.templates-in:8001
msgid "Architecture not supported"
msgstr "Arkitektuvra ii dorjojuvvo"

#. Type: error
#. Description
#. :sl3:
#: ../choose-mirror-bin.templates-in:8001
msgid ""
"The specified Debian archive mirror does not seem to support your "
"architecture. Please try a different mirror."
msgstr ""
"Válljejuvvon Debian-arkiivaspeajal ii oro doarjumin du arkitektuvrra. "
"Geahččal eará speadjala."

#. Type: text
#. Description
#. main-menu
#. :sl1:
#: ../choose-mirror-bin.templates-in:9001
msgid "Choose a mirror of the Debian archive"
msgstr "Vállje Debian-arkiivva speadjala"

#. Type: select
#. Choices
#. :sl1:
#. Type: select
#. Choices
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:2001
#: ../choose-mirror-bin.templates.ftp.sel-in:2001
#, fuzzy
msgid "enter information manually"
msgstr "čális ieš dieđuid"

#. Type: select
#. Default
#. Translators, you should put here the ISO 3166 code of a country
#. which you know hosts at least one Debian HTTP mirror. Please check
#. that the country really has a Debian HTTP mirror before putting a
#. random value here
#.
#. First check that the country you mention here is listed in
#. http://cvs.debian.org/ *checkout* /webwml/english/mirror/Mirrors.masterlist?rev=HEAD\&cvsroot=webwml&content-type=text/plain
#. (remove the spaces between "*" and "/")
#.
#. BE CAREFUL to use the TWO LETTER ISO-3166 CODE and not anything else
#.
#. You do not need to translate what's between the square brackets
#. You should even NOT put square brackets in translations:
#. msgid "US[ Default value for http]"
#. msgstr "FR"
#. :sl1:
#: ../choose-mirror-bin.templates.http-in:2002
msgid "US[ Default value for http]"
msgstr "US"

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:2003
#: ../choose-mirror-bin.templates.ftp.sel-in:2003
#, fuzzy
msgid "Debian archive mirror country:"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:2003
#: ../choose-mirror-bin.templates.ftp.sel-in:2003
msgid ""
"The goal is to find a mirror of the Debian archive that is close to you on "
"the network -- be aware that nearby countries, or even your own, may not be "
"the best choice."
msgstr ""

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:3001
#: ../choose-mirror-bin.templates.ftp.sel-in:3001
#, fuzzy
msgid "Debian archive mirror:"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:3001
#: ../choose-mirror-bin.templates.ftp.sel-in:3001
msgid ""
"Please select a Debian archive mirror. You should use a mirror in your "
"country or region if you do not know which mirror has the best Internet "
"connection to you."
msgstr ""

#. Type: select
#. Description
#. :sl1:
#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:3001
#: ../choose-mirror-bin.templates.ftp.sel-in:3001
msgid "Usually, ftp.<your country code>.debian.org is a good choice."
msgstr ""

#. Type: string
#. Description
#. :sl1:
#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:4001
#: ../choose-mirror-bin.templates.ftp.base-in:2001
#, fuzzy
msgid "Debian archive mirror hostname:"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: string
#. Description
#. :sl1:
#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:4001
#: ../choose-mirror-bin.templates.ftp.base-in:2001
#, fuzzy
msgid ""
"Please enter the hostname of the mirror from which Debian will be downloaded."
msgstr ""
"Čális seamma beassansáni ođđasit nannen dihte ahte lea rievttes beassansátni."

#. Type: string
#. Description
#. :sl1:
#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:4001
#: ../choose-mirror-bin.templates.ftp.base-in:2001
msgid ""
"An alternate port can be specified using the standard [hostname]:[port] "
"format."
msgstr ""

#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:5001
#: ../choose-mirror-bin.templates.ftp.base-in:3001
#, fuzzy
msgid "Debian archive mirror directory:"
msgstr "Dárkkisteamen Debian-arkiivva speadjala"

#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:5001
#: ../choose-mirror-bin.templates.ftp.base-in:3001
msgid ""
"Please enter the directory in which the mirror of the Debian archive is "
"located."
msgstr ""

#. Type: string
#. Description
#. :sl1:
#: ../choose-mirror-bin.templates.http-in:6001
msgid "HTTP proxy information (blank for none):"
msgstr "HTTP proxy-dieđut (guorisin proxy haga):"

#. Type: string
#. Description
#. :sl1:
#: ../choose-mirror-bin.templates.http-in:6001
msgid ""
"If you need to use a HTTP proxy to access the outside world, enter the proxy "
"information here. Otherwise, leave this blank."
msgstr ""
"Jus fertet geavahit HTTP-proxy beassan dihte olggobeal máilbmái, čális "
"proxy- dieđuid dása. Guođe guorusin muđui."

#. Type: string
#. Description
#. :sl1:
#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.http-in:6001
#: ../choose-mirror-bin.templates.ftp.base-in:4001
msgid ""
"The proxy information should be given in the standard form of \"http://"
"[[user][:pass]@]host[:port]/\"."
msgstr ""

#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.ftp.base-in:4001
#, fuzzy
msgid "FTP proxy information (blank for none):"
msgstr "HTTP proxy-dieđut (guorisin proxy haga):"

#. Type: string
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.ftp.base-in:4001
#, fuzzy
msgid ""
"If you need to use a FTP proxy to access the outside world, enter the proxy "
"information here. Otherwise, leave this blank."
msgstr ""
"Jus fertet geavahit HTTP-proxy beassan dihte olggobeal máilbmái, čális "
"proxy- dieđuid dása. Guođe guorusin muđui."

#. Type: select
#. Default
#. Translators, you should put here the ISO 3166 code of a country
#. which you know hosts at least one Debian FTP mirror. Please check
#. that the country really has a Debian FTP mirror before putting a
#. random value here
#.
#. First check that the country you mention here is listed in
#. http://cvs.debian.org/ *checkout* /webwml/english/mirror/Mirrors.masterlist?rev=HEAD\&cvsroot=webwml&content-type=text/plain
#. (remove the spaces between "*" and "/")
#.
#. BE CAREFUL to use the TWO LETTER ISO-3166 CODE and not anything else
#.
#. You do not need to translate what's between the square brackets
#. You should even NOT put square brackets in translations:
#. msgid "US[ Default value for ftp]"
#. msgstr "FR"
#. :sl2:
#: ../choose-mirror-bin.templates.ftp.sel-in:2002
#, fuzzy
msgid "US[ Default value for ftp]"
msgstr "US"

#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.both-in:2001
#, fuzzy
msgid "Protocol for file downloads:"
msgstr "Protokolla mainna fiillaid viežžá:"

#. Type: select
#. Description
#. :sl2:
#: ../choose-mirror-bin.templates.both-in:2001
msgid ""
"Please select the protocol to be used for downloading files. If unsure, "
"select \"http\"; it is less prone to problems involving firewalls."
msgstr ""
