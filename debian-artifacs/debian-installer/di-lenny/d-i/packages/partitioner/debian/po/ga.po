# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Irish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2006-03-21 14:42-0500\n"
"Last-Translator: Kevin Patrick Scannell <scannell@SLU.EDU>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl5:
#: ../partitioner.templates:1001
msgid "Finish"
msgstr "Críochnaigh"

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002
msgid "Disk to partition:"
msgstr ""

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002
msgid "Please choose one of the listed disks to create partitions on it."
msgstr ""

#. Type: select
#. Description
#. :sl5:
#: ../partitioner.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:2001
msgid "No disk found"
msgstr "Níor aimsíodh diosca"

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:2001
msgid ""
"Unable to find any disk in your system. Maybe some kernel modules need to be "
"loaded."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:3001
msgid "Partitioning error"
msgstr "Earráid deighilte"

#. Type: error
#. Description
#. :sl5:
#: ../partitioner.templates:3001
msgid "Failed to partition the disk ${DISC}."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#. Main menu item
#: ../partitioner.templates:4001
msgid "Partition a hard drive"
msgstr "Deighil tiomántán crua"
