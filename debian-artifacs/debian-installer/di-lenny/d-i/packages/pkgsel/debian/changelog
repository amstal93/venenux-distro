pkgsel (0.24) unstable; urgency=low

  [ Updated translations ]
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম খান)
  * Bosnian (bs.po) by Armin Besirovic
  * Danish (da.po)
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Hebrew (he.po) by Lior Kaplan
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Indonesian (id.po) by Arief S Fitrianto
  * Georgian (ka.po) by Aiet Kolkhi
  * Central Khmer (km.po) by KHOEM Sokhem
  * Latvian (lv.po) by Aigars Mahinovs
  * Macedonian (mk.po) by Arangel Angov
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Vanja Cvelbar
  * Albanian (sq.po) by Elian Myftiu
  * Serbian (sr.po) by Veselin Mijušković
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Ukrainian (uk.po) by Borys Yanovych
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Deng Xiyue

 -- Otavio Salvador <otavio@debian.org>  Sun, 21 Sep 2008 19:38:10 -0300

pkgsel (0.23) unstable; urgency=low

  * Check for codename instead of suite to avoid system upgrades for Etch.
    Thanks to Ferenc Wagner for spotting the error.

  [ Updated translations ]
  * Catalan (ca.po) by Jordi Mallach
  * Hindi (hi.po) by Kumar Appaiah

 -- Frans Pop <fjp@debian.org>  Thu, 28 Aug 2008 12:27:46 +0200

pkgsel (0.22) unstable; urgency=low

  [ Frans Pop ]
  * Disable system upgrades for Etch installs as upgrade options used are
    incompatible with the Etch version of aptitude. Closes: #492593.
  * There is no need to run debconf-apt-progress --config as in-target already
    sets the correct environment.
  * Multiply progress steps by 10 in postinst script.
  * Advance progress bar when running pre-pkgsel.d scripts.
  * Drop the advancement of the progress bar in pre-pkgsel.d/10popcon.
    (Closes: #492861)

  [ Jérémy Bobbio ]
  * Add missing warning() definition in postinst script.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay(Bongop) (DIT,BHUTAN)
  * Greek, Modern (el.po) by Emmanuel Galatoulas
  * Esperanto (eo.po) by Felipe Castro
  * French (fr.po) by Christian Perrier
  * Croatian (hr.po) by Josip Rodin
  * Italian (it.po) by Milo Casagrande
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Norwegian Bokmål (nb.po) by Hans Fredrik Nordhaug
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Russian (ru.po) by Yuri Kozlov
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 26 Aug 2008 10:58:05 +0200

pkgsel (0.21) unstable; urgency=low

  [ Joey Hess ]
  * Move redundant error handling code into a function.
  * Add a call to aptitude safe-upgrade before running tasksel,
    so that security fixes not present on install CDs (or mirrors) 
    will be pulled in. Closes: #479431.
    - debconf prompts during the upgrade will be provided to d-i
    - dpkg is forced to install new verisons of conffiles

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivanov
  * Czech (cs.po) by Miroslav Kure
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Esko Arajärvi
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Gujarati (gu.po) by Kartik Mistry
  * Italian (it.po) by Milo Casagrande
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Changwoo Ryu
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Marathi (mr.po) by Sampada
  * Dutch (nl.po) by Frans Pop
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Slovak (sk.po) by Ivan Masár
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Mert Dirik
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Wed, 16 Jul 2008 13:49:53 +0200

pkgsel (0.20) unstable; urgency=low

  [ Updated translations ]
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * Marathi (mr.po) by Sampada
  * Dutch (nl.po) by Frans Pop
  * Punjabi (Gurmukhi) (pa.po) by Amanpreet Singh Alam

 -- Otavio Salvador <otavio@debian.org>  Thu, 08 May 2008 00:26:24 -0300

pkgsel (0.19) unstable; urgency=low

  * Change back to using apt-install in pre-pkgsel.d/01laptop-detect now that
    it's safe to do so. Requires: di-utils (>= 1.56).
  * Unmount /cdrom during package installation even if the user did not scan
    additional CDs as otherwise accessing the CD may fail. Closes: #474346.

  [ Updated translations ]
  * Marathi (mr.po)
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Frans Pop <fjp@debian.org>  Mon, 28 Apr 2008 07:20:08 +0200

pkgsel (0.18) unstable; urgency=low

  * Avoid using apt-install in pre-pkgsel.d/01laptop-detect because it
    interferes with multi-CD support.

 -- Frans Pop <fjp@debian.org>  Wed, 05 Mar 2008 05:31:39 +0100

pkgsel (0.17) unstable; urgency=low

  [ Colin Watson ]
  * Pass pkgsel/include to aptitude following "--" to prevent typos in
    preseed files being interpreted as aptitude options by accident.
  * Silence syslog noise if /usr/lib/pre-pkgsel.d is empty.

  [ Otavio Salvador ]
  * Install laptop-detect only when running at i386, amd64 and powerpc.

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Changwoo Ryu
  * Latvian (lv.po) by Viesturs Zarins
  * Malayalam (ml.po) by  A|
  * Panjabi (pa.po) by A S Alam
  * Slovak (sk.po) by Ivan MasPraveenr

 -- Otavio Salvador <otavio@debian.org>  Wed, 13 Feb 2008 12:06:05 -0200

pkgsel (0.16) unstable; urgency=low

  [ Dann Frazier ]
  * Add support for a /usr/lib/pre-pkgsel.d run-parts style directory.
    Closes: #447326.

  [ Frans Pop ]
  * Support CD/DVD changing during package installation.
    For this to be possible, the installation CD/DVD has to be unmounted in
    the D-I environment, which means that during most of pkgsel (including
    pre-pkgsel.d scripts) the installation of udebs is not possible. The
    installation CD/DVD is reloaded before the end of pkgsel to ensure
    following components can again install udebs.

  [ Updated translations ]
  * Belarusian (be.po) by Hleb Rubanau
  * Bulgarian (bg.po) by Damyan Ivanov
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * German (de.po) by Jens Seidel
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae Park
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrișor
  * Russian (ru.po) by Yuri Kozlov
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tamil (ta.po) by Dr.T.Vasudevan
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Frans Pop <fjp@debian.org>  Mon, 12 Nov 2007 13:27:54 +0100

pkgsel (0.15) unstable; urgency=low

  [ Frans Pop ]
  * Drop isinstallable test for sarge, we don't support sarge installs
    anymore.

  [ Updated translations ]
  * Italian (it.po) by Stefano Canepa
  * Panjabi (pa.po) by A S Alam
  * Portuguese (pt.po) by Miguel Figueiredo
  * Vietnamese (vi.po) by Clytie Siddall
  
 -- Otavio Salvador <otavio@debian.org>  Tue, 11 Sep 2007 09:30:21 -0300

pkgsel (0.14) unstable; urgency=low

  [ Joey Hess ]
  * Pass APT::Install-Recommends=false to apt when installing
    popularity-contest to avoid dragging in any recommends there with the new
    apt.
  * No need to do this for aptitude runs, the existing --without-recommends
    is sufficient.

  [ Otavio Salvador ]
  * Avoid running in case live-installer is being used.

  [ Updated translations ]
  * Romanian (ro.po) by Eddy Petrișor

 -- Otavio Salvador <otavio@ossystems.com.br>  Thu, 21 Jun 2007 11:51:33 -0300

pkgsel (0.13) unstable; urgency=low

  * Multiply menu-item-numbers by 100

  [ Updated translations ]
  * Esperanto (eo.po) by Serge Leblanc

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Apr 2007 14:40:27 -0400

pkgsel (0.12) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Frans Pop <fjp@debian.org>  Tue, 27 Feb 2007 16:59:38 +0100

pkgsel (0.11) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Hungarian (hu.po) by SZERVÁC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Wed, 31 Jan 2007 11:54:49 +0100

pkgsel (0.10) unstable; urgency=low

  * Add missing debconf dependency.
  * Add Lintian override for standards-version.

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke

 -- Frans Pop <fjp@debian.org>  Tue, 24 Oct 2006 16:25:18 +0200

pkgsel (0.09) unstable; urgency=low

  * Revert postinst back to last working version.

 -- Joey Hess <joeyh@debian.org>  Thu, 24 Aug 2006 14:30:48 -0400

pkgsel (0.08) unstable; urgency=low

  [ Colin Watson ]
  * Add a facility to install a few extra packages in addition to tasks by
    preseeding pkgsel/include.
  * fc-cache is slow. Divert it away during installation, and run it just
    once at the end.
  
  [ Otavio Salvador ]
  * Add a facility to disable installation of popularity-contest by
    preseeding popularity-contest/participate to false.
  * Add a facility to disable tasksel run by preseeding tasksel/first to
    an empty string.

  [ Updated translations ]
  * Panjabi (pa.po) by A S Alam
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Otavio Salvador <otavio@debian.org>  Tue,  8 Aug 2006 00:21:26 -0300

pkgsel (0.07) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Progga
  * Bosnian (bs.po) by Safir Secerovic
  * Danish (da.po) by Claus Hindsgaul
  * Dzongkha (dz.po)
  * Estonian (et.po) by Siim Põder
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hungarian (hu.po) by SZERVÑC Attila
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Russian (ru.po) by Yuri Kozlov
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Tamil (ta.po) by Damodharan Rajalingam
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Frans Pop <fjp@debian.org>  Thu, 13 Jul 2006 17:34:53 +0200

pkgsel (0.06) unstable; urgency=low

  * Add isinstallable file to skip pkgsel when installing Sarge.

  [ Updated translations ]
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Basque (eu.po) by Piarres Beobide
  * Hungarian (hu.po) by SZERVÑC Attila
  * Khmer (km.po) by hok kakada
  * Slovenian (sl.po) by Matej Kovačič

 -- Frans Pop <fjp@debian.org>  Mon, 20 Mar 2006 18:34:28 +0100

pkgsel (0.05) unstable; urgency=low

  [ Joey Hess ]
  * Remove the read-edid and mdetect code; tasksel takes care of this as of
    version 2.39.

  [ Updated translations ]
  * Bulgarian (bg.po) by Ognyan Kulev
  * Catalan (ca.po) by Jordi Mallach
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hebrew (he.po) by Lior Kaplan
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Punjabi (Gurmukhi) (pa_IN.po) by Amanpreet Singh Alam
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Cuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Frans Pop <fjp@debian.org>  Wed, 25 Jan 2006 16:13:34 +0100

pkgsel (0.04) unstable; urgency=low

  * Use in-target for purging, debconf seems to be talking to cdebconf during
    the purge otherwise.
  * Run dpkg --configure -a if tasksel fails to avoid leaving the system
    with packages in an unconfigured state and breaking pkgsel re-runs.
  * Detect cleanup failure and log about it and still take down the progress
    bar.

  [ Updated translations ]
  * Greek, Modern (1453-) (el.po) by quad-nrg.net

 -- Joey Hess <joeyh@debian.org>  Thu, 29 Dec 2005 08:05:05 -0500

pkgsel (0.03) unstable; urgency=low

  [ Colin Watson ]
  * Propagate debian-installer/keymap from cdebconf to /target, used by
    xserver-xorg to infer the X keymap.
  * Use loops for diversion handling so that the list is kept in just one
    place.
  * Stop removing popularity-contest for now until the cdebconf PURGE crash
    is fixed.

  [ Joey Hess ]
  * Consolidate debconf-copydbs into one command.
  * Pass -q to a few apt-get commands, to avoid gratuotous logging of download
    progress (the main cause of this will be fixed in tasksel though).
  * Propigate out nonzero exit codes from tasksel to main-menu so that it can
    signal a back up.

  [ Updated translations ]
  * Catalan (ca.po) by Guillem Jover

 -- Colin Watson <cjwatson@debian.org>  Mon, 26 Dec 2005 23:30:48 +0000

pkgsel (0.02) unstable; urgency=low

  [ Joey Hess ]
  * Set LANG to C in postinst so that the bare chroot calls don't trigger
    perl warning messages for C.UTF-8 locale. (See #344159)
  * Note that this does not affect the LANG set for the main package
    installation run, which will be handled by in-target.
  * Fix progress bar title.

  [ Colin Watson ]
  * Fix di-utils build-dependency, and bump required version to 1.20 for
    in-target.
  * Capitalise db_progress subcommand names.
  * Remove trailing spaces from pkgsel.templates, which confused cdebconf.
  * Change section to debian-installer (fixes lintian warning).

  [ Updated translations ]
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Indonesian (id.po) by Parlin Imanuel Toh
  * Italian (it.po) by Giuseppe Sacco
  * Korean (ko.po) by Sunjae park
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Frans Pop
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Tagalog (tl.po) by Eric Pareja

 -- Colin Watson <cjwatson@debian.org>  Sat, 24 Dec 2005 18:31:54 +0000

pkgsel (0.01) unstable; urgency=low

  [ Joey Hess ]
  * First release, split off from base-config.
  * Needs tasksel 2.36 and debconf 1.4.62.

  [ Christian Perrier ]
  * Change the menu entry to be more friendly to non technical users
  * Add a few comments for translators

  [ Joey Hess ]
  * Propagate three debconf variables from cdebconf to /target:
    debian-installer/language, debian-installer/country
      sed by dictionaries-common to determine which dictionary to default to.
    passwd/username
      Used by exim4-config to determine where to send root mail.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Andrei Darashenka
  * Bulgarian (bg.po) by Ognyan Kulev
  * Bengali (bn.po) by Baishampayan Ghose
  * Bosnian (bs.po) by Safir Šećerović
  * Catalan (ca.po) by Guillem Jover
  * Czech (cs.po) by Miroslav Kure
  * Welsh (cy.po) by Dafydd Harries
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by Greek Translation Team
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Persian (fa.po) by Arash Bijanzadeh
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Krunoslav Gernhard
  * Hungarian (hu.po) by VEROK Istvan
  * Indonesian (id.po) by Arief S Fitrianto
  * Icelandic (is.po) by David Steinn Geirsson
  * Italian (it.po) by Giuseppe Sacco
  * Japanese (ja.po) by Kenshi Muto
  * Korean (ko.po) by Sunjae park
  * Kurdish (ku.po) by Erdal Ronahi
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Macedonian (mk.po) by Georgi Stanojevski
  * Bokmål, Norwegian (nb.po) by Bjørn Steensrud
  * Dutch (nl.po) by Bart Cornelis
  * Norwegian Nynorsk (nn.po)
  * Norwegian Nynorsk (pa_IN.po) by Amanpreet Singh Alam
  * Polish (pl.po) by Bartosz Fenski
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Jure Čuhalev
  * Albanian (sq.po) by Elian Myftiu
  * Swedish (sv.po) by Daniel Nylander
  * Tagalog (tl.po) by Eric Pareja
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Xhosa (xh.po) by Canonical Ltd
  * Simplified Chinese (zh_CN.po) by Ming Hua
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Joey Hess <joeyh@debian.org>  Fri, 16 Dec 2005 13:10:03 -0500
