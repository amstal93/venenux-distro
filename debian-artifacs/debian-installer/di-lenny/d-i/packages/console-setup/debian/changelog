console-setup (1.28) unstable; urgency=low

  [ Debconf translations ]
  * Belarusian. Closes: #487293

 -- Christian Perrier <bubulle@debian.org>  Tue, 23 Sep 2008 19:49:58 +0200

console-setup (1.27) unstable; urgency=low

  [ Debconf translations ]
  * French
  * Basque. Closes: #490363
  * Turkish. Closes: #490365
  * Thai
  * Bulgarian
  * Swedish. Closes: #490569
  * German. Closes: #490714
  * Portuguese. Closes: #491011
  * Vietnamese. Closes: #491046
  * Bulgarian
  * Czech
  * Galician

 -- Christian Perrier <bubulle@debian.org>  Fri, 18 Jul 2008 11:33:49 +0200

console-setup (1.26) unstable; urgency=low

  [ Colin Watson ]
  * Add a template for the main menu item name.

  [ Debconf translations ]
  * Russian. Closes: #487242
  * Basque. Closes: #487313
  * Turkish. Closes: #487439
  * Swedish. Closes: #487308

 -- Christian Perrier <bubulle@debian.org>  Tue, 08 Jul 2008 20:40:35 +0200

console-setup (1.25) unstable; urgency=low

  [ Anton Zinoviev ]
  * Correction in Debconf template - Terminus supports also Greek.
    Unfuzzy Belarusian and German translations.
    
  [ Debconf translations ]
    - French
    - Galician
    - German. Closes: #485457
    - Bulgarian
    - Vietnamese. Closes: #485779
    - Portuguese. Closes: #486090
    - Czech

 -- Christian Perrier <bubulle@debian.org>  Wed, 18 Jun 2008 20:24:01 +0200

console-setup (1.24) unstable; urgency=medium

  [ Anton Zinoviev ]
  * Support for X key types such as PC_CONTROL_LEVEL2 and PC_ALT_LEVEL2.
    Never use ShiftR.  Thanks to Alexander E. Patrakov, closes: #484822.
    Urgency medium because #484822 is related to the default settings used
    by d-i for some languages.
  * Improved support for non 'KP_...' keys in the keypad block.
  * console-setup-mini and udebs: fix some bugs in modifiers in
    precompiled non-latin layouts
  * Support for some new X key symbols (some of them spelling errors).
  * ckbcomp: accept a list after virtual_modifiers
  * Debconf support for grp:alt_caps_toggle, grp:lctrl_lshift_toggle,
    grp:sclk_toggle, lv3:alt_switch, lv3:enter_switch, compose:lwin
  * Removed debconf support for grp:alts_toggle, grp:ctrls_toggle,
    grp:shifts_toggle, grp:shift_caps_toggle as it is impossible to
    support them on the console.
  * update Keyboard/ckb with the keyboard data of xkb-data version 1.3-1
    (this syncs console-setup-mini and udebs with the main package)
  * xmlreader: ignore tags languageList and countryList.
  * Remove dh_installxfonts from debian/rules.
  
 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 08 Jun 2008 21:30:54 +0300

console-setup (1.23) unstable; urgency=low

  [ Petter Reinholdtsen ]
  * Correct dependency info in init.d scripts to make sure they start
    after console-screen and keymap. Closes: #475826.

  [ Colin Watson ]
  * Map dead_stroke to U+002D HYPHEN-MINUS.
  * Support XKB rules with multiple consequents (thanks, Adam Simpkins;
    LP: #139710).
  * Allow specifying alternate rules using XKBRULES (thanks, Adam Simpkins;
    LP: #139712).
  * Handle some special cases for the KPDL key (LP: #189529).

  [ Debconf template translations ]
  * Galician. Closes: #480983.
  * German. Closes: #475045.
  * Portuguese. Closes: #478347.

 -- Christian Perrier <bubulle@debian.org>  Sun, 01 Jun 2008 18:40:29 +0200

console-setup (1.22) unstable; urgency=low

  [ Colin Watson ]
  * Proofread and correct debconf templates (LP: #181860).
  * Treat 'any' as a synonym for 'NoSymbol' in XKB input files (LP: #93077).

  [ Piarres Beobide ]
  * Debconf Basque translation. Closes: #469650

  [ Frans Pop ]
  * Add support for PA Semi's evaluation systems (#464429). Thanks to
    Olof Johansson for the patch.

  [ Anton Zinoviev ]
  * Use boottime.kmap.gz instead of ckbcomp in setupcon only if the
    default configuration is requested and boottime.kmap.gz is not older
    than /etc/default/console-setup.  The package has used
    boottime.kmap.gz since version 1.20.  This closes: #473217.
  * ckbcomp: define X key codes 214-217 for AT keyboards (multimedia,
    hence fake keys).  Thanks to Michael Biebl, Christoph Anton Mitterer
    and Juhapekka Tolvanen, closes: #444488.
  * Return the template configuration file in the udeb.
  * Update README.fonts - Terminus now supports Greek.  Add depends
    console-terminus > 4.26.
  * Install the manpages for ckbcomp and setupcon also in
    console-setup-mini.

 -- Anton Zinoviev <zinoviev@debian.org>  Fri, 04 Apr 2008 22:41:21 +0300

console-setup (1.21) unstable; urgency=low

  * Silently ignore 'vendor' tag in xorg.xml.
  * Use 'chomp' rather than 'chop' in ckbcomp to deal with XKB symbols files
    that are missing their final newline (closes: #461191).

 -- Colin Watson <cjwatson@debian.org>  Thu, 17 Jan 2008 12:58:25 +0000

console-setup (1.20) unstable; urgency=low

  [ Colin Watson ]
  * Set default model to pc105 on powerpc/ps3 and powerpc/cell systems.
  * Clean up the package description a little.
  * Only default to Canadian layout for fr_CA; use U.S. English for other
    *_CA locales (LP: #64418).
  * Improve ckbcomp(1) formatting.
  * Run setupcon with --save in the postinst to ensure that required data is
    copied to /etc.
  * Load the saved keymap in /etc/console-setup/boottime.kmap.gz if
    available rather than running ckbcomp (which is slow), unless we're
    being called with --save or --save-only.

  [ Debconf templates translations ]
  * Belarusian. Closes: #447109
  * Galician. Closes: #447940
  * Russian.

  [ Christian Perrier ]
  * Correct "Provides:" in console-setup init script to allow insserv
    to insert console-setup properly in the boot sequence.
    Thanks to Petter Reinholdtsen for the patch. Closes: #458486
  * Add the required "Default-Stop" and "Required-Stop" fields in the
    init.d/console-setup script.
  * Set myself as Uploader.
  * Fixing lintian warnings:
    - Remove "perl, libxml-parser-perl" from Build-Depends. They were
      duplicated in Build-Depends-Indep
    - Do not ignore non-zero exit status of "make maintainer-clean" in the
      clean target
    - Turn console-setup/dont_ask_layout into an error template. That'll
      get rid of the lintian warning and this is more accurate anyway.
  * Fixing lintian errors:
    - No longer install an example file in 
      /usr/share/doc/console-setup-mini/examples/console-setup
      for the udeb package

  [ Petter Reinholdtsen ]
  * Correct "provides" header in init.d/keyboard-setup too.
  * Correct dependencies of init.d/keyboard-setup and
    init.d/console-setup to reflect their intended start sequence.
  * Add the required "Default-Stop" and "Required-Stop" fields in the
    init.d/keyboard-setup script.
  * Update standards-version from 3.7.2 to 3.7.3.  No changes needed.
  * Add lintian override to make it that
    /usr/share/console-setup/KeyboardNames.pl is not executable.

 -- Christian Perrier <bubulle@debian.org>  Sat, 05 Jan 2008 09:04:03 +0100

console-setup (1.19) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Fix additional ellipsis in keyboard-setup initscript. (Closes: #435285)

  [ Otavio Salvador ]
  * Provides keyboard-setup virtual package.
  
  [ Anton Zinoviev ]
  * ckbcomp: '#' is also a comment character.  Thanks to Matt Kraai,
    closes: #443709.

  [ Debconf templates translations ]
  * German. Closes: #438395

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 23 Sep 2007 20:10:44 +0300

console-setup (1.18) unstable; urgency=low

  * In d-i environment read the encoding from debian-installer/charset
    variable.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 15 Jul 2007 23:50:29 +0300

console-setup (1.17) unstable; urgency=low

  * Add a manual page for ckbcomp.  Thanks to Vassilis Pandis,
    closes: #405005. 
  * Apply a patch by Colin Watson:
    - enable udebs;
    - remove unhelpful defaults for several questions;
    - allow preseeding of console-setup/modelcode,
      console-setup/layoutcode, console-setup/variantcode,
      console-setup/optionscode, and console-setup/fontsize;
    - remove outdated note about loadkeys in console-setup-udeb's
      description;
    - Add a few bits of installer integration to console-setup-udeb
      borrowed from kbd-chooser. 
    I disabled the code for making UTF-8 default charmap in d-i because
    there are locales which use other charmap.  However I made setupcon to
    always use UTF-8 in d-i environment.
  * Updated French and Dutch translations, new Portuguese.  Thanks to
    Pedro Ribeiro, Jean-Luc Coulon and Bart Cornelis.
    Closes: #424665, #425773, #426082.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 15 Jul 2007 16:35:55 +0300

console-setup (1.16) unstable; urgency=low

  * Update ckbconf with the most recent list of xkeysyms.
  * Thanks to Colin Watson and Ubuntu team for the many bugs and
    improvements fixed in this release.  Patches were provided by Colin
    Watson:
    - Do not sorce the debconf library in the middle of the postinst,
      closes: #420831.
    - Remove from the source package several directories
      debian/UDEB-PACKAGE-NAME.  (The udebs were removed from
      debian/control before running the clean target.)  Closes: #420912.
    - Map fr_CH locale to Swiss French default keyboard
      layout in config.proto.  Closes: #420828.
    - Pass xkbdir variable on the make-command line, because else its
      value is overriden and the keymap definitions in Keyboard/ckb are
      used.  Closes: #420827.
    - A new option --save-only for setupcon.
    - Ship KeyboardNames.pl and kbdnames-maker.  Closes: #420914.
    - Fix Esperanto locale check in config.proto, closes: #420785.  Make
      'epo' the default layout for Esperanto.
    - The Bosnian layout is 'ba', not 'bs'.  Closes: #420787.
    - The layout variant is 'deva', not 'dev'.  Closes: #420797.
    - Do not leave empty default codeset for unsupported locales.
      Closes: #420802.  
    - Use 'jp(latin)' instead of 'us' in the Latin mode of Japanese
      keyboards.  Override the keyboard model if it was chosen by default
      and the layout is Japanese or Brasilian.  Closes: #420824.
    - Add --save-only option to setupcon.  Closes: #420832.
    - Multiply the Installer-Menu-Item field of the udebs by 100.
      Closes: #421260.
    - By default do not use AltGr with basic US keyboard.  Closes: #421263.
    - Make sure amiga, ataritt, sun4 and sun5 are in KeyboardNames.pl even
      if they are not in the xml database.  Closes: #420821.
    - Talk about 'national' instead of 'non-Latin' mode in the templates.
      Treat the Lithuanian keyboard as a two-mode keyboard similarly to
      the Serbian keyboard.  Closes: #421261.
  * Do not ignore the Control+ and Alt+non_symbol_key combinations.
    Thanks to Karsten Hilbert, closes: #421796.
  * Two additions to the FAQ:
    - Add a suggestion for the VBERestore option.  Thanks to Dmitry
      Semyonov, closes: #402143.
    - Document CapsLock working as ShiftLock in Unicode mode.
  * Added manual page for setupcon.  Thanks to Vassilis Pandis,
    closes: #405005. 
  * Added Dutch translation.  Thanks to Bart Cornelis, closes: #415518.
  * Updated Bulgarian translation.
  
 -- Anton Zinoviev <zinoviev@debian.org>  Tue,  1 May 2007 23:48:12 +0300

console-setup (1.15) unstable; urgency=low

  * Updated Georgian letters for Fixed16 and Fixed14.  Thanks to Vladimer
    Sichinava.

 -- Anton Zinoviev <zinoviev@debian.org>  Wed, 14 Mar 2007 21:50:54 +0200

console-setup (1.14) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf translations:
    - Czech. Closes: #408602
    - Spanish. Closes: #412128
    - Galician. Closes: #413356
    - Convert French to UTF-8

 -- Christian Perrier <bubulle@debian.org>  Sun,  4 Mar 2007 19:54:11 +0100

console-setup (1.13) unstable; urgency=low

  * Unfuzzy the translations.

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 13 Nov 2006 21:24:44 +0200

console-setup (1.12) unstable; urgency=low

  * standards.equivalents: approximate symbols used by some popular
    console applications.
  * New German translation.  Thanks to Matthias Julius, closes: #396815.
  * Updated French translation.  Thanks to Jean-Luc Coulon, closes: #398023.
  * Allign the choices of console-setup/codeset.
  * Updated Fonts/bdf/georgian16.psf (author: Gia Shervashidze).  Update
    provided by Vladimer Sichinava.

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 13 Nov 2006 17:30:40 +0200

console-setup (1.11) unstable; urgency=low

  * Instead of X keycode 123 (<KPDC>) map X keycode 211 (<AB11>) to kernel
    keycode 89.  Thanks to Rafael Almeida, closes: #394962.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 24 Oct 2006 21:08:24 +0300

console-setup (1.10) unstable; urgency=low

  * Update ckbcomp for the new xkb-data (now not all xkb_keycodes in
    keycodes/macintosh are old macintosh).

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 23 Oct 2006 11:54:06 +0300

console-setup (1.9) unstable; urgency=low

  * Remove the udebs in order to make the migration from unstable to
    testing automatic.
  * Update the version of the Terminus font from 4.16 to 4.20.  (This is
    not important for Debian, since in Debian the Terminus font is
    provided by the console-terminus package.)
  * New mini-font georgian16.bdf to be used for the Georgian letters in
    Fixed16, author: Gia Shervashidze.  Thanks to Vladimer Sichinava.
  * Add FAQ.  Thanks to Thue Janus Kristensen, closes: #389963, #389970.
  * Fix two problems reported by Martin-Éric Racine.  Closes: #393850.
    - console-setup-mini should not ask questions in debconf for the encoding
      since only UTF-8 is supported.
    - make the purpose of the codeset question clearer.
  * Use user-friendly choices in the codeset question.
  * Updated Bulgarian translation of the templates.
  * Add some capital Greek letters to standard.equivalents and approximate
    BOX DRAWINGS HEAVY HORIZONTAL by BOX DRAWINGS LIGHT HORIZONTAL.
  * Make the bullet to be the first symbol in useful.set - it is used by w3m.

 -- Anton Zinoviev <zinoviev@debian.org>  Fri, 20 Oct 2006 01:53:11 +0300

console-setup (1.8) unstable; urgency=low

  * setupcon: new option --force to skip the 'are we on the console'
    check.  To be used for d-i because there /proc/self/fd/2 points to
    pipe.  Thanks to Tollef Fog Heen.
  * config.proto: Handle C locale with debian-installer/locale not being
    available.  Thanks to Colin Watson for the bug report and the patch,
    closes: #386835.
  * config.proto: Handle properly the case when the user backs up from the
    first Debconf question.  In the template file change "none" to "this
    default value is completely ignored".  Thanks to Colin Watson, 
    closes: #386837.
  * Handle properly the cs layout in config.proto and kbdcompiler.  Thanks
    to Colin Watson, closes: #386838.
  * Supply correct debian/po/POTFILES.in.  Thanks to Thomas Huriaux,
    closes: #387631.
  * Use /lib/lsb/init-functions in the init scripts when available.
    Thanks to Colin Watson, closes: #386844.
  * config.proto: Try to use the value of debian-installer/keymap to get
    default keyboard layout and variant and when that is possible ask the
    corresponding Debconf questions with medium instead of critical
    priority.  Thanks to Colin Watson, closes: #386836.
  * config.proto(ask_debconf): when the provided default value is
    unavailable, try to use the empty string.  Useful when asking for the
    keyboard variant.
  * Add README.Debian for console-setup.
  * Add boot script order header to the init scripts.  Thanks to
    Martin-Éric Racine, closes: #386545.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 19 Sep 2006 11:56:15 +0300

console-setup (1.7) unstable; urgency=low

  [ Christian Perrier ]
  * Updated French translation
  * Added Czech translation. Closes: #360333

  [ Anton Zinoviev ]
  * Keyboard/ckbcomp: include /usr/share/X11/xkb to the list of
    directories for keyboard data.  The data from /etc/console-setup/ckb
    are still the default.
  * Sync the Keyboard/ckb data with the data in xkb-data package, version
    0.8-5.  Update ckbcomp with two new keysyms (Cyrillic_(CHE|KA)_vertbar).
  * Do not package the keyboard data.  Depend on xkb-data.  Thanks to
    Denis Barbier, closes: #359775.
  * Fix a shell syntax error in debian/config.proto that causes
    installation of console-setup to hang on powerpc.  Thanks to Colin
    Watson, closes: #374765.
  * ckbcomp: new option -compact to generate compact keymaps.
  * New experimental package console-setup-mini and udebs for d-i.
  * Correct a typo in the Bulgarian translation.  Thanks to Yavor Doganov,
    closes: #359677.
  * Updated Standards-Version: 3.7.2

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 22 Jul 2006 23:00:00 +0300

console-setup (1.6) unstable; urgency=low

  [ Anton Zinoviev ]
  * Few comments in debian/templates.
  * Document in console-setup/fontsize-fb template that non 8-pixel wide
    fonts can not be used with console-tools.  Add a warning in setupcon
    if the user tries to use such fonts with console-tools.  Thanks to
    Hans Ulrich Niedermann, closes: #352911.
  * debian/postinst: protect in double quotes the variables read from
    $CONFIGFILE.  Thanks to Jonas Meurer, closes: #353525.
  * ckbcomp: Do not interpret xkeysyms such as F10, F11 and F12 as
    hexadecimal numbers.  Thanks to Bálint Balogh, closes: #354564.
  * Debconf: if there are unrecognised options, do not override the value
    of XKBOPTIONS in /etc/default/console-setup.  Thanks to Javier Kohen,
    closes: #354473.
  * Updated Bulgarian translation.
  
  [ Christian Perrier ]
  * Added French translation. Closes: #354446

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 19 Mar 2006 23:53:42 +0200

console-setup (1.5) unstable; urgency=low

  * In the Debconf question for the font size add an information that some
    of the font sizes are non available in text mode and with radeonfb.
    Thanks to Hans Ulrich Niedermann, closes: #352911.

 -- Anton Zinoviev <zinoviev@debian.org>  Wed, 15 Feb 2006 12:22:04 +0200

console-setup (1.4) unstable; urgency=low

  * Make debconf templates a bit less verbose.  Make them compliant with
    writing style recommended in the developers references.  Thanks to
    Christian Perrier for the patch, closes: #352772.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 14 Feb 2006 20:52:24 +0200

console-setup (1.3) unstable; urgency=low

  * Make the Debconf config script not override the user's answers with
    default values when executed at postinst time.  Thanks to Juhapekka
    Tolvanen, closes: #352299.

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 13 Feb 2006 21:41:20 +0200

console-setup (1.2) unstable; urgency=low

  * Fix some bugs related to non-UTF8 modes:
    - setupcon always sets up the first console in UTF-8 mode
    - setupcon doesn't load ACM in the first console if kbd is used 
    - ckbcomp has to interpret the "acute" Xkeysym as apostrophe.
    Thanks to Norbert Preining, closes: #352449.

 -- Anton Zinoviev <zinoviev@debian.org>  Mon, 13 Feb 2006 21:37:42 +0200

console-setup (1.1) unstable; urgency=low

  * Run setupcon in postinst.  Thanks to Recai Oktaş for the suggestion.
  * Setupcon uses /proc/self/fd/2 (standard error) instead of
    /proc/self/fd/0 (standard output) in order to determine whether we are
    on the console or not.  That way setupcon can detect that we are on
    the console even when it is invoked from postinst and Debconf has
    changed standard input and output.
  * Changes in the text of some questions: "What is the origin of our
    keyboard" instead of "What is the layout of your keyboard" and "What
    is the layout of your keyboard" instead of "What is the variant of
    your keyboard".  Ask the console-setup/variant question with critical
    priority.  Thanks to Arnt Karlsen, closes: #352405.
  * Fix the true/yes discrepancy in setupcon which caused the options -k
    and -f not to work.
  * Updated Bulgarian translation.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 11 Feb 2006 23:24:54 +0200

console-setup (1.0) unstable; urgency=low

  * Initial release
  * Turkish translation by Recai Oktaş (thanks also for initial testing
    and pointing few typos).

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 14 Jan 2006 16:20:35 +0200
