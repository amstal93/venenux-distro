# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Lithuanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004...2008.
# Marius Gedminas <mgedmin@b4net.lt>, 2004.
# Darius Skilinskas <darius10@takas.lt>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-11 22:48+0000\n"
"PO-Revision-Date: 2008-04-24 10:11+0300\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../nobootloader.templates:1001
msgid "Continue without boot loader"
msgstr "Tęsti be pradinio įkėliklio"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Failed to mount /target/proc"
msgstr "Nepavyko prijungti /target/proc"

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "Nepavyko prijungti 'proc' failų sistemos prie /target/proc."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Detaliau žiūrėkite į /var/log/syslog arba ketvirtojoje virtualioje konsolėje."

#. Type: error
#. Description
#. :sl4:
#: ../nobootloader.templates:2001
msgid "Warning: Your system may be unbootable!"
msgstr "Dėmesio: Jūsų sistema gali nepasikelti!"

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:4001
#: ../nobootloader.templates:5001
msgid "Setting firmware variables for automatic boot"
msgstr "Mikroprogramos (firmware) kintamųjų nustatymas automatiniam įkėlimui"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Some variables need to be set in the Genesi firmware in order for your "
"system to boot automatically.  At the end of the installation, the system "
"will reboot.  At the firmware prompt, set the following firmware variables "
"to enable auto-booting:"
msgstr ""
"Kai kurie mikroprogramos Genesi kintamieji turi būti nustatyti, tam kad "
"sistema galėtų būti automatiškai įkelta. Įdiegimo pabaigoje, sistema bus "
"perkraunama. Esant mikroprogramos kvietimui,  nustatykite sekančius "
"mikroprogramos kintamuosius, kad būtų galimas automatinis įkėlimas:"

#. Type: note
#. Description
#. :sl4:
#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001 ../nobootloader.templates:5001
msgid ""
"You will only need to do this once.  Afterwards, enter the \"boot\" command "
"or reboot the system to proceed to your newly installed system."
msgstr ""
"Jūs turite padaryti tai tik vieną kartą. Po to įveskite komandą \"boot\" "
"arba perkraukite sistemą, tam kad pratęstumėte naujai įdiegtoje sistemoje."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:3001
msgid ""
"Alternatively, you will be able to boot the kernel manually by entering, at "
"the firmware prompt:"
msgstr ""
"Arba, galėsite įkelti branduolį rankiniu būdu, esant mikroprogramos "
"kvietimui, įvesdami:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"Some variables need to be set in CFE in order for your system to boot "
"automatically. At the end of installation, the system will reboot. At the "
"firmware prompt, set the following variables to simplify booting:"
msgstr ""
"Kai kurie CFE kintamieji turi būti nustatyti, tam kad sistema galėtų būti "
"automatiškai įkelta. Įdiegimo pabaigoje, sistema bus perkraunama. Esant "
"mikroprogramos kvietimui, nustatykite sekančius mikroprogramos kintamuosius "
"sistemos įkėlimo supaprastinimui:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"You will only need to do this once. This enables you to just issue the "
"command \"boot_debian\" at the CFE prompt."
msgstr ""
"Jums reikės tai atlikti tik vieną kartą. Tai suteiks galimybę, esant CFE "
"kvietimui (prompt), tik įvesti komandą \"boot_debian\"."

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:4001
msgid ""
"If you prefer to auto-boot on every startup, you can set the following "
"variable in addition to the ones above:"
msgstr ""
"Jei pageidaujate automatinio įkėlimo kiekvieno paleidimo metu, šalia "
"anksčiau paminėtų kintamųjų galite papildomai nustatyti sekantį kintamąjį:"

#. Type: note
#. Description
#. :sl4:
#: ../nobootloader.templates:5001
msgid ""
"Some variables need to be set in the Netwinder NeTTrom firmware in order for "
"your system to boot linux automatically.  At the end of this installation "
"stage, the system will reboot, and the firmware will attempt to autoboot.  "
"You can abort this by pressing any key. You will then be dropped into the "
"NeTTrom command system where you have to execute the following commands:"
msgstr ""
"Kai kurie Netwinder NeTTrom mikroprogramos (firmware) kintamieji turi būti "
"nustatyti, tam kad sistema galėtų būti įkelta automatiškai. Šios įdiegimo "
"stadijos pabaigoje, sistema bus perkraunama, ir mikroprograma (fiemware) "
"bandys atlikti automatinį įkėlimą. Jūs galite nutraukti šį procesą, paspaudę "
"bet kurį klavišą. Tuomet atsidursite NeTTrom komandinėje sistemoje, kur "
"turėsite paleisti vykdyti sekančias komandas:"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:6001
msgid "No boot loader installed"
msgstr "Pradinis įkėliklis neįdiegtas"

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:6001
msgid ""
"No boot loader has been installed, either because you chose not to or "
"because your specific architecture doesn't support a boot loader yet."
msgstr ""
"Pradinis įkėliklis neįdiegtas, nes arba taip nusprendėte, arba Jūsų "
"kompiuterio architektūrai dar nėra pradinio įkėliklio."

#. Type: note
#. Description
#. :sl3:
#: ../nobootloader.templates:6001
msgid ""
"You will need to boot manually with the ${KERNEL} kernel on partition "
"${BOOT} and ${ROOT} passed as a kernel argument."
msgstr ""
"Jums teks atlikti įkeltį rankiniu būdu, naudojant branduolį ${KERNEL}, "
"esantį disko skirsnyje ${BOOT}, ir perduodant jam parametrą ${ROOT}."
