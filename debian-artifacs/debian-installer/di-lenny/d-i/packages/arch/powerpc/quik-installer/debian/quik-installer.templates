Template: quik-installer/progress
Type: text
# :sl4:
_Description: Installing quik

Template: quik-installer/progress/apt-install
Type: text
# :sl4:
_Description: Installing quik boot loader

Template: quik-installer/apt-install-failed
Type: boolean
Default: true
# :sl4:
_Description: quik installation failed.  Continue anyway?
 The quik package failed to install into /target/.  Installing quik
 as a boot loader is a required step.  The install problem might however be
 unrelated to quik, so continuing the installation may be possible.

Template: quik-installer/progress/checking
Type: text
# :sl4:
_Description: Checking partitions

Template: quik-installer/noroot
Type: error
# :sl4:
_Description: No root partition found
 No partition is mounted as your new root partition.
 You must mount a root partition first.

Template: quik-installer/root_not_on_first_disk
Type: error
# :sl4:
_Description: Root partition not on first disk
 The quik boot loader requires the root partition to be on the first disk.
 Please return to the partitioning step.

Template: quik-installer/boot_not_on_first_disk
Type: error
# :sl4:
_Description: Boot partition not on first disk
 The quik boot loader requires the boot partition to be on the first disk.
 Please return to the partitioning step.

Template: quik-installer/boot_not_ext2
Type: error
# :sl4:
_Description: Boot partition must be on ext2
 The quik boot loader requires the partition that holds /boot to be formatted
 using the ext2 file system. Please return to the partitioning step.

Template: quik-installer/oldworld_warning
Type: boolean
Default: true
# :sl4:
_Description: Do you really want to install the quik boot loader?
 You have chosen to install the quik boot loader. You will not be able to
 boot any other operating system from this disk. Furthermore, your machine
 may not be bootable in any manner after this process completes. If you are
 left with a blank screen, you may need to try a cold boot and hold down
 Command-Option-P-R.
 .
 Be aware that this code has not been thoroughly tested.

Template: quik-installer/non_oldworld_warning
Type: boolean
Default: false
# :sl4:
_Description: Do you really want to install the quik boot loader?
 You have chosen to install the quik boot loader. You will not be able to
 boot any other operating system from this disk. Furthermore, your machine
 may not be bootable in any manner after this process completes.
 .
 Be aware that this code has not been thoroughly tested.

Template: quik-installer/progress/conf
Type: text
# :sl4:
_Description: Creating quik configuration

Template: quik-installer/conferr
Type: error
# :sl4:
_Description: Failed to create quik configuration
 The creation of the main quik configuration file failed.
 .
 Check /var/log/syslog or see virtual console 4 for the details.
 .
 Warning: Your system may be unbootable!

Template: quik-installer/resolve_vmlinux
Type: error
# :sl4:
_Description: Failed to resolve kernel symlink
 /vmlinux or /boot/vmlinux in the installed system appears not to be a
 symlink to a kernel image. This is probably a bug.

Template: quik-installer/resolve_initrd
Type: error
# :sl4:
_Description: Failed to resolve initrd symlink
 /initrd.img or /boot/initrd.img in the installed system appears not to be a
 symlink to an initial RAM disk image. This is probably a bug.

Template: quik-installer/progress/install
Type: text
# :sl4:
_Description: Installing quik into bootstrap partition

Template: quik-installer/quikerr
Type: error
# :sl4:
_Description: Failed to install boot loader
 The installation of the quik boot loader failed.
 .
 Check /var/log/syslog or see virtual console 4 for the details.
 .
 Warning: your system may be unbootable!

Template: quik-installer/progress/openfirmware
Type: text
# :sl4:
_Description: Setting up OpenFirmware

Template: quik-installer/boot-device_failed
Type: error
# :sl4:
_Description: Unable to configure OpenFirmware
 Setting the OpenFirmware boot-device variable failed.
 You will have to configure OpenFirmware yourself to boot.

Template: quik-installer/boot-command_failed
Type: error
# :sl4:
# This error may not be fatal, depending on the exact type of Mac.
_Description: Problem configuring OpenFirmware
 Setting the OpenFirmware boot-command variable failed.
 You may have intermittent boot failures.

Template: quik-installer/success
Type: note
# :sl4:
_Description: Successfully installed quik
 The quik boot loader was successfully installed.
 .
 The new system is now ready to boot.

Template: quik-installer/mounterr
Type: error
# :sl4:
_Description: Failed to mount /target/proc
 Mounting the proc file system on /target/proc failed.
 .
 Check /var/log/syslog or see virtual console 4 for the details.
 .
 Warning: Your system may be unbootable!

Template: debian-installer/quik-installer/title
Type: text
#  Main menu item
# :sl4:
_Description: Install quik on a hard disk
