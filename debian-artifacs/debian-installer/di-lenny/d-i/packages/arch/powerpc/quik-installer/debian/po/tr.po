# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Turkish messages for debian-installer.
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Mert Dirik <mertdirik@gmail.com>, 2008.
# Recai Oktaş <roktas@omu.edu.tr>, 2004, 2005, 2008.
# Osman Yüksel <yuxel@sonsuzdongu.com>, 2004.
# Özgür Murat Homurlu <ozgurmurat@gmx.net>, 2004.
# Halil Demirezen <halild@bilmuh.ege.edu.tr>, 2004.
# Murat Demirten <murat@debian.org>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-07-07 20:31+0200\n"
"Last-Translator: Mert Dirik <mertdirik@gmail.com>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:1001
msgid "Installing quik"
msgstr "Quik kuruluyor"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:2001
msgid "Installing quik boot loader"
msgstr "Quik önyükleyici kuruluyor"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:3001
msgid "quik installation failed.  Continue anyway?"
msgstr "Quik kurulumu başarısız. Yine de devam edilsin mi?"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:3001
msgid ""
"The quik package failed to install into /target/.  Installing quik as a boot "
"loader is a required step.  The install problem might however be unrelated "
"to quik, so continuing the installation may be possible."
msgstr ""
"Quik paketi /target/'a kurulamadı. Bir önyükleyici olarak quik'in kurulması "
"gerekiyor. Kurulum problemi quik'le ilgili olmayabileceğinden kuruluma devam "
"etmeniz mümkün olabilir."

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:4001
msgid "Checking partitions"
msgstr "Bölümler denetleniyor"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:5001
msgid "No root partition found"
msgstr "Kök bölümü bulunamadı"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:5001
msgid ""
"No partition is mounted as your new root partition. You must mount a root "
"partition first."
msgstr ""
"Kök bölümü olarak bağlanmış bir bölüm bulunamadı. Önce bir kök bölümü "
"bağlamalısınız."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:6001
msgid "Root partition not on first disk"
msgstr "Birinci diskte kök bölümü bulunamadı"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:6001
msgid ""
"The quik boot loader requires the root partition to be on the first disk. "
"Please return to the partitioning step."
msgstr ""
"Quik önyükleyici, kök bölümünün birinci diskte olmasına ihtiyaç duyar. "
"Lütfen bölümleme adımına geri dönün."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:7001
msgid "Boot partition not on first disk"
msgstr "Önyükleme bölümü birinci diskte değil"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:7001
msgid ""
"The quik boot loader requires the boot partition to be on the first disk. "
"Please return to the partitioning step."
msgstr ""
"Quik önyükleyici, önyükleme bölümünün birinci diskte olmasına ihtiyaç duyar. "
"Lütfen bölümleme adımına geri dönün."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:8001
msgid "Boot partition must be on ext2"
msgstr "Önyükleme bölümü ext2 türünde olmalı"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:8001
msgid ""
"The quik boot loader requires the partition that holds /boot to be formatted "
"using the ext2 file system. Please return to the partitioning step."
msgstr ""
"Quik önyükleyici, önyükleme bölümünün ext2 dosya sistemiyle "
"biçimlendirilmesine ihtiyaç duyar. Lütfen bölümleme adımına geri dönün."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001 ../quik-installer.templates:10001
msgid "Do you really want to install the quik boot loader?"
msgstr "Quik önyükleyiciyi kurmak istediğinize gerçekten emin misiniz?"

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001
msgid ""
"You have chosen to install the quik boot loader. You will not be able to "
"boot any other operating system from this disk. Furthermore, your machine "
"may not be bootable in any manner after this process completes. If you are "
"left with a blank screen, you may need to try a cold boot and hold down "
"Command-Option-P-R."
msgstr ""
"Quik önyükleyiciyi kurmayı seçtiniz. Bu diskte bulunan herhangi bir işletim "
"sisteminden açılış yapamayacaksınız. Daha da ötesi, bu işlem tamamlandığında "
"makineniz açılamaz hale de gelebilir. Eğer açılış sırasında boş bir ekranla "
"karşılaşırsanız Komut-Seçenek (Option)-P-R tuşlarını basılı tutarak "
"donanımsal önyükleme yapmayı deneyebilirsiniz."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:9001 ../quik-installer.templates:10001
msgid "Be aware that this code has not been thoroughly tested."
msgstr "Bu kodun etraflı şekilde test edilmediğini unutmayın."

#. Type: boolean
#. Description
#. :sl4:
#: ../quik-installer.templates:10001
msgid ""
"You have chosen to install the quik boot loader. You will not be able to "
"boot any other operating system from this disk. Furthermore, your machine "
"may not be bootable in any manner after this process completes."
msgstr ""
"Quik önyükleyiciyi kurmayı seçtiniz. Bu diskte bulunan herhangi bir işletim "
"sisteminden açılış yapamayacaksınız. Daha da ötesi, bu işlem tamamlandığında "
"makineniz hiçbir şekilde açılamaz hale de gelebilir."

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:11001
msgid "Creating quik configuration"
msgstr "Quik yapılandırılması oluşturuluyor"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001
msgid "Failed to create quik configuration"
msgstr "Quik yapılandırması oluşturulamadı"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001
msgid "The creation of the main quik configuration file failed."
msgstr "Quik ana yapılandırma dosyası oluşturma işlemi başarısız."

#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001 ../quik-installer.templates:16001
#: ../quik-installer.templates:21001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Ayrıntılı bilgi için /var/log/syslog dosyasına veya dördüncü konsola bakın."

#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:12001 ../quik-installer.templates:21001
msgid "Warning: Your system may be unbootable!"
msgstr "Uyarı: Sisteminiz açılamaz duruma gelebilir!"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:13001
msgid "Failed to resolve kernel symlink"
msgstr "Çekirdeği gösteren sembolik bağ çözülemedi"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:13001
msgid ""
"/vmlinux or /boot/vmlinux in the installed system appears not to be a "
"symlink to a kernel image. This is probably a bug."
msgstr ""
"Kurulu sistemdeki /vmlinux veya /boot/vmlinux sembolik bağlarının herhangi "
"çekirdek imajını göstermediği görünüyor. Bu muhtemelen bir yazılım hatası."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:14001
msgid "Failed to resolve initrd symlink"
msgstr "\"initrd\" sembolik bağı çözülemedi"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:14001
msgid ""
"/initrd.img or /boot/initrd.img in the installed system appears not to be a "
"symlink to an initial RAM disk image. This is probably a bug."
msgstr ""
"Kurulu sistemdeki /vmlinux veya /boot/vmlinux sembolik bağlarının herhangi "
"bir başlangıç RAM disk imajına işaret etmediği görünüyor. Bu muhtemelen bir "
"yazılım hatası."

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:15001
msgid "Installing quik into bootstrap partition"
msgstr "Quik önyükleme bölümüne kuruluyor"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:16001
msgid "Failed to install boot loader"
msgstr "Önyükleyici kurulumu başarısız"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:16001
msgid "The installation of the quik boot loader failed."
msgstr "Quik önyükleyici kurulumu başarısız."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:16001
msgid "Warning: your system may be unbootable!"
msgstr "Uyarı: Sisteminiz açılamaz duruma gelebilir!"

#. Type: text
#. Description
#. :sl4:
#: ../quik-installer.templates:17001
msgid "Setting up OpenFirmware"
msgstr "OpenFirmware ayarlanıyor"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:18001
msgid "Unable to configure OpenFirmware"
msgstr "OpenFirmware yapılandırılamadı"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:18001
msgid ""
"Setting the OpenFirmware boot-device variable failed. You will have to "
"configure OpenFirmware yourself to boot."
msgstr ""
"OpenFirmware'in boot-device (önyükleme-aygıtı) değişkeni ayarlanamadı. "
"Sistemin açılabilmesi için OpenFirmware'i kendiniz yapılandırmalısınız."

#. Type: error
#. Description
#. :sl4:
#. This error may not be fatal, depending on the exact type of Mac.
#: ../quik-installer.templates:19001
msgid "Problem configuring OpenFirmware"
msgstr "OpenFirmware yapılandırılmasında sorun oluştu"

#. Type: error
#. Description
#. :sl4:
#. This error may not be fatal, depending on the exact type of Mac.
#: ../quik-installer.templates:19001
msgid ""
"Setting the OpenFirmware boot-command variable failed. You may have "
"intermittent boot failures."
msgstr ""
"OpenFirmware'in boot-command (önyükleme-komutu) değişkeni ayarlanamadı. "
"Arada bir açılış hatalarıyla karşılaşabilirsiniz."

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001
msgid "Successfully installed quik"
msgstr "Quik kurulumu başarıyla sonuçlandı"

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001
msgid "The quik boot loader was successfully installed."
msgstr "Quik önyükleyici başarıyla kuruldu."

#. Type: note
#. Description
#. :sl4:
#: ../quik-installer.templates:20001
msgid "The new system is now ready to boot."
msgstr "Yeni sisteminiz artık açılmaya hazır."

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:21001
msgid "Failed to mount /target/proc"
msgstr "/target/proc bağlanması başarısız"

#. Type: error
#. Description
#. :sl4:
#: ../quik-installer.templates:21001
msgid "Mounting the proc file system on /target/proc failed."
msgstr "/target/proc üzerine proc dosya sistemi bağlanması başarısız."

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../quik-installer.templates:22001
msgid "Install quik on a hard disk"
msgstr "Quik'i bir sabit diske kur"
