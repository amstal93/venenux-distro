glantank (1.9) unstable; urgency=low

  * Drop finish-install.d/09fix-shadow since RTC support is built into
    the kernel now.
  * glantank-utils: depend on flash-kernel 2.7 since the functionality
    of glantank-update-kernel has been moved there now.  Update
    glantank-update-kernel and the package description to make it clear
    that this is a transitional package.
  * Drop the glantank-installer udeb since this is handled by
    flash-kernel-installer now.

 -- Martin Michlmayr <tbm@cyrius.com>  Sun, 24 Aug 2008 16:39:11 +0300

glantank (1.8) unstable; urgency=low

  [ Gordon Farquharson ]
  * Add an initramfs hook to set the root parition using
    conf/param.conf.

  [ Updated translations ]
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 10 May 2008 07:59:03 +0200

glantank (1.7) unstable; urgency=low

  [ Updated translations ]
  * Indonesian (id.po) by Arief S Fitrianto
  * Polish (pl.po) by Bartosz Fenski

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 15 Feb 2008 19:53:21 +0100

glantank (1.6) unstable; urgency=low

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Korean (ko.po) by Changwoo Ryu
  * Kurdish (ku.po) by Erdal Ronahi
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Panjabi (pa.po) by A S Alam
  * Vietnamese (vi.po) by Clytie Siddall

 -- Aurelien Jarno <aurel32@debian.org>  Mon, 04 Feb 2008 23:06:03 +0100

glantank (1.5) unstable; urgency=low

  * Multiply menu-item-numbers by 100

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Apr 2007 14:56:41 -0400

glantank (1.4) unstable; urgency=low

  [ Joey Hess ]
  * Add support for armel.

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 27 Feb 2007 19:51:45 +0000

glantank (1.3) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Danish (da.po) by Claus Hindsgaul
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Romanian (ro.po) by Eddy Petrișor

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 01 Feb 2007 16:26:27 +0100

glantank (1.2) unstable; urgency=medium

  * Move the code touching /etc/shadow from the postinst to a
    finish-install.d script so it will be run after user-setup's
    06user-setup.

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 23 Nov 2006 09:47:00 +0100

glantank (1.1) unstable; urgency=medium

  * Call sed in a way that the permission of /etc/shadow are not
    altered.  Thanks Joey Hess, Geert Stappers and David Härdeman.

  [ Updated translations ]
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 21 Nov 2006 14:47:22 +0000

glantank (1.0) unstable; urgency=medium

  * Clear the entry of days since 1970 that the passwd was last changed
    because there's no RTC support for this machine right now and SSH
    won't let you login otherwise.

  [ Updated translations ]
  * Bulgarian (bg.po) by Damyan Ivaniv
  * Bosnian (bs.po) by Safir Secerovic
  * Polish (pl.po) by Bartosz Fenski
  * Slovenian (sl.po) by Matej Kovačič

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 18 Nov 2006 21:43:12 +0000

glantank (0.5) unstable; urgency=high

  * Make sure the symlink to the initramfs in /boot is relative,
    otherwise the system will fail to boot.
  * Changed the priority of glantank-installer to standard so d-i will
    load it by default.
  * Add XB-Subarchitecture to the control file for glantank-installer.

 -- Martin Michlmayr <tbm@cyrius.com>  Thu, 26 Oct 2006 20:34:12 +0100

glantank (0.4) unstable; urgency=low

  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * German (de.po) by Jens Seidel
  * Greek, Modern (1453-) (el.po) by QUAD-nrg.net
  * Indonesian (id.po) by Arief S Fitrianto
  * Kurdish (ku.po) by Erdal Ronahi
  * Nepali (ne.po) by Shiva Prasad Pokharel
  * Albanian (sq.po) by Elian Myftiu
  * Tamil (ta.po) by Damodharan Rajalingam
  * Tagalog (tl.po) by Eric Pareja
  * Wolof (wo.po) by Mouhamadou Mamoune Mbacke
  * Simplified Chinese (zh_CN.po) by Ming Hua

 -- Martin Michlmayr <tbm@cyrius.com>  Tue, 24 Oct 2006 20:24:14 +0100

glantank (0.3) unstable; urgency=low

  [ Martin Michlmayr ]
  * Don't use the full path for glantank-update-kernel in the kernel hook.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Mahay Alam Khan (মাহে আলম)
  * Catalan (ca.po) by Jordi Mallach
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po) by Jurmey Rabgay
  * Esperanto (eo.po) by Serge Leblanc
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Stefano Canepa
  * Japanese (ja.po) by Kenshi Muto
  * Khmer (km.po) by Khoem Sokhem
  * Korean (ko.po) by Sunjae park
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Norwegian Bokmal (nb.po) by Bjørn Steensrud
  * Portuguese (pt.po) by Miguel Figueiredo
  * Portuguese (Brazil) (pt_BR.po) by André Luís Lopes
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Slovak (sk.po) by Peter Mann
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Turkish (tr.po) by Recai Oktaş
  * Ukrainian (uk.po) by Eugeniy Meshcheryakov
  * Vietnamese (vi.po) by Clytie Siddall
  * Traditional Chinese (zh_TW.po) by Tetralet

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 09 Oct 2006 17:11:40 +0100

glantank (0.2) unstable; urgency=low

  [ Christian Perrier ]
  * Rewrite the templates to fit D-I writing style. Add i18n stuff as well.

  [ Updated translations ]
  * Czech (cs.po) by Miroslav Kure
  * Danish (da.po) by Claus Hindsgaul
  * Greek (el.po) by quad-nrg.net
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * French (fr.po) by Christian Perrier
  * Galician (gl.po) by Jacobo Tarrio
  * Hebrew (he.po) by Lior Kaplan
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Lithuanian (lt.po) by Kęstutis Biliūnas
  * Macedonian (mk.po) by Georgi Stanojevski
  * Dutch (nl.po) by Bart Cornelis
  * Portuguese (pt.po) by Miguel Figueiredo
  * Romanian (ro.po) by Eddy Petrişor
  * Russian (ru.po) by Yuri Kozlov
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Tagalog (tl.po) by Eric Pareja

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 30 Sep 2006 16:34:13 +0200

glantank (0.1) unstable; urgency=low

  * Initial release.

 -- Martin Michlmayr <tbm@cyrius.com>  Mon, 25 Sep 2006 21:27:35 +0200

