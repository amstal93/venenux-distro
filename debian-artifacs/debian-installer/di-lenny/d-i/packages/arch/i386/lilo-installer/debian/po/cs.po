# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE
# packages/po/cs.po
#
# DO NOT MODIFY IT DIRECTLY : SUCH CHANGES WILL BE LOST
# 
# Czech messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2007-11-09 23:03+0100\n"
"Last-Translator:  Miroslav Kure <kurem@debian.cz>\n"
"Language-Team:  Czech <provoz@debian.cz>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../lilo-installer.templates:1001
msgid "${disc}: Master Boot Record"
msgstr "${disc}: Hlavní zaváděcí záznam"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../lilo-installer.templates:1001
msgid "${part}: new Debian partition"
msgstr "${part}: nová oblast Debianu"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../lilo-installer.templates:1001 ../lilo-installer.templates:2001
msgid "Other choice (Advanced)"
msgstr "Jiná volba (Pokročilé)"

#. Type: select
#. Description
#. :sl3:
#. Type: select
#. Description
#. :sl3:
#. Type: string
#. Description
#. :sl3:
#: ../lilo-installer.templates:1002 ../lilo-installer.templates:2002
#: ../lilo-installer.templates:3001
msgid "LILO installation target:"
msgstr "Cíl pro instalaci LILA:"

#. Type: select
#. Description
#. :sl3:
#: ../lilo-installer.templates:1002
msgid ""
"The LILO program needs to be installed to make your new system bootable. By "
"installing it onto your disk's Master Boot Record, LILO will take complete "
"control of the boot process, but if you want to use a different boot "
"manager, just install LILO on the new Debian partition instead."
msgstr ""
"Abyste mohli zavádět svůj nový systém, musí se nainstalovat program LILO. "
"Nainstalujete-li jej do hlavního zaváděcího záznamu svého disku, LILO "
"převezme kontrolu nad zaváděcím procesem. Chcete-li použít jiný zavaděč, "
"nainstalujte LILO do oblasti s nově instalovaným systémem."

#. Type: select
#. Description
#. :sl3:
#: ../lilo-installer.templates:1002
msgid "If unsure, install LILO into the Master Boot Record."
msgstr ""
"Pokud si nejste jisti, nainstalujte LILO do hlavního zaváděcího záznamu "
"(MBR)."

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../lilo-installer.templates:2001
msgid "${disc}: software RAID array"
msgstr "${disc}: pole softwarového RAIDu"

#. Type: select
#. Description
#. :sl3:
#: ../lilo-installer.templates:2002
msgid ""
"The LILO program needs to be installed to make your new system bootable. You "
"may choose to install it onto a software RAID array or another device."
msgstr ""
"Abyste mohli zavádět svůj nový systém, musí se nainstalovat program LILO. "
"Můžete jej nainstalovat na softwarový RAID nebo na jiné zařízení."

#. Type: string
#. Description
#. :sl3:
#: ../lilo-installer.templates:3001
msgid ""
"Please enter the device name of the partition or disk onto which LILO should "
"be installed, such as /dev/hda or /dev/sda1."
msgstr ""
"Zadejte prosím jméno zařízení oblasti nebo disku, kam se má LILO "
"nainstalovat. (Například /dev/hda nebo /dev/sda1.)"

#. Type: error
#. Description
#. :sl3:
#: ../lilo-installer.templates:4001
msgid "Invalid partition name"
msgstr "Neplatný název oblasti"

#. Type: error
#. Description
#. :sl3:
#: ../lilo-installer.templates:4001
msgid ""
"The path ${path} does not represent a partition or hard disk device. Please "
"try again."
msgstr ""
"Cesta ${path} nepředstavuje ani oblast, ani pevný disk. Zkuste to znovu."

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:5001
msgid "Installing LILO..."
msgstr "Instaluje se LILO..."

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:6001
msgid "Activating partition ${bootdev}"
msgstr "Aktivuje se oblast ${bootdev}"

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:7001
msgid "Creating lilo.conf"
msgstr "Vytváří se lilo.conf"

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:8001
msgid "Installing the LILO package"
msgstr "Instaluje se balík LILO"

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:9001
msgid "Running LILO for ${bootdev}"
msgstr "Spouští se LILO pro ${bootdev}"

#. Type: note
#. Description
#. :sl3:
#: ../lilo-installer.templates:10001
msgid "LILO configured to use a serial console"
msgstr "LILO bylo nastaveno, aby použilo konzoli na sériovém portu"

#. Type: note
#. Description
#. :sl3:
#: ../lilo-installer.templates:10001
msgid ""
"LILO is configured to use serial port ${PORT} as the console. ${PORT_SPEED}"
msgstr ""
"LILO je nastaveno, aby jako konzoli použilo sériový port ${PORT}. "
"${PORT_SPEED}"

#. Type: text
#. Description
#. :sl3:
#: ../lilo-installer.templates:11001
msgid "The serial port speed is set to ${SPEED}."
msgstr "Rychlost sériového portu je nastavena na ${SPEED}."

#. Type: boolean
#. Description
#. :sl3:
#: ../lilo-installer.templates:12001
msgid "Would you like to make this partition active?"
msgstr "Chcete tuto oblast nastavit jako aktivní?"

#. Type: boolean
#. Description
#. :sl3:
#: ../lilo-installer.templates:12001
msgid ""
"You have chosen to install LILO on a partition that is not the active one. "
"If this partition is not marked active, then LILO will not be loaded by the "
"boot loader.  This may cause you to be unable to boot into the system that "
"is being installed."
msgstr ""
"Chcete nainstalovat LILO na oblast, která není aktivní. Pokud oblast není "
"aktivní, zavaděč LILO nezavede a tím pádem se nemusíte dostat do "
"instalovaného systému."

#. Type: boolean
#. Description
#. :sl3:
#: ../lilo-installer.templates:12001
msgid ""
"You should make this partition active unless you have another boot loader "
"that will allow you to access your new Linux installation."
msgstr ""
"Pokud nemáte jiný zavaděč, kterým byste se dostali do svého nového systému, "
"měli byste tuto oblast označit jako aktivní."

#. Type: boolean
#. Description
#. :sl3:
#: ../lilo-installer.templates:13001
msgid "LILO installation failed.  Continue anyway?"
msgstr "Instalace LILO selhala. Přesto pokračovat?"

#. Type: boolean
#. Description
#. :sl3:
#: ../lilo-installer.templates:13001
msgid ""
"The lilo package failed to install into /target/.  Installing LILO as a boot "
"loader is a required step.  The install problem might however be unrelated "
"to LILO, so continuing the installation may be possible."
msgstr ""
"Balík lilo se nedokázal nainstalovat do /target. Instalace zavaděče (tedy "
"lila) je vyžadována. Nastalý problém ale nutně nemusí souviset s lilem a "
"možná lze v instalaci pokračovat."

#. Type: error
#. Description
#. :sl3:
#: ../lilo-installer.templates:14001
msgid "LILO installation failed"
msgstr "Instalace LILO selhala"

#. Type: error
#. Description
#. :sl3:
#: ../lilo-installer.templates:14001
msgid "Running \"/sbin/lilo\" failed with error code \"${ERRCODE}\"."
msgstr "Příkaz \"/sbin/lilo\" skončil s chybou \"${ERRCODE}\"."

#. Type: text
#. Description
#. Main menu item
#. :sl3:
#: ../lilo-installer.templates:15001
msgid "Install the LILO boot loader on a hard disk"
msgstr "Instalovat zavaděč LILO na pevný disk"
