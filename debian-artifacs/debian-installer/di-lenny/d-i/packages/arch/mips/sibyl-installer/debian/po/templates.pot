# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid "SiByl boot loader installation failed.  Continue anyway?"
msgstr ""

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid ""
"The SiByl package failed to install into /target/.  Installing SiByl as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to SiByl, so continuing the installation may be possible."
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:2001
msgid "Installing the SiByl boot loader"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:3001
msgid "Installing the SiByl package"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:4001
msgid "Creating SiByl configuration"
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../sibyl-installer.templates:5001
msgid "Install the SiByl boot loader on a hard disk"
msgstr ""

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:6001
msgid "SiByl boot partition"
msgstr ""
