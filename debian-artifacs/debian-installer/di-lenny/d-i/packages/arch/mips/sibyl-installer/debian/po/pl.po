# THIS FILE IS AUTOMATICALLY GENERATED FROM THE MASTER FILE:
# packages/po/pl.po
#
# DO NOT MODIFY IT DIRECTLY: SUCH CHANGES WILL BE LOST
# 
# Polish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Copyright (C) 2004-2006 Bartosz Feński <fenio@debian.org>
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2008-02-08 13:30+0100\n"
"Last-Translator: Bartosz Fenski <fenio@debian.org>\n"
"Language-Team: Polish <pddp@debian.linux.org.pl>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid "SiByl boot loader installation failed.  Continue anyway?"
msgstr ""
"Instalacja programu rozruchowego SiByl nie powiodła się. Kontynuować mimo "
"tego?"

#. Type: boolean
#. Description
#. :sl4:
#: ../sibyl-installer.templates:1001
msgid ""
"The SiByl package failed to install into /target/.  Installing SiByl as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to SiByl, so continuing the installation may be possible."
msgstr ""
"Instalacja pakietu SiByl w /target/ nie powiodła się. Instalacja SiByl jako "
"programu rozruchowego jest etapem wymaganym. Możliwe jednak, że błąd jest "
"niezwiązany z SiByl i kontynuacja instalacji będzie możliwa."

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:2001
msgid "Installing the SiByl boot loader"
msgstr "Instalacja programu rozruchowego SiByl"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:3001
msgid "Installing the SiByl package"
msgstr "Instalowanie pakietu SiByl"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:4001
msgid "Creating SiByl configuration"
msgstr "Tworzenie konfiguracji SiByl"

#. Type: text
#. Description
#. Main menu item
#. :sl4:
#: ../sibyl-installer.templates:5001
msgid "Install the SiByl boot loader on a hard disk"
msgstr "Zainstaluj program rozruchowy SiByl na dysku twardym"

#. Type: text
#. Description
#. :sl4:
#: ../sibyl-installer.templates:6001
msgid "SiByl boot partition"
msgstr "Partycja rozruchowa SiByl"
