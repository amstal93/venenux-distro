# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:1001
msgid "Installing vmelilo"
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:2001
msgid "Installing vmelilo boot loader"
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:3001
msgid "vmelilo installation failed.  Continue anyway?"
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:3001
msgid ""
"The vmelilo package failed to install into /target/.  Installing vmelilo as "
"a boot loader is a required step.  The install problem might however be "
"unrelated to vmelilo, so continuing the installation may be possible."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:4001
msgid "Looking for root partition..."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:5001
msgid "No root partition found"
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:5001
msgid ""
"No partition is mounted as your new root partition. You must mount a root "
"partition first."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:6001
msgid "Creating vmelilo configuration..."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001
msgid "Failed to create vmelilo configuration"
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001
msgid "The creation of the vmelilo configuration file failed."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001 ../vmelilo-installer.templates:9001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:7001 ../vmelilo-installer.templates:9001
msgid "Warning: Your system may be unbootable!"
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:8001
msgid "Installing vmelilo into bootstrap partition..."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:9001
msgid "Failed to install boot loader"
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../vmelilo-installer.templates:9001
msgid "The installation of the vmelilo boot loader failed."
msgstr ""

#. Type: text
#. Description
#. Main menu item
#. :sl5:
#: ../vmelilo-installer.templates:10001
msgid "Install the vmelilo boot loader on a hard disk"
msgstr ""
