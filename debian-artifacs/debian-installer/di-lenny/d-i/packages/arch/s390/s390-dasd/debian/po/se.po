# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of se.po to Northern Saami
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt#
#
# Børre Gaup <boerre@skolelinux.no>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: se\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:33+0000\n"
"PO-Revision-Date: 2006-11-25 21:21+0100\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Saami <i18n-sme@lister.ping.uio.no>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. :sl5:
#. TRANSLATORS, please translate "Finish" the same way you translate it in
#. the " Select "Finish" at the bottom of the list when you are done" string
#: ../s390-dasd.templates:1001
msgid "Finish"
msgstr ""

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available disks:"
msgstr ""

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following disk access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr ""

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose disk:"
msgstr ""

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a disk. You have to specify the complete device number, "
"including leading zeros."
msgstr ""

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
#, fuzzy
msgid "Invalid disk"
msgstr "Gustohis sturrodat"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
#, fuzzy
msgid "Format the disk?"
msgstr "Formatere partišuvnna:"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"The installer is unable to detect if the device ${device} has already been "
"formatted or not. Disks need to be formatted before you can create "
"partitions."
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"If you are sure the disk has already been correctly formatted, you don't "
"need to do so again."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
#, fuzzy
msgid "Formatting ${device}..."
msgstr "Ráhkadeamen ovttadatfiillaid ..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "Configure disk access storage devices (DASD)"
msgstr ""
