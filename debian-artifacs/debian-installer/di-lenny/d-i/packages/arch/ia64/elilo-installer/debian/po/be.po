# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of be.po to Belarusian (Official spelling)
#
# Andrei Darashenka <adorosh2@it.org.by>, 2005, 2006.
# Pavel Piatruk <berserker@neolocation.com>, 2006.
# Nasciona Piatrouskaja <naska.pet@gmail.com>, 2006.
# Hleb Rubanau <g.rubanau@gmail.com>, 2006.#.
# Pavel Piatruk <berserker@neolocation.com>, 2007.
# Pavel Piatruk <berserker@neolocation.com>, 2006, 2007, 2008.
# Hleb Rubanau <g.rubanau@gmail.com>, 2006, 2007.#.
# Nasciona Piatrouskaja <naska1@tut.by>, 2006.
# Andrei Darashenka <adorosh2@it.org.by>, 2005.#.
# Paul Petruk <berserker@neolocation.com>, 2007.
# Hleb Rubanau <g.rubanau@gmail.com>, 2006.
# Paul Petruk <berserker@neolocation.com>, 2007.##.
# Pavel Piatruk <piatruk.p@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: be\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-08-26 19:33+0300\n"
"Last-Translator: Pavel Piatruk <piatruk.p@gmail.com>\n"
"Language-Team: Belarusian (Official spelling) <i18n@mova.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid "Partition for boot loader installation:"
msgstr "Падзел для ўсталявання загрузчыка:"

#. Type: select
#. Description
#: ../elilo-installer.templates:1001
msgid ""
"Partitions currently available in your system are listed. Please choose the "
"one you want elilo to use to boot your new system."
msgstr ""
"У спісе пералічаныя падзелы, што даступныя ў Вашай сістэме. Калі ласка, "
"выберыце той, які elilo мусіць выкарыстоўваць для загрузкі Вашай новай "
"сістэмы."

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid "No boot partitions detected"
msgstr "Не вызначана ніводнага загрузачнага падзела."

#. Type: error
#. Description
#: ../elilo-installer.templates:2001
msgid ""
"There were no suitable partitions found for elilo to use.  Elilo needs a "
"partition with a FAT file system, and the boot flag set."
msgstr ""
"Няма падзелаў, прыдатных да выкарыстання загрузчыкам elilo. Загрузчыку Elilo "
"патрэбны падзел з файлавай сістэмай FAT і знакам \"загрузачны\"."

#. Type: text
#. Description
#. Main menu item
#: ../elilo-installer.templates:3001
msgid "Install the elilo boot loader on a hard disk"
msgstr "Усталяваць загрузчык elilo на жорсткі дыск."

#. Type: text
#. Description
#: ../elilo-installer.templates:4001
msgid "Installing the ELILO package"
msgstr "Усталяванне пакета ELILO"

#. Type: text
#. Description
#: ../elilo-installer.templates:5001
msgid "Running ELILO for ${bootdev}"
msgstr "Запуск ELILO дзеля ${bootdev}"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid "ELILO installation failed.  Continue anyway?"
msgstr "Усталёўка ELILO не атрымалася.  Працягнуць, не зважаючы на гэта?"

#. Type: boolean
#. Description
#: ../elilo-installer.templates:6001
msgid ""
"The elilo package failed to install into /target/.  Installing ELILO as a "
"boot loader is a required step.  The install problem might however be "
"unrelated to ELILO, so continuing the installation may be possible."
msgstr ""
"Пакет elilo не атрымалася ўсталяваць у /target/.  Усталяванне ELILO як "
"загрузчыка - неабходны крок.  Аднак, напатканая праблема можа не датычыцца "
"ўласна ELILO, тады ў Вас ёсць шанец працягнуць."

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "ELILO installation failed"
msgstr "Усталяванне ELILO не атрымалася"

#. Type: error
#. Description
#: ../elilo-installer.templates:7001
msgid "Running \"/usr/sbin/elilo\" failed with error code \"${ERRCODE}\"."
msgstr "Запуск \"/usr/sbin/elilo\" перарваўся з кодам памылкі \"${ERRCODE}\"."
