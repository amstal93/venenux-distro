# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Irish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-17 22:51+0000\n"
"PO-Revision-Date: 2006-03-21 14:42-0500\n"
"Last-Translator: Kevin Patrick Scannell <scannell@SLU.EDU>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:1001
msgid "Install the system"
msgstr "Suiteáil an córas"

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:2001
msgid "Installing the system..."
msgstr "Córas á shuiteáil..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:3001
msgid "Copying data to disk..."
msgstr ""

#. Type: text
#. Description
#. :sl3:
#. This string appears when the live-installer package
#. drop network settings that are used only by the
#. Live system
#: ../live-installer.templates:4001
msgid "Cleaning up network settings..."
msgstr "Socruithe líonra á nglanadh..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:5001
msgid "Removing packages specific to the live system..."
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:6001
msgid "Reconfiguring X.org..."
msgstr ""

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:7001
msgid "Running ${SCRIPT}..."
msgstr "${SCRIPT} á rith..."

#. Type: select
#. Choices
#. :sl3:
#: ../live-installer.templates:8001
msgid "normal"
msgstr ""

#. Type: select
#. Choices
#. :sl3:
#: ../live-installer.templates:8001
msgid "live"
msgstr ""

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid "Type of installation:"
msgstr ""

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid ""
"The live system can be installed to hard disk using one of the following "
"options."
msgstr ""

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid ""
"If you choose 'normal', the system will be installed as a regular system. If "
"you choose 'live', the installed system will continue to act as a live "
"system but it can then be launched directly from the hard disk."
msgstr ""
