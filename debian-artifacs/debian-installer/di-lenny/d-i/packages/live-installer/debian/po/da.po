# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_da.po to
# Danish messages for debian-installer.
# This file is distributed under the same license as debian-installer.
# Henrik Christian Grove <debian@3001.dk>, 2008.
# Jesper Dahl Nyerup <debian@jespernyerup.dk>, 2008.
# Jacob Sparre Andersen <sparre@nbi.dk>, 2008.
# Claus Hindsgaul <claus.hindsgaul@gmail.com>, 2004-2007.
# Reviewed 2007 by Niels Rasmussen
#
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_da\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-17 22:51+0000\n"
"PO-Revision-Date: 2008-09-19 16:35+0200\n"
"Last-Translator: \n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);X-Generator: KBabel 1.11.4\n"

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:1001
msgid "Install the system"
msgstr "Installér systemet"

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:2001
msgid "Installing the system..."
msgstr "Installerer systemet..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:3001
msgid "Copying data to disk..."
msgstr "Kopierer data til disk..."

#. Type: text
#. Description
#. :sl3:
#. This string appears when the live-installer package
#. drop network settings that are used only by the
#. Live system
#: ../live-installer.templates:4001
msgid "Cleaning up network settings..."
msgstr "Rydder op i netværksindstillinger..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:5001
msgid "Removing packages specific to the live system..."
msgstr "Fjerner pakker, som er specifikke for live-systemet..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:6001
msgid "Reconfiguring X.org..."
msgstr "Rekonfigurerer X.org..."

#. Type: text
#. Description
#. :sl3:
#: ../live-installer.templates:7001
msgid "Running ${SCRIPT}..."
msgstr "Udfører ${SCRIPT}..."

#. Type: select
#. Choices
#. :sl3:
#: ../live-installer.templates:8001
msgid "normal"
msgstr "normal"

#. Type: select
#. Choices
#. :sl3:
#: ../live-installer.templates:8001
msgid "live"
msgstr "live"

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid "Type of installation:"
msgstr "Installationstype:"

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid ""
"The live system can be installed to hard disk using one of the following "
"options."
msgstr ""
"Live-systemet kan installeres på harddisken ved hjælp af en af de følgende "
"valgmuligheder."

#. Type: select
#. Description
#. :sl3:
#: ../live-installer.templates:8002
msgid ""
"If you choose 'normal', the system will be installed as a regular system. If "
"you choose 'live', the installed system will continue to act as a live "
"system but it can then be launched directly from the hard disk."
msgstr ""
"Hvis du vælger 'normal', vil systemet blive installeret på almindelig vis. "
"Hvis du vælger 'live', vil systemet fortsat fungere som et live-system, men "
"kan da startes direkte fra harddisken."
