Template: debian-installer/iso-scan/title
Type: text
#  Main menu item
# :sl1:
_Description: Scan hard drives for an installer ISO image

Template: cdrom/suite
Type: select
Choices: stable, testing, unstable
Description: for internal use only
 Suite to install

Template: cdrom/codename
Type: string
Description: for internal use only
 Codename for the selected suite

Template: iso-scan/detect_progress_title
Type: text
# :sl3:
_Description: Detecting hardware to find hard drives

Template: iso-scan/progress_title
Type: text
# :sl3:
_Description: Searching drives for an installer ISO image

Template: iso-scan/progress_mount
Type: text
# :sl3:
_Description: Mounting ${DRIVE}...

Template: iso-scan/progress_scan
Type: text
# :sl3:
_Description: Scanning ${DRIVE} (in ${DIRECTORY})...

Template: iso-scan/ask_second_pass
Type: boolean
Default: false
# :sl3:
_Description: Do full disk search for installer ISO image?
 The quick scan for installer ISO images, which looks only in common
 places, did not find an installer ISO image. It's possible that a
 more thorough search will find the ISO image, but it may take a long
 time.

Template: iso-scan/no-isos
Type: error
# :sl3:
_Description: Failed to find an installer ISO image
 No installer ISO images were found. If you downloaded the ISO image,
 it may have a bad filename (not ending in ".iso"), or it may be on a
 file system that could not be mounted. You'll have to do a network
 install instead, or reboot and fix the ISO image.

Template: iso-scan/bad-isos
Type: error
# :sl3:
_Description: Failed to find an installer ISO image
 While one or more possible ISO images were found, they could not be mounted.
 The ISO image you downloaded may be corrupt.
 .
 Please do a network install instead, or reboot and fix the ISO image.

Template: iso-scan/other-isos
Type: error
# :sl3:
_Description: No installer ISO image found
 While one or more possible ISO images were found, they did not look like
 valid installer ISO images. 
 .
 Please do a network install instead, or reboot and fix the ISO image.

Template: iso-scan/success
Type: note
# :sl3:
_Description: Successfully mounted ${SUITE} installer ISO image
 The ISO file ${FILENAME} on ${DEVICE} (${SUITE}) will be used as the
 installation ISO image.

Template: iso-scan/filename
Type: text
Description: for internal use only
 For use by other parts of d-i, such as base-installer
