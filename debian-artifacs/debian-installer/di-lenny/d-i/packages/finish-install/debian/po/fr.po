# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to French
# Copyright (C) 2004-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Christian Perrier <bubulle@debian.org>, 2002-2004.
# Pierre Machard <pmachard@debian.org>, 2002-2004.
# Denis Barbier <barbier@debian.org>, 2002-2004.
# Philippe Batailler <philippe.batailler@free.fr>, 2002-2004.
# Michel Grentzinger <mic.grentz@online.fr>, 2003-2004.
# Christian Perrier <bubulle@debian.org>, 2005, 2006, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-01-26 07:32+0000\n"
"PO-Revision-Date: 2008-08-16 22:26-0300\n"
"Last-Translator: Christian Perrier <bubulle@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: text
#. Description
#. Main menu item
#. :sl1:
#: ../finish-install.templates:1001
msgid "Finish the installation"
msgstr "Terminer l'installation"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:2001
msgid "Finishing the installation"
msgstr "Fin de l'installation"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:3001
msgid "Running ${SCRIPT}..."
msgstr "Exécution du script ${SCRIPT}..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:4001
msgid "Configuring network..."
msgstr "Configuration du réseau..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:5001
msgid "Setting up frame buffer..."
msgstr "Configuration du tampon vidéo..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:6001
msgid "Unmounting file systems..."
msgstr "Démontage des systèmes de fichiers..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:7001
msgid "Rebooting into your new system..."
msgstr "Redémarrage du nouveau système..."

#. Type: note
#. Description
#. :sl1:
#: ../finish-install.templates:8001
msgid "Installation complete"
msgstr "Installation terminée"

#. Type: note
#. Description
#. :sl1:
#: ../finish-install.templates:8001
msgid ""
"Installation is complete, so it is time to boot into your new system. Make "
"sure to remove the installation media (CD-ROM, floppies), so that you boot "
"into the new system rather than restarting the installation."
msgstr ""
"L'installation est terminée et vous allez pouvoir maintenant démarrer le "
"nouveau système. Veuillez vérifier que le support d'installation (CD, "
"disquettes) est bien retiré afin que le nouveau système puisse démarrer et "
"éviter de relancer la procédure d'installation."
