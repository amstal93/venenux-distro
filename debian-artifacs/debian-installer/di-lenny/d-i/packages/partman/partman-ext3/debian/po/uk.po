# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of uk.po to Ukrainian
# translation of uk.po to
# Ukrainian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Eugeniy Meshcheryakov <eugen@debian.org>, 2005, 2006, 2007.
# Євгеній Мещеряков <eugen@debian.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: uk\n"
"Report-Msgid-Bugs-To: partman-ext3@packages.debian.org\n"
"POT-Creation-Date: 2009-06-27 12:50+0000\n"
"PO-Revision-Date: 2008-09-17 17:09+0300\n"
"Last-Translator: Borys Yanovych <borman@pravex.kiev.ua>\n"
"Language-Team: uk <>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: text
#. Description
#. :sl1:
#: ../partman-ext3.templates:1001
#, fuzzy
#| msgid ""
#| "Checking the ext3 file system in partition #${PARTITION} of ${DEVICE}..."
msgid ""
"Checking the ${TYPE} file system in partition #${PARTITION} of ${DEVICE}..."
msgstr ""
"Перевірка файлової системи ext3 на розділі #${PARTITION} пристрою "
"${DEVICE}..."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "Повернутися до меню та виправити помилки?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:2001
#, fuzzy
#| msgid ""
#| "The test of the file system with type ext3 in partition #${PARTITION} of "
#| "${DEVICE} found uncorrected errors."
msgid ""
"The test of the file system with type ${TYPE} in partition #${PARTITION} of "
"${DEVICE} found uncorrected errors."
msgstr ""
"Перевірка файлової системи типу ext3 на розділі #${PARTITION} пристрою "
"${DEVICE} виявила невиправлені помилки."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"Якщо ви не повернетесь до меню розбивки та не виправите помилки, то розділ "
"не буде використано взагалі."

#. Type: error
#. Description
#. :sl2:
#: ../partman-ext3.templates:3001
msgid "Failed to create a file system"
msgstr "Не вдалося створити файлову систему"

#. Type: error
#. Description
#. :sl2:
#: ../partman-ext3.templates:3001
#, fuzzy
#| msgid ""
#| "The ext3 file system creation in partition #${PARTITION} of ${DEVICE} "
#| "failed."
msgid ""
"The ${TYPE} file system creation in partition #${PARTITION} of ${DEVICE} "
"failed."
msgstr ""
"Не вдалося створити файлову систему ext3 на розділі #${PARTITION} пристрою "
"${DEVICE}."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "Чи бажаєте ви повернутися до меню розбивки?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:4001
#, fuzzy
#| msgid ""
#| "No mount point is assigned for the ext3 file system in partition #"
#| "${PARTITION} of ${DEVICE}."
msgid ""
"No mount point is assigned for the ${TYPE} file system in partition #"
"${PARTITION} of ${DEVICE}."
msgstr ""
"Для файлової системи ext3 на розділі #${PARTITION} пристрою ${DEVICE} не "
"призначено точку монтування."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"Якщо ви не повернетесь до меню розбивки та не призначите точку монтування, "
"то цей розділ не буде використовуватися взагалі."

#. Type: text
#. Description
#. :sl2:
#. This is an item in the menu "Action on the partition"
#: ../partman-ext3.templates:5001
msgid "Mount point:"
msgstr "Точка монтування:"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext3.templates:6001 ../partman-ext3.templates:8001
msgid "ext3"
msgstr "ext3"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-ext3.templates:7001
msgid "Ext3 journaling file system"
msgstr "файлова система Ext3"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext3.templates:9001 ../partman-ext3.templates:11001
msgid "ext4"
msgstr "ext4"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-ext3.templates:10001
msgid "Ext4 journaling file system"
msgstr "файлова система Ext4"

#. Type: boolean
#. Description
#. :sl2:
#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:12001 ../partman-ext3.templates:13001
msgid "Go back to the menu and correct this problem?"
msgstr "Повернутися до меню та виправити цю проблему?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:12001
msgid ""
"Your boot partition has not been configured with the ext2 or ext3 file "
"system. This is needed by your machine in order to boot. Please go back and "
"use either the ext2 or ext3 file system."
msgstr ""
"Ваш завантажувальний розділ не був сконфігурований з файловою системою ext2 "
"або ext3. Це є необхідною умовою для завантаження вашої машини. Будь ласка, "
"поверніться та вкажіть файлову систему ext2 або ext3."

#. Type: boolean
#. Description
#. :sl2:
#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:12001 ../partman-ext3.templates:13001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"Якщо ви не повернетесь до меню розбивки та не виправите цю помилку, то "
"розділ буде використаний „як є“. Це значить, що, можливо, ви не матимете "
"можливість завантажитися із вашого жорсткого диску."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-ext3.templates:13001
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""
"Ваш завантажувальний розділ не знаходиться на першому первинному розділі "
"вашого жорсткого диску. Це необхідно, щоб ваша система мала можливість "
"завантажуватися. Поверніться назад і використайте перший первинний розділ як "
"завантажувальний."
