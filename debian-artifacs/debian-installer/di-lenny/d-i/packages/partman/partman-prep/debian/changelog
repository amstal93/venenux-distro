partman-prep (16lenny1) unstable; urgency=high

  [ Christian Perrier ]
  * Rebuild with fixed Danish translation

 -- Colin Watson <cjwatson@debian.org>  Fri, 09 Jan 2009 23:28:14 +0000

partman-prep (16) unstable; urgency=low

  [ Frans Pop ]
  * Remove Cajus Pollmeier and Sven Luther as Uploaders with many thanks for
    their past contributions.

  [ Updated translations ]
  * Bosnian (bs.po) by Armin Besirovic
  * Welsh (cy.po) by Jonathan Price
  * Danish (da.po)
  * Esperanto (eo.po) by Felipe Castro
  * Basque (eu.po) by Iñaki Larrañaga Murgoitio
  * French (fr.po) by Christian Perrier
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Italian (it.po) by Milo Casagrande
  * Latvian (lv.po) by Peteris Krisjanis
  * Marathi (mr.po) by Sampada
  * Panjabi (pa.po) by Amanpreet Singh Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Serbian (sr.po) by Veselin Mijušković

 -- Jérémy Bobbio <lunar@debian.org>  Tue, 14 Oct 2008 01:22:01 +0200

partman-prep (15) unstable; urgency=low

  [ Updated translations ]
  * Amharic (am.po) by tegegne tefera
  * Finnish (fi.po) by Esko Arajärvi
  * Hindi (hi.po) by Kumar Appaiah
  * Indonesian (id.po) by Arief S Fitrianto
  * Korean (ko.po) by Changwoo Ryu
  * Kurdish (ku.po) by Amed Çeko Jiyan
  * Malayalam (ml.po) by Praveen|പ്രവീണ്‍ A|എ
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Polish (pl.po) by Bartosz Fenski

 -- Aurelien Jarno <aurel32@debian.org>  Fri, 15 Feb 2008 19:37:05 +0000

partman-prep (14) unstable; urgency=low

  [ Frans Pop ]
  * Moved definitions.sh to ./lib/base.sh. Requires partman-base (>= 114).
  * Rename update.d script in line with other components.
  * prep_sync_flag: use single assignment for adding flags.
  * Major whitespace cleanup and a few minor coding style fixes.

 -- Aurelien Jarno <aurel32@debian.org>  Fri, 15 Feb 2008 19:36:56 +0000

partman-prep (13) unstable; urgency=low

  [ Frans Pop ]
  * Move deletion of SVN directories to install-rc script.
  * Improve the way install-rc is called.
  * Let's make use of the fact that debhelper knows how to create a udeb.

  [ Colin Watson ]
  * Use 'mkdir -p' rather than more awkward test-then-create constructions.
  * Use debian/compat rather than DH_COMPAT.

  [ Updated translations ]
  * Danish (da.po) by Claus Hindsgaul
  * Italian (it.po) by Stefano Canepa
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Romanian (ro.po) by Eddy Petrișor
  * Vietnamese (vi.po) by Clytie Siddall

 -- Colin Watson <cjwatson@debian.org>  Tue, 02 Oct 2007 17:23:26 +0100

partman-prep (12) unstable; urgency=low

  * Remove valid_filesystems/prep script; since it doesn't return anything
    when called with 'existing' or 'formatable', and nothing in partman
    calls any valid_filesystems scripts with any other arguments, it appears
    to be a no-op.
  * Move sanity-checking scripts from finish.d to check.d. Requires
    partman-base 106.

 -- Colin Watson <cjwatson@debian.org>  Fri, 27 Apr 2007 10:00:02 +0100

partman-prep (11) unstable; urgency=low

  [ Updated translations ]
  * Malayalam (ml.po) by Praveen A

 -- Colin Watson <cjwatson@debian.org>  Tue, 27 Feb 2007 23:12:21 +0000

partman-prep (10) unstable; urgency=low

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Belarusian (be.po) by Pavel Piatruk
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * Esperanto (eo.po) by Serge Leblanc
  * Kurdish (ku.po) by rizoye-xerzi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Panjabi (pa.po) by A S Alam
  * Portuguese (Brazil) (pt_BR.po) by Felipe Augusto van de Wiel (faw)
  * Romanian (ro.po) by Eddy Petrișor
  * Slovenian (sl.po) by Matej Kovačič

 -- Colin Watson <cjwatson@debian.org>  Thu,  1 Feb 2007 10:53:45 +0000

partman-prep (9) unstable; urgency=low

  [ Wouter Verhelst ]
  * Add $SELF to uploaders
  * Release
  [ Updated translations ]
  * Belarusian (be.po) by Andrei Darashenka
  * Spanish (es.po) by Javier Fernández-Sanguino Peña
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Finnish (fi.po) by Tapio Lehtonen
  * Hindi (hi.po) by Nishant Sharma
  * Croatian (hr.po) by Josip Rodin
  * Hungarian (hu.po) by SZERVÁC Attila
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malayalam (ml.po) by Praveen A
  * Punjabi (Gurmukhi) (pa.po) by A S Alam
  * Tamil (ta.po) by Damodharan Rajalingam
  * Vietnamese (vi.po) by Clytie Siddall

 -- Wouter Verhelst <wouter@debian.org>  Thu, 26 Oct 2006 13:58:40 +0200

partman-prep (8) unstable; urgency=low

  [ Holger Levsen ]
  * added ppc64 to control (Closes: #301106, #301108)
  * Remove Standards-Version as it is not relevant for udebs

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bosnian (bs.po) by Safir Secerovic
  * Catalan (ca.po) by Jordi Mallach
  * Danish (da.po) by Claus Hindsgaul
  * German (de.po) by Jens Seidel
  * Dzongkha (dz.po)
  * Esperanto (eo.po) by Serge Leblanc
  * Estonian (et.po) by Siim Põder
  * Basque (eu.po) by Piarres Beobide
  * Irish (ga.po) by Kevin Patrick Scannell
  * Hungarian (hu.po) by SZERVÑC Attila
  * Italian (it.po) by Stefano Canepa
  * Georgian (ka.po) by Aiet Kolkhi
  * Khmer (km.po) by Leang Chumsoben
  * Kurdish (ku.po) by Erdal Ronahi
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Nepali (ne.po) by Shiva Pokharel
  * Norwegian Nynorsk (nn.po) by Håvard Korsvoll
  * Northern Sami (se.po) by Børre Gaup
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Thai (th.po) by Theppitak Karoonboonyanan
  * Vietnamese (vi.po) by Clytie Siddall

 -- Colin Watson <cjwatson@debian.org>  Thu, 13 Jul 2006 17:26:39 +0100

partman-prep (7) unstable; urgency=low

  [ Sven Luther ]
  * The prep partition is called 'prep' and not 'prep-boot'. I thought we had
    fixed this ages ago, but apparently not.

 -- Sven Luther <luther@debian.org>  Thu, 16 Feb 2006 07:36:59 +0000

partman-prep (6) unstable; urgency=low

  * Rename partman-prep/text/prep to partman/method_long/prep so that
    partman-target can use it (as the current method in use for a
    partition).
  * Use partman-prep/text/method rather than partman/method_long/prep as our
    entry in the "How to use this partition:" menu.

  [ Updated translations ]
  * Finnish (fi.po) by Tapio Lehtonen
  * Hungarian (hu.po) by SZERVÑC Attila
  * Slovak (sk.po) by Peter Mann
  * Slovenian (sl.po) by Matej Kovačič
  * Swedish (sv.po) by Daniel Nylander

 -- Colin Watson <cjwatson@debian.org>  Wed, 15 Feb 2006 10:54:27 +0000

partman-prep (5) unstable; urgency=low

  [ Colin Watson ]
  * Use 'rm -f' rather than more awkward test-then-remove constructions.

  [ Sven Luther ]
  * Adapted to chrp_rs6k -> chrp_ibm transition.

  [ Updated translations ]
  * Arabic (ar.po) by Ossama M. Khayat
  * Bengali (bn.po) by Baishampayan Ghose
  * Greek, Modern (1453-) (el.po) by Konstantinos Margaritis
  * French (fr.po) by Christian Perrier
  * Icelandic (is.po) by David Steinn Geirsson
  * Kurdish (ku.po) by Erdal Ronahi
  * Latvian (lv.po) by Aigars Mahinovs
  * Malagasy (mg.po) by Jaonary Rabarisoa
  * Polish (pl.po) by Bartosz Fenski
  * Romanian (ro.po) by Eddy Petrişor
  * Slovenian (sl.po) by Jure Čuhalev
  * Swedish (sv.po) by Daniel Nylander
  * Vietnamese (vi.po) by Clytie Siddall

 -- Colin Watson <cjwatson@debian.org>  Mon, 23 Jan 2006 12:34:58 +0000

partman-prep (4) unstable; urgency=low

  [ Frans Pop ]
  * Changed dependency from partman to partman-base.

  [ Sven Luther ]
  * Fixed typo in finish.d/prep, so partman-prep finish.d method will set the
    right boot_partitions value for prep-installer to use later on.
 -- Sven Luther <luther@debian.org>  Wed, 31 Aug 2005 18:30:26 +0200

partman-prep (3) unstable; urgency=low

  * Sven Luther
    - reupload to fix .po svn/merge mess :/

 -- Sven Luther <luther@debian.org>  Sun, 30 Jan 2005 07:30:11 +0100

partman-prep (2) unstable; urgency=low

  * Colin Watson
    - Encode templates in UTF-8.
  * Updated translations:
    - Czech (cs.po) by Miroslav Kure
    - German (de.po) by Dennis Stampfer
    - Greek, Modern (1453-) (el.po) by Greek Translation Team
    - Spanish (Castilian) (es.po) by Javier Fernandez-Sanguino Peña
    - Gallegan (gl.po) by Hctor Fenndez Lpez
    - Indonesian (id.po) by Arief S Fitrianto
    - Lithuanian (lt.po) by Kęstutis Biliūnas
    - Bøkmal, Norwegian (nb.po) by Hans Fredrik Nordhaug
    - Romanian (ro.po) by Eddy Petrisor
    - Russian (ru.po) by Dmitry Beloglazov
    - Slovak (sk.po) by Peter KLFMANiK Mann
    - Turkish (tr.po) by Recai Oktaş
    - Ukrainian (uk.po) by Eugeniy Meshcheryakov

 -- Colin Watson <cjwatson@debian.org>  Sat, 29 Jan 2005 20:18:38 +0000

partman-prep (1) unstable; urgency=low

  * Cajus Pollmeier :
    - Initial version forked from partman-palo packages
  * Sven Luther :
    - Added prep to XB-Subarchitecture field
    - Added partman-prep/boot_partitions debconf values for use with
      yaboot-installer and prep-installer.

 -- Sven Luther <luther@debian.org>  Fri,  7 Jan 2005 11:18:21 +0100

