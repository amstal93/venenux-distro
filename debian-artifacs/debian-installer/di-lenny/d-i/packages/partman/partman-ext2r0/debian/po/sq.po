# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Albanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-03-26 13:16+0000\n"
"PO-Revision-Date: 2007-09-26 23:09+0200\n"
"Last-Translator:  Elian Myftiu <elian.myftiu@gmail.com>\n"
"Language-Team: Debian L10n Albanian <debian-l10n-albanian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:1001
msgid ""
"Checking the ext2 (revision 0) file system in partition #${PARTITION} of "
"${DEVICE}..."
msgstr ""
"Duke kontrolluar filesistemin ext2 (revision 0) në ndarjen Nr. ${PARTITION} "
"të ${DEVICE}..."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid "Go back to the menu and correct errors?"
msgstr "Kthehu mbrapa në menu dhe korrigjo gabimet?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"The test of the file system with type ext2 (revision 0) in partition #"
"${PARTITION} of ${DEVICE} found uncorrected errors."
msgstr ""
"Testimi i filesistemit të llojit ext2 (revision 0) në ndarjen Nr. "
"${PARTITION} të ${DEVICE} gjeti gabime të pakorrigjueshme."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:2001
msgid ""
"If you do not go back to the partitioning menu and correct these errors, the "
"partition will not be used at all."
msgstr ""
"Nëse nuk kthehesh mbrapa tek menuja e ndarjes të korrigjosh këto gabime, "
"atëhere ndarja nuk do përdoret fare."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid "Failed to create a file system"
msgstr "Dështoi në krijimin e filesistemit"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:3001
msgid ""
"The ext2 (revision 0) file system creation in partition #${PARTITION} of "
"${DEVICE} failed."
msgstr ""
"Krijimi i filesistemit ext2 (revision 0) në ndarjen Nr. ${PARTITION} të "
"${DEVICE} dështoi."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid "Do you want to return to the partitioning menu?"
msgstr "Dëshiron të kthehesh tek menuja e ndarjes?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"No mount point is assigned for the ext2 (revision 0) file system in "
"partition #${PARTITION} of ${DEVICE}."
msgstr ""
"Nuk është caktuar asnjë pikë montimi për filesistemin ext2 (revision 0) në "
"ndarjen Nr. ${PARTITION} të ${DEVICE}."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:4001
msgid ""
"If you do not go back to the partitioning menu and assign a mount point from "
"there, this partition will not be used at all."
msgstr ""
"Nëse nuk kthehesh mbrapa tek menuja e ndarjes dhe nuk cakton një pikë "
"montimi që aty, kjo ndarje nuk do përdoret fare."

#. Type: select
#. Choices
#. :sl4:
#. what's to be entered is a mount point
#: ../partman-ext2r0.templates:5001
msgid "Enter manually"
msgstr "Krijo vetë"

#. Type: select
#. Choices
#. :sl4:
#. "it" is a partition
#: ../partman-ext2r0.templates:5001
msgid "Do not mount it"
msgstr "Mos e monto"

#. Type: select
#. Description
#. Type: string
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:5002 ../partman-ext2r0.templates:6001
msgid "Mount point for this partition:"
msgstr "Pika e montimit për këtë ndarje:"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Invalid mount point"
msgstr "Pikë montimi e pavlefshme"

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "The mount point you entered is invalid."
msgstr "Pika e montimit që zgjodhe është e pavlefshme."

#. Type: error
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:7001
msgid "Mount points must start with \"/\". They cannot contain spaces."
msgstr "Pikat e montimit duhet të fillojnë me \"/\". Nuk lejohen hapësirat."

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#: ../partman-ext2r0.templates:8001 ../partman-ext2r0.templates:10001
msgid "ext2r0"
msgstr "ext2r0"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-ext2r0.templates:9001
msgid "old Ext2 (revision 0) file system"
msgstr "filesistemi i vjetër Ext2 (rishikim 0)"

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid "Go back to the menu and correct this problem?"
msgstr "Të kthehem mbrapa në menu dhe të korrigjoj gabimet?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001
msgid ""
"Your boot partition has not been configured with the old ext2 (revision 0) "
"file system.  This is needed by your machine in order to boot.  Please go "
"back and use the old ext2 (revision 0) file system."
msgstr ""
"Ndarja e nisjes nuk është konfiguruar me filesistemin e vjetër ext2 "
"(revision 0).  Kjo është e nevojshme në mënyrë që kompjuteri të niset.  Të "
"lutem kthehu mbrapa dhe përdor filesistemin e vjetër ext2 (revision 0)."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:11001 ../partman-ext2r0.templates:12001
#: ../partman-ext2r0.templates:13001
msgid ""
"If you do not go back to the partitioning menu and correct this error, the "
"partition will be used as is.  This means that you may not be able to boot "
"from your hard disk."
msgstr ""
"Nëse nuk kthehesh mbrapa tek menuja e ndarjes të korrigjosh këto gabime, "
"atëhere ndarja nuk do përdoret fare.  Ndoshta mund të mos jesh në gjendje të "
"nisesh nga disku i ngurtë."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:12001
msgid ""
"Your boot partition is not located on the first primary partition of your "
"hard disk.  This is needed by your machine in order to boot.  Please go back "
"and use your first primary partition as a boot partition."
msgstr ""
"Ndarja e nisjes nuk gjendet në ndarjen e parë primare të diskut të ngurtë.  "
"Kjo është e nevojshme në mënyrë që kompjuteri të niset.  Të lutem kthehu "
"mbrapa dhe përdor ndarjen e parë primare si ndarje nisjeje."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-ext2r0.templates:13001
msgid ""
"Your root partition is not a primary partition of your hard disk.  This is "
"needed by your machine in order to boot.  Please go back and use a primary "
"partition for your root partition."
msgstr ""
"Ndarja root nuk është një ndarje primare e diskut të ngurtë.  Kjo është e "
"nevojshme në mënyrë që kompjuteri të niset.  Të lutem kthehu mbrapa dhe "
"përdor një ndarje primare për ndarjen root."
