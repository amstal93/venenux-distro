# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of ku.po to Kurdish
# Kurdish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Rizoyê Xerzî <riza dot seckin at gmail dot com>
# Erdal Ronahi <erdal dot ronahi at gmail dot com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: ku\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-31 22:50+0000\n"
"PO-Revision-Date: 2008-09-13 14:02+0200\n"
"Last-Translator: Amed Çeko Jiyan <amedcj@gmail.com>\n"
"Language-Team: ku <ubuntu-l10n-kur@lists.ubuntu.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!= 1);\n"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:1001
msgid "Software RAID device"
msgstr "Cîhaza RAID ya Nivîsbariyê"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:2001
msgid "Configure software RAID"
msgstr "Nivîsbariya RAID veava bike"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001
msgid "Write the changes to the storage devices and configure RAID?"
msgstr ""
"Bila guherîn li cîhazên depokirinê were tomarkirin û RAID were veavakirin?"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001
msgid ""
"Before RAID can be configured, the changes have to be written to the storage "
"devices.  These changes cannot be undone."
msgstr ""
"Berî ku RAID were veavakirin divê berê berê guherîn li cîhazên depokirinê "
"were tomarkirin. Ev guherîn dû re nayê vegerandin."

#. Type: boolean
#. Description
#. :sl3:
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:3001 ../partman-md.templates:4001
msgid ""
"When RAID is configured, no additional changes to the partitions in the "
"disks containing physical volumes are allowed.  Please convince yourself "
"that you are satisfied with the current partitioning scheme in these disks."
msgstr ""
"Di dema veavakirina RAIDê de li ser beşên dîskan de ye yên cîltên fîzîkî "
"dihundirîne, destûr nayê dayîn ku bila guherîneke din were kirin. Ji kerema "
"xwe re sax bin bê ka dabeşkirinên heyî yên li ser van dîskan gorî xwestekên "
"te ye yan na."

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:4001
msgid "Keep current partition layout and configure RAID?"
msgstr "Bila pergala dabeşkirina heyî were parastin û RAID were veavakirin?"

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "RAID configuration failure"
msgstr "Çewtiya veavakirina RAIDê"

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "An error occurred while writing the changes to the storage devices."
msgstr "Dema guherîn li cîhazên depokirinê dihate tomarkirin çewtiyek çêbû."

#. Type: error
#. Description
#. :sl3:
#: ../partman-md.templates:5001
msgid "RAID configuration has been aborted."
msgstr "Veavakirina RAIDê hate betalkirin."

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:6001
msgid "physical volume for RAID"
msgstr "Volume ya fîzîkî ji bo RAID"

#. Type: text
#. Description
#. :sl3:
#: ../partman-md.templates:7001
msgid "raid"
msgstr "raid"

#  msgid "Done setting up the partition"
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Remove existing software RAID partitions?"
msgstr "Partîsiyonên RAID yên nivîsbariyê yên heyî werin rakirin?"

#  msgid ""
#  "The selected device already contains the following LVM logical volumes, "
#  "volume groups and physical volumes which are about to be removed:"
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid ""
"The selected device contains partitions used for software RAID devices. The "
"following devices and partitions are about to be removed:"
msgstr ""
"Cîhaza ku hatiye hilbijartin partîsiyonên ji bo cîhazên RAiD dihundirîne. Ew "
"cîhaz û partîsiyon dê werin rakirin:"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Software RAID devices about to be removed: ${REMOVED_DEVICES}"
msgstr "Cîhaza Software RAID li bêr rakirinê ye: ${REMOVED_DEVICES}"

#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid "Partitions used by these RAID devices: ${REMOVED_PARTITIONS}"
msgstr ""
"Partition ji hêla van cîhazên RAID ve hat bikaranîn: ${REMOVED_PARTITIONS}"

#  msgid ""
#  "Note that this will also permanently erase any data currently on the "
#  "logical volumes."
#. Type: boolean
#. Description
#. :sl3:
#: ../partman-md.templates:8001
msgid ""
"Note that this will also permanently erase any data currently on the "
"software RAID devices."
msgstr ""
"Nîşe: Her wisa evê her tim hemû daneyên li ser cîhazên RAID yên nivîsbarî jê "
"bibe."
