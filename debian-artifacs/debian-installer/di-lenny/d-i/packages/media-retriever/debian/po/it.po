# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Italian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# The translation team (for all four levels):
# Cristian Rigamonti <cri@linux.it>
# Danilo Piazzalunga <danilopiazza@libero.it>
# Davide Meloni <davide_meloni@fastwebnet.it>
# Davide Viti <zinosat@tiscali.it>
# Filippo Giunchedi <filippo@esaurito.net>
# Giuseppe Sacco <eppesuig@debian.org>
# Lorenzo 'Maxxer' Milesi 
# Renato Gini
# Ruggero Tonelli
# Samuele Giovanni Tonon <samu@linuxasylum.net>
# Stefano Canepa <sc@linux.it>
# Stefano Melchior <stefano.melchior@openlabs.it>
# Milo Casagrande <milo@ubuntu.com>, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-10 22:49+0000\n"
"PO-Revision-Date: 2008-07-24 21:58+0200\n"
"Last-Translator: Milo Casagrande <milo@ubuntu.com>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:1001
msgid "Scanning removable media"
msgstr "Analisi del supporto rimovibile"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid "Cannot read removable media, or no drivers found."
msgstr ""
"Impossibile leggere il supporto rimovibile o non è stato trovato alcun "
"driver."

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid ""
"There was a problem reading data from the removable media. Please make sure "
"that the right media is present. If you continue to have trouble, your "
"removable media might be bad."
msgstr ""
"Si è verificato un errore nel leggere i dati dal supporto rimovibile, "
"assicurarsi che sia stato inserito il supporto corretto. In caso di continui "
"errori, il supporto rimovibile potrebbe essere difettoso."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid "Load drivers from removable media now?"
msgstr "Caricare i driver dal supporto rimovibile ora?"

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"You probably need to load drivers from removable media before continuing "
"with the installation. If you know that the install will work without extra "
"drivers, you can skip this step."
msgstr ""
"Probabilmente si devono caricare dei driver da un supporto rimovibile prima "
"di continuare l'installazione. Se si è certi che l'installazione andrà "
"avanti senza la necessità di altri driver, saltare questo passo."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"If you do need to load drivers, insert the appropriate removable media, such "
"as a driver floppy or USB stick before continuing."
msgstr ""
"Nel caso si debbano caricare dei driver, inserire il supporto rimovibile "
"appropriato, come un driver floppy o una chiave USB, prima di continuare."

#. Type: text
#. Description
#. main-menu
#: ../load-media.templates:2001
msgid "Load drivers from removable media"
msgstr "Caricare driver da supporto rimovibile"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid "Unknown removable media. Try to load anyway?"
msgstr "Supporto rimovibile sconosciuto. Caricarlo ugualmente?"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid ""
"Detected removable media that is not a known driver media. Please make sure "
"that the correct media is inserted. You can still continue if you have an "
"unofficial removable media you want to use."
msgstr ""
"Il supporto rimovibile rilevato non contiene driver. Verificare che sia "
"stato inserito il supporto corretto. È possibile proseguire anche "
"utilizzando un supporto rimovibile non ufficiale."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid "Please insert ${DISK_LABEL} ('${DISK_NAME}') first."
msgstr "Inserire ${DISK_LABEL} («${DISK_NAME}») per primo."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid ""
"Due to dependencies between packages, drivers must be loaded in the correct "
"order."
msgstr ""
"A causa di dipendenze tra i pacchetti, i dischetti driver devono essere "
"caricati nell'ordine corretto."

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid "Load drivers from another removable media?"
msgstr "Caricare i driver da un altro supporto rimovibile?"

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid ""
"To load additional drivers from another removable media, please insert the "
"appropriate removable media, such as a driver floppy or USB stick before "
"continuing."
msgstr ""
"Per caricare ulteriori driver da un altro supporto rimovibile, inserire il "
"supporto appropriato, come un driver floppy o una chiave USB, prima di "
"continuare."
