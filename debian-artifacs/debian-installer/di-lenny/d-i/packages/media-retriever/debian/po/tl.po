# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Tagalog messages for debian-installer.
# Copyright (C) 2004-2008 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Ipinamamahagi ang talaksang ito alinsunod sa lisensiya ng debian-installer.
# Eric Pareja <xenos@upm.edu.ph>, 2004-2008
# Rick Bahague, Jr. <rbahaguejr@gmail.com>, 2004
# Reviewed by Roel Cantada on Feb-Mar 2005.
# Sinuri ni Roel Cantada noong Peb-Mar 2005.
# This file is maintained by Eric Pareja <xenos@upm.edu.ph>
# Inaalagaan ang talaksang ito ni Eric Pareja <xenos@upm.edu.ph>
#
# ituloy angsulong mga kapatid http://www.upm.edu.ph/~xenos
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2008-07-10 22:49+0000\n"
"PO-Revision-Date: 2008-06-18 07:02+0800\n"
"Last-Translator: Eric Pareja <xenos@upm.edu.ph>\n"
"Language-Team: Tagalog <debian-tl@banwa.upm.edu.ph>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:1001
msgid "Scanning removable media"
msgstr "Tinatanaw ang removable media"

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid "Cannot read removable media, or no drivers found."
msgstr "Hindi mabasa ang removable media, o walang natagpuang drivers dito."

#. Type: text
#. Description
#. :sl2:
#: ../media-retriever.templates:2001
msgid ""
"There was a problem reading data from the removable media. Please make sure "
"that the right media is present. If you continue to have trouble, your "
"removable media might be bad."
msgstr ""
"Nagkaroon ng problema sa pagbasa ng datos mula sa removable media. Paki-"
"siguro na naka-kasa ang tamang  media sa drive. Kung patuloy na magka-"
"problema, maaaring may sira ang inyong removable media."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid "Load drivers from removable media now?"
msgstr "Ipasok ang mga CD-ROM driver mula sa removable media?"

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"You probably need to load drivers from removable media before continuing "
"with the installation. If you know that the install will work without extra "
"drivers, you can skip this step."
msgstr ""
"Baka kinakailangan ninyong magpasok ng mga driver mula sa removable media "
"bago kayo magpatuloy ng pagluklok. Kung alam ninyong aandar ang inyong "
"install na walang extra na mga driver, maaari ninyong laktawan ang hakbang "
"na ito."

#. Type: boolean
#. Description
#: ../load-media.templates:1001
msgid ""
"If you do need to load drivers, insert the appropriate removable media, such "
"as a driver floppy or USB stick before continuing."
msgstr ""
"Kung kinakailangan ninyong magpasok ng mga driver, ikasa ang tugmang "
"removable media tulad ng driver floppy o USB stick bago magpatuloy."

#. Type: text
#. Description
#. main-menu
#: ../load-media.templates:2001
msgid "Load drivers from removable media"
msgstr "Ipasok ang mga drivers mula sa removable media?"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid "Unknown removable media. Try to load anyway?"
msgstr "Di kilalang removable media. Subukang ipasok pa rin?"

#. Type: boolean
#. Description
#: ../load-media.templates:3001
msgid ""
"Detected removable media that is not a known driver media. Please make sure "
"that the correct media is inserted. You can still continue if you have an "
"unofficial removable media you want to use."
msgstr ""
"Hindi kilala ang removable media bilang driver media. Pakitiyak na naka-kasa "
"ang tamang media sa drive. Maaari kayong magpatuloy kung may hindi opisyal "
"na removable media ang nais ninyong gamitin."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid "Please insert ${DISK_LABEL} ('${DISK_NAME}') first."
msgstr "Pakipasok muna ang ${DISK_LABEL} ('${DISK_NAME}')."

#. Type: text
#. Description
#: ../load-media.templates:4001
msgid ""
"Due to dependencies between packages, drivers must be loaded in the correct "
"order."
msgstr ""
"Dahil sa mga dependensiya ng mga pakete, kinakailangang ipasok ang mga "
"drivers sa tamang pagkakasunud-sunod."

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid "Load drivers from another removable media?"
msgstr "Ipasok ang mga drivers mula sa iba pang removable media?"

#. Type: boolean
#. Description
#: ../load-media.templates:5001
msgid ""
"To load additional drivers from another removable media, please insert the "
"appropriate removable media, such as a driver floppy or USB stick before "
"continuing."
msgstr ""
"Kung kinakailangan ninyong magpasok ng mga driver mula sa iba removable "
"media, ikasa ang wastong removable media tulad ng driver floppy or USB stick "
"bago magpatuloy."
