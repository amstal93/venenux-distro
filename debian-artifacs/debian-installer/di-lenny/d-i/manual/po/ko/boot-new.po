# boot-new.xml
# Kwangwoo Lee <kwlee@dynamicroot.org>, 2005
# Changwoo Ryu <cwryu@debian.org>, 2005, 2006, 2007.
#
# 이 번역은 완성 단계로 품질에 많은 신경을 쓰고 있습니다. 반드시 메일링 리스트에
# 번역 사실을 알리고 토의를 하십시오.
#
msgid ""
msgstr ""
"Project-Id-Version: boot-new.xml\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2007-02-27 20:31+0000\n"
"PO-Revision-Date: 2007-12-24 22:05+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: Korean <debian-l10n-korean@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-xml2pot; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Tag: title
#: boot-new.xml:5
#, no-c-format
msgid "Booting Into Your New Debian System"
msgstr "새로운 데비안 시스템으로 부팅하기"

#. Tag: title
#: boot-new.xml:7
#, no-c-format
msgid "The Moment of Truth"
msgstr "진실의 시간"

#. Tag: para
#: boot-new.xml:8
#, no-c-format
msgid ""
"Your system's first boot on its own power is what electrical engineers call "
"the <quote>smoke test</quote>."
msgstr "시스템이 혼자 힘으로 하는 최초의 부팅을 전기 엔지니어들은 <quote>스모크 테스트(smoke test)</quote>라고 부릅니다."

#. Tag: para
#: boot-new.xml:13
#, no-c-format
msgid ""
"If you did a default installation, the first thing you should see when you "
"boot the system is the menu of the <classname>grub</classname> or possibly "
"the <classname>lilo</classname> bootloader. The first choices in the menu "
"will be for your new Debian system. If you had any other operating systems "
"on your computer (like Windows) that were detected by the installation "
"system, those will be listed lower down in the menu."
msgstr "기본값으로 설치를 마쳤다면, 시스템을 부팅할 때 맨 처음에 <classname>grub</classname> 메뉴나 <classname>lilo</classname> 부트로더가 나타날 것입니다. 메뉴의 첫번째로 들어가면 새로 설치한 데비안 시스템입니다. 설치할 때 컴퓨터의 다른 운영 체제를 (윈도우즈 등) 찾았다면 그 운영 체제도 메뉴의 아래 부분에 있습니다."

#. Tag: para
#: boot-new.xml:23
#, no-c-format
msgid ""
"If the system fails to start up correctly, don't panic. If the installation "
"was successful, chances are good that there is only a relatively minor "
"problem that is preventing the system from booting Debian. In most cases "
"such problems can be fixed without having to repeat the installation. One "
"available option to fix boot problems is to use the installer's built-in "
"rescue mode (see <xref linkend=\"rescue\"/>)."
msgstr "시스템이 제대로 시작하지 않아도 당황하지 마십시오. 설치할 때 문제가 없었다면, 대부분은 데비안 부팅만 안 되는 비교적 작은 문제일 뿐입니다. 이러한 문제는 보통 다시 설치하지 않더라도 해결할 수 있습니다. 해결하는 한 가지 방법은 설치 프로그램에 내장된 응급 복구 모드를 이용하는 것입니다. (<xref linkend=\"rescue\"/> 참고.)"

#. Tag: para
#: boot-new.xml:32
#, no-c-format
msgid ""
"If you are new to Debian and Linux, you may need some help from more "
"experienced users. <phrase arch=\"x86\">For direct on-line help you can try "
"the IRC channels #debian or #debian-boot on the OFTC network. Alternatively "
"you can contact the <ulink url=\"&url-list-subscribe;\">debian-user mailing "
"list</ulink>.</phrase> <phrase arch=\"not-x86\">For less common "
"architectures like &arch-title;, your best option is to ask on the <ulink "
"url=\"&url-list-subscribe;\">debian-&arch-listname; mailing list</ulink>.</"
"phrase> You can also file an installation report as described in <xref "
"linkend=\"submit-bug\"/>. Please make sure that you describe your problem "
"clearly and include any messages that are displayed and may help others to "
"diagnose the issue."
msgstr "데비안 및 리눅스를 처음 접한다면, 좀 더 경험이 많은 사람에게 도움을 청해야 할 경우도 있습니다. <phrase arch=\"x86\">직접적인 온라인 도움을 요청하려면 OFTC 네트워크의 #debian이나 #debian-boot를 이용해 보십시오. 아니면 <ulink url=\"&url-list-subscribe;\">debian-user 메일링 리스트</ulink>에 연락할 수도 있습니다.</phrase> <phrase arch=\"not-x86\">&arch-title;처럼 많이 사용하지 않는 아키텍처의 경우에는, <ulink url=\"&url-list-subscribe;\">debian-&arch-listname; 메일링 리스트</ulink>를 이용하는 게 가장 좋습니다.</phrase> <xref linkend=\"submit-bug\"/>의 설명에 따라 설치 보고서를 제출할 수도 있습니다. 여러분이 겪은 문제를 분명하게 설명하고 화면에 표시된 메세지를 모두 포함해 주십시오. 그래야 다른 사람이 더 쉽게 이 문제를 파악할 수 있습니다."

#. Tag: para
#: boot-new.xml:48
#, no-c-format
msgid ""
"If you had any other operating systems on your computer that were not "
"detected or not detected correctly, please file an installation report."
msgstr "컴퓨터에 다른 운영 체제가 있지만 설치할 때 찾지 못했거나 제대로 찾지 못했다면, 설치 보고서를 제출하십시오."

#. Tag: title
#: boot-new.xml:55
#, no-c-format
msgid "BVME 6000 Booting"
msgstr "BVME 6000 부팅"

#. Tag: para
#: boot-new.xml:56
#, no-c-format
msgid ""
"If you have just performed a diskless install on a BVM or Motorola VMEbus "
"machine: once the system has loaded the <command>tftplilo</command> program "
"from the TFTP server, from the <prompt>LILO Boot:</prompt> prompt enter one "
"of:"
msgstr "만일 BVM 또는 모토로라 VMEbus 컴퓨터에 디스크 없이 (diskless) 설치를 수행한 직후라면, TFTP 서버에서 <command>tftplilo</command> 프로그램을 로드한 뒤에 <prompt>LILO Boot:</prompt> 프롬프트에서 다음 중 하나를 입력합니다:"

#. Tag: para
#: boot-new.xml:64
#, no-c-format
msgid ""
"<userinput>b6000</userinput> followed by &enterkey; to boot a BVME 4000/6000"
msgstr "BVME 4000/6000에서 부팅하려면, <userinput>b6000</userinput>이라고 입력하고 &enterkey;를 누릅니다."

#. Tag: para
#: boot-new.xml:69
#, no-c-format
msgid "<userinput>b162</userinput> followed by &enterkey; to boot an MVME162"
msgstr "MVME162에서 부팅하려면, <userinput>b162</userinput>라고 입력하고 &enterkey;를 누릅니다."

#. Tag: para
#: boot-new.xml:74
#, no-c-format
msgid ""
"<userinput>b167</userinput> followed by &enterkey; to boot an MVME166/167"
msgstr "MVME166/167에서 부팅하려면, <userinput>b167</userinput>이라고 입력하고 &enterkey;를 누릅니다."

#. Tag: title
#: boot-new.xml:86
#, no-c-format
msgid "Macintosh Booting"
msgstr "매킨토시 부팅"

#. Tag: para
#: boot-new.xml:88
#, no-c-format
msgid ""
"Go to the directory containing the installation files and start up the "
"<command>Penguin</command> booter, holding down the <keycap>command</keycap> "
"key. Go to the <userinput>Settings</userinput> dialogue (<keycombo> "
"<keycap>command</keycap> <keycap>T</keycap> </keycombo>), and locate the "
"kernel options line which should look like <userinput>root=/dev/ram "
"ramdisk_size=15000</userinput> or similar."
msgstr "설치 파일이 들어 있는 디렉토리로 가서, <keycap>command</keycap>을 누른채로 <command>Penguin</command> booter를 시작합니다. <userinput>Settings</userinput> 대화 상자로 가서 (<keycombo> <keycap>command</keycap> <keycap>T</keycap> </keycombo>), <userinput>root=/dev/ram ramdisk_size=15000</userinput>처럼 되어 있는 커널 옵션을 찾으십시오."

#. Tag: para
#: boot-new.xml:98
#, no-c-format
msgid ""
"You need to change the entry to <userinput>root=/dev/<replaceable>yyyy</"
"replaceable></userinput>. Replace the <replaceable>yyyy</replaceable> with "
"the Linux name of the partition onto which you installed the system (e.g. "
"<filename>/dev/sda1</filename>); you wrote this down earlier. For users with "
"tiny screens, adding <userinput>fbcon=font:VGA8x8</userinput> (or "
"<userinput>video=font:VGA8x8</userinput> on pre-2.6 kernels) may help "
"readability. You can change this at any time."
msgstr "해당 항목을 <userinput>root=/dev/<replaceable>yyyy</replaceable></userinput>로 바꿔야 합니다. <replaceable>yyyy</replaceable>는 시스템을 설치한 파티션의 리눅스 이름입니다. (예를 들어 <filename>/dev/sda1</filename>.) 앞에서 이 이름을 적어 뒀어야 합니다. 화면이 작은 사용자의 경우 <userinput>fbcon=font:VGA8x8</userinput> (혹은 2.6보다 오래된 커널의 경우 <userinput>video=font:VGA8x8</userinput>) 설정을 하면 더 보기 좋습니다. 이 설정은 언제든 바꿀 수 있습니다."

#. Tag: para
#: boot-new.xml:109
#, no-c-format
msgid ""
"If you don't want to start GNU/Linux immediately each time you start, "
"uncheck the <userinput>Auto Boot</userinput> option. Save your settings in "
"the <filename>Prefs</filename> file using the <userinput>Save Settings As "
"Default</userinput> option."
msgstr "시작할 때 즉시 GNU/리눅스로 시작하지 않으려면, <userinput>Auto Boot</userinput> 옵션을 사용하지 마십시오. <userinput>Save Settings As Default</userinput> 옵션을 사용해 설정을 <filename>Prefs</filename> 파일에 저장하십시오."

#. Tag: para
#: boot-new.xml:116
#, no-c-format
msgid ""
"Now select <userinput>Boot Now</userinput> (<keycombo> <keycap>command</"
"keycap> <keycap>B</keycap> </keycombo>) to start your freshly installed GNU/"
"Linux instead of the RAMdisk installer system."
msgstr "이제 <userinput>Boot Now</userinput> (<keycombo> <keycap>command</keycap> <keycap>B</keycap> </keycombo>) 명령어로 램디스크에 설치한 시스템이 아니라 새롭게 설치한 GNU/리눅스를 시작합니다."

#. Tag: para
#: boot-new.xml:122
#, no-c-format
msgid ""
"Debian should boot, and you should see the same messages as when you first "
"booted the installation system, followed by some new messages."
msgstr "데비안이 부팅하고, 처음 설치 시스템 부팅할 때와 똑같은 메시지가 나오고, 뒤이어서 처음 보는 메시지도 나올 것입니다."

#. Tag: title
#: boot-new.xml:132
#, no-c-format
msgid "OldWorld PowerMacs"
msgstr "올드월드 파워맥"

#. Tag: para
#: boot-new.xml:133
#, no-c-format
msgid ""
"If the machine fails to boot after completing the installation, and stops "
"with a <prompt>boot:</prompt> prompt, try typing <userinput>Linux</"
"userinput> followed by &enterkey;. (The default boot configuration in "
"<filename>quik.conf</filename> is labeled Linux). The labels defined in "
"<filename>quik.conf</filename> will be displayed if you press the "
"<keycap>Tab</keycap> key at the <prompt>boot:</prompt> prompt. You can also "
"try booting back into the installer, and editing the <filename>/target/etc/"
"quik.conf</filename> placed there by the <guimenuitem>Install Quik on a Hard "
"Disk</guimenuitem> step. Clues for dealing with <command>quik</command> are "
"available at <ulink url=\"&url-powerpc-quik-faq;\"></ulink>."
msgstr "설치를 마친 다음 부팅이 실패하면, <prompt>boot:</prompt> 프롬프트에서 멈춰서 <userinput>Linux</userinput>를 입력해보십시오. (<filename>quik.conf</filename> 안에 기본 부팅 설정이 Linux라는 레이블로 되어 있습니다.) <prompt>boot:</prompt> 프롬프트에서 <keycap>Tab</keycap> 키를 누르면 <filename>quik.conf</filename>에 정의한 레이블 목록을 표시합니다. 설치 프로그램으로 돌아가 <guimenuitem>Install Quik on a Hard Disk</guimenuitem> 단계에서 설정한 <filename>/target/etc/quik.conf</filename> 파일을 바꿔서 부팅할 수도 있습니다. <command>quik</command>를 사용하는 여러가지 힌트는 <ulink url=\"&url-powerpc-quik-faq;\"></ulink>에서 읽을 수 있습니다."

#. Tag: para
#: boot-new.xml:147
#, no-c-format
msgid ""
"To boot back into MacOS without resetting the nvram, type <userinput>bye</"
"userinput> at the OpenFirmware prompt (assuming MacOS has not been removed "
"from the machine). To obtain an OpenFirmware prompt, hold down the "
"<keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>o</"
"keycap> <keycap>f</keycap> </keycombo> keys while cold booting the machine. "
"If you need to reset the OpenFirmware nvram changes to the MacOS default in "
"order to boot back to MacOS, hold down the <keycombo> <keycap>command</"
"keycap> <keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </"
"keycombo> keys while cold booting the machine."
msgstr "nvram을 리셋하지 않고 맥오에스로 다시 부팅하려면, OpenFirmware 프롬프트에서 <userinput>bye</userinput>를 입력합니다. (맥오에스를 컴퓨터에서 지우지 않았다고 가정합니다.) 콜드 부팅하는 동안 <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>o</keycap> <keycap>f</keycap> </keycombo> 키를 누르고 있으면 OpenFirmware 프롬프트가 나타납니다. OpenFirmware nvram을 리셋해서 맥오에스 기본값으로 돌아가려면 (다시 맥오에스 부팅으로 돌아가려면), 콜드 부팅하는 동안 <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </keycombo> 키를 누르고 있으십시오."

#. Tag: para
#: boot-new.xml:160
#, no-c-format
msgid ""
"If you use <command>BootX</command> to boot into the installed system, just "
"select your desired kernel in the <filename>Linux Kernels</filename> folder, "
"un-choose the ramdisk option, and add a root device corresponding to your "
"installation; e.g. <userinput>/dev/hda8</userinput>."
msgstr "설치한 시스템으로 부팅할 때 <command>BootX</command>를 사용한다면, <filename>Linux Kernels</filename> 폴더에서 원하는 커널을 선택하고, 램디스크 옵션의 선택을 해제한 뒤에 설치에 사용한 루트 장치 이름을 입력하십시오. 예를 들어 <userinput>/dev/hda8</userinput>과 같이 씁니다."

#. Tag: title
#: boot-new.xml:172
#, no-c-format
msgid "NewWorld PowerMacs"
msgstr "뉴월드 파워맥"

# &debian; => 데비안 GNU/리눅스, 조사 구분에 주의
#. Tag: para
#: boot-new.xml:173
#, no-c-format
msgid ""
"On G4 machines and iBooks, you can hold down the <keycap>option</keycap> key "
"and get a graphical screen with a button for each bootable OS, &debian; will "
"be a button with a small penguin icon."
msgstr "G4와 아이북에서 <keycap>option</keycap> 키를 누르고 있으면, 부팅 가능한 운영 체제마다 단추가 하나씩 있는 그래픽 화면이 나타납니다. &debian;는 작은 펭귄 아이콘이 들어 있는 단추입니다."

#. Tag: para
#: boot-new.xml:180
#, no-c-format
msgid ""
"If you kept MacOS and at some point it changes the OpenFirmware <envar>boot-"
"device</envar> variable you should reset OpenFirmware to its default "
"configuration. To do this hold down the <keycombo> <keycap>command</keycap> "
"<keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </keycombo> "
"keys while cold booting the machine."
msgstr "맥오에스를 계속 유지하는 경우 맥오에스에서 OpenFirmware의 <envar>boot-device</envar> 변수를 바꾸면, OpenFirmware를 기본값 설정으로 리셋해야 합니다. 리셋하려면 콜드 부팅하는 동안 <keycombo> <keycap>command</keycap> <keycap>option</keycap> <keycap>p</keycap> <keycap>r</keycap> </keycombo> 키를 누르고 있으십시오."

#. Tag: para
#: boot-new.xml:188
#, no-c-format
msgid ""
"The labels defined in <filename>yaboot.conf</filename> will be displayed if "
"you press the <keycap>Tab</keycap> key at the <prompt>boot:</prompt> prompt."
msgstr "<prompt>boot:</prompt> 프롬프트에서 <keycap>tab</keycap> 키를 누르면 <filename>yaboot.conf</filename>에 정의한 레이블을 표시합니다."

# &debian; => 데비안 GNU/리눅스, 조사 구분에 주의
#. Tag: para
#: boot-new.xml:194
#, no-c-format
msgid ""
"Resetting OpenFirmware on G3 or G4 hardware will cause it to boot &debian; "
"by default (if you correctly partitioned and placed the Apple_Bootstrap "
"partition first). If you have &debian; on a SCSI disk and MacOS on an IDE "
"disk this may not work and you will have to enter OpenFirmware and set the "
"<envar>boot-device</envar> variable, <command>ybin</command> normally does "
"this automatically."
msgstr "G3 또는 G4 하드웨어에서 OpenFirmware를 리셋하면 기본으로 &debian;를 부팅합니다. (올바르게 파티션을 나누고 Apple_Bootstrap 파티션이 앞쪽에 놓여 있는 경우.) 데비안이 SCSI 디스크에 들어 있고 맥오에스가 IDE 디스크에 들어 있으면, 리셋해도 데비안으로 부팅하지 않을 것입니다. OpenFirmware에 들어가 <envar>boot-device</envar> 변수를 설정해야 합니다. <command>ybin</command> 프로그램을 이용하면 이 변수 설정을 자동으로 합니다."

# &debian; => 데비안 GNU/리눅스, 조사 구분에 주의
#. Tag: para
#: boot-new.xml:203
#, no-c-format
msgid ""
"After you boot &debian; for the first time you can add any additional "
"options you desire (such as dual boot options) to <filename>/etc/yaboot."
"conf</filename> and run <command>ybin</command> to update your boot "
"partition with the changed configuration. Please read the <ulink url=\"&url-"
"powerpc-yaboot-faq;\">yaboot HOWTO</ulink> for more information."
msgstr "&debian;가 처음으로 부팅하면, <filename>/etc/yaboot.conf</filename>에 (듀얼 부팅 옵션 등) 원하는 어떤 옵션이라도 추가 할 수 있고, <command>ybin</command>를 실행하면 바꾼 설정에 맞춰 부팅 파티션을 업데이트할 수 있습니다. 더 자세한 정보는 <ulink url=\"&url-powerpc-yaboot-faq;\">yaboot HOWTO</ulink>를 읽어 보십시오."

#. Tag: title
#: boot-new.xml:221
#, no-c-format
msgid "Mounting encrypted volumes"
msgstr "암호화 볼륨 마운트하기"

#. Tag: para
#: boot-new.xml:223
#, no-c-format
msgid ""
"If you created encrypted volumes during the installation and assigned them "
"mount points, you will be asked to enter the passphrase for each of these "
"volumes during the boot. The actual procedure differs slightly between dm-"
"crypt and loop-AES."
msgstr "설치할 때 암호화 볼륨을 만들고 마운트 위치를 지정했다면, 부팅할 때 각각의 볼륨에 대해 암호를 입력하게 됩니다. 실제 절차는 dm-crypt와 loop-AES가 약간 다릅니다."

#. Tag: title
#: boot-new.xml:233
#, no-c-format
msgid "dm-crypt"
msgstr "dm-crypt"

#. Tag: para
#: boot-new.xml:235
#, no-c-format
msgid ""
"For partitions encrypted using dm-crypt you will be shown the following "
"prompt during the boot: <informalexample><screen>\n"
"Starting early crypto disks... <replaceable>part</replaceable>_crypt"
"(starting)\n"
"Enter LUKS passphrase:\n"
"</screen></informalexample> In the first line of the prompt, "
"<replaceable>part</replaceable> is the name of the underlying partition, e."
"g. sda2 or md0. You are now probably wondering <emphasis>for which volume</"
"emphasis> you are actually entering the passphrase. Does it relate to your "
"<filename>/home</filename>? Or to <filename>/var</filename>? Of course, if "
"you have just one encrypted volume, this is easy and you can just enter the "
"passphrase you used when setting up this volume. If you set up more than one "
"encrypted volume during the installation, the notes you wrote down as the "
"last step in <xref linkend=\"partman-crypto\"/> come in handy. If you did "
"not make a note of the mapping between <filename><replaceable>part</"
"replaceable>_crypt</filename> and the mount points before, you can still "
"find it in <filename>/etc/crypttab</filename> and <filename>/etc/fstab</"
"filename> of your new system."
msgstr ""
"dm-crypt로 암호화한 파티션의 경우 부팅할 때 다음과 같이 물어봅니다: <informalexample><screen>\n"
"Starting early crypto disks... <replaceable>part</replaceable>_crypt(starting)\n"
"Enter LUKS passphrase:\n"
"</screen></informalexample> 첫번째 줄에서, <replaceable>part</replaceable>는 실제 파티션의 이름입니다. (예를 들어 sda2나 md0.) 여기에서 과연 <emphasis>어떤 볼륨의</emphasis> 암호를 실제로 입력해야 하는 지 궁금할 것입니다. <filename>/home</filename>일까요? 아니면 <filename>/var</filename>일까요? 물론, 암호화 볼륨이 1개뿐이라면, 이 볼륨을 설정할 때 사용한 암호를 입력하면 됩니다. 설치할 때 암호화 볼륨을 여러 개 설정했다면, <xref linkend=\"partman-crypto\"/>의 마지막 단계에서 적어 놓은 메모를 잘 가지고 있어야 합니다. <filename><replaceable>part</replaceable>_crypt</filename>에 해당되는 사항과 거기에 해당하는 마운트 위치를 적어 놓지 않았다면 새로 설치한 시스템의 <filename>/etc/crypttab</filename>과 <filename>/etc/fstab</filename>에서 찾아 볼 수도 있습니다."

#. Tag: para
#: boot-new.xml:258
#, no-c-format
msgid ""
"The prompt may look somewhat different when an encrypted root file system is "
"mounted. This depends on which initramfs generator was used to generate the "
"initrd used to boot the system. The example below is for an initrd generated "
"using <classname>initramfs-tools</classname>:"
msgstr "이 프롬프트는 암호화한 루트 파일시스템을 마운트할 때는 약간 다릅니다. 시스템을 부팅할 때 사용하는 initrd를 만들 때 어떤 initramfs 만들기 프로그램을 사용했느냐에 따라 다릅니다. 아래의 예제는 <classname>initramfs-tools</classname>로 initrd를 만들 경우에 대한 예제입니다:"

#. Tag: screen
#: boot-new.xml:265
#, no-c-format
msgid ""
"Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
"Begin: Running /scripts/local-top ...\n"
"Enter LUKS passphrase:"
msgstr ""
"Begin: Mounting <emphasis>root file system</emphasis>... ...\n"
"Begin: Running /scripts/local-top ...\n"
"Enter LUKS passphrase:"

#. Tag: para
#: boot-new.xml:267 boot-new.xml:292
#, no-c-format
msgid ""
"No characters (even asterisks) will be shown while entering the passphrase. "
"If you enter the wrong passphrase, you have two more tries to correct it. "
"After the third try the boot process will skip this volume and continue to "
"mount the next filesystem. Please see <xref linkend=\"crypto-troubleshooting"
"\"/> for further information."
msgstr "암호를 입력할 때는 아무런 글자도 (별표 조차도) 나타나지 않습니다. 암호를 잘못 입력하면 두 번 더 시도할 수 있습니다. 세 번째 시도에서 틀리면 부팅 과정에서 해당 볼륨을 건너뛰고 다음 파일 시스템으로 넘어갑니다. 자세한 정보는 <xref linkend=\"crypto-troubleshooting\"/> 부분을 보십시오."

#. Tag: para
#: boot-new.xml:275 boot-new.xml:300
#, no-c-format
msgid "After entering all passphrases the boot should continue as usual."
msgstr "암호를 모두 입력하면 부팅은 평소처럼 계속 진행합니다."

#. Tag: title
#: boot-new.xml:283
#, no-c-format
msgid "loop-AES"
msgstr "loop-AES"

#. Tag: para
#: boot-new.xml:285
#, no-c-format
msgid ""
"For partitions encrypted using loop-AES you will be shown the following "
"prompt during the boot:"
msgstr ""
"loop-AES를 사용해 암호화한 파티션의 경우 부팅할 때 다음 프롬프트가 나옵니다:"

#. Tag: screen
#: boot-new.xml:290
#, no-c-format
msgid ""
"Checking loop-encrypted file systems.\n"
"Setting up /dev/loop<replaceable>X</replaceable> (/<replaceable>mountpoint</"
"replaceable>)\n"
"Password:"
msgstr ""
"Checking loop-encrypted file systems.\n"
"Setting up /dev/loop<replaceable>X</replaceable> (/<replaceable>mountpoint</"
"replaceable>)\n"
"Password:"

#. Tag: title
#: boot-new.xml:308
#, no-c-format
msgid "Troubleshooting"
msgstr "문제 해결"

#. Tag: para
#: boot-new.xml:310
#, no-c-format
msgid ""
"If some of the encrypted volumes could not be mounted because a wrong "
"passphrase was entered, you will have to mount them manually after the boot. "
"There are several cases."
msgstr "암호가 틀려서 암호화 볼륨을 마운트하지 못할 경우, 부팅한 다음에 수동으로 마운트해야 합니다. 여러가지 경우가 있습니다."

#. Tag: para
#: boot-new.xml:319
#, no-c-format
msgid ""
"The first case concerns the root partition. When it is not mounted "
"correctly, the boot process will halt and you will have to reboot the "
"computer to try again."
msgstr "첫번째 경우는 루트 파티션입니다. 올바르게 마운트하지 않으면, 부팅 과정이 멈추게 되고 컴퓨터를 다시 시작해서 암호를 다시 입력해야 합니다."

#. Tag: para
#: boot-new.xml:326
#, no-c-format
msgid ""
"The easiest case is for encrypted volumes holding data like <filename>/home</"
"filename> or <filename>/srv</filename>. You can simply mount them manually "
"after the boot. For loop-AES this is a one-step operation: "
"<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></"
"userinput>\n"
"<prompt>Password:</prompt>\n"
"</screen></informalexample> where <replaceable>/mount_point</replaceable> "
"should be replaced by the particular directory (e.g. <filename>/home</"
"filename>). The only difference from an ordinary mount is that you will be "
"asked to enter the passphrase for this volume."
msgstr ""
"가장 쉬운 경우는 암호화 볼륨에 <filename>/home</filename>이나 <filename>/srv</filename>처럼 데이터가 들어 있는 경우입니다. 부팅한 다음에 이 볼륨을 수동으로 마운트하기만 하면 됩니다. loop-AES의 경우 한 번에 끝나는 작업입니다: <informalexample><screen> <prompt>#</prompt> <userinput>mount <replaceable>/마운트_위치</replaceable></userinput>\n"
"<prompt>Password:</prompt>\n"
"</screen></informalexample> 여기서 <replaceable>/마운트_위치</replaceable>는 해당 디렉토리를 (예를 들어 <filename>/home</filename>) 씁니다. 일반적인 마운트와 다른 부분은 이 볼륨의 암호를 입력하는 것 뿐입니다."

#. Tag: para
#: boot-new.xml:340
#, no-c-format
msgid ""
"For dm-crypt this is a bit trickier. First you need to register the volumes "
"with <application>device mapper</application> by running: "
"<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
"</screen></informalexample> This will scan all volumes mentioned in "
"<filename>/etc/crypttab</filename> and will create appropriate devices under "
"the <filename>/dev</filename> directory after entering the correct "
"passphrases. (Already registered volumes will be skipped, so you can repeat "
"this command several times without worrying.) After successful registration "
"you can simply mount the volumes the usual way:"
msgstr ""
"dm-crypt의 경우에는 약간 까다롭습니다. 먼저 해당 볼륨을 다음 명령어로 <application>device mapper</application>에 등록해야 합니다: <informalexample><screen>\n"
"<prompt>#</prompt> <userinput>/etc/init.d/cryptdisks start</userinput>\n"
"</screen></informalexample> 이렇게 하면 <filename>/etc/crypttab</filename>에 들어 있는 모든 볼륨을 검색하고 암호를 올바르게 입력할 때마다 <filename>/dev</filename> 디렉토리 아래에 적당한 장치를 만듭니다. (이미 등록한 볼륨은 건너 뛰므로, 걱정하지 말고 이 명령어를 여러번 실행해도 됩니다.) 올바르게 등록을 마치면 해당 볼륨을 평소와 다름없이 마운트할 수 있습니다:"

#. Tag: screen
#: boot-new.xml:355
#, no-c-format
msgid ""
"<prompt>#</prompt> <userinput>mount <replaceable>/mount_point</replaceable></"
"userinput>"
msgstr ""
"<prompt>#</prompt> <userinput>mount <replaceable>/마운트_위치</replaceable></"
"userinput>"

#. Tag: para
#: boot-new.xml:358
#, no-c-format
msgid ""
"If any volume holding noncritical system files could not be mounted "
"(<filename>/usr</filename> or <filename>/var</filename>), the system should "
"still boot and you should be able to mount the volumes manually like in the "
"previous case. However, you will also need to (re)start any services usually "
"running in your default runlevel because it is very likely that they were "
"not started. The easiest way to achieve this is by switching to the first "
"runlevel and back by entering <informalexample><screen>\n"
"<prompt>#</prompt> <userinput>init 1</userinput>\n"
"</screen></informalexample> at the shell prompt and pressing <keycombo> "
"<keycap>Control</keycap> <keycap>D</keycap> </keycombo> when asked for the "
"root password."
msgstr ""
"꼭 필요하지는 않은 시스템 파일이 들어 있는 볼륨중에 하나라도 (<filename>/usr</filename> 혹은 <filename>/var</filename>) 마운트할 수 없는 경우, 그래도 시스템이 부팅하고 수동으로 볼륨을 마운트할 수 있습니다. 하지만 현재 런레벨의 각종 서비스를 (다시) 시작해야 할 수도 있습니다. 서비스가 제대로 시작하지 않았을 가능성이 높기 때문입니다. 가장 쉬운 방법은 쉘에서 다음과 같은 명령어로 첫번째 런레벨로 갔다가 다시 돌아오는 방법입니다:<informalexample><screen>\n"
"<prompt>#</prompt> <userinput>init 1</userinput>\n"
"</screen></informalexample> 이렇게 입력하고 루트 암호를 물어보면 <keycombo> <keycap>Control</keycap> <keycap>D</keycap> </keycombo>를 누릅니다."

#. Tag: title
#: boot-new.xml:381
#, no-c-format
msgid "Log In"
msgstr "로그인"

#. Tag: para
#: boot-new.xml:383
#, no-c-format
msgid ""
"Once your system boots, you'll be presented with the login prompt. Log in "
"using the personal login and password you selected during the installation "
"process. Your system is now ready for use."
msgstr "패키지 설치가 끝나면 로그인 프롬프트를 표시합니다. 설치할 때 입력한 개인 로그인 및 암호를 이용해 로그인합니다. 그러면 이제 시스템을 사용할 준비를 다 마쳤습니다."

#. Tag: para
#: boot-new.xml:389
#, no-c-format
msgid ""
"If you are a new user, you may want to explore the documentation which is "
"already installed on your system as you start to use it. There are currently "
"several documentation systems, work is proceeding on integrating the "
"different types of documentation. Here are a few starting points."
msgstr "처음 설치한 사용자라면 문서를 살펴보고 싶을 것입니다. 이 문서는 시스템을 시작할 때부터 시스템 안에 설치되어 있습니다. 현재 여러 개의 문서 시스템이 있고, 여러가지 종류의 문서를 통합하는 작업을 진행하고 있습니다. 다음과 같은 방법으로 문서 보기를 시작할 수 있습니다."

#. Tag: para
#: boot-new.xml:397
#, no-c-format
msgid ""
"Documentation accompanying programs you have installed can be found in "
"<filename>/usr/share/doc/</filename>, under a subdirectory named after the "
"program (or, more precise, the Debian package that contains the program). "
"However, more extensive documentation is often packaged separately in "
"special documentation packages that are mostly not installed by default. For "
"example, documentation about the package management tool <command>apt</"
"command> can be found in the packages <classname>apt-doc</classname> or "
"<classname>apt-howto</classname>."
msgstr "설치한 프로그램에 들어 있는 문서는 <filename>/usr/share/doc/</filename> 아래에, 그 프로그램의 이름으로 (정확히 말해 그 프로그램이 들어 있는 데비안 패키지의 이름으로) 된 서브 디렉토리에 들어 있습니다. 하지만 이보다 자세한 문서는 별도의 문서 패키지에 들어 있고, 이 패키지는 보통 기본으로 설치하지 않습니다. 예를 들어 <command>apt</command> 패키지 관리 도구에 관한 문서는 <classname>apt-doc</classname> 혹은 <classname>apt-howto</classname> 패키지에 들어 있습니다."

#. Tag: para
#: boot-new.xml:408
#, no-c-format
msgid ""
"In addition, there are some special folders within the <filename>/usr/share/"
"doc/</filename> hierarchy. Linux HOWTOs are installed in <emphasis>.gz</"
"emphasis> (compressed) format, in <filename>/usr/share/doc/HOWTO/en-txt/</"
"filename>. After installing <classname>dhelp</classname>, you will find a "
"browsable index of documentation in <filename>/usr/share/doc/HTML/index."
"html</filename>."
msgstr "또 <filename>/usr/share/doc/</filename> 아래에 특수 폴더가 몇 개 더 있습니다. 리눅스 HOWTO는 <filename>/usr/share/doc/HOWTO/en-txt/</filename> 안에 <emphasis>.gz</emphasis> (압축한) 형식으로 들어 있습니다. <classname>dhelp</classname>를 설치하면 <filename>/usr/share/doc/HTML/index.html</filename> 파일에 브라우저로 볼 수 있는 문서 목록이 있습니다."

#. Tag: para
#: boot-new.xml:417
#, no-c-format
msgid ""
"One easy way to view these documents using a text based browser is to enter "
"the following commands: <informalexample><screen>\n"
"$ cd /usr/share/doc/\n"
"$ w3m .\n"
"</screen></informalexample> The dot after the <command>w3m</command> command "
"tells it to show the contents of the current directory."
msgstr ""
"다음 명령으로 텍스트 기반 브라우저를 사용하면 간단히 이 문서를 볼 수 있습니다 : <informalexample><screen>\n"
"$ cd /usr/share/doc/\n"
"$ w3m .\n"
"</screen></informalexample> <command>w3m</command> 명령 다음에 나오는 점은 현재 디렉토리의 내용을 표시한다는 뜻입니다."

#. Tag: para
#: boot-new.xml:427
#, no-c-format
msgid ""
"If you have a graphical desktop environment installed, you can also use its "
"web browser. Start the web browser from the application menu and enter "
"<userinput>/usr/share/doc/</userinput> in the address bar."
msgstr "그래픽 데스크탑 환경을 설치했다면, 그 환경의 웹 브라우저를 이용할 수 있습니다. 프로그램 메뉴에서 웹 브라우저를 실행해서 주소창에 <userinput>/usr/share/doc/</userinput>을 입력하고 Enter를 누르십시오."

#. Tag: para
#: boot-new.xml:433
#, no-c-format
msgid ""
"You can also type <userinput>info <replaceable>command</replaceable></"
"userinput> or <userinput>man <replaceable>command</replaceable></userinput> "
"to see documentation on most commands available at the command prompt. "
"Typing <userinput>help</userinput> will display help on shell commands. And "
"typing a command followed by <userinput>--help</userinput> will usually "
"display a short summary of the command's usage. If a command's results "
"scroll past the top of the screen, type <userinput>|&nbsp;more</userinput> "
"after the command to cause the results to pause before scrolling past the "
"top of the screen. To see a list of all commands available which begin with "
"a certain letter, type the letter and then two tabs."
msgstr "<userinput>info <replaceable>명령어</replaceable></userinput> 또는 <userinput>man <replaceable>명령어</replaceable></userinput> 명령을 입력하면, 명령어 프롬프트에서 사용할 수 있는 대부분의 명령에 대한 문서를 볼 수 있습니다. <userinput>help</userinput>를 입력하면 쉘 명령어에 대한 도움말을 표시합니다. 명령어 뒤에 <userinput>--help</userinput> 옵션을 붙이면 짤막한 명령어 사용법을 표시합니다. 명령어의 결과가 화면 위로 지나가 버린다면 <userinput>|&nbsp;more</userinput>를 명령 뒤에 붙이면 화면 위로 스크롤되 지나가기 전에 출력을 일시 정지할 수 있습니다. 어떤 글자로 시작하는 명령어의 목록을 보려면 그 글자를 입력하고 탭을 두번 누릅니다."
