<!-- original version: 39529 -->

<para>

Als u weinig ervaring heeft met het indelen van schijven of gewoon meer
gedetailleerde informatie wenst, kunt u <xref linkend="partitioning"/>
raadplegen.

</para><para>

Allereerst zal u de gelegenheid worden geboden om een gehele harde schijf,
of (indien aanwezig) de beschikbare vrije ruimte op een schijf, automatisch
in te delen.
Dit wordt <quote>begeleide schijfindeling</quote> genoemd. Als u dit niet
wenst, kies dan de optie <guimenuitem>Schijfindeling handmatig
bepalen</guimenuitem> uit het menu.

</para>

   <sect3 id="partman-auto">
   <title>Begeleide schijfindeling</title>
<para>

Als u kiest voor begeleide schijfindeling, heeft u mogelijk twee opties:
de partities direct op de harde schijf aanmaken (de klassieke methode) of
gebruik maken van logisch volumebeheer (LVM). In het tweede geval zal het
installatiesysteem de meeste partities aanmaken binnen één grote overkoepelende
partitie; het voordeel van deze methode is dat de grootte van de partities
binnen die overkoepelende partitie relatief eenvoudig kan worden gewijzigd.
Merk op dat de optie om gebruik te maken van LVM niet op alle platforms
beschikbaar hoeft te zijn.

</para>
<warning><para>

Als u kiest voor begeleide schijfindeling met gebruik van LVM, zal het
niet mogelijk zijn om wijzigingen in de partitietabel ongedaan te maken.
Met deze keuze worden effectief alle gegevens verwijderd die zich nu op
de harde schijf bevinden

</para></warning>
<para>

Nadat u heeft gekozen voor begeleide schijfindeling (klassieke methode
danwel met gebruik van LVM), kunt u een keuze maken uit de schema's
weergegeven in onderstaande tabel. Elk van deze schema's
heeft zijn voor- en nadelen, waarvan een aantal wordt besproken in
<xref linkend="partitioning"/>. Als u twijfelt, kies dan de eerste optie.
Merk op dat begeleide schijfindeling een zekere minimale vrije schijfruimte
nodig heeft om te kunnen werken. Als u niet tenminste ongeveer 1GB ruimte
(afhankelijk van het gekozen schema) beschikbaar heeft, zal de begeleide
schijfindeling mislukken.

</para>

<informaltable>
<tgroup cols="3">
<thead>
<row>
  <entry>Schema voor schijfindeling</entry>
  <entry>Minimale ruimte</entry>
  <entry>Aangemaakte partities</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Alles in één partitie</entry>
  <entry>600MB</entry>
  <entry><filename>/</filename>, swap</entry>
</row><row>
  <entry>Afzonderlijke /home partitie</entry>
  <entry>500MB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>, swap
  </entry>
</row><row>
  <entry>Afzonderlijke /home, /usr, /var en /tmp partities</entry>
  <entry>1GB</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>,
    <filename>/usr</filename>, <filename>/var</filename>,
    <filename>/tmp</filename>, swap
  </entry>
</row>

</tbody></tgroup></informaltable>

<para>

Als u kiest voor begeleide schijfindeling met gebruik van LVM, dan zal het
installatiesysteem ook een afzonderlijke <filename>/boot</filename> partitie
aanmaken. Alle andere partities, met uitzondering van de wisselgeheugen (swap)
partitie, zullen worden aangemaakt in de LVM partitie.

</para><para arch="ia64">

Als u voor uw IA64 systeem kiest voor begeleide schijfindeling, zal een
extra partitie, geformatteerd met een FAT16 opstartbaar bestandssysteem,
worden aangemaakt ten behoeve van de EFI-opstartlader.
Er is ook een aanvullende menuoptie in het keuzemenu met formateeropties
om handmatig een EFI-opstartpartitie te kunnen aanmaken.

</para><para arch="alpha">

Als u voor uw Alpha systeem kiest voor begeleide schijfindeling, zal een
extra, niet-geformatteerde partitie worden aangemaakt aan het begin van
uw harde schijf om deze schijfruimte te reserveren voor de opstartlader
<command>aboot</command>.

</para><para>

Nadat u een schema heeft geselecteerd, zal op het volgende scherm de
nieuwe partitie-indeling worden getoond met daarbij indicaties of en
hoe deze geformatteerd zullen worden en op welke aanhechtpunten ze
gekoppeld zullen worden.

</para><para>

De partitie-indeling zou er als volgt uit kunnen zien:

<informalexample><screen>
  IDE1 master (hda) - 6.4 GB WDC AC36400L
        #1 primair   16.4 MB  B f ext2       /boot
        #2 primair  551.0 MB      swap       swap
        #3 primair    5.8 GB      ntfs
           pri/log    8.2 MB      VRIJE RUIMTE

  IDE1 slave (hdb) - 80.0 GB ST380021A
        #1 primair   15.9 MB      ext3
        #2 primair  996.0 MB      fat16
        #3 primair    3.9 GB      xfs        /home
        #5 logisch    6.0 GB    f ext3       /
        #6 logisch    1.0 GB    f ext3       /var
        #7 logisch  498.8 MB      ext3
        #8 logisch  551.5 MB      swap       swap
        #9 logisch   65.8 GB      ext2
</screen></informalexample>

Dit voorbeeld toont twee IDE harde schijven die zijn opgedeeld in
verschillende partities; de eerste schijf heeft nog vrije ruimte. Elke
regel voor een partitie bevat de volgende informatie: partitienummer,
type en grootte van de partitie, wijzigingindicatoren, het bestandssysteem
en het aanhechtpunt (indien aanwezig) voor de partitie.
Merk op dat de hier weergegeven indeling niet kan worden gerealiseerd met
behulp van begeleide schijfindeling; zij toont echter wel de flexibiliteit
die kan worden bereikt met handmatige schijfindeling.
<!-- FJP: De wijzigingindicatoren zijn de smileys die aangeven wat er
          met een partitie gat gebeuren. Dit moet nog worden uitgewerkt.-->

</para><para>

Hiermee is de begeleide schijfindeling voltooid. Als u tevreden bent met de
voorgestelde indeling, kunt u de menuoptie <guimenuitem>Schijfindeling
afsluiten &amp; veranderingen naar schijf schrijven</guimenuitem> kiezen om
de nieuwe partitie-indeling te implementeren (zoals beschreven aan het einde
van deze paragraaf). Als u nog niet tevreden bent, kunt u kiezen voor
<guimenuitem>Veranderingen aan partities ongedaan maken</guimenuitem> en de
begeleide schijfindeling nogmaals uit te voeren of om de voorgestelde indeling
te wijzigen zoals hieronder beschreven voor handmatige schijfindeling.

</para>
   </sect3>

   <sect3 id="partman-manual">
   <title>Handmatige schijfindeling</title>
<para>

Als u kiest voor handmatige schijfindeling, zal een vergelijkbaar scherm
worden getoond als hiervoor weergegeven, maar dan met uw bestaande
partitie-indeling en nog zonder aanhechtpunten. Hoe u de schijfindeling
en het gebruik van partities voor uw nieuwe Debian systeem handmatig kunt
instellen, wordt hieronder behandeld.

</para><para>

Als een een nog maagdelijke schijf (waarop nog geen partities of vrije
ruimte gedefinieerd is) selecteert, zal u de mogelijkheid krijgen om een
nieuwe partitietabel te creëren (deze is nodig om partities te kunnen
aanmaken). Daarna behoort onder de geselecteerde schijf een nieuwe regel
met <quote>VRIJE RUIMTE</quote> te verschijnen.

</para><para>

Als u een regel met vrije ruimte selecteert, kunt u een nieuwe partitie
creëren. U zult een korte serie vragen over grootte, type (primair of
logisch) en positie (aan het begin of einde van de vrije ruimte) moeten
beantwoorden. Daarna krijgt u een gedetailleerd overzicht van de nieuwe
partitie. Daarin vindt u als aanhechtpunt, aankoppelopties, de indicatie of
de partitie opstartbaar moet zijn, en typisch gebruik. Als de
standaardwaarden u niet bevallen, kunt u ze naar behoefte wijzigen. Door
bijvoorbeeld de optie <guimenuitem>Gebruiken als:</guimenuitem> te selecteren,
kunt u een ander bestandssysteem voor de partitie selecteren, inclusief de
mogelijkheid om de partitie te gebruiken voor wisselgeheugen ('swap'), als
onderdeel van software RAID of LVM, of om de partitie helemaal niet te
gebruiken. Een andere aardige optie is de mogelijkheid om gegevens vanaf
een andere partitie naar de nieuwe partitie te kopiëren.
Als u tevreden bent met de nieuwe partitie kiest u <guimenuitem>Klaar met
instellen van partitie</guimenuitem> waarna u terugkeert naar het hoofdmenu
van <command>partman</command>.

</para><para>

Als u een instelling van een partitie wilt wijzigen, selecteert u
deze gewoon waarna u in het configuratiemenu voor de partitie komt.
Dit is hetzelfde scherm als bij het aanmaken van een nieuwe partitie en
u kunt dus dezelfde serie instellingen wijzigen. Iets dat mogelijk niet
meteen duidelijk is, is dat u de grootte van een partitie kunt wijzigen
door de regel met de grootte van de partitie te selecteren.
Bestandssystemen waarvan bekend is dat dit werkt zijn in ieder geval
fat16, fat32, ext2, ext3 en swap. Dit menu stelt u ook in staat om een
partitie te verwijderen.

</para><para>

Zorg ervoor dat u tenminste twee partities aanmaakt: één voor het
<emphasis>root</emphasis>-bestandssysteem (dat gekoppeld moet worden
aan het aanhechtpunt <filename>/</filename>) en één voor
<emphasis>swap</emphasis>. Als u vergeet om het root-bestandssysteem
aan te koppelen, zal <command>partman</command> u niet verder laten gaan
tot dit probleem is verholpen.

</para><para arch="ia64">

Als u vergeet om een EFI-opstartpartitie te selecteren en formatteren, zal
<command>partman</command> dit signaleren en u beletten verder te gaan tot
u er een heeft toegewezen.

</para><para>

De functionaliteit van <command>partman</command> kan worden vergroot
met behulp van installatiesysteemmodules, maar is afhankelijk van het
platform waartoe uw systeem behoort. Als u dus niet alle beschreven
mogelijkheden ziet, controleer dan of alle vereiste modules (zoals
<filename>partman-ext3</filename>, <filename>partman-xfs</filename>,
of <filename>partman-lvm</filename>) zijn geladen.

</para><para>

Als u tevreden bent met het eindresultaat van de schijfindeling, kiest u
de menuoptie <guimenuitem>Schijfindeling afsluiten &amp; veranderingen naar
schijf schrijven</guimenuitem>. Daarna zal een overzicht worden getoond van
de wijzigingen in de schijfindeling en zal u worden gevraagd om deze te
bevestigen voordat de gewenste wijzigingen worden doorgevoerd.

</para>
   </sect3>
