<!-- retain these comments for translator revision tracking -->
<!-- original version: 56442 untranslated -->

 <sect1 condition="bootable-usb" id="boot-usb-files">
 <title>Preparing Files for USB Memory Stick Booting</title>

<para>

There are two installation methods possible when booting from USB stick.
The first is to install completely from the network. The second is to
also copy a CD image onto the USB stick and use that as a source for
packages, possibly in combination with a mirror. This second method is
the more common.

</para><para>

For the first installation method you'll need to download an installer
image from the <filename>netboot</filename> directory (at the location
mentioned in <xref linkend="where-files"/>) and use the
<quote>flexible way</quote> explained below to copy the files to the USB
stick.

</para><para>

Installation images for the second installation method can be found in
the <filename>hd-media</filename> directory and either the <quote>easy
way</quote> or the <quote>flexible way</quote> can be used to copy the
image to the USB stick. For this installation method you will also need
to download a CD image. The installation image and the CD image must be
based on the same release of &d-i;. If they do not match you are likely
to get errors<footnote>

<para>
The error message that is most likely to be displayed is that no kernel
modules can be found. This means that the version of the kernel module
udebs included on the CD image is different from the version of the
running kernel.
</para>

</footnote> during the installation.

</para><para>

To prepare the USB stick, you will need a system where GNU/Linux is
already running and where USB is supported. With current GNU/Linux systems
the USB stick should be automatically recognized when you insert it. If
it is not you should check that the usb-storage kernel module is loaded.
When the USB stick is inserted, it will be mapped to a device named
<filename>/dev/sdX</filename>, where the <quote>X</quote> is a letter
in the range a-z. You should be able to see to which device the USB
stick was mapped by running the command <command>dmesg</command> after
inserting it. To write to your stick, you may have to turn off its write
protection switch.

</para>
<warning><para>

The procedures described in this section will destroy anything already
on the device! Make very sure that you use the correct device name for
your USB stick. If you use the wrong device the result could be that all
information on for example a hard disk could be lost.

</para></warning>
<para>

Note that the USB stick should be at least 256 MB in size (smaller
setups are possible if you follow <xref linkend="usb-copy-flexible"/>).

</para>

  <sect2 id="usb-copy-easy">
  <title>Copying the files &mdash; the easy way</title>
<para>

There is an all-in-one file <filename>hd-media/boot.img.gz</filename>
which contains all the installer files (including the kernel)
<phrase arch="x86">as well as <classname>syslinux</classname> and its
configuration file.</phrase>
<phrase arch="powerpc">as well as <classname>yaboot</classname> and its
configuration file.</phrase>

</para><para>

Note that, although convenient, this method does have one major
disadvantage: the logical size of the device will be limited to 256 MB,
even if the capacity of the USB stick is larger. You will need to
repartition the USB stick and create new file systems to get its full
capacity back if you ever want to use it for some different purpose.
A second disadvantage is that you cannot copy a full CD image onto
the USB stick, but only the smaller businesscard or netinst CD images.

</para><para arch="x86">

To use this image you only have to extract it directly to your USB stick:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sdX</replaceable>
</screen></informalexample>

</para><para arch="powerpc">

Create a partition of type "Apple_Bootstrap" on your USB stick using
<command>mac-fdisk</command>'s <userinput>C</userinput> command and
extract the image directly to that:

<informalexample><screen>
# zcat boot.img.gz &gt; /dev/<replaceable>sdX2</replaceable>
</screen></informalexample>

</para><para>

After that, mount the USB memory stick
<phrase arch="x86">(<userinput>mount
/dev/<replaceable>sdX</replaceable> /mnt</userinput>),</phrase>
<phrase arch="powerpc">(<userinput>mount
/dev/<replaceable>sdX2</replaceable> /mnt</userinput>),</phrase>
which will now have
<phrase arch="x86">a FAT filesystem</phrase>
<phrase arch="powerpc">an HFS filesystem</phrase>
on it, and copy a Debian netinst or businesscard ISO image to it.
Unmount the stick (<userinput>umount /mnt</userinput>) and you are done.

</para>
  </sect2>

  <sect2 id="usb-copy-flexible">
  <title>Copying the files &mdash; the flexible way</title>
<para>

If you like more flexibility or just want to know what's going on, you
should use the following method to put the files on your stick. One
advantage of using this method is that &mdash; if the capacity of your
USB stick is large enough &mdash; you have the option of copying a
full CD ISO image to it.

</para>

&usb-setup-x86.xml;
&usb-setup-powerpc.xml;

  </sect2>

  <!-- TODO: doesn't this section belong later? -->
  <sect2 arch="x86">
  <title>Booting the USB stick</title>
<warning><para>

If your system refuses to boot from the memory stick, the stick may
contain an invalid master boot record (MBR). To fix this, use the
<command>install-mbr</command> command from the package
<classname>mbr</classname>:

<informalexample><screen>
# install-mbr /dev/<replaceable>sdX</replaceable>
</screen></informalexample>

</para></warning>
  </sect2>
 </sect1>
