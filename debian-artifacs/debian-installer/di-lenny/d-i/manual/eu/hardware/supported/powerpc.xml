<!-- retain these comments for translator revision tracking -->
<!-- original version: 11648 untranslated -->


  <sect2 arch="powerpc"><title>CPU, Main Boards, and Video Support</title>
<para>

There are four major supported <emphasis>&architecture;</emphasis>
flavors: PMac (Power-Macintosh) PReP, Apus, and CHRP machines.  Ports
to other <emphasis>&architecture;</emphasis> architectures, such as
the Be-Box and MBX architecture, are underway but not yet supported by
Debian. We may have a 64bit port (Power3) in the future.

</para><para>

Apple (and briefly a few other manufacturers - Power Computing, for
example) makes a series of Macintosh computers based on the PowerPC
processor. For purposes of architecture support, they are categorized
as NuBus, OldWorld PCI, and NewWorld.

</para><para>

Macintosh computers using the 680x0 series of processors are not in
the PowerPC family but are instead m68k machines. Those models start 
with `Mac II' or have a 3-digit model number such as Centris 650
or Quadra 950. Apple's pre-iMac PowerPC model numbers have four digits.

</para><para>

NuBus systems are not currently supported by debian/powerpc. The
monolithic Linux/PPC kernel architecture does not have support for
these machines; instead, one must use the MkLinux Mach microkernel,
which Debian does not yet support. These include the following:

<itemizedlist>
<listitem><para>

Power Macintosh 6100, 7100, 8100

</para></listitem>
<listitem><para>

Performa 5200, 6200, 6300

</para></listitem>
<listitem><para>

Powerbook 1400, 2300, and 5300

</para></listitem>
<listitem><para>

Workgroup Server 6150, 8150, 9150

</para></listitem>
</itemizedlist>

A linux kernel for these machines and limited support is available at 
<ulink url="http://nubus-pmac.sourceforge.net/"></ulink>

</para><para>

OldWorld systems are most Power Macintoshes with a floppy drive and a
PCI bus. Most 603, 603e, 604, and 604e based Power Macintoshes are
OldWorld machines. The beige colored G3 systems are also OldWorld.

</para><para>

The so called NewWorld PowerMacs are any PowerMacs in translucent
colored plastic cases. That includes all iMacs, iBooks, G4 systems,
blue colored G3 systems, and most PowerBooks manufactured in and after
1999. The NewWorld PowerMacs are also known for using the `ROM in RAM'
system for MacOS, and were manufactured from mid-1998 onwards.

</para><para>

Recently introduced Macintosh systems have hardware which is more well
supported by the 2.4 Linux kernel. For some, the 2.2 kernel just
doesn't work. The new-powermac flavor, which uses the 2.4 kernel, has
been added to keep up with the hardware. The new-powermac flavor may
also be installed on other OldWorld and NewWorld machines. Machines
for which new-powermac is highly recommended are flagged with an
asterisk below.

</para><para>

Specifications for Apple hardware are available at 
<ulink url="http://www.info.apple.com/support/applespec.html">AppleSpec</ulink>,
and, for older hardware, 
<ulink url="http://www.info.apple.com/support/applespec.legacy/index.html">AppleSpec Legacy</ulink>.

</para><para>

<informaltable>
<tgroup cols="3">
<colspec colname="c1"/>
<colspec colname="c2"/>
<colspec colname="c3"/>
<thead>
<row>
  <entry namest="c1" nameend="c2">Model Name/Number</entry>
  <entry>Architecture</entry>
</row>
</thead>

<tbody>
<row>
  <entry morerows="22">Apple</entry>
  <entry>iMac Bondi Blue, 5 Flavors, Slot Loading</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>iMac Summer 2000, Early 2001</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* iBook, iBook SE, iBook Dual USB</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* iBook2</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>Power Macintosh Blue and White (B&amp;W) G3</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* Power Macintosh G4 PCI, AGP, Cube</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* Power Macintosh G4 Gigabit Ethernet</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* Power Macintosh G4 Digital Audio, Quicksilver</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>PowerBook G3 FireWire Pismo (2000)</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>PowerBook G3 Lombard (1999)</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>* PowerBook G4 Titanium</entry>
  <entry>powermac-NewWorld</entry>
</row><row>
  <entry>Performa 4400, 54xx, 5500</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Performa 6360, 6400, 6500</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh 4400, 5400</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh 7200, 7300, 7500, 7600</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh 8200, 8500, 8600</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh 9500, 9600</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh (Beige) G3 Minitower</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Power Macintosh (Beige) Desktop, All-in-One</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>PowerBook 2400, 3400, 3500</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>PowerBook G3 Wallstreet (1998)</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Twentieth Anniversary Macintosh</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Workgroup Server 7250, 7350, 8550, 9650, G3</entry>
  <entry>powermac-OldWorld</entry>
</row>

<row>
  <entry morerows="1">Power Computing</entry>
  <entry>PowerBase, PowerTower / Pro, PowerWave</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>PowerCenter / Pro, PowerCurve</entry>
  <entry>powermac-OldWorld</entry>
</row>

<row>
  <entry>UMAX</entry>
  <entry>C500, C600, J700, S900</entry>
  <entry>powermac-OldWorld</entry>
</row>

<row>
  <entry>APS</entry>
  <entry>APS Tech M*Power 604e/2000</entry>
  <entry>powermac-OldWorld</entry>
</row>

<row>
  <entry morerows="5">Motorola</entry>
  <entry>Starmax 3000, 4000, 5000, 5500</entry>
  <entry>powermac-OldWorld</entry>
</row><row>
  <entry>Firepower, PowerStack Series E, PowerStack II</entry>
  <entry>prep</entry>
</row><row>
  <entry>MPC 7xx, 8xx</entry>
  <entry>prep</entry>
</row><row>
  <entry>MTX, MTX+</entry>
  <entry>prep</entry>
</row><row>
  <entry>MVME2300(SC)/24xx/26xx/27xx/36xx/46xx</entry>
  <entry>prep</entry>
</row><row>
  <entry>MCP(N)750</entry>
  <entry>prep</entry>
</row>

<row>
  <entry morerows="4">IBM RS/6000</entry>
  <entry>40P, 43P</entry>
  <entry>prep</entry>
</row><row>
  <entry>Power 830/850/860 (6070, 6050)</entry>
  <entry>prep</entry>
</row><row>
  <entry>6030, 7025, 7043</entry>
  <entry>prep</entry>
</row><row>
  <entry>p640</entry>
  <entry>prep</entry>
</row><row>
  <entry>B50, 43P-150, 44P</entry>
  <entry>chrp</entry>
</row>

<row>
  <entry>Amiga Power-UP Systems (APUS)</entry>
  <entry>A1200, A3000, A4000</entry>
  <entry>apus</entry>
</row>

</tbody></tgroup></informaltable>

</para>
  </sect2>
