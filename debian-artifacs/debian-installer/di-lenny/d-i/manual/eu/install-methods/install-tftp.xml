<!-- retain these comments for translator revision tracking -->
<!-- original version: 16879 untranslated -->

 <sect1 condition="supports-tftp" id="install-tftp">
 <title>Preparing Files for TFTP Net Booting</title>
<para>

If your machine is connected to a local area network, you may be able
to boot it over the network from another machine, using TFTP. If you
intend to boot the installation system from another machine, the
boot files will need to be placed in specific locations on that machine,
and the machine configured to support booting of your specific machine.

</para><para>

You need to set up a TFTP server, and for CATS machines, a BOOTP server
<phrase condition="supports-rarp">, or RARP server</phrase>
<phrase condition="supports-dhcp">, or DHCP server</phrase>.

</para><para>

<phrase condition="supports-rarp">The Reverse Address Resolution Protocol (RARP) is
one way to tell your client what IP address to use for itself. Another
way is to use the BOOTP protocol. </phrase>

<phrase condition="supports-bootp">BOOTP is an IP protocol that
informs a computer of its IP address and where on the network to obtain
a boot image. </phrase>

<phrase arch="m68k"> Yet another alternative exists on VMEbus
systems: the IP address can be manually configured in boot ROM. </phrase>

<phrase condition="supports-dhcp">The DHCP (Dynamic Host Configuration
Protocol) is a more flexible, backwards-compatible extension of BOOTP.
Some systems can only be configured via DHCP. </phrase>

</para><para arch="powerpc">

For PowerPC, if you have a NewWorld Power Macintosh machine, it is a
good idea to use DHCP instead of BOOTP.  Some of the latest machines
are unable to boot using BOOTP.

</para><para arch="alpha"> 

Unlike the Open Firmware found on Sparc and PowerPC machines, the SRM
console will <emphasis>not</emphasis> use RARP to obtain its IP
address, and therefore you must use BOOTP for net booting your Alpha.
You can also enter the IP configuration for network interfaces
directly in the SRM console.

<footnote>
<para>

Alpha systems can also be net-booted using the DECNet MOP (Maintenance
Operations Protocol), but this is not covered here.  Presumably, your
local OpenVMS operator will be happy to assist you should you have
some burning need to use MOP to boot Linux on your Alpha.

</para>
</footnote></para><para arch="hppa"> 

Some older HPPA machines (e.g. 715/75) use RBOOTD rather than BOOTP.
An RBOOTD package is available on the parisc-linux web site.

</para><para>

The Trivial File Transfer Protocol (TFTP) is used to serve the boot
image to the client.  Theoretically, any server, on any platform,
which implements these protocols, may be used.  In the examples in
this section, we shall provide commands for SunOS 4.x, SunOS 5.x
(a.k.a. Solaris), and GNU/Linux.

</para>

&tftp-rarp.xml;
&tftp-bootp.xml;
&tftp-dhcp.xml;

  <sect2 id="tftpd">
  <title>Enabling the TFTP Server</title>
<para>

To get the TFTP server ready to go, you should first make sure that
<command>tftpd</command> is enabled.  This is usually enabled by having
something like the following line in <filename>/etc/inetd.conf</filename>:

<informalexample><screen>

tftp dgram udp wait nobody /usr/sbin/tcpd in.tftpd /tftpboot

</screen></informalexample>

Debian packages will in general set this up correctly by default when they
are installed.

</para><para>

Look in that file and remember the directory which is used as the
argument of <command>in.tftpd</command>; you'll need that below.  The
<userinput>-l</userinput> argument enables some versions of
<command>in.tftpd</command> to log all requests to the system logs;
this is useful for diagnosing boot errors.  If you've had to change
<filename>/etc/inetd.conf</filename>, you'll have to notify the
running <command>inetd</command> process that the file has changed.
On a Debian machine, run <userinput>/etc/init.d/inetd
reload</userinput>; on other machines,
find out the process ID for <command>inetd</command>, and run
<userinput>kill -HUP <replaceable>inetd-pid</replaceable></userinput>.

</para><note arch="x86"><para>

To use the Pre-boot Execution Environment (PXE) method of TFTP
booting, you will need a TFTP server with <userinput>tsize</userinput>
support.  On a &debian; server, the <classname>atftpd</classname> and
<classname>tftpd-hpa</classname> packages qualify.

</para></note><para arch="mips">

If your TFTP server is a GNU/Linux box running Linux 2.4.X you'll need
to set the following on your server:

<informalexample><screen>

echo 1 > /proc/sys/net/ipv4/ip_no_pmtu_disc

</screen></informalexample>

to turn off Path MTU discovery, otherwise the Indy's PROM can't
download the kernel. Furthermore, make sure TFTP packets are sent from
a source port no greater than 32767, or the download will stall after
the first packet.  Again, it's Linux 2.4.X tripping this bug in the
PROM, and you can avoid it by setting

<informalexample><screen>

echo "2048 32767" > /proc/sys/net/ipv4/ip_local_port_range

</screen></informalexample>

to adjust the range of source ports the Linux TFTP server uses.

</para>
  </sect2>

  <sect2 id="tftp-images">
  <title>Move TFTP Images Into Place</title>
<para>

Next, place the TFTP boot image you need, as found in 
<xref linkend="where-files"/>, in the <command>tftpd</command> 
boot image directory.  Generally, this directory will be
<filename>/tftpboot</filename>.  You'll have to make a link from that
file to the file which <command>tftpd</command> will use for booting a
particular client.  Unfortunately, the file name is determined by the
TFTP client, and there are no strong standards.

</para><para arch="powerpc">

On NewWorld Power Macintosh machines, you will need to set up the
<command>yaboot</command> boot loader as the TFTP boot image.
<command>Yaboot</command> will then retrieve the kernel and RAMdisk
images via TFTP itself.  For net booting, use the
<filename>yaboot-netboot.conf</filename>.  Just rename this to
<filename>yaboot.conf</filename> in the TFTP directory.

</para><para arch="not-powerpc">

Often, the file that the TFTP client will look for is
<replaceable>client-ip-in-hexclient-architecture</replaceable>.  To
compute <replaceable>client-ip-in-hex</replaceable>, take each byte of
the client IP address and translate it into hexadecimal notation.  If
you have a machine handy with the <command>bc</command> program, you
can use the program.  First issue the <userinput>obase=16</userinput>
command to set the output to hex, then enter the individual components
of the client IP one at a time.  As for
<replaceable>client-architecture</replaceable>, try out some values.

</para><para arch="x86">

For PXE booting, you can use the boot loader included with
<classname>syslinux</classname>: <filename>pxelinux.0</filename>. The boot
loader should be copied into the <filename>/tftpboot</filename>
folder. Then create a subdirectory within
<filename>/tftpboot</filename> named
<filename>/tftpboot/pxelinux.cfg</filename>, and within that directory
create a text file <filename>default</filename>. Here is an example of
a <filename>default</filename> file's contents:

<informalexample><screen>

default lanlinux
prompt 1

label lanlinux
  kernel vmlinuz
  append initrd=initrd.gz devfs=mount root=/dev/ram

</screen></informalexample>

<userinput>devfs=mount</userinput> is important, because without it
there may be problems mounting the initrd.gz file system once the
kernel is booted.

</para><para arch="x86">

Finally, copy the <filename>vmlinuz</filename> and
<filename>initrd.gz</filename> files (for the netboot installation method)
from the Debian ftp archive into the <filename>/tftpboot</filename> folder,
where the bootloader will be looking for them.

</para>


   <sect3 arch="mipsel">
   <title>DECstation TFTP Images</title>
<para>

For DECstations, there are tftpimage files for each subarchitecture,
which contain both kernel and installer in one file. The naming
convention is tftpimage-<replaceable>subarchitecture</replaceable> or
tftpimage-<replaceable>subarchitecture</replaceable>.gz.  If the
tftpimage is gzipped (the name ends with .gz), you need to unpack it
first with <userinput>gunzip
tftpimage-<replaceable>subarchitecture</replaceable>.gz</userinput>,
as DECstations cannot boot compressed files by TFTP. Copy the
tftpimage file you would like to use to
<userinput>/tftpboot/tftpboot.img</userinput> if you work with the
example BOOTP/DHCP setups described above.

</para><para>

The DECstation firmware boots by TFTP with the command <userinput>boot
<replaceable>#</replaceable>/tftp</userinput>, where
<replaceable>#</replaceable> is the number of the TurboChannel device
from which to boot. On most DECstations this is "3".  If the
BOOTP/DHCP server does not supply the filename or you need to pass
additional parameters, they can optionally be appended with the
following syntax:

</para><para>

<userinput>boot #/tftp/filename param1=value1 param2=value2 ...</userinput>

</para><para>

Several DECstation firmware revisions show a problem with regard to
net booting: the transfer starts, but after some time it stops with
an <computeroutput>a.out err</computeroutput>. This can have several reasons:

<orderedlist>
<listitem><para>

The firmware does not respond to ARP requests during a TFTP
transfer. This leads to an ARP timeout and the transfer stops.  The
solution is to add the MAC address of the Ethernet card in the
DECstation statically to the ARP table of the TFTP server.  This is
done by running <userinput>arp -s
<replaceable>IP-address</replaceable>
<replaceable>MAC-address</replaceable></userinput> as root on the
machine acting as TFTP server. The MAC-address of the DECstation can
be read out by entering <command>cnfg</command> at the DECstation
firmware prompt.

</para></listitem>
<listitem><para>

The firmware has a size limit on the files that can be booted
by TFTP.

</para></listitem>
</orderedlist>

There are also firmware revisions that cannot boot via TFTP at all. An
overview about the different firmware revisions can be found at the
NetBSD web pages:
<ulink url="http://www.netbsd.org/Ports/pmax/board-list.html#proms"></ulink>.

</para>
   </sect3>

   <sect3 arch="alpha">
   <title>Alpha TFTP Booting</title>
<para>
On Alpha, you must specify the filename (as a relative path to the
boot image directory) using the <userinput>-file</userinput> argument
to the SRM <userinput>boot</userinput> command, or by setting the
<userinput>BOOT_FILE</userinput> environment variable.  Alternatively,
the filename can be given via BOOTP (in ISC <command>dhcpd</command>,
use the <userinput>filename</userinput> directive).  Unlike Open
Firmware, there is <emphasis>no default filename</emphasis> on SRM, so
you <emphasis>must</emphasis> specify a filename by either one of
these methods.

</para>
   </sect3>

   <sect3 arch="sparc">
   <title>SPARC TFTP Booting</title>
<para>

SPARC architectures for instance use the subarchitecture names, such
as ``SUN4M'' or ``SUN4C''; in some cases, the architecture is left
blank, so the file the client looks for is just
<filename>client-ip-in-hex</filename>.  Thus, if your system
subarchitecture is a SUN4C, and its IP is 192.168.1.3, the filename
would be <filename>C0A80103.SUN4C</filename>.

</para><para>

You can also force some sparc systems to look for a specific file name
by adding it to the end of the OpenPROM boot command, such as
<userinput>boot net my-sparc.image</userinput>. This must still reside
in the directory that the TFTP server looks in.

</para>
   </sect3>

   <sect3 arch="m68k">
   <title>BVM/Motorola TFTP Booting</title>
<para>

For BVM and Motorola VMEbus systems copy the files
&bvme6000-tftp-files; to <filename>/tftpboot/</filename>.

</para><para>

Next, configure your boot ROMs or BOOTP server to initially load the
<filename>tftplilo.bvme</filename> or
<filename>tftplilo.mvme</filename> files from the TFTP server.  Refer
to the <filename>tftplilo.txt</filename> file for your subarchitecture
for additional system-specific configuration information.

</para>
   </sect3>

   <sect3 arch="mips">
   <title>SGI Indys TFTP Booting</title>
<para>

On SGI Indys you can rely on the <command>bootpd</command> to supply
the name of the TFTP file. It is given either as the
<userinput>bf=</userinput> in <filename>/etc/bootptab</filename> or as
the <userinput>filename=</userinput> option in
<filename>/etc/dhcpd.conf</filename>.

</para>
   </sect3>
  </sect2>

  <sect2 id="tftp-low-memory">
  <title>TFTP Installation for Low-Memory Systems</title>
<para>

On some systems, the standard installation RAMdisk, combined with the
memory requirements of the TFTP boot image, cannot fit in memory.  In
this case, you can still install using TFTP, you'll just have to go
through the additional step of NFS mounting your root directory over
the network as well.  This type of setup is also appropriate for
diskless or dataless clients.

</para><para>

First, follow all the steps above in <xref linkend="install-tftp"/>.

<orderedlist>
<listitem><para>

Copy the Linux kernel image on your TFTP server using the
<userinput>a.out</userinput> image for the architecture you are
booting.

</para></listitem>
<listitem><para>

Untar the root archive on your NFS server (can be the same system as
your TFTP server): 

<informalexample><screen>

# cd /tftpboot
# tar xvzf root.tar.gz

</screen></informalexample>

Be sure to use the GNU <command>tar</command> (other tar programs, like the
SunOS one, badly handle devices as plain files).

</para></listitem>
<listitem><para>

Export your <filename>/tftpboot/debian-sparc-root</filename> directory
with root access to your client.  E.g., add the following line to
<filename>/etc/exports</filename> (GNU/Linux syntax, should be similar
for SunOS):

<informalexample><screen>

/tftpboot/debian-sparc-root client(rw,no_root_squash)

</screen></informalexample>

NOTE: "client" is the host name or IP address recognized by the server for
the system you are booting.

</para></listitem>
<listitem><para>

Create a symbolic link from your client IP address in dotted notation
to <filename>debian-sparc-root</filename> in the
<filename>/tftpboot</filename> directory.  For example, if the client
IP address is 192.168.1.3, do

<informalexample><screen>

# ln -s debian-sparc-root 192.168.1.3

</screen></informalexample>

</para></listitem>
</orderedlist>

</para>

  </sect2>

  <sect2 condition="supports-nfsroot">
  <title>Installing with TFTP and NFS Root</title>
<para>

Installing with TFTP and NFS Root is similar to 
<xref linkend="tftp-low-memory"/> because you don't want to 
load the RAMdisk anymore but boot from the newly created NFS-root file
system.  You then need to replace the symlink to the tftpboot image by
a symlink to the kernel image (for example,
<filename>linux-a.out</filename>).

</para><para>

RARP/TFTP requires all daemons to be running on the same server (the
workstation is sending a TFTP request back to the server that replied
to its previous RARP request).

</para>

  </sect2>
 </sect1>
