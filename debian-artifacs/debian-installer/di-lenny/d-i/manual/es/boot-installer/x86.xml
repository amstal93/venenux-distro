<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56419 -->
<!-- actualizado por rudy, 5 diciembre 2004 -->
<!-- revisado Francisco Garc�a <franciscomanuel.garcia@hispalinux.es>, 27 enero 2005 -->
<!-- revisado rudy, 24 feb. 2005 -->
<!-- revisado igortamara, 17 nov. 2008 -->

  <sect2 arch="x86"><title>Arranque desde un CD-ROM</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!--
El CD #1 del juego de CD-ROM oficial de Debian para &arch-title;
presentar� un cursor <prompt>boot:</prompt> en la mayor�a del hardware.
Pulse <keycap>F4</keycap> para ver la lista de opciones de n�cleo
disponibles desde las que puede arrancar. Simplemente escriba el nombre
de la <quote>variante</quote> (idepci, vanilla, compact, bf24) en el cursor
<prompt>boot:</prompt> seguido de &enterkey;.

</para><para>

Si su hardware no puede arrancar desde im�genes m�ltiples,
ponga uno de los otros CDs en la unidad. Aparentemente la mayor�a
de unidades de CD-ROM SCSI no soportan el arranque desde
m�ltiples im�genes de <command>isolinux</command>, por lo que los
usuarios con CD-ROMs SCSI deber�n intentarlo con el CD2 (vanilla),
CD3 (compact) o CD5 (bf2.4).

</para><para>

Cada uno de los CDs del 2 al 5 arrancar� una <quote>variante</quote> diferente dependiendo
en que tipo de CD-ROM est� insertado. Vea <xref linkend="kernel-choice"/>
para informaci�n sobre las distintas <quote>variantes</quote>. Esta es la disposici�n
de las variantes en los diferentes CD-ROMs:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

Permite una selecci�n de las im�genes de n�cleo desde el cual arrancar
(la variante <quote>idepci</quote> es la predeterminada, si no elige nada).

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Arranca la variante <quote>vanilla</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Arranca la variante <quote>compact</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Arranca la variante <quote>idepci</quote>.

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Arranca la variante <quote>bf2.4</quote>.

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

  </sect2>
  <sect2 arch="x86" id="boot-win32">
  <title>Arranque desde Windows</title>
<para>

Para lanzar el instalador desde Windows, debe obtener primero un medio de
instalaci�n en CD-ROM/DVD-ROM o una memoria USB configurada como se describe
en <xref linkend="official-cdrom"/> y <xref linkend="boot-usb-files"/>.

</para><para>

Si usa un CD o DVD de instalaci�n, se lanzar� un programa de pre-instalaci�n
autom�ticamente cuando inserta el disco.
En caso de que Windows no lo ejecute autom�ticamente, o si est� usando una
memoria USB, puede ejecutarlo manualmente accediendo al dispositivo y
ejecutando <command>setup.exe</command>.

</para><para>

Despu�s de haber iniciado el programa, se le har� una serie preliminar
de preguntas y el sistema estar� preparado para comenzar el instalador de
&debian;.

</para>
  </sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
     until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Arranque desde una partici�n de DOS</title>

&boot-installer-intro-hd.xml;

<para>

Arranque DOS (no Windows) sin ning�n controlador cargado. Para hacer
esto, presione <keycap>F8</keycap> en el momento exacto (y opcionalmente
elija la opci�n <quote>safe mode command prompt only</quote>). Ingrese en el subdirectorio
para la variante que elija, p. ej.:

<informalexample><screen>
cd c:\install
</screen></informalexample>.

Seguidamente, ejecute <command>install.bat</command>.
Se cargar� el n�cleo y despu�s el sistema de instalaci�n.

</para><para>

Por favor, tenga en cuenta que existe un problema con loadlin (#142421) que
impide que <filename>install.bat</filename> pueda usarse con
la variante bf2.4. El s�ntoma de este problema es el mensaje de error
<computeroutput>invalid compressed format</computeroutput>.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <title>Arranque desde Linux usando <command>LILO</command> o
  <command>GRUB</command></title>

<para>
Para arrancar el instalador desde el disco duro, primero deber�
descargar los ficheros necesarios como se describe en
<xref linkend="boot-drive-files"/>.
</para>

<para>
Si intenta usar el disco duro solamente para arrancar y descargar
todo a trav�s de la red, deber� descargar el fichero
<filename>netboot/debian-installer/&architecture;/initrd.gz</filename> y su
n�cleo correspondiente <filename>netboot/debian-installer/&architecture;/linux</filename>.
Esto le permite reparticionar el disco duro desde donde arranc� el
instalador, aunque debe hacerlo con cuidado.
</para>

<para>
Alternativamente, si desea mantener una partici�n existente en el
disco duro sin modificarla durante la instalaci�n, debe descargar el
fichero <filename>hd-media/initrd.gz</filename> y su n�cleo correspondiente,
as� como copiar una ISO de CD en el disco duro (aseg�rese de que el nombre
del fichero termine en <literal>.iso</literal>). Entonces el instalador puede arrancar
desde el disco duro e instalar desde la imagen de CD, sin necesitar la red.
</para>

<para>
Para <command>LILO</command>, deber� configurar dos cosas esenciales en
/etc/lilo.conf:
<itemizedlist>
<listitem><para>

para cargar <filename>initrd.gz</filename> del instalador al momento del
arranque;

</para></listitem>
<listitem><para>

y hacer que el n�cleo <filename>vmlinuz</filename> use este disco RAM como
su partici�n ra�z.

</para></listitem>
</itemizedlist>

Este es un ejemplo de <filename>/etc/lilo.conf</filename>:

</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

Para m�s detalles, vea las p�ginas de manual de
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> y
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>. Ahora ejecute
<userinput>lilo</userinput> y reinicie.

</para><para>

El procedimiento para <command>GRUB</command> es bastante similar. Localice su
<filename>menu.lst</filename> en el directorio <filename>/boot/grub/</filename>
(algunas veces est� en <filename>/boot/boot/grub/</filename>), y a�ada las
siguientes l�neas:

<informalexample><screen>
title  Nueva instalaci�n
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

y reinicie.

</para><para>
Tenga en cuenta que puede tener que ajustar el valor de
<userinput>ramdisksize</userinput>
en funci�n del tama�o de la imagen initrd.
Desde este momento en adelante, no habr� diferencia entre
<command>GRUB</command> o <command>LILO</command>.

</para>
  </sect2>

  <sect2 arch="x86" condition="bootable-usb" id="usb-boot">
  <title>Arranque desde un dispositivo de memoria USB</title>
<para>

Asumimos que ha preparado todo conforme se describe en <xref
linkend="boot-dev-select"/> y <xref linkend="boot-usb-files"/>. Ahora
simplemente conecte su dispositivo de memoria USB en alguno de los
conectores USB libres y reinicie el ordenador. El sistema deber� arrancar,
y deber�a presentar un cursor <prompt>boot:</prompt>. Aqu� puede
ingresar argumentos de arranque adicionales o simplemente presionar
&enterkey;.

</para>
  </sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <title>Arranque desde disquetes</title>
<para>

Deber�a haber descargado previamente las im�genes que necesita y creado los
disquetes desde �stas como se explica en <xref linkend="create-floppy"/>.
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see
<xref linkend="rescue-replace-kernel"/>. -->

</para><para>

Para arrancar el instalador desde el disquete, introd�zcalo en la unidad de
disquete primaria, apague el sistema como lo har�a normalmente, luego enci�ndalo
nuevamente.

</para><para>

Para instalar desde una unidad LS-120 (versi�n ATAPI) con un juego
de disquetes, necesitar� especificar la ubicaci�n virtual de la
unidad de disquete. Puede hacer esto con el argumento de arranque
<emphasis>root=</emphasis>, a�adiendo el dispositivo asignado por el
controlador de disquetes IDE. Por ejemplo, si su unidad LS-120 est�
conectada como el primer dispositivo IDE (maestro) en el segundo
cable, debe ingresar
<userinput>install root=/dev/hdc</userinput> en el cursor de arranque.

</para><para>

Note que en algunas m�quinas, <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Supr</keycap></keycombo> no reinicia
la m�quina apropiadamente, por lo que se recomienda reiniciar en
forma <quote>forzada</quote> o total. Si est� instalando desde un sistema operativo
existente (p. ej. desde un sistema DOS) no tiene opci�n. De otro modo,
por favor reinicie en forma <quote>forzada</quote> o total cuando arranque.

</para><para>

Se acceder� al disquete, deber�a ver una pantalla que presente
el disquete de arranque y finalice mostrando el cursor
<prompt>boot:</prompt>.

</para><para>

Cuando presione &enterkey;, deber�a ver el mensaje
<computeroutput>Loading...</computeroutput>, seguido de
<computeroutput>Uncompressing Linux...</computeroutput>, y
luego una pantalla llena de informaci�n sobre el hardware
de su sistema. Puede encontrar m�s informaci�n sobre esta fase
del proceso de arranque en <xref linkend="kernel-msgs"/>.

</para><para>

Despu�s de arrancar el disquete de arranque, se solicita el
disquete marcado como <quote>root</quote>. Ins�rtelo en la unidad y presione
&enterkey;, los contenidos se cargar�n en memoria. El programa
instalador <command>debian-installer</command> se cargar�
autom�ticamente.

</para>
  </sect2>

  <sect2 arch="x86" id="boot-tftp"><title>Arranque con TFTP</title>

&boot-installer-intro-net.xml;

<para>

Existen varias formas de realizar un arranque con TFTP en i386.

</para>

   <sect3><title>Tarjetas de red o placas base que soportan PXE</title>
<para>

Podr�a ser que su tarjeta de red o placa base provea la
funcionalidad de arranque PXE. Lo que es una reimplementaci�n
de <trademark class="trade">Intel</trademark> del arranque TFTP.
De ser su caso podr�a tener la posibilidad de configurar su BIOS
para arrancar desde la red.

</para>
   </sect3>

   <sect3><title>Tarjeta de red con bootROM</title>
<para>

Podr�a ser que su tarjeta de red (NIC) provea la funcionalidad
de arranque usando TFTP.

</para><para condition="FIXME">

D�jenos (<email>&email-debian-boot-list;</email>) saber como
lo efectu�. Por favor, haga referencia a este documento.

</para>
   </sect3>

   <sect3><title>Etherboot</title>
<para>

El <ulink url="http://www.etherboot.org">proyecto etherboot</ulink>
provee disquetes de arranque e incluso <quote>bootroms</quote> que efect�an un
arranque usando TFTP.

</para>
   </sect3>

  </sect2>

  <sect2 arch="x86" id="boot-screen">
  <title>La pantalla de arranque</title>
<para>

Cuando arranca el instalador, se le presentar� una pantalla gr�fica amigable
con el logo de Debian y un men�:

<informalexample><screen>
Installer boot menu

Install
Graphical install
Advanced options       >
Help

Press ENTER to boot or TAB to edit a menu entry
</screen></informalexample>

De acuerdo al m�todo de instalaci�n que est� usando, la opci�n
<quote>Graphical install</quote> podr�a no estar disponible.

</para><para>

Para una instalaci�n normal, seleccione bien la opci�n
<quote>Install</quote> o <quote>Graphical install</quote>(bien sea
con las flechas del teclado o tecleando la primera letra(resaltada))
y presione &enterkey; para iniciar el instalador.

</para><para>

La opci�n <quote>Advanced options</quote> da acceso a un segundo
men� que permite arrancar el instalador en modos experto, rescate y
para instalaciones automatizadas.

</para><para>

Si desea o requiere a�adir alg�n par�metro al arranque bien sea al
instalador o al n�cleo, presione &tabkey;. Que desplegar� la orden de
arranque predeterminada para la opci�n de men� seleccionada y le permitir�
a�adir opciones adicionales.  Las ventanas de ayuda(ver m�s adelante) muestran
algunas opciones comunes. Presione &enterkey; para arrancar el instalador
con sus opciones; Si presiona &escapekey; volver� al men� de arranque y
deshar� cualquier cambio que usted haya hecho.

</para><para>

Al elegir la opci�n <quote>Help</quote> aparecer� la primera pantalla de 
ayuda, que le ofrecer� un vistazo a las pantallas de ayuda disponibles.
Tenga en cuenta que no es posible volver al men� de arranque despu�s
de que las pantallas de ayuda se han desplegado.  Pero las pantallas de
ayuda F3 y F4 muestran �rdenes equivalentes a los m�todos de arranque
listados en el men�.  Todas las pantallas de ayuda tienen un s�mbolo de
arranque en el que se puede teclear la orden de arranque:

<informalexample><screen>
Press F1 for the help index, or ENTER to boot:
</screen></informalexample>

En este s�mbolo de arranque puede presionar &enterkey; para arrancar el
instalador con las opciones predeterminadas o inclu�r una orden de
arranque espec�fica y opcionalmente par�metros de arranque. En las
pantallas de ayuda puede encontrar una buena cantidad de par�metros de
arranque que podr�an ser �tiles.  Si usted a�ade cualquier par�metro a la
l�nea �rdenes de arranque, aseg�rese de teclear el m�todo de arranque
primero( el predeterminado es <userinput>install</userinput>) y un
espacio antes del primer par�metro (p.e., 
<userinput>install fb=false</userinput>).

<note><para>

En este punto se asume que el teclado tiene como disposici�n Ingl�s
Americano  predeterminada. Esto significa que si su teclado tiene una
disposici�n distinta(espec�fica de su idioma), los caracteres que aparecen
en la pantalla pueden diferir de los que usted esperar�a que aparecieran
cuando teclea los par�metros. Wikipedia tiene un
<ulink url="&url-us-keymap;">esquema de la disposici�n de un teclado US</ulink>
que puede usar como referencia para encontrar las teclas correctas a usar.

</para></note>
<note><para>

Si est� usando un sistema que tiene la BIOS configurada para usar la consola
serial, es posible que no vea la pantalla gr�fica inicial al arrancar el
instalador; puede incluso no ver el men� de arranque.  Lo mismo puede suceder
si est� instalando el sistema v�a un dispositivo de administraci�n remota que
provee una interfaz de texto a la consola VGA. Algunos ejemplos de estos
dispositivos incluyen la consola de texto de Compaq <quote>integrated Lights
Out</quote> (iLO) y la de HP <quote>Integrated Remote Assistant</quote> (IRA).

</para><para>

Para saltar la pantalla gr�fica de arranque puede presionar sin mirar
&escapekey; para obtener un s�mbolo de sistema de texto o (tambi�n sin mirar) 
presionar <quote>H</quote> seguido de &enterkey; para seleccionar la opci�n
<quote>Help</quote> descrita anteriormente. Despu�s de haber tecleado deber�
poder verlo en el s�mbolo.  Para evitar que el instalador use el framebuffer 
para el resto de la instalaci�n, puede a�adir tambi�n 
<userinput>fb=false</userinput> al s�mbolo de arranque, como se describe en
el texto de ayuda.

</para></note>

</para>

  </sect2>
