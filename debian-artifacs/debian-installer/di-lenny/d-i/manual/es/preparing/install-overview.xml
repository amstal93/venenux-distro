<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- original version: 56322 -->
<!-- revisado por jfs, 8 octubre 2004 -->
 <sect1 id="install-overview">
 <title>Descripci�n del proceso de instalaci�n</title>
<para>

Antes de nada, una nota sobre reinstalaciones: con Debian es muy raro que se
produzca una circunstancia que requiera la reinstalaci�n completa de su
sistema. El caso m�s com�n es un fallo mec�nico en el disco duro de su
sistema.


</para><para>

Muchos sistemas operativos habituales exigen que se realice una instalaci�n
completa cuando se producen fallos cr�ticos o cuando es necesario actualizar
a nuevas versiones del sistema operativo. Incluso aunque no sea necesaria
una instalaci�n completa, a menudo los programas deben reinstalarse para que
funcionen correctamente en el nuevo sistema operativo.

</para><para>

En &debian; es m�s probable, si tiene un problema, que pueda reparar su
sistema operativo en lugar de reemplazarlo. Las actualizaciones nunca
requieren una reinstalaci�n total, siempre podr� actualizar sobre el mismo
sistema. Los programas casi siempre son compatibles con las versiones
sucesivas del sistema operativo. Si una nueva versi�n del programa requiere
soporte adicional de software, el sistema de paquetes de Debian se asegura
que todo el software necesario sea autom�ticamente identificado e instalado.
El hecho es que se ha dedicado mucho esfuerzo para evitar la necesidad de
reinstalar un sistema, consid�relo su �ltima opci�n. El instalador
<emphasis>no</emphasis> est� dise�ado para reinstalar sobre un sistema
existente.

</para><para>

Esta es una peque�a gu�a de los pasos por los que pasar� durante el proceso
de instalaci�n:

</para>

<orderedlist>
<listitem><para>

Realice una copia de seguridad de los datos o documentaci�n existentes en el
disco duro donde planea realizar la instalaci�n.

</para></listitem>
<listitem><para>

Reuna informaci�n sobre su sistema, as� como toda la documentaci�n
que necesite antes de iniciar la instalaci�n.

</para></listitem>
<listitem><para>

Cree un espacio particionable para Debian en su disco duro.

</para></listitem>
<listitem><para>

Localice y/o descargue el programa del instalador as� como los ficheros de
cualquier controlador especializado que su m�quina necesite (excepto para
usuarios de Debian CD).

</para></listitem>
<listitem><para>

Cree las cintas/disquetes/memorias USB o instale los ficheros de
arranque (la mayor�a de los usuarios de CD pueden arrancar desde uno de
�stos).

</para></listitem>
<listitem><para>

Arranque el sistema de instalaci�n.

</para></listitem>
<listitem><para>

Elija el idioma para la instalaci�n.

</para></listitem>
<listitem arch="not-s390"><para>

Active la conexi�n de red, si est� disponible.

</para></listitem>

<listitem arch="s390"><para>

Configure una interfaz de red.

</para></listitem>
<listitem arch="s390"><para>

Abra una conexi�n ssh al nuevo sistema.

</para></listitem>
<listitem arch="s390"><para>

Conecte uno o m�s DASDs (Direct Access Storage Device).

</para></listitem>

<listitem><para>

Cree y monte las particiones en las que instalar� Debian.

</para></listitem>
<listitem><para>

Espere a la descarga/instalaci�n/configuraci�n autom�tica del
<firstterm>sistema base</firstterm>.

</para></listitem>
<listitem><para>

Instale un <firstterm>gestor de arranque</firstterm>
que pueda iniciar &debian; y/o su sistema existente.

</para></listitem>
<listitem><para>

Inicie por primera vez el sistema que acaba de instalar.

</para></listitem>
</orderedlist>

<para condition="gtk">

Tiene la opci�n de usar 
<phrase arch="x86">una versi�n</phrase> 
<phrase arch="powerpc">una versi�n experimental</phrase> 
del sistema de instalaci�n gr�fico en &arch-title;. 
Consulte <xref linkend="graphical"/> si desea m�s informaci�n sobre este
instalador gr�fico.

</para><para>

Si tiene problemas durante la instalaci�n, es muy �til saber qu� paquetes
est�n involucrados en cada paso. A continuaci�n le presentamos a los �actores
principales� en el �drama� de la instalaci�n:

</para><para>

Este manual trata sobre el programa de instalaci�n <classname>debian-installer</classname>. Detecta el hardware y carga
los controladores apropiados, usa <classname>dhcp-client</classname>
para configurar la conexi�n de red, ejecuta
<classname>debbootstrap</classname> para instalar los paquetes del
sistema base y <classname>tasksel</classname> para que pueda seleccionar
algunos programas adicionales. Muchos otros programas realizan peque�as partes de este proceso,
pero <classname>debian-installer</classname> termina su tarea en el momento en que inicia por primera vez su nuevo sistema.

</para><para>

Puede ajustar el sistema a sus necesidades con 
<classname>tasksel</classname>, que le permite escoger dentro de un
conjunto de grupos de programa predefinidos, tales como los necesarios
para disponer de un servidor Web o de un entorno de escritorio.

</para><para>

Una opci�n importante durante la instalaci�n es si se realiza o no
la instalaci�n del entorno de escritorio gr�fico, que est� formado por el
sistema X Window y alguno de los entornos de escritorio gr�ficos disponibles.
Si elige no instalar la tarea <quote>Entorno de escritorio</quote> dispondr�
�nicamente de un sistema muy b�sico de interfaz de l�nea de �rdenes.  La
instalaci�n del entorno de escritorio es opcional porque ocupa una cantidad
significativa de disco y porque muchos sistemas &debian; son servidores que
realmente no necesitan una interfaz
gr�fica de usuario para hacer su trabajo.

</para><para arch="not-s390">

Debe saber que el sistema X Window est� completamente separado del
<classname>debian-installer</classname> y de hecho es mucho m�s
complicado. La instalaci�n y soluci�n de problemas del sistema
X Window no se encuentra dentro del alcance de este manual.

</para>
 </sect1>
