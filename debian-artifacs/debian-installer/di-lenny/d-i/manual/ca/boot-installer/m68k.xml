<!-- retain these comments for translator revision tracking -->
<!-- original version: 45435 -->

  <sect2 arch="m68k"><title>Escollir el mètode d'instal·lació</title>

<para>

Algunes subarquitectures de &arch-title; tenen l'opció d'arrencar utilitzant
un nucli del linux 2.4.x o bé 2.2.x. Quan tingueu esta opció, escolliu el
nucli 2.4.x. L'instal·lador necessitarà menys memòria quan s'utilitza un
nucli de linux 2.4.x ja que el suport per 2.2.x necessita d'un ramdisk amb
mida fixa i el 2.4.x utilitza tmpfs.

</para><para>

Si esteu utilitzant un nucli de linux 2.2.4, aleshores necessitareu utilitzar
el paràmetre del nucli &ramdisksize;.

</para><para>

Assegureu-vos que <userinput>root=/dev/ram</userinput> és un dels vostres
paràmetres del nucli.

</para><para>

Si teniu problemes, llegiu
<ulink url="&url-m68k-cts-faq;">cts's &arch-title; debian-installer FAQ</ulink>.

</para>

<itemizedlist>
	<listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-atari"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-bvme6000"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-mac"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-mvme"/></para></listitem>
	<listitem><para><xref linkend="m68k-boot-q40"/></para></listitem>
</itemizedlist>


    <sect3 id="m68k-boot-amiga"><title>Amiga</title>
<para>

L'únic mètode d'instal·lació de que es disposa als amiga és el disc dur
(vegeu <xref linkend="m68k-boot-hd"/>).
<emphasis>En altres paraules, no es pot arrencar des del cdrom.</emphasis>

</para><para>

L'Amiga no funciona amb bogl, així que si vegeu errors de bogl, així que
si vegeu errors bogl, necessitareu afegir el paràmetre d'arrencada
<userinput>fb=false</userinput>.

</para>
    </sect3>

    <sect3 id="m68k-boot-atari"><title>Atari</title>
<para>

L'instal·lador per Atari es pot engegar des del disc dur
(vegeu <xref linkend="m68k-boot-hd"/>) o des de disquets
(vegeu <xref linkend="boot-from-floppies"/>).
<emphasis>En altres paraules, no es possible arrencar des de cdrom.</emphasis>

</para><para>

L'Atari no funciona amb bogl, així que si vegeu errors de bogl,
necessitareu afegir el paràmetre d'arrencada
<userinput>fb=false</userinput>.

</para>
    </sect3>

    <sect3 id="m68k-boot-bvme6000"><title>BVME6000</title>
<para>

L'instal·lador del BVME6000 es pot arrencar des de cdrom
(vegeu <xref linkend="m68k-boot-cdrom"/>), disquets
(vegeu <xref linkend="boot-from-floppies"/>), o des de la xarxa
(vegeu <xref linkend="boot-tftp"/>).

</para>
    </sect3>

    <sect3 id="m68k-boot-mac"><title>Macintosh</title>
<para>

L'únic mètode d'instal·lació que es disposa als mac és des de
disc dur (vegeu <xref linkend="m68k-boot-hd"/>).
<emphasis>En altres paraules, no es pot arrencar des de cdrom.</emphasis>
Els macs no disposen d'un nucli 2.4.x.

</para><para>

Si el vostre maquinari utilitza un bus scsi basat en 53c9x, aleshores
necessitareu incloure el paràmetre <userinput>mac53c9x=1,0</userinput>.
que deixa engegada l'autodetecció, però que deshabilita la desconnexió del
SCSI. Adoneu-vos que especificar aquest paràmetre tan sols és necessari
si disposeu de més d'un disc dur; en altre cas, el sistema funcionarà
de forma més ràpida si no l'especifiqueu.

</para>
    </sect3>

    <sect3 id="m68k-boot-mvme"><title>MVME147 i MVME16x</title>
<para>

L'instal·lador pel MVME147 i MVME16 es pot engegar des de disquets
(vegeu <xref linkend="boot-from-floppies"/>)
o des de la xarxa (vegeu <xref linkend="boot-tftp"/>).
<emphasis>En altres paraules, no es pot engegar des de cdrom.</emphasis>

</para>
    </sect3>

    <sect3 id="m68k-boot-q40"><title>Q40/Q60</title>
<para>

L'únic mètode d'instal·lació de que es disposa al Q40/Q60 és
des del disc dur (vegeu <xref linkend="m68k-boot-hd"/>).
<emphasis>En altres paraules, no es pot engegar des de cdrom.</emphasis>

</para>
    </sect3>

  </sect2>

  <sect2 arch="m68k" id="m68k-boot-hd"><title>Arrencada des d'un disc dur</title>

&boot-installer-intro-hd.xml;

<para>

Es poden utilitzar al menys sis ramdisks diferents per arrencar des del
disc dur, cadascun dels tres diferents, amb i sense suport pel nucli
linux 2.2.x (per més detalls, vegeu el
<ulink url="&disturl;/main/installer-&architecture;/current/images/MANIFEST">MANIFEST</ulink>).

</para><para>

Els tres tipus distints de ramdisk són <filename>cdrom</filename>,
<filename>hd-media</filename> i <filename>nativehd</filename>. Estos
es diferencien tan sols en la seva font d'instal·lació dels paquets.
El ramdisk <filename>cdrom</filename> utilitza un cdrom per aconseguir els
paquets del debian-installer. El ramdisk <filename>hd-media</filename>
utilitza una imatge iso d'un cdrom que és a un disc dur. Per últim, el
ramdisk <filename>nativehd</filename> utilitza la xarxa per instal·lar
els paquets.

</para>

<itemizedlist>
	<listitem><para><xref linkend="m68k-boothd-amiga"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-atari"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-mac"/></para></listitem>
	<listitem><para><xref linkend="m68k-boothd-q40"/></para></listitem>
</itemizedlist>


    <sect3 id="m68k-boothd-amiga"><title>Arrencada des d'AmigaOS</title>
<para>

Al <command>Workbench</command>, s'engega la instal·lació de Linux
fent doble clic a la icona <guiicon>StartInstall</guiicon> al directori
<filename>debian</filename>.

</para><para>

Heu de prémer &enterkey; dues vegades després que el programa instal·lador
de l'Amiga haja mostrat informació de depuració a una finestra. Després
d'açò, la pantalla es farà grisa, i s'apagarà uns segons. A continuació,
una pantalla negra amb el text en blanc apareixerà, mostrant tot tipus
d'informació de depuració del nucli. Aquests missatges es desplaçaran
massa de pressa per llegir-los, però és normal. Després d'un parell de
segons, el programa d'instal·lació s'engegarà automàticament, així que
podreu continuar després a <xref linkend="d-i-intro"/>.

</para>
    </sect3>


    <sect3 id="m68k-boothd-atari"><title>Arrencada des d'Atari TOS</title>
<para>

A l'escriptori GEM, engegueu el procés d'instal·lació de Linux fent
doble clic a la icona <guiicon>bootstra.prg</guiicon> al directori
<filename>debian</filename> i fent clic l'<guibutton>Ok</guibutton>
al quadre de diàleg de les opcions del programa.

</para><para>

Heu de prémer la tecla &enterkey; després que el programa d'arrencada
de l'Atari haja mostrat informació de depuració a una finestra. Després
d'açò, la pantalla es farà grisa, i es pararà uns segons. A continuació
una pantalla negra amb el text en blanc s'obrirà, mostrant tot tipus
d'informació de depuració del nucli. Aquestos missatges es desplaçaran
massa de pressa per llegir-los, però és normal. Després d'un parell de
segons, el programa d'instal·lació s'hauria d'engegar automàticament,
ara podeu continuar a <xref linkend="d-i-intro"/>.

</para>
    </sect3>


    <sect3 id="m68k-boothd-mac"><title>Arrencada des de MacOS</title>
<para>

Heu de conservar el sistema Mac original i arrencar des d'ell. És
<emphasis>essencial</emphasis> que, quan arrenqueu el MacOS per preparar
l'arrencada del carregador Penguin, podeu aconseguir el mateix esborrant
les extensions i els panells de control des del sistema de carpetes del
Mac. Si no ho feu, les extensions podrien quedar executant-se i provocar
problemes aleatoris amb el nucli de linux que s'executa.

</para><para>

Els Macs necessiten del carregador <command>Penguin</command>, que es pot
descarregar des de <ulink url="&url-m68k-mac;">el projecte de sourceforge.net
Linux/mac68k</ulink>. Si no teniu les eines per gestionar un arxiu
<command>Stuffit</command>, podeu posar-lo a un disquet amb format MacOS
des d'un altra màquina de qualsevol arquitectura amb GNU/Linux emprant
les eines <command>hmount</command>, <command>hcopy</command>, i
<command>humount</command> del paquet <classname>hfsutils</classname>.

</para><para>

A l'escriptori MacOS, engegueu el Procés d'instal·lació de Linux fent doble
clic a la icona <guiicon>Penguin Prefs</guiicon> al directori
<filename>Penguin</filename>. L'arrencador <command>Penguin</command>
s'engegarà. Aneu al punt <guimenuitem>Settings</guimenuitem> al menú
<guimenu>File</guimenu> i cliqueu la pestanya <guilabel>Kernel</guilabel>.
Seleccioneu les imatges del nucli (<filename>vmlinuz</filename>) i del
ramdisk (<filename>initrd.gz</filename>) al directori
<filename>install</filename> clicant als botons corresponents a la part
superior dreta, i navegant seleccioneu els diàlegs per trobar els fitxers.

</para><para>

Per assignar els paràmetres d'arrencada al Penguin, escolliu
<guimenu>File</guimenu> -&gt; <guimenuitem>Settings...</guimenuitem>,
i canvieu a la pestanya <guilabel>Options</guilabel>. Els paràmetres
d'arrencada es poden escriure a l'àrea d'entrada de text. Si voleu
utilitzar sempre aquests paràmetres, seleccioneu <guimenu>File</guimenu>
-&gt; <guimenuitem>Save Settings as Default</guimenuitem>.

</para><para>

Tanqueu el diàleg <guilabel>Settings</guilabel>, deseu el paràmetres
i arrenqueu utilitzant l'element <guimenuitem>Boot Now</guimenuitem>
al menú <guimenu>File</guimenu>.

</para><para>

L'arrencador <command>Penguin</command> mostrarà alguna informació de
depuració a una finestra. Després, la pantalla es farà gris, i hi haurà
uns segons de retràs. A continuació s'obrirà una pantalla negra amb el
text en blanc, mostrant tot tipus d'informació de depuració del nucli.
Aquests missatges poden desplaçar-se de forma molt ràpida per que es
pugui llegir, però açò és normal. Després d'un parell de segons, el
programa d'instal·lació hauria d'engegar-se automàticament, així que
podeu continuar a <xref linkend="d-i-intro"/>.

</para>

    </sect3>

    <sect3 id="m68k-boothd-q40"><title>Arrencada des de Q40/Q60</title>

<para>

FIXME

</para><para>

El programa d'instal·lació s'hauria d'engegar automàticament, així que
continueu baix a <xref linkend="d-i-intro"/>.

</para>

    </sect3>
  </sect2>


  <sect2 arch="m68k" id="m68k-boot-cdrom"><title>Arrencada des d'un CD-ROM</title>
<para>

En aquest moment, tan sols l'única subarquitectura de &arch-title; que suporta
arrencar des de CD-ROM és la BVME6000.

</para>

&boot-installer-intro-cd.xml;

  </sect2>


  <sect2 arch="m68k" id="boot-tftp"><title>Arrencada amb el TFTP</title>

&boot-installer-intro-net.xml;

<para>

Després d'arrencar els sistemes VMEbus voreu l'indicador del LILO
<prompt>Boot:</prompt>. A l'indicador introduïu una de les opcions següents
per arrencar Linux i començar la instal·lació corresponent dels programes
de Debian utilitzant l'emulació de terminal vt102:

<!-- Because the &enterkey; definition uses <keycap>,    -->
<!-- we use <screen> instead of <userinput> in this list -->

<itemizedlist>
<listitem><para>

escriviu <screen>i6000 &enterkey;</screen> per instal·lar un BVME4000/6000

</para></listitem>
<listitem><para>

escriviu <screen>i162 &enterkey;</screen> per instal·lar un MVME162

</para></listitem>
<listitem><para>

escriviu <screen>i167 &enterkey;</screen> per instal·lar un MVME166/167

</para></listitem>
    </itemizedlist>

</para><para>

A més a més podeu afegir la cadena <screen>TERM=vt100</screen> per
utilitzar la emulació de terminal vt100, p.ex.,
<screen>i6000 TERM=vt100 &enterkey;</screen>.

</para>
  </sect2>


  <sect2 arch="m68k" id="boot-from-floppies">
  <title>Arrencada des de disquets</title>
<para>

Per la majoria d'arquitectures &arch-title;, el mètode d'arrencada recomanat
és arrencar des del sistema de fitxers local.

</para><para>

En aquest moment, l'arrencada des de disquet tan sols està suportat per
l'Atari i el VME (amb disquetera SCSI al VME).

</para>
 </sect2>
