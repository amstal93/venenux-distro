<!-- retain these comments for translator revision tracking -->
<!-- original version: 56322 -->

 <sect1 id="non-debian-partitioning">
 <title>Im Voraus partitionieren für eine Multiboot-Installation</title>
<para>

Das Partitionieren Ihrer Festplatte ist das Aufteilen Ihrer Platte
in einzelne Abschnitte. Jeder Abschnitt ist von den anderen unabhängig. Es
ist so ähnlich wie das Aufstellen von Wänden in einem Haus; wenn Sie in
einem Raum ein Möbelstück aufstellen, beeinflusst das die anderen Räume
nicht.

</para><para arch="s390">

Wenn hier von <quote>Festplatten</quote> gesprochen wird, sollten Sie das
für die &arch-title;-Welt als DASD oder VM-Minidisk verstehen. Mit einem
Rechner ist in diesem Fall ein LPAR oder VM-Gast gemeint.

</para><para>

Wenn Sie bereits ein Betriebssystem auf Ihrem Rechner haben

<phrase arch="x86">
(z.B. Windows 9x, Windows NT/2000/XP, OS/2, MacOS, Solaris, FreeBSD, &hellip;)
</phrase>

<phrase arch="alpha">
(z.B. Tru64 (Digital UNIX), OpenVMS, Windows NT, FreeBSD, &hellip;)
</phrase>

<phrase arch="s390">
(z.B. VM, z/OS, OS/390, &hellip;)
</phrase>

<phrase arch="m68k">
(z.B. Amiga OS, Atari TOS, Mac OS, &hellip;)
</phrase>

und Linux auf die gleiche Festplatte installieren möchten, müssen Sie sie
neu partitionieren. Debian benötigt eigene Festplatten-Partitionen.
Es kann nicht auf Windows- oder MacOS-Partitionen installiert werden.
Es kann sich möglicherweise Partitionen mit anderen Linux-Systemen teilen,
was hier jedoch nicht behandelt wird.
Sie benötigen mindestens eine eigene Partition für das Debian-Root-Verzeichnis.

</para><para>

Sie können Informationen über Ihre bisherigen Partitionen mittels eines
Partitionierungs-Tools Ihres aktuellen Betriebssystems bekommen<phrase
arch="x86">, wie etwa fdisk oder PartitionMagic</phrase><phrase
arch="powerpc">, wie etwa Drive Setup, HD Toolkit oder MacTools</phrase><phrase
arch="m68k">, wie etwa HD SC Setup, HDToolBox oder SCSITool</phrase><phrase
arch="s390">, wie dem VM diskmap</phrase>.
Partitionierungsprogramme bieten immer eine Möglichkeit,
existierende Partitionen anzuzeigen, ohne Änderungen vorzunehmen.

</para><para>

Normalerweise zerstört die Änderung einer Partition, die bereits ein Dateisystem
enthält, alle Daten, die darauf gespeichert sind. Daher sollten Sie vor einer
Neupartitionierung immer alle Ihre Daten sichern. Denken Sie an die
Analogie mit dem Haus, auch dort würden Sie Ihr Mobiliar aus dem Zimmer
räumen, bevor Sie eine Mauer verschieben, da Sie ansonsten riskieren, es zu
zerstören.

</para><para arch="hppa" condition="FIXME">

<emphasis>FIXME: write about HP-UX disks?</emphasis>

</para><para>

Wenn Ihr Rechner mehr als eine Festplatte hat, möchten Sie vielleicht
eine der Festplatten komplett für Debian verwenden. Wenn dies der Fall
ist, müssen Sie diese Festplatte nicht partitionieren, bevor Sie die
Installation starten; das im Installer enthaltene
Partitionierungsprogramm kann diese Aufgabe problemlos übernehmen.

</para><para>

Hat Ihr Rechner nur eine Festplatte und Sie möchten Ihr Betriebssystem komplett
durch &debian; ersetzen, können Sie mit dem Partitionieren auch
warten, bis es als Teil des Installationsprozesses aufgerufen wird
(siehe <xref linkend="di-partition"/>). Das funktioniert jedoch nur, wenn Sie den
Installer von System-Tapes, CD-ROM oder per Netzwerk-Boot starten.
Denken Sie daran: wenn Sie von Dateien booten, die sich auf der
Festplatte befinden, und diese Festplatte im Installationsvorgang neu
partitionieren (das heißt, Sie zerstören die Boot-Dateien), sollten Sie
hoffen, dass die Installation gleich
auf Anhieb erfolgreich funktioniert. Zumindest sollten Sie in diesem
Fall eine alternative Methode zum Wiederherstellen des Systems wie zum
Beispiel die CDs oder Tapes der Originalinstallation bereithalten.

</para><para>

Wenn Ihr Gerät bereits mehrere Partitionen hat und genug Platz durch das
Löschen oder Ersetzen von einer oder mehreren von ihnen geschaffen werden
kann, dann können Sie ebenfalls warten und das Partitionierungsprogramm des
Debian-Installers verwenden. Sie sollten sich trotzdem die folgenden
Informationen durchlesen, da es spezielle Umstände (wie die Reihenfolge
der existierenden Partitionen innerhalb der Partitionstabelle) gibt,
die Sie dazu zwingen könnten, die Partitionierung doch vor dem
Debian-Installationsprozess durchzuführen.

</para><para arch="x86">

Wenn Ihr Rechner ein FAT- oder NTFS-Dateisystem enthält, wie es von DOS und
Windows genutzt wird, können Sie mit dem Partitionieren warten
und das Partitionierungswerkzeug des Debian-Installers verwenden, um die
Partition zu verkleinern.

</para><para>

In allen anderen Fällen müssen Sie Ihre Festplatte vor der Installation
neu partitionieren, um partitionierbaren Platz für Debian zu schaffen.
Wenn einige der Partitionen zu anderen Betriebssystemen gehören, sollten
Sie diese unter Verwendung der Partitionsprogramme dieser Betriebssysteme
anlegen. Wir empfehlen, <emphasis>nicht</emphasis> zu versuchen,
Partitionen für &debian; unter Verwendung von Systemprogrammen anderer
Betriebssysteme zu erstellen. Stattdessen sollten Sie nur die nativen
Partitionen dieses Betriebssystems erstellen, die Sie behalten wollen.

</para><para>

Wenn Sie mehr als ein Betriebssystem auf dem gleichen Gerät installieren,
sollten Sie alle anderen Betriebssysteme installieren, bevor Sie mit
der Linux-Installation beginnen. Windows- und andere
Betriebssystem-Installationen könnten das Starten von Linux unmöglich machen,
oder Ihnen empfehlen, nicht-eigene Partitionen neu zu formatieren.

</para><para>

Sie können Beschädigungen durch solche Aktionen reparieren oder vermeiden,
aber das vorherige Installieren dieser Systeme erspart Probleme.

</para><para arch="powerpc">

Damit OpenFirmware &debian; automatisch startet, sollten sich die
Linux-Partitionen vor allen anderen Partitionen auf der Platte befinden,
speziell vor MacOS-Boot-Partitionen. Daran sollten Sie denken, wenn Sie vor der
Linuxinstallation partitionieren; Sie sollten einen Platzhalter für eine Linux-Partition
<emphasis>vor</emphasis> den anderen bootfähigen Partitionen auf der Platte
anlegen. (Die kleinen Partitionen, die für Apple-Disk-Treiber reserviert sind,
sind <emphasis>keine</emphasis> bootfähigen Partitionen.)
Sie können die Platzhalter-Partition später während der eigentlichen Installation
mit den Linux-Partitionierungsprogrammen löschen und durch Linux-Partitionen ersetzen.

</para><para>

Wenn Sie momentan eine Festplatte mit nur einer Partition haben (eine gängige
Einstellung für Desktop-Computer) und das aktuelle Betriebssystem
und Debian per Multi-Boot starten wollen, so müssen Sie:

  <orderedlist>
<listitem><para>

Alles auf dem Computer sichern.

</para></listitem>
<listitem><para>

Mit dem Installationsmedium des originalen Betriebssystems, wie der CD-ROM oder
den Tapes, starten.

<phrase arch="powerpc">Wenn Sie von einer MacOS-CD starten, halten Sie
die Taste <keycap>c</keycap> während des Bootens gedrückt,
um den Start des MacOS-Systems von der CD zu erzwingen.</phrase>

</para></listitem>
<listitem><para>

Verwenden Sie die nativen Partitionierungsprogramme von MacOS zum Erstellen der
nativen Systempartition(en). Erzeugen Sie entweder eine
Platzhalter-Partition für Linux oder lassen Sie Speicherplatz für &debian; frei.

</para></listitem>
<listitem><para>

Installieren Sie das native Betriebssystem neu in seiner neuen (verkleinerten) Partition.

</para></listitem>
<listitem><para>

Starten Sie das native Betriebssystem, um sicherzustellen, dass alles
funktioniert und um die Debian-Installer-Boot-Dateien herunterzuladen.

</para></listitem>
<listitem><para>

Starten Sie den Debian-Installer, um Debian zu installieren.

</para></listitem>
</orderedlist>

</para>

&nondeb-part-alpha.xml; 
&nondeb-part-x86.xml; 
&nondeb-part-m68k.xml; 
&nondeb-part-sparc.xml; 
&nondeb-part-powerpc.xml;

 </sect1>
