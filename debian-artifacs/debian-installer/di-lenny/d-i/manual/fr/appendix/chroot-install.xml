<!-- original version: 55631 -->

 <sect1 id="linux-upgrade">
 <title>Installer &debian; à partir d'un système Unix/Linux</title>

<para>

Cette partie explique comment installer &debian; à partir d'un système
Unix ou Linux existant, sans utiliser le système d'installation avec menus
qui est exposé dans la suite de ce manuel. Les utilisateurs
qui changeaient leur distribution Red Hat, Mandrake et Suse pour &debian;
réclamaient ce guide d'installation. Dans cette 
partie, on suppose que le lecteur a acquis une certaine familiarité avec 
les commandes *nix et qu'il sait parcourir un système de fichiers.
<prompt>$</prompt> symbolisera une commande qui doit être saisie dans 
le système actuel, alors que <prompt>#</prompt> fera référence à une commande 
qui doit être saisie dans le <emphasis>chroot</emphasis> Debian.

</para><para>

Une fois que vous aurez configuré le nouveau système Debian,
vous pourrez y installer les données des utilisateurs (s'il y en a),
et continuer à travailler. &debian; s'installe sans aucune interruption de
service. C'est également une façon intelligente de résoudre les problèmes 
d'équipement qui, d'ordinaire, donnent du fil à retordre aux systèmes 
d'installation ou aux systèmes d'amorçage.

</para>

<note><para>

Cette procédure étant essentiellement <quote>manuelle</quote>, sachez que
la configuration du système vous demandera beaucoup de temps. Elle vous demandera aussi
plus de connaissances de Debian et de Linux en général qu'une installation standard.
Cette procédure ne peut résulter en un système identique à un système installé normalement.
Elle ne donne que les premières étapes de la configuration d'un système. D'autres étapes
seront sans doute nécessaires.
</para></note>
   
  <sect2>
  <title>Pour commencer</title>
<para>

Avec votre outil habituel de partitionnement *nix, partitionnez le disque
dur et créez au moins un système de fichiers ainsi qu'une zone d'échange 
(swap). Vous devez disposer d'au moins 350&nbsp;Mo pour
l'installation d'un système en mode console et d'au moins 1&nbsp;Go si vous
avez l'intention d'installer X (et plus si vous voulez installer un environnement
de bureau comme GNOME ou KDE).

</para><para>

Pour créer un système de fichiers sur les partitions, par exemple, pour créer
un système de fichiers ext3 sur la partition <filename>/dev/hda6</filename>
(ce sera la partition racine dans notre exemple), il suffit de faire&nbsp;:

<informalexample><screen>
# mke2fs -j /dev/<replaceable>hda6</replaceable>
</screen></informalexample>

Pour créer un système de fichier ext2, il suffit d'enlever le 
<userinput>-j</userinput>.

</para><para>

Initialisez et activez la zone d'échange (swap) (modifiez le numéro de la
partition en fonction de celui que vous souhaitez utiliser avec Debian)&nbsp;:

<informalexample><screen>
# mkswap /dev/<replaceable>hda5</replaceable>
# sync; sync; sync
# swapon /dev/<replaceable>hda5</replaceable>
</screen></informalexample>

Montez une partition sur <filename>/mnt/debinst</filename> (le point 
d'installation, qui deviendra le système de fichier racine 
(<filename>/</filename>) sur votre nouveau système). Le point de montage est 
totalement arbitraire, c'est de cette façon qu'on y fera référence par la 
suite.

<informalexample><screen>
# mkdir /mnt/debinst
# mount /dev/<replaceable>hda6</replaceable> /mnt/debinst
</screen></informalexample>

</para>
<note><para>

Si vous voulez que certaines parties du système de fichiers (p. ex. /usr) soient
montées sur des partitions distinctes, vous devez créer et monter ces répertoires
vous-même avant de commencer l'étape suivante.

</para></note>
  </sect2>

  <sect2>
  <title>Installer <command>debootstrap</command></title>
<para>

L'outil utilisé par l'installateur Debian et reconnu comme outil officiel 
pour installer un système Debian de base s'appelle 
<command>debootstrap</command>. Il utilise <command>wget</command> et 
<command>ar</command>, mais ne 
dépend que de <classname>/bin/sh</classname> et des outils Unix/Linux élémentaires
<footnote>
<para>
Ces programmes sont les utilitaires GNU (coreutils) et des commandes comme
<command>sed</command>, <command>grep</command>, <command>tar</command>
et <command>gzip</command>.
</para></footnote>.
Installez <command>wget</command> et <command>ar</command> s'ils ne sont pas déjà
installés sur votre système, puis téléchargez et installez <command>debootstrap</command>.

</para>
<!-- The files linked to here are from 2004 and thus currently not usable
<para>
Si vous possédez un système basé sur rpm, vous pouvez utiliser alien
pour convertir le .deb en .rpm, ou télécharger une «&nbsp;version
rpm-isée&nbsp;» depuis
<ulink url="http://people.debian.org/~blade/install/debootstrap"></ulink>

</para>
-->
<para>
Ou, vous pouvez utiliser la procédure qui suit pour l'installer vous-même.
Créez un répertoire pour y décompresser le .deb&nbsp;:

<informalexample><screen>
# mkdir work
# cd work
</screen></informalexample>

Le programme <command>debootstrap</command> se trouve dans l'archive Debian
(assurez-vous d'avoir sélectionné le fichier correspondant à votre
architecture). Téléchargez le <command>debootstrap</command> .deb à partir de
<ulink url="http://ftp.debian.org/debian/pool/main/d/debootstrap/">pool</ulink>,
copiez le paquet dans le répertoire de travail, et décompressez les fichiers.
Vous aurez besoin d'avoir les droits du superutilisateur pour 
installer le binaire.

<informalexample><screen>
# ar -x debootstrap_0.X.X_all.deb
# cd /
# zcat /full-path-to-work/work/data.tar.gz | tar xv
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Lancer <command>debootstrap</command></title>
<para>

<command>Debootstrap</command> peut télécharger les fichiers nécessaires 
directement depuis l'archive debian. Vous pouvez remplacer toutes les
références à <userinput>&archive-mirror;/debian</userinput> dans les 
commandes ci-dessous, par un miroir de l'archive Debian qui se trouve à 
proximité de chez vous, relativement au réseau. Les miroirs sont listés sur
<ulink url="http://www.debian.org/misc/README.mirrors"></ulink>.

</para><para>

Si vous possédez une version cédérom de &debian; &releasename;, montée sur 
/cdrom,
vous pouvez remplacer l'URL http par l'URL d'un fichier&nbsp;:
<userinput>file:/cdrom/debian/</userinput>

</para><para>

Dans la commande <command>debootstrap</command> remplacez 
<replaceable>ARCH</replaceable> par l'une des expressions suivantes&nbsp;:

<userinput>alpha</userinput>, 
<userinput>amd64</userinput>,
<userinput>arm</userinput>,
<userinput>armel</userinput>,
<userinput>hppa</userinput>, 
<userinput>i386</userinput>, 
<userinput>ia64</userinput>, 
<userinput>m68k</userinput>,
<userinput>mips</userinput>, 
<userinput>mipsel</userinput>, 
<userinput>powerpc</userinput>, 
<userinput>s390</userinput>, ou
<userinput>sparc</userinput>.

<informalexample><screen>
# /usr/sbin/debootstrap --arch ARCH &releasename; \
     /mnt/debinst http://ftp.us.debian.org/debian
</screen></informalexample>

</para>
  </sect2>

  <sect2>
  <title>Configurer le système de base</title>

<para>

Maintenant vous disposez d'un vrai système Debian, certes un peu élémentaire.
Lancez le programme <command>chroot</command>&nbsp;:

<informalexample><screen>
# LANG=C chroot /mnt/debinst /bin/bash
</screen></informalexample>

Il est maintenant possible d'avoir à indiquer la définition du terminal
pour être compatible avec le système de base de Debian. Par exemple&nbsp;:

<informalexample><screen>
# export TERM=<replaceable>xterm-color</replaceable>
</screen></informalexample>

</para>
<sect3>

   <title>Créer les fichiers de périphériques</title>
<para>

Pour l'instant, <filename>/dev/</filename> contient seulement des fichiers élémentaires.
D'autres fichiers seront nécessaires pour les prochaines étapes de l'installation.
La manière de les créer dépend du système sur lequel l'installation se fait&nbsp;; elle
dépend aussi du noyau que vous utiliserez (modulaire ou pas) et du choix entre fichiers
dynamiques (en utilisant <classname>udev</classname>) ou fichiers statiques pour le nouveau
système.

</para><para>

Voici quelques options disponibles&nbsp;:

<itemizedlist>
<listitem><para>

créer un ensemble standard de fichiers de périphériques statiques avec
<informalexample><screen>
# cd /dev
# MAKEDEV generic
</screen></informalexample>

</para></listitem>
<listitem><para>

créer seulement quelques fichiers choisis avec la commande <command>MAKEDEV</command>

</para></listitem>
<listitem><para>

monter (option bind) le répertoire /dev du système hôte sur le répertoire /dev du système cible.
Il faut remarquer que les scripts postinst de certains paquets peuvent essayer
de créer des fichiers de périphériques&nbsp;; cette option doit être employée avec précaution.

</para></listitem>
</itemizedlist>

</para>
</sect3>

   <sect3>
   <title>Monter les partitions</title>
<para>

Vous devez créer <filename>/etc/fstab</filename>.

<informalexample><screen>
# editor /etc/fstab
</screen></informalexample>

Voici un exemple que vous pouvez modifier à votre convenance&nbsp;:

<informalexample><screen>
# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump pass
/dev/XXX         /             ext3    defaults                 0    1
/dev/XXX         /boot         ext3    ro,nosuid,nodev          0    2

/dev/XXX         none          swap    sw                       0    0
proc             /proc         proc    defaults                 0    0

/dev/fd0         /media/floppy   auto    noauto,rw,sync,user,exec 0    0
/dev/cdrom       /media/cdrom    iso9660 noauto,ro,user,exec      0    0

/dev/XXX         /tmp          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /var          ext3    rw,nosuid,nodev          0    2
/dev/XXX         /usr          ext3    rw,nodev                 0    2
/dev/XXX         /home         ext3    rw,nosuid,nodev          0    2
</screen></informalexample>

Utilisez <userinput>mount -a</userinput> pour monter l'ensemble des systèmes 
de fichiers que vous avez indiqué dans votre fichier 
<filename>/etc/fstab</filename>, ou,
pour monter un à un chaque système de fichiers, utilisez&nbsp;:

<informalexample><screen>
# mount /path  # par exemple :  mount /usr
</screen></informalexample>

Les systèmes Debian actuels montent les médias extractibles sous le répertoire
<filename>/media</filename>, mais conservent des liens symboliques de compatibilité
sous la racine <filename>/</filename>. Pour les créer&nbsp;:

<informalexample><screen>
# cd /media
# mkdir cdrom0
# ln -s cdrom0 cdrom
# cd /
# ln -s media/cdrom
</screen></informalexample>

Vous pouvez monter le système de fichiers proc plusieurs fois et à divers
endroits, cependant on choisit d'habitude <filename>/proc</filename>. 
Si vous n'utilisez pas <userinput>mount -a</userinput>, assurez-vous d'avoir monté proc 
avant d'aller plus loin&nbsp;:

<informalexample><screen>
# mount -t proc proc /proc
</screen></informalexample>

</para>
<para>

La commande <userinput>ls /proc</userinput> affiche maintenant un répertoire
non vide. Si cela échoue, vous pouvez monter proc hors du chroot de cette manière :

<informalexample><screen>
# mount -t proc proc /mnt/debinst/proc
</screen></informalexample>

</para>
   </sect3>

   <sect3>
   <title>Configurer le fuseau horaire</title>

<para>
Une option du fichier <filename>/etc/default/rcS</filename> détermine si
le système considère que l'horloge système est réglée sur le temps UTC ou non.
La commande suivante permet de choisir le fuseau horaire.

<informalexample><screen>
# editor /etc/default/rcS
# tzconfig
</screen></informalexample>


</para>
   </sect3>

   <sect3>
   <title>Configurer le réseau</title>
<para>

Pour configurer le réseau, éditez les fichiers
<filename>/etc/network/interfaces</filename>,
<filename>/etc/resolv.conf</filename>,
<filename>etc/hostname</filename> et
<filename>/etc/hosts</filename>.

<informalexample><screen>
# editor /etc/network/interfaces 
</screen></informalexample>

Voici quelques exemples simples qui proviennent de
<filename>/usr/share/doc/ifupdown/examples</filename>&nbsp;:

<informalexample><screen>
######################################################################
# /etc/network/interfaces -- configuration file for ifup(8), ifdown(8)
# See the interfaces(5) manpage for information on what options are 
# available.
######################################################################

# We always want the loopback interface.
#
auto lo
iface lo inet loopback

# To use dhcp:
#
# auto eth0
# iface eth0 inet dhcp

# An example static IP setup: (broadcast and gateway are optional)
#
# auto eth0
# iface eth0 inet static
#     address 192.168.0.42
#     network 192.168.0.0
#     netmask 255.255.255.0
#     broadcast 192.168.0.255
#     gateway 192.168.0.1
</screen></informalexample>

Indiquez vos serveurs de noms et les directives search dans le fichier
<filename>/etc/resolv.conf</filename>&nbsp;:

<informalexample><screen>
# editor /etc/resolv.conf
</screen></informalexample>

Un fichier <filename>/etc/resolv.conf</filename> simple&nbsp;:

<informalexample><screen>
# search hqdom.local
# nameserver 10.1.1.36
# nameserver 192.168.9.100
</screen></informalexample>

Indiquez le nom de votre système (de 2 à 63 caractères)&nbsp;:

<informalexample><screen>
# echo DebianHostName &gt; /etc/hostname
</screen></informalexample>

Et un fichier <filename>/etc/hosts</filename> élémentaire, avec gestion de IPv6&nbsp;:

<informalexample><screen>
127.0.0.1 localhost DebianHostName

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
</screen></informalexample>

Si vous possédez plusieurs cartes réseau, faites en sorte d'indiquer
par ordre les modules que vous voulez charger dans le fichier 
<filename>/etc/modules</filename>. De cette façon, lors du démarrage, chaque 
carte sera associée avec le nom d'interface (eth0, eth1, etc.) que vous 
souhaitiez.

</para>
   </sect3>

   <sect3>
   <title>Configurer Apt</title>
<para>

Debootstrap aura créé un fichier <filename>/etc/apt/sources.list</filename>
élémentaire qui permettra d'installer d'autres paquets.
Cependant, vous pouvez ajouter d'autres sources, par exemple les sources
concernant les mises à jour de sécurité ou celles des paquets source&nbsp;:

<informalexample><screen>
deb-src http://ftp.us.debian.org/debian &releasename; main

deb http://security.debian.org/ &releasename;/updates main
deb-src http://security.debian.org/ &releasename;/updates main
</screen></informalexample>

N'oubliez pas de lancer <userinput>aptitude update</userinput> après avoir modifié
le fichier <filename>/etc/apt/sources.list</filename>.

</para>
   </sect3>
<sect3>
   <title>Configurer les locales et le clavier</title>
<para>

Pour configurer les paramètres locaux, pour l'utilisation d'une autre langue
que l'anglais, installez le paquet <classname>locales</classname> puis configurez-le&nbsp;:
Il est conseillé d'utiliser les locales UTF-8.

<informalexample><screen>
# aptitude install locales
# dpkg-reconfigure locales
</screen></informalexample>

Pour configurer le clavier (si besoin est)&nbsp;:

<informalexample><screen>
# aptitude install console-data
# dpkg-reconfigure console-data
</screen></informalexample>

</para><para>
Il faut noter que le clavier ne peut pas être configuré à l'intérieur du chroot.
Il sera configuré au prochain démarrage de la machine.
</para>
</sect3>

  </sect2>

  <sect2>
  <title>Installer un noyau</title>

<para>

Si vous avez l'intention de démarrer ce système, vous aurez vraisemblablement
besoin d'un noyau Linux ainsi que d'un programme d'amorçage. Identifiez
les paquets contenant des noyaux avec&nbsp;:

<informalexample><screen>
# apt-cache search linux-image
</screen></informalexample>

</para>
<para>
Si vous comptez utiliser un noyau préalablement mis en paquet, vous pouvez d'abord créer
un fichier de configuration <filename>/etc/kernel-img.conf</filename>.
Voici un fichier d'exemple&nbsp;:


<informalexample><screen>
# Kernel image management overrides
# See kernel-img.conf(5) for details
do_symlinks = yes
relative_links = yes
do_bootloader = yes
do_bootfloppy = no
do_initrd = yes
link_in_boot = no
</screen></informalexample>

</para>
<para>

Pour des informations supplémentaires sur ce fichier, consultez sa page
de manuel qui est disponible dès que le paquet
<classname>kernel-package</classname> est installé. Il est recommandé de vérifier
que les valeurs correspondent bien à votre système.
</para>

<para>
Installez alors le noyau de votre choix en utilisant son nom&nbsp;:

<informalexample><screen>
# aptitude install linux-image-<replaceable>&kernelversion;-arch-etc</replaceable>
</screen></informalexample>

Si vous n'avez pas créé de fichier <filename>/etc/kernel-img.conf</filename>
avant l'installation d'un noyau Debian, des questions y faisant référence vous
seront posées pendant l'installation du noyau.
</para> 
  </sect2>

  <sect2>
<title>Configurer le programme d'amorçage</title>
<para>

Pour que votre système &debian; puisse démarrer, configurez le programme
d'amorçage pour qu'il charge le noyau installé avec votre nouvelle partition
racine. <command>Debootstrap</command> n'installe pas de programme d'amorçage mais vous pouvez
utiliser <command>aptitude</command> pour en installer un.

</para><para arch="x86">

Faites <userinput>info grub</userinput> ou 
<userinput>man lilo.conf</userinput> pour connaître les instructions de 
configuration des programmes d'amorçage. Si vous souhaitez conserver le 
système dont vous vous êtes servi pour installer Debian, ajoutez simplement 
une entrée pour l'installation de Debian dans votre 
<filename>menu.lst</filename> ou dans votre <filename>lilo.conf</filename>. 
Pour <filename>lilo.conf</filename>, vous pouvez également le copier sur le 
nouveau système et l'éditer. Une fois que 
vous l'aurez modifié, lancez <command>lilo</command> (souvenez-vous que lilo utilise le fichier 
<filename>lilo.conf</filename> qui se trouve dans le même système que lui).

</para><para arch="x86">

L'installation et la configuration de <classname>grub</classname> est aussi simple que
ça&nbsp;:

<informalexample><screen>
# aptitude install grub
# grub-install /dev/<replaceable>hda</replaceable>
# update-grub
</screen></informalexample>

La deuxième commande installe <command>grub</command>
(ici, sur le MBR de <literal>hda</literal>). La dernière commande crée
un fichier <filename>/boot/grub/menu.lst</filename> fonctionnel.
</para>
<para>

On suppose ici qu'un fichier <filename>/dev/hda</filename> a été créé.
Il y a d'autres méthodes pour installer <command>grub</command>,
mais elles sortent du cadre de cette annexe.

</para>

<para arch="x86">

Voici un exemple simple de fichier <filename>/etc/lilo.conf</filename>&nbsp;:

<informalexample><screen>
boot=/dev/<replaceable>hda6</replaceable>
root=/dev/<replaceable>hda6</replaceable>
install=menu
delay=20
lba32
image=/vmlinuz
initrd=/initrd.img
label=Debian
</screen></informalexample>


</para>

<para arch="x86">

Selon le programme d'amorçage choisi, vous pouvez apporter d'autres
changements au fichier <filename>/etc/kernel-img.conf</filename>.
</para>
<para arch="x86">
Pour le programme <classname>grub</classname>, vous
pouvez mettre l'option <literal>do_bootloader</literal> à <quote>no</quote>. Pour
mettre à jour automatiquement le fichier<filename>/boot/grub/menu.lst</filename>
lors de la suppression ou l'installation de noyaux Debian, ajoutez ces lignes&nbsp;:

<informalexample><screen>
postinst_hook = update-grub
postrm_hook   = update-grub
</screen></informalexample>

Pour le programme d'amorçage <classname>lilo</classname>, la valeur de 
<literal>do_bootloader</literal> doit être <quote>yes</quote>.

</para>

<para arch="powerpc">

Consultez le <userinput>man yaboot.conf</userinput> pour les instructions de 
configuration du programme d'amorçage. Si vous souhaitez conserver le système 
dont vous vous êtes servi pour installer Debian, ajoutez simplement une 
entrée pour l'installation de Debian dans votre yaboot.conf actuel. Vous 
pouvez également le copier sur le nouveau système et l'éditer. Une fois que 
vous l'aurez
modifié, lancez ybin (souvenez-vous que ybin utilise le yaboot.conf qui se 
trouve dans le même système que lui).
</para><para arch="powerpc">

Voici un exemple simple de fichier <filename>/etc/yaboot.conf</filename>&nbsp;:

<informalexample><screen>
boot=/dev/hda2
device=hd:
partition=6
root=/dev/hda6
magicboot=/usr/lib/yaboot/ofboot
timeout=50
image=/vmlinux
label=Debian
</screen></informalexample>

Sur certaines machines, vous devrez utiliser <userinput>ide0:</userinput> 
au lieu de <userinput>hd:</userinput>. 

</para>
  </sect2>
  <sect2>
<title>Touches finales</title>
<para>

Comme signalé auparavant, le système sera très basique. Pour l'améliorer
sensiblement, il existe une méthode très simple pour installer les paquets
dont la priorité est <quote>standard</quote>&nbsp;:

<informalexample><screen>
# tasksel install standard
</screen></informalexample>

Bien sûr, vous pouvez toujours utiliser <command>aptitude</command> pour installer
des paquets un à un.
</para>
<para>
Après l'installation il y aura beaucoup de paquets dans 
<filename>/var/cache/apt/archives/</filename>. Vous pouvez libérer un peu d'espace avec&nbsp;:

<informalexample><screen>
# aptitude clean
</screen></informalexample>

</para>
  </sect2>
 </sect1>
