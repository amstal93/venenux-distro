<!-- original version: 56419 -->

  <sect2 arch="x86"><title>Amorcer depuis un cédérom</title>

&boot-installer-intro-cd.xml;

<!-- We'll comment the following section until we know exact layout -->
<!-- 
Le cédérom n°&nbsp;1 du jeu officiel de cédéroms pour
      &arch-title; vous présentera une invite <prompt>boot:</prompt> sur la
      plupart des matériels. Appuyez sur <keycap>F4</keycap> pour afficher la
      liste des options du noyau disponibles à partir desquelles
      amorcer. Tapez simplement le nom de la saveur (idepci, vanilla,
      compact, bf24) après l'invite de <prompt>boot:</prompt> et validez par
      &enterkey;.

</para><para>

Si votre matériel ne supporte pas l'amorçage d'images multiples,
      insérez un des autres cédéroms dans le lecteur. Il semble que la
      plupart des lecteurs de cédéroms SCSI soient incapables de gérer
l'amorçage d'images multiples de <command>isolinux</command>, les
      possesseurs de tels lecteurs de cédéroms devraient essayer le
      cédérom 2 (vanilla), 3 (compact) ou 5 (bf2.4).

</para><para>

Les cédéroms numéro 2 à 5 amorceront chacun sur une saveur différente
dépendant du cédérom inséré. cf.<xref linkend="kernel-choice"/>
pour une discussion sur les différentes
variantes. Voici comment sont disposées les variantes sur les
      différents cédéroms&nbsp;:

<variablelist>
<varlistentry>
<term>CD 1</term><listitem><para>

           Sur les nouveaux matériels, il permet une sélection
           d'images de noyau à amorcer. Sur les vieux matériels, il
           amorcera le noyau en saveur «&nbsp;idepci&nbsp;»&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 2</term><listitem><para>

Amorce la saveur «&nbsp;vanilla&nbsp;»&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 3</term><listitem><para>

Amorce la saveur «&nbsp;compact&nbsp;»&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 4</term><listitem><para>

Amorce la saveur «&nbsp;idepci&nbsp;»&nbsp;;

</para></listitem></varlistentry>
<varlistentry>
<term>CD 5</term><listitem><para>

Amorce la saveur «&nbsp;bf2.4&nbsp;».

</para></listitem></varlistentry>

 </variablelist>

</para><para>

-->

</sect2>

 <sect2 arch="x86" id="boot-win32">
  <title>Amorcer sur Windows</title>

<para>
Pour lancer l'installateur à partir de Windows, vous devez avoir un CD-ROM/DVD-ROM ou
une clé USB d'installation. Consultez les sections <xref linkend="official-cdrom"/>
et <xref linkend="boot-usb-files"/>.
</para>

<para>
Quand vous insérez le CD-ROM/DVD-ROM, un programme de pré-installation doit normalement
être lancé. Si Windows ne l'exécute pas ou si vous utilisez une clé USB, vous pouvez le lancer
vous-même en exécutant la commande <command>setup.exe</command> sur le périphérique.   
</para>
<para>
Ce programme pose quelques questions préliminaires et prépare le système pour le lancement
de l'installateur &debian;.
</para>

  </sect2>

<!-- FIXME the documented procedure does not exactly work, commented out
      until fixes

  <sect2 arch="x86" id="install-from-dos">
  <title>Amorcer depuis une partition DOS</title>

&boot-installer-intro-hd.xml;

<para>

Démarrez en MS-DOS (pas en Windows) sans charger de pilotes. Pour faire cela, 
il faut presser <keycap>F8</keycap> au bon moment (et éventuellement 
sélectionner l'option «&nbsp;safe mode command prompt only&nbsp;»&nbsp;: 
«&nbsp;mode DOS sans échec&nbsp;»). Puis allez dans le sous-répertoire qui
contient la saveur que vous avez choisie, p. ex.

<informalexample><screen>
cd c:\current\compact
</screen></informalexample>.  

Ensuite exécutez <command>install.bat</command>. Le noyau se chargera et 
lancera l'installateur.

</para><para>

Veuillez noter qu'il y a actuellement un problème avec loadlin 
(bogue n°142421) qui empêche le fichier <filename>install.bat</filename> 
d'être utilisé par la saveur bf2.4. Le symptôme de ce problème est le message 
<computeroutput>invalid compressed format</computeroutput>.

</para>
  </sect2>

END FIXME -->

  <sect2 arch="x86" id="boot-initrd">
  <title>Amorcer à partir de Linux avec <command>LILO</command>
  ou <command>GRUB</command></title>

<para>
Pour amorcer l'installateur sur un disque dur, vous devez d'abord télécharger
les fichiers et les installer comme l'explique la section
<xref linkend="boot-drive-files"/>.
</para>

<para>
Si vous voulez utiliser le disque seulement pour l'amorçage et ensuite 
utiliser le réseau pour télécharger tous les paquets, vous devez récupérer 
le fichier <filename>netboot/debian-installer/&architecture;/initrd.gz</filename> et le
noyau qui lui correspond, <filename>netboot/debian-installer/&architecture;/linux</filename>.
Vous pourrez ainsi repartitionner le disque sur lequel se trouve l'installateur.
Toutefois cette opération demande un grand soin.
</para>

<para>
Si vous voulez préserver une partition de ce disque, vous pouvez
télécharger le fichier <filename>hd-media/initrd.gz</filename> et le noyau
correspondant&nbsp;; copiez aussi une image CD sur le disque (assurez-vous
que le nom de cette image finit en <literal>.iso</literal>). L'installateur
peut alors se lancer à partir du disque dur et s'installer à partir de cette
image, sans avoir besoin du réseau.
</para>

<para>

Pour <command>LILO</command>, vous devez configurer deux choses essentielles
dans <filename>/etc/lilo.conf</filename>&nbsp;:
<itemizedlist>
<listitem><para>

le chargement de <filename>initrd.gz</filename> au moment de l'amorçage&nbsp;;

</para></listitem>
<listitem><para>

l'utilisation par le noyau <filename>vmlinuz</filename> d'un disque virtuel
en mémoire comme sa partition racine.

</para></listitem>
</itemizedlist>

Voici un exemple de fichier <filename>/etc/lilo.conf</filename>&nbsp;:

</para><para>

<informalexample><screen>
image=/boot/newinstall/vmlinuz
       label=newinstall
       initrd=/boot/newinstall/initrd.gz
</screen></informalexample>

Pour plus de précisions, référez-vous aux pages de manuel de
<citerefentry><refentrytitle>initrd</refentrytitle>
<manvolnum>4</manvolnum></citerefentry> et de 
<citerefentry><refentrytitle>lilo.conf</refentrytitle>
<manvolnum>5</manvolnum></citerefentry>. Exécutez maintenant
<userinput>lilo</userinput> et relancez la machine.
</para>

<para>
La procédure pour <command>GRUB</command> est relativement similaire.
Cherchez le fichier <filename>menu.lst</filename> dans le répertoire
<filename>/boot/grub/</filename> (parfois dans
<filename>/boot/boot/grub/</filename>), ajoutez les lignes suivantes&nbsp;:

<informalexample><screen>
title  New Install
kernel (hd0,0)/boot/newinstall/vmlinuz
initrd (hd0,0)/boot/newinstall/initrd.gz
</screen></informalexample>

et redémarrez.
</para>
<para>
Il sera peut-être nécessaire d'augmenter 
le paramètre <userinput>ramdisk_size</userinput>, selon l'image que vous
démarrez.
À partir de maintenant, il ne devrait plus y avoir de différences
entre <command>GRUB</command> et <command>LILO</command>.
</para>
</sect2>

<sect2 arch="x86" condition="bootable-usb" id="usb-boot">
<title>Amorcer sur une clé USB</title>

<para>
Supposons que tout a été préparé comme l'expliquent les sections
<xref linkend="boot-dev-select"/> et <xref linkend="boot-usb-files"/>. 
Branchez maintenant votre clé USB dans un port libre et relancez la machine.
Le système devrait se lancer et une invite <prompt>boot:</prompt> apparaître.
Maintenant, vous pouvez saisir des paramètres optionnels ou simplement
appuyer sur la touche &enterkey;.
</para>

</sect2>

  <sect2 arch="x86" condition="supports-floppy-boot" id="floppy-boot">
  <title>Amorcer depuis des disquettes</title>
<para>

Vous devez avoir déjà téléchargé les images de disquettes nécessaires
à partir des images présentes dans <xref linkend="create-floppy"/>. 
<!-- missing-doc FIXME If you need to, you can also modify the boot floppy; see\
<xref linkend="rescue-replace-kernel"/>. -->
</para>
<para>

Pour amorcer depuis la disquette d'amorçage, placez-la dans le premier 
lecteur de disquette, éteignez la machine comme 
vous le faites habituellement puis rallumez-la.

</para><para>

Pour réaliser l'installation à partir d'un lecteur LS-120 (version ATAPI) 
avec un jeu de disquettes, vous devrez préciser l'emplacement virtuel du 
lecteur de disquette. On utilise le paramètre d'amorçage
<emphasis>root=</emphasis>, en indiquant le périphérique que le 
pilote ide-floppy aura simulé. Par exemple si votre lecteur est connecté sur 
la première interface IDE en seconde position sur le câble, entrez
<userinput>install root=/dev/hdc</userinput> à l'invite de démarrage.

</para><para>

Remarquez bien que sur certaines machines <keycombo><keycap>Control</keycap>
<keycap>Alt</keycap> <keycap>Delete</keycap></keycombo> ne remet pas 
correctement la machine à zéro. Éteindre la machine est alors recommandé. Si 
vous installez depuis un système d'exploitation existant, (p. ex. à partir 
d'une machine MS-DOS), vous n'aurez pas le choix. Sinon, éteignez la
machine et rallumez-la.

</para><para>

La disquette sera lue et vous devriez apercevoir un écran 
présentant la disquette d'amorçage et l'invite <prompt>boot:</prompt> au bas de
l'écran.
</para>

<para>

Une fois que vous avez appuyé sur la touche &enterkey;, vous devez voir le 
message
<computeroutput>Loading...</computeroutput> suivi par
<computeroutput>Uncompressing Linux...</computeroutput> et ensuite un écran
rempli d'informations sur les éléments matériels de votre machine. Vous
pouvez trouver un complément d'information sur cette phase dans la
<xref linkend="kernel-msgs"/>.
</para>

<para>
Après avoir démarré depuis la disquette d'amorçage, la disquette root est
demandée. Insérez-la puis pressez la touche &enterkey; et le contenu est
chargé en mémoire. L'<command>installateur Debian</command> est 
automatiquement lancé.
</para>
</sect2>

  <sect2 arch="x86" id="boot-tftp"><title>Amorcer avec TFTP</title>
 
&boot-installer-intro-net.xml;
 
<para>

Il y a plusieurs façons d'amorcer avec TFTP sur une machine i386.

</para>

   <sect3><title>Carte réseau ou carte mère avec PXE</title>
<para>

Il est possible que votre interface réseau ou votre carte mère permette
l'amorçage PXE. C'est une réimplémentation de l'amorçage TFTP par 
<trademark class="trade">Intel</trademark>. Dans ce cas, vous pourrez 
configurer votre bios pour qu'il s'amorce sur le réseau.

</para>
   </sect3>

   <sect3><title>Carte réseau avec ROM de démarrage réseau</title>
<para>

Une carte d'interface réseau peut offrir la possibilité de démarrer par TFTP.

</para><para condition="FIXME">

Dites-nous (<email>&email-debian-boot-list;</email>) comment vous 
avez fait. Veuillez vous référer à ce document.

</para>
   </sect3>

   <sect3><title>Etherboot</title>
<para>
Le <ulink url="http://www.etherboot.org">projet etherboot</ulink> offre des
disquettes d'amorçage et même des ROM qui permettent l'amorçage par TFTP.
</para>
</sect3>
</sect2>

  <sect2 arch="x86" id="boot-screen">
<title>L'écran d'amorçage</title>
<para>
Quand l'installateur démarre, apparaît un écran montrant le logo de la distribution
et un menu&nbsp;: 

<informalexample><screen>
Installer boot menu

Install
Graphical install
Advanced options       >
Help

Press ENTER to boot or TAB to edit a menu entry
</screen></informalexample>

Selon la méthode d'installation utilisée, l'option <quote>Graphical install</quote>
est, ou non, disponible.
</para>
<para>
Pour une installation normale, sélectionnez l'entrée <quote>Install</quote>
ou l'entrée <quote>Graphical install</quote> en utilisant les touches avec
flèche du clavier ou en tapant la première lettre en surbrillance.
Vous pouvez maintenant appuyer sur la touche &enterkey; pour
lancer l'installateur.
</para>
<para>
L'entrée <quote>Advanced options</quote> donne accès à un second menu
qui permet d'amorcer l'installateur en mode <quote>expert</quote> ou mode <quote>rescue</quote>.
Il est aussi utilisé pour les installations automatisées.
</para>
<para>
Si vous voulez ajouter des paramètres d'amorçage pour l'installateur ou pour le noyau,
appuyez sur la touche &tabkey;. La commande d'amorçage pour l'entrée du menu choisie est affichée
et l'on peut ajouter des options. Les écrans d'aide listent les principales options possibles.
Appuyez sur la touche &enterkey; pour lancer l'installateur avec vos options. Pour revenir au menu et
annuler toutes vos saisies, appuyez sur la touche &escapekey;.
</para> 
<para>
Quand vous sélectionnez l'entrée <quote>Help</quote>, un premier écran
présente tous les écrans d'aide disponibles. Il n'est pas possible de revenir
au menu d'amorçage une fois dans l'aide. Cependant les écrans F3 et F4 listent
des commandes équivalentes aux commandes d'amorçage listées dans le menu principal.
Tous les écrans d'aide ont une invite permettant de saisir une commande&nbsp;:

<informalexample><screen>
Press F1 for the help index, or ENTER to boot:
</screen></informalexample>

Vous pouvez alors appuyer sur la touche &enterkey; pour amorcer l'installateur
avec les options par défaut ou bien saisir une commande particulière avec les
paramètres d'amorçage nécessaires.
Les écrans d'aide listent certains paramètres utiles.
Si vous ajoutez des paramètres à la ligne de commande, n'oubliez pas
d'indiquer la méthode d'amorçage (la valeur par défaut est
<userinput>install</userinput>) et un espace avant le premier paramètre
(par exemple, <userinput>install fb=false</userinput>). 

<note><para>

Le clavier à cet instant est supposé être un clavier de type <quote>American English</quote>.
Si votre carte clavier est agencée autrement, les caractères affichés sur l'écran
seront parfois différents de ceux que vous saisissez. Il existe sur Wikipedia un 
<ulink url="&url-us-keymap;">schéma de la carte clavier US</ulink> qui peut vous aider à saisir les
bons caractères.

</para></note>
<note><para>
Si le BIOS du système est configuré pour utiliser une console série, il se peut
que l'écran graphique initial ne s'affiche pas au moment de l'amorçage de l'installateur,
ni le menu d'amorçage.
Cela peut aussi arriver si vous installez le système depuis une machine distante qui offre une
interface texte à la console VGA, par exemple la console texte
<quote>integrated Lights Out</quote> (iLO) de Compaq ou la 
<quote>Integrated Remote Assistant</quote> (IRA) de HP.
</para>
<para>

Pour sauter l'écran d'amorçage graphique, vous pouvez appuyer sur la touche &escapekey;
pour obtenir l'invite d'amorçage en mode texte, ou appuyer sur la touche <quote>H</quote> puis
sur la touche &enterkey; pour sélectionner l'option <quote>Help</quote> décrite plus haut.
Toute saisie sera alors affichée sur la ligne d'invite. Pour empêcher l'installateur
d'utiliser le tampon vidéo dans la suite de l'installation, vous pouvez
ajouter le paramètre <userinput>fb=false</userinput> à l'invite d'amorçage.

</para></note>

</para>
  </sect2>
