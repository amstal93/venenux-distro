<!-- original version: 45435 -->

<sect2 arch="m68k"><title>Choisir une méthode d'installation</title>
 
<para>
Certaines sous-architectures de &arch-title; peuvent être amorcées avec
un noyau linux de la série 2.2.x ou un noyau de la série 2.4.x. Quand ce choix
existe, essayez la série 2.4.x. L'installateur demande moins de mémoire avec
un noyau de la série 2.4.x et il utilise tmpfs. Un noyau de la série 2.2.x
demande un disque virtuel de taille fixe.
</para>
<para>
Si vous utilisez un noyau de la série 2.2.x, vous devez utiliser 
le paramètre du noyau &ramdisksize;.
</para>

<para>
Assurez-vous que vous avez bien passé le paramètre 
<userinput>root=/dev/ram</userinput> au noyau.
</para>
<para>
En cas de problèmes, consultez
<ulink url="&url-m68k-cts-faq;">cts's &arch-title; debian-installer FAQ</ulink>.
</para>

<itemizedlist>
        <listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-amiga"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-atari"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-bvme6000"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-mac"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-mvme"/></para></listitem>
        <listitem><para><xref linkend="m68k-boot-q40"/></para></listitem>
</itemizedlist>

<sect3 id="m68k-boot-amiga"><title>Amiga</title>
<para>

La seule méthode possible pour amorcer un Amiga est d'utiliser un disque
dur (voyez <xref linkend="m68k-boot-hd"/>).
<emphasis>Autrement dit, le cédérom n'est pas amorçable.</emphasis>
</para>
<para>
Amiga ne fonctionne pas avec bogl, et si vous voyez des erreurs venant de
bogl, vous devez passer le paramètre suivant au noyau,
<userinput>fb=false</userinput>.
</para>
</sect3>

<sect3 id="m68k-boot-atari"><title>Atari</title>
<para>
Avec Atari, vous pouvez lancer l'installateur soit à partir d'un disque dur
(voyez <xref linkend="m68k-boot-hd"/>) soit à partir de disquettes (voyez
<xref linkend="boot-from-floppies"/>).
<emphasis>Autrement dit, le cédérom n'est pas amorçable.</emphasis>
</para>
<para>
Atari ne fonctionne pas avec bogl, et si vous voyez des erreurs venant de
bogl, vous devez passer le paramètre suivant au noyau,
<userinput>fb=false</userinput>.
</para>
</sect3>

<sect3 id="m68k-boot-bvme6000"><title>BVME6000</title>
<para>

L'installateur pour BVME6000 peut être lancé à partir d'un cédérom
(voyez <xref linkend="m68k-boot-cdrom"/>), de disquettes (voyez
<xref linkend="boot-from-floppies"/>) ou à partir du réseau (voyez
<xref linkend="boot-tftp"/>).
</para>
</sect3>

<sect3 id="m68k-boot-mac"><title>Macintosh</title>
<para>

Le seule méthode d'installation possible pour un mac est d'utiliser un
disque dur (voyez<xref linkend="m68k-boot-hd"/>).
<emphasis>Autrement dit, le cédérom n'est pas amorçable.</emphasis>
Il n'y a pas de noyau 2.4.x pour les Macs.
</para>
<para>
Si votre matériel utilise un bus scsi de type 53c9x, vous devrez sans doute
inclure le paramètre <userinput>mac53c9x=1,0</userinput>. Si votre matériel 
utilise deux bus scsi, par exemple Quadra 950, vous devrez inclure le
paramètre <userinput>mac53c9x=2,0</userinput>. Vous pouvez aussi indiquer
<userinput>mac53c9x=-1,0</userinput>, ce qui conserve la détection automatique mais
désactive la fonction SCSI de déconnexion. Ce paramètre n'est pas nécessaire si vous
n'avez qu'un seul disque. Le système ira plus vite si vous ne l'utilisez pas.

</para>
</sect3>

<sect3 id="m68k-boot-mvme"><title>MVME147 et MVME16x</title>
<para>

L'installateur pour MVME147 et MVME16x peut être lancé à partir de disquettes 
(voyez <xref linkend="boot-from-floppies"/>) ou partir du réseau (voyez
<xref linkend="boot-tftp"/>).
<emphasis>Autrement dit, le cédérom n'est pas amorçable.</emphasis>
</para>
</sect3>

<sect3 id="m68k-boot-q40"><title>Q40/Q60</title>
<para>

Le seule méthode d'installation possible pour un Q40/Q60 est d'utiliser un
disque dur (voyez<xref linkend="m68k-boot-hd"/>).
<emphasis>Autrement dit, le cédérom n'est pas amorçable.</emphasis>
</para>
</sect3>

 </sect2>

 <sect2 arch="m68k" id="m68k-boot-hd"><title>Amorcer depuis un disque dur</title>

&boot-installer-intro-hd.xml;

<para>
On peut utiliser au moins six disques virtuels différents pour amorcer depuis
un disque dur&nbsp;: trois types, chacun avec ou sans le noyau de la série 
2.2.x
(voyez <ulink url="&disturl;/main/installer-&architecture;/cur\rent/images/MANIFEST">MANIFEST</ulink> pour des précisions).
</para>
<para>
Les trois types de disques virtuels sont <filename>cdrom</filename>,
<filename>hd-media</filename> et <filename>nativehd</filename>. Ces disques
diffèrent seulement par la source où ils prennent les paquets de 
l'installation. Le disque <filename>cdrom</filename>  utilise un cédérom pour
obtenir les paquets de l'installateur. Le disque <filename>hd-media</filename>
utilise une image iso de cédérom qui se trouve sur le disque dur. Et le disque
<filename>nativehd</filename> utilise le réseau pour installer les paquets.
</para>

<itemizedlist>
        <listitem><para><xref linkend="m68k-boothd-amiga"/></para></listitem>
        <listitem><para><xref linkend="m68k-boothd-atari"/></para></listitem>
        <listitem><para><xref linkend="m68k-boothd-mac"/></para></listitem>
        <listitem><para><xref linkend="m68k-boothd-q40"/></para></listitem>
</itemizedlist>

<sect3 id="m68k-boothd-amiga"><title>Amorcer sur AmigaOS</title>
<para>

Dans le <command>Workbench</command>, lancez le processus d'installation de Linux en cliquant deux fois sur l'icône <guiicon>StartInstall</guiicon> dans le 
répertoire <filename>debian</filename>.

</para><para>

Vous devrez sans doute enfoncer deux fois la touche &enterkey; après que le 
programme d'installation d'Amiga aura affiché des informations de débogage 
dans une fenêtre. Après cela, l'écran deviendra gris, il faudra attendre 
quelques secondes, puis un écran noir avec du texte en blanc devrait 
apparaître et afficher toutes sortes d'informations de débogage du noyau. 
Ces messages pourraient défiler trop vite pour pouvoir être lus, mais ce 
n'est pas grave. Après quelques secondes, le programme d'installation devrait 
démarrer automatiquement et vous pourrez poursuivre avec la section 
<xref linkend="d-i-intro"/>.

</para>
 </sect3>


  <sect3 id="m68k-boothd-atari"><title>Amorcer sur Atari TOS</title>
<para>

Sur le bureau GEM, lancez le processus d'installation en cliquant deux fois
sur l'icône <guiicon>bootstra.prg</guiicon> dans le répertoire 
<filename>debian</filename> et en cliquant sur <guibutton>Ok</guibutton> dans 
la boîte de dialogue d'options.

</para>
<para>

Vous devrez sans doute enfoncer la touche &enterkey; après que le programme 
d'installation d'Atari aura affiché des informations de débogage dans une 
fenêtre. Après cela, l'écran deviendra gris, il faudra attendre quelques
secondes, puis un écran noir avec du texte en blanc devrait apparaître et 
afficher toutes sortes d'informations de débogage du noyau. Ces messages 
pourraient défiler trop vite pour pouvoir être lus, mais ce n'est pas grave. 
Après quelques secondes, le programme d'installation devrait démarrer 
automatiquement et vous pourrez continuer avec la section 
<xref linkend="d-i-intro"/>.

</para>
</sect3>


  <sect3 id="m68k-boothd-mac"><title>Amorcer sur MacOS</title>
<para>
Vous devez garder le système original Mac et amorcer avec lui. Il est
<emphasis>essentiel</emphasis>, lorsque vous lancez MacOS pour amorcer le
programme Penguin linux, que vous teniez la touche <keycap>shift</keycap>
enfoncée pour empêcher le chargement d'extensions. Si vous n'utilisez MacOS
que pour charger linux, vous pouvez arriver au même résultat en supprimant
toutes les extensions et les panneaux de contrôle dans le dossier System de
Mac. Sinon des extensions peuvent s'exécuter et empécher le bon fonctionnement du noyau linux.
</para>
<para>
Les Mac demandent le programme d'amorçage <command>Penguin</command>. 
Il peut être téléchargé sur le projet
<ulink url="&url-m68k-mac;">Linux/mac68k de sourceforge.net</ulink>.
Si vous n'avez pas les outils pour gérer une archive <command>Stuffit</command>, 
vous pouvez la mettre sur une disquette au format MacOS en utilisant une deuxième machine
GNU/Linux et les outils <command>hmount</command>, <command>hcopy</command>,
et <command>humount</command> qui appartiennent à la suite <classname>hfsutils</classname>.
</para>

<para>
Sur le bureau MacOS, lancez le processus d'installation de
Linux en cliquant deux foix sur l'icône <guiicon>Penguin Prefs</guiicon>
dans le dossier <filename>Penguin</filename>. Le programme
<command>Penguin</command> va s'exécuter. Allez dans
<guimenuitem>Settings</guimenuitem>, dans le menu
<guimenu>File</guimenu>, cliquez sur <guilabel>Kernel</guilabel>.
Sélectionnez le noyau (<filename>vmlinuz</filename>) et le disque virtuel
(<filename>initrd.gz</filename>) dans le répertoire
<filename>install</filename> en cliquant sur les boutons correspondant dans 
le coin supérieur droit et en se déplaçant dans les dialogues de choix de 
fichier afin de trouver les fichiers. 
</para>
<para>
Pour indiquer les paramètres dans Penguin, choisissez 
<guimenu>File</guimenu> -&gt; <guimenuitem>Settings...</guimenuitem>, et
passez sur <guilabel>Options</guilabel>. On peut saisir les paramètres
dans la zone de texte. Si vous voulez conserver ces paramètres, choisissez
<guimenu>File</guimenu> -&gt; <guimenuitem>Save Settings as Default</guimenuitem>.
</para>
<para>
Fermez la boite de dialogue <guilabel>Settings</guilabel>, sauvegardez vos
paramètres et lancez l'amorçage avec l'élément <guimenuitem>Boot Now</guimenuitem> du menu <guimenu>File</guimenu>.
</para>

<para>
Le programme <command>Penguin</command> affichera quelques infos de débogage 
dans une fenêtre. Après cela, l'écran deviendra gris et il y aura une pause 
de quelques secondes. Ensuite, un écran noir avec du texte blanc devrait
apparaître et afficher de informations de débogage du noyau. Ces messages 
pourraient défiler trop vite pour que vous ayez le temps de les lire. Ce 
n'est pas grave. Après quelques secondes, le programme d'installation devrait
démarrer automatiquement, et vous pouvez continuer avec la section
<xref linkend="d-i-intro"/>.

</para>

</sect3>

<sect3 id="m68k-boothd-q40"><title>Amorcer avec Q40/Q60</title>


<para>
FIXME
</para>
<para>
Le programme d'installation devrait se lancer automatiquement. Vous pouvez
continuer avec <xref linkend="d-i-intro"/>.
</para>
</sect3>
</sect2>


  <sect2 arch="m68k" id="m68k-boot-cdrom"><title>Amorcer depuis un cédérom</title>
<para>
La seule sous-architecture de &arch-title; qui peut s'amorcer à partir d'in
cédérom est BVME6000.
</para>

&boot-installer-intro-cd.xml;
</sect2>


<sect2 arch="m68k" id="boot-tftp"><title>Amorcer avec TFTP</title>

&boot-installer-intro-net.xml;

<para>
Après l'amorçage des systèmes VMEbus, vous aurez l'invite LILO&nbsp;: 
<prompt>Boot:</prompt>. Tapez l'un des arguments suivants pour amorcer Linux 
et commencer l'installation dans de bonnes conditions, en utilisant 
l'émulation du terminal vt102&nbsp;:

<!-- Because the &enterkey; definition uses <keycap>,    -->
<!-- we use <screen> instead of <userinput> in this list -->

<itemizedlist>
<listitem><para>

tapez <screen>i6000 &enterkey;</screen> pour l'installation BVME4000/6000

</para></listitem>
<listitem><para>

tapez <screen>i162 &enterkey;</screen> pour l'installation MVME162

</para></listitem>
<listitem><para>

tapez <screen>i167 &enterkey;</screen> pour l'installation MVME166/167

</para></listitem>
    </itemizedlist>

</para><para>

Vous pouvez aussi ajouter la chaîne <screen>TERM=vt100</screen> pour utiliser 
une émulation du terminal vt100, par exemple,
<screen>i6000 TERM=vt100 &enterkey;</screen>.

</para>
  </sect2>


  <sect2 arch="m68k" id="boot-from-floppies">
  <title>Amorcer depuis des disquettes</title>
<para>

La méthode recommandée pour la plupart des architectures &arch-title; est 
d'amorcer depuis un système de fichiers local.

</para><para>

Amorcer depuis la disquette de secours n'est possible qu'avec Atari et VME 
(avec le lecteur de disquettes SCSI pour les VME). 

</para>
 </sect2>

