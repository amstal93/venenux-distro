<!-- original version: 56425 -->

<para>

Si le partitionnement vous inquiète ou si vous voulez des précisions supplémentaires,
voyez l'<xref linkend="partitioning"/>.

</para>

<warning arch="sparc"><para>
<!-- BTS: #384653 -->

Quand un disque a déjà été utilisé sous Solaris, le programme de
partitionnement peut ne pas détecter correctement la taille du disque.
Le problème n'est pas réglé par la création d'une nouvelle table des partitions.
Il faut modifier les premiers secteurs du disque de cette façon&nbsp;:

<informalexample><screen>
# dd if=/dev/zero of=/dev/hd<replaceable>X</replaceable> bs=512 count=2; sync
</screen></informalexample>

Notez que cela rendra inaccessibles les données de ce disque.
</para></warning>


<para>

Dans un premier temps, vous aurez la possibilité de partitionner un disque
entier ou une partie de disque, de façon automatique. 
C'est le partitionnement <quote>assisté</quote>. Si vous n'aimez pas
ce côté automatique, choisissez l'option 
<guimenuitem>Manuel</guimenuitem> dans le menu.
</para>
  
   <sect3 id="partman-auto">
   <title>Le partitionnement assisté</title>

<para>

Si vous choisissez le partitionnement assisté, vous aurez le choix entre trois solutions&nbsp;:
créer directement les partitions sur le disque (méthode classique), utiliser
le gestionnaire de volumes logiques, (LVM, <quote>Logical Volume management</quote>)
ou utiliser LVM avec chiffrement <footnote>

<para>
L'installateur chiffrera le groupe de volumes avec une clé AES 256 bits et utilisera
la fonction <quote>dm-crypt</quote> du noyau.</para></footnote>.
</para>
<note><para>
Il se peut toutefois que cette dernière option ne soit pas disponible
sur toutes les architectures.
</para></note>
<para>

Si vous utilisez LVM ou LVM avec chiffrement, l'installateur créera
les partitions dans une seule grande partition. L'avantage de cette méthode est la possibilité
de redimensionner facilement ces partitions. Si elle est chiffrée, la grande partition
ne sera pas accessible sans une phrase spéciale qui sécurisera vos données.

</para>
<para>

Avec LVM avec chiffrement, l'installateur effacera automatiquement le disque en y
écrivant des données aléatoires. Cela améliore la sécurité car cela rend impossible  
de dire quelles parties du disque sont utilisées et cela efface toutes les traces d'une
installation précédente. Cependant cela prend du temps.
</para>

<note><para>

Si vous avez choisi le partitionnement assisté avec LVM ou LVM avec chiffrement,
la table des partitions du disque choisi doit être modifiée. Ces modifications
détruisent réellement les données présentes sur le disque. Il ne sera pas possible
ensuite d'annuler ces modifications. Cependant l'installateur vous demandera de
confirmer ces modifications avant qu'elles ne soient écrites sur le disque.

</para></note>

<para>

Après avoir choisi le partitionnement assisté (soit la méthode classique,
soit les méthodes LVM), vous devrez choisir le disque à partitionner.
Vérifiez que tous les disques sont listés et choisissez le bon si vous avez plusieurs
disques. L'ordre de la liste peut différer de celui qui vous est habituel. La taille
des disques peut vous aider à les identifier.
</para>
<para>

Les données présentes sur le disque choisi seront finalement détruites. Mais il vous
est toujours demandé de confirmer les modifications qui doivent être apportées.
Si vous avez choisi la méthode classique, vous pourrez annuler tout changement
jusqu'à la fin du processus. Avec les méthodes LVM, cela n'est pas possible.
</para>

<para>
Vous pourrez ensuite choisir un schéma parmi les modèles décrits dans le tableau ci-dessous.
Tous ces schémas ont leurs avantages et leurs inconvénients, certains sont discutés dans 
l'<xref linkend="partitioning"/>. Choisissez le premier si vous
n'avez pas d'avis tranché. N'oubliez pas que le partitionnement assisté
a besoin de suffisamment d'espace libre pour pouvoir fonctionner. Si vous ne lui donnez
pas au moins 1&nbsp;GO, il échouera.

</para>

<informaltable>
<tgroup cols="3">
<thead>
<row>
<entry>Schéma de partitionnement</entry>
<entry>Espace minimum</entry>
  <entry>Partitions créées</entry>
</row>
</thead>

<tbody>
<row>
  <entry>Tous les fichiers dans une seule partition</entry>
  <entry>600 Mo</entry>
  <entry><filename>/</filename>, swap</entry>
</row><row>
  <entry>Partition /home distincte</entry>
  <entry>500 Mo</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>, swap
  </entry>
</row><row>
  <entry>Partitions /home, /usr, /var et /tmp distinctes</entry>
  <entry>1 Go</entry>
  <entry>
    <filename>/</filename>, <filename>/home</filename>,
    <filename>/usr</filename>, <filename>/var</filename>,
    <filename>/tmp</filename>, swap
  </entry>
</row>

</tbody></tgroup></informaltable>

<para>

Si vous avez choisi le partitionnement assisté avec LVM, l'installateur
créera aussi une partition <filename>/boot</filename> distincte.
Les autres partitions (et aussi la partition d'échange) seront créées dans la partition LVM.

</para>

<para arch="ia64">
Si vous choisissez le partitionnement assisté avec le système IA-64, une
partition supplémentaire, avec un système de fichiers FAT16 amorçable, 
sera créée pour le programme d'amorçage EFI. Il y a aussi un élément de menu
supplémentaire qui permet de formater soi-même une partition comme partition
EFI amorçable.
</para>
<para arch="alpha">

Si vous avez choisi le partitionnement automatique pour le système Alpha,
une partition non formatée sera installée au début du disque pour réserver
de l'espace pour le programme aboot.
</para>

<para>

Sur l'écran suivant, vous verrez votre table des partitions, avec le type de 
formatage des partitions et leur point de montage. 
</para>
<para>
La liste des partitions pourrait ressembler à ceci&nbsp;:

<informalexample><screen>
  IDE1 master (hda) - 6.4 Go WDC AC36400L
        #1 primary   16.4 Mo  B f ext2      /boot
        #2 primary  551.0 Mo     swap       swap
        #3 primary    5.8 Go     ntfs
           pri/log    8.2 Mo     FREE SPACE

  IDE1 slave (hdb) - 80.0 Go ST380021A
        #1 primary   15.9 Mo      ext3
        #2 primary  996.0 Mo      fat16
        #3 primary    3.9 Go      xfs        /home
        #5 logical    6.0 Go    f ext3       /
        #6 logical    1.0 Go    f ext3       /var
        #7 logical  498.8 Mo      ext3
        #8 logical  551.5 Mo      swap       swap
        #9 logical   65.8 Go      ext2
</screen></informalexample>

Cet exemple affiche les partitions de deux disques durs IDE. Le premier
possède un espace libre. Chaque ligne donne le numéro de la partition,
son type, sa taille, des drapeaux facultatifs, le système de fichiers et le
point de montage s'il existe. Note&nbsp;: cet exemple ne peut pas être créé avec
le partitionnement assisté, mais il peut l'être avec le partitionnement manuel.

</para>
<para>

Ainsi se termine le partitionnement assisté. Si vous êtes satisfait de la
table des partitions créée, vous pouvez sélectionner
<guimenuitem>Terminer le partitionnement et écrire les modifications sur  
le disque</guimenuitem> dans le menu pour la créer réellement (voyez la fin de
cette section). Si vous n'êtes pas satisfait, vous pouvez choisir
<guimenuitem>Ne pas modifier les partitions</guimenuitem>
pour relancer le partitionnement assisté ou bien pour modifier les changements proposés,
voyez le partitionnement <quote>manuel</quote> ci-dessous.

</para>
</sect3>

   <sect3 id="partman-manual">
   <title>Le partitionnement <quote>manuel</quote></title>


<para>

Un écran similaire au précédent sera affiché si vous avez choisi le
partitionnement manuel&nbsp;; mais c'est votre table des partitions qui est
montrée, sans les points de montage. La suite de cette section expliquera
comment créer une table des partitions et indiquera l'usage de ces partitions.
</para> 

<para>
Si vous avez sélectionné un disque vierge, sans partition ni espace libre,
on vous proposera de créer une table des partitions&nbsp;; c'est nécessaire
pour créer des partitions. Une nouvelle ligne, intitulée
<quote>ESPACE LIBRE</quote> devrait apparaître sous le disque sélectionné.
</para>
<para>
Si vous avez sélectionné un espace libre, on vous proposera de créer 
de nouvelles partitions. On vous posera des questions sur la taille de la
partition, son type (primaire ou logique), son début et sa fin dans
l'espace libre. Puis la nouvelle partition sera présentée en détail.
L'élément principal est <guimenuitem>Utiliser comme :</guimenuitem> qui indique si
la partition possède un système de fichiers ou si elle est utilisée comme 
partition d'échange, comme système RAID, LVM, si elle est chiffrée
ou si elle n'est pas utilisée du tout. Il y a d'autres options
comme le point de montage, les options de montage, le drapeau d'amorçage, tout dépend de
la manière dont elle est utilisée.
Si vous n'aimez pas les valeurs présélectionnées,
n'hésitez pas à les changer. Par exemple, en choisissant l'option
<guimenuitem>Utiliser comme :</guimenuitem>, vous pouvez demander un autre système de 
fichiers ou demander d'utiliser cette partition comme partition 
d'échange, comme partition RAID logiciel ou partition LVM, et même demander
de ne pas l'utiliser du tout. Une autre possibilité agréable est de pouvoir
copier les données d'une partition existante sur cette partition.
Quand vous êtes satisfait de votre nouvelle partition, choisissez
<guimenuitem>Terminer le paramétrage de la partition</guimenuitem> et vous serez ramené à
l'écran principal de <command>partman</command>.
</para>
<para>
Si vous voulez modifier votre partition, sélectionnez-la et vous reviendrez
au menu de configuration des partitions. C'est le même écran que pour une 
création et vous pourrez aussi changer les mêmes options. Ce n'est pas 
évident au premier regard, mais on peut modifier la taille de la partition en 
sélectionnant l'élément affichant la taille.
Les systèmes de fichiers fat16, fat32, ext2, ext3 et swap sont réputés bien
fonctionner. Ce menu permet aussi de supprimer une partition.
</para>

<para>
N'oubliez pas de créer au moins deux partitions, une partition d'échange,
<emphasis>swap</emphasis> et une partition pour le système de fichiers de la
racine (<emphasis>root</emphasis>) qui sera monté en <filename>/</filename>.
Si vous ne montez pas le système de fichiers de la racine, 
<command>partman</command> ne pourra continuer que si vous corrigez le 
problème. 
</para>
<para arch="ia64">
Si vous oubliez de créer une partition amorçable EFI, 
<command>partman</command> le détectera et vous demandera d'en créer une.
</para>
<para>

On peut ajouter des fonctionnalités à <command>partman</command> avec des
modules de l'installateur. Aussi, quand vous ne trouvez pas les options promises,
vérifiez que vous avez chargé tous les modules requis, par exemple,
<filename>partman-ext3</filename>,
<filename>partman-xfs</filename>, ou
<filename>partman-lvm</filename>.

</para><para>

Quand les choix de partitionnement vous conviennent, choisissez l'option 
<guimenuitem>Terminer le partitionnement</guimenuitem> du menu. Un résumé des
modifications apportées aux disques sera affiché et on vous demandera une 
confirmation avant de créer les systèmes de fichiers.

</para>
   </sect3>
