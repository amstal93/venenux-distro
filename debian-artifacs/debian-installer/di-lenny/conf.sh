# Directory configuration
ARCH=$(dpkg --print-architecture)
. conf-$ARCH.sh

KABI=$(echo $KERNELVER|cut -d- -f2)
BASE=$HOME/d-i/d-i.bpo/lenny-$ARCH
DI=$BASE/d-i
DEBIANCD=$BASE/debian-cd
DISCOVER=$BASE/discover
INITRAMFS=$BASE/initramfs-tools
MDADM=$BASE/mdadm
DEBCONF=$BASE/debconf
PCIUTILS=$BASE/pciutils
BPOCONFIG=$BASE/base-config
PARTED=$BASE/parted
LOCALAPT=$BASE/local-apt
LOCALPACKAGES=$LOCALAPT/local-deb
TMPMOUNT=/mnt
STAMPDIR=$LOCALAPT/stamp

case "$ARCH" in
i386)
	ORIGISO=rsync://hanzubon.jp/debian-cd/5.0.3/i386/iso-cd/debian-503-i386-netinst.iso
	;;
amd64)
	ORIGISO=rsync://hanzubon.jp/debian-cd/5.0.3/amd64/iso-cd/debian-503-amd64-netinst.iso
	;;
armel)
	ORIGISO=rsync://hanzubon.jp/debian-cd/5.0.3/armel/iso-cd/debian-503-armel-netinst.iso
	;;
esac
ISOIMAGE=$(echo "$ORIGISO" | sed -e "s/.*\///")
CDIMAGE=$BASE/cdimage
CDPOOLDIR=$CDIMAGE/archive/pool/main
LOCALCDIMAGE=$CDIMAGE/lenny-custom-`date +'%m%d'`.iso

CP="cp -lf"
LOCALAPTOPT="-o Dir::Etc::SourceList=$LOCALAPT/sources.list \
	-o Dir::Cache::Archives=$LOCALAPT/cache \
	-o Dir::State::Lists=$LOCALAPT/lists \
	-o Dir::State=$LOCALAPT/state \
	-o APT::GPGV::TrustedKeyring=$LOCALAPT/trusted.gpg \
	-o Debug::NoLocking=true"
BPOPOOL="http://www.backports.org/debian/pool/main/"
INDICES="http://cdn.debian.or.jp/debian/indices/"
